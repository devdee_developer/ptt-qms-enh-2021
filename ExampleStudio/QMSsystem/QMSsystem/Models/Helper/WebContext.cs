﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace QMSsystem.Models.Helper
{
    public static class WebContext
    {
        public static string GetImageBackgroudPath
        {
            get
            {
                return ConfigurationManager.AppSettings["IMAGE_BACKGROUP"];
            }
        }


        public static string GetFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["IMAGE_FILES"];
            }
        }

        public static string GetReportOffControlPath
        {
            get
            {
                return ConfigurationManager.AppSettings["RP_CONTROL_FILES"];
            }
        }
        public static string GetIqcFormulaPath
        {
            get
            {
                return ConfigurationManager.AppSettings["IQC_FORMULA_FILES"];
            }
        }

        public static string GetReportOffSpecPath
        {
            get
            {
                return ConfigurationManager.AppSettings["RP_SPEC_FILES"];
            }
        }
    }
}