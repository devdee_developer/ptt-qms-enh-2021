﻿Option Explicit On
Imports System
Imports System.IO
Imports System.Threading
Imports Excel = Microsoft.Office.Interop.Excel

Public Class Form1
    '~~> Release the objects
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click

        Dim xlApp As New Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim szPath As String
        Dim EQAddIn As Boolean

        szPath = Directory.GetCurrentDirectory()
        '~~> Start Excel and open the workbook.
        xlWorkBook = xlApp.Workbooks.Open(szPath & "\macro.xlsm")




        For Each oaddin In xlApp.AddIns
            With oaddin
                Debug.Print(oaddin.Name)
                If .Installed = True Then
                    'Debug.Print.Name()
                    xlApp.Workbooks.Open(.FullName)
                End If
                If oaddin.Name = "Exaquantum Explorer Add-In.xla" Then
                    EQAddIn = True
                End If
                If oaddin.Name = "Exaquantum Explorer Add-In.xlam" Then
                    EQAddIn = True
                End If
            End With
        Next


        '~~> Run the macros.
        xlApp.Run("RunMe")

        '~~> Clean-up: Close the workbook and quit Excel.
        xlWorkBook.Close(False)

        '~~> Quit the Excel Application
        xlApp.Quit()

        '~~> Clean Up
        releaseObject(xlApp)
        releaseObject(xlWorkBook)



    End Sub


    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        '~~> Define your Excel Objects
        Dim xlApp As New Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim szPath As String


        szPath = Directory.GetCurrentDirectory()
        '~~> Start Excel and open the workbook.
        xlWorkBook = xlApp.Workbooks.Open(szPath & "\macro.xlsm")

        '~~> Run the macros.
        xlApp.Run("ShowMsg", "Hello from VB .NET Client", "Demo 2nd Button")

        '~~> Clean-up: Close the workbook and quit Excel.
        xlWorkBook.Close(False)

        '~~> Quit the Excel Application
        xlApp.Quit()

        '~~> Clean Up
        releaseObject(xlApp)
        releaseObject(xlWorkBook)


    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click

        '~~> Define your Excel Objects
        Dim xlApp As New Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlSheets As Excel.Sheets
        Dim szPath As String
        Dim i As Integer

        Dim szTimeCol As String
        Dim szValueCol As String
        Dim szFlowCol As String

        szTimeCol = "F"
        szValueCol = "G"
        szFlowCol = "H"

        Dim EQAddIn As Boolean

        Me.Enabled = False

        For Each oaddin In xlApp.AddIns
            With oaddin
                Debug.Print(oaddin.Name)
                If .Installed = True Then
                    'Debug.Print.Name()
                    xlApp.Workbooks.Open(.FullName)
                End If
                If oaddin.Name = "Exaquantum Explorer Add-In.xla" Then
                    EQAddIn = True
                End If
                If oaddin.Name = "Exaquantum Explorer Add-In.xlam" Then
                    EQAddIn = True
                End If
            End With
        Next 

        szPath = Directory.GetCurrentDirectory()
        '~~> Start Excel and open the workbook.
        xlWorkBook = xlApp.Workbooks.Open(szPath & "\test.xlsx")
        'MessageBox.Show("start : " & xlWorkBook.ActiveSheet.Range("B2").Value & " end : " & xlWorkBook.ActiveSheet.Range("B3").Value)
        xlWorkBook.ActiveSheet.Range("B2").Value = TextBox1.Text
        xlWorkBook.ActiveSheet.Range("C2").Value = TextBox2.Text
        '~~> Run the macros.
        'xlApp.Run("InjectExaqTimeStamp", TextBox1.Text, DateTimePicker1.Value)

        Thread.Sleep(2000)
        xlWorkBook.ActiveSheet.Range("G10").Select()



        'MessageBox.Show("value : " & xlWorkBook.ActiveSheet.Range("G10").Value & " value : " & xlWorkBook.ActiveSheet.Range("B2").Value)


        'MessageBox.Show("start : " & xlWorkBook.ActiveSheet.Range("B2").Value & " end : " & xlWorkBook.ActiveSheet.Range("B3").Value)
        'MessageBox.Show("value : " & xlWorkBook.ActiveSheet.Range("G10").Value)
        'MessageBox.Show("value : " & xlWorkBook.ActiveSheet.Range("G7").Value)

     

        For num As Integer = 9 To 39
            DataGridView1.Rows.Add(New String() {xlWorkBook.ActiveSheet.Range(szTimeCol & num).Value, _
                                                 xlWorkBook.ActiveSheet.Range(szValueCol & num).Value, _
                                                 xlWorkBook.ActiveSheet.Range(szFlowCol & num).Value _
                                                })
        Next


        '~~> Clean-up: Close the workbook and quit Excel.
        xlWorkBook.Close(False)

        '~~> Quit the Excel Application
        xlApp.Quit()

        '~~> Clean Up
        releaseObject(xlApp)
        releaseObject(xlWorkBook)

        Me.Enabled = True
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
