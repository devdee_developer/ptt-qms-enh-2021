﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;

namespace TestMVC4.CoreDB
{
    public partial class STUDENT : EntityObject
    {
        public static List<STUDENT> GetAll(TestMVC4DBEntities db)
        {
            return db.STUDENT.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static STUDENT SearchById(TestMVC4DBEntities dataContext, long id)
        {
            return dataContext.STUDENT.Where(m => m.ID == id).FirstOrDefault();
        }
    }


}
