﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestMVC4.CoreDB.services;
using TestMVC4.CoreDB;
using TestMvc4.Model;
using TestMvc4.Models.Grid;
using TestMvc4.Models.Helper;


namespace TestMvc4.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }


        //[HttpPost]
        //[LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.CreateAutomoblie, PrivilegeModeEnum.ReadAutomoblie, PrivilegeModeEnum.EditAutomoblie, PrivilegeModeEnum.DeleteAutomoblie, PrivilegeModeEnum.ReportAutombolie }, DataAccess = DataAccessEnum.Authen)]
        public JsonResult CreateSTUDENT(CreateSTUDENT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                STUDENTServices carSearvice = new STUDENTServices();

                try
                {
                    STUDENT dt = new STUDENT();//Database Structure on TestMVC4DB.Designer.cs
                    DateTime toDay = DateTime.Now;


                    //  $scope.createSTUDENT = { STUDENT_ID: 0, STUDENT_NAME: "test_name", STUDENT_SNAME: "test_sname" };
                    dt.ID = model.STUDENT_ID;
                    dt.NAME = model.STUDENT_NAME;
                    dt.SNAME = model.STUDENT_SNAME;
                    dt.CREATE_USER = "get from session";
                    dt.UPDATE_DATE = toDay;
                    dt.UPDATE_USER = "get from session";                    
                    dt.CREATE_DATE = toDay;

                    long nResult = carSearvice.SaveSTUDENT(_db, dt);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.OK, message = "Save Success!" };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.Error, message = "Failure !" };
                    }


                }
                catch (Exception ex)
                {
                    
                    result = new { result = ReturnStatus.Error, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.Error, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [AllowAnonymous]
        public JsonResult GetSTUDENT(STUDENTSearch searchGrid)
        {
            //////SortColumn: "asc", SortOrder: "NAME",
            STUDENTServices carSearvice = new STUDENTServices();
            int count;
            GridSettings grid = new GridSettings(searchGrid.SortColumn, true);
            Object result;
            grid.PageIndex = searchGrid.PageIndex;
            grid.PageSize = searchGrid.PageSize;

            if ((searchGrid.SortColumn == "") || (searchGrid.SortColumn == null))
            {
                grid.SortColumn = "ID";
            }
            else
            {
                grid.SortColumn = searchGrid.SortColumn;
            }

            grid.SortOrder = searchGrid.SortOrder;

            List<STUDENT> listData = carSearvice.getSTUDENT(_db);

            //Fillter
            if ((null != searchGrid.mSearch.STUDENT_NAME) && (searchGrid.mSearch.STUDENT_NAME != "")
                &&
                (null != searchGrid.mSearch.STUDENT_SNAME) && (searchGrid.mSearch.STUDENT_SNAME != ""))
            {
                listData = listData.Where(m => m.NAME.Contains(searchGrid.mSearch.STUDENT_NAME)).ToList();
            }

            var query = grid.LoadGridData<STUDENT>(listData.AsQueryable(), out count);
            var data = query.ToArray();


            try
            {
                result = new
                {
                    result = ReturnStatus.OK,
                    total = (int)Math.Ceiling((double)count / grid.PageSize),
                    page = grid.PageIndex,
                    records = count,
                    rows = (from host in data
                            select new STUDENTView
                            {
                                STUDENT_ID = host.ID,
                                STUDENT_NAME = host.NAME,
                                STUDENT_SNAME = host.SNAME,
                                STUDENT_YEAR_NAME = ""
                            }).ToArray()
                };

            }
            catch (Exception ex)
            {
                result = new { result = ReturnStatus.Error, message = this.getEexceptionError(ex) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        //[HttpPost]
        //[LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.DeleteAutomoblie }, DataAccess = DataAccessEnum.Authen)]
        public ActionResult DeleteSTUDENT(long ID)
        {

            STUDENTServices carService = new STUDENTServices();
            Object result;
            try
            {

                bool bResult = carService.DeleteSTUDENT(_db, ID);

                if (true == bResult)
                {
                    result = new { result = ReturnStatus.OK, message = "Delete" };
                }
                else
                {
                    result = new { result = ReturnStatus.Error, message = "get Last Error" };
                }
            }
            catch (Exception ex)
            {
                result = new { result = ReturnStatus.Error, message = this.getEexceptionError(ex) };
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
