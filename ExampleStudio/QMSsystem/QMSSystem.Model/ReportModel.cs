﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace QMSSystem.Model
{
    public class ReportModel
    {
    }

    public class CreateQMS_RP_OFF_CONTROL
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public byte DOC_STATUS { get; set; }

        public string TITLE_DESC { get; set; }
        public string DETAIL_DESC { get; set; }

        public string TITLE_GRAPH { get; set; }
        public string TITLE_GRAPH_X { get; set; }
        public string TITLE_GRAPH_Y { get; set; }

        public byte AUTOGEN_FLAG { get; set; }
        public byte SHOW_DASHBOARD { get; set; }
        public byte STATUS_RAW_DATA { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

    public class ViewQMS_RP_OFF_CONTROL
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public string OFF_CONTROL_TYPE_NAME { get; set; }

        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public byte DOC_STATUS { get; set; }
        public string REPORT_DOC_STATUS { get; set; }

        public string TITLE_DESC { get; set; }
        public string DETAIL_DESC { get; set; }

        public string TITLE_GRAPH { get; set; }
        public string TITLE_GRAPH_X { get; set; }
        public string TITLE_GRAPH_Y { get; set; }
        public byte AUTOGEN_FLAG { get; set; }
        public byte SHOW_DASHBOARD { get; set; }
        public byte STATUS_RAW_DATA { get; set; }

        public bool ITEM_SELECTED { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

    public class CreateQMS_RP_OFF_CONTROL_DETAIL
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public string CONTROL_VALUE { get; set; }
        public long ROOT_CAUSE_ID { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

    public class ViewQMS_RP_OFF_CONTROL_DETAIL
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public long ROOT_CAUSE_ID { get; set; }
        public string ROOT_CAUSE_TYPE_NAME { get; set; }
        public string CONTROL_VALUE { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

    public class CreateQMS_RP_OFF_CONTROL_GRAPH
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public string PREVIOUS { get; set; }
        public decimal OFF_CONTROL { get; set; }
        public short POSITION { get; set; }
        public string GRAPH_DESC { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_RP_OFF_CONTROL_GRAPH
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public string PREVIOUS { get; set; }
        public decimal OFF_CONTROL { get; set; }
        public short POSITION { get; set; }
        public string GRAPH_DESC { get; set; }

        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }

        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

    public class CreateQMS_RP_OFF_CONTROL_ATTACH
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public string NAME { get; set; }
        public string PATH { get; set; }
        public short POSITION { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_RP_OFF_CONTROL_ATTACH
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public string NAME { get; set; }
        public string PATH { get; set; }
        public short POSITION { get; set; }
        

        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }

        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

    public class CreateQMS_RP_OFF_CONTROL_SUMMARY
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public decimal VALUE { get; set; }
        public byte DATA_TYPE { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_RP_OFF_CONTROL_SUMMARY
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public decimal VALUE { get; set; }
        public byte DATA_TYPE { get; set; }
        public string SHOW_OUTPUT { get; set; }
        public byte SHOW_RANK { get; set; }

        public byte DELETE_FLAG { get; set; }

        public byte ITEM_TYPE { get; set; }
        public bool ITEM_SELECTED { get; set; }
        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewALL_QMS_RP_OFF_CONTROL_SUMMARY
    {
        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixOffControl { get; set; }
        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixInputVolume { get; set; }
        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixPercentage { get; set; }
    }


    public class ViewMatrixSummary
    {
        public int nRows { get; set; }
        public int nCols { get; set; }
        public decimal offControl { get; set; }
        public decimal inputVol { get; set; }
        public decimal percentage { get; set; }
    }

    public class CreateQMS_RP_TREND_DATA
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte TREND_TYPE { get; set; }
        public string TREND_DESC { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 

    }

    public class ViewQMS_RP_TREND_DATA
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte TREND_TYPE { get; set; }
        public string TREND_DESC { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }

    public class ViewQMS_RP_TREND_DATA_ComboList
    {
        public long ID { get; set; }
        public string NAME { get; set; } 
    }


    public class CreateQMS_RP_TREND_DATA_DETAIL
    {
        public long ID { get; set; }
        public long TREND_DATA_ID { get; set; }

        public long CONTROL_ID { get; set; }
        public string PRODUCT_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        //public long PLANT_ID { get; set; }

        public string TEMPLATE_AREA { get; set; }
        public string SAMPLE_ID { get; set; }
        public string TANK_ID { get; set; }
        public string ITEM_ID { get; set; }

        public byte DATE_TYPE { get; set; }
        public decimal TICK_MIN { get; set; }
        public decimal TICK_MAX { get; set; }
        public decimal TICK_STEP { get; set; }
    }

    public class ViewQMS_RP_TREND_DATA_DETAIL
    {
        public long ID { get; set; }
        public long TREND_DATA_ID { get; set; }
        public long CONTROL_ID { get; set; }
        public string PRODUCT_ID { get; set; }
        public string CUSTOMER_ID { get; set; }

        //public long PLANT_ID { get; set; }
        //public long TEMPLATE_ID { get; set; }
        public string TEMPLATE_AREA { get; set; }
        public string SAMPLE_ID { get; set; }
        public string ITEM_ID { get; set; }
        public byte DATE_TYPE { get; set; }
        public decimal TICK_MIN { get; set; }
        public decimal TICK_MAX { get; set; }
        public decimal TICK_STEP { get; set; }

        public string TANK_ID { get; set; }
    }

    public class SaveResultQMS_RP_TREND
    {
        public long REPORT_MASTER { get; set; }
        public long REPORT_DETAIL_1 { get; set; }
        public long REPORT_DETAIL_2 { get; set; }
    }

    public class CreateQMS_RP_TREND
    {
        public long ID { get; set; }

        public CreateQMS_RP_TREND_DATA REPORT_MASTER { get;set; }
        public CreateQMS_RP_TREND_DATA_DETAIL REPORT_DETAIL_1 { get; set; }
        public CreateQMS_RP_TREND_DATA_DETAIL REPORT_DETAIL_2 { get; set; }
    }

    public class ViewQMS_RP_TREND
    {
        public long ID { get; set; }

        public string NAME { get; set; }
        public byte TREND_TYPE { get; set; }
        public string TREND_DESC { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public string SAMPLE { get; set; }
        public string ITEM {get; set;}

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewListRP_OFF_CONTROL_SUMMARY
    {
        public List<ViewRP_OFF_CONTROL_SUMMARY> ListOffControl { get; set; }
    }

    public class ViewRP_OFF_CONTROL_SUMMARY
    {
        public long ID { get; set; }

        public long RP_OFF_CONTROL { get; set; }
        public string PLANT { get; set; }
        public string PRODUCT { get; set; }
        public decimal OFF_VALUE { get; set; }
        public decimal INPUT_VOLUME { get; set; }
        public decimal PERCENT_VOLUME { get; set; }
    }


    public class QMS_TR_COA_DATA_DATA_LAKE
    {
        public long ID { get; set; }

        public DateTime SAMPLING_DATE { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string TANK_NAME { get; set; }
        public string DISPLAY_VALUE { get; set; }
        public string UNIT_NAME { get; set; }
        public string ITEM_NAME { get; set; }

    }

    public class QMS_TR_COA_DATA_DATA_LAKE_DELETE
    {
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }


    public class ViewALL_QMS_RP_OFF_CONTROL_DETAIL
    {
        public List<List<ViewResultQMS_RP_OFF_CONTROL_DETAIL>> matrixComposition { get; set; }
    }

    public class ViewResultQMS_RP_OFF_CONTROL_DETAIL
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string COMPOSITION { get; set; }
        public string START_DATE { get; set; }
        public decimal TOTAL_VOLUME { get; set; }
        public decimal PERCENT { get; set; }

    }

    public class ViewRETURN_QMS_RP_OFF_CONTROL_DETAIL
    {
        public List<ViewOBJ_QMS_RP_OFF_CONTROL_DETAIL> labels { get; set; }
        public List<ViewOBJ_QMS_RP_OFF_CONTROL_DETAIL> datasets { get; set; }
    }

    public class ViewOBJ_QMS_RP_OFF_CONTROL_DETAIL
    {
        public string label { get; set; }
        public string YEAR { get; set; }
        public List<ViewDATA> data { get; set; }

    }

    public class ViewYEAR
    {
        public string YEAR { get; set; }

    }
    public class ViewDATA
    {
        public Decimal VOLUME_TOTAL { get; set; }

    }


    public class OffControlStaticByYearChartBarMod
    {
        public List<string> labels = new List<string>();
        public List<OffControlStaticByYearDataBarMod> datasets { get; set; } = new List<OffControlStaticByYearDataBarMod>();
    }
    public class OffControlStaticByYearDataBarMod
    {
        public string label { get; set; }

        public List<decimal?> data = new List<decimal?>();
    }


    public class CreateQMS_TR_PRODUCT_REMARK
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string COMPOSITION { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public string SHOW_START_DATE { get; set; }
        public string SHOW_END_DATE { get; set; }
        public string ACTIVITY { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

    public class ViewQMS_TR_PRODUCT_REMARK
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string COMPOSITION { get; set; }
        public DateTime SIMPLE_DATE { get; set; }
        public string ACTIVITY { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

    }

}
