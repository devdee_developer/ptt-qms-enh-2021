﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using TestMVC4.CoreDB;
using TestMvc4.Models.Helper;

namespace TestMvc4.Controllers
{
    public class BaseController : Controller
    {
        protected TestMVC4DBEntities _db = new TestMVC4DBEntities();

            //protected BaseFont bf { get; set; }

            //protected Font fnt16 { get; set; }
            //protected Font fnt14 { get; set; }
            //protected Font fnt14Red { get; set; }
            //protected Font fnt12 { get; set; }
            //protected Font fnt11 { get; set; }
            //protected Font fnt10 { get; set; }
            //protected Font fnt9 { get; set; }
            //protected Font fnt8 { get; set; }
            //protected Font fnt7 { get; set; }
            //protected Font fntHeader { get; set; }
            //protected Font fnt11BOLD { get; set; }

            //protected Font fnt12Red { get; set; }
            //protected Font fnt12Yellow { get; set; }
            //protected Font fnt12Green { get; set; }
            //protected Font fnt12Blue { get; set; }
            //protected Font fntCancel { get; set; }

            //protected Image image { get; set; }
            //protected string imagepath { get; set; }

            //protected void initFonts()
            //{

            //    bf = BaseFont.CreateFont(Server.MapPath("~/Content/THSarabun.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            //    // สร้าง Font จาก BaseFont
            //    fnt16 = new Font(bf, 16);
            //    fnt14 = new Font(bf, 14);
            //    fnt14Red = new Font(bf, 14);
            //    fnt12 = new Font(bf, 12);
            //    fnt11 = new Font(bf, 11);
            //    fnt10 = new Font(bf, 10);
            //    fnt9 = new Font(bf, 9);
            //    fnt8 = new Font(bf, 8);
            //    fnt7 = new Font(bf, 7);

            //    fntHeader = new Font(bf, 14, Font.BOLD | Font.UNDERLINE);
            //    fnt11BOLD = new Font(bf, 14, Font.BOLD);

            //    fnt12Red = new Font(bf, 12);
            //    fnt12Yellow = new Font(bf, 12);
            //    fnt12Green = new Font(bf, 12);
            //    fnt12Blue = new Font(bf, 12);
            //    fnt12Red.Color = Color.RED;
            //    fnt12Yellow.Color = Color.YELLOW;
            //    fnt12Green.Color = Color.GREEN;
            //    fnt12Blue.Color = Color.BLUE;
            //    fnt14Red.Color = Color.RED;

            //    fntCancel = new Font(bf, 14, Font.STRIKETHRU);

            //    imagepath = Server.MapPath("~/Content/img");
            //    image = Image.GetInstance(imagepath + "/logo.jpg");

            //}

            public string errMsg { get; set; }

            /// <summary>
            /// Gets the current site session.
            /// </summary>
            //public SiteSession CurrentSiteSession
            //{
            //    get
            //    {
            //        SiteSession siteSession = (SiteSession)this.Session["SiteSession"];
            //        return siteSession;
            //    }
            //}

        //    public IEnumerable<SelectListItem> UseType = new List<SelectListItem>
        //{
        //    new SelectListItem {Value = "1", Text = Resources.String.UseMyself},
        //    new SelectListItem {Value = "2", Text = Resources.String.Rent} 
            
        //};

        //    public IEnumerable<SelectListItem> CarStatus = new List<SelectListItem>
        //{
        //    new SelectListItem {Value = "1", Text = Resources.String.Use},
        //    new SelectListItem {Value = "2", Text = Resources.String.NoUse}            
        //};

        //    public IEnumerable<SelectListItem> ListOfMonths = new List<SelectListItem>
        //{
        //    new SelectListItem {Value = "1", Text = Resources.String.January},
        //    new SelectListItem {Value = "2", Text = Resources.String.February},
        //    new SelectListItem {Value = "3", Text = Resources.String.March},
        //    new SelectListItem {Value = "4", Text = Resources.String.April},
        //    new SelectListItem {Value = "5", Text = Resources.String.May},
        //    new SelectListItem {Value = "6", Text = Resources.String.June},
        //    new SelectListItem {Value = "7", Text = Resources.String.July},
        //    new SelectListItem {Value = "8", Text = Resources.String.August},
        //    new SelectListItem {Value = "9", Text = Resources.String.September},
        //    new SelectListItem {Value = "10", Text = Resources.String.October},
        //    new SelectListItem {Value = "11", Text = Resources.String.November},
        //    new SelectListItem {Value = "12", Text = Resources.String.December},
        //};

        //    public IEnumerable<SelectListItem> getDeptList()
        //    {
        //        List<SelectListItem> listDept = new List<SelectListItem>();
        //        SiteSession siteSession = (Session["SiteSession"] == null ? null : (SiteSession)Session["SiteSession"]);

        //        if (siteSession == null)
        //        {
        //            return listDept;
        //        }

        //        List<int> list = new List<int>();
        //        list = User_department.GetUserDeptIdList(_db, siteSession.UserID);

        //        var query = Department.GetComboList(_db);
        //        listDept = (from depart in query
        //                    where list.Contains(depart.list_id)
        //                    select new SelectListItem
        //                    {
        //                        Value = depart.list_id.ToString(),
        //                        Text = depart.list_name
        //                    }
        //                        ).ToList();

        //        return listDept;
        //    }

        //    public IEnumerable<SelectListItem> getSecList()
        //    {
        //        List<SelectListItem> listSec = new List<SelectListItem>();
        //        SiteSession siteSession = (Session["SiteSession"] == null ? null : (SiteSession)Session["SiteSession"]);

        //        if (siteSession == null)
        //        {
        //            return listSec;
        //        }
        //        var query = EasyPO.Core.Section.GetComboList(_db);
        //        listSec = (from sec in query
        //                   select new SelectListItem
        //                   {
        //                       Value = sec.list_id.ToString(),
        //                       Text = sec.list_name
        //                   }
        //                    ).ToList();

        //        return listSec;
        //    }

        //    public IEnumerable<SelectListItem> getCarNameList()
        //    {

        //        List<SelectListItem> listResult = new List<SelectListItem>();
        //        SiteSession siteSession = (Session["SiteSession"] == null ? null : (SiteSession)Session["SiteSession"]);

        //        if (siteSession == null)
        //        {
        //            return listResult;
        //        }

        //        CarServices carService = new CarServices();
        //        List<Automoblie> listAut = carService.getAllCar(_db);

        //        if (null != listAut && listAut.Count() > 0)
        //        {
        //            listResult = (from dt in listAut
        //                          select new SelectListItem
        //                          {
        //                              Value = dt.am_id.ToString(),
        //                              Text = dt.am_registion
        //                          }
        //                    ).ToList();
        //        }

        //        return listResult;
        //    }

        //    public List<SelectListItem> GetListOfCarBrand()
        //    {
        //        CarServices carService = new CarServices();
        //        List<Automoblie> listCar = carService.getAllCar(_db);
        //        List<SelectListItem> listResult = new List<SelectListItem>();

        //        if (listCar.Count() > 0)
        //        {
        //            List<String> listbrand = listCar.GroupBy(m => m.am_brand).Select(m => m.Key).ToList();

        //            foreach (String dt in listbrand)
        //            {
        //                SelectListItem carbrand = new SelectListItem();
        //                carbrand.Text = dt;
        //                carbrand.Value = dt;

        //                listResult.Add(carbrand);
        //            }
        //        }
        //        return listResult;
        //    }

        //    public List<SelectListItem> GetListOfCarRegister()
        //    {
        //        CarServices carService = new CarServices();
        //        List<Automoblie> listCar = carService.getAllCar(_db);
        //        List<SelectListItem> listResult = new List<SelectListItem>();

        //        if (listCar.Count() > 0)
        //        {
        //            foreach (Automoblie dt in listCar)
        //            {
        //                SelectListItem carbrand = new SelectListItem();
        //                carbrand.Text = dt.am_registion;
        //                carbrand.Value = dt.am_id.ToString();
        //                listResult.Add(carbrand);
        //            }
        //        }
        //        return listResult;
        //    }

        //    public string genEamilData()
        //    {
        //        string bodyResult = "";
        //        try
        //        {
        //            List<ReportCarView> listData = this.getCarCost(true); //เอาแต่ค่าที่เกิน 
        //            ReportCarQtyView dataSummary = this.getCarQtyRequest();

        //            if (listData.Count() > 0)
        //            {
        //                int count = 1;
        //                bodyResult = "<table> ";
        //                bodyResult += "<thead> ";
        //                bodyResult += " <tr> ";
        //                bodyResult += " <th>" + Resources.String.Number + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarRegister + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarTaxPrice + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarTaxYear + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarInsurance + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarInsurancePay + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarInsuranceEnd + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarLawPrice + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarLawEnd + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarFinancePay + "</th> ";
        //                bodyResult += " <th>" + Resources.String.CarFinanceNext + "</th> ";
        //                bodyResult += " </tr> ";
        //                bodyResult += "</thead> ";

        //                foreach (ReportCarView dt in listData)
        //                {
        //                    bodyResult += "<tbody> ";
        //                    bodyResult += " <tr> ";
        //                    bodyResult += " <td>" + count.ToString() + "</td> ";
        //                    bodyResult += " <td>" + dt.am_registion + "</td> ";
        //                    bodyResult += " <td  align='right'>" + String.Format("{0:N}", dt.car_tax) + "</td> ";
        //                    bodyResult += " <td>" + this.ShowYearMonthDay(dt.car_tax_expire) + "</td> ";
        //                    bodyResult += " <td>" + dt.car_com_insurance + "</td> ";
        //                    bodyResult += " <td  align='right'>" + String.Format("{0:N}", dt.car_insurance) + "</td> ";
        //                    bodyResult += " <td>" + this.ShowYearMonthDay(dt.car_insurance_expire) + "</td> ";
        //                    bodyResult += " <td  align='right'>" + String.Format("{0:N}", dt.car_law_cost) + "</td> ";
        //                    bodyResult += " <td>" + this.ShowYearMonthDay(dt.car_law_date) + "</td> ";
        //                    bodyResult += " <td  align='right'>" + String.Format("{0:N}", dt.car_finance) + "</td> ";
        //                    bodyResult += " <td>" + this.ShowYearMonthDay(dt.car_finance_nextday) + "</td> ";
        //                    bodyResult += " </tr> ";
        //                    bodyResult += "</tbody> ";
        //                    count++;
        //                }

        //                bodyResult += "</table>";
        //            }

        //            bodyResult = Resources.String.CarMailWarning + bodyResult;

        //            bodyResult += "<br/><br/><br/>";

        //            bodyResult += "<table  width='400px;' >";

        //            bodyResult += "<tr>";
        //            bodyResult += "<th>" + @Resources.String.Detail + "</th>";
        //            bodyResult += "<th>" + @Resources.String.Qty + "</th>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<td>" + @Resources.String.TaxQtyReq + "</td>";
        //            bodyResult += "<td align='right'>" + String.Format("{0:N}", dataSummary.taxQty) + "</td>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<td>" + @Resources.String.LawQtyReq + "</td>";
        //            bodyResult += "<td align='right'>" + String.Format("{0:N}", dataSummary.lawQty) + "</td>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<td>" + @Resources.String.InsuranceQtyReq + "</td>";
        //            bodyResult += "<td align='right'>" + String.Format("{0:N}", dataSummary.insuranceQty) + "</td>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<td>" + @Resources.String.FinanceQtyReq + "</td>";
        //            bodyResult += "<td align='right'>" + String.Format("{0:N}", dataSummary.financeQty) + "</td>";
        //            bodyResult += "<tr>";
        //            bodyResult += "<tr>";
        //            bodyResult += "</table>";

        //        }
        //        catch (Exception ex)
        //        {

        //        }

        //        return bodyResult;
        //    }

        //    protected ReportCarQtyView getCarQtyRequest()
        //    {
        //        ReportCarQtyView data = new ReportCarQtyView();
        //        ConfigService configs = new ConfigService();
        //        Config dtConfig = configs.getConfig(_db);
        //        DateTime todayNow = DateTime.Now.AddDays(dtConfig.config_alertday);

        //        List<ReportCarView> listData = this.getCarCost(true);

        //        if (listData.Count() > 0)
        //        {
        //            data.financeQty = listData.Where(m => m.car_finance_nextday <= todayNow && m.car_finance_nextday != null).Count();
        //            data.insuranceQty = listData.Where(m => m.car_insurance_expire <= todayNow && m.car_insurance_expire != null).Count();
        //            data.lawQty = listData.Where(m => m.car_law_date <= todayNow && m.car_law_date != null).Count();
        //            data.taxQty = listData.Where(m => m.car_tax_expire <= todayNow && m.car_tax_expire != null).Count();
        //            var listTest = listData.Where(m => m.car_tax_expire <= todayNow && m.car_tax_expire != null).ToList();
        //        }



        //        return data;
        //    }


            //protected ReportCarMACostView getCarMACostRequest()
            //{
            //    ReportCarMACostView data = new ReportCarMACostView();
            //    ConfigService configs = new ConfigService();
            //    Config dtConfig = configs.getConfig(_db);
            //    DateTime todayNow = DateTime.Now.AddDays(dtConfig.config_alertday);

            //    List<ReportCarView> listData = this.getCarCost(true);

            //    if (listData.Count() > 0)
            //    {
            //        data.financeQty = listData.Where(m => m.car_finance_nextday <= todayNow).Count();
            //        data.insuranceQty = listData.Where(m => m.car_insurance_expire <= todayNow).Count();
            //        data.lawQty = listData.Where(m => m.car_law_date <= todayNow).Count();
            //        data.taxQty = listData.Where(m => m.car_tax_expire <= todayNow).Count();
            //    }

            //    return data;
            //}

            //private int getFinancePayQty(List<Aut_finance> list, long id)
            //{
            //    int iResult = 0;

            //    try
            //    {
            //        if (null != list && list.Count() > 0)
            //        {
            //            list = list.Where(m => m.fin_am_id == id).ToList();
            //            iResult = list.Count();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        errMsg = this.getEexceptionError(ex);
            //    }

            //    return iResult;
            //}

            //protected List<ReportCarView> getCarCost(bool bShowOnlyPay)
            //{
            //    List<ReportCarView> listResult = new List<ReportCarView>();
            //    CarServices carService = new CarServices();
            //    List<Automoblie> listData = carService.getAllCar(_db);
            //    List<Aut_fuel> listFuel = carService.getAllFuel(_db);
            //    List<Aut_tax> listTax = carService.getAllTaxGroupById(_db);
            //    List<Aut_insurance> listIns = carService.getAllInsuranceGroupById(_db); //carService.getAllInsurance(_db);
            //    List<Aut_finance> listFinance = carService.getAllFinanceGroupById(_db);
            //    List<Aut_law> listLaw = carService.getAllLawGroupById(_db);
            //    List<MaintananceCost> listDetail = carService.getAllMaintananceCost(_db);
            //    List<Aut_maintanance> listMa = carService.getAllMaintanance(_db);

            //    listResult = (from host in listData
            //                  select new ReportCarView
            //                  {
            //                      am_id = host.am_id,

            //                      am_registion = host.am_registion,
            //                      am_use_start = host.am_use_start,
            //                      am_number = host.am_number,
            //                      am_status = host.am_status,
            //                      am_model = host.am_model,

            //                      am_brand = host.am_brand,

            //                      am_usetype_name = this.getUseType(host.am_use_type.ToString()),
            //                      am_status_name = this.getCarStatus(host.am_use_type.ToString()),
            //                      am_fuel_name = this.getFuelName(host.am_fuel, listFuel),

            //                      car_tax = this.getTaxValue(host.am_id, listTax),
            //                      car_tax_expire = this.getTaxDate(host.am_id, listTax),
            //                      car_com_insurance = host.am_insurance,
            //                      car_insurance = this.getInsValue(host.am_id, listIns),
            //                      car_insurance_expire = this.getInsDate(host.am_id, listIns),
            //                      car_finance = this.getFinanceValue(host.am_id, listFinance),
            //                      am_finance_remain = (host.am_finance_qty - getFinancePayQty(listFinance, host.am_id)),
            //                      car_finance_nextday = this.getFinanceDate(host.am_id, listFinance),
            //                      car_law_cost = this.getLawValue(host.am_id, listLaw),
            //                      car_law_date = this.getLawDate(host.am_id, listLaw),
            //                      car_ma_cost = carService.getTotalMaintCostByCarId(host.am_id, listMa, listDetail)

            //                  }).ToList();

            //    if (true == bShowOnlyPay && listResult.Count() > 0)
            //    {
            //        ConfigService configs = new ConfigService();
            //        Config dtConfig = configs.getConfig(_db);
            //        DateTime todayNow = DateTime.Now.AddDays(dtConfig.config_alertday);

            //        listResult = listResult.Where(m => m.car_tax_expire <= todayNow
            //            || m.car_insurance_expire <= todayNow
            //            || (m.car_finance_nextday <= todayNow && m.am_finance_remain > 0)
            //            || m.car_law_date <= todayNow).ToList();

            //    }

            //    return listResult;
            //}

            //public List<SelectListItem> GetListOfYears()
            //{
            //    StockService stoService = new StockService();
            //    List<DateTime> listDateTime = stoService.getYearGroupStockRequest(_db);
            //    List<SelectListItem> listResult = new List<SelectListItem>();

            //    if (listDateTime.Count() > 0)
            //    {
            //        int maxYear = listDateTime.Max(m => m.Year);
            //        int minYear = listDateTime.Min(m => m.Year);

            //        for (int i = minYear; i <= maxYear; i++)
            //        {
            //            SelectListItem data = new SelectListItem();
            //            data.Text = (i > 2500) ? (i - 543).ToString() : i.ToString();
            //            data.Value = data.Text;
            //            listResult.Add(data);
            //        }

            //        //foreach (DateTime dt in listDateTime)
            //        //{
            //        //    SelectListItem data = new SelectListItem();
            //        //    data.Text = (dt.Year > 2500) ? (dt.Year - 543).ToString() : dt.Year.ToString();
            //        //    data.Value = data.Text;
            //        //    listResult.Add(data);
            //        //}
            //    }




            //    return listResult;
            //}

            //public string getUseType(string value)
            //{
            //    string result = "";

            //    foreach (SelectListItem dt in UseType)
            //    {
            //        if (dt.Value == value)
            //        {
            //            result = dt.Text;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public string getCarStatus(string value)
            //{
            //    string result = "";

            //    foreach (SelectListItem dt in CarStatus)
            //    {
            //        if (dt.Value == value)
            //        {
            //            result = dt.Text;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public string getFuelName(int value, List<Aut_fuel> listData)
            //{
            //    string result = "";

            //    foreach (Aut_fuel dt in listData)
            //    {
            //        if (dt.fuel_id == value)
            //        {
            //            result = dt.fuel_name;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public decimal getTaxValue(long id, List<Aut_tax> listData)
            //{
            //    decimal result = 0;
            //    //try
            //    //{ 
            //    //    result = listData.Where(m => m.tax_am_id == id).OrderBy(m => m.tax_end).Select( m=> m.tax_price).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}
            //    foreach (Aut_tax dt in listData)
            //    {
            //        if (dt.tax_am_id == id)
            //        {
            //            result = dt.tax_price;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public decimal getInsValue(long id, List<Aut_insurance> listData)
            //{
            //    decimal result = 0;

            //    //try
            //    //{
            //    //    result = listData.Where(m => m.ins_am_id == id).OrderBy(m => m.ins_end).Select(m => m.ins_prices).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}

            //    foreach (Aut_insurance dt in listData)
            //    {
            //        if (dt.ins_am_id == id)
            //        {
            //            result = dt.ins_prices;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public decimal getFinanceValue(long id, List<Aut_finance> listData)
            //{
            //    decimal result = 0;
            //    //try
            //    //{
            //    //    result = listData.Where(m => m.fin_am_id == id).OrderBy(m => m.fin_pay_date).Select(m => m.fin_price).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}

            //    foreach (Aut_finance dt in listData)
            //    {
            //        if (dt.fin_am_id == id)
            //        {
            //            result = dt.fin_price;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public decimal getLawValue(long id, List<Aut_law> listData)
            //{
            //    decimal result = 0;
            //    //try
            //    //{
            //    //    result = listData.Where(m => m.law_am_id == id).OrderBy(m => m.law_end).Select(m => m.law_price).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}

            //    foreach (Aut_law dt in listData)
            //    {
            //        if (dt.law_am_id == id)
            //        {
            //            result = dt.law_price;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public DateTime? getTaxDate(long id, List<Aut_tax> listData)
            //{
            //    DateTime? result = null;

            //    //try
            //    //{
            //    //    result = listData.Where(m => m.tax_am_id == id).OrderBy(m => m.tax_end).Select(m => m.tax_end).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    result = null;
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}
            //    foreach (Aut_tax dt in listData)
            //    {
            //        if (dt.tax_am_id == id)
            //        {
            //            result = dt.tax_end;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public DateTime? getInsDate(long id, List<Aut_insurance> listData)
            //{
            //    DateTime? result = null;

            //    //try
            //    //{
            //    //    result = listData.Where(m => m.ins_am_id == id).OrderBy(m => m.ins_end).Select(m => m.ins_end).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    result = null;
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}

            //    foreach (Aut_insurance dt in listData)
            //    {
            //        if (dt.ins_am_id == id)
            //        {
            //            result = dt.ins_end;
            //            break;
            //        }
            //    }

            //    return result;
            //}


            //public DateTime? getFinanceDate(long id, List<Aut_finance> listData)
            //{
            //    DateTime? result = null;

            //    //try
            //    //{
            //    //    result = listData.Where(m => m.fin_am_id == id).OrderBy(m => m.fin_pay_date).Select(m => m.fin_pay_date).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    result = null;
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}
            //    foreach (Aut_finance dt in listData)
            //    {
            //        if (dt.fin_am_id == id)
            //        {
            //            result = dt.fin_pay_date;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            //public DateTime? getLawDate(long id, List<Aut_law> listData)
            //{
            //    DateTime? result = null;

            //    //try
            //    //{
            //    //    result = listData.Where(m => m.law_am_id == id).OrderBy(m => m.law_end).Select(m => m.law_end).FirstOrDefault();
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    result = null;
            //    //    errMsg = this.getEexceptionError(ex);
            //    //}
            //    foreach (Aut_law dt in listData)
            //    {
            //        if (dt.law_am_id == id)
            //        {
            //            result = dt.law_end;
            //            break;
            //        }
            //    }

            //    return result;
            //}

            ///// <summary>
            ///// The data context.
            ///// </summary>
            //protected purchaseEntities _db = new purchaseEntities();
            ///// <summary>
            ///// Disable the Async support.
            ///// </summary>
            ///// <remarks>
            ///// Must be diable, otherwise ExecuteCore() will not be invoked in MVC4 like was in MVC3!
            ///// </remarks>
            //protected override bool DisableAsyncSupport
            //{
            //    get { return true; }
            //}

            ///// <summary>
            ///// Dispose the used resource.
            ///// </summary>
            ///// <param name="disposing">The disposing flag.</param>
            //protected override void Dispose(bool disposing)
            //{
            //    // _db.Dispose();
            //    base.Dispose(disposing);
            //}

            ///// <summary>
            ///// Manage the internationalization before to invokes the action in the current controller context.
            ///// </summary>
            //protected override void ExecuteCore()
            //{
            //    int culture = 0;
            //    if (this.Session == null || this.Session["CurrentUICulture"] == null)
            //    {
            //        int.TryParse(System.Configuration.ConfigurationManager.AppSettings["Culture"], out culture);
            //        this.Session["CurrentUICulture"] = culture;
            //    }
            //    else
            //    {
            //        culture = (int)this.Session["CurrentUICulture"];
            //    }
            //    //
            //    SiteSession.CurrentUICulture = culture;
            //    //
            //    // Invokes the action in the current controller context.
            //    //
            //    base.ExecuteCore();
            //}

            ///// <summary>
            ///// Called when an unhandled exception occurs in the action.
            ///// </summary>
            ///// <param name="filterContext">Information about the current request and action.</param>
            //protected override void OnException(ExceptionContext filterContext)
            //{
            //    if (filterContext.Exception is UnauthorizedAccessException)
            //    {
            //        //
            //        // Manage the Unauthorized Access exceptions
            //        // by redirecting the user to Home page.
            //        //
            //        filterContext.ExceptionHandled = true;
            //        filterContext.Result = RedirectToAction("Home", "Index");
            //    }
            //    //
            //    base.OnException(filterContext);
            //}


            ///// <summary>
            ///// Renders an action result to a string. This is done by creating a fake http context
            ///// and response objects and have that response send the data to a string builder
            ///// instead of the browser.
            ///// </summary>
            ///// <param name="result">The action result to be rendered to string.</param>
            ///// <returns>The data rendered by the given action result.</returns>
            //protected string RenderActionResultToString(ActionResult result)
            //{
            //    // Create memory writer.
            //    var sb = new StringBuilder();
            //    var memWriter = new StringWriter(sb);

            //    // Create fake http context to render the view.
            //    var fakeResponse = new HttpResponse(memWriter);
            //    var fakeContext = new HttpContext(System.Web.HttpContext.Current.Request, fakeResponse);
            //    var fakeControllerContext = new ControllerContext(
            //        new HttpContextWrapper(fakeContext),
            //        this.ControllerContext.RouteData,
            //        this.ControllerContext.Controller);
            //    var oldContext = System.Web.HttpContext.Current;
            //    System.Web.HttpContext.Current = fakeContext;

            //    // Render the view.
            //    result.ExecuteResult(fakeControllerContext);

            //    // Restore data.
            //    System.Web.HttpContext.Current = oldContext;

            //    // Flush memory and return output.
            //    memWriter.Flush();
            //    return sb.ToString();
            //}

            ///// <summary>
            ///// Returns a PDF action result. This method renders the view to a string then
            ///// use that string to generate a PDF file. The generated PDF file is then
            ///// returned to the browser as binary content. The view associated with this
            ///// action should render an XML compatible with iTextSharp xml format.
            ///// </summary>
            ///// <param name="model">The model to send to the view.</param>
            ///// <returns>The resulted BinaryContentResult.</returns>
            //protected ActionResult ViewPdf(object model)
            //{
            //    // Create the iTextSharp document.
            //    //Document doc = new Document();
            //    //// Set the document to write to memory.
            //    //MemoryStream memStream = new MemoryStream();
            //    //PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            //    //writer.CloseStream = false;
            //    //doc.Open();

            //    //// Render the view xml to a string, then parse that string into an XML dom.
            //    //string xmltext = this.RenderActionResultToString(this.View(model));
            //    //XmlDocument xmldoc = new XmlDocument();
            //    //xmldoc.InnerXml = xmltext.Trim();

            //    //// Parse the XML into the iTextSharp document.
            //    //ITextHandler textHandler = new ITextHandler(doc);
            //    //textHandler.Parse(xmldoc);

            //    //byte[] buf = null;

            //    //// Close and get the resulted binary data.
            //    //try
            //    //{
            //    //    doc.Close();
            //    //    buf = new byte[memStream.Position];
            //    //    memStream.Position = 0;
            //    //    memStream.Read(buf, 0, buf.Length);
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //   var st = ex.Message;
            //    //}

            //    //// Send the binary data to the browser.
            //    //return new BinaryContentResult(buf, "application/pdf");
            //    return View();
            //}

            protected string getModelStateError(ModelStateDictionary ModelState)
            {
                string result = "";
                try
                {
                    var errorList = (from item in ModelState.Values
                                     from error in item.Errors
                                     select error.ErrorMessage).ToList();
                    result = "System failure : " + errorList[0].ToString();
                }
                catch (Exception ex)
                {
                    result = this.getEexceptionError(ex);
                }

                return result;
            }

            public string getEexceptionError(Exception ex)
            {
                string result = "";

                if (ex.InnerException != null)
                {
                    result = ex.Message + ex.InnerException.Message;
                }
                else
                {
                    result = ex.Message;
                }

                LogHelper.Log(ex.Message);

                return result;
            }

            public void MakeFolderExist(String folderPath)
            {
                DirectoryInfo di = new DirectoryInfo(folderPath);
                try
                {
                    if (!di.Exists) //not exitsts
                    {
                        di.Create(); //create new....  
                    }
                }
                catch
                {
                    //do something........
                }
            }

            public void DeleteFile(String path)
            {
                try
                {
                    System.IO.File.Delete(path);
                }
                catch (Exception ex)
                {
                    string error = getEexceptionError(ex);
                }
            }

            public string MoveFile(string sourcePath, string desPath, string filename, string desName)
            {
                string strActualFile = "";
                try
                {

                    string strTempFile = sourcePath + filename;

                    if (System.IO.File.Exists(strTempFile))
                    {
                        strActualFile = desPath + desName;

                        this.MakeFolderExist(desPath);

                        System.IO.File.Move(strTempFile, strActualFile);
                        System.IO.File.Delete(strTempFile);
                    }
                }
                catch (Exception ex)
                {
                    strActualFile = this.getEexceptionError(ex);
                }
                return strActualFile;
            }

            public string CopyTempToPrFolder(string sourcePath, string desPath, string filename)
            {
                string strActualFile = "";
                try
                {

                    string strTempFile = sourcePath + filename;

                    if (System.IO.File.Exists(strTempFile))
                    {
                        strActualFile = desPath + filename;

                        this.MakeFolderExist(desPath);

                        System.IO.File.Move(strTempFile, strActualFile);
                        System.IO.File.Delete(strTempFile);
                    }
                }
                catch (Exception ex)
                {
                    strActualFile = this.getEexceptionError(ex);
                }
                return strActualFile;
            }

            //public string ShowNameOfPOStatus(POStatus data)
            //{

            //    //WAITAPPROVE = 0,
            //    //CANCEL = 1,
            //    //APPROVE = 2,
            //    //DELETE = 3,
            //    //UPDATE = 4,
            //    //ADD_NEW_DETAIL = 5,
            //    //UPDATE_DETAIL = 6,
            //    //DELETE_DETAIL = 7,
            //    //CLOSE_STATUS = 8

            //    string result = "";

            //    switch (data)
            //    {
            //        case POStatus.WAITAPPROVE:
            //            result = @Resources.String.StatusWaitApprove;
            //            break;

            //        case POStatus.CANCEL:
            //            result = @Resources.String.StatusCancel;
            //            break;

            //        case POStatus.APPROVE:
            //            result = @Resources.String.StatusApprove;
            //            break;

            //        case POStatus.DELETE:
            //            result = @Resources.String.CloseJob;//@Resources.String.Delete;
            //            break;

            //        case POStatus.UPDATE:
            //            result = @Resources.String.Edit;
            //            break;

            //        case POStatus.ADD_NEW_DETAIL:
            //            result = @Resources.String.Add;
            //            break;

            //        case POStatus.UPDATE_DETAIL:
            //            result = @Resources.String.Edit;
            //            break;

            //        case POStatus.DELETE_DETAIL:
            //            result = @Resources.String.CloseJob;//@Resources.String.Delete;
            //            break;

            //        case POStatus.CLOSE_STATUS:
            //            result = @Resources.String.CloseJob;
            //            break;
            //    }
            //    return result;

            //}

            //public string ShowNameOfMatStatus(MatStatus data)
            //{

            //    //    WAITAPPROVE = 0,
            //    //    CANCEL = 1,
            //    //    APPROVE = 2,
            //    //    DELETE = 3,
            //    //    UPDATE = 4,
            //    //    ADD_NEW_DETAIL = 5,
            //    //    UPDATE_DETAIL = 6,
            //    //    DELETE_DETAIL = 7,
            //    //    ISSUE_PO = 8,
            //    //    PO_APPROVE = 9,
            //    //    PO_UNAPPROVE = 10,
            //    //    CLOSE_STATUS = 11

            //    string result = "";

            //    switch (data)
            //    {
            //        case MatStatus.WAITAPPROVE:
            //            result = @Resources.String.StatusWaitApprove;
            //            break;

            //        case MatStatus.CANCEL:
            //            result = @Resources.String.StatusCancel;
            //            break;

            //        case MatStatus.APPROVE:
            //            result = @Resources.String.StatusApprove;
            //            break;

            //        case MatStatus.DELETE:
            //            result = @Resources.String.Delete;
            //            break;

            //        case MatStatus.UPDATE:
            //            result = @Resources.String.Edit;
            //            break;

            //        case MatStatus.ADD_NEW_DETAIL:
            //            result = @Resources.String.Add;
            //            break;

            //        case MatStatus.UPDATE_DETAIL:
            //            result = @Resources.String.Edit;
            //            break;

            //        case MatStatus.DELETE_DETAIL:
            //            result = @Resources.String.Delete;
            //            break;

            //        case MatStatus.ISSUE_PO:
            //            result = @Resources.String.IssuePOAlready;
            //            break;

            //        case MatStatus.PO_APPROVE:
            //            result = @Resources.String.POApprove;
            //            break;

            //        case MatStatus.PO_UNAPPROVE:
            //            result = @Resources.String.POUnApprove;
            //            break;

            //        case MatStatus.CLOSE_STATUS:
            //            result = @Resources.String.CloseJob;
            //            break;
            //    }
            //    return result;

            //}

            //public decimal ShowCashAmountSummary(Material pr)
            //{

            //    var query = Material_detail.GetListByMatIdEx(_db, pr.mat_id, (int)MatStatus.APPROVE);
            //    var dataDetail = query.ToList();
            //    decimal summary = 0;

            //    foreach (Material_detail dt in dataDetail)
            //    {
            //        summary += (dt.matd_price * dt.matd_qty);//(dt.matd_price == null) ? 0 : (double)dt.matd_price;
            //    }

            //    return summary;
            //}

            //public Double ShowAmountSummary(Purchase_order po)
            //{

            //    var query = Purchase_order_detail.GetListByPoId(_db, po.po_id);
            //    var dataDetail = query.ToList();
            //    Double summary = 0;
            //    float discountData;
            //    float vatCost;

            //    foreach (Purchase_order_detail dt in dataDetail)
            //    {
            //        summary += (dt.pod_amount == null) ? 0 : (double)dt.pod_amount;
            //    }


            //    if (po.po_discount_type == 1)
            //    {
            //        discountData = (100 - (float)po.po_discount) / 100;
            //        summary *= discountData;
            //    }
            //    else
            //    {
            //        summary -= Double.Parse(po.po_discount.ToString());
            //    }

            //    discountData = (float)po.po_vat / 100;
            //    vatCost = (float)summary * discountData;
            //    summary += vatCost;

            //    return summary;
            //}

            //public string ShowFullDate(DateTime dt)
            //{
            //    string result = "";
            //    //string[] monthStrTH = { @Resources.String.January , "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
            //    string[] monthStrTH = { @Resources.String.January, @Resources.String.February, @Resources.String.March, 
            //                        @Resources.String.April, @Resources.String.May, @Resources.String.June,
            //                        @Resources.String.July, @Resources.String.August, @Resources.String.September, 
            //                        @Resources.String.October, @Resources.String.November, @Resources.String.December 
            //                      };

            //    if (dt.Year < 2500)
            //    {

            //        result = dt.Day.ToString() + " " + monthStrTH[dt.Month - 1] + " " + (dt.Year + 543).ToString();
            //    }
            //    else
            //    {
            //        result = dt.Day.ToString() + " " + monthStrTH[dt.Month - 1] + " " + dt.Year.ToString();
            //    }

            //    return result;
            //}

            //public string ShowYearMonthDay(DateTime? dt)
            //{
            //    string result = "";

            //    if (null != dt)
            //    {

            //        if (dt.Value.Day < 10)
            //        {

            //            result += "0" + dt.Value.Day.ToString() + "/";
            //        }
            //        else
            //        {
            //            result += dt.Value.Day.ToString() + "/";
            //        }

            //        if (dt.Value.Month < 10)
            //        {

            //            result += "0" + dt.Value.Month.ToString() + "/";
            //        }
            //        else
            //        {
            //            result += dt.Value.Month.ToString() + "/";
            //        }

            //        if (dt.Value.Year < 2500)
            //        {

            //            result += (dt.Value.Year + 543).ToString();
            //        }
            //        else
            //        {
            //            result += dt.Value.Year.ToString();
            //        }
            //    }


            //    return result;
            //}

            //public string ShowDate(DateTime? dt)
            //{
            //    string result = "";

            //    if (null != dt)
            //    {
            //        //string[] monthStrTH = { "ม.ค", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย", "ก.ค", "ส.ค", "ก.ย", "ต.ค", "พ.ย", "ธ.ค" };
            //        string[] monthStrTH = { @Resources.String.Jan, @Resources.String.Feb, @Resources.String.Mar, 
            //                        @Resources.String.Apr, @Resources.String.May2, @Resources.String.Jun,
            //                        @Resources.String.Jul, @Resources.String.Aug, @Resources.String.Sep, 
            //                        @Resources.String.Oct, @Resources.String.Nov, @Resources.String.Dec 
            //                      };

            //        if (dt.Value.Year < 2500)
            //        {

            //            result = dt.Value.Day.ToString() + "/" + monthStrTH[dt.Value.Month - 1] + "/" + (dt.Value.Year + 543).ToString();
            //        }
            //        else
            //        {
            //            result = dt.Value.Day.ToString() + "/" + monthStrTH[dt.Value.Month - 1] + "/" + dt.Value.Year.ToString();
            //        }
            //    }
            //    else
            //    {

            //    }

            //    return result;
            //}

            //public string MakeSpace(int qty)
            //{
            //    string result = "";

            //    for (int i = 0; i < qty; i++)
            //    {
            //        result += " ";
            //    }

            //    return result;
            //}

            //public string ThaiBaht(string txt)
            //{
            //    string bahtTxt, n, bahtTH = "";
            //    double amount;
            //    try { amount = Convert.ToDouble(txt); }
            //    catch { amount = 0; }
            //    bahtTxt = amount.ToString("####.00");
            //    string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
            //    string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
            //    string[] temp = bahtTxt.Split('.');
            //    string intVal = temp[0];
            //    string decVal = temp[1];
            //    if (Convert.ToDouble(bahtTxt) == 0)
            //        bahtTH = "ศูนย์บาทถ้วน";
            //    else
            //    {
            //        for (int i = 0; i < intVal.Length; i++)
            //        {
            //            n = intVal.Substring(i, 1);
            //            if (n != "0")
            //            {
            //                if ((i == (intVal.Length - 1)) && (n == "1"))
            //                    bahtTH += "หนึ่ง";
            //                else if ((i == (intVal.Length - 2)) && (n == "2"))
            //                    bahtTH += "ยี่";
            //                else if ((i == (intVal.Length - 2)) && (n == "1"))
            //                    bahtTH += "";
            //                else
            //                    bahtTH += num[Convert.ToInt32(n)];
            //                bahtTH += rank[(intVal.Length - i) - 1];
            //            }
            //        }
            //        bahtTH += "บาท";
            //        if (decVal == "00")
            //            bahtTH += "ถ้วน";
            //        else
            //        {
            //            for (int i = 0; i < decVal.Length; i++)
            //            {
            //                n = decVal.Substring(i, 1);
            //                if (n != "0")
            //                {
            //                    if ((i == decVal.Length - 1) && (n == "1"))
            //                        bahtTH += "หนึ่ง";
            //                    else if ((i == (decVal.Length - 2)) && (n == "2"))
            //                        bahtTH += "ยี่";
            //                    else if ((i == (decVal.Length - 2)) && (n == "1"))
            //                        bahtTH += "";
            //                    else
            //                        bahtTH += num[Convert.ToInt32(n)];
            //                    bahtTH += rank[(decVal.Length - i) - 1];
            //                }
            //            }
            //            bahtTH += "สตางค์";
            //        }
            //    }
            //    return "(" + bahtTH + ")";
            //}

        } 
}
