﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data.EntityClient
Imports QMSSystem.Model

Public Class Form1
    Dim nTotalData As Integer
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        dpkStart.Focus()
        dpkStart.Select()
        show_ToolTip_btLoadDataLake()
        show_ToolTip_btEventRawData()
        show_ToolTip_btQmsRpOffControl()
        show_ToolTip_btQmsRpOffControlSummary()
        show_ToolTip_btQmsRpOffControlStamp()
        show_ToolTip_btAbnormal()
        show_ToolTip_btEventCal()
        show_ToolTip_btnCheckTotalEXQProduct()
    End Sub
    Public Function show_ToolTip_btLoadDataLake()
        Dim toolTip1 As New ToolTip()

        toolTip1.AutoPopDelay = 5000
        toolTip1.InitialDelay = 500
        toolTip1.ReshowDelay = 500

        toolTip1.ShowAlways = True
        toolTip1.SetToolTip(btLoadDataLake, "โหลดข้อมูลการวัดค่า Tag จากระบบ Data Lake ตามช่วงวันที่")
    End Function
    Public Function show_ToolTip_btEventRawData()
        Dim toolTip2 As New ToolTip()

        toolTip2.AutoPopDelay = 5000
        toolTip2.InitialDelay = 500
        toolTip2.ReshowDelay = 500

        toolTip2.ShowAlways = True
        toolTip2.SetToolTip(btEventRawData, "โหลดข้อมูล Event Raw Data จากระบบ Blue Print ตามช่วงวันที่")
    End Function
    Public Function show_ToolTip_btQmsRpOffControl()
        Dim toolTip3 As New ToolTip()

        toolTip3.AutoPopDelay = 5000
        toolTip3.InitialDelay = 500
        toolTip3.ReshowDelay = 500

        toolTip3.ShowAlways = True
        toolTip3.SetToolTip(btQmsRpOffControl, "โหลดข้อมูล Off Control ไปยัง Data Lake ตามช่วงวันที่")
    End Function
    Public Function show_ToolTip_btQmsRpOffControlSummary()
        Dim toolTip4 As New ToolTip()

        toolTip4.AutoPopDelay = 5000
        toolTip4.InitialDelay = 500
        toolTip4.ReshowDelay = 500

        toolTip4.ShowAlways = True
        toolTip4.SetToolTip(btQmsRpOffControlSummary, "โหลดข้อมูล Off Control Summary ไปยัง Data Lake ตามช่วงวันที่")
    End Function
    Public Function show_ToolTip_btQmsRpOffControlStamp()
        Dim toolTip5 As New ToolTip()

        toolTip5.AutoPopDelay = 5000
        toolTip5.InitialDelay = 500
        toolTip5.ReshowDelay = 500

        toolTip5.ShowAlways = True
        toolTip5.SetToolTip(btQmsRpOffControlStamp, "โหลดข้อมูล COA ที่เป็น LPG PRODUCT Density ไปยัง Data Lake ตามช่วงวันที่")
    End Function
    Public Function show_ToolTip_btAbnormal()
        Dim toolTip6 As New ToolTip()

        toolTip6.AutoPopDelay = 5000
        toolTip6.InitialDelay = 500
        toolTip6.ReshowDelay = 500

        toolTip6.ShowAlways = True
        toolTip6.SetToolTip(btAbnormal, "โหลดข้อมูล Downtime และ Reduce Feed จาก Blue Print ตามช่วงวันที่")
    End Function
    Public Function show_ToolTip_btEventCal()
        Dim toolTip7 As New ToolTip()

        toolTip7.AutoPopDelay = 5000
        toolTip7.InitialDelay = 500
        toolTip7.ReshowDelay = 500

        toolTip7.ShowAlways = True
        toolTip7.SetToolTip(btEventCal, "โหลดข้อมูล Event Cal ทั้งหมด จากระบบ Data Lake")
    End Function
    Public Function show_ToolTip_btnCheckTotalEXQProduct()
        Dim toolTip8 As New ToolTip()

        toolTip8.AutoPopDelay = 5000
        toolTip8.InitialDelay = 500
        toolTip8.ReshowDelay = 500

        toolTip8.ShowAlways = True
        toolTip8.SetToolTip(btnCheckTotalEXQProduct, "เช็คข้อมูล Total Tag Exaquantum ในระบบ ตามวันที่")
    End Function
    Private Sub BackgroundWorker1_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime
        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()
        Dim MasterDataService As New QMSSystem.CoreDB.Services.MasterDataService("Service")
        Dim cogService As New QMSSystem.CoreDB.Services.ConfigServices("Service")
        Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
        createTemplateConnect = cogService.getEXA_SCHEDULE(db)

        Try
            worker.ReportProgress(1) 'pgbStatus.Value = 1
            Dim interval As Integer = (endDate - startDate).TotalDays + 1

            If interval > 0 Then
                stepValue = 100 / (interval)
            Else
                stepValue = 100 / 1
            End If

            startDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0)
            endDate = New DateTime(dpkEnd.Value.Year, dpkEnd.Value.Month, dpkEnd.Value.Day, 23, 59, 59)

            Do Until (startDate >= endDate)

                actionDate = startDate.AddMinutes(-2) 'tempfor add data

                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                End If

                Try
                    MasterDataService.LoadDataLakeTODB(db, startDate, startDate.AddDays(1))
                    System.Threading.Thread.Sleep(1000)

                Catch ex As Exception

                End Try

                Total += stepValue
                If Total > 100 Then
                    Total = 100
                End If
                worker.ReportProgress(Convert.ToInt32(Total))

                startDate = startDate.AddDays(1)
                Threading.Thread.Sleep(500)
            Loop
        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub
    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        lblStatus.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus.Text = "Canceled!"
            btLoadDataLake.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus.Text = "Error: " & e.Error.Message
            btLoadDataLake.Enabled = True
        Else
            lblStatus.Text = "Done!"
            btLoadDataLake.Enabled = True
        End If
    End Sub
    Private Sub LoadDataLake_Click(sender As System.Object, e As System.EventArgs) Handles btLoadDataLake.Click
        btLoadDataLake.Enabled = False
        lblStatus.Text = "0%"
        If BackgroundWorker1.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker1.RunWorkerAsync()
        End If
    End Sub
    Public Function GetConnectionString() As String

        Dim DBConnection As String
        DBConnection = System.Configuration.ConfigurationManager.ConnectionStrings("QMSDBEntities").ToString()

        Return DBConnection

        Dim providerName As String = "System.Data.SqlClient"

        ' Server Localhost
        ' Specify the provider name, server and database.
        'Dim serverName As String = ".\SQLEXPRESS2008"
        'Dim databaseName As String = "PTT-GSPQualityManagementSystem_test"

        ' Server Test
        ' Specify the provider name, server and database.
        Dim serverName As String = "ptt-db-t03.ptt.corp"
        Dim databaseName As String = "PTT-GSPQualityManagementSystem_test"
        Dim UserID As String = "pttgspqprdusr"
        Dim Password As String = "cpttgspqprdusr"

        ' Server Production
        ' Specify the provider name, server and database.
        'Dim serverName As String = "ptt-db-p03.ptt.corp"
        'Dim databaseName As String = "PTT-GSPQualityManagementSystem"
        'Dim UserID As String = "pttgspqprdusr"
        'Dim Password As String = "cpttgspqprdusr"

        ' Initialize the connection string builder for the
        ' underlying provider.
        Dim sqlBuilder As New SqlConnectionStringBuilder

        ' Server Localhost
        ' Set the properties for the data source.
        'sqlBuilder.DataSource = serverName
        'sqlBuilder.InitialCatalog = databaseName
        'sqlBuilder.IntegratedSecurity = True
        'sqlBuilder.MultipleActiveResultSets = True

        'Server Test
        'Set the properties for the data source.
        sqlBuilder.DataSource = serverName
        sqlBuilder.InitialCatalog = databaseName
        sqlBuilder.PersistSecurityInfo = True
        sqlBuilder.UserID = UserID
        sqlBuilder.Password = Password

        'Server Production
        'Set the properties for the data source.
        'sqlBuilder.DataSource = serverName
        'sqlBuilder.InitialCatalog = databaseName
        'sqlBuilder.PersistSecurityInfo = True
        'sqlBuilder.UserID = UserID
        'sqlBuilder.Password = Password

        ' Build the SqlConnection connection string.
        Dim providerString As String = sqlBuilder.ToString

        ' Initialize the EntityConnectionStringBuilder.
        Dim entityBuilder As New EntityConnectionStringBuilder

        'Set the provider name.
        entityBuilder.Provider = providerName
        ' Set the provider-specific connection string.
        entityBuilder.ProviderConnectionString = providerString
        ' Set the Metadata location to the current directory.
        entityBuilder.Metadata = "res://*//QMSSystem.csdl|" & _
                                    "res://*//QMSSystem.ssdl|" & _
                                    "res://*//QMSSystem.msl"

        Return entityBuilder.ToString()
    End Function
    Private Sub btEventRawData_Click(sender As System.Object, e As System.EventArgs) Handles btEventRawData.Click
        btEventRawData.Enabled = False
        lblStatus.Text = "0%"
        If BackgroundWorker2.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker2.RunWorkerAsync()
        End If
    End Sub

    Private Sub BackgroundWorker2_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime
        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()
        Dim TransactionService As New QMSSystem.CoreDB.Services.TransactionService("Service")
        Dim cogService As New QMSSystem.CoreDB.Services.ConfigServices("Service")
        Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
        createTemplateConnect = cogService.getEXA_SCHEDULE(db)

        Try
            worker.ReportProgress(1) 'pgbStatus.Value = 1
            Dim interval As Integer = (endDate - startDate).TotalDays + 1

            If interval > 0 Then
                stepValue = 100 / (interval)
            Else
                stepValue = 100 / 1
            End If

            startDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0)
            endDate = New DateTime(dpkEnd.Value.Year, dpkEnd.Value.Month, dpkEnd.Value.Day, 23, 59, 59)

            Do Until (startDate >= endDate)

                actionDate = startDate.AddMinutes(-2) 'tempfor add data

                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                End If

                Try
                    TransactionService.LoadEventRawDataFromDataLakeTODB(db, startDate, startDate.AddDays(1))
                    System.Threading.Thread.Sleep(1000)

                Catch ex As Exception

                End Try

                Total += stepValue
                If Total > 100 Then
                    Total = 100
                End If
                worker.ReportProgress(Convert.ToInt32(Total))

                startDate = startDate.AddDays(1)
                Threading.Thread.Sleep(500)
            Loop
        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub
    Private Sub BackgroundWorker2_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker2.ProgressChanged
        lblStatus.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker2_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker2.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus.Text = "Canceled!"
            btEventRawData.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus.Text = "Error: " & e.Error.Message
            btEventRawData.Enabled = True
        Else
            lblStatus.Text = "Done!"
            btEventRawData.Enabled = True
        End If
    End Sub

    Private Sub btQmsRpOffControl_Click(sender As System.Object, e As System.EventArgs) Handles btQmsRpOffControl.Click
        btQmsRpOffControl.Enabled = False
        lblStatus.Text = "0%"
        If BackgroundWorker3.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker3.RunWorkerAsync()
        End If
    End Sub

    Private Sub BackgroundWorker3_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker3.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime
        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()
        Dim TransactionService As New QMSSystem.CoreDB.Services.TransactionService("Service")
        Dim cogService As New QMSSystem.CoreDB.Services.ConfigServices("Service")
        Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
        createTemplateConnect = cogService.getEXA_SCHEDULE(db)

        Try
            worker.ReportProgress(1) 'pgbStatus.Value = 1
            Dim interval As Integer = (endDate - startDate).TotalDays + 1

            If interval > 0 Then
                stepValue = 100 / (interval)
            Else
                stepValue = 100 / 1
            End If

            startDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0)
            endDate = New DateTime(dpkEnd.Value.Year, dpkEnd.Value.Month, dpkEnd.Value.Day, 23, 59, 59)

            Do Until (startDate >= endDate)

                actionDate = startDate.AddMinutes(-2) 'tempfor add data

                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                End If

                Try
                    TransactionService.LoadRP_OFF_CONTROLFromDBTODataLake(db, startDate, startDate.AddDays(1))
                    System.Threading.Thread.Sleep(1000)

                Catch ex As Exception

                End Try

                Total += stepValue
                If Total > 100 Then
                    Total = 100
                End If
                worker.ReportProgress(Convert.ToInt32(Total))

                startDate = startDate.AddDays(1)
                Threading.Thread.Sleep(500)
            Loop
        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub
    Private Sub BackgroundWorker3_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker3.ProgressChanged
        lblStatus.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker3_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker3.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus.Text = "Canceled!"
            btQmsRpOffControl.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus.Text = "Error: " & e.Error.Message
            btQmsRpOffControl.Enabled = True
        Else
            lblStatus.Text = "Done!"
            btQmsRpOffControl.Enabled = True
        End If
    End Sub

    Private Sub btQmsRpOffControlSummary_Click(sender As System.Object, e As System.EventArgs) Handles btQmsRpOffControlSummary.Click
        btQmsRpOffControlSummary.Enabled = False
        lblStatus.Text = "0%"
        If BackgroundWorker4.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker4.RunWorkerAsync()
        End If
    End Sub

    Private Sub BackgroundWorker4_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker4.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime
        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()
        Dim TransactionService As New QMSSystem.CoreDB.Services.TransactionService("Service")
        Dim cogService As New QMSSystem.CoreDB.Services.ConfigServices("Service")
        Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
        createTemplateConnect = cogService.getEXA_SCHEDULE(db)

        Try
            worker.ReportProgress(1) 'pgbStatus.Value = 1
            Dim interval As Integer = (endDate - startDate).TotalDays + 1

            If interval > 0 Then
                stepValue = 100 / (interval)
            Else
                stepValue = 100 / 1
            End If

            startDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0)
            endDate = New DateTime(dpkEnd.Value.Year, dpkEnd.Value.Month, dpkEnd.Value.Day, 23, 59, 59)

            Do Until (startDate >= endDate)

                actionDate = startDate.AddMinutes(-2) 'tempfor add data

                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                End If

                Try
                    TransactionService.LoadRP_OFF_CONTROL_SUMMARYFromDBTODataLake(db, startDate, startDate.AddDays(1))
                    System.Threading.Thread.Sleep(1000)

                Catch ex As Exception

                End Try

                Total += stepValue
                If Total > 100 Then
                    Total = 100
                End If
                worker.ReportProgress(Convert.ToInt32(Total))

                startDate = startDate.AddDays(1)
                Threading.Thread.Sleep(500)
            Loop
        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub
    Private Sub BackgroundWorker4_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker4.ProgressChanged
        lblStatus.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker4_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker4.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus.Text = "Canceled!"
            btQmsRpOffControlSummary.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus.Text = "Error: " & e.Error.Message
            btQmsRpOffControlSummary.Enabled = True
        Else
            lblStatus.Text = "Done!"
            btQmsRpOffControlSummary.Enabled = True
        End If
    End Sub

    Private Sub btAbnormal_Click(sender As System.Object, e As System.EventArgs) Handles btAbnormal.Click
        btAbnormal.Enabled = False
        lblStatus2.Text = "0%"
        If BackgroundWorker5.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker5.RunWorkerAsync()
        End If
    End Sub

    Private Sub BackgroundWorker5_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker5.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime
        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()
        Dim TransactionService As New QMSSystem.CoreDB.Services.TransactionService("Service")
        Dim cogService As New QMSSystem.CoreDB.Services.ConfigServices("Service")
        Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
        createTemplateConnect = cogService.getEXA_SCHEDULE(db)

        Try
            worker.ReportProgress(5) 'pgbStatus.Value = 1
            startDate = New DateTime()
            endDate = New DateTime()
            If (worker.CancellationPending = True) Then
                e.Cancel = True
            End If
            Try
                TransactionService.LoadAbnormalFromDataLakeTODB(db, startDate, endDate)
                System.Threading.Thread.Sleep(1000)

            Catch ex As Exception

            End Try
            worker.ReportProgress(Convert.ToInt32(100))

            Threading.Thread.Sleep(500)

        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub
    Private Sub BackgroundWorker5_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker5.ProgressChanged
        lblStatus2.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker5_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker5.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus2.Text = "Canceled!"
            btAbnormal.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus2.Text = "Error: " & e.Error.Message
            btAbnormal.Enabled = True
        Else
            lblStatus2.Text = "Done!"
            btAbnormal.Enabled = True
        End If
    End Sub

    Private Sub btEventCal_Click(sender As System.Object, e As System.EventArgs) Handles btEventCal.Click
        btEventCal.Enabled = False
        lblStatus2.Text = "0%"
        If BackgroundWorker6.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker6.RunWorkerAsync()
        End If
    End Sub

    Private Sub BackgroundWorker6_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker6.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime
        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()
        Dim TransactionService As New QMSSystem.CoreDB.Services.TransactionService("Service")
        Dim cogService As New QMSSystem.CoreDB.Services.ConfigServices("Service")
        Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
        createTemplateConnect = cogService.getEXA_SCHEDULE(db)

        Try
            worker.ReportProgress(5) 'pgbStatus.Value = 1
            If (worker.CancellationPending = True) Then
                e.Cancel = True
            End If
            Try
                TransactionService.LoadDataLake_EVENT_DATA_TODB(db)
                System.Threading.Thread.Sleep(1000)

            Catch ex As Exception

            End Try
            worker.ReportProgress(Convert.ToInt32(100))

            Threading.Thread.Sleep(500)

        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub
    Private Sub BackgroundWorker6_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker6.ProgressChanged
        lblStatus2.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker6_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker6.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus2.Text = "Canceled!"
            btEventCal.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus2.Text = "Error: " & e.Error.Message
            btEventCal.Enabled = True
        Else
            lblStatus2.Text = "Done!"
            btEventCal.Enabled = True
        End If
    End Sub

    Private Sub BackgroundWorker7_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker7.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime
        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()
        Dim TransactionService As New QMSSystem.CoreDB.Services.TransactionService("Service")
        Dim cogService As New QMSSystem.CoreDB.Services.ConfigServices("Service")
        Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
        createTemplateConnect = cogService.getEXA_SCHEDULE(db)

        Try
            worker.ReportProgress(1) 'pgbStatus.Value = 1
            Dim interval As Integer = (endDate - startDate).TotalDays + 1

            If interval > 0 Then
                stepValue = 100 / (interval)
            Else
                stepValue = 100 / 1
            End If

            startDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0)
            endDate = New DateTime(dpkEnd.Value.Year, dpkEnd.Value.Month, dpkEnd.Value.Day, 23, 59, 59)

            Do Until (startDate >= endDate)

                actionDate = startDate.AddMinutes(-2) 'tempfor add data

                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                End If

                Try
                    TransactionService.LoadOffControlFromDBTODataLake(db, startDate, startDate.AddDays(1))
                    System.Threading.Thread.Sleep(1000)

                Catch ex As Exception

                End Try

                Total += stepValue
                If Total > 100 Then
                    Total = 100
                End If
                worker.ReportProgress(Convert.ToInt32(Total))

                startDate = startDate.AddDays(1)
                Threading.Thread.Sleep(500)
            Loop
        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub

    Private Sub btQmsRpOffControlStamp_Click(sender As System.Object, e As System.EventArgs) Handles btQmsRpOffControlStamp.Click
        btQmsRpOffControlStamp.Enabled = False
        lblStatus.Text = "0%"
        If BackgroundWorker7.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker7.RunWorkerAsync()
        End If
    End Sub
    Private Sub BackgroundWorker7_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker7.ProgressChanged
        lblStatus.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker7_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker7.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus.Text = "Canceled!"
            btQmsRpOffControlStamp.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus.Text = "Error: " & e.Error.Message
            btQmsRpOffControlStamp.Enabled = True
        Else
            lblStatus.Text = "Done!"
            btQmsRpOffControlStamp.Enabled = True
        End If
    End Sub

    Private Sub btnCheckTotalEXQProduct_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckTotalEXQProduct.Click
        btnCheckTotalEXQProduct.Enabled = False
        Dim startDate As DateTime
        startDate = dpkStart.Value
        lblStatus2.Text = "0%"
        If BackgroundWorker8.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker8.RunWorkerAsync()
        End If


    End Sub

    Private Sub BackgroundWorker8_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker8.ProgressChanged
        lblStatus2.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub
    Private Sub BackgroundWorker8_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker8.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus2.Text = "Canceled!"
            btnCheckTotalEXQProduct.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus2.Text = "Error: " & e.Error.Message
            btnCheckTotalEXQProduct.Enabled = True
        Else
            lblStatus2.Text = "Done!" & " Total " & nTotalData.ToString() & " rows"
            btnCheckTotalEXQProduct.Enabled = True
            'lblStatus.Text = " Total " & nTotalData.ToString() & " rows"
        End If
    End Sub

    Private Sub BackgroundWorker8_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker8.DoWork
        Dim startDate As DateTime
        startDate = New DateTime(dpkStart.Value.Year, dpkStart.Value.Month, dpkStart.Value.Day, 0, 0, 0)

        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim TransactionService As New QMSSystem.CoreDB.Services.TransactionService("Service")
        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities()



        nTotalData = TransactionService.getTotalDataForCheckDataLakeInputing(db, startDate) 
        worker.ReportProgress(100)
    End Sub
End Class
