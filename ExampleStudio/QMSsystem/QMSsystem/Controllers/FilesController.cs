﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;
using System.Drawing; 
using System.Text.RegularExpressions;
using QMSsystem.Models.Helper;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSSystem.CoreDB;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.All }, DataAccess = DataAccessEnum.Authen)]
    public class FilesController : BaseController
    {
        //
        // GET: /Files/
        private static readonly Dictionary<string, string> MIMETypesDictionary = new Dictionary<string, string>
          {
            {"ai", "application/postscript"},
            {"aif", "audio/x-aiff"},
            {"aifc", "audio/x-aiff"},
            {"aiff", "audio/x-aiff"},
            {"asc", "text/plain"},
            {"atom", "application/atom+xml"},
            {"au", "audio/basic"},
            {"avi", "video/x-msvideo"},
            {"bcpio", "application/x-bcpio"},
            {"bin", "application/octet-stream"},
            {"bmp", "image/bmp"},
            {"cdf", "application/x-netcdf"},
            {"cgm", "image/cgm"},
            {"class", "application/octet-stream"},
            {"cpio", "application/x-cpio"},
            {"cpt", "application/mac-compactpro"},
            {"csh", "application/x-csh"},
            {"css", "text/css"},
            {"dcr", "application/x-director"},
            {"dif", "video/x-dv"},
            {"dir", "application/x-director"},
            {"djv", "image/vnd.djvu"},
            {"djvu", "image/vnd.djvu"},
            {"dll", "application/octet-stream"},
            {"dmg", "application/octet-stream"},
            {"dms", "application/octet-stream"},
            {"doc", "application/msword"},
            {"docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {"dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
            {"docm","application/vnd.ms-word.document.macroEnabled.12"},
            {"dotm","application/vnd.ms-word.template.macroEnabled.12"},
            {"dtd", "application/xml-dtd"},
            {"dv", "video/x-dv"},
            {"dvi", "application/x-dvi"},
            {"dxr", "application/x-director"},
            {"eps", "application/postscript"},
            {"etx", "text/x-setext"},
            {"exe", "application/octet-stream"},
            {"ez", "application/andrew-inset"},
            {"gif", "image/gif"},
            {"gram", "application/srgs"},
            {"grxml", "application/srgs+xml"},
            {"gtar", "application/x-gtar"},
            {"hdf", "application/x-hdf"},
            {"hqx", "application/mac-binhex40"},
            {"htm", "text/html"},
            {"html", "text/html"},
            {"ice", "x-conference/x-cooltalk"},
            {"ico", "image/x-icon"},
            {"ics", "text/calendar"},
            {"ief", "image/ief"},
            {"ifb", "text/calendar"},
            {"iges", "model/iges"},
            {"igs", "model/iges"},
            {"jnlp", "application/x-java-jnlp-file"},
            {"jp2", "image/jp2"},
            {"jpe", "image/jpeg"},
            {"jpeg", "image/jpeg"},
            {"jpg", "image/jpeg"},
            {"js", "application/x-javascript"},
            {"kar", "audio/midi"},
            {"latex", "application/x-latex"},
            {"lha", "application/octet-stream"},
            {"lzh", "application/octet-stream"},
            {"m3u", "audio/x-mpegurl"},
            {"m4a", "audio/mp4a-latm"},
            {"m4b", "audio/mp4a-latm"},
            {"m4p", "audio/mp4a-latm"},
            {"m4u", "video/vnd.mpegurl"},
            {"m4v", "video/x-m4v"},
            {"mac", "image/x-macpaint"},
            {"man", "application/x-troff-man"},
            {"mathml", "application/mathml+xml"},
            {"me", "application/x-troff-me"},
            {"mesh", "model/mesh"},
            {"mid", "audio/midi"},
            {"midi", "audio/midi"},
            {"mif", "application/vnd.mif"},
            {"mov", "video/quicktime"},
            {"movie", "video/x-sgi-movie"},
            {"mp2", "audio/mpeg"},
            {"mp3", "audio/mpeg"},
            {"mp4", "video/mp4"},
            {"mpe", "video/mpeg"},
            {"mpeg", "video/mpeg"},
            {"mpg", "video/mpeg"},
            {"mpga", "audio/mpeg"},
            {"ms", "application/x-troff-ms"},
            {"msh", "model/mesh"},
            {"mxu", "video/vnd.mpegurl"},
            {"nc", "application/x-netcdf"},
            {"oda", "application/oda"},
            {"ogg", "application/ogg"},
            {"pbm", "image/x-portable-bitmap"},
            {"pct", "image/pict"},
            {"pdb", "chemical/x-pdb"},
            {"pdf", "application/pdf"},
            {"pgm", "image/x-portable-graymap"},
            {"pgn", "application/x-chess-pgn"},
            {"pic", "image/pict"},
            {"pict", "image/pict"},
            {"png", "image/png"}, 
            {"pnm", "image/x-portable-anymap"},
            {"pnt", "image/x-macpaint"},
            {"pntg", "image/x-macpaint"},
            {"ppm", "image/x-portable-pixmap"},
            {"ppt", "application/vnd.ms-powerpoint"},
            {"pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {"potx","application/vnd.openxmlformats-officedocument.presentationml.template"},
            {"ppsx","application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
            {"ppam","application/vnd.ms-powerpoint.addin.macroEnabled.12"},
            {"pptm","application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
            {"potm","application/vnd.ms-powerpoint.template.macroEnabled.12"},
            {"ppsm","application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
            {"ps", "application/postscript"},
            {"qt", "video/quicktime"},
            {"qti", "image/x-quicktime"},
            {"qtif", "image/x-quicktime"},
            {"ra", "audio/x-pn-realaudio"},
            {"ram", "audio/x-pn-realaudio"},
            {"ras", "image/x-cmu-raster"},
            {"rdf", "application/rdf+xml"},
            {"rgb", "image/x-rgb"},
            {"rm", "application/vnd.rn-realmedia"},
            {"roff", "application/x-troff"},
            {"rtf", "text/rtf"},
            {"rtx", "text/richtext"},
            {"sgm", "text/sgml"},
            {"sgml", "text/sgml"},
            {"sh", "application/x-sh"},
            {"shar", "application/x-shar"},
            {"silo", "model/mesh"},
            {"sit", "application/x-stuffit"},
            {"skd", "application/x-koan"},
            {"skm", "application/x-koan"},
            {"skp", "application/x-koan"},
            {"skt", "application/x-koan"},
            {"smi", "application/smil"},
            {"smil", "application/smil"},
            {"snd", "audio/basic"},
            {"so", "application/octet-stream"},
            {"spl", "application/x-futuresplash"},
            {"src", "application/x-wais-source"},
            {"sv4cpio", "application/x-sv4cpio"},
            {"sv4crc", "application/x-sv4crc"},
            {"svg", "image/svg+xml"},
            {"swf", "application/x-shockwave-flash"},
            {"t", "application/x-troff"},
            {"tar", "application/x-tar"},
            {"tcl", "application/x-tcl"},
            {"tex", "application/x-tex"},
            {"texi", "application/x-texinfo"},
            {"texinfo", "application/x-texinfo"},
            {"tif", "image/tiff"},
            {"tiff", "image/tiff"},
            {"tr", "application/x-troff"},
            {"tsv", "text/tab-separated-values"},
            {"txt", "text/plain"},
            {"ustar", "application/x-ustar"},
            {"vcd", "application/x-cdlink"},
            {"vrml", "model/vrml"},
            {"vxml", "application/voicexml+xml"},
            {"wav", "audio/x-wav"},
            {"wbmp", "image/vnd.wap.wbmp"},
            {"wbmxl", "application/vnd.wap.wbxml"},
            {"wml", "text/vnd.wap.wml"},
            {"wmlc", "application/vnd.wap.wmlc"},
            {"wmls", "text/vnd.wap.wmlscript"},
            {"wmlsc", "application/vnd.wap.wmlscriptc"},
            {"wrl", "model/vrml"},
            {"xbm", "image/x-xbitmap"},
            {"xht", "application/xhtml+xml"},
            {"xhtml", "application/xhtml+xml"},
            {"xls", "application/vnd.ms-excel"},                        
            {"xml", "application/xml"},
            {"xpm", "image/x-xpixmap"},
            {"xsl", "application/xml"},
            {"xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {"xltx","application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
            {"xlsm","application/vnd.ms-excel.sheet.macroEnabled.12"},
            {"xltm","application/vnd.ms-excel.template.macroEnabled.12"},
            {"xlam","application/vnd.ms-excel.addin.macroEnabled.12"},
            {"xlsb","application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
            {"xslt", "application/xslt+xml"},
            {"xul", "application/vnd.mozilla.xul+xml"},
            {"xwd", "image/x-xwindowdump"},
            {"xyz", "chemical/x-xyz"},
            {"zip", "application/zip"}
          };
          
        public static string GetMIMEType(string fileName)
        {
            //get file extension
            string extension = Path.GetExtension(fileName).ToLowerInvariant();

            if (extension.Length > 0 &&
                MIMETypesDictionary.ContainsKey(extension.Remove(0, 1)))
            {
                return MIMETypesDictionary[extension.Remove(0, 1)];
            }
            return "unknown/unknown";
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SaveBackgroudImage()
        {
            //BackgroundWorker test = new BackgroundWorker();
            string strTempFileName = ""; 
            DateTime logTime = DateTime.Now;
          
            object result;

            JsonResult status = new JsonResult();
            ConfigServices cogService = new ConfigServices(_username);
            try
            {

                result = new { result = "Success" }; 
                if (Request.Files.Count > 0)
                {

                    if ((Request.Files[0].ContentLength > 0))// && (Request.Files[0].ContentLength < FileLength))
                    {

                        string extension = System.IO.Path.GetExtension(Request.Files[0].FileName);
                        strTempFileName = Request.Files[0].FileName;

                        //#region check_extension
                        //if (extension.ToLower() != ".xls" && extension.ToLower() != ".xlsx" && extension.ToLower() != ".csv")
                        //{
                        //    result = new { result = "Error", message = @Resources.Strings.FormatXLSOnly };
                        //    return Json(result, JsonRequestBehavior.AllowGet);
                        //}
                        //#endregion

                        // Save  file
                        // Request.Files["FileUpload"].FileName; // iRandomNumber1 + iRandomNumber2 + strFileExtension;
                        string strSavePath = Server.MapPath(WebContext.GetImageBackgroudPath);// +"ptt_head_new.jpg";
                        string strTempFile = strSavePath + "ptt_head_new.jpg";   
                        this.MakeFolderExist(strSavePath);

                        if (System.IO.File.Exists(strTempFile))
                            System.IO.File.Delete(strTempFile);

                        Request.Files[0].SaveAs(strTempFile);

                        cogService.SaveStampUser(_db, (int)ST_CONFIG_STAMP.S_Home);
                         
                        result = new { result = "Success", message = "Success"  };
                    }
                    else
                    {
                        result = new { result = "Error", message = @Resources.Strings.Error};
                    }

                }
                else
                {
                    result = new { result = "Error", message = @Resources.Strings.NoFile };
                }

                //return status;
            }
            catch (Exception ex)
            {
                result = new { result = "Error", message = this.getEexceptionError(ex) };
            }


            if (!Request.AcceptTypes.Contains("application/json"))
                return base.Json(result, "text/plain");
            else
                return base.Json(result);
        }

        public JsonResult SaveTemplateImport()
        {
            
            string strTempFileName = "";
            DateTime logTime = DateTime.Now;

            object result;

            JsonResult status = new JsonResult();

            try
            {

                result = new { result = "Success" };
                if (Request.Files.Count > 0)
                {
                    if ((Request.Files[0].ContentLength > 0))// && (Request.Files[0].ContentLength < FileLength))
                    {

                        long plantId = Convert.ToInt64(Request.Form["plantId"]);
                        long productId = Convert.ToInt64(Request.Form["productId"]);
                        int downloadType = Convert.ToInt32(Request.Form["downloadType"]);
                        DateTime TagDate = Convert.ToDateTime( Request.Form["TagDate"] );
                        string msgError = "Success";
                        long nResult = 0;

                        TagDate = new DateTime(TagDate.Year, TagDate.Month, TagDate.Day, 0, 0, 0);

                        string extension = System.IO.Path.GetExtension(Request.Files[0].FileName);

                        if (extension.ToLower() != ".xls" && extension.ToLower() != ".xlsx") // && extension.ToLower() != ".csv")
                        {
                            result = new { result = "Error", message = @Resources.Strings.FormatXLSOnly };
                        }
                        else
                        {
                            TransactionService transService = new TransactionService(_username);
                             
                            MasterDataService masterDataServices = new MasterDataService(_username);
                            strTempFileName = Request.Files[0].FileName;
                            string strSavePath = Server.MapPath(WebContext.GetFilePath);// +"ptt_head_new.jpg";
                            string strTempFile = strSavePath + "ImportFileTemp"  + extension; //strSavePath + DateTime.Now.ToString("yyyyMMddhhmmss") + extension; 
                            this.MakeFolderExist(strSavePath);

                            if (System.IO.File.Exists(strTempFile))
                                System.IO.File.Delete(strTempFile);
                            Request.Files[0].SaveAs(strTempFile);

                            if (downloadType == (byte)TEMPLATE_IMP_EXP_TYPE.PRODUCT)
                            {
                                nResult = transService.SaveQMS_TR_EXQ_PRODUCTByExcelFile(_db, strTempFile, TagDate, downloadType, plantId, productId);
                            }
                            else if (downloadType == (byte)TEMPLATE_IMP_EXP_TYPE.DOWNTIME)
                            {
                                nResult = transService.SaveQMS_TR_EXQ_DOWNTIMEByExcelFile(_db, strTempFile, TagDate, downloadType, plantId, productId);
                            }
                            else
                            {
                                nResult = transService.SaveQMS_TR_EXQ_REDUCE_FEEDByExcelFile(_db, strTempFile, TagDate, downloadType, plantId, productId);
                            }

                            //List<ShiftYear> listYear = masterDataServices.getShiftYear(_db);

                            if (nResult == -1)
                            {
                                msgError = transService._errMsg;
                            }

                            result = new { result = "Success", message = msgError  };
                        }
                    }
                    else
                    {
                        result = new { result = "Error", message = @Resources.Strings.Error };
                    }

                }
                else
                {
                    result = new { result = "Error", message = @Resources.Strings.NoFile };
                } 
            }
            catch (Exception ex)
            {
                result = new { result = "Error", message = this.getEexceptionError(ex) };
            }


            if (!Request.AcceptTypes.Contains("application/json"))
                return base.Json(result, "text/plain");
            else
                return base.Json(result);
        }

        public JsonResult SaveShiftOperation()
        {
            //BackgroundWorker test = new BackgroundWorker();
            string strTempFileName = "";
            DateTime logTime = DateTime.Now;

            object result;

            JsonResult status = new JsonResult();

            try
            {

                result = new { result = "Success" };
                if (Request.Files.Count > 0)
                {
                    if ((Request.Files[0].ContentLength > 0))// && (Request.Files[0].ContentLength < FileLength))
                    {
                        short year = Convert.ToInt16(Request.Form["year"]);

                        string extension = System.IO.Path.GetExtension(Request.Files[0].FileName);

                        if (extension.ToLower() != ".xls" && extension.ToLower() != ".xlsx") // && extension.ToLower() != ".csv")
                        {
                            result = new { result = "Error", message = @Resources.Strings.FormatXLSOnly };
                        }
                        else
                        {
                            MasterDataService masterDataServices = new MasterDataService(_username);
                            strTempFileName = Request.Files[0].FileName;
                            string strSavePath = Server.MapPath(WebContext.GetFilePath);// +"ptt_head_new.jpg";
                            string strTempFile = strSavePath + year.ToString() + extension; //strSavePath + DateTime.Now.ToString("yyyyMMddhhmmss") + extension; 
                            this.MakeFolderExist(strSavePath);

                            if (System.IO.File.Exists(strTempFile))
                                System.IO.File.Delete(strTempFile);
                            Request.Files[0].SaveAs(strTempFile);

                            masterDataServices.saveQMS_MA_OPERATION_SHIFTByExcelFile(_db, strTempFile, year);

                            List<ShiftYear> listYear = masterDataServices.getShiftYear(_db);

                            result = new { result = "Success", message = "Success", listYear = listYear };
                        }
                    }
                    else
                    {
                        result = new { result = "Error", message = @Resources.Strings.Error };
                    }

                }
                else
                {
                    result = new { result = "Error", message = @Resources.Strings.NoFile };
                }

                //return status;
            }
            catch (Exception ex)
            {
                result = new { result = "Error", message = this.getEexceptionError(ex) };
            }


            if (!Request.AcceptTypes.Contains("application/json"))
                return base.Json(result, "text/plain");
            else
                return base.Json(result);
        }

        public JsonResult SaveFileAttachReport()
        {
            //BackgroundWorker test = new BackgroundWorker();
            string strTempFileName = "";
            DateTime logTime = DateTime.Now;

            object result = new { result = "Error", message = @Resources.Strings.Error }; ;

            JsonResult status = new JsonResult();

            try
            {

                result = new { result = "Success" };
                if (Request.Files.Count > 0)
                {
                    if ((Request.Files[0].ContentLength > 0))// && (Request.Files[0].ContentLength < FileLength))
                    { 
                        string thisFileName = Request.Form["FileName"];
                        long thisRP_OFF_CONTROL_ID = Convert.ToInt64(Request.Form["RP_OFF_CONTROL_ID"]);
                        long RP_OFF_CONTROL_ATTACH_ID = Convert.ToInt64(Request.Form["RP_OFF_CONTROL_ATTACH_ID"]);
                        string extension = System.IO.Path.GetExtension(Request.Files[0].FileName);

                        if (extension.ToLower() != ".xls" && extension.ToLower() != ".xlsx"
                            && extension.ToLower() != ".csv" && extension.ToLower() != ".pdf"
                            && extension.ToLower() != ".doc" && extension.ToLower() != ".docx" 
                            ) //  .xls,.csv,.xlsx,.doc,.docx,.pdf)
                        {
                            result = new { result = "Error", message = @Resources.Strings.FormatDocumentOnly };
                        }
                        else
                        {

                            if (null == thisFileName ||  thisFileName == "")
                            {
                                result = new { result = "Error", message = @Resources.Strings.PleaseInputFileName };
                            }
                            else
                            {
                                strTempFileName = Request.Files[0].FileName;

                                ReportServices reportServices = new ReportServices(_username);
                                List<QMS_RP_OFF_CONTROL> listOffControl = QMS_RP_OFF_CONTROL.GetAllType(_db);
                                byte OFF_CONTROL_TYPE = reportServices.getTypeOffControl(listOffControl, thisRP_OFF_CONTROL_ID);
                                string strSavePath;
                                string urlSavePath;
                                if (OFF_CONTROL_TYPE == 1)
                                {
                                    urlSavePath = WebContext.GetReportOffControlPath;
                                    strSavePath = Server.MapPath(WebContext.GetReportOffControlPath);// +"ptt_head_new.jpg";
                                }
                                else
                                {
                                    urlSavePath = WebContext.GetReportOffSpecPath;
                                    strSavePath = Server.MapPath(WebContext.GetReportOffSpecPath);
                                }
                                string newFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + extension;
                                string strTempFile = strSavePath + newFileName;
                                this.MakeFolderExist(strSavePath);

                                if (System.IO.File.Exists(strTempFile))
                                    System.IO.File.Delete(strTempFile);
                                Request.Files[0].SaveAs(strTempFile);

                                string thisFileUpload = strTempFile;

                                ReportServices ReportServices = new ReportServices(_username);
                                CreateQMS_RP_OFF_CONTROL_ATTACH model = new CreateQMS_RP_OFF_CONTROL_ATTACH();
                                model.ID = RP_OFF_CONTROL_ATTACH_ID;
                                model.RP_OFF_CONTROL_ID = thisRP_OFF_CONTROL_ID;
                                model.NAME = thisFileName;
                                model.PATH = urlSavePath + newFileName; ;


                                try
                                {
                                    long nResult = ReportServices.SaveQMS_RP_OFF_CONTROL_ATTACH(_db, model);

                                    if (nResult > 0)
                                    {
                                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                                    }
                                    else
                                    {
                                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                                    }
                                }
                                catch (Exception ex)
                                {
                                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                                }

                                result = new { result = "Success", message = "Success", thisFileUpload };
                            }  
                        }
                    }
                    else
                    {
                        result = new { result = "Error", message = @Resources.Strings.Error };
                    }

                }
                else
                {
                    result = new { result = "Error", message = @Resources.Strings.NoFile };
                }

                //return status;
            }
            catch (Exception ex)
            {
                result = new { result = "Error", message = this.getEexceptionError(ex) };
            }


            if (!Request.AcceptTypes.Contains("application/json"))
                return base.Json(result, "text/plain");
            else
                return base.Json(result);
        }
        public JsonResult SaveFileAttachIQCFormula()
        {
            //BackgroundWorker test = new BackgroundWorker();
            string strTempFileName = "";
            DateTime logTime = DateTime.Now;

            object result = new { result = "Error", message = @Resources.Strings.Error }; ;

            JsonResult status = new JsonResult();

            try
            {

                result = new { result = "Success" };
                if (Request.Files.Count > 0)
                {
                    if ((Request.Files[0].ContentLength > 0))// && (Request.Files[0].ContentLength < FileLength))
                    {
                        string thisFileName = Request.Form["FileName"];
                        long QMS_MA_IQC_METHOD_ID = Convert.ToInt64(Request.Form["QMS_MA_IQC_METHOD_ID"]);
                       
                        string extension = System.IO.Path.GetExtension(Request.Files[0].FileName);

                        if (extension.ToLower() != ".xls" && extension.ToLower() != ".xlsx"
                            && extension.ToLower() != ".csv" && extension.ToLower() != ".pdf"
                            && extension.ToLower() != ".doc" && extension.ToLower() != ".docx"
                            ) //  .xls,.csv,.xlsx,.doc,.docx,.pdf)
                        {
                            result = new { result = "Error", message = @Resources.Strings.FormatDocumentOnly };
                        }
                        else
                        {

                            if (null == thisFileName || thisFileName == "")
                            {
                                result = new { result = "Error", message = @Resources.Strings.PleaseInputFileName };
                            }
                            else
                            {
                                strTempFileName = Request.Files[0].FileName;

                              
                                string strSavePath;
                                string urlSavePath;

                                    urlSavePath = WebContext.GetIqcFormulaPath;
                                    strSavePath = Server.MapPath(WebContext.GetIqcFormulaPath);// +"ptt_head_new.jpg";

                                    //string newFileName = Path.GetFileNameWithoutExtension(thisFileName);
                                    string newFileName = Path.GetFileNameWithoutExtension(thisFileName) + DateTime.Now.ToString("yyyyMMddhhmmss") + QMS_MA_IQC_METHOD_ID + extension;
                                string strTempFile = strSavePath + newFileName;
                                string filename = Path.GetFileNameWithoutExtension(strTempFile);
                                this.MakeFolderExist(strSavePath);

                                if (System.IO.File.Exists(strTempFile))
                                    System.IO.File.Delete(strTempFile);
                                Request.Files[0].SaveAs(strTempFile);

                                string thisFileUpload = strTempFile;


                                result = new { result = "Success", message = newFileName, thisFileUpload };
                            }
                        }
                    }
                    else
                    {
                        result = new { result = "Error", message = @Resources.Strings.Error };
                    }

                }
                else
                {
                    result = new { result = "Error", message = @Resources.Strings.NoFile };
                }

                //return status;
            }
            catch (Exception ex)
            {
                result = new { result = "Error", message = this.getEexceptionError(ex) };
            }


            if (!Request.AcceptTypes.Contains("application/json"))
                return base.Json(result, "text/plain");
            else
                return base.Json(result);
        } 

        public void DownloadImage(string imageData, string fname)
        {

            object result;
            try
            {
                string imageContent = Regex.Match(imageData, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                byte[] contents = Convert.FromBase64String(imageContent);
                MemoryStream ms = new MemoryStream(contents);
                Image returnImage = Image.FromStream(ms); 

               if (ms != null)
               {
                    Byte[] byteArray = ms.ToArray();
                    ms.Flush();
                    ms.Close();
                    Response.BufferOutput = true;
                    // Clear all content output from the buffer stream
                    Response.Clear(); 
                    Response.ClearHeaders();
                    // Add a HTTP header to the output stream that specifies the default filename
                    // for the browser’s download dialog
                    string timeStamp = Convert.ToString(DateTime.Now.ToString("MMddyyyy_HHmmss"));
                    Response.AddHeader("Content-Disposition",
                                       "attachment; filename=testFileName_" + timeStamp + ".fileextention");
                    // Set the HTTP MIME type of the output stream
                    Response.ContentType = "application/octet-stream";
                    // Write the data
                    Response.BinaryWrite(byteArray);
                    Response.End();
                }

 
            }
            catch (Exception ex)
            {
                result = new { result = "Error", message = this.getEexceptionError(ex) };
            } 
        }
         
        public void DownloadAttachReportFile(long id)
        {
            ReportServices respService = new ReportServices(_username);

            CreateQMS_RP_OFF_CONTROL_ATTACH repAttachData =  respService.getQMS_RP_OFF_CONTROL_ATTACHById(_db, id);

            string filename = repAttachData.PATH;

            string name = "";
            string[] words = filename.Split('/');
            foreach (string word in words)
            {
                name = word;
            }
            try
            {
                string desPath = "";
                desPath = Server.MapPath(filename);
                string MIMEtype = GetMIMEType(name);
                string extension = Path.GetExtension(name).ToLowerInvariant();

                if (System.IO.File.Exists(desPath))
                {
                    System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
                    Response.ClearContent();
                    Response.Clear();
                    Response.ContentType = MIMEtype;
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + repAttachData.NAME + extension);
                    Response.TransmitFile(desPath);
                    Response.Flush();
                    Response.End();
                }
                
            }
            catch (Exception ex)
            {
                
            }
        }

    }
}
