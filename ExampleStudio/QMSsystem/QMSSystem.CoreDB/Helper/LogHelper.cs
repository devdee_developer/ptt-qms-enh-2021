﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.IO;

namespace QMSSystem.CoreDB.Helper
{
    public class LogHelper 
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void WriteErrorLog(string err)
        {
            try
            {
                logger.Error(err);
            }
            catch
            {
            }
        }
        public static void Log(string info)
        {
            try
            {
                logger.Trace(info);
            }
            catch
            {
            }
        }
    }

    public static class LogWriter
    {
        public static string szFolderName = @"LogService";

        private static void CheckFolderExit()
        {
            string logsDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + szFolderName;
            //Path.Combine(Environment.CurrentDirectory, szFolderNameCheck);
            bool exists = System.IO.Directory.Exists(logsDirectory);

            if (!exists)
                System.IO.Directory.CreateDirectory(logsDirectory);
        }

        public static void Log(Exception ex)
        {
            try
            {
                CheckFolderExit();
                string logsDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + szFolderName;
                string logFile = logsDirectory + "\\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "-error_log.txt";
                using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(logFile, true))
                {
                    file.WriteLine(DateTime.Now.ToString() + " : " + ex.Message);
                    file.WriteLine(DateTime.Now.ToString() + " : " + ex.StackTrace);
                    if (ex.InnerException != null)
                    {
                        file.WriteLine(DateTime.Now.ToString() + " : " + ex.InnerException.Message);
                    }
                }
            }
            catch (Exception e)
            {

            }
        }


        public static void WriteErrorLog(string ex)
        {
            try
            {
                CheckFolderExit();
                string logsDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + szFolderName;
                string logFile = logsDirectory + "\\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "-Opt_log.txt";
                using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(logFile, true))
                {
                    file.WriteLine(DateTime.Now.ToString() + " : " + ex);
                }
            }
            catch (Exception e)
            {

            }
        }


        public static void WriteCSVLog(string ex)
        {
            try
            {
                CheckFolderExit();
                string logsDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + szFolderName;
                string logFile = logsDirectory + "\\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "-csv_data.txt";
                using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(logFile, true))
                {
                    file.WriteLine(DateTime.Now.ToString() + " : " + ex);
                }
            }
            catch (Exception e)
            {

            }
        }
    }

   
}
