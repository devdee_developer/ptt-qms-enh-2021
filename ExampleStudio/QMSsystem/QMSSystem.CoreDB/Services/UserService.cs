﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QMSSystem.Model;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Data;
using System.IO;
using System.Drawing;

namespace QMSSystem.CoreDB.Services
{
    public class ModelDomainUser
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userLogonName { get; set; }
        public string employeeID { get; set; }
        public string emailAddress { get; set; }
        public string telephone { get; set; }
        public string address { get; set; }
        public string password { get; set; }
    }

    public class UserService : BaseService
    {
        
        // private AuthorizationData getAuthorizationData(int code, bool access)
        //{
        //    AuthorizationData item = new AuthorizationData();

        //    try
        //    {
        //        item.code = code;
        //        item.access = access;
        //    }
        //    catch (FaultException ex)
        //    {
        //        m_error = ParseErrorMessage(ex);
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return item;
        //} 

        //List<AuthorizationData> listData = new List<AuthorizationData>();

        //listData.Add(getAuthorizationData((int)SectionEnum.CLIENT, m_sessionModel.auth.ManageClient));

        

        
        public string domainName { get; set; }  
        public string domainUser { get; set; }  
        public string domainPassword { get; set; } 
        public string domainPath { get; set; }
        public string _msgError;

        public UserService(string userName) : base(userName) { }

        public UserService()
        { 
            this._msgError = "";
            
        } 

        public UserService(string domainName, string domainUser, string domainPassword)
        {
            this.domainName = domainName;
            this.domainUser = domainUser;
            this.domainPassword = domainPassword;
            this.domainPath = "LDAP://" + domainName;
            this._msgError = "";
        }

        public string getLastError()
        {
            return this._msgError;
        }
         
        public List<LIST_AUTH_DATA> convertToListAuth(List<AuthenAccess> listData)
        {
            List<LIST_AUTH_DATA> listResult = new List<LIST_AUTH_DATA>();

            try
            {
                if (listData != null && listData.Count() > 0)
                {
                    foreach (AuthenAccess dt in listData)
                    {
                        LIST_AUTH_DATA tempData = new LIST_AUTH_DATA();

                        tempData.E_NAME = dt.name;
                        tempData.access = dt.access;

                        listResult.Add(tempData);
                    }
                }

            }
            catch (Exception ex)
            {
                _msgError = ex.Message; 
            }

            return listResult;
        }

        private bool getPevileageStatus(int PvlCode, List<AuthenAccess> listPvl)
        {
            bool bResult = false;

            foreach (AuthenAccess dt in listPvl)
            {
                if (PvlCode == dt.code)
                {
                    bResult = dt.access;
                    break;
                }
            }

            return bResult;
        }

        public PrivilegeMode getPrivilegeModeByListAuthen(List<AuthenAccess> listAccess )
        {
            PrivilegeMode createData = new PrivilegeMode();

            try
            { 
                    //DASHBOARD 
                createData.D_Home = getPevileageStatus((int)PrivilegeModeEnum.D_Home, listAccess);

                //DASHBOARD 
                createData.D_Dashboard = getPevileageStatus((int)PrivilegeModeEnum.D_Dashboard, listAccess);

                //Off Control & spec
                createData. D_OffControl  = getPevileageStatus((int)PrivilegeModeEnum.D_OffControl, listAccess);
                createData.D_OffSpec = getPevileageStatus((int)PrivilegeModeEnum.D_OffSpec, listAccess);

                //Trend Data
                createData.D_TrendHighlight = getPevileageStatus((int)PrivilegeModeEnum.D_TrendHighlight, listAccess);
                createData.D_TrendGAS = getPevileageStatus((int)PrivilegeModeEnum.D_TrendGAS, listAccess);
                createData.D_TrendUtility = getPevileageStatus((int)PrivilegeModeEnum.D_TrendUtility, listAccess);
                createData.D_TrendProduct = getPevileageStatus((int)PrivilegeModeEnum.D_TrendProduct, listAccess);
                createData.D_TrendEmission = getPevileageStatus((int)PrivilegeModeEnum.D_TrendEmission, listAccess);
                createData.D_ProductCSC = getPevileageStatus((int)PrivilegeModeEnum.D_ProductCSC, listAccess);
                createData.D_TrendWasteWater = getPevileageStatus((int)PrivilegeModeEnum.D_TrendWasteWater, listAccess);
                createData.D_TrendHotOilFlashPoint = getPevileageStatus((int)PrivilegeModeEnum.D_TrendHotOilFlashPoint, listAccess);
                createData.D_TrendHotOilPhysical = getPevileageStatus((int)PrivilegeModeEnum.D_TrendHotOilPhysical, listAccess);
                createData.D_TrendSulfurInGasWeekly = getPevileageStatus((int)PrivilegeModeEnum.D_TrendSulfurInGasWeekly, listAccess);
                createData.D_TrendSulfurInGasMonthly = getPevileageStatus((int)PrivilegeModeEnum.D_TrendSulfurInGasMonthly, listAccess);
                createData.D_TrendHgInPLMonthly = getPevileageStatus((int)PrivilegeModeEnum.D_TrendHgInPLMonthly, listAccess);
                createData.D_TrendHgInPL = getPevileageStatus((int)PrivilegeModeEnum.D_TrendHgInPL, listAccess);
                createData.D_TrendHgOutletMRU = getPevileageStatus((int)PrivilegeModeEnum.D_TrendHgOutletMRU, listAccess);
                createData.D_TrendTwoHundredThousandPond = getPevileageStatus((int)PrivilegeModeEnum.D_TrendTwoHundredThousandPond, listAccess);
                createData.D_TrendHgStab = getPevileageStatus((int)PrivilegeModeEnum.D_TrendHgStab, listAccess);
                createData.D_TrendAcidOffGas = getPevileageStatus((int)PrivilegeModeEnum.D_TrendAcidOffGas, listAccess);
                createData.D_TrenddObservePondsAndOilyWaters = getPevileageStatus((int)PrivilegeModeEnum.D_TrenddObservePondsAndOilyWaters, listAccess);
                createData.D_TrenddObservePonds = getPevileageStatus((int)PrivilegeModeEnum.D_TrenddObservePonds, listAccess);
                createData.D_TrenddOilyWaters = getPevileageStatus((int)PrivilegeModeEnum.D_TrenddOilyWaters, listAccess);
                createData.D_TrendLabControlChart = getPevileageStatus((int)PrivilegeModeEnum.D_TrendLabControlChart, listAccess);

                //Master Data
                createData.M_PlantAndProduct = getPevileageStatus((int)PrivilegeModeEnum.M_PlantAndProduct, listAccess);
                createData.M_ReduceFeed = getPevileageStatus((int)PrivilegeModeEnum.M_ReduceFeed, listAccess);
                createData.M_DownTime = getPevileageStatus((int)PrivilegeModeEnum.M_DownTime, listAccess);
                createData.M_CorrectData = getPevileageStatus((int)PrivilegeModeEnum.M_CorrectData, listAccess);
                createData.M_RootCause = getPevileageStatus((int)PrivilegeModeEnum.M_RootCause, listAccess);
                createData.M_KPI = getPevileageStatus((int)PrivilegeModeEnum.M_KPI, listAccess);
                createData.M_IntervalSampling = getPevileageStatus((int)PrivilegeModeEnum.M_IntervalSampling, listAccess);
                createData.M_SigmaLevel = getPevileageStatus((int)PrivilegeModeEnum.M_SigmaLevel, listAccess);
                createData.M_Shift = getPevileageStatus((int)PrivilegeModeEnum.M_Shift, listAccess);
                createData.M_ControlGas = getPevileageStatus((int)PrivilegeModeEnum.M_ControlGas, listAccess);
                createData.M_SpecGas = getPevileageStatus((int)PrivilegeModeEnum.M_SpecGas, listAccess);
                createData.M_ControlUtility = getPevileageStatus((int)PrivilegeModeEnum.M_ControlUtility, listAccess);
                createData.M_ControlEmission = getPevileageStatus((int)PrivilegeModeEnum.M_ControlEmission, listAccess);
                createData.M_ControlProdcut = getPevileageStatus((int)PrivilegeModeEnum.M_ControlProdcut, listAccess);
                createData.M_ControlClarifiedSystem = getPevileageStatus((int)PrivilegeModeEnum.M_ControlClarifiedSystem, listAccess);
                createData.M_ControlWatseWater = getPevileageStatus((int)PrivilegeModeEnum.M_ControlWatseWater, listAccess);
                createData.M_ControlObservePonds = getPevileageStatus((int)PrivilegeModeEnum.M_ControlObservePonds, listAccess);
                createData.M_ControlOilyWaters = getPevileageStatus((int)PrivilegeModeEnum.M_ControlOilyWaters, listAccess);
                createData.M_ControlHotOilFlashPoint = getPevileageStatus((int)PrivilegeModeEnum.M_ControlHotOilFlashPoint, listAccess);
                createData.M_ControlHotOilPhysical = getPevileageStatus((int)PrivilegeModeEnum.M_ControlHotOilPhysical, listAccess);
                createData.M_ControlSulfurInGasWeekly = getPevileageStatus((int)PrivilegeModeEnum.M_ControlSulfurInGasWeekly, listAccess);
                createData.M_ControlSulfurInGasMonthly = getPevileageStatus((int)PrivilegeModeEnum.M_ControlSulfurInGasMonthly, listAccess);
                createData.M_ControlHgInPLMonthly = getPevileageStatus((int)PrivilegeModeEnum.M_ControlHgInPLMonthly, listAccess);
                createData.M_ControlHgInPL = getPevileageStatus((int)PrivilegeModeEnum.M_ControlHgInPL, listAccess);
                createData.M_ControlHgOutletMRU = getPevileageStatus((int)PrivilegeModeEnum.M_ControlHgOutletMRU, listAccess);
                createData.M_ControlTwoHundredThousandPond = getPevileageStatus((int)PrivilegeModeEnum.M_ControlTwoHundredThousandPond, listAccess);
                createData.M_ControlHgStab = getPevileageStatus((int)PrivilegeModeEnum.M_ControlHgStab, listAccess);
                createData.M_ControlAcidOffGas = getPevileageStatus((int)PrivilegeModeEnum.M_ControlAcidOffGas, listAccess);
                createData.M_TemplateGas = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateGas, listAccess);
                createData.M_TemplateUtility = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateUtility, listAccess);
                createData.M_TemplateEmission = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateEmission, listAccess);
                createData.M_TemplateClarifiedSystem = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateClarifiedSystem, listAccess);
                createData.M_TemplateWatseWater = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateWatseWater, listAccess);
                createData.M_TemplateObservePonds = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateObservePonds, listAccess);
                createData.M_TemplateOilyWaters = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateOilyWaters, listAccess);
                createData.M_TemplateHotOilFlashPoint = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateHotOilFlashPoint, listAccess);
                createData.M_TemplateHotOilPhysical = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateHotOilPhysical, listAccess);
                createData.M_TemplateSulfurInGasWeekly = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateSulfurInGasWeekly, listAccess);
                createData.M_TemplateSulfurInGasMonthly = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateSulfurInGasMonthly, listAccess);
                createData.M_TemplateHgInPLMonthly = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateHgInPLMonthly, listAccess);
                createData.M_TemplateHgInPL = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateHgInPL, listAccess);
                createData.M_TemplateHgOutletMRU = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateHgOutletMRU, listAccess);
                createData.M_TemplateTwoHundredThousandPond = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateTwoHundredThousandPond, listAccess);
                createData.M_TemplateHgStab = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateHgStab, listAccess);
                createData.M_TemplateAcidOffGas = getPevileageStatus((int)PrivilegeModeEnum.M_TemplateAcidOffGas, listAccess);
                createData.M_ProductMerge = getPevileageStatus((int)PrivilegeModeEnum.M_ProductMerge, listAccess);
                createData.M_CustomerMerge = getPevileageStatus((int)PrivilegeModeEnum.M_CustomerMerge, listAccess);
                createData.M_Email = getPevileageStatus((int)PrivilegeModeEnum.M_Email, listAccess);
                createData.M_Unit = getPevileageStatus((int)PrivilegeModeEnum.M_Unit, listAccess);

                //Mansge Data
                createData.N_RawDataExport = getPevileageStatus((int)PrivilegeModeEnum.N_RawDataExport, listAccess);
                createData.N_OffControlCal = getPevileageStatus((int)PrivilegeModeEnum.N_OffControlCal, listAccess);
                createData.N_OffSpecCal = getPevileageStatus((int)PrivilegeModeEnum.N_OffSpecCal, listAccess);
                createData.N_TurnAround = getPevileageStatus((int)PrivilegeModeEnum.N_TurnAround, listAccess);
                createData.N_DownTime = getPevileageStatus((int)PrivilegeModeEnum.N_DownTime, listAccess);
                createData.N_ChangeGrade = getPevileageStatus((int)PrivilegeModeEnum.N_ChangeGrade, listAccess);
                createData.N_CrossVolume = getPevileageStatus((int)PrivilegeModeEnum.N_CrossVolume, listAccess);
                createData.N_Abnormal = getPevileageStatus((int)PrivilegeModeEnum.N_Abnormal, listAccess);
                createData.N_ReduceFeed = getPevileageStatus((int)PrivilegeModeEnum.N_ReduceFeed, listAccess);
                createData.N_Exception = getPevileageStatus((int)PrivilegeModeEnum.N_Exception, listAccess);
                createData.N_TemplateGas = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateGas, listAccess);
                createData.N_TemplateUtility = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateUtility, listAccess);
                createData.N_TemplateEmission = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateEmission, listAccess);
                createData.N_TemplateCOA = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateCOA, listAccess);
                createData.N_ImpExpTemplate = getPevileageStatus((int)PrivilegeModeEnum.N_ImpExpTemplate, listAccess);
                createData.N_TemplateClarifiedSystem = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateClarifiedSystem, listAccess);
                createData.N_TemplateWasteWater = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateWasteWater, listAccess);
                createData.N_TemplateObservePonds = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateObservePonds, listAccess);
                createData.N_TemplateOilyWaters = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateOilyWaters, listAccess);
                createData.N_TemplateHotOilFlashPoint = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateHotOilFlashPoint, listAccess);
                createData.N_TemplateHotOilPhysical = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateHotOilPhysical, listAccess);
                createData.N_TemplateSulfurInGasWeekly = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateSulfurInGasWeekly, listAccess);
                createData.N_TemplateSulfurInGasMonthly = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateSulfurInGasMonthly, listAccess);
                createData.N_TemplateHgInPLMonthly = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateHgInPLMonthly, listAccess);
                createData.N_TemplateHgInPL = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateHgInPL, listAccess);
                createData.N_TemplateHgOutletMRU = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateHgOutletMRU, listAccess);
                createData.N_TemplateTwoHundredThousandPond = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateTwoHundredThousandPond, listAccess);
                createData.N_TemplateHgStab = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateHgStab, listAccess);
                createData.N_TemplateAcidOffGas = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateAcidOffGas, listAccess);
                createData.N_LabControlChart = getPevileageStatus((int)PrivilegeModeEnum.N_LabControlChart, listAccess);
                createData.N_TemplateCSCProduct = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateCSCProduct, listAccess);
                createData.N_TemplateCOAToDataLake = getPevileageStatus((int)PrivilegeModeEnum.N_TemplateCOAToDataLake, listAccess);
                createData.N_DataLake = getPevileageStatus((int)PrivilegeModeEnum.N_DataLake, listAccess);
                //createData.N_LoadDataLake = getPevileageStatus((int)PrivilegeModeEnum.N_LoadDataLake, listAccess);
                //createData.N_LoadEventRawDataToDB = getPevileageStatus((int)PrivilegeModeEnum.N_LoadEventRawDataToDB, listAccess);
                //createData.N_LoadQmsRpOffControlToDataLake = getPevileageStatus((int)PrivilegeModeEnum.N_LoadQmsRpOffControlToDataLake, listAccess);
                //createData.N_LoadQmsRpOffControlSummaryToDataLake = getPevileageStatus((int)PrivilegeModeEnum.N_LoadQmsRpOffControlSummaryToDataLake, listAccess);
                //createData.N_LoadQmsRpOffControlStampToDataLake = getPevileageStatus((int)PrivilegeModeEnum.N_LoadQmsRpOffControlStampToDataLake, listAccess);
                //createData.N_LoadAbnormalToDB = getPevileageStatus((int)PrivilegeModeEnum.N_LoadAbnormalToDB, listAccess);
                //createData.N_LoadEventCalToDB = getPevileageStatus((int)PrivilegeModeEnum.N_LoadEventCalToDB, listAccess);
                //createData.N_CheckTotalEXQProduct = getPevileageStatus((int)PrivilegeModeEnum.N_CheckTotalEXQProduct, listAccess);

                //Report
                createData.R_OffControlSum = getPevileageStatus((int)PrivilegeModeEnum.R_OffControlSum, listAccess);
                createData.R_OffSpecSum = getPevileageStatus((int)PrivilegeModeEnum.R_OffSpecSum, listAccess);
                createData.R_CorrectData = getPevileageStatus((int)PrivilegeModeEnum.R_CorrectData, listAccess);
                createData.R_Shift = getPevileageStatus((int)PrivilegeModeEnum.R_Shift, listAccess);
                createData.R_TrendGas = getPevileageStatus((int)PrivilegeModeEnum.R_TrendGas, listAccess);
                createData.R_TrendUtitliy = getPevileageStatus((int)PrivilegeModeEnum.R_TrendUtitliy, listAccess);
                createData.R_TrendProduct = getPevileageStatus((int)PrivilegeModeEnum.R_TrendProduct, listAccess);


                //Setting 
                createData.S_Home = getPevileageStatus((int)PrivilegeModeEnum.S_Home, listAccess);
                createData.S_Abnormal = getPevileageStatus((int)PrivilegeModeEnum.S_Abnormal, listAccess);
                createData.S_ExportData = getPevileageStatus((int)PrivilegeModeEnum.S_ExportData, listAccess);
                createData.S_ReportOff = getPevileageStatus((int)PrivilegeModeEnum.S_ReportOff, listAccess);
                createData.S_ExtraSchedule = getPevileageStatus((int)PrivilegeModeEnum.S_ExtraSchedule, listAccess);
                createData.S_TrendHightlight = getPevileageStatus((int)PrivilegeModeEnum.S_TrendHightlight, listAccess);
                createData.S_Connect = getPevileageStatus((int)PrivilegeModeEnum.S_Connect, listAccess);
                createData.S_Log = getPevileageStatus((int)PrivilegeModeEnum.S_Log, listAccess);
                createData.S_Authen = getPevileageStatus((int)PrivilegeModeEnum.S_Authen, listAccess);
                createData.S_ProductCSCPath = getPevileageStatus((int)PrivilegeModeEnum.S_ProductCSCPath, listAccess);
                createData.S_SortOption = getPevileageStatus((int)PrivilegeModeEnum.S_SortOption, listAccess);

            }
            catch (Exception ex)
            {
                _msgError = ex.Message; 
            }

            return createData;
        }


        private List<AuthenAccess> setPevileageStatusToList(int PvlCode, bool bStatus, List<AuthenAccess> listPvl)
        { 
            foreach (AuthenAccess dt in listPvl)
            {
                if (PvlCode == dt.code)
                {
                    dt.access = bStatus; 
                    break;
                }
            }

            return listPvl;
        }

        public List<AuthenAccess> getListAuthenByPrivilegeMode(List<AuthenAccess> listAccess, PrivilegeMode pMode)
        {
            PrivilegeMode createData = new PrivilegeMode();

            try
            {
                //DASHBOARD 
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_Home, pMode.D_Home, listAccess);

                //DASHBOARD 
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_Dashboard, pMode.D_Dashboard, listAccess);

                //Off Control & spec
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_OffControl, pMode.D_OffControl, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_OffSpec, pMode.D_OffSpec, listAccess);

                //Trend Data
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendHighlight, pMode.D_TrendHighlight, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendGAS, pMode.D_TrendGAS, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendUtility, pMode.D_TrendUtility, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendProduct, pMode.D_TrendProduct, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendEmission, pMode.D_TrendEmission, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_ProductCSC, pMode.D_ProductCSC, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendWasteWater, pMode.D_TrendWasteWater, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendHotOilFlashPoint, pMode.D_TrendHotOilFlashPoint, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendHotOilPhysical, pMode.D_TrendHotOilPhysical, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendSulfurInGasWeekly, pMode.D_TrendSulfurInGasWeekly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendSulfurInGasMonthly, pMode.D_TrendSulfurInGasMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendHgInPLMonthly, pMode.D_TrendHgInPLMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendHgInPL, pMode.D_TrendHgInPL, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendHgOutletMRU, pMode.D_TrendHgOutletMRU, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendTwoHundredThousandPond, pMode.D_TrendTwoHundredThousandPond, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendHgStab, pMode.D_TrendHgStab, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendAcidOffGas, pMode.D_TrendAcidOffGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrenddObservePondsAndOilyWaters, pMode.D_TrenddObservePondsAndOilyWaters, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrenddObservePonds, pMode.D_TrenddObservePonds, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrenddOilyWaters, pMode.D_TrenddOilyWaters, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.D_TrendLabControlChart, pMode.D_TrendLabControlChart, listAccess);

                //Master Data
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_PlantAndProduct, pMode.M_PlantAndProduct, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ReduceFeed, pMode.M_ReduceFeed, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_DownTime, pMode.M_DownTime, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_CorrectData, pMode.M_CorrectData, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_RootCause, pMode.M_RootCause, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_KPI, pMode.M_KPI, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_IntervalSampling, pMode.M_IntervalSampling, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_SigmaLevel, pMode.M_SigmaLevel, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_Shift, pMode.M_Shift, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlGas, pMode.M_ControlGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_SpecGas, pMode.M_SpecGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlEmission, pMode.M_ControlEmission, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlUtility, pMode.M_ControlUtility, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlProdcut, pMode.M_ControlProdcut, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlClarifiedSystem, pMode.M_ControlClarifiedSystem, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlWatseWater, pMode.M_ControlWatseWater, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlObservePonds, pMode.M_ControlObservePonds, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlOilyWaters, pMode.M_ControlOilyWaters, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlHotOilFlashPoint, pMode.M_ControlHotOilFlashPoint, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlHotOilPhysical, pMode.M_ControlHotOilPhysical, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlSulfurInGasWeekly, pMode.M_ControlSulfurInGasWeekly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlSulfurInGasMonthly, pMode.M_ControlSulfurInGasMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlHgInPLMonthly, pMode.M_ControlHgInPLMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlHgInPL, pMode.M_ControlHgInPL, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlHgOutletMRU, pMode.M_ControlHgOutletMRU, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlTwoHundredThousandPond, pMode.M_ControlTwoHundredThousandPond, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlHgStab, pMode.M_ControlHgStab, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ControlAcidOffGas, pMode.M_ControlAcidOffGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateWatseWater, pMode.M_TemplateWatseWater, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateObservePonds, pMode.M_TemplateObservePonds, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateOilyWaters, pMode.M_TemplateOilyWaters, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateGas, pMode.M_TemplateGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateUtility, pMode.M_TemplateUtility, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateEmission, pMode.M_TemplateEmission, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateClarifiedSystem, pMode.M_TemplateClarifiedSystem, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateHotOilFlashPoint, pMode.M_TemplateHotOilFlashPoint, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateHotOilPhysical, pMode.M_TemplateHotOilPhysical, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateSulfurInGasWeekly, pMode.M_TemplateSulfurInGasWeekly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateSulfurInGasMonthly, pMode.M_TemplateSulfurInGasMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateHgInPLMonthly, pMode.M_TemplateHgInPLMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateHgInPL, pMode.M_TemplateHgInPL, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateHgOutletMRU, pMode.M_TemplateHgOutletMRU, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateTwoHundredThousandPond, pMode.M_TemplateTwoHundredThousandPond, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateHgStab, pMode.M_TemplateHgStab, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_TemplateAcidOffGas, pMode.M_TemplateAcidOffGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_ProductMerge, pMode.M_ProductMerge, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_CustomerMerge, pMode.M_CustomerMerge, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_Email, pMode.M_Email, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.M_Unit, pMode.M_Unit, listAccess);

                //Mansge Data
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_RawDataExport, pMode.N_RawDataExport, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_OffControlCal, pMode.N_OffControlCal, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_OffSpecCal, pMode.N_OffSpecCal, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TurnAround, pMode.N_TurnAround, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_DownTime, pMode.N_DownTime, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_ChangeGrade, pMode.N_ChangeGrade, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_CrossVolume, pMode.N_CrossVolume, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_Abnormal, pMode.N_Abnormal, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_ReduceFeed, pMode.N_ReduceFeed, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_Exception, pMode.N_Exception, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateGas, pMode.N_TemplateGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateUtility, pMode.N_TemplateUtility, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateEmission, pMode.N_TemplateEmission, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateCOA, pMode.N_TemplateCOA, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_ImpExpTemplate, pMode.N_ImpExpTemplate, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateClarifiedSystem, pMode.N_TemplateClarifiedSystem, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateWasteWater, pMode.N_TemplateWasteWater, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateObservePonds, pMode.N_TemplateObservePonds, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateOilyWaters, pMode.N_TemplateOilyWaters, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateHotOilFlashPoint, pMode.N_TemplateHotOilFlashPoint, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateHotOilPhysical, pMode.N_TemplateHotOilPhysical, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateSulfurInGasWeekly, pMode.N_TemplateSulfurInGasWeekly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateSulfurInGasMonthly, pMode.N_TemplateSulfurInGasMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateHgInPLMonthly, pMode.N_TemplateHgInPLMonthly, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateHgInPL, pMode.N_TemplateHgInPL, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateHgOutletMRU, pMode.N_TemplateHgOutletMRU, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateTwoHundredThousandPond, pMode.N_TemplateTwoHundredThousandPond, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateHgStab, pMode.N_TemplateHgStab, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateAcidOffGas, pMode.N_TemplateAcidOffGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LabControlChart, pMode.N_LabControlChart, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateCSCProduct, pMode.N_TemplateCSCProduct, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_TemplateCOAToDataLake, pMode.N_TemplateCOAToDataLake, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_DataLake, pMode.N_DataLake, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LoadDataLake, pMode.N_LoadDataLake, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LoadEventRawDataToDB, pMode.N_LoadEventRawDataToDB, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LoadQmsRpOffControlToDataLake, pMode.N_LoadQmsRpOffControlToDataLake, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LoadQmsRpOffControlSummaryToDataLake, pMode.N_LoadQmsRpOffControlSummaryToDataLake, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LoadQmsRpOffControlStampToDataLake, pMode.N_LoadQmsRpOffControlStampToDataLake, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LoadAbnormalToDB, pMode.N_LoadAbnormalToDB, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_LoadEventCalToDB, pMode.N_LoadEventCalToDB, listAccess);
                //listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.N_CheckTotalEXQProduct, pMode.N_CheckTotalEXQProduct, listAccess);

                //Report
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.R_OffControlSum, pMode.R_OffControlSum, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.R_OffSpecSum, pMode.R_OffSpecSum, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.R_CorrectData, pMode.R_CorrectData, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.R_Shift, pMode.R_Shift, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.R_TrendGas, pMode.R_TrendGas, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.R_TrendUtitliy, pMode.R_TrendUtitliy, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.R_TrendProduct, pMode.R_TrendProduct, listAccess);


                //Setting 
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_Home, pMode.S_Home, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_Abnormal, pMode.S_Abnormal, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_ExportData, pMode.S_ExportData, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_ReportOff, pMode.S_ReportOff, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_ExtraSchedule, pMode.S_ExtraSchedule, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_TrendHightlight, pMode.S_TrendHightlight, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_Connect, pMode.S_Connect, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_Log, pMode.S_Log, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_Authen, pMode.S_Authen, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_ProductCSCPath, pMode.S_ProductCSCPath, listAccess);
                listAccess = setPevileageStatusToList((int)PrivilegeModeEnum.S_SortOption, pMode.S_SortOption, listAccess);
            }
            catch (Exception ex)
            {
                _msgError = ex.Message;
            }

            return listAccess;
        }

        public List<AuthenAccess> getDefaultListAuthenAccess(bool status)
        {
            List<AuthenAccess> listResult = new List<AuthenAccess>();

            try
            { 
                listResult = Enum.GetValues(typeof(PrivilegeModeEnum))
                                            .Cast<PrivilegeModeEnum>()
                                            .Select(
                                                        v => new AuthenAccess()
                                                        {
                                                            name = v.ToString(),
                                                            code = (int)v,
                                                            access = status
                                                        }
                                                    ).ToList();
                if (false == status)
                {
                    foreach (AuthenAccess dt in listResult)
                    {

                        if (dt.code == (int)PrivilegeModeEnum.D_Home
                            || dt.code == (int)PrivilegeModeEnum.D_OffControl
                            || dt.code == (int)PrivilegeModeEnum.D_OffSpec
                            || dt.code == (int)PrivilegeModeEnum.D_TrendHighlight
                            || dt.code == (int)PrivilegeModeEnum.D_TrendGAS
                            || dt.code == (int)PrivilegeModeEnum.D_TrendUtility
                            || dt.code == (int)PrivilegeModeEnum.D_TrendProduct
                            || dt.code == (int)PrivilegeModeEnum.D_TrendEmission
                            || dt.code == (int)PrivilegeModeEnum.D_ProductCSC
                            )
                        {
                            dt.access = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _msgError = ex.Message; 
            }
             

            return listResult;
        }
         

        public DirectoryEntry GetDirectoryEntry(string userName, string password)
        {

            DirectoryEntry direct_entry = new DirectoryEntry("LDAP://LDAP.ptt.corp", userName, password);//กำหนด LDAP Server , username,password
            //DirectoryEntry direct_entry = new DirectoryEntry("LDAP://PTT", userName, password);//กำหนด LDAP Server , username,password            
            //DirectoryEntry direct_entry = new DirectoryEntry("LDAP://hq-addc1.pttep.com", userName, password);//กำหนด LDAP Server , username,password
            direct_entry.AuthenticationType = AuthenticationTypes.Secure;

            this.domainUser = userName;
            this.domainPassword = password;
            this.domainName = "LDAP.ptt.corp";
            this.domainPath = "LDAP://LDAP.ptt.corp";

            return direct_entry;
        }

        public DirectoryEntry GetDirectoryEntryEx(string userName, string password)
        {
            //DirectoryEntry direct_entry = new DirectoryEntry("LDAP://PTT", userName, password);//กำหนด LDAP Server , username,password
            DirectoryEntry direct_entry = new DirectoryEntry("LDAP://LDAP.ptt.corp", userName, password);//กำหนด LDAP Server , username,password
            //DirectoryEntry direct_entry = new DirectoryEntry("LDAP://hq-addc1.pttep.com", userName, password);//กำหนด LDAP Server , username,password
            direct_entry.AuthenticationType = AuthenticationTypes.Secure;
            return direct_entry;
        }

       

        #region login RRO
//        [WebMethod]
//        public XmlDocument Get_login(string username, string password, string LoginType)
//        {
//            XmlDataDocument xml = new XmlDataDocument();
//            System.Text.StringBuilder sb = new System.Text.StringBuilder();
//            System.IO.StringWriter sw = new System.IO.StringWriter(sb);

//            if (LoginType == "1") //แก้ไข case นี้ ให้เท่ากับ 2 เพื่อ test บน production test เสร็จให้แก้กลับเป็น 1
//            {
//                #region Login Form by Username and Password

//                //=== chetck super admin ===========
//                DataTable dt = objExecuteDB.VerifyLogin(int.Parse("3"), username, password);
//                if (dt != null && dt.Rows.Count > 0)
//                {

//                    //=== รอการดึงข้อมูลจาก LDAP โดยตรงเลย (Verify Username & Password ในนี้เลย) ===========
//                    if (IsCheckUserExist(username, password)) //=== ถ้า Verify username & password ผ่าน
//                    {
//                        string logonName_LDAP = username;
//                        //==== check  have data in mas employee data ======= //                       
//                        DataTable dtEmployee = objEmployee.GetEmployee_Master_LogOnName(logonName_LDAP);
//                        if (dtEmployee != null && dtEmployee.Rows.Count > 0) // กรณีทีพอข้อมูล จาก EX หรือ IN
//                        {
//                            //=== insert/update data to mas_employee and mas_user ===========================
//                            objExecuteDB.Insert_Update_EmployeeMaster(dtEmployee, "", "", "", "");

//                            dtEmployee.WriteXml(sw);
//                            xml.LoadXml(sw.ToString());
//                            return xml;
//                        }
//                        else
//                        {
//                            xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่สามารถloginได้</EmpCode></Table></NewDataSet>");
//                            return xml;
//                        }
//                    }
//                    else
//                    {
//                        //==== กรณีที่ Verify ใน LDAP ไม่ผ่าน(ต้องมาเช็คใน Mas_User ว่าเขาเป็น พวก contractor ที่ key เข้าไปเอง=======
//                        DataTable dtEmployee_User = objExecute.ExecucteSql("select * from V_Mas_User where UserName='" + username + "' and [Password]='" + password + "' and IsActived=1");
//                        if (dtEmployee_User != null && dtEmployee_User.Rows.Count > 0)
//                        {
//                            string empCode_ = dtEmployee_User.Rows[0]["EmpCode"].ToString();

//                            DataTable dtEmployee = objEmployee.GetEmployee_Master(string.Format("EmpCode='{0}'", empCode_), "IN");
//                            if (dtEmployee != null && dtEmployee.Rows.Count > 0) // กรณีทีพอข้อมูล จาก EX หรือ IN
//                            {
//                                //=== insert/update data to mas_employee and mas_user ===========================
//                                objExecuteDB.Insert_Update_EmployeeMaster(dtEmployee, "", "", "", "");

//                                dtEmployee.WriteXml(sw);
//                                xml.LoadXml(sw.ToString());
//                                return xml;
//                            }
//                            else
//                            {
//                                xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่สามารถloginได้</EmpCode></Table></NewDataSet>");
//                                return xml;
//                            }
//                        }
//                        else
//                        {
//                            xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่สามารถloginได้</EmpCode></Table></NewDataSet>");
//                            return xml;
//                        }
//                    }

//                }
//                else
//                {
//                    xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่สามารถloginได้</EmpCode></Table></NewDataSet>");
//                    return xml;
//                }
//                #endregion

//            }
//            else if (LoginType == "2") //แก้ไข case นี้ ให้เท่ากับ 1 เพื่อ test บน production test เสร็จให้แก้กลับเป็น 2
//            {
//                //==== Fix Login by Emplyee Code=======//
//                string empCodeFix = username;
//                string passVerfiy = objExecuteDB.ExecuteScalar(string.Format("select ConfigValue from Mas_SystemConfig where configCode='C004' "));
//                if (empCodeFix != "" && password == passVerfiy && passVerfiy != "")
//                {
//                    #region Verfiy empCode & Password pass
//                    //=== check employee ในระบบข้างนอก ==========
//                    DataTable dtEmpMaster = objEmployee.GetEmployee_Master(string.Format("EmpCode='{0}'", empCodeFix), "EX");
//                    if (dtEmpMaster != null && dtEmpMaster.Rows.Count > 0)
//                    {
//                        //=== insert/update data to mas_employee and mas_user ===========================
//                        objExecuteDB.Insert_Update_EmployeeMaster(dtEmpMaster, "", "", "", "");

//                        dtEmpMaster.WriteXml(sw);
//                        xml.LoadXml(sw.ToString());
//                        return xml;
//                    }
//                    else
//                    {

//                        DataTable dtEmployee_User = objExecute.ExecucteSql("select * from V_Mas_User where UserName='" + empCodeFix + "' and IsActived=1");
//                        if (dtEmployee_User != null && dtEmployee_User.Rows.Count > 0)
//                        {
//                            string empCode_ = dtEmployee_User.Rows[0]["EmpCode"].ToString();

//                            DataTable dtEmployee = objEmployee.GetEmployee_Master(string.Format("EmpCode='{0}'", empCode_), "IN");
//                            if (dtEmployee != null && dtEmployee.Rows.Count > 0) // กรณีทีพอข้อมูล จาก EX หรือ IN
//                            {
//                                //=== insert/update data to mas_employee and mas_user ===========================
//                                objExecuteDB.Insert_Update_EmployeeMaster(dtEmpMaster, "", "", "", "");


//                                dtEmployee.WriteXml(sw);
//                                xml.LoadXml(sw.ToString());
//                                return xml;
//                            }
//                            else
//                            {
//                                xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่สามารถloginได้</EmpCode></Table></NewDataSet>");
//                                return xml;
//                            }
//                        }
//                        else
//                        {
//                            xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่สามารถloginได้</EmpCode></Table></NewDataSet>");
//                            return xml;
//                        }


//                    }
//                    #endregion
//                }
//                else
//                {
//                    xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่สามารถloginได้</EmpCode></Table></NewDataSet>");
//                    return xml;
//                }

//            }
//            else
//            {
//                xml.LoadXml("<NewDataSet><Table><EmpCode>ไม่ได้เลือก Type Login </EmpCode></Table></NewDataSet>");
//                return xml;
//            }

//        }

//        public DataTable GetEmployee_Master_LogOnName(string logOnName)
//        {
//            string expression = "";
//            if (EmployeeType == "PTT")
//            {
//                expression = string.Format("CODE='{0}'", logOnName);
//            }
//            else
//            {
//                expression = string.Format("LOGON_NAME='{0}'", logOnName);
//            }

//            DataTable dtResult = GetEmployee_Master(expression, "EX");
//            if (dtResult != null && dtResult.Rows.Count > 0)
//            {
//                return dtResult;
//            }
//            else
//            {
//                expression = string.Format("UserName='{0}'", logOnName);
//                DataTable dtResult_in = GetEmployee_Master(expression, "IN");
//                return dtResult_in;

//            }

//        }

//        public DataTable GetEmployee_Master(string Expression)
//        {

//            string expressionNew = ReplaceExpression(Expression);
//            /* string sql = string.Format(@" SELECT ROW_NUMBER() OVER(ORDER BY CODE) AS RowNumber, 
//     tb1.*,tb2.t_name,e_name,tb2.posname as posnameFull from personel_info tb1 inner join position tb2 on tb1.POSCODE=tb2.poscode  where    {0} ", expressionNew); */

//            string sql = string.Format(@"  SELECT ROW_NUMBER() OVER(ORDER BY CODE) AS RowNumber, 
//tb1.*,tb2.t_name,e_name,tb2.posname as posnameFull_backup,tb3.unitabbr as posnameFull 
//from personel_info tb1 
//		inner join position tb2 on tb1.POSCODE=tb2.poscode
//		inner join unit tb3 on tb1.UNITCODE=tb3.unitcode   where    {0} ", expressionNew);
//            DataTable dt = RenameColumnName(ExecucteSql(sql));
//            return dt;


//        }
        #endregion 

        public int isAdminAuthenAccess(string username, string ConnectionStrings, out string imagePath, out string userShowName, out string userDeptName )
        {
            int bResult = (int)AuthenResult.Deny;

            imagePath = ""; //http://hq-web-s13.pttplc.com/directory/photo/490090.jpg
            userShowName = username;
            userDeptName = "";
            if (username == "admin")
            {
                bResult = (int)AuthenResult.Admin;
                userShowName = "Administrator";
            }
            else if (username == "user")
            {
                bResult = (int)AuthenResult.User;
                userShowName = "User";
            }
            else
            {
                //to do check in pis database

                PISSystem pisServices = new PISSystem();
                string codeName  = username;
                if( username == "sp590020"){
                    codeName = "490090";
                }

                PersonalUnit depDummyCode = pisServices.getDummyFromUnitabbr("ผยก.", false); //gest dummy code for user
                PersonalUnit adminDummyCode = pisServices.getDummyFromUnitabbr("คพ.ทผก.", true); //gest dummy code for user

                DataTable dtEmployee = pisServices.GetEmployee_Master_LogOnName(codeName);//("490090");
                if (dtEmployee != null && dtEmployee.Rows.Count > 0) // กรณีทีพอข้อมูล จาก EX หรือ IN
                {
                    //=== insert/update data to mas_employee and mas_user ===========================
                    //objExecuteDB.Insert_Update_EmployeeMaster(dtEmployee, "", "", "", "");
                    //resultPass = true;
                    string record_empCode = dtEmployee.Rows[0]["EmpCode"].ToString();
                    string record_employeeName = dtEmployee.Rows[0]["EmployeeName"].ToString();
                    string record_userName = dtEmployee.Rows[0]["LogOnName"].ToString();
                    string record_email = dtEmployee.Rows[0]["Email"].ToString();
                    string record_unitName = dtEmployee.Rows[0]["UnitName"].ToString();
                    string record_userUnitCode = dtEmployee.Rows[0]["unitcode"].ToString();
                    string record_UnitLongname = dtEmployee.Rows[0]["UnitLongname"].ToString();
                    string record_userLevel = dtEmployee.Rows[0]["level"].ToString();
                    string record_unitdummy = dtEmployee.Rows[0]["unitdummy"].ToString(); //'1-1-12-2-6%'
                    string record_PositionName = dtEmployee.Rows[0]["PositionName"].ToString();


                    if (record_unitdummy.IndexOf(adminDummyCode.DUMMY_RELATIONSHIP) >= 0)//admin
                    {
                        bResult = (int)AuthenResult.Admin; 
                    }
                    else if (record_unitdummy.IndexOf(depDummyCode.DUMMY_RELATIONSHIP) >= 0)//admin
                    {
                        bResult = (int)AuthenResult.User;
                    }
                    else
                    {
                        bResult = (int)AuthenResult.Deny;
                    }

                    userShowName = record_employeeName;
                    userDeptName = record_UnitLongname;
                    imagePath = "http://hq-web-s13.pttplc.com/directory/photo/" + record_empCode + ".jpg";
 
                }
                else
                {
                    //errorInfo.InnerHtml = objExecuteDB.GetMessage("4");  //ไม่สามารถดึงข้อมูลจาก DB work flow ได้       
                    //errorInfo.Visible = true;
                } 
            }


            return bResult;
        }

        public Image GetUserPicture(string userName, string password)
        {
            DirectoryEntry direct_entry = GetDirectoryEntryEx(userName, password);
            DirectorySearcher direct_search = new DirectorySearcher();
            direct_search.SearchRoot = direct_entry;
            direct_search.Filter = "(&(ObjectClass=user)(SAMAccountName=" + userName + "))"; //ตรวจสอบเฉพาะ username ที่ใส่เข้ามาทาง textbox
            //SearchResultCollection result_col = direct_search.FindAll();
            
            try
            {
                SearchResult user = direct_search.FindOne();

                if (user != null)
                {
                    foreach (string propName in user.Properties.PropertyNames)
                    {
                        if (direct_entry.Properties[propName].Value != null)
                        {
                            this.writeErrorLog(propName + " = " + direct_entry.Properties[propName].Value.ToString());
                        }
                        else
                        {
                            this.writeErrorLog(propName + " = NULL");
                        }
                    } 

                    //foreach (ResultPropertyCollection iTest in user.Properties)
                    //{ 
                    //    this.writeErrorLog(iTest.PropertyNames + " : " + iTest.Values);
                    //}
                    
                    this.writeErrorLog("Found user try thumbnailPhoto ");
                    byte[] bb = (byte[])user.Properties["thumbnailPhoto"][0];

                    if (null != bb)
                    {
                        using (MemoryStream s = new MemoryStream(bb))
                        {
                            this.writeErrorLog("thumbnailPhoto have data ");
                            return Bitmap.FromStream(s);
                        }
                    }
                    else
                    {
                        this.writeErrorLog("Found user try jpegPhoto ");
                        byte[] bbs = (byte[])user.Properties["jpegPhoto"][0];
                        using (MemoryStream s = new MemoryStream(bbs))
                        {
                            this.writeErrorLog("jpegPhoto have data ");
                            return Bitmap.FromStream(s);
                        }
                    }


                }
                else
                {
                    this.writeErrorLog("Found out user");
                }
            }
            catch(Exception ex)
            {
                this.writeErrorLog(ex.Message + "thumbnailPhoto");
                return null;
            }
            
             
            return null;

            //DirectoryEntry direct_entry = GetDirectoryEntryEx(userName, password);
            //try
            //{
            //    using (DirectorySearcher dsSearcher = new DirectorySearcher())
            //    {
            //        dsSearcher.SearchRoot = direct_entry;
            //        dsSearcher.Filter = "(&(objectClass=user) (cn=" + userName + "))";
            //        dsSearcher.Filter = "(&(ObjectClass=user)(SAMAccountName=" + userName + "))";
            //        SearchResultCollection result_col = dsSearcher.FindAll();
            //        SearchResult result = dsSearcher.FindOne();

            //        using (DirectoryEntry user = new DirectoryEntry(result.Path))
            //        {
            //            byte[] data = user.Properties["jpegPhoto"].Value as byte[];

            //            if (data != null)
            //            {
            //                using (MemoryStream s = new MemoryStream(data))
            //                {
            //                    return Bitmap.FromStream(s);
            //                }
            //            }

            //            return null;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _msgError = ex.Message;
            //    return null;
            //}
        }

        public bool IsCheckUserExistEx(string username, string password)
        {
            try
            {

                #region for testing

                //if (username == "admin" && password == "adminqms@1234")
                if (username == "admin" && password == "1qaz@WSX")//แก้ไขรหัสผ่าน by Devdee.Pop 23/06/2020
                {
                    return true;
                }
                //else if (username == "user" && password == "userqms@1234")
                else if (username == "user" && password == "3edc@WSX")//แก้ไขรหัสผ่าน by Devdee.Pop 23/06/2020
                {
                    return true;
                }
                //else if (username != "" && password == "simqms@1234")
                //{
                //    return true;
                //}

                #endregion for testing

                DirectoryEntry direct_entry = GetDirectoryEntryEx(username, password);
                DirectorySearcher direct_search = new DirectorySearcher();
                direct_search.SearchRoot = direct_entry;
                direct_search.Filter = "(&(ObjectClass=user)(SAMAccountName=" + username + "))"; //ตรวจสอบเฉพาะ username ที่ใส่เข้ามาทาง textbox
                SearchResultCollection result_col = direct_search.FindAll();


                // Test2Label.Text = result_col.Count.ToString();
                if (result_col.Count > 0)
                {
                    //var testImage = GetUserPicture(username, password); //temporary.. 
                    return true;
                }
                else
                {
                    return false;
                }//ถ้ามี username ชื่อนี้อยู่ในระบบ server จริงก็ให้ ส่งค่า true ไปที่ event ที่ชื่อ Button1_Click
            }
            catch (Exception ex)
            {
                //objLog.writeLog(ex, this.GetType().FullName, new System.Diagnostics.StackFrame().GetMethod().ToString(), objSession.GetUserName(), Request.Path.ToString());
                //errorInfo.InnerHtml = objExecuteDB.GetMessage("25");
                return false;
            }
        }

        public bool IsCheckUserExist(string username, string password)
        {
            bool bResult = false;
            try
            {
                DirectoryEntry direct_entry = GetDirectoryEntry(username, password);
                DirectorySearcher direct_search = new DirectorySearcher();
                direct_search.SearchRoot = direct_entry;
                direct_search.Filter = "(&(ObjectClass=user)(SAMAccountName=" + username + "))"; //ตรวจสอบเฉพาะ username ที่ใส่เข้ามาทาง textbox
                SearchResultCollection result_col = direct_search.FindAll();
                // Test2Label.Text = result_col.Count.ToString();

                if (result_col.Count > 0)
                {
                    PrincipalContext principalContext = null;
                    principalContext = new PrincipalContext(ContextType.Domain, this.domainName, this.domainUser, this.domainPassword);
                    UserPrincipal userPrincipal = new UserPrincipal(principalContext);
                    bResult = true;
                } 
            }
            catch (Exception ex)
            {
                _msgError = ex.Message; 
            }

            return bResult;
        }

        public void credentialCheck(string username, string password)
        {
            try{

                //if (IsCheckUserExist(username, password)) //=== ถ้า Verify username & password ผ่าน
                //    {
                //        string logonName_LDAP = txtUserName.Text;
                //        //==== check  have data in mas employee data ======= //                       
                //        DataTable dtEmployee = objEmployee.GetEmployee_Master_LogOnName(logonName_LDAP);
                //        if (dtEmployee != null && dtEmployee.Rows.Count > 0) // กรณีทีพอข้อมูล จาก EX หรือ IN
                //        {
                //            //=== insert/update data to mas_employee and mas_user ===========================
                //            objExecuteDB.Insert_Update_EmployeeMaster(dtEmployee, "", "", "", "");
                //            resultPass = true;
                //            record_empCode = dtEmployee.Rows[0]["EmpCode"].ToString();
                //            record_employeeName = dtEmployee.Rows[0]["EmployeeName"].ToString();
                //            record_userName = dtEmployee.Rows[0]["LogOnName"].ToString();
                //            record_email = dtEmployee.Rows[0]["Email"].ToString();
                //            record_unitName = dtEmployee.Rows[0]["UnitName"].ToString();
                //            record_userUnitCode = dtEmployee.Rows[0]["unitcode"].ToString();
                //            record_UnitLongname = dtEmployee.Rows[0]["UnitLongname"].ToString();
                //            record_userLevel = dtEmployee.Rows[0]["level"].ToString();
                //            record_unitdummy = dtEmployee.Rows[0]["unitdummy"].ToString();
                //            record_PositionName = dtEmployee.Rows[0]["PositionName"].ToString();

                //            record_userGroup = SetGroupID(dtEmployee.Rows[0]["unitcode"].ToString());

                //            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
                //            conn.Open();
                //            SqlCommand sqlCmd = new SqlCommand("select * from Mas_User_Group where UserGroupID=@p0 and IsActived=1", conn);
                //            sqlCmd.Parameters.Add("@p0", record_userGroup);
                //            SqlDataReader sqlReder = sqlCmd.ExecuteReader();
                //            DataTable dtEmployee_User_Group = new DataTable();
                //            dtEmployee_User_Group.Load(sqlReder);

                //            if (dtEmployee_User_Group != null && dtEmployee_User_Group.Rows.Count > 0)
                //            {
                //                record_userGroup_name = dtEmployee_User_Group.Rows[0]["UserGroupName"].ToString();
                //            }

                //            conn.Close();
                //        }
                //        else
                //        {
                //            errorInfo.InnerHtml = objExecuteDB.GetMessage("4");  //ไม่สามารถดึงข้อมูลจาก DB work flow ได้       
                //            errorInfo.Visible = true;
                //        }
                //    }
                //    else
                //    {
                //        //==== กรณีที่ Verify ใน LDAP ไม่ผ่าน(ต้องมาเช็คใน Mas_User ว่าเขาเป็น พวก contractor ที่ key เข้าไปเอง=======
                //        SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
                //        conn.Open();
                //        SqlCommand sqlCmd = new SqlCommand("select * from V_Mas_User where UserName=@p0 and [Password]=@p1 and IsActived=1", conn);
                //        sqlCmd.Parameters.Add("@p0", logonName);
                //        sqlCmd.Parameters.Add("@p1", objEncry.EncryptData(txtPassword.Text));
                //        SqlDataReader sqlReder = sqlCmd.ExecuteReader();
                //        DataTable dtEmployee_User = new DataTable();
                //        dtEmployee_User.Load(sqlReder);

                //        if (dtEmployee_User != null && dtEmployee_User.Rows.Count > 0)
                //        {
                //            string empCode_ = dtEmployee_User.Rows[0]["EmpCode"].ToString();

                //            DataTable dtEmployee = objEmployee.GetEmployee_Master(string.Format("EmpCode='{0}'", empCode_), "IN");
                //            if (dtEmployee != null && dtEmployee.Rows.Count > 0) // กรณีทีพอข้อมูล จาก EX หรือ IN
                //            {
                //                //=== check password ======                    
                //                resultPass = true;
                //                record_empCode = dtEmployee.Rows[0]["EmpCode"].ToString();
                //                record_employeeName = dtEmployee.Rows[0]["EmployeeName"].ToString();
                //                record_userName = logonName;
                //                record_email = dtEmployee.Rows[0]["Email"].ToString();
                //                record_userUnitCode = dtEmployee_User.Rows[0]["UnitName"].ToString();
                //                record_UnitLongname = dtEmployee_User.Rows[0]["UnitName"].ToString();
                //                record_userLevel = "5";
                //                record_unitdummy = "";
                //                record_PositionName = dtEmployee.Rows[0]["PositionName"].ToString();
                //                record_userGroup_name = dtEmployee_User.Rows[0]["UserGroupName"].ToString();
                //                record_userGroup = objEmployee.GetUserGroup(record_empCode);

                //                //resultPass = true;
                //                //record_empCode = dtEmployee_User.Rows[0]["EmpCode"].ToString();
                //                //record_employeeName = dtEmployee_User.Rows[0]["EmployeeName"].ToString();
                //                //record_userName = dtEmployee_User.Rows[0]["UserName"].ToString();
                //                //record_email = dtEmployee_User.Rows[0]["Email"].ToString();
                //                //record_unitName = dtEmployee_User.Rows[0]["UnitName"].ToString();
                //                //record_userUnitCode = dtEmployee_User.Rows[0]["UnitName"].ToString();
                //                //record_UnitLongname = dtEmployee_User.Rows[0]["UnitName"].ToString();
                //                //record_userLevel = "5";

                //                //record_unitdummy = "";
                //                //record_PositionName = dtEmployee_User.Rows[0]["PositionName"].ToString();
                //                //record_userGroup_name = dtEmployee_User.Rows[0]["UserGroupName"].ToString();
                //                //record_userGroup = dtEmployee_User.Rows[0]["GroupID"].ToString();
                //            }
                //            else
                //            {
                //                _msgError = "ไม่สามารถดึงข้อมูลจาก DB work flow ได้ "; 
                //            }
                //        }
                //        else
                //        {
                //            _msgError = "รายชื่อ Username หรือ Password ไม่ถูกต้อง";
                //        }

                //        conn.Close();
                //    } 

            } catch (Exception ex){
        
            }

        }

        public bool checkLogin(string username, string password)
        {
            bool bResult = false;
            try
            {
                using (var context = new PrincipalContext(ContextType.Domain, this.domainName))
                {
                    bResult = context.ValidateCredentials(username, password);
                }
            }
            catch (Exception ex)
            {
                _msgError = ex.Message;
            }
            return bResult;
        }

        //public List<ShowUserDomainList> listAllUser(long DomainID)
        //{
        //    List<ShowUserDomainList> listResult = new List<ShowUserDomainList>();

        //    using (var context = new PrincipalContext(ContextType.Domain, this.domainName, this.domainUser, this.domainPassword))
        //    {
        //        using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
        //        {
        //            foreach (var result in searcher.FindAll())
        //            {
        //                DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;

        //                ShowUserDomainList temp = new ShowUserDomainList();

        //                temp.DomainID = DomainID;
        //                temp.UserName = de.Properties["samAccountName"].Value.ToString();
        //                temp.EditAble = true;

        //                listResult.Add(temp); 
        //            }
        //        }
        //    }

        //    return listResult;
        //}

        //public void getAllClient()
        //{
        //    //Note : microsoft is the name of my domain for testing purposes.
        //    try
        //    {
        //        DirectoryEntry entry = new DirectoryEntry(this.domainPath, this.domainUser, this.domainPassword);
        //        DirectorySearcher mySearcher = new DirectorySearcher(entry);


        //        mySearcher.Filter = ("(objectClass=computer)");
        //        Console.WriteLine("Listing of computers in the Active Directory");
        //        Console.WriteLine("============================================");
        //        foreach (SearchResult resEnt in mySearcher.FindAll())
        //        {
        //            Console.WriteLine(resEnt.GetDirectoryEntry().Name.ToString());
        //            Console.WriteLine("=========== End of Listing =============");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("\n\n Error : {0} ", ex.Message);
        //    }
        //}

        //public bool DeleteUserPrincipalContext(ModelDomainUser modelUser)
        //{
        //    bool bResult = false;
        //    // Creating the PrincipalContext
        //    PrincipalContext principalContext = null;
        //    try
        //    {
        //        principalContext = new PrincipalContext(ContextType.Domain, this.domainName, this.domainUser, this.domainPassword);//, "DC=kdts2008,DC=local");
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Failed to create PrincipalContext. Exception: " + e);
        //        this._msgError = e.Message;
        //    }

        //    try
        //    {
        //        // Check if user object already exists in the store
        //        UserPrincipal usr = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, modelUser.userLogonName);
        //        if (usr != null)
        //        {
        //            //delete
        //            usr.Delete();

        //            //update
        //            //usr.Enabled = false;
        //            //usr.Save();

        //            return true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        this._msgError = ex.Message;
        //        Console.WriteLine(" Error : " + ex.Message);
        //        return true;
        //    }

        //    return true;
        //}

        //public bool CreateUserPrincipalContext(ModelDomainUser modelUser)
        //{
        //    // Creating the PrincipalContext
        //    PrincipalContext principalContext = null;
        //    try
        //    {
        //        principalContext = new PrincipalContext(ContextType.Domain, this.domainName, this.domainUser, this.domainPassword);//, "DC=kdts2008,DC=local");
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Failed to create PrincipalContext. Exception: " + e);
        //    }

        //    try
        //    {
        //        // Check if user object already exists in the store
        //        UserPrincipal usr = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, modelUser.userLogonName);
        //        if (usr != null)
        //        {

        //            Console.WriteLine(modelUser.userLogonName + " already exists. Please use a different User Logon Name.");

        //            //delete
        //            //usr.Delete();

        //            //update
        //            //usr.Enabled = false;
        //            //usr.Save();

        //            return true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        Console.WriteLine(" Error : " + ex.Message);
        //        return true;
        //    }


        //    // Create the new UserPrincipal object
        //    UserPrincipal userPrincipal = new UserPrincipal(principalContext);

        //    if (modelUser.lastName != null && modelUser.lastName.Length > 0)
        //        userPrincipal.Surname = modelUser.lastName;

        //    if (modelUser.firstName != null && modelUser.firstName.Length > 0)
        //        userPrincipal.GivenName = modelUser.firstName;

        //    if (modelUser.employeeID != null && modelUser.employeeID.Length > 0)
        //        userPrincipal.EmployeeId = modelUser.employeeID;

        //    if (modelUser.emailAddress != null && modelUser.emailAddress.Length > 0)
        //        userPrincipal.EmailAddress = modelUser.emailAddress;

        //    if (modelUser.telephone != null && modelUser.telephone.Length > 0)
        //        userPrincipal.VoiceTelephoneNumber = modelUser.telephone;

        //    if (modelUser.userLogonName != null && modelUser.userLogonName.Length > 0)
        //        userPrincipal.SamAccountName = modelUser.userLogonName;

        //    //pwdOfNewlyCreatedUser = "abcde@@12345!~";
        //    userPrincipal.SetPassword(modelUser.password);

        //    userPrincipal.Enabled = true;
        //    userPrincipal.ExpirePasswordNow();

        //    try
        //    {
        //        userPrincipal.Save();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Exception creating user object. " + e);
        //        return false;
        //    }

        //    /***************************************************************
        //     *   The below code demonstrates on how you can make a smooth 
        //     *   transition to DirectoryEntry from AccountManagement namespace, 
        //     *   for advanced operations.
        //     ***************************************************************/
        //    if (userPrincipal.GetUnderlyingObjectType() == typeof(DirectoryEntry))
        //    {
        //        DirectoryEntry entry = (DirectoryEntry)userPrincipal.GetUnderlyingObject();
        //        if (modelUser.address != null && modelUser.address.Length > 0)
        //            entry.Properties["streetAddress"].Value = modelUser.address;
        //        try
        //        {
        //            entry.CommitChanges();
        //        }
        //        catch (Exception e)
        //        {
        //            Console.WriteLine("Exception modifying address of the user. " + e);
        //            return false;
        //        }
        //    }

        //    return true;
        //} 
    }
}
