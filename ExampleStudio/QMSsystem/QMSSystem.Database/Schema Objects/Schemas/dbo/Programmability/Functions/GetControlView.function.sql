﻿CREATE FUNCTION [dbo].[GetControlView] (
		@MAX_FLAG tinyint ,
		@MIN_FLAG tinyint ,
		@MAX_VALUE decimal ,
		@MIN_VALUE decimal,
		@CONC_FLAG tinyint ,
		@LOADING_FLAG tinyint ,
		@CONC_VALUE decimal ,
		@LOADING_VALUE decimal 
)
RETURNS VARCHAR(250)


