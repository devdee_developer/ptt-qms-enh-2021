﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("QMSLIMSDATA")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("DevDeeThailand")> 
<Assembly: AssemblyProduct("QMSLIMSDATA")> 
<Assembly: AssemblyCopyright("Copyright ©  2017")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("fe5dfb17-d5b2-4640-afa3-b7bd5f9d68bf")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2023.02.28.1")>
<Assembly: AssemblyFileVersion("2023.02.28.1")>
