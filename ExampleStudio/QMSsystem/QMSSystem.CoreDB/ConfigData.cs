﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QMSSystem.Model;

namespace QMSSystem.CoreDB
{
    public class ConfigData
    {
    }

    public partial class QMS_ST_AUTHORIZATION
    {
        public static List<QMS_ST_AUTHORIZATION> GetAll(QMSDBEntities db)
        {
            return db.QMS_ST_AUTHORIZATION.ToList();
        }

        public static QMS_ST_AUTHORIZATION GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_ST_AUTHORIZATION.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_ST_AUTHORIZATION GetByGROUP_AUTHId(QMSDBEntities dataContext, byte id)
        {
            return dataContext.QMS_ST_AUTHORIZATION.Where(m => m.GROUP_AUTH == id).FirstOrDefault();
        } 
    }

    public partial class QMS_ST_EXPORT_PREFIX
    {
        public static List<QMS_ST_EXPORT_PREFIX> GetAll(QMSDBEntities db)
        {
            return db.QMS_ST_EXPORT_PREFIX.ToList();
        }

        public static QMS_ST_EXPORT_PREFIX GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_ST_EXPORT_PREFIX.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_ST_EXPORT_PREFIX GetByPlantId(QMSDBEntities dataContext, long PlantId)
        {
            return dataContext.QMS_ST_EXPORT_PREFIX.Where(m => m.PLANT_ID == PlantId).FirstOrDefault();
        }

        public static QMS_ST_EXPORT_PREFIX GetByPlantIdSkipId(QMSDBEntities dataContext, long id, long PlantId)
        {
            return dataContext.QMS_ST_EXPORT_PREFIX.Where(m => m.PLANT_ID == PlantId && m.ID != id).FirstOrDefault();
        }

        public static List<QMS_ST_EXPORT_PREFIX> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_ST_EXPORT_PREFIX.Where( m => ids.Contains( m.ID) ).ToList();
        }

        public static List<QMS_ST_EXPORT_PREFIX> GetAllBySearch(QMSDBEntities db, ExportPrefixSearchModel searchModel)
        {
            List<QMS_ST_EXPORT_PREFIX> lstResult = new List<QMS_ST_EXPORT_PREFIX>();

            lstResult = db.QMS_ST_EXPORT_PREFIX.ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if ( searchModel.PLANT_ID  > 0)
                {
                    lstResult = lstResult.Where(m => m.PLANT_ID == searchModel.PLANT_ID).ToList();
                }

            }
            return lstResult; 
        }
    }

    public partial class QMS_ST_EXAQUATUM_SCHEDULE
    {
        public static List<QMS_ST_EXAQUATUM_SCHEDULE> GetAll(QMSDBEntities db)
        {
            return db.QMS_ST_EXAQUATUM_SCHEDULE.Where( m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_ST_EXAQUATUM_SCHEDULE> GetAllByStatus(QMSDBEntities db, int status)
        {
            return db.QMS_ST_EXAQUATUM_SCHEDULE.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS == status).ToList();
        }

        public static List<QMS_ST_EXAQUATUM_SCHEDULE> GetAllBySearch(QMSDBEntities db, ExaScheduleSearchModel search)
        {
            List<QMS_ST_EXAQUATUM_SCHEDULE> listResult = new List<QMS_ST_EXAQUATUM_SCHEDULE>();

            listResult = db.QMS_ST_EXAQUATUM_SCHEDULE.Where(m => m.DELETE_FLAG == 0).ToList();

            if (listResult.Count() > 0)
            {
                if (search.PLANT_ID > 0)
                {
                    listResult = listResult.Where(m => m.PLANT_ID == search.PLANT_ID).ToList();
                }

                if (search.DOC_STATUS > 0)
                {
                    listResult = listResult.Where(m => m.DOC_STATUS == search.DOC_STATUS).ToList();
                }

                if (search.START_DATE != null)
                {
                    listResult = listResult.Where(m => m.START_DATE >= search.START_DATE).ToList();
                }

                if (search.END_DATE != null)
                {
                    listResult = listResult.Where(m => m.START_DATE <= search.END_DATE).ToList();
                }

            }

            return listResult;
        }

        public static List<QMS_ST_EXAQUATUM_SCHEDULE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_ST_EXAQUATUM_SCHEDULE.Where(m => ids.Contains(m.ID)).ToList();
        }
        
        public static QMS_ST_EXAQUATUM_SCHEDULE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_ST_EXAQUATUM_SCHEDULE.Where(m => m.ID == id && m.DELETE_FLAG == 0).FirstOrDefault();
        }
    }

    public partial class QMS_ST_FILE_ATTACH
    {
        public static List<QMS_ST_FILE_ATTACH> GetAll(QMSDBEntities db)
        {
            return db.QMS_ST_FILE_ATTACH.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_ST_FILE_ATTACH GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_ST_FILE_ATTACH.Where(m => m.ID == id && m.DELETE_FLAG == 0).FirstOrDefault();
        }
   
        public static List<QMS_ST_FILE_ATTACH> GetActive(QMSDBEntities db)
        {
            return db.QMS_ST_FILE_ATTACH.Where(m => m.DELETE_FLAG == 0 ).ToList(); //linq 1 = Off control 2 = Off Spec
        }

        public static List<QMS_ST_FILE_ATTACH> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_ST_FILE_ATTACH.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_ST_FILE_ATTACH> GetAllBySearch(QMSDBEntities db, FileAttachSearchModel searchModel)
        {
            List<QMS_ST_FILE_ATTACH> lstResult = new List<QMS_ST_FILE_ATTACH>();

            lstResult = db.QMS_ST_FILE_ATTACH.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

            
                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    lstResult = lstResult.Where(m => m.NAME.Contains(searchModel.NAME)).ToList();
                }

                if (null != searchModel.PATH && "" != searchModel.PATH)
                {
                    lstResult = lstResult.Where(m => m.PATH.Contains(searchModel.PATH)).ToList();
                }

                if (null != searchModel.BOARD_TYPE && searchModel.BOARD_TYPE > 0)
                {
                    lstResult = lstResult.Where(m => m.BOARD_TYPE == searchModel.BOARD_TYPE ).ToList();
                }

            }
            return lstResult;
        }
  
    }

    public partial class QMS_ST_SYSTEM_CONFIG
    {
        public static List<QMS_ST_SYSTEM_CONFIG> GetAll(QMSDBEntities db)
        {
            return db.QMS_ST_SYSTEM_CONFIG.ToList();
        }

        public static QMS_ST_SYSTEM_CONFIG GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_ST_SYSTEM_CONFIG.Where(m => m.ID == id).FirstOrDefault();
        }
        
    }

    public partial class QMS_ST_SYSTEM_LOG
    {
        public static List<QMS_ST_SYSTEM_LOG> GetAll(QMSDBEntities db)
        {
            return db.QMS_ST_SYSTEM_LOG.ToList();
        }

        public static List<QMS_ST_SYSTEM_LOG> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_ST_SYSTEM_LOG.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static QMS_ST_SYSTEM_LOG GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_ST_SYSTEM_LOG.Where(m => m.ID == id ).FirstOrDefault();
        }

        public static List<QMS_ST_SYSTEM_LOG> GetAllBySearch(QMSDBEntities db, SystemLogSearchModel searchModel)
        {
            List<QMS_ST_SYSTEM_LOG> lstResult = new List<QMS_ST_SYSTEM_LOG>();

            lstResult = db.QMS_ST_SYSTEM_LOG.ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {


                if (null != searchModel.PROCESS_NAME && "" != searchModel.PROCESS_NAME)
                {
                    lstResult = lstResult.Where(m => m.PROCESS_NAME.Contains(searchModel.PROCESS_NAME)).ToList();
                }

                if (null != searchModel.PROCESS_DESC && "" != searchModel.PROCESS_DESC)
                {
                    lstResult = lstResult.Where(m => m.PROCESS_DESC.Contains(searchModel.PROCESS_DESC)).ToList();
                }

                if (null != searchModel.ERROR_LEVEL && searchModel.ERROR_LEVEL > 0)
                {
                    lstResult = lstResult.Where(m => m.ERROR_LEVEL == searchModel.ERROR_LEVEL).ToList();
                }

            }
            return lstResult;
        }
    }


    public partial class QMS_ST_SYSTEM_USER_STAMP
    {
        public static List<QMS_ST_SYSTEM_USER_STAMP> GetAll(QMSDBEntities db)
        {
            return db.QMS_ST_SYSTEM_USER_STAMP.ToList();
        }

        public static QMS_ST_SYSTEM_USER_STAMP GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_ST_SYSTEM_USER_STAMP.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_ST_SYSTEM_USER_STAMP GetBySystemType(QMSDBEntities dataContext, int SYSTEM_TYPE)
        {
            return dataContext.QMS_ST_SYSTEM_USER_STAMP.Where(m => m.SYSTEM_TYPE == SYSTEM_TYPE).FirstOrDefault();
        }
         
    }


}
