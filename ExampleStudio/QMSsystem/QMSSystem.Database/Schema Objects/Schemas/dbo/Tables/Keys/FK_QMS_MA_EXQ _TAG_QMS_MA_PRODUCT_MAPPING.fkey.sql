﻿ALTER TABLE [dbo].[QMS_MA_EXQ_TAG]  WITH CHECK ADD  CONSTRAINT [FK_QMS_MA_EXQ _TAG_QMS_MA_PRODUCT_MAPPING] FOREIGN KEY([PRODUCT_MAPPING_ID])
REFERENCES [dbo].[QMS_MA_PRODUCT_MAPPING] ([ID])


GO
ALTER TABLE [dbo].[QMS_MA_EXQ_TAG] CHECK CONSTRAINT [FK_QMS_MA_EXQ _TAG_QMS_MA_PRODUCT_MAPPING]

