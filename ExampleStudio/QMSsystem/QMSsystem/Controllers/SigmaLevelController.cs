﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_SigmaLevel }, DataAccess = DataAccessEnum.Authen)]
    public class SigmaLevelController : BaseController
    {
        //
        // GET: /SigmaLevel/
        public ActionResult ListMasterSigmaLevel()//control ListMasterSigmaLevel.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            SigmaLevelSearchModel searchSigmaLevel = new SigmaLevelSearchModel();
            CreateQMS_MA_SIGMA_LEVEL createQMS_MA_SIGMA_LEVEL = new CreateQMS_MA_SIGMA_LEVEL();
            createQMS_MA_SIGMA_LEVEL.ACTIVE_DATE = getDummyDateTime();


            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

            ViewBag.searchSigmaLevel = searchSigmaLevel;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_SIGMA_LEVEL = createQMS_MA_SIGMA_LEVEL;

            return View();
        }

        public ActionResult CreateMasterSigmaLevel(long id = 0)//control CresteMasterSigmaLevel.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_SIGMA_LEVEL createQMS_MA_SIGMA_LEVEL = new CreateQMS_MA_SIGMA_LEVEL();
            string actionName = Resources.Strings.AddNew;

            createQMS_MA_SIGMA_LEVEL.ACTIVE_DATE = getDummyDateTime();

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_SIGMA_LEVEL = masterDataServices.getQMS_MA_SIGMA_LEVELById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

            ViewBag.createQMS_MA_SIGMA_LEVEL = createQMS_MA_SIGMA_LEVEL;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_MA_SIGMA_LEVEL(CreateQMS_MA_SIGMA_LEVEL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_SIGMA_LEVEL(_db, model);

                    if (nResult > 0)
                    {
                        model = masterDataServices.getQMS_MA_SIGMA_LEVELById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_SIGMA_LEVELBySearch(SigmaLevelSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_SIGMA_LEVEL> listData = new List<ViewQMS_MA_SIGMA_LEVEL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_SIGMA_LEVEL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_SIGMA_LEVEL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_SIGMA_LEVELByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}