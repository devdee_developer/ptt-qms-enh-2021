﻿ALTER TABLE [dbo].[QMS_MA_PRODUCT_MAPPING]  WITH CHECK ADD  CONSTRAINT [FK_QMS_MA_PRODUCT_MAPPING_QMS_MA_GRADE] FOREIGN KEY([GRADE_ID])
REFERENCES [dbo].[QMS_MA_GRADE] ([ID])


GO
ALTER TABLE [dbo].[QMS_MA_PRODUCT_MAPPING] CHECK CONSTRAINT [FK_QMS_MA_PRODUCT_MAPPING_QMS_MA_GRADE]

