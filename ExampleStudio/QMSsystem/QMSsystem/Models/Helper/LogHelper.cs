﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;

namespace QMSsystem.Models.Helper
{
    public static class LogHelper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Log(string info)
        {
            try
            {
                logger.Trace(info);
            }
            catch
            {

            }
        }
    }
}