﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_RootCause }, DataAccess = DataAccessEnum.Authen)]
    public class RootCauseController : BaseController
    {
        //
        // GET: /RootCause/
        public ActionResult ListMasterRootCause()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            RootCauseSearchModel searchRootCause = new RootCauseSearchModel();



            ViewBag.listRootCauseType = masterDataServices.getQMS_MA_ROOT_CAUSE_TYPEActiveList(_db);

            ViewBag.searchRootCause = searchRootCause;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public ActionResult CreateMasterRootCause(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            RootCauseTypeSearchModel searchRootCauseType = new RootCauseTypeSearchModel();
            CreateQMS_MA_ROOT_CAUSE createQMS_MA_ROOT_CAUSE = new CreateQMS_MA_ROOT_CAUSE();
            string actionName = Resources.Strings.AddNew;

            
            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_ROOT_CAUSE = masterDataServices.getQMS_MA_ROOT_CAUSEById(_db, id);
            }

            ViewBag.listRootCauseType = masterDataServices.getQMS_MA_ROOT_CAUSE_TYPEActiveList(_db);
                      
            ViewBag.createQMS_MA_ROOT_CAUSE = createQMS_MA_ROOT_CAUSE;
            ViewBag.searchRootCauseType = searchRootCauseType;
            ViewBag.listPageSizeRootCause = this.getPageSizeList();
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult GetActiveRootCauseList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_ROOT_CAUSE> listData = new List<ViewQMS_MA_ROOT_CAUSE>();
                    listData = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateQMS_MA_ROOT_CAUSE(CreateQMS_MA_ROOT_CAUSE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_ROOT_CAUSE(_db, model);

                    if (nResult > 0)
                    {
                        model = masterDataServices.getQMS_MA_ROOT_CAUSEById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_ROOT_CAUSEBySearch(RootCauseSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_ROOT_CAUSE> listData = new List<ViewQMS_MA_ROOT_CAUSE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_ROOT_CAUSE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_ROOT_CAUSE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_ROOT_CAUSEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
            

        public ActionResult ListRootCauseType()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            RootCauseTypeSearchModel searchRootCauseType = new RootCauseTypeSearchModel();
            List<ViewQMS_MA_ROOT_CAUSE_TYPE> listRootCauseType = new List<ViewQMS_MA_ROOT_CAUSE_TYPE>();

            ViewBag.listRootCauseType = masterDataServices.getQMS_MA_ROOT_CAUSE_TYPEActiveList(_db);

            ViewBag.searchRootCauseType = searchRootCauseType;
            ViewBag.listRootCauseType = listRootCauseType;
            ViewBag.listPageSizeRootCauseType = this.getPageSizeList();

            return View();
        }

        public JsonResult CreateQMS_MA_ROOT_CAUSE_TYPE(CreateQMS_MA_ROOT_CAUSE_TYPE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_ROOT_CAUSE_TYPE(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActiveRootCauseTypeList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_ROOT_CAUSE_TYPE> listData = new List<ViewQMS_MA_ROOT_CAUSE_TYPE>();
                    listData = masterDataServices.getQMS_MA_ROOT_CAUSE_TYPEActiveList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_ROOT_CAUSE_TYPE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_ROOT_CAUSE_TYPEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_ROOT_CAUSE_TYPEBySearch(RootCauseTypeSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_ROOT_CAUSE_TYPE> listData = new List<ViewQMS_MA_ROOT_CAUSE_TYPE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_ROOT_CAUSE_TYPE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }
}
