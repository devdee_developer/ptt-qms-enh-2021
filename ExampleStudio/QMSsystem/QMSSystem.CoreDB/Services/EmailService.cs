﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace QMSSystem.CoreDB.Services
{
    public class EmailService
    {

        public EmailService()
        {
            EmailDefault.SmtpServer = ConfigurationManager.AppSettings["SMTP_SERVER"] ?? "e2k10-hubrelay.ptt.corp";
            EmailDefault.SmtpServerPort = (ConfigurationManager.AppSettings["SMTP_SERVER_PORT"] == null)? Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_SERVER_PORT"]): 25;
            EmailDefault.SmtpSecureOption = ConfigurationManager.AppSettings["SMTP_SECURE_OPTION"] ?? "";
            EmailDefault.SmtpUsername = ConfigurationManager.AppSettings["SMTP_SERVER_USERNAME"] ?? "";
            EmailDefault.SmtpPassword = ConfigurationManager.AppSettings["SMTP_SERVER_PASSWORD"] ?? "";
            EmailDefault.SmtpSenderEmail = ConfigurationManager.AppSettings["SMTP_SENDER_EMAIL"] ?? "GSP-LAB-REQUEST@pttplc.com";
            EmailDefault.SmtpFixSendTo = ConfigurationManager.AppSettings["SMTP_FIX_SENDTO"] ?? "";

            EmailDefault.DummyMail = ConfigurationManager.AppSettings["DummyMail"] ?? "monchai@devdeethailand.com;akarapon@devdeethailand.com;";


            var issendmail = ConfigurationManager.AppSettings["IsSendMail"];
            if (!string.IsNullOrEmpty(issendmail))
            {
                if (issendmail.ToUpper() == "TRUE") EmailDefault.IsSendMail = true;
            }

            var IsDummyMail = ConfigurationManager.AppSettings["IsDummyMail"];
            if (!string.IsNullOrEmpty(IsDummyMail))
            {
                if (IsDummyMail.ToUpper() == "TRUE") { EmailDefault.IsDummyMail = true; }
                else { EmailDefault.IsDummyMail = false; }
            }
        } 


        private IEnumerable<string> CleanUpAddress(string[] src)
        {
            var res = new List<string>();
            foreach (var item in src)
            {
                var s = item.Trim().ToLower();
                if (string.IsNullOrEmpty(s)) continue;
                res.Add(s);
            }
            return res.Distinct();
        }

        public void SendSmtpMail(string host, int port, string username, string password, string from, string displayname, string tos, string ccs, string subject, string body, bool ishtml)
        {

            if (EmailDefault.IsDummyMail) //ใช้ mail dummy
            {
                tos = EmailDefault.DummyMail;
                ccs = "";
            }

            var client = new SmtpClient(host, port);
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                client.Credentials = new NetworkCredential(username, password);
            }

            var message = new MailMessage();
            message.From = new MailAddress(from, displayname, System.Text.Encoding.UTF8);
            var toaddrs = CleanUpAddress(tos.Split(';'));
            foreach (var to in toaddrs) message.To.Add(to);

            var ccaddrs = CleanUpAddress(ccs.Split(';'));
            foreach (var cc in ccaddrs) message.CC.Add(cc);

            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Body = body;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            message.Subject = subject;
            message.IsBodyHtml = ishtml;

            client.Send(message);
        }

    }


    public static class EmailDefault
    {
       
        //-- Email
        public static bool IsSendMail = false;
        public static string SmtpServer = "";
        public static int SmtpServerPort = 0;
        public static string SmtpSecureOption = "";
        public static string SmtpUsername = "";
        public static string SmtpPassword = "";
        public static string SmtpSenderEmail = "";
        public static string SmtpFixSendTo = ""; 
        public static bool IsDummyMail = false;
        public static string DummyMail = "monchai@devdeethailand.com;akarapon@devdeethailand.com";
          
    }
}
