﻿Option Explicit On
 

Public Class Form1

    Dim objSession As QUANTUMAUTOMATIONLib.Session
    Dim WithEvents objDataAccess As QUANTUMAUTOMATIONLib.QDataAccess
    'Attributes objDataAccess.VB_VarHelpID = -1
    Dim WithEvents objAEAccess As QUANTUMAUTOMATIONLib.QAEAccess
    'Attribute objAEAccess.VB_VarHelpID = -1
    Dim objBrowser As QUANTUMAUTOMATIONLib.Browse2
    Dim rootTag As QUANTUMAUTOMATIONLib.Browse2

    Dim qdaNew As Integer
    Dim qdaMoreRows As Integer
    Dim qdaEnd As Integer

    Dim qeaNew As Integer
    Dim qeaMoreEvents As Integer
    Dim qeaEnd As Integer

    Dim brDetail As Integer


    ''=======================================================================
    ''Synopsis:      Event Handlers
    ''-----------------------------------------------------------------------
     
    

    '=======================================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to recieve data event for Alarm & Events data read
    '-------------------------------------------------------------------------------------------------------

    Private Sub objAEAccess_OnAE(ByVal EventReturnType As QUANTUMAUTOMATIONLib.qeaEventReturnType, ByVal Events As Object)
        Dim vOPCAEPumpHistID As Object
        Dim sSource As String
        Dim sMessage As String
        Dim dTimeStamp As Date
        Dim lSequenceNo As Long
        Dim lEventCategory As Long
        Dim lSeverity As Long
        Dim sConditionName As String
        Dim lNoOfAttributes As Long
        Dim vEventAttributesArray As Object
        Dim vEventAttribute As Object
        Dim Count As Long
        Dim i As Long

        ' Check if the data is Empty
        If Not Events Is Nothing Then
            ' Loop through the entire array and store the AE data
            ' NB: In this sample only the last data in the Array is stored
            ' when we are out of the loop
            For Count = 0 To UBound(Events, 2)
                vOPCAEPumpHistID = Events(0, Count)         ' OPCAEPumpHistID
                sSource = Events(1, Count)                  ' Source
                sMessage = Events(2, Count)                 ' Message
                dTimeStamp = Events(3, Count)               ' TimeStamp
                lSequenceNo = Events(4, Count)              ' SequenceNumber
                lEventCategory = Events(5, Count)           ' EventCate
                lSeverity = Events(6, Count)                ' Severity
                sConditionName = Events(7, Count)           ' ConditionName
                lNoOfAttributes = Events(8, Count)          ' Number Of EventAttributes
                vEventAttributesArray = Events(9, Count)    ' EventAttributes in an Array

                ' Loop through all the EventAttributes and store them
                For i = 0 To lNoOfAttributes - 1
                    vEventAttribute = vEventAttributesArray(i)
                Next i

            Next Count

        End If

        ' Check whether more data to come
        If EventReturnType = qeaNew Then
            MsgBox("First notification received")
        ElseIf EventReturnType = qeaMoreEvents Then
            MsgBox("More notifications to be received")
        ElseIf EventReturnType = qeaEnd Then
            MsgBox("Last notification received")
        End If

    End Sub

    '========================================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to recieve Error event for Alarm & Events data read
    '--------------------------------------------------------------------------------------------------------

    Private Sub objAEAccess_OnError(ByVal Number As Long, ByVal Description As String, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long)
        ' Display the Error message
        MsgBox("AE DataAccess OnError() caught. Error Number: " & CStr(Number) & " Description: " & Description & " Source:- " & Source & " HelpFile:- " & HelpFile & " HelpContext:- " & CStr(HelpContext))

    End Sub


    Private Sub Form1_Leave(sender As Object, e As System.EventArgs) Handles Me.Leave

    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Dim serverTime As Date
        Try
            ' Get a session object for Live DataAccess
            objSession = New QUANTUMAUTOMATIONLib.Session

            ' Get the Browser Object from Session.
            objBrowser = objSession.Browser

            'Try
            '    'rootTag = objSession.TagBuilder
            '    rootTag = objSession.TagMaintenance
            '    'rootTag = objSession.TagAnalyser 
            '    Label4.Text &= " Can get Admin tag "
            'Catch ex As Exception
            '    TextBox2.Text = " Error : " & ex.Message

            'End Try


            serverTime = objSession.GetServerTime()
            'rootTag = objSession.GetQuantumRoot()

            qdaNew = 0
            qdaMoreRows = 1
            qdaEnd = 2
            qeaNew = 0
            qeaMoreEvents = 1
            qeaEnd = 2

            brDetail = 2

            Me.Button1.Enabled = True
            Me.Button2.Enabled = True
            Me.Button3.Enabled = True
            Me.Button4.Enabled = False
        Catch ex As Exception
            TextBox2.Text = " Error : " & ex.Message

        End Try


    End Sub


    ''============================================================================================
    ''Synopsis:      Illustrates the use of Exaquantum API to read live history data Synchronously
    ''--------------------------------------------------------------------------------------------

    Private Sub cmdSyncReadHistoryValue_Click()
        Dim rsBrowseResults As ADODB.Recordset
        Dim sItem As String
        Dim lNumItems As Long
        Dim Count As Long
        Dim vItemID As Object
        Dim vData As Object
        Dim vValue As Object
        Dim lQuality As Long
        Dim dTimeStamp As Date
        Dim dQueryTime As Date

        ' Get a DataAccess object.
        objDataAccess = New QUANTUMAUTOMATIONLib.QDataAccess

        ' Give the Session Pointer to the DataAccess object.
        objDataAccess.SetSession(objSession)

        ' Define the Items required by their paths.
        sItem = "PTTGSP.GSP1.Name" ' "PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH4.PV.Value.Timestamp" '"Root.Tag1.Value"

        lNumItems = 1
        brDetail = 2

        ' Resolve the paths to Item Id
        rsBrowseResults = objBrowser.PathToMetaData(sItem, lNumItems, brDetail)

        ' Using the Recordset returned from Browse add the Item Id
        ' to the DataAccess collection.
        vItemID = rsBrowseResults.Fields("ItemID").Value
        If Not vItemID Is Nothing  Then
            ' Add the item to the DataAccess collection

            objDataAccess.Add(vItemID)
        Else
            MsgBox("Path is invalid:" & sItem)
            Exit Sub
        End If

        ' Lets query for the last 5 minutes
        dQueryTime = DateAdd("n", -5, Now)

        ' Set the QueryTimes on DataAccess object
        objDataAccess.SetQueryTimes(dQueryTime)

        ' Get the latest values of all Items in the collection.
        vData = objDataAccess.ReadValue

        ' Loop through all the VTQs returned and store them in an array
        If Not vData Is Nothing  Then
            ' Loop through the entire array and store the data
            ' NB: In this sample only the last data in the Array is stored
            ' when we are out of the loop
            For Count = 0 To UBound(vData(1, 0), 2)
                vValue = vData(1, 0)(0, Count)     ' Value
                lQuality = vData(1, 0)(1, Count)   ' Quality
                dTimeStamp = vData(1, 0)(2, Count) ' Timestamp
            Next Count
        End If

    End Sub

    ''==============================================================================================
    ''Synopsis:      Illustrates the use of Exaquantum API to read Alarm & Events data Synchronously
    ''----------------------------------------------------------------------------------------------

    'Private Sub cmdSyncReadAlarmEvent_Click()
    '    Dim ConnectStr As String
    '    Dim objConnection As ADODB.Connection
    '    Dim objRecordset As ADODB.Recordset
    '    Dim Count As Long
    '    Dim i As Long
    '    Dim vAEId As Object
    '    Dim Events As Object
    '    Dim vOPCAEPumpHistID As Object
    '    Dim sSource As String
    '    Dim sMessage As String
    '    Dim dTimeStamp As Date
    '    Dim lSequenceNo As Long
    '    Dim lEventCategory As Long
    '    Dim lSeverity As Long
    '    Dim sConditionName As String
    '    Dim lNoOfAttributes As Long
    '    Dim vEventAttributesArray As Object
    '    Dim vEventAttribute As Object
    '    Dim dQueryTime As Date

    '    ' Get a Alarm & Event Access object.
    '    objAEAccess = New QUANTUMAUTOMATIONLib.QAEAccess

    '    ' Give the Session Pointer to the Alarm & Event Access object.
    '    objAEAccess.SetSession(objSession)

    '    ' Make the Connect String
    '    objConnection = New ADODB.Connection
    '    ConnectStr = "Provider=sqloledb;"                       'Provider
    '    ConnectStr = ConnectStr + "Network Library=dbmssocn;"   'Network Library
    '    ConnectStr = ConnectStr + "Server=;"                    'Server Name(Local)
    '    ConnectStr = ConnectStr + "database=QConfig;"           'Database Name
    '    ConnectStr = ConnectStr + "Trusted_Connection=yes"      'Make it trusted connection

    '    'Connect to the database
    '    objConnection.Open(ConnectStr)

    '    ' Run the SQL Script and get the recordset
    '    objRecordset = New ADODB.Recordset
    '    objRecordset.LockType = adLockOptimistic
    '    objRecordset.Open("SELECT OPCAEPumpHistId FROM OPCServer WHERE Name = 'OPC Gateway 1'", objConnection)

    '    'Check whether the Recordset is returned from query
    '    If objRecordset.EOF Then
    '        MsgBox("Invalid OPC Gateway name")
    '        Exit Sub
    '    End If

    '    ' Get the OPCAEPumpHistId (first row only for this example)
    '    vAEId = objRecordset!OPCAEPumpHistId

    '    'Check whether the OPC Gateway Alarm & Events Id is found
    '    If Not IsNull(vAEId) Then
    '        ' Add the AE Id into the AEAccess collection
    '        objAEAccess.Add(vAEId)
    '    Else
    '        MsgBox("OPCAEPumpHistId is Null")
    '        Exit Sub
    '    End If


    '    ' Lets query for the last 5 minutes
    '    dQueryTime = DateAdd("n", -5, Now)

    '    ' Set the QueryTimes on AEAccess object
    '    objAEAccess.SetQueryTimes(dQueryTime)

    '    ' Now read the values
    '    Events = objAEAccess.ReadValue

    '    ' Check to see if there are any data returned
    '    If Not IsNull(Events) Then
    '        ' Loop through the entire array and store the AE data
    '        ' NB: In this sample only the last data in the Array is stored
    '        ' when we are out of the loop
    '        For Count = 0 To UBound(Events, 2)
    '            vOPCAEPumpHistID = Events(0, Count)         ' OPCAEPumpHistID
    '            sSource = Events(1, Count)                  ' Source
    '            sMessage = Events(2, Count)                 ' Message
    '            dTimeStamp = Events(3, Count)               ' TimeStamp
    '            lSequenceNo = Events(4, Count)              ' SequenceNumber
    '            lEventCategory = Events(5, Count)           ' EventCate
    '            lSeverity = Events(6, Count)                ' Severity
    '            sConditionName = Events(7, Count)           ' ConditionName
    '            lNoOfAttributes = Events(8, Count)          ' Number Of EventAttributes
    '            vEventAttributesArray = Events(9, Count)    ' EventAttributes in an Array

    '            ' Loop through all the EventAttributes and store them
    '            For i = 0 To lNoOfAttributes - 1
    '                vEventAttribute = vEventAttributesArray(i)
    '            Next i

    '        Next Count
    '    End If

    'End Sub


    '==============================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to read History data Asynchronously
    '----------------------------------------------------------------------------------------------

    Private Sub cmdAsyncReadHistoryValues_Click()
        Dim rsBrowseResults As ADODB.Recordset
        Dim sItem As String
        Dim lNumItems As Long
        Dim vItemID As Object
        Dim vData As Object
        Dim dQueryTime As Date

        ' Get a DataAccess object.
        objDataAccess = New QUANTUMAUTOMATIONLib.QDataAccess

        ' Give the Session Pointer to the DataAccess object.
        objDataAccess.SetSession(objSession)

        ' Define the Items required by their paths.
        sItem = "Root.Tag1.Value"
        lNumItems = 1

        ' Resolve the paths to Item Ids
        rsBrowseResults = objBrowser.PathToMetaData(sItem, lNumItems, brDetail)

        ' Using the Recordset returned from Browse add the Item Id
        ' to the DataAccess collection.
        vItemID = rsBrowseResults.Fields("ItemID")
        If Not vItemID Is Nothing Then
            ' Add the item to the DataAccess collection
            objDataAccess.Add(vItemID)
        Else
            MsgBox("Path is invalid:" & sItem)
            Exit Sub
        End If

        ' Lets query for the last 5 minutes
        dQueryTime = DateAdd("n", -5, Now)

        ' Set the QueryTimes on DataAccess object
        objDataAccess.SetQueryTimes(dQueryTime)

        ' Call Execute on DataAccess object to read data asynchronously
        objDataAccess.Execute()

    End Sub

    '=============================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to cancel Asynchronous History data read
    '---------------------------------------------------------------------------------------------

    Private Sub cmdCancel_Click()
        ' Check to see whether Items are added into  Data Access object
        If objDataAccess.Count > 0 Then
            ' Send Cancel to DataAccess Object
            objDataAccess.Cancel()
        End If

    End Sub

    '==========================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to read Alarm & Events Asynchronously
    '------------------------------------------------------------------------------------------

    'Private Sub cmdASyncReadAlarmEvents_Click()
    '    Dim vAEId As Object
    '    Dim vData As Object
    '    Dim vAEData As Object
    '    Dim lQuality As Long
    '    Dim dTimeStamp As Date
    '    Dim ConnectStr As String
    '    Dim objConnection As ADODB.Connection
    '    Dim objRecordset As ADODB.Recordset
    '    Dim dQueryTime As Date

    '    ' Get a Alarm & Event Access object.
    '    objAEAccess = New QUANTUMAUTOMATIONLib.QAEAccess

    '    ' Give the Session Pointer to the Alarm & Event Access object.
    '    objAEAccess.SetSession(objSession)

    '    ' Make the Connect String
    '    objConnection = New ADODB.Connection
    '    ConnectStr = "Provider=sqloledb;"                       'Provider
    '    ConnectStr = ConnectStr + "Network Library=dbmssocn;"   'Network Library
    '    ConnectStr = ConnectStr + "Server=;"                    'Server Name(Local)
    '    ConnectStr = ConnectStr + "database=QConfig;"           'Database Name
    '    ConnectStr = ConnectStr + "Trusted_Connection=yes"      'Make it trusted connection

    '    'Connect to the database
    '    objConnection.Open(ConnectStr)

    '    ' Run the SQL Script and get the recordset
    '    objRecordset = New ADODB.Recordset
    '    objRecordset.LockType = adLockOptimistic
    '    objRecordset.Open("SELECT OPCAEPumpHistId FROM OPCServer WHERE Name = 'OPC Gateway 1'", objConnection)

    '    'Check whether the Recordset is returned from query
    '    If objRecordset.EOF Then
    '        MsgBox("Invalid OPC Gateway name")
    '        Exit Sub
    '    End If

    '    ' Get the OPCAEPumpHistId (first row only for this example)
    '    vAEId = objRecordset!OPCAEPumpHistId

    '    'Check whether the OPC Gateway Alarm & Events Id is found
    '    If Not IsNull(vAEId) Then
    '        ' Add the AE Id into the AEAccess collection
    '        objAEAccess.Add(vAEId)
    '    Else
    '        MsgBox("OPCAEPumpHistId is Null")
    '        Exit Sub
    '    End If

    '    ' Lets query for the last 5 minutes
    '    dQueryTime = DateAdd("n", -5, Now)

    '    ' Set the QueryTimes on AEAccess object
    '    objAEAccess.SetQueryTimes(dQueryTime)

    '    ' Call Execute on AEAccess to read data asynchronously
    '    objAEAccess.Execute()

    'End Sub

    '========================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to cancel Asynchronous AE data read
    '----------------------------------------------------------------------------------------

    Private Sub cmdCancelAE_Click()
        ' Check to see whether Items are added into AEAccess object
        If objAEAccess.Count > 0 Then
            ' Send Cancel to AEAccess Object
            objAEAccess.Cancel()
        End If 
    End Sub 
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim sItemsArray(1) As String
        Dim lNumItems As Long
        Dim Count As Long
        Dim vItemID As Object 
        Dim i As Integer
        Dim sFieldname As String
        Dim rsBrowseResults As ADODB.Recordset
        Dim sItem As String
        Dim objSession As QUANTUMAUTOMATIONLib.Session2
        Dim objDataAccess As QUANTUMAUTOMATIONLib.QDataAccess

        sItem = "PTTGSP" '"Root.Tag1.Value"

        ' Define the Items required by their paths.
        sItemsArray(0) = "Root.GSP1.process.5FQI750.PV.value" '"Root.Tag1.Value"
        sItemsArray(1) = "Root.GSP1.process.5FQI760.PV.value"
        lNumItems = 2

        objSession = New QUANTUMAUTOMATIONLib.Session2
        objSession.ServerName = "EXAQ-GSP1"

        objDataAccess = New QUANTUMAUTOMATIONLib.QDataAccess
        objBrowser = objSession.Browser
        objDataAccess.SetSession(objSession)

        ' Resolve the paths to Item Ids
        rsBrowseResults = objBrowser.PathToMetaData(sItemsArray, lNumItems, brDetail)
        'rsBrowseResults = objBrowser.PathToMetaData(sItem, lNumItems, brDetail)
        ' Run through the Recordset returned from Browse and add check which
        ' is valid.
        sFieldname = ""
        For Count = 1 To lNumItems
            vItemID = rsBrowseResults.Fields("ItemID") 'rsBrowseResults!ItemID
            If Not IsDBNull(rsBrowseResults.Fields("ItemID")) Then
                ' Do something with the Item ID..
                For i = 1 To rsBrowseResults.Fields.Count - 1
                    sFieldname &= rsBrowseResults.Fields.Item(i).Name & " " '& rsBrowseResults.Fields.Item(i).Value & ":"
                    Label2.Text &= rsBrowseResults.Fields.Item(i).Value & " "
                Next

                'objDataAccess.Add(vItemID)
                'ExaqData = objDataAccess.ReadValue
                'tmpData = ExaqData(1, 0)(0, 0)
                'objDataAccess.Remove(objDataAccess.Item(0))

                Label1.Text = sFieldname
            Else
                MsgBox("Path is invalid:" & sItemsArray(Count - 1))
            End If
            rsBrowseResults.MoveNext()
        Next Count
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim rsBrowseResults As ADODB.Recordset
        Dim sItem As String
        Dim lNumItems As Long
        Dim Count As Long
        Dim vItemID As Object
        Dim vData As Object
        Dim vValue As Object
        Dim lQuality As Long
        Dim dTimeStamp As Date
        Dim dQueryTime As Date
        Dim brDetail As Integer
        brDetail = 2



        Me.Enabled = False
        ' Get a DataAccess object.
        objDataAccess = New QUANTUMAUTOMATIONLib.QDataAccess

        ' Give the Session Pointer to the DataAccess object.
        objDataAccess.SetSession(objSession)


        dTimeStamp = objSession.GetServerTime()

        'PTTGSP.GSP6.3617.LEVEL.3617LIA001.PV.Value:Timestamp","2016-08-01 14:41:00","NOW","1899-12-30 00:00:00","1899-12-30 00:00:00",,0,0,0,1,,,,,,,,,0,,0
        ' Define the Items required by their paths.
        sItem = "PTTGSP.GSP6.3617.LEVEL.3617LIA001.PV.Value" '"Root.Tag1.Value"
        'sItem = "PTTGSP.GSP1.GC ONLINE.700-QR-006 STR1 PROPANE.NORMALIZE.700QR-6AC2H6.PV.Value:Value" '"Root.Tag1.Value"
        lNumItems = 1
        ' Resolve the paths to Item Id
        rsBrowseResults = objBrowser.PathToMetaData(sItem, lNumItems, brDetail)
    
        ' Using the Recordset returned from Browse add the Item Id
        ' to the DataAccess collection.
        vItemID = rsBrowseResults.Fields("ItemID") 'rsBrowseResults!ItemID
        If Not IsDBNull(rsBrowseResults.Fields("ItemID")) Then ' IsNull(vItemID) Then
            ' Add the item to the DataAccess collection
            'objDataAccess.Add(vItemID)
            For i = 1 To rsBrowseResults.Fields.Count - 1
                Label1.Text &= rsBrowseResults.Fields.Item(i).Name & "      " '& rsBrowseResults.Fields.Item(i).Value & ":"
                Label2.Text &= rsBrowseResults.Fields.Item(i).UnderlyingValue & "      "
            Next 
        Else
            MsgBox("Path is invalid:" & sItem)
            Exit Sub
        End If
        '' Lets query for the last 5 minutes
        'dQueryTime = DateAdd("n", -5, Now)
        '' Set the QueryTimes on DataAccess object
        'objDataAccess.SetQueryTimes(dQueryTime)
        '' Get the latest values of all Items in the collection.
        'vData = objDataAccess.ReadValue


        '' Loop through all the VTQs returned and store them
        'If (vbNull <> vData) Then
        '    ' Loop through the entire array and store the data
        '    ' NB: In this sample only the last data in the Array is stored when weare out of the loop
        '    For Count = 0 To UBound(vData(1, 0), 2)
        '        vValue = vData(1, 0)(0, Count) ' Value
        '        lQuality = vData(1, 0)(1, Count) ' Quality
        '        dTimeStamp = vData(1, 0)(2, Count) ' Timestamp
        '    Next Count
        'End If

        Me.Enabled = True

        Me.Button1.Enabled = False
        Me.Button2.Enabled = True
        Me.Button3.Enabled = False
        Me.Button4.Enabled = False
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim rsBrowseResults As ADODB.Recordset
        Dim sItem As String
        Dim lNumItems As Long
        Dim Count As Long
        Dim vItemID As Object
        Dim vData As Object
        Dim vValue As Object
        Dim lQuality As Long
        Dim dTimeStamp As Date
        Dim dQueryTime As Date
        Dim brDetail As Integer

        brDetail = 2

        ' Get a DataAccess object.
        objDataAccess = New QUANTUMAUTOMATIONLib.QDataAccess

        ' Give the Session Pointer to the DataAccess object.
        objDataAccess.SetSession(objSession)

        ' Define the Items required by their paths.
        sItem = "PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH4.PV.Value:Timestamp"
        lNumItems = 1
        ' Resolve the paths to Item Id
        rsBrowseResults = objBrowser.PathToMetaData(sItem, lNumItems, brDetail)
        ' Using the Recordset returned from Browse add the Item Id
        ' to the DataAccess collection.
        vItemID = rsBrowseResults.Fields("ItemID")
        If Not IsDBNull(rsBrowseResults.Fields("ItemID")) Then
            ' Add the item to the DataAccess collection
            objDataAccess.Add(vItemID)
        Else
            MsgBox("Path is invalid:" & sItem)
            Exit Sub
        End If
        ' Lets query for the last 5 minutes
        dQueryTime = DateAdd("n", -5, Now)
        ' Set the QueryTimes on DataAccess object
        objDataAccess.SetQueryTimes(dQueryTime)
        ' Get the latest values of all Items in the collection.
        vData = objDataAccess.ReadValue

        ' Loop through all the VTQs returned and store them
        If (vbNull <> vData) Then
            ' Loop through the entire array and store the data
            ' NB: In this sample only the last data in the Array is stored when we are out of the loop
            For Count = 0 To UBound(vData(1, 0), 2)
                vValue = vData(1, 0)(0, Count) ' Value
                lQuality = vData(1, 0)(1, Count) ' Quality
                dTimeStamp = vData(1, 0)(2, Count) ' Timestamp
            Next Count
        End If
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        cmdSyncReadHistoryValue_Click()
    End Sub



    '=============================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to recieve data event for live data read
    '---------------------------------------------------------------------------------------------
    Private Sub objDataAccess_OnData(ItemID As Object, DataReturnType As QUANTUMAUTOMATIONLib.qdaDataReturnType, Data As Object) Handles objDataAccess.OnData
        Dim vValue As Object
        Dim lQuality As Long
        Dim dTimeStamp As Date
        Dim vItemID As Object
        Dim Count As Long

        ' Read the Item Id for which this data is sent
        vItemID = ItemID


        ' Loop through the entire array and store the data
        ' NB: In this sample only the last data in the Array is stored
        ' when we are out of the loop
        If Not Data Is Nothing Then        ' Ignore notification if Data is NULL
            For Count = 0 To UBound(Data, 2)
                vValue = Data(0, Count)     ' Value
                lQuality = Data(1, Count)   ' Quality
                dTimeStamp = Data(2, Count) ' Timestamp
            Next Count
        End If

        ' Check whether more data to come
        If DataReturnType = qdaNew Then
            MsgBox("First notification received")
        ElseIf DataReturnType = qdaMoreRows Then
            MsgBox("More notifications to be received")
        ElseIf DataReturnType = qdaEnd Then
            MsgBox("Last notification received")
        End If
    End Sub
    '==============================================================================================
    'Synopsis:      Illustrates the use of Exaquantum API to recieve Error event for live data read
    '----------------------------------------------------------------------------------------------

    Private Sub objDataAccess_OnError(Number As Integer, Description As String, Source As String, HelpFile As String, HelpContext As Integer) Handles objDataAccess.OnError
        ' Display the Error message
        MsgBox("Live Data Access OnError() caught. Error Number: " & CStr(Number) & " Description: " & Description & " Source:- " & Source & " HelpFile:- " & HelpFile & " HelpContext:- " & CStr(HelpContext))

    End Sub
End Class

