﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_CrossVolume }, DataAccess = DataAccessEnum.Authen)]
    public class CrossVolumeController : BaseController
    {
        //
        // GET: /CrossVolume/

        public ActionResult ListManageCrossVolume()//control ListCrossVolumeExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CrossVolumeSearchModel searchCrossVolume = new CrossVolumeSearchModel();
            CreateQMS_TR_CROSS_VOLUME createQMS_TR_CROSS_VOLUME = new CreateQMS_TR_CROSS_VOLUME();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel(); 

            createQMS_TR_CROSS_VOLUME.START_DATE = getDummyDateTime();
            createQMS_TR_CROSS_VOLUME.END_DATE = getDummyDateTime();
            createQMS_TR_CROSS_VOLUME.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.ABNORMAL;

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusListWithInitail();
            ViewBag.listDocStatusEx = this.getDocStatusList();
            ViewBag.listProductTo = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

            ViewBag.searchCrossVolume = searchCrossVolume;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_TR_CROSS_VOLUME = createQMS_TR_CROSS_VOLUME;
            ViewBag.volumeSearch = volumeSearch; 
            return View();
        }

        public ActionResult CreateManageCrossVolume(long id = 0)//control CresteCrossVolumeExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_CROSS_VOLUME createQMS_TR_CROSS_VOLUME = new CreateQMS_TR_CROSS_VOLUME();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_CROSS_VOLUME.START_DATE = getDummyDateTime();
            createQMS_TR_CROSS_VOLUME.END_DATE = getDummyDateTime();
            createQMS_TR_CROSS_VOLUME.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.ABNORMAL;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_CROSS_VOLUME = transactionServices.getQMS_TR_CROSS_VOLUMEById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listToProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();
            ViewBag.listProductTo = masterDataServices.GetProductListByNotinId(_db, createQMS_TR_CROSS_VOLUME.PRODUCT_ID);

            ViewBag.createQMS_TR_CROSS_VOLUME = createQMS_TR_CROSS_VOLUME;
            ViewBag.volumeSearch = volumeSearch;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_CROSS_VOLUME(CreateQMS_TR_CROSS_VOLUME model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    //reset time
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);

                    VALIDATE_EXCEPTION_OVERLLAP checkOverlap = transactionServices.IsOverlapMasterCorrectDataEx(_db, model.ID, model.PLANT_ID, model.PRODUCT_ID, 0, model.START_DATE, model.END_DATE, (byte)CORRECT_DATA_TYPE.CROSS_VOLUME);
                     if (checkOverlap.IS_OVERLAP)
                     {
                         result = new { result = ReturnStatus.ERROR, message = checkOverlap.MSG_ERROR };
                     }
                     else
                     {
                         long nResult = transactionServices.SaveQMS_TR_CROSS_VOLUME(_db, model);

                         if (nResult > 0)
                         {
                             model.ID = nResult;
                             model.CROSS_VOLUME_CHECK = true; //alway true;
                             transactionServices.SaveQMS_TR_OFF_CONTROL_CAL_With_CROSS_VOLUME(_db, model); //Adjust
                             model = transactionServices.getQMS_TR_CROSS_VOLUMEById(_db, nResult);
                             result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                         }
                         else
                         {
                             result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                         }
                     }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_CROSS_VOLUMEBySearch(CrossVolumeSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_CROSS_VOLUME> listData = new List<ViewQMS_TR_CROSS_VOLUME>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_CROSS_VOLUME(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_CROSS_VOLUME(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_CROSS_VOLUMEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_CROSS_VOLUME(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_CROSS_VOLUMEByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductListByNotinId(long productId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_PRODUCT> listData = new List<ViewQMS_MA_PRODUCT>();
                    listData = masterDataServices.GetProductListByNotinId(_db, productId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
