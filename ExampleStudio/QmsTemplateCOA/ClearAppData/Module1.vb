﻿Imports System.IO

Module Module1

    Sub Main(args As String())
        Dim appData As String = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).ToString() & "\Local\Temp"
        Console.WriteLine("Clear AppData 1.0 Start")

        If args.Length > 0 Then
            appData = args(0)
        End If
        ClearAppData(appData)
        Console.WriteLine("")
        Console.WriteLine("Clear AppData 1.0 End")
        Console.ReadLine()
    End Sub

    Public Function ClearAppData(appData As String) As Boolean


        Console.WriteLine("Check file in folder : " & appData)
        Dim nCount As Integer
        If (System.IO.Directory.Exists(appData)) Then
            'System.IO.Directory.Delete(appData)
            Dim nCheck = Directory.GetDirectories(appData).Length
            Console.WriteLine("Total file & folder in directory  : " & nCheck)

            Console.Write("Start check and delete : 1 ")
            For Each DirName As String In Directory.GetDirectories(appData)
                nCount = nCount + 1

                If ((nCount Mod 50) = 0) Then
                    Console.Write("/ " & nCount)
                End If



                If DirName.Contains("TMP_") Then
                    If (System.IO.Directory.Exists(DirName)) Then
                        Console.WriteLine(DirName)
                        DeleteDirectory(DirName)
                        'System.IO.Directory.Delete(DirName)
                    End If
                End If
                'Console.WriteLine(Dir)
            Next
        End If

        Return True
    End Function
    Private Sub DeleteDirectory(path As String)
        If Directory.Exists(path) Then
            'Delete all files from the Directory
            For Each filepath As String In Directory.GetFiles(path)
                File.Delete(filepath)
            Next
            'Delete all child Directories
            For Each dir As String In Directory.GetDirectories(path)
                DeleteDirectory(dir)
            Next
            'Delete a Directory
            Directory.Delete(path)
        End If
    End Sub

End Module
