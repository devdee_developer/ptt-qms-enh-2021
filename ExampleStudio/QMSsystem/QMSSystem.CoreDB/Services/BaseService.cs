﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QMSSystem.CoreDB.Helper;
using QMSSystem.Model;
using System.Globalization;
using System.Text.RegularExpressions;

namespace QMSSystem.CoreDB.Services
{
    public class BaseService
    {
        public string _errMsg;
        public long _result;
        public string _currentUserName;
        public int _errFlag = 1;
          
        public BaseService()
        {
            _errMsg = "";
            _result = -1;
        }

        public BaseService(string userName)
        {
            _errMsg = "";
            _result = -1;
            _currentUserName = userName;
        }

        protected DateTime resetSecond(DateTime data)
        {
            DateTime mDummy = new DateTime(data.Year, data.Month, data.Day, data.Hour, data.Minute, 0);
            return mDummy;
        }

        public List<string> listColumn = new List<string>(new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB",
            "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" });

        protected string[] monthEnString = new string[] { "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december" };
        protected string[] monthEnShortString = new string[] { "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" };
        protected string[] monthThString = new string[] { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
        protected string[] monthThShortString = new string[] { "ม.ค.",  "ก.พ.",  "มี.ค.", "เม.ย.",  "พ.ค.",  "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.",  "ธ.ค." };
        public string getError()
        {
            return _errMsg;
        }

        public void writeErrorLog(string msg)
        {
            _errMsg = msg;
            LogWriter.WriteErrorLog(msg);
            //LogHelper.WriteErrorLog(msg);
        }

        public string getEexceptionError(Exception ex)
        {
            string result = "";

            if (ex.InnerException != null)
            {
                result = ex.Message + " : " + ex.InnerException.Message;
            }
            else
            {
                result = ex.Message;
            }

            return result;
        }  

        public string GetDateString(DateTime dt)
        {
            string result = "";


            if (dt.Year < 2500)
            {
                result = dt.Year.ToString();//
            }
            else
            {
                result = (dt.Year - 543).ToString();
            }

            if (dt.Month < 10)
            {
                result += "-0" + dt.Month.ToString();
            }
            else
            {
                result += "-" + dt.Month.ToString();
            }

            if (dt.Day < 10)
            {
                result += "-0" + dt.Day.ToString();
            }
            else
            {
                result += "-" + dt.Day.ToString();
            }

            return result + "T00:00:00.000Z";
        }

        protected List<ViewQMS_MA_CONTROL> getControlGroupList(QMSDBEntities db, byte gruopType)
        {
            List<ViewQMS_MA_CONTROL> listResult = new List<ViewQMS_MA_CONTROL>();
            MasterDataService masterDataServices = new MasterDataService(_currentUserName);
            ControlValueSearch model = new ControlValueSearch();

            model.PageIndex = 0;
            model.PageSize = -1; // get All
            model.SortColumn = "NAME";
            model.SortOrder = "desc";

            model.mSearch = new ControlValueSearchModel();
            model.mSearch.GROUP_TYPE = gruopType;


            long count = 0;
            long totalPage = 0;
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listResult = masterDataServices.searchQMS_MA_CONTROL(db, model, out count, out totalPage, out listPageIndex);

            return listResult;
        }

        public void setCurrentUserName(string userName)
        {
            _currentUserName = userName;
        }

        public string getPlantName(List<QMS_MA_PLANT> listPlant, long id)
        {
            string szResult = "";
            try
            {
                szResult = listPlant.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getPlantName(List<ViewQMS_MA_PLANT> listPlant, long id)
        {
            string szResult = "";
            try
            {
                szResult = listPlant.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        } 

        public string getProductName(List<QMS_MA_PRODUCT> listProduct, long id)
        {
            string szResult = "";
            try
            {
                szResult = listProduct.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public byte getTypeOffControl(List<QMS_RP_OFF_CONTROL> listOffControl, long id)
        {
            byte szResult = 0;
            try
            {
                szResult = listOffControl.Where(m => m.ID == id).Select(m => m.OFF_CONTROL_TYPE).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        } 

        public string getEXA_TAG_NAME(List<QMS_MA_REDUCE_FEED_DETAIL> listReduceFeedDetail, long id)
        {
            string szResult = "";
                        
            try
            {
                
                foreach (QMS_MA_REDUCE_FEED_DETAIL dt in listReduceFeedDetail)
                {
                    if(dt.REDUCE_FEED_ID == id){
                        if ( szResult == "" ){ 
                            szResult = dt.EXA_TAG_NAME;
                        }else{  
                            szResult = szResult + " , " + dt.EXA_TAG_NAME;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getDOWNTIME_NAME(List<QMS_MA_DOWNTIME_DETAIL> listDowntimeDetail, long id)
        {
            string szResult = "";

            try
            {

                foreach (QMS_MA_DOWNTIME_DETAIL dt in listDowntimeDetail)
                {
                    if (dt.DOWNTIME_ID == id)
                    {
                        if (szResult == "")
                        {
                            szResult = dt.EXA_TAG_NAME;
                        }
                        else
                        {
                            szResult = szResult + " , " + dt.EXA_TAG_NAME;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getGradeName(List<QMS_MA_GRADE> listGrade, long id)
        {
            string szResult = "";
            try{
                szResult = listGrade.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }catch(Exception ex){

            } 
            return szResult;
        }

        public string reovleMultiplicand(byte id)
        {
            string szResult = "";

            switch (id)
            {
                case (byte)MULTIPLICAND.COUNT_OUPPUT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.COUNT_OUPPUT;
                    break;
                case (byte)MULTIPLICAND.UNCOUNT_OUPPUT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.UNCOUNT_OUPPUT;
                    break;
                case (byte)MULTIPLICAND.MOVED_OUPPUT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.MOVED_OUPPUT;
                    break;
            }  
            return szResult;
        }

        public string reovleDenominator(byte id)
        {
            string szResult = "";

            switch (id)
            {
                case (byte)DENOMINATOR.COUNT_INPUT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.COUNT_INPUT;
                    break;
                case (byte)DENOMINATOR.UNCOUNT_INPUT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.UNCOUNT_INPUT;
                    break;
                case (byte)DENOMINATOR.MOVED_INPUT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.MOVED_INPUT;
                    break;
            }
            return szResult;
        }

        public string reovleOffControlMethod(byte id)
        {
            string szResult = "";

            switch (id)
            {
                case (byte)OFF_CONTROL_CAL_METHOD.NOT_CALUCATE:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.NOT_CALUCATE;
                    break;
                case (byte)OFF_CONTROL_CAL_METHOD.NORMAL_CALUCATE:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.NORMAL_CALUCATE;
                    break;
                case (byte)OFF_CONTROL_CAL_METHOD.OFFCONTROL_OFF:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.OFFCONTROL_OFF;
                    break;
                case (byte)OFF_CONTROL_CAL_METHOD.OFFCONTROL_ON:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.OFFCONTROL_ON;
                    break;
            }
            return szResult;
        }

        public string getRootCauseTypeName(List<QMS_MA_ROOT_CAUSE_TYPE> listRootCauseTypeName, long id)
        {
            string szResult = "";
            try
            {
                szResult = listRootCauseTypeName.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getRootCauseName(List<QMS_MA_ROOT_CAUSE> listRootCauseName, long id)
        {
            string szResult = "";
            try
            {
                szResult = listRootCauseName.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getPossibleName(List<QMS_MA_IQC_POSSICAUSE> listPossibleCause, long id)
        {
            string szResult = "";
            try
            {
                szResult = listPossibleCause.Where(m => m.POSSICAUSE_ID == id).Select(m => m.POSSICAUSE_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public List<ViewQMS_MA_IQC_POSSICAUSE> setArrowUPAndDown(List<ViewQMS_MA_IQC_POSSICAUSE> listResult)
        {

            if (listResult.Count() > 1)
            {
                long count = 1;
                long maxCount = listResult.Count();

                foreach (ViewQMS_MA_IQC_POSSICAUSE dt in listResult)
                {
                    if (count == 1)
                    {
                        dt.ARROW_UP = false;
                    }
                    else if (count == maxCount)
                    {
                        dt.ARROW_DOWN = false;
                    }
                    count++;
                }
            }
            else
            {
                foreach (ViewQMS_MA_IQC_POSSICAUSE dt in listResult)
                {
                    dt.ARROW_UP = false;
                    dt.ARROW_DOWN = false;
                }
            }

            return listResult;
        }

        public string getEmployeeName(List<QMS_MA_EMAIL> listEmail, long id)
        {
            string szResult = "";
            try
            {
                szResult = listEmail.Where(m => m.ID == id).Select(m => m.EMPLOYEE_ID).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

    
   


        public string getNameMail(List<QMS_MA_EMAIL> listEmail, long id)
        {
            string szResult = "";
            try
            {
                szResult = listEmail.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getIQCEquipName(List<QMS_MA_IQC_EQUIP> listEquip, long id)
        {
            string szResult = "";
            try
            {
                szResult = listEquip.Where(m => m.ID == id).Select(m => m.EQUIP_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getPossibleReason(List<QMS_MA_IQC_CONTROLREASON> listControlReason, long id)
        {
            string szResult = "";
            try
            {
                szResult = listControlReason.Where(m => m.CONTROLRULE_ID == id).Select(m => m.REASON).FirstOrDefault();

            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getEmail(List<QMS_MA_EMAIL> listEmail, long id)
        {
            string szResult = "";
            try
            {
                szResult = listEmail.Where(m => m.ID == id).Select(m => m.EMAIL).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getIQCRuleName(List<QMS_MA_IQC_RULE> listIQCRule, long id)
        {
            string szResult = "";
            try
            {
                szResult = listIQCRule.Where(m => m.ID == id).Select(m => m.RULE_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getIQCItemName(List<QMS_MA_IQC_ITEM> listIQCItem, long id)
        {
            string szResult = "";
            try
            {
                szResult = listIQCItem.Where(m => m.ID == id).Select(m => m.ITEM_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getIQCItemEquipName(List<QMS_MA_IQC_ITEMEQUIP> listIQCItemEquip, long id, QMSDBEntities db)
        {
            List<QMS_MA_IQC_ITEM> listIQCItem = QMS_MA_IQC_ITEM.GetAll(db);
            List<QMS_MA_IQC_EQUIP> listIQCEquip = QMS_MA_IQC_EQUIP.GetAll(db);
            string szResult = "";
            long Item_id = 0;
            long Equip_id = 0;
            try
            {
                Item_id = listIQCItemEquip.Where(m => m.ID == id).Select(m => m.ITEM_ID).FirstOrDefault();
                szResult = listIQCItem.Where(m => m.ID == Item_id).Select(m => m.ITEM_NAME).FirstOrDefault();
                Equip_id = listIQCItemEquip.Where(m => m.ID == id).Select(m => m.EQUIP_ID).FirstOrDefault();
                szResult += " (" + listIQCEquip.Where(m => m.ID == Equip_id).Select(m => m.EQUIP_NAME).FirstOrDefault() + ") ";
                
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getIQCsigmaName(List<QMS_MA_IQC_METHOD> listIQCMethod, long id)
        {
            string szResult = "";
            try
            {
                szResult = listIQCMethod.Where(m => m.ID == id).Select(m => m.METHOD_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public decimal getIQCControlDataUCL(QMSDBEntities db, IQCControlDataSearch searchModel, DateTime KEEPTIME)
        {
            List<QMS_MA_IQC_FORMULA> listIQCFormula = QMS_MA_IQC_FORMULA.GetAll(db);
            decimal szResult = 0;
            try
            {
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.METHOD_ID == m.METHOD_ID)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.ITEM_ID == m.FORMULA_PROD)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (m.ACTIVE_DATE <= KEEPTIME && KEEPTIME <= m.EXPIRE_DATE)).ToList();                
                szResult = listIQCFormula.Select(m => m.FORMULA_UCL).FirstOrDefault().Value;
 
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public decimal getIQCControlDataUWL(QMSDBEntities db, IQCControlDataSearch searchModel, DateTime KEEPTIME)
        {
            List<QMS_MA_IQC_FORMULA> listIQCFormula = QMS_MA_IQC_FORMULA.GetAll(db);
            decimal szResult = 0;
            try
            {
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.METHOD_ID == m.METHOD_ID)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.ITEM_ID == m.FORMULA_PROD)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (m.ACTIVE_DATE <= KEEPTIME && KEEPTIME <= m.EXPIRE_DATE)).ToList();
                szResult = listIQCFormula.Select(m => m.FORMULA_UWL).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public decimal getIQCControlDataAVG(QMSDBEntities db, IQCControlDataSearch searchModel, DateTime KEEPTIME)
        {
            List<QMS_MA_IQC_FORMULA> listIQCFormula = QMS_MA_IQC_FORMULA.GetAll(db);
            decimal szResult = 0;
            try
            {
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.METHOD_ID == m.METHOD_ID)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.ITEM_ID == m.FORMULA_PROD)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (m.ACTIVE_DATE <= KEEPTIME && KEEPTIME <= m.EXPIRE_DATE)).ToList();
                szResult = listIQCFormula.Select(m => m.FORMULA_AVG).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public decimal getIQCControlDataLWL(QMSDBEntities db, IQCControlDataSearch searchModel, DateTime KEEPTIME)
        {
            List<QMS_MA_IQC_FORMULA> listIQCFormula = QMS_MA_IQC_FORMULA.GetAll(db);
            decimal szResult = 0;
            try
            {
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.METHOD_ID == m.METHOD_ID)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.ITEM_ID == m.FORMULA_PROD)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (m.ACTIVE_DATE <= KEEPTIME && KEEPTIME <= m.EXPIRE_DATE)).ToList();
                szResult = listIQCFormula.Select(m => m.FORMULA_LWL).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public decimal getIQCControlDataLCL(QMSDBEntities db, IQCControlDataSearch searchModel, DateTime KEEPTIME)
        {
            List<QMS_MA_IQC_FORMULA> listIQCFormula = QMS_MA_IQC_FORMULA.GetAll(db);
            decimal szResult = 0;
            try
            {
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.METHOD_ID == m.METHOD_ID)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (searchModel.mSearch.ITEM_ID == m.FORMULA_PROD)).ToList();
                listIQCFormula = listIQCFormula.Where(m => (m.ACTIVE_DATE <= KEEPTIME && KEEPTIME <= m.EXPIRE_DATE)).ToList();
                szResult = listIQCFormula.Select(m => m.FORMULA_LCL).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }


        public List<PageIndexList> getPageIndexList(long total )
        {
            List<PageIndexList> listResult = new List<PageIndexList>();
             
            for (int i = 0; i < total; i++)
            {
                listResult.Add(new PageIndexList { PageIndex = i + 1 });
            } 

            return listResult;
        }

        public string getCorrectDataTypeName(List<QMS_MA_CORRECT_DATA> listCorrectDataType, long id)
        {
            string szResult = "";
            try
            {
                szResult = listCorrectDataType.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getControlColumnName(List<QMS_MA_CONTROL_COLUMN> listData, long id)
        {
            string szResult = "";
            try
            {
                szResult = listData.Where(m => m.ID == id).Select(m => m.ITEM).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getControlColumnNameEx(List<ViewQMS_MA_CONTROL_COLUMN> listData, long id)
        {
            string szResult = "";
            try
            {
                szResult = listData.Where(m => m.ID == id).Select(m => m.ITEM).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getControlROWName(List<QMS_MA_CONTROL_ROW> listData, long id)
        {
            string szResult = "";
            try
            {
                szResult = listData.Where(m => m.ID == id).Select(m => m.SAMPLE).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getControlROWNameEx(List<ViewQMS_MA_CONTROL_ROW> listData, long id)
        {
            string szResult = "";
            try
            {
                szResult = listData.Where(m => m.ID == id).Select(m => m.SAMPLE).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getROWName(List<ViewQMS_MA_TEMPLATE_ROW> listData, long id)
        {
            string szResult = "";
            try
            {
                szResult = listData.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getColumnNameItem(List<ViewQMS_MA_TEMPLATE_COLUMN> listData, long id)
        {
            string szResult = "";
            try
            {
                szResult = listData.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getROWNameItem(List<ViewQMS_MA_TEMPLATE_ROW> listData, long id)
        {
            string szResult = "";
            try
            {
                szResult = listData.Where(m => m.ID == id).Select(m => m.ITEM_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getUnitName(List<QMS_MA_UNIT> listUnit, long id)
        {
            string szResult = "";
            try
            {
                szResult = listUnit.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getUnitName(List<ViewQMS_MA_UNIT> listUnit, long id)
        {
            string szResult = "";
            try
            {
                szResult = listUnit.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getUnitName(List<QMS_MA_CONTROL_COLUMN> listColumn, List<QMS_MA_UNIT> listUnit, long id)
        {
            string szResult = "";
            try
            {
                var unitId = listColumn.Where(m => m.ID == id).Select(m => m.UNIT_ID).FirstOrDefault();

                if (null != unitId)
                {
                    szResult = listUnit.Where(m => m.ID == unitId).Select(m => m.NAME).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getTemplateName(List<QMS_MA_TEMPLATE> listTemplateName, long id)
        {
            string szResult = "";
            try
            {
                szResult = listTemplateName.Where(m => m.ID == id).Select(m => m.NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getExaTagName(List<QMS_MA_EXQ_TAG> listExaTagName, long id)
        {
            string szResult = "";
            try
            {
                szResult = listExaTagName.Where(m => m.ID == id).Select(m => m.EXA_TAG_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getDowntimeName(List<QMS_MA_DOWNTIME> listDowntimeName, long id)
        {
            string szResult = "";
            try
            {
                szResult = listDowntimeName.Where(m => m.ID == id).Select(m => m.EXCEL_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string getReduceFeedName(List<QMS_MA_REDUCE_FEED> listReduceFeedName, long id)
        {
            string szResult = "";
            try
            {
                szResult = listReduceFeedName.Where(m => m.ID == id).Select(m => m.EXCEL_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        public string reovleDocStatus(byte id)
        {
            string szResult = "";

            switch (id)
            {
                case (byte)DOC_STATUS.PENDING:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.PENDING;
                    break;
                case (byte)DOC_STATUS.CONFIRM:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.CONFIRM;
                    break;
                case (byte)DOC_STATUS.REJECT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.REJECT;
                    break;
                case (byte)DOC_STATUS.INITIAL:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.INITIAL;
                    break;
            }
            return szResult;
        }

        public string reovleTREND_GRAPH_FLAG(byte id)
        {
            string szResult = "";

            switch (id)
            {
                case (byte)TREND_GRAPH_ENUM.OFF:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.OFF;
                    break;
                case (byte)TREND_GRAPH_ENUM.ON:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.ON;
                    break; 
            }
            return szResult;
        }

        public string reovleReportDocStatus(byte id)
        {
            string szResult = "";

            switch (id)
            {
                case (byte)REPORT_DOC_STATUS.DRAFT:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.DRAFT;
                    break;
                case (byte)REPORT_DOC_STATUS.PUBLIC:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.PUBLIC;
                    break;
               
            }
            return szResult;
        }

        public string revoleTypeReportName(byte id)
        {
            string szResult = "";

            switch (id)
            {
                case (byte)OFF_TYPE.CONTROL:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.CONTROL;
                    break;
                case (byte)OFF_TYPE.SPEC:
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.SPEC;
                    break;

            }
            return szResult;
        }


        public string getchColumnExcel(int id)
        {
            string szResult = "";

            switch (id)
            {
                case 1: szResult = "A";break;
                case 2: szResult = "B"; break;
                case 3: szResult = "C"; break;
                case 4: szResult = "D"; break;
                case 5: szResult = "E"; break;
                case 6: szResult = "F"; break;
                case 7: szResult = "G"; break;
                case 8: szResult = "H"; break;
                case 9: szResult = "I"; break;
                case 10: szResult = "J"; break;
                case 11: szResult = "K"; break;
                case 12: szResult = "L"; break;
                case 13: szResult = "M"; break;
                case 14: szResult = "N"; break;
                case 15: szResult = "O"; break;
                case 16: szResult = "P"; break;
                case 17: szResult = "Q"; break;
                case 18: szResult = "R"; break;
                case 19: szResult = "S"; break;
                case 20: szResult = "T"; break;
                case 21: szResult = "U"; break;
                case 22: szResult = "V"; break;
                case 23: szResult = "W"; break;
                case 24: szResult = "X"; break;
                case 25: szResult = "Y"; break;
                case 26: szResult = "Z"; break;
                case 27: szResult = "AA"; break;
                case 28: szResult = "AB"; break;
                case 29: szResult = "AC"; break;
                case 30: szResult = "AD"; break;
                case 31: szResult = "AE"; break;
                case 32: szResult = "AF"; break;
                case 33: szResult = "AG"; break;
                case 34: szResult = "AH"; break;
                case 35: szResult = "AI"; break;
                case 36: szResult = "AJ"; break;
                case 37: szResult = "AK"; break;
                case 38: szResult = "AL"; break;
                case 39: szResult = "AM"; break;
                case 40: szResult = "AN"; break;
                case 41: szResult = "AO"; break;
                case 42: szResult = "AP"; break;
                case 43: szResult = "AQ"; break;
                case 44: szResult = "AR"; break;
                case 45: szResult = "AS"; break;
                case 46: szResult = "AT"; break;
                case 47: szResult = "AU"; break;
                case 48: szResult = "AV"; break;
                case 49: szResult = "AW"; break;
                case 50: szResult = "AX"; break;
                case 51: szResult = "AY"; break;
                case 52: szResult = "AZ"; break;

            }
            return szResult;
        }

        public long getControlGroupColumnIdByControlDataId(List<ViewQMS_MA_CONTROL_DATA> listData, long id)
        {
            long szResult = 0;

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    ViewQMS_MA_CONTROL_DATA data = listData.Where(m => m.ID == id).FirstOrDefault();
                    if (null != data)
                        szResult = data.CONTROL_COLUMN_ID; //QMS_MA_CONTROL_COLUMN ID
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public long getControlGroupRowIdByControlDataId(List<ViewQMS_MA_CONTROL_DATA> listData, long id)
        {
            long szResult = 0;

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    ViewQMS_MA_CONTROL_DATA data = listData.Where(m => m.ID == id).FirstOrDefault();
                    if (null != data)
                        szResult = data.CONTROL_ROW_ID; //QMS_MA_CONTROL_ROW ID
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }


        public string getProductMappingIdByProductMappingId(List<ViewQMS_MA_PRODUCT_MERGE> listData, long? id)
        {
            string szResult = "";

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    ViewQMS_MA_PRODUCT_MERGE data = listData.Where(m => m.ID == id).FirstOrDefault();
                    if (null != data)
                        szResult = data.DISPLAY_NAME; //QMS_MA_PRODUCT_MERGE
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public string getCustomerMappingIdByCustomerMappingId(List<ViewQMS_MA_CUSTOMER_MERGE> listData, long? id)
        {
            string szResult = "";

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    ViewQMS_MA_CUSTOMER_MERGE data = listData.Where(m => m.ID == id).FirstOrDefault();
                    if (null != data)
                        szResult = data.CUSTOMER_NAME; //QMS_MA_CUSTOMER_MERGE
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public long getControlGroupIdByControlDataId(List<ViewQMS_MA_CONTROL_DATA> listData, long id)
        {
            long szResult = 0;

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    ViewQMS_MA_CONTROL_DATA data = listData.Where(m => m.ID == id).FirstOrDefault();
                    if (null != data)
                        szResult = data.CONTROL_ID; //QMS_MA_CONTROL ID
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public string getControlValueTextByControlDataId(List<ViewQMS_MA_CONTROL_DATA> listData, long id)
        {
            string szResult = "-";

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    ViewQMS_MA_CONTROL_DATA data = listData.Where(m => m.ID == id).FirstOrDefault();
                    if (null != data)
                        szResult = getControlValueText(data);
                }
            }
            catch (Exception ex)
            {

            }
             
            return szResult;
        }

        public string getControlConfigValueTextByControlDataId(List<ViewQMS_MA_CONTROL_DATACONF> listData, long id)
        {
            string szResult = "-";

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    ViewQMS_MA_CONTROL_DATACONF data = listData.Where(m => m.ID == id).FirstOrDefault();
                    if (null != data)
                        szResult = getControlValueText(data);
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public string getControlValueTextByControlDataIdEx(List<ViewQMS_MA_CONTROL_DATA> listData, long controlId, long changeControlId )
        {
            string szResult = "-";
            ViewQMS_MA_CONTROL_DATA data = new ViewQMS_MA_CONTROL_DATA();

            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    data = listData.Where(m => m.ID == controlId).FirstOrDefault();
                    if (null != data)
                    {
                        szResult = getControlValueText(data);


                        data = listData.Where(m => m.ID == changeControlId).FirstOrDefault();
                        if (null != data)
                        {
                            szResult += " , " + getControlValueText(data);
                        }
                        else
                        {
                            //Not things.
                        }
                    }
                    else
                    {
                        data = listData.Where(m => m.ID == changeControlId).FirstOrDefault();
                        if (null != data)
                        {
                            szResult += " , " + getControlValueText(data);
                        }
                        else
                        {
                            //Not things.
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public string getShowConvertValue(int ExaType, decimal value)
        {
            string szResult = "-";

            try
            {
                if (ExaType == (int)EXA_TAG_TYPE.QUANTITY_PV)
                {
                    szResult = Convert.ToDouble(value).ToString();
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }


        public string getShowProofValue(int ExaType, decimal value)
        {
            string szResult = "-";

            try
            {
                if (ExaType == (int)EXA_TAG_TYPE.QUANTITY_PV)
                {
                    szResult = Convert.ToDouble(value).ToString();
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public string getControlValueText(ViewQMS_MA_CONTROL_DATA tempData)
        {
            return getControlValueTextEx(tempData.MIN_FLAG, tempData.MAX_FLAG, tempData.MIN_VALUE, tempData.MAX_VALUE, tempData.CONC_FLAG, tempData.LOADING_FLAG, tempData.CONC_VALUE, tempData.LOADING_VALUE);
        }

        public string getControlValueText(ViewQMS_MA_CONTROL_DATACONF tempData)
        {
            return getControlValueTextEx(tempData.MIN_FLAG.Value, tempData.MAX_FLAG.Value, tempData.MIN_VALUE.Value, tempData.MAX_VALUE.Value, tempData.CONC_FLAG.Value, tempData.LOADING_FLAG.Value, tempData.CONC_VALUE.Value, tempData.LOADING_VALUE.Value);
        }

        public string getControlValueTextEx(bool MIN_FLAG, bool MAX_FLAG, decimal MIN_VALUE, decimal MAX_VALUE, 
                                    bool CONC_FLAG, bool LOADING_FLAG, decimal CONC_VALUE, decimal LOADING_VALUE)
        {
            string CONTROL_DATA_NAME = ""; 

            if (MIN_FLAG == false && MAX_FLAG == false) //เป็นงาน conc/loadding
            { 
                if (CONC_FLAG == true)
                {
                    CONTROL_DATA_NAME = CONC_VALUE.ToString("0.#####") + " / ";
                }
                else
                {
                    CONTROL_DATA_NAME = "- / ";
                }

                if (LOADING_FLAG == true)
                {
                    CONTROL_DATA_NAME += LOADING_VALUE.ToString("0.#####");
                }
                else
                {
                    CONTROL_DATA_NAME += "-";
                }
            }
            else if (MIN_FLAG == false)
            { // ค่า max
                CONTROL_DATA_NAME = QMSSystem.CoreDB.Resource.ResourceString.MAXValue + " " + MAX_VALUE.ToString("0.#####");
            }
            else if (MAX_FLAG == false)
            { // ค่า min
                CONTROL_DATA_NAME = QMSSystem.CoreDB.Resource.ResourceString.MINValue + " " + MIN_VALUE.ToString("0.#####");
            }
            else //has max and min
            {
                CONTROL_DATA_NAME = MIN_VALUE.ToString("0.#####") + " - " + MAX_VALUE.ToString("0.#####");
            }  

            return CONTROL_DATA_NAME;
        }

        public List<ViewQMS_MA_PRODUCT> setArrowUPAndDown(List<ViewQMS_MA_PRODUCT> listResult)
        {
            
            if (listResult.Count() > 1)
            {
                long count = 1;
                long maxCount = listResult.Count();

                foreach (ViewQMS_MA_PRODUCT dt in listResult)
                {
                    if (count == 1)
                    {
                        dt.ARROW_UP = false;
                    }
                    else if (count == maxCount)
                    {
                        dt.ARROW_DOWN = false;
                    }
                    count++;
                }
            }
            else
            {
                foreach (ViewQMS_MA_PRODUCT dt in listResult)
                {
                    dt.ARROW_UP = false;
                    dt.ARROW_DOWN = false;
                }
            }

            return listResult;
        }

        public List<ViewQMS_MA_PLANT> setArrowUPAndDown(List<ViewQMS_MA_PLANT> listResult)
        {

            if (listResult.Count() > 1)
            {
                long count = 1;
                long maxCount = listResult.Count();

                foreach (ViewQMS_MA_PLANT dt in listResult)
                {
                    if (count == 1)
                    {
                        dt.ARROW_UP = false;
                    }
                    else if (count == maxCount)
                    {
                        dt.ARROW_DOWN = false;
                    }
                    count++;
                }
            }
            else
            {
                foreach (ViewQMS_MA_PLANT dt in listResult)
                {
                    dt.ARROW_UP = false;
                    dt.ARROW_DOWN = false;
                }
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> setArrowUPAndDown(List<ViewQMS_MA_EXQ_TAG> listResult)
        {

            if (listResult.Count() > 1)
            {
                long count = 1;
                long maxCount = listResult.Count();

                foreach (ViewQMS_MA_EXQ_TAG dt in listResult)
                {
                    if (count == 1)
                    {
                        dt.ARROW_UP = false;
                    }
                    else if (count == maxCount)
                    {
                        dt.ARROW_DOWN = false;
                    }
                    count++;
                }
            }
            else
            {
                foreach (ViewQMS_MA_EXQ_TAG dt in listResult)
                {
                    dt.ARROW_UP = false;
                    dt.ARROW_DOWN = false;
                }
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_GRAPH> setArrowUPAndDown(List<ViewQMS_RP_OFF_CONTROL_GRAPH> listResult)
        {

            if (listResult.Count() > 1)
            {
                long count = 1;
                long maxCount = listResult.Count();

                foreach (ViewQMS_RP_OFF_CONTROL_GRAPH dt in listResult)
                {
                    if (count == 1)
                    {
                        dt.ARROW_UP = false;
                    }
                    else if (count == maxCount)
                    {
                        dt.ARROW_DOWN = false;
                    }
                    count++;
                }
            }
            else
            {
                foreach (ViewQMS_RP_OFF_CONTROL_GRAPH dt in listResult)
                {
                    dt.ARROW_UP = false;
                    dt.ARROW_DOWN = false;
                }
            }

            return listResult;
        }


        public string getColumnNameFromFieldName(string fieldName)
        {
            int startIndex = fieldName.IndexOfAny("0123456789".ToCharArray());
            string column = fieldName.Substring(0, startIndex);
            int row = Int32.Parse(fieldName.Substring(startIndex));

            return column;
        }

        public int getRowsNumberFromFieldName(string fieldName)
        {
            int startIndex = fieldName.IndexOfAny("0123456789".ToCharArray());
            string column = fieldName.Substring(0, startIndex);
            int row = Int32.Parse(fieldName.Substring(startIndex));

            return row;
        }

        public decimal getConvertTextToDecimal(string textValue)
        {
            decimal decimalValue = 0;

            try
            {
                decimalValue = Decimal.Parse(textValue, NumberStyles.Any, CultureInfo.InvariantCulture); //Convert.ToDecimal(textValue);
            }
            catch (Exception ex)
            {

            } 
            return decimalValue;
        }

        public decimal? getConvertTextToDecimalEx(string textValue)
        {
            decimal? decimalValue = null;

            try
            {
                decimalValue = Decimal.Parse(textValue, NumberStyles.Any, CultureInfo.InvariantCulture); //Convert.ToDecimal(textValue);
            }
            catch (Exception ex)
            {

            }
            return decimalValue;
        } 

        public string getWeekOfDay(int year, int month, int day)
        {
            string dayOfWeek = "";

            try
            {
                DateTime tempDate = new DateTime(year, month, day);
                dayOfWeek = tempDate.ToString("ddd");

            }
            catch (Exception ex)
            {

            }

            return dayOfWeek;
        }

        public string getShowDate(int year, int month, int day)
        {
            string sResult = "";

            try
            {
                DateTime tempDate = new DateTime(year, month, day);
                sResult = tempDate.ToString("dd/MM/yyyy");

            }
            catch (Exception ex)
            {

            }

            return sResult;
        }

        public string getExaScheduleName(int id)
        {
            string szResult = "";
            try
            {
                if (id == (byte)EXA_SCHEDULE_STATUS.PENDING)
                {
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.PENDING; 
                }
                else if (id == (byte)EXA_SCHEDULE_STATUS.CONFIRM)
                {
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.CONFIRM; 
                }
                else if (id == (byte)EXA_SCHEDULE_STATUS.FAIL)
                {
                    szResult = QMSSystem.CoreDB.Resource.ResourceString.FAIL; 
                }
            }
            catch (Exception ex)
            {

            }
            return szResult;
        }

        protected string getColumnNameById(long id, List<ViewQMS_MA_EXQ_TAG> listExaTag)
        {
            string ColumnName = "";

            try
            {
                ColumnName = listExaTag.Where(m => m.ID == id).Select(m => m.EXCEL_NAME).FirstOrDefault();
            }
            catch
            {

            }

            return ColumnName;
        }

        protected long getSumTagById(List<ViewQMS_MA_EXQ_TAG> listExaTag)
        {
            long ColumnName = 0;

            try
            {
                ColumnName = listExaTag.Where(m => m.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_SUM ).Select(m => m.ID).FirstOrDefault();
            }
            catch
            {

            }

            return ColumnName;
        }

        protected long getPVTagById(List<ViewQMS_MA_EXQ_TAG> listExaTag)
        {
            long ColumnName = 0;

            try
            {
                ColumnName = listExaTag.Where(m => m.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV).Select(m => m.ID).FirstOrDefault();
            }
            catch
            {

            }

            return ColumnName;
        }

        protected long getPVTagById(List<QMS_MA_EXQ_TAG> listExaTag)
        {
            long ColumnName = 0;

            try
            {
                ColumnName = listExaTag.Where(m => m.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV).Select(m => m.ID).FirstOrDefault();
            }
            catch
            {

            }

            return ColumnName;
        }

        //protected bool IsFlowRateError(decimal flowValue, List<ViewQMS_MA_EXQ_TAG> listExaTag)
        //{
        //    bool bStatus = false;

        //    try
        //    {
        //        decimal checkflowValue = listExaTag.Where(m => m.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV  ).Select(m => m.TAG_FLOW_CONVERT_VALUE).FirstOrDefault();

        //        if (flowValue < checkflowValue)
        //        {
        //            bStatus = true;
        //        }
        //    }
        //    catch
        //    {

        //    }

        //    return bStatus;
        //}

        protected bool IsFlowRateError(decimal flowValue, List<ViewQMS_MA_EXQ_TAG> listExaTag, long id)
        {
            bool bStatus = false;

            try
            {
                decimal checkflowValue = listExaTag.Where(m => m.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV && m.ID == id ).Select(m => m.TAG_FLOW_CHECK_VALUE).FirstOrDefault();

                if (flowValue < checkflowValue)
                {
                    bStatus = true;
                }
            }
            catch
            {

            }

            return bStatus;
        }
         
        protected CheckOffControlSpecModel CheckOffControlSpec(decimal valueTag, List<ViewQMS_MA_CONTROL_DATA> listControlData, long controlSpecId)
        {
            CheckOffControlSpecModel bResult = new CheckOffControlSpecModel();
            bResult.OFF_STATUS = 0;

            try
            {
                ViewQMS_MA_CONTROL_DATA controlData = new ViewQMS_MA_CONTROL_DATA();
                controlData = listControlData.Where(m => m.ID == controlSpecId).FirstOrDefault();

                if (null != controlData)
                {
                    bResult.CONTROL_NAME = controlData.CONTROL_COLUMN_NAME;
                    bResult.CONTROL_VALUE = controlData.MAX_VALUE;
                    if (controlData.MAX_FLAG == true && valueTag > controlData.MAX_VALUE)
                    {
                        bResult.OFF_STATUS = (byte)OFF_CONTROL.OFF_HIGH;
                    }

                    if (controlData.MIN_FLAG == true && valueTag < controlData.MIN_VALUE)
                    {
                        bResult.OFF_STATUS = (byte)OFF_CONTROL.OFF_LOW;
                    }

                    //if(controlData.CONC_LOADING_FLAG มีการคิดค่า form ไหม
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public decimal  getDecimalFromDisplayValue(string dispalyValue, decimal enterValue , int decimalLenght)
        {
            decimal dResult = enterValue;

            try
            {
                dResult = decimal.Parse(Regex.Replace(dispalyValue, @"[^\d.]", ""));

            }
            catch
            {


            }

            dResult = TruncateToDecimalPlace((double)dResult, decimalLenght);

            return dResult;
        }

        
        public SpecControlValue convertStringToDecimal(string text)
        {
            SpecControlValue modelValue = new SpecControlValue();

            try
            {
                modelValue.SpecView = text;

                int pos = 0;
                text = text.ToLower();
                //check "Max"
                if (text.IndexOf("max") > -1)
                {
                    pos = text.IndexOf("max");

                    modelValue.bMaxValue = true;
                    modelValue.MaxValue = getConvertTextToDecimal(text.Substring(0, pos));// Convert.ToDecimal(text.Substring(0, pos));

                }
                else if (text.IndexOf("min") > -1)
                {
                    pos = text.IndexOf("min");
                    modelValue.bMinValue = true;
                    modelValue.MinValue = getConvertTextToDecimal(text.Substring(0, pos));// Convert.ToDecimal(text.Substring(0, pos));
                }
                else if (text.IndexOf("-") > -1)
                {
                    pos = text.IndexOf("-");
                    int posLen = text.Length;
                    posLen = posLen - (pos + 1);


                    modelValue.bMaxValue = true;
                    modelValue.bMinValue = true;
                    modelValue.MaxValue = getConvertTextToDecimal(text.Substring(pos + 1, posLen));//  Convert.ToDecimal(text.Substring(pos + 1, posLen));
                    modelValue.MinValue = getConvertTextToDecimal(text.Substring(0, pos));// Convert.ToDecimal(text.Substring(0, pos));
                } 

                //check "Min"

                //check "00-00"

            }catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return modelValue;
        }

        public decimal TruncateToDecimalPlace(double numberToTruncate, int decimalPlaces)
        {
            decimal power = (decimal)(Math.Pow(10.0, (double)decimalPlaces));
            
            return Math.Truncate((power * (decimal)numberToTruncate)) / power;
        }

        public decimal getRangeDecimalPlace(double numberToTruncate, int decimalPlaces)
        {

            decimal power = (decimal)(Math.Pow(10.0, (double)decimalPlaces));

            return Math.Truncate( (decimal)numberToTruncate) / power;
        }

        public int getDecimalLenght(string szNum)
        {
            int nResult = 0;

            try
            {

                bool start = false;
                foreach (var s in szNum)
                {
                    if (s == '.')
                    {
                        start = true;
                    }
                    else if (start)
                    {
                        nResult++;
                    }
                }


            }
            catch (Exception ex)
            {

            }

            return nResult;
        }

        public decimal? covertTextToDecimal(string text )
        {
            decimal? dResult = null;

            try
            {
                dResult = getConvertTextToDecimal(text);// Convert.ToDecimal(text);
            }
            catch (Exception ex)
            {
                
            }

            return dResult;

        }

        public decimal? covertTextToDecimal(string text, decimal enterValue)
        {
            decimal? dResult = enterValue;

            try
            {
                dResult = getConvertTextToDecimal(text);// Convert.ToDecimal(text);
            }
            catch (Exception ex)
            {
                dResult = enterValue; //ถ้าแปลงค่าไม่ได้ ให้ใช้ enter value
            }

            return dResult;

        }

        public int getStartRow(DateTime startSearch, DateTime startData, int offset)
        {
            int bResult = 0;
            try
            {
                if (startSearch >= startData)
                {
                    bResult = offset + 1; //เท่ากับต่ำแหน่งแรก
                }
                else
                {
                    int diff = ((int)(startData - startSearch).TotalMinutes / 5);
                    bResult = offset + diff + 1; //ชดเชย ตัวเอง 
                }

            }
            catch
            {

            }

            return bResult;
        }

        public int getEndRowRecords(double start, double end)
        {
            int bResult = 0;
            try
            {
                double diff = 0;
                if (end > start)
                {
                    diff = end - start;
                }
                else
                {
                    diff = start - end;
                }

                bResult = Convert.ToInt32(diff); //นับตัวเองด้วย 

            }
            catch
            {

            }

            return bResult;
        }

        public int getEndRow(DateTime startData, DateTime endSearch, DateTime endtData, int offset)
        {
            int bResult = 0;
            try
            {
                if (endSearch >= endtData)
                {
                    int diff = ((int)(endtData - startData).TotalMinutes / 5);
                    bResult = diff + offset + 1;   //ชดเชย ตัวเอง
                }
                else
                {
                    int diff = ((int)(endSearch - startData).TotalMinutes / 5);
                    bResult = diff + offset + 1; //ชดเชย ตัวเอง
                } 
            }
            catch
            {

            }

            return bResult;
        }

        public int getConvertDoubleToInt(double data)
        {
            int bResult = 0;
            try
            {
                bResult = Convert.ToInt32(data);

            }
            catch
            {

            }

            return bResult;
        }

        public string[] getTagConvertData(string TAG_GCERROR)
        {
            string[] szArrayResult = new string[720];
            try
            {
                string[] tempArray =  TAG_GCERROR.Split(',').ToArray();
                if (tempArray.Count() == 1 && TAG_GCERROR == "NULL") //cannot read data
                {
                }
                else
                {
                    for (int i = 0; i < tempArray.Count(); i++)
                    {
                        szArrayResult[i] = tempArray[i];
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return szArrayResult;
        }

        public DateTime? getDateTimeFromFileName(string fileName, string m_dtFile)
        {
            DateTime? dtResult = null;
            try
            {
                int targetLength = fileName.Length;
                int fileLenght = m_dtFile.Length;
                string fileDate = m_dtFile.Substring(targetLength, 10); //ดึง เฉพาะวัน ที่ 00-00-0000 10 ตัวพอดี

                //กำหนด folder year ให้เป็น คศ วันที่ 1 ของเดือนนั้นๆ
                dtResult = DateTime.ParseExact(fileDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
       
            }
            catch (Exception ex)
            {

            }

            return dtResult;
        }
       

        //protected bool IsOffControlError(decimal compareValue,long id,  List<ViewQMS_MA_EXQ_TAG> listExaTag, int flagControl)
        //{
        //    bool bStatus = false;

        //    try
        //    {
        //        ViewQMS_MA_EXQ_TAG ExaTag = listExaTag.Where( m => m.ID == id).FirstOrDefault();

        //        if (flagControl == (int)OFF_TYPE.CONTROL)
        //        {
        //            ExaTag.CONTROL_VALUE == CheckOffControlSpec
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch
        //    {

        //    }

        //    return bStatus;
        //}
 
    }

    

    public static class ArrayExtention
    {

        public static T[] ConcatenateArray<T>(this T[] array1, T[] array2)
        {
            T[] result = new T[array1.Length + array2.Length];
            array1.CopyTo(result, 0);
            array2.CopyTo(result, array1.Length);
            return result;
        } 

    }





}
