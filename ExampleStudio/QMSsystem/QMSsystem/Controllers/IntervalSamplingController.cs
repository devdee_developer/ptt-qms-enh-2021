﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_IntervalSampling }, DataAccess = DataAccessEnum.Authen)]
    public class IntervalSamplingController : BaseController
    {
        //
        // GET: /IntervalSampling/
        public ActionResult ListMasterIntervalSampling()//control ListMasterIntervalSampling.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IntervalSamplingSearchModel searchIntervalSampling = new IntervalSamplingSearchModel();
            CreateQMS_MA_CPK_INTERVAL_SAMPLING CreateQMS_MA_CPK_INTERVAL_SAMPLING = new CreateQMS_MA_CPK_INTERVAL_SAMPLING();
            //CreateQMS_MA_CPK_INTERVAL_SAMPLING.ACTIVE_DATE = getDummyDateTime();


            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

            ViewBag.searchIntervalSampling = searchIntervalSampling;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.CreateQMS_MA_CPK_INTERVAL_SAMPLING = CreateQMS_MA_CPK_INTERVAL_SAMPLING;

            return View();
        }

        public ActionResult CreateMasterIntervalSampling(long id = 0)//control CreateMasterIntervalSampling.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CPK_INTERVAL_SAMPLING CreateQMS_MA_CPK_INTERVAL_SAMPLING = new CreateQMS_MA_CPK_INTERVAL_SAMPLING();
            string actionName = Resources.Strings.AddNew;

            //createQMS_MA_CPK_INTERVAL_SAMPLING.ACTIVE_DATE = getDummyDateTime();

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                CreateQMS_MA_CPK_INTERVAL_SAMPLING = masterDataServices.getQMS_MA_CPK_INTERVAL_SAMPLINGById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

            ViewBag.CreateQMS_MA_CPK_INTERVAL_SAMPLING = CreateQMS_MA_CPK_INTERVAL_SAMPLING;

            ViewBag.ActionName = actionName;
            return View();
        }


        public JsonResult GetQMS_MA_CPK_INTERVAL_SAMPLINGBySearch(IntervalSamplingSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CPK_INTERVAL_SAMPLING> listData = new List<ViewQMS_MA_CPK_INTERVAL_SAMPLING>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_CPK_INTERVAL_SAMPLING(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteQMS_MA_CPK_INTERVAL_SAMPLING(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_CPK_INTERVAL_SAMPLINGByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_CPK_INTERVAL_SAMPLING(CreateQMS_MA_CPK_INTERVAL_SAMPLING model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_CPK_INTERVAL_SAMPLING(_db, model);

                    if (nResult > 0)
                    {
                        model = masterDataServices.getQMS_MA_CPK_INTERVAL_SAMPLINGById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_IntervalSampling, PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home }, DataAccess = DataAccessEnum.Authen)]
        public JsonResult GetComposationbyplanIdproIdControlValue(long PlanId, long ProductId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING> listData = masterDataServices.GetComposationbyplanIdproIdControlValue(_db, PlanId, ProductId);
                    //listData = listData.Where(x => x.SAMPLE_NAME != "").ToList();
                    //listData = masterDataServices.setMappingSortingSample(_db, listData, (byte)TemplateType, "");
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }








    }
}
