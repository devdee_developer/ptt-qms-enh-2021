﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_RawDataExport }, DataAccess = DataAccessEnum.Authen)]
    public class RawDataController : BaseController
    {
        //
        // GET: /Manage/

        public ActionResult ListRawDataExport()//control ListRawDataExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            RawDataSearchModel searchRawData = new RawDataSearchModel();
            CreateQMS_TR_RAW_DATA createQMS_TR_RAW_DATA = new CreateQMS_TR_RAW_DATA();
            createQMS_TR_RAW_DATA.DATE_START = getDummyDateTime();
            createQMS_TR_RAW_DATA.DATE_END = getDummyDateTime();

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
       
            ViewBag.searchRawData = searchRawData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_TR_RAW_DATA = createQMS_TR_RAW_DATA;
            return View();
        }

        public ActionResult CreateRawDataExport(long id = 0)//control CresteRawDataExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_RAW_DATA createQMS_TR_RAW_DATA = new CreateQMS_TR_RAW_DATA();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_RAW_DATA.DATE_START = getDummyDateTime();
            createQMS_TR_RAW_DATA.DATE_END = getDummyDateTime();

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_RAW_DATA = transactionServices.getQMS_TR_RAW_DATAById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            ViewBag.createQMS_TR_RAW_DATA = createQMS_TR_RAW_DATA;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_RAW_DATA(CreateQMS_TR_RAW_DATA model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.DATE_START = resetSecond(model.DATE_START);
                    model.DATE_END = resetSecond(model.DATE_END);
                    long nResult = transactionServices.SaveQMS_TR_RAW_DATA(_db, model);

                    if (nResult > 0)
                    {
                        model = transactionServices.getQMS_TR_RAW_DATAById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_RAW_DATABySearch(RawDataSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_RAW_DATA> listData = new List<ViewQMS_TR_RAW_DATA>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_RAW_DATA(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_RAW_DATA(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_RAW_DATAByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public void RawDataExcel(ProductExaDataSearchModel searchModel)
        {
            TransactionService transDataServices = new TransactionService(_username); 
            ExcelPackage excel = transDataServices.ProductExaDataExcelByPlantId(_db, searchModel);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.RawDataExport + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }
    }
}
