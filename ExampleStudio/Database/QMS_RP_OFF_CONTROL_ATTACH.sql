USE [QMSSystem]
GO
/****** Object:  Table [dbo].[QMS_RP_OFF_CONTROL_ATTACH]    Script Date: 1/2/2560 9:29:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QMS_RP_OFF_CONTROL_ATTACH](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[RP_OFF_CONTROL_ID] [bigint] NOT NULL,
	[NAME] [nvarchar](200) NULL,
	[PATH] [nvarchar](2048) NOT NULL,
	[DELETE_FLAG] [tinyint] NOT NULL,
	[CREATE_USER] [nvarchar](50) NOT NULL,
	[CREATE_DATE] [datetime] NOT NULL,
	[UPDATE_USER] [nvarchar](50) NOT NULL,
	[UPDATE_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_QMS_RP_OFF_CONTROL_ATTACH] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[QMS_RP_OFF_CONTROL_ATTACH] ADD  CONSTRAINT [DF_QMS_RP_OFF_CONTROL_ATTACH_DELETE_FLAG]  DEFAULT ((0)) FOR [DELETE_FLAG]
GO
ALTER TABLE [dbo].[QMS_RP_OFF_CONTROL_ATTACH] ADD  CONSTRAINT [DF_QMS_RP_OFF_CONTROL_ATTACH_CREATE_DATE]  DEFAULT (getdate()) FOR [CREATE_DATE]
GO
ALTER TABLE [dbo].[QMS_RP_OFF_CONTROL_ATTACH] ADD  CONSTRAINT [DF_QMS_RP_OFF_CONTROL_ATTACH_UPDATE_DATE]  DEFAULT (getdate()) FOR [UPDATE_DATE]
GO
ALTER TABLE [dbo].[QMS_RP_OFF_CONTROL_ATTACH]  WITH CHECK ADD  CONSTRAINT [FK_QMS_RP_OFF_CONTROL_ATTACH_QMS_RP_OFF_CONTROL] FOREIGN KEY([RP_OFF_CONTROL_ID])
REFERENCES [dbo].[QMS_RP_OFF_CONTROL] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[QMS_RP_OFF_CONTROL_ATTACH] CHECK CONSTRAINT [FK_QMS_RP_OFF_CONTROL_ATTACH_QMS_RP_OFF_CONTROL]
GO
