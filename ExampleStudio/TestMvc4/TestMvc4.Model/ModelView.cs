﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TestMvc4.Model
{

    public class Search
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
    }


    public class SearchSTUDENT
    {
        public long STUDENT_ID { get; set; }
        public string STUDENT_NAME { get; set; }
        public string STUDENT_SNAME { get; set; }
    }


    public class STUDENTSearch : Search
    {
        public SearchSTUDENT mSearch { get; set; }
    }


    public class STUDENTView
    {
        public long STUDENT_ID { get; set; }
        public string STUDENT_NAME { get; set; }
        public string STUDENT_SNAME { get; set; }
        public string STUDENT_YEAR_NAME { get; set; }
    }


}
