﻿using System.Web;
using System.Web.Optimization;

namespace QMSsystem
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.js",
                        "~/Scripts/bootstrap.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(                
            //            "~/Scripts/fastclick.js",
            //            "~/Scripts/nprogress.js",
            //            "~/Scripts/bootstrap-progressbar.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/chosen.jquery.js",
                      "~/Scripts/angular-animate.js",
                      "~/Scripts/angular-sanitize.js",
                      "~/Scripts/angular-strap.js",
                      "~/Scripts/angular-strap.tpl.js",
                      "~/Scripts/angular.app.utility.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/chart").Include(
                      /*"~/Scripts/Chart.bundle.js",*/
                      "~/Scripts/Chart.js",
                      "~/Scripts/angular-chart.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/uploadfile").Include(
                      "~/Scripts/ng-file-upload-shim.js",
                      "~/Scripts/ng-file-upload.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/graph").Include(
                        "~/Scripts/Chart.js", 
                        "~/Scripts/jquery.sparkline.js",
                        "~/Scripts/raphael.js",
                        "~/Scripts/morris.js",
                        "~/Scripts/Flot/jquery.flot.js",
                        "~/Scripts/Flot/jquery.flot.pie.js",
                        "~/Scripts/Flot/jquery.flot.time.js",
                        "~/Scripts/Flot/jquery.flot.js",
                        "~/Scripts/Flot/jquery.flot.stack.js",
                        "~/Scripts/Flot/jquery.flot.resize.js"
                        ));
            
            bundles.Add(new ScriptBundle("~/bundles/custome").Include( 
                        "~/Scripts/site.js"));


            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                        "~/Scripts/moment.js",
                        "~/Scripts/jquery-ui-1.12.1.js"
                        //"~/Scripts/daterangepicker.js"
                        ));
             
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/loginlayout").Include("~/Content/loginlayout.css"));
            bundles.Add(new StyleBundle("~/Content/themes2").Include( "~/Content/bootstrap.css" , "~/Content/angular-strap.css"));

            bundles.Add(new StyleBundle("~/Content/themes").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/angular-strap.css",
                        "~/Content/font-awesome/css/font-awesome.css"));

            bundles.Add(new ScriptBundle("~/Content/datepicker").Include(
                        //"~/Content/themes/base/datepicker.css",    
                        //"~/Content/daterangepicker.css",
                         "~/Content/chosen.css"
                        ));

        }
    }
}