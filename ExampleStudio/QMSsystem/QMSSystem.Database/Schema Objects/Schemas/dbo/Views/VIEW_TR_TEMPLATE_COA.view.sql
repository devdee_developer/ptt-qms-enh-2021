﻿CREATE VIEW [dbo].[VIEW_TR_TEMPLATE_COA]
AS
SELECT        dbo.QMS_TR_TEMPLATE_COA.CUSTOMER_NAME, dbo.QMS_TR_TEMPLATE_COA.PRODUCT_NAME, dbo.QMS_TR_TEMPLATE_COA_DETAIL.ITEM_NAME, dbo.QMS_TR_TEMPLATE_COA_DETAIL.UNIT_NAME, 
                         dbo.QMS_TR_TEMPLATE_COA_DETAIL.USER_SEQUENCE, dbo.QMS_TR_TEMPLATE_COA_DETAIL.TEST_METHOD, dbo.QMS_TR_TEMPLATE_COA_DETAIL.SPECIFICATION, dbo.GetControlView(contrl.MAX_FLAG, 
                         contrl.MIN_FLAG, contrl.MAX_VALUE, contrl.MIN_VALUE, contrl.CONC_FLAG, contrl.LOADING_FLAG, contrl.CONC_VALUE, contrl.LOADING_VALUE) AS ControlView
FROM            dbo.QMS_TR_TEMPLATE_COA INNER JOIN
                         dbo.QMS_TR_TEMPLATE_COA_DETAIL ON dbo.QMS_TR_TEMPLATE_COA.CUSTOMER_NAME = dbo.QMS_TR_TEMPLATE_COA_DETAIL.CUSTOMER_NAME AND 
                         dbo.QMS_TR_TEMPLATE_COA.PRODUCT_NAME = dbo.QMS_TR_TEMPLATE_COA_DETAIL.PRODUCT_NAME LEFT OUTER JOIN
                         dbo.QMS_MA_CONTROL_DATA AS contrl ON dbo.QMS_TR_TEMPLATE_COA_DETAIL.CONTROL_ID = contrl.ID
WHERE        (dbo.QMS_TR_TEMPLATE_COA.TREND_GRAPH_FLAG = 1) AND (dbo.QMS_TR_TEMPLATE_COA.DELETE_FLAG = 0) AND (dbo.QMS_TR_TEMPLATE_COA_DETAIL.DELETE_FLAG = 0) AND 
                         (dbo.QMS_TR_TEMPLATE_COA_DETAIL.TREND_GRAPH_FLAG = 1)


GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "QMS_TR_TEMPLATE_COA"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 241
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "QMS_TR_TEMPLATE_COA_DETAIL"
            Begin Extent = 
               Top = 6
               Left = 279
               Bottom = 136
               Right = 482
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "contrl"
            Begin Extent = 
               Top = 6
               Left = 520
               Bottom = 136
               Right = 736
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_TR_TEMPLATE_COA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_TR_TEMPLATE_COA'

