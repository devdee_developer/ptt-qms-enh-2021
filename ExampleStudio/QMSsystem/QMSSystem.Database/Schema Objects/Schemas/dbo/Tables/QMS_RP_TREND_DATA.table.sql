﻿CREATE TABLE [dbo].[QMS_RP_TREND_DATA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[TREND_TYPE] [tinyint] NOT NULL,
	[TREND_DESC] [nvarchar](2048) NOT NULL,
	[START_DATE] [datetime] NOT NULL,
	[END_DATE] [datetime] NOT NULL,
	[DELETE_FLAG] [tinyint] NOT NULL,
	[CREATE_USER] [nvarchar](50) NOT NULL,
	[CREATE_DATE] [datetime] NOT NULL,
	[UPDATE_USER] [nvarchar](50) NOT NULL,
	[UPDATE_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_QMS_RP_TREND_DATA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


