﻿
USE [master]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PTT-GSPQualityManagementSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET ANSI_NULLS OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET ANSI_PADDING OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET ARITHABORT OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET  DISABLE_BROKER
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET RECOVERY FULL
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET  MULTI_USER
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET DB_CHAINING OFF
GO
USE [PTT-GSPQualityManagementSystem]
GO
/****** Object:  UserDefinedFunction [dbo].[GetControlView]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_CONTROL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_CONTROL_COLUMN]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_CONTROL_DATA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_CONTROL_ROW]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_CORRECT_DATA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_DOWNTIME]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_DOWNTIME_DETAIL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_EMAIL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_EXQ_ACCUM_TAG]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_EXQ_TAG]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_GRADE]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_KPI]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_OPERATION_SHIFT]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_PLANT]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_PRODUCT]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_PRODUCT_MAPPING]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_REDUCE_FEED]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_REDUCE_FEED_DETAIL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_ROOT_CAUSE]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_ROOT_CAUSE_TYPE]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_TEMPLATE]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_TEMPLATE_COLUMN]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_TEMPLATE_CONTROL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_TEMPLATE_ROW]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_MA_UNIT]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_RP_OFF_CONTROL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_RP_OFF_CONTROL_ATTACH]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_RP_OFF_CONTROL_DETAIL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_RP_OFF_CONTROL_GRAPH]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_RP_OFF_CONTROL_SUMMARY]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_RP_TREND_DATA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_RP_TREND_DATA_DETAIL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_ST_AUTHORIZATION]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_ST_EXAQUATUM_SCHEDULE]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_ST_EXPORT_PREFIX]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_ST_FILE_ATTACH]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_ST_SYSTEM_CONFIG]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_ST_SYSTEM_LOG]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_ST_SYSTEM_USER_STAMP]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_ABNORMAL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_CHANGE_GRADE]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_COA_DATA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_CROSS_VOLUME]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_DOWNTIME]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_EXCEPTION]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_EXQ_DOWNTIME]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_EXQ_PRODUCT]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_EXQ_REDUCE_FEED]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_FILE_TREND_DATA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_OFF_CONTROL_CAL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_RAW_DATA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_REDUCE_FEED]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_TEMPLATE_COA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_TEMPLATE_COA_DETAIL]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_TREND_DATA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[QMS_TR_TURN_AROUND]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  View [dbo].[VIEW_TR_DATA_COA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  View [dbo].[VIEW_TR_TEMPLATE_COA]    Script Date: 8/25/2018 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
USE [master]
GO
ALTER DATABASE [PTT-GSPQualityManagementSystem] SET  READ_WRITE
GO
--Syntax Error: Incorrect syntax near CONTAINMENT.
--/****** Object:  Database [PTT-GSPQualityManagementSystem]    Script Date: 8/25/2018 10:10:30 AM ******/
--CREATE DATABASE [PTT-GSPQualityManagementSystem]
-- CONTAINMENT = NONE
-- ON  PRIMARY 
--( NAME = N'PTT-GSPQualityManagementSystem_test', FILENAME = N'S:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Data\PTT-GSPQualityManagementSystem.mdf' , SIZE = 2509824KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
-- LOG ON 
--( NAME = N'PTT-GSPQualityManagementSystem_test_log', FILENAME = N'T:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Data\PTT-GSPQualityManagementSystem_log.ldf' , SIZE = 285120KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)

GO
--Syntax Error: Incorrect syntax near COMPATIBILITY_LEVEL.
--ALTER DATABASE [PTT-GSPQualityManagementSystem] SET COMPATIBILITY_LEVEL = 110

GO
--Syntax Error: Incorrect syntax near HONOR_BROKER_PRIORITY.
--ALTER DATABASE [PTT-GSPQualityManagementSystem] SET HONOR_BROKER_PRIORITY OFF 

GO
--Syntax Error: Incorrect syntax near FILESTREAM.
--ALTER DATABASE [PTT-GSPQualityManagementSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 

GO
--Syntax Error: Incorrect syntax near TARGET_RECOVERY_TIME.
--ALTER DATABASE [PTT-GSPQualityManagementSystem] SET TARGET_RECOVERY_TIME = 0 SECONDS 

GO
--Syntax Error: Incorrect syntax near ADD.
--ALTER ROLE [db_datareader] ADD MEMBER [pttgspread]

GO
--Syntax Error: Incorrect syntax near ADD.
--ALTER ROLE [db_owner] ADD MEMBER [pttgspqtstusr]

GO
--Syntax Error: Incorrect syntax near ADD.
--ALTER ROLE [db_owner] ADD MEMBER [pttgspqprdusr]

GO
--Syntax Error: Incorrect syntax near +.
--Syntax Error: Incorrect syntax near +.
--AS BEGIN
--    DECLARE @CONTROL_DATA_NAME VARCHAR(250)
--
--    SET @CONTROL_DATA_NAME = ''
--
--    IF (@MIN_FLAG = 0 and @MAX_FLAG = 0)  
--		BEGIN
--        IF (@CONC_FLAG = 1) 
--            SET @CONTROL_DATA_NAME =  convert(varchar,convert(decimal(8,2),@CONC_VALUE)) + ' / '; 
--        ELSE 
--            SET @CONTROL_DATA_NAME = '- / '; 
--
--        IF (@LOADING_FLAG = 1) 
--            SET @CONTROL_DATA_NAME +=  convert(varchar,convert(decimal(8,2),@LOADING_VALUE)); 
--        ELSE 
--            SET @CONTROL_DATA_NAME += '-';
--		END
--    ELSE IF (@MIN_FLAG = 0) 
--		SET @CONTROL_DATA_NAME = 'MAX ' + convert(varchar,convert(decimal(8,2),@MAX_VALUE));
--    ELSE IF (@MAX_FLAG = 0) 
--        SET @CONTROL_DATA_NAME = 'MIN ' + convert(varchar,convert(decimal(8,2),@MIN_VALUE));
--    ELSE   
--        SET @CONTROL_DATA_NAME = convert(varchar,convert(decimal(8,2),@MIN_VALUE))  + ' - ' + convert(varchar,convert(decimal(8,2),@MAX_VALUE)) ;
--
--    RETURN @CONTROL_DATA_NAME
--END



GO
