USE [QMSSystem]
GO
/****** Object:  Table [dbo].[QMS_TR_EXQ_DOWNTIME]    Script Date: 1/27/2017 10:12:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QMS_TR_EXQ_DOWNTIME](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PLANT_ID] [bigint] NOT NULL,
	[TAG_DATE] [datetime] NOT NULL,
	[TAG_EXA_ID] [bigint] NOT NULL,
	[TAG_VALUE] [text] NOT NULL,
	[TAG_CONVERT] [text] NOT NULL,
	[TAG_GCERROR] [text] NOT NULL,
	[STATUS_1] [tinyint] NOT NULL,
	[STATUS_2] [tinyint] NOT NULL,
 CONSTRAINT [PK_QMS_TR_EXQ_DOWNTIME] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QMS_TR_EXQ_PRODUCT]    Script Date: 1/27/2017 10:12:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QMS_TR_EXQ_PRODUCT](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PLANT_ID] [bigint] NOT NULL,
	[PRODUCT_ID] [bigint] NOT NULL,
	[TAG_DATE] [datetime] NOT NULL,
	[TAG_EXA_ID] [bigint] NOT NULL,
	[TAG_VALUE] [text] NOT NULL,
	[TAG_CONVERT] [text] NOT NULL,
	[TAG_GCERROR] [text] NOT NULL,
	[STATUS_1] [tinyint] NOT NULL,
	[STATUS_2] [tinyint] NOT NULL,
 CONSTRAINT [PK_QMS_TR_EXQ_PRODUCT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QMS_TR_EXQ_REDUCE_FEED]    Script Date: 1/27/2017 10:12:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QMS_TR_EXQ_REDUCE_FEED](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PLANT_ID] [bigint] NOT NULL,
	[TAG_DATE] [datetime] NOT NULL,
	[TAG_EXA_ID] [bigint] NOT NULL,
	[TAG_VALUE] [text] NOT NULL,
	[TAG_CONVERT] [text] NOT NULL,
	[TAG_GCERROR] [text] NOT NULL,
	[STATUS_1] [tinyint] NOT NULL,
	[STATUS_2] [tinyint] NOT NULL,
 CONSTRAINT [PK_QMS_TR_EXQ_REDUCE_FEED] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
