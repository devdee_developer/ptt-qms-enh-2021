﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Oracle.DataAccess.Client;
using System.Data;
using QMSSystem.Model;
using System.Globalization;
using System.IO;
using OfficeOpenXml;
using QMSSystem.CoreDB.Helper;
using Oracle.ManagedDataAccess.Client;
using Newtonsoft.Json;
//using System.Data.OracleClient;

namespace QMSSystem.CoreDB.Services
{
    public class COAServices : BaseService
    {
        public string _errMsg;
        public long _result;
        public string _currentUserName;

        private QMSDBEntities _db;

        private static OracleCommand cmd;
        private static OracleDataAdapter da;
        private static DataSet ds;

        private static CreateQMS_ST_LIMS_CONNECT configLIMS;

        public COAServices()
        {
            _errMsg = "";
            _result = -1;

            configLIMS = null;
        }

        public COAServices(QMSDBEntities db, string userName)
        {
            _db = db;
            _errMsg = "";
            _result = -1;
            _currentUserName = userName;

            ConfigServices cogService = new ConfigServices(userName); 
            configLIMS = cogService.getLIMS_CONNECTION(_db);
        }

        public void testConnect(){

            OracleConnection conn = null;
            if (configLIMS != null)
            {
                conn = DBUtils.GetDBConnectionEx(configLIMS.LIMS_SERVER, configLIMS.LIMS_PORT, configLIMS.LIMS_SID, configLIMS.LIMS_USER, configLIMS.LIMS_PASSWORD);
            }
            else
            {
                conn = DBUtils.GetDBConnection();
            }
            conn.Open();
            string SQL = DBUtils.getViewDataSQL2();
            cmd = new OracleCommand(SQL, conn);
            OracleDataReader reader = cmd.ExecuteReader();
            try
            {
                
                while (reader.Read())
                {
                    Console.Write(reader.GetValue(0) + "::::");
                    Console.Write(reader.GetValue(1) + "::::");
                    Console.Write(reader.GetValue(2) + "::::");
                    Console.Write(reader.GetValue(3) + "::::");
                    Console.Write(reader.GetValue(4) + "::::");
                    Console.Write(reader.GetValue(5) + "::::");
                    Console.Write(reader.GetValue(6) + "::::");
                    Console.Write(reader.GetValue(7) + "::::");
                    Console.Write(reader.GetValue(8) + "::::");
                }
            }
            finally
            {
                reader.Close();
            }
        }

        public DateTime convertDatetimeData(string dateData)
        {
            DateTime dTime = DateTime.Now;
            try
            {
                dTime = Convert.ToDateTime(dateData);
            }
            catch
            {

            }

            return dTime;
        }

        public decimal convertToDecimal(string dateData)
        {
            decimal dDecimal = 0;
            try
            {
                dDecimal = getConvertTextToDecimal(dateData);//Convert.ToDecimal(dateData);
            }
            catch
            {

            }

            return dDecimal;
        }

        public int convertToInt(string dateData)
        {
            int dDecimal = 0;
            try
            {
                dDecimal = int.Parse(dateData, NumberStyles.Any, CultureInfo.InvariantCulture);
            }
            catch
            {

            }

            return dDecimal;
        }

        public List<V_TEMPLATE_MASTER> getV_TEMPLATE_MASTERListByDateTime(DateTime startTime, DateTime endTime)
        {
            List<V_TEMPLATE_MASTER> listResult = new List<V_TEMPLATE_MASTER>();

            try
            {
                string startDate = startTime.ToString("dd-MM-yyyy", new CultureInfo("en-US"));
                string endDate = endTime.ToString("dd-MM-yyyy", new CultureInfo("en-US"));

                LogHelper.WriteErrorLog(string.Format(" getV_TEMPLATE_MASTERListByDateTime startTime   : {0}  endTime : {1} ", startTime.ToString("dd-MM-yyyy HH:mm:ss"), endTime.ToString("dd-MM-yyyy HH:mm:ss")));
                OracleConnection conn = null;
                if (configLIMS != null)
                {
                    conn = DBUtils.GetDBConnectionEx(configLIMS.LIMS_SERVER, configLIMS.LIMS_PORT, configLIMS.LIMS_SID, configLIMS.LIMS_USER, configLIMS.LIMS_PASSWORD);
                }
                else
                {
                    conn = DBUtils.GetDBConnection();
                }
                conn.Open();

                string SQL = DBUtils.getViewDataSQLMaster(startDate, endDate);
                cmd = new OracleCommand(SQL, conn);
                OracleDataReader reader = cmd.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        V_TEMPLATE_MASTER tempResult = new V_TEMPLATE_MASTER();

                        tempResult.ID = reader["ID"].ToString();
                        tempResult.CUSTOMERNAME = reader["CUSTOMERNAME"].ToString();
                        tempResult.PRODUCT = reader["PRODUCT"].ToString();
                        tempResult.CREATEDT = convertDatetimeData(reader["CREATEDT"].ToString());
                         
                        listResult.Add(tempResult);
                    }
                }
                finally
                {
                    reader.Close();
                } 
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(string.Format("getV_TEMPLATE_MASTERListByDateTime  : {0}  ", ex.Message));
                if (ex.InnerException != null)
                {
                    this.writeErrorLog("inner message :" + ex.InnerException.Message);
                }
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }
         
        public List<V_TEMPLATE_DETAIL> getV_TEMPLATE_DETAILListByDateTime(DateTime startTime, DateTime endTime)
        {
            List<V_TEMPLATE_DETAIL> listResult = new List<V_TEMPLATE_DETAIL>();
           

            try
            {
                string startDate = startTime.ToString("dd-MM-yyyy", new CultureInfo("en-US"));
                string endDate = endTime.ToString("dd-MM-yyyy", new CultureInfo("en-US"));
                OracleConnection conn = null;

                LogHelper.WriteErrorLog(string.Format(" getV_TEMPLATE_DETAILListByDateTime startTime   : {0}  endTime : {1} ", startTime.ToString("dd-MM-yyyy HH:mm:ss"), endTime.ToString("dd-MM-yyyy HH:mm:ss")));
                 
                if (configLIMS != null)
                {
                    conn = DBUtils.GetDBConnectionEx(configLIMS.LIMS_SERVER, configLIMS.LIMS_PORT, configLIMS.LIMS_SID, configLIMS.LIMS_USER, configLIMS.LIMS_PASSWORD ); 
                }
                else
                {
                    conn = DBUtils.GetDBConnection();
                }

                
                conn.Open();

                string SQL = DBUtils.getViewDataSQLDetail(startDate, endDate);
                cmd = new OracleCommand(SQL, conn);
                OracleDataReader reader = cmd.ExecuteReader();
                 
                try
                {

                    while (reader.Read())
                    {
                        V_TEMPLATE_DETAIL tempResult = new V_TEMPLATE_DETAIL();

                        tempResult.ID = reader["ID"].ToString();
                        tempResult.SAMPLINGDATE = convertDatetimeData(reader["SAMPLINGDATE"].ToString());
                        tempResult.SAMPLINGNAME = reader["SAMPLINGNAME"].ToString();
                        tempResult.SAMPLINGPOINT = reader["SAMPLINGPOINT"].ToString();
                        tempResult.CUSTOMERNAME = mappingCustomer(reader["CUSTOMERNAME"].ToString());

                        tempResult.PRODUCT_NAME = reader["PRODUCTNAME"].ToString();
                        tempResult.TANK_NAME = mappingCustomer(reader["TANKNAME"].ToString());
                        tempResult.ITEM = reader["ITEM"].ToString();
                        tempResult.UNIT = reader["UNIT"].ToString();

                        tempResult.USER_SEQUENCE = convertToInt(reader["USERSEQUENCE"].ToString());
                        tempResult.TEST_METHOD = reader["TESTMETHOD"].ToString();
                        tempResult.SPECIFICATION = reader["SPECIFICATION"].ToString();
                        tempResult.DISPLAYVALUE = reader["DISPLAYVALUE"].ToString();
                        tempResult.ENTERVALUE = convertToDecimal(reader["ENTEREDVALUE"].ToString());
                          
                        
                        listResult.Add(tempResult);
  
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(string.Format("getV_TEMPLATE_DETAILListByDateTime   : {0}  ", ex.Message));
                this.writeErrorLog(string.Format("getV_TEMPLATE_DETAILListByDateTime   : {0}  ", ex.Message));
            }

            return listResult;
        }

        public string mappingCustomer(string customer)
        {
            string result = customer;
            try
            {
                if (customer == "GC OLE 1")
                {
                    result = "PTTGC-I1";
                }
                if (customer == "GC OLE 2")
                {
                    result = "PTTGC-I4";
                }
                if (customer == "GC OLE 3")
                {
                    result = "PTTPE";
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public int IsNewMasterRecord(TEMPLATE_COA_CHECK dtTemplateCheck, List<ViewQMS_TR_TEMPLATE_COA> listMaster)
        {
            int bResult = 1;

            try
            {
                if (null != listMaster && listMaster.Count() > 0)
                {
                    //ViewQMS_TR_TEMPLATE_COA dtTemplateCOA = listMaster.Where(m => m.COA_ID.Trim() == dtTemplateCheck.ID.Trim()).FirstOrDefault();
                    ViewQMS_TR_TEMPLATE_COA dtTemplateCOA = listMaster.Where(m => m.CUSTOMER_NAME.Trim() == dtTemplateCheck.CUSTOMER_NAME.Trim() && m.PRODUCT_NAME.Trim() == dtTemplateCheck.PRODUCT_NAME.Trim()).FirstOrDefault();

                    if (null == dtTemplateCOA)
                    {
                        bResult = 1;// update master and detail.
                    }
                    else
                    {
                        //if (dtTemplateCOA.COA_DT_COUNT != dtTemplateCheck.Qty)//ใช้ไม่ได้กรณีที่ ตัวที่วัดเท่าเดิมแต่เปลี่นตัววัด
                        //{
                        //    bResult = 2; // update detail.
                        //}
                        bResult = 2;//alway 2
                    } 
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return bResult;
        }

        public string getProductName(string COA_ID)
        {
            string szProductName = "";
            int pos = 0;
            try
            {
                pos = COA_ID.IndexOf("-");
                if (pos > 0)
                {
                    szProductName = COA_ID.Substring(0, pos);
                }
                else
                {
                    szProductName = "";
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return szProductName;
        }

        public string getCustomerName(string COA_ID)
        {
            string szCustomerName = "";
            int pos = 0;
            try
            {
                pos = COA_ID.IndexOf("-");
                if (pos > 0)
                {
                    szCustomerName = COA_ID.Substring(pos + 1);
                }
                else
                {
                    szCustomerName = "";
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return szCustomerName;
        }

        public string getProductNameEx(string productName)
        {
            string szProductName = "";
            int pos = 0;
            int pos2 = 0;
            try
            {
                pos = productName.IndexOf("(");
                pos2 = productName.IndexOf(")");
                if (pos > 0 && pos2 > 0)
                {
                    szProductName = productName.Substring(0, pos);
                }
                else
                {
                    szProductName = productName;
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return szProductName;
        }

        public string getTankName(string productName)
        {
            string szTankName = "";
            int pos = 0;
            int pos2 = 0;
            try
            {
                pos = productName.IndexOf("(");
                pos2 = productName.IndexOf(")");

                if (pos > 0 && pos2 > 0)
                {
                    szTankName = productName.Substring(pos + 1);
                    pos2 = szTankName.IndexOf(")");
                    if (pos2 > 0)
                    {
                        szTankName = szTankName.Substring(0, pos2);
                    }

                }
                else
                {
                    szTankName = "";
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return szTankName;
        }

        public CreateQMS_TR_TEMPLATE_COA_DETAIL convertOracleViewToModel(V_TEMPLATE_DETAIL dtDetail)
        {
            CreateQMS_TR_TEMPLATE_COA_DETAIL detailModel = new CreateQMS_TR_TEMPLATE_COA_DETAIL();

            try
            {
                detailModel.ID = 0;

                detailModel.COA_ID = dtDetail.ID;

                detailModel.CUSTOMER_NAME = dtDetail.CUSTOMERNAME;
                detailModel.PRODUCT_NAME = dtDetail.PRODUCT_NAME;
                detailModel.TANK_NAME = dtDetail.TANK_NAME;
                detailModel.ITEM_NAME = dtDetail.ITEM;
                detailModel.UNIT_NAME = dtDetail.UNIT;
                detailModel.USER_SEQUENCE = dtDetail.USER_SEQUENCE;

                detailModel.TEST_METHOD = dtDetail.TEST_METHOD;
                detailModel.SPECIFICATION = dtDetail.SPECIFICATION;

                detailModel.CONTROL_ID = 0;
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return detailModel;
        }

        public void SaveTemplateCOADetail(TEMPLATE_COA_CHECK dtTemplateCheck, List<ViewQMS_TR_TEMPLATE_COA> listMaster, List<V_TEMPLATE_DETAIL> listDetail)
        {
            TransactionService transSevice = new TransactionService(this._currentUserName);
            List<ViewQMS_TR_TEMPLATE_COA_DETAIL> listCOADetail = new List<ViewQMS_TR_TEMPLATE_COA_DETAIL>();
            try
            {
                ViewQMS_TR_TEMPLATE_COA masterTemplate = listMaster.Where( m => m.CUSTOMER_NAME == dtTemplateCheck.CUSTOMER_NAME && m.PRODUCT_NAME == dtTemplateCheck.PRODUCT_NAME).FirstOrDefault();
                listCOADetail = transSevice.getQMS_TR_TEMPLATE_COA_DETAILListByCOAID(_db, masterTemplate.CUSTOMER_NAME, masterTemplate.PRODUCT_NAME);

                if (listDetail.Count() > 0)
                {
                    foreach (V_TEMPLATE_DETAIL dtDetail in listDetail)
                    {
                        ViewQMS_TR_TEMPLATE_COA_DETAIL checkDuplicate = listCOADetail.Where(m => m.ITEM_NAME.Trim() == dtDetail.ITEM.Trim()
                            && m.UNIT_NAME.Trim() == dtDetail.UNIT.Trim()
                            && m.SPECIFICATION.Trim() == dtDetail.SPECIFICATION.Trim()
                            && m.TEST_METHOD.Trim() == dtDetail.TEST_METHOD.Trim()
                            //&& m.USER_SEQUENCE == dtDetail.USER_SEQUENCE
                            ).FirstOrDefault();

                        if (null == checkDuplicate)
                        {
                            CreateQMS_TR_TEMPLATE_COA_DETAIL detailModel = convertOracleViewToModel(dtDetail);
                            transSevice.SaveQMS_TR_TEMPLATE_COA_DETAIL(_db, detailModel);

                            ViewQMS_TR_TEMPLATE_COA_DETAIL tempDetail = new ViewQMS_TR_TEMPLATE_COA_DETAIL();
                            tempDetail.ID = detailModel.ID;
                            tempDetail.COA_ID = detailModel.COA_ID;
                            tempDetail.CUSTOMER_NAME = detailModel.CUSTOMER_NAME;
                            tempDetail.PRODUCT_NAME = detailModel.PRODUCT_NAME;
                            
                            tempDetail.ITEM_NAME = detailModel.ITEM_NAME;
                            tempDetail.UNIT_NAME = detailModel.UNIT_NAME;
                            tempDetail.USER_SEQUENCE = detailModel.USER_SEQUENCE;
                            tempDetail.TEST_METHOD = detailModel.TEST_METHOD;

                            tempDetail.SPECIFICATION = detailModel.SPECIFICATION;
                            tempDetail.CONTROL_ID = detailModel.CONTROL_ID; 
                            tempDetail.PRODUCT_MERGE_ID = detailModel.PRODUCT_MERGE_ID;

                            listCOADetail.Add(tempDetail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
           // return listCOADetail;
        }

        public int DailyCOAMasterDataCheck(DateTime startTime, DateTime endTime)
        {
            int nResult = 0;
            int nMasterCheck = 0;
             
            try
            {
                CreateQMS_TR_TEMPLATE_COA model = new CreateQMS_TR_TEMPLATE_COA();
                TransactionService transSevice = new TransactionService(this._currentUserName);
                List<ViewQMS_TR_TEMPLATE_COA> listMaster = transSevice.getQMS_TR_TEMPLATE_COAList(_db);

                //Step 0. delete old data .
                transSevice.DeleteQMS_TR_COA_DATAByDateTime(_db, startTime, endTime);

                //Step 1. get Master header 
                List<V_TEMPLATE_MASTER> listMasterResult = getV_TEMPLATE_MASTERListByDateTime(startTime, endTime);
                //Step 2. get Detail 
                List<V_TEMPLATE_DETAIL> listDetailResult = getV_TEMPLATE_DETAILListByDateTime(startTime, endTime);
                string json = JsonConvert.SerializeObject(listDetailResult);
                if (null != listMasterResult && listMasterResult.Count() > 0)
                {
                    //get all master in our database 
                }

                LogHelper.WriteErrorLog(string.Format("DailyCOAMasterDataCheck master : {0} / detail : {1}", listMasterResult.Count(), listDetailResult.Count()));

                if (null != listDetailResult && listDetailResult.Count() > 0) //master น่าจะต้อง ระบุจำนวน detail เหมือนกัน
                {
                    
                    #region master template check 
                    //List<TEMPLATE_COA_CHECK> listTemplateCheck = listDetailResult.GroupBy(x => new { x.ID })
                    //                                            .Select(g => new TEMPLATE_COA_CHECK {
                    //                                                            ID = g.Key.ID, 
                    //                                                            Qty = g.Count()
                    //                                            }).ToList();

                    List<TEMPLATE_COA_CHECK> listTemplateCheck = listDetailResult.GroupBy(x => new { x.CUSTOMERNAME, x.PRODUCT_NAME })
                                                               .Select(g => new TEMPLATE_COA_CHECK
                                                               {
                                                                   //ID = g.Key.ID,
                                                                   CUSTOMER_NAME = g.Key.CUSTOMERNAME,
                                                                   PRODUCT_NAME = g.Key.PRODUCT_NAME,
                                                                   Qty = g.Count()
                                                               }).ToList();


                    LogHelper.WriteErrorLog(string.Format("DailyCOAMasterDataCheck master : {0} / detail : {1}", listMasterResult.Count(), listDetailResult.Count()));

                    foreach (TEMPLATE_COA_CHECK dtTemplateCheck in listTemplateCheck)
                    {
                        nMasterCheck = IsNewMasterRecord(dtTemplateCheck, listMaster);
                        List<V_TEMPLATE_DETAIL> listDetail = listDetailResult.Where(m => m.CUSTOMERNAME.ToLower().Trim() == dtTemplateCheck.CUSTOMER_NAME.ToLower().Trim() && m.PRODUCT_NAME.ToLower().Trim() == dtTemplateCheck.PRODUCT_NAME.ToLower().Trim()).ToList();

                        if (null != listDetail && listDetail.Count() > 0)
                        {
                            if (nMasterCheck == 1)//update Master & detail 
                            {
                               //V_TEMPLATE_MASTER masterData = listMasterResult.Where(m => m.CUSTOMERNAME == dtTemplateCheck.CUSTOMER_NAME && m.PRODUCT == dtTemplateCheck.PRODUCT_NAME).FirstOrDefault();

                                //Save master
                                model = new CreateQMS_TR_TEMPLATE_COA();
                                model.ID = 0;
                                model.COA_ID = listDetail[0].ID;// masterData.ID;
                                model.CUSTOMER_NAME = listDetail[0].CUSTOMERNAME;  //masterData.CUSTOMERNAME;
                                model.PRODUCT_NAME = listDetail[0].PRODUCT_NAME;  //getProductNameEx(masterData.PRODUCT);

                                model.DOC_STATUS = (byte)TEMPLATE_COA_DOC_STATUS.PENDING;
                                model.ID = transSevice.SaveQMS_TR_TEMPLATE_COA(_db, model);

                                ViewQMS_TR_TEMPLATE_COA tempData = new ViewQMS_TR_TEMPLATE_COA();
                                tempData.ID = model.ID;
                                tempData.COA_ID = model.COA_ID;
                                tempData.CUSTOMER_NAME = model.CUSTOMER_NAME;
                                tempData.PRODUCT_NAME = model.PRODUCT_NAME;

                                tempData.DOC_STATUS = model.DOC_STATUS;
                                listMaster.Add(tempData);

                                if (model.ID > 0)
                                {
                                    //Save Detail
                                    SaveTemplateCOADetail(dtTemplateCheck, listMaster, listDetail);
                                }

                            }
                            else if (nMasterCheck == 2) //update only detail
                            {

                                SaveTemplateCOADetail(dtTemplateCheck, listMaster, listDetail);
                            }
                        }
                    }

                    #endregion

                    #region data input

                    CreateQMS_TR_COA_DATA dtData = new CreateQMS_TR_COA_DATA();
                    LogHelper.WriteErrorLog(string.Format("Start To save detail : {0}", listDetailResult.Count()));

                   // dtData.ID 
                    foreach( V_TEMPLATE_DETAIL tempDetail in listDetailResult){
                        dtData = new CreateQMS_TR_COA_DATA();
                        dtData.ID = 0;
                        dtData.CUSTOMER_NAME = tempDetail.CUSTOMERNAME;
                        dtData.SAMPLING_DATE = tempDetail.SAMPLINGDATE;
                        dtData.SAMPLING_NAME = tempDetail.SAMPLINGNAME;
                        dtData.SAMPLING_POINT = tempDetail.SAMPLINGPOINT;
                        dtData.TEST_METHOD = tempDetail.TEST_METHOD;
                        dtData.PRODUCT_NAME = tempDetail.PRODUCT_NAME;
                        dtData.TANK_NAME = tempDetail.TANK_NAME;
                        dtData.ITEM_NAME = tempDetail.ITEM;
                        dtData.UNIT_NAME = tempDetail.UNIT;
                        dtData.SPECIFICATION = tempDetail.SPECIFICATION;
                        dtData.DISPLAY_VALUE = tempDetail.DISPLAYVALUE;
                        dtData.ENTERED_VALUE = tempDetail.ENTERVALUE;
           
                        long saveResult = transSevice.SaveQMS_TR_COA_DATA(_db, dtData);

                       // LogHelper.WriteErrorLog(string.Format("SaveQMS_TR_COA_DATA result : {0}", saveResult));
                    }

                    #endregion
                }
                

            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(string.Format("DailyCOAMasterDataCheck result : {0}", ex.Message));
                this.writeErrorLog(string.Format("DailyCOAMasterDataCheck result : {0}", ex.Message));

                this.writeErrorLog(ex.Message);

                if (ex.InnerException != null)
                {
                    this.writeErrorLog("inner message :" + ex.InnerException.Message);
                }

                
            }

            return nResult;
        }

        public bool CreateDirectoryCSC(string PATH_NAME, DateTime startTime, string CUSTOMER_NAME)
        {
            bool bResult = false;
            try
            {

                //folder customer name
                if (!Directory.Exists(PATH_NAME + "\\" + CUSTOMER_NAME))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PATH_NAME + "\\" + CUSTOMER_NAME);
                }

                //folder year
                if (!Directory.Exists(PATH_NAME + "\\" + CUSTOMER_NAME + "\\" + startTime.Year))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PATH_NAME + "\\" + CUSTOMER_NAME + "\\" + startTime.Year);
                }

                string szMonth = (startTime.Month < 10) ? "0" + startTime.Month.ToString() : startTime.Month.ToString();

                //folder month
                if (!Directory.Exists(PATH_NAME + "\\" + CUSTOMER_NAME + "\\" + startTime.Year + "\\" + szMonth))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PATH_NAME + "\\" + CUSTOMER_NAME + "\\" + startTime.Year + "\\" + szMonth);
                }

                bResult = true;
                //if (!File.Exists(PATH_NAME + "\\" + coaData.CUSTOMER_NAME + "\\" + startTime.Year + "\\" + szMonth + "\\" + szFileName))
                //{
                //    // Try to create the directory.
                //    File.Create(PATH_NAME + "\\" + coaData.CUSTOMER_NAME + "\\" + startTime.Year + "\\" + szMonth + "\\" + szFileName);
                //}
            }
            catch(Exception ex)
            {
                this.writeErrorLog(ex.Message);

                if (ex.InnerException != null)
                {
                    this.writeErrorLog("inner message :" + ex.InnerException.Message);
                }
            }

            return bResult;
        }

        public ExcelPackage GetExcelPackageFromPath(string szFilePath, string productName, bool bForceDelete)
        {
            ExcelPackage excel = null;

            try
            {
                FileInfo newFile = new FileInfo(szFilePath);
                

                if(true == bForceDelete)
                {
                    File.Delete(szFilePath);
                    System.Threading.Thread.Sleep(500); 

                }

                excel = new ExcelPackage(newFile); 
                if (!File.Exists(szFilePath))
                { 
                    ExcelWorksheet worksheet = excel.Workbook.Worksheets.Add(productName); 
                }
                
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return excel;
        }

        public void DailyCOAWriteProductQualityToCSC(DateTime startTime, DateTime endTime, bool bForceDelete)
        {
            try
            {
                TransactionService transServices = new TransactionService(_currentUserName);
                ConfigServices cogServices = new ConfigServices(_currentUserName);
                List<QMS_TR_COA_DATA> listCOAData = QMS_TR_COA_DATA.GetByListDateTime(_db, startTime, endTime);
                List<CUSTOMER_COA> listCustomerCOA = new List<CUSTOMER_COA>();
                ProductQualityToCSCSearchModel searchModel = new ProductQualityToCSCSearchModel();

                CreatePRODUCT_CSC_PATH modelCSC = cogServices.getProductCSCPath(_db);// @"\\ryg-app-s01\Lab_Report\Reports\Product Quality to CSC"; //@"F:\Product Quality to CSC"; // @"\\ryg-app-s01\Lab_Report\Reports\Product Quality to CSC";
                string szFileName = modelCSC.PRODUCT_CSC_NAME;// @Resource.ResourceString.ProductQualityToCSC;//+ " " + "Feed Quality from PTT_Pang +".xlsx";
                string szExtension = ".xlsx";
                string szFilePath = "";
                string szMonth = "";

                searchModel.START_DATE = startTime;
                searchModel.END_DATE = endTime;

                this.writeErrorLog("listCOAData.Count {"+ listCOAData.Count()+"}");
                //.OrderBy(m => m.Key.CUSTOMER_NAME)
                if (listCOAData.Count() > 0)
                {
                    listCustomerCOA = listCOAData.GroupBy(m => new { m.CUSTOMER_NAME })
                                                         .Select(m => new CUSTOMER_COA
                                                         {
                                                             CUSTOMER = m.Key.CUSTOMER_NAME,
                                                             COA_ID = m.Key.CUSTOMER_NAME
                                                         }).OrderBy(m => m.CUSTOMER).ToList();

                    string curCustomer = null;

                    foreach (CUSTOMER_COA coaData in listCustomerCOA)
                    {
                        ExcelPackage excel = null;

                        if (null == curCustomer || (curCustomer != coaData.CUSTOMER))
                        { //เจอ customer ใหม่

                            curCustomer = coaData.CUSTOMER;
                            searchModel.CUSTOMER_NAME = coaData.CUSTOMER;


                            //searchModel.LIST_PRODUCT = listCOAData.Where(m => m.CUSTOMER_NAME == curCustomer).GroupBy(m => m.PRODUCT_NAME)
                            //                                 .Select(m => new PRODUCT_COA
                            //                                 {
                            //                                     PRODUCT = m.Key,//ไม่ได้ใช้ในกรณีนี้
                            //                                     COA_ID = m.Key
                            //                                 }).ToList();

                            searchModel.LIST_PRODUCT = transServices.getProductCOAListByCustomer(_db, curCustomer);
                            this.writeErrorLog("searchModel.LIST_PRODUCT : " + searchModel.LIST_PRODUCT.Count());
                            if (null != searchModel.LIST_PRODUCT && searchModel.LIST_PRODUCT.Count() > 0)
                            {
                                //searchModel.LIST_PRODUCT = searchModel.LIST_PRODUCT.OrderByDescending(m => m.POSITION).ToList();
                                this.writeErrorLog("Before CreateDirectoryCSC : " + DateTime.Now.ToString());
                                if (true == CreateDirectoryCSC(modelCSC.PRODUCT_CSC_PATH, startTime, curCustomer))
                                {
                                    try
                                    {
                                        this.writeErrorLog("After CreateDirectoryCSC success : " + DateTime.Now.ToString());
                                        szMonth = (startTime.Month < 10) ? "0" + startTime.Month.ToString() : startTime.Month.ToString();
                                        this.writeErrorLog("Before Create excel : " + DateTime.Now.ToString());
                                        szFilePath = modelCSC.PRODUCT_CSC_PATH + "\\" + curCustomer + "\\" + startTime.Year + "\\" + szMonth + "\\" + szFileName + "_" + curCustomer + "_" + szMonth + "_" + startTime.Year + szExtension;
                                        excel = GetExcelPackageFromPath(szFilePath, searchModel.LIST_PRODUCT[0].PRODUCT, bForceDelete);

                                        this.writeErrorLog("After Create excel : "+ szFilePath + " " + DateTime.Now.ToString());
                                        //ได้ file excel แล้ว render data
                                        excel = transServices.CSCDataExcelExecute(_db, searchModel, excel, true);
                                    }
                                    catch(Exception exExcel)
                                    {
                                        this.writeErrorLog("Error excel : " + exExcel.Message.ToString());
                                    }
                                    
                                }
                                
                            }
                        }
                        this.writeErrorLog("Create excel success: " + szFilePath + " " + DateTime.Now.ToString());
                        if (null != excel)
                        {
                            try
                            {
                                excel.Save();
                                this.writeErrorLog("Save excel success: " + szFilePath + " " + DateTime.Now.ToString());
                                //excel.Dispose();
                            }
                            catch (Exception exExcel)
                            {
                                this.writeErrorLog("Save Error excel : " + exExcel.Message.ToString());
                            }
                        }

                    }

                    //QMS_TR_TEMPLATE_COA template_id = QMS_TR_TEMPLATE_COA.GetByCOA_ID(db, product.COA_ID);
                    //List<QMS_TR_TEMPLATE_COA_DETAIL> listDetail = QMS_TR_TEMPLATE_COA_DETAIL.GetByCOAId(db, template_id.ID);

                    //if (!Directory.Exists(PATH_NAME))
                    //{
                    //     Try to create the directory.
                    //    DirectoryInfo di = Directory.CreateDirectory(PATH_NAME);
                    //}
                }
            }
            catch (IOException ioex)
            {
                //Console.WriteLine(ioex.Message);
                this.writeErrorLog(ioex.Message);
                if (ioex.InnerException != null)
                {
                    this.writeErrorLog("inner message :" + ioex.InnerException.Message);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);

                if (ex.InnerException != null)
                {
                    this.writeErrorLog("inner message :" + ex.InnerException.Message);
                }
            }
        }
    }
}
