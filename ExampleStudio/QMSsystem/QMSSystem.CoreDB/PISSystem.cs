﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QMSSystem.Model;
using System.Data.SqlClient;
using System.Data;
using QMSSystem.CoreDB.Helper.Grid;

namespace QMSSystem.CoreDB
{
    public class PISSystem
    {
        public string msgError;
        public string m_connectiong;
        public string m_tblPersonalInfo = "personel_info";
        public string m_tblUnit = "unit";
        public string EmployeeType { get; set; }

        public PISSystem()
        {
            m_connectiong = "Data Source=hq-db-v07; Initial Catalog=PIS; User ID=pisread; Password=pis@dm1n";
            this.EmployeeType = "PTT";
        }

        public PISSystem(string connectionString)
        {
            m_connectiong = connectionString;
            this.EmployeeType = "PTT";
        }

        public SqlConnection getConnection()
        {
            //server 
            SqlConnection connection = new SqlConnection(m_connectiong);
            return connection;
        }

        public List<PersonalUnit> getAllPersonalUnit()
        {
            DataTable result = new DataTable();
            List<PersonalUnit> listResult = new List<PersonalUnit>();
            try
            {
                string queryString = "SELECT unitcode, unitabbr FROM unit";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        PersonalUnit temp = new PersonalUnit();
                        temp.UNITCODE = reader["unitcode"].ToString();
                        temp.UNITABBR = reader["unitabbr"].ToString();
                        listResult.Add(temp);
                    }

                    result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public PersonalUnit getDummyFromUnitabbr(string unitAbbr, bool isLab = false)
        {
            DataTable result = new DataTable();
            PersonalUnit dtResult = new PersonalUnit();

            dtResult.UNITABBR = unitAbbr;
           

            try
            {
                string queryString = string.Format("SELECT DUMMY_RELATIONSHIP, unitcode, unitabbr FROM unit WHERE unitabbr ='{0}'", unitAbbr);
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        dtResult.UNITCODE = reader["unitcode"].ToString();
                        dtResult.UNITABBR = reader["unitabbr"].ToString();
                        dtResult.DUMMY_RELATIONSHIP = reader["DUMMY_RELATIONSHIP"].ToString();
                        break; 
                    }

                    result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            if (null == dtResult.DUMMY_RELATIONSHIP || "" == dtResult.DUMMY_RELATIONSHIP)
            {
                if (isLab)
                {
                    dtResult.DUMMY_RELATIONSHIP = "1-1-14-2-6-6-2";
                }
                else
                {
                    dtResult.DUMMY_RELATIONSHIP = "1-1-14-2-6";
                }
            }

            return dtResult;
        }

        public List<PersonalInfo> getAllPersonalInfoData(PersonalInfoSearch searchModel)
        {
            DataTable result = new DataTable();
            List<PersonalInfo> listResult = new List<PersonalInfo>();

            bool bHasCondition = false;

            try
            {
                string queryString = "SELECT p.CODE, p.INAME, p.FNAME, p.LNAME, p.FULLNAMETH, p.UNITCODE, p.POSCODE, p.EmailAddr, u.unitabbr, po.posname, po.AB_NAME "
                                    + " FROM [PIS].[dbo].[personel_info] p   "
                                    + " LEFT JOIN [PIS].[dbo].[unit] u ON p.UNITCODE = u.unitcode "
                                    + " LEFT JOIN [PIS].[dbo].[position] po ON p.POSCODE = po.poscode"
                                    + " WHERE u.[DUMMY_RELATIONSHIP] like '1-1-12-2-6%' ";//-1-12-2-6

                bHasCondition = true;

                if (!string.IsNullOrEmpty(searchModel.mSearch.UNIT_NAME))// != "" && null != searchModel.mSearch.UNIT_NAME)
                { 
                    queryString +=  " AND u.unitabbr like '%' + @UNIT_NAME + '%' ";
                }

                if (!string.IsNullOrEmpty(searchModel.mSearch.NAME_SURNAME))// != "" && null != searchModel.mSearch.NAME_SURNAME)
                {
                    if (true == bHasCondition)
                    {
                        queryString += " AND p.FULLNAMETH like  '%' + @NAME_SURNAME + '%' ";
                    }
                    else
                    {
                        queryString += " WHERE p.FULLNAMETH like  '%' + @NAME_SURNAME + '%' ";
                        bHasCondition = true;
                    }
                }

                if (!string.IsNullOrEmpty(searchModel.mSearch.POSTITION))// != "" && null != searchModel.mSearch.POSTITION)
                {
                    if (true == bHasCondition)
                    {
                        queryString += " AND po.posname like  '%' + @POSTITION + '%' ";
                    }
                    else
                    {
                        queryString += " WHERE po.posname like  '%' + @POSTITION + '%' ";
                        bHasCondition = true;
                    }
                }

                if (!string.IsNullOrEmpty(searchModel.mSearch.EMPLOY_ID))// != "" && null != searchModel.mSearch.EMPLOY_ID)
                {
                    if (true == bHasCondition)
                    {
                        queryString += " AND p.CODE like '%' + @EMPLOY_ID + '%' ";
                    }
                    else
                    {
                        queryString += " WHERE p.CODE like '%' + @EMPLOY_ID + '%' ";
                        bHasCondition = true;
                    }
                }

                //ORDER 
                //searchModel.SortColumn = (searchModel.SortColumn == "" || null == searchModel.SortColumn) ? "CODE" : searchModel.SortColumn;
                //long OFFSET = (searchModel.PageIndex - 1) * searchModel.PageSize;
                //queryString += " ORDER BY " + searchModel.SortColumn + " " + searchModel.SortOrder;
                //queryString += " OFFSET " + OFFSET + " ROWS FETCH NEXT " + searchModel.PageSize  + " ROWS ONLY";


                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);

                if (!string.IsNullOrEmpty(searchModel.mSearch.UNIT_NAME))
                {
                    command.Parameters.AddWithValue("@UNIT_NAME", searchModel.mSearch.UNIT_NAME);
                }

                if (!string.IsNullOrEmpty(searchModel.mSearch.NAME_SURNAME))
                {
                    command.Parameters.AddWithValue("@NAME_SURNAME", searchModel.mSearch.NAME_SURNAME);
                }

                if (!string.IsNullOrEmpty(searchModel.mSearch.POSTITION))
                {
                    command.Parameters.AddWithValue("@POSTITION", searchModel.mSearch.POSTITION);
                }

                if (!string.IsNullOrEmpty(searchModel.mSearch.EMPLOY_ID))
                {
                    command.Parameters.AddWithValue("@EMPLOY_ID", searchModel.mSearch.EMPLOY_ID);
                }


                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        PersonalInfo temp = new PersonalInfo();

                        temp.PERSON_CODE = reader["CODE"].ToString();
                        temp.INAME = reader["INAME"].ToString();
                        temp.FNAME = reader["FNAME"].ToString();
                        temp.LNAME = reader["LNAME"].ToString();
                        temp.FULLNAMETH = reader["INAME"].ToString() + " " + reader["FNAME"].ToString() + " " + reader["LNAME"].ToString();
                        temp.POSCODE = reader["POSCODE"].ToString();

                        temp.POSNAME = reader["posname"].ToString();
                        temp.UNITCODE = reader["UNITCODE"].ToString();
                        temp.UNITNAME = reader["unitabbr"].ToString();
                        temp.EMAIL = reader["EmailAddr"].ToString();

                        listResult.Add(temp);
                    }

                    result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<PageIndexList> getPageIndexList(long total)
        {
            List<PageIndexList> listResult = new List<PageIndexList>();

            for (int i = 0; i < total; i++)
            {
                listResult.Add(new PageIndexList { PageIndex = i + 1 });
            }

            return listResult;
        }

        public List<PersonalInfo> searchPersonalInfo(PersonalInfoSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<PersonalInfo> objResult = new List<PersonalInfo>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            GridSettings grid = new GridSettings(searchModel.SortColumn, true);
            grid.PageIndex = searchModel.PageIndex;
            grid.PageSize = searchModel.PageSize;
            grid.SortOrder = searchModel.SortOrder;
            if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
            {
                grid.SortColumn = "PERSON_CODE";
            }
            else
            {
                grid.SortColumn = searchModel.SortColumn;
            }

            try
            {
                List<PersonalInfo> listData = this.getAllPersonalInfoData(searchModel);
                objResult = grid.LoadGridData<PersonalInfo>(listData.AsQueryable(), out count, out totalPage).ToList();
                listPageIndex = getPageIndexList(totalPage);
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }


            return objResult;
        }


        public DataTable GetEmployee_Master_LogOnName(string logOnName)
        {
            string expression = "";
            if (EmployeeType == "PTT")
            {
                expression = string.Format("CODE='{0}'", logOnName);
            }
            else
            {
                expression = string.Format("LOGON_NAME='{0}'", logOnName);
            }

            DataTable dtResult = GetEmployee_Master(expression, "EX");
            if (dtResult != null && dtResult.Rows.Count > 0)
            {
                return dtResult;
            }
            else
            {
                expression = string.Format("UserName='{0}'", logOnName);
                DataTable dtResult_in = GetEmployee_Master(expression, "IN");
                return dtResult_in;

            }

        }

        /*========= Employee Information ==========*/
        public DataTable GetEmployee_Master(string expression, string sourceType)
        {
            DataTable dbResult = new DataTable();
            try
            {

                string sql = "";
                if (sourceType == "EX")
                {

                    //=== in dbworkflow or pis ====
                    #region Switch source
                    if (EmployeeType == "PTT")
                    {
                        //==== PTT ============*//
                        sql = @"
                        SELECT
                        distinct
                        CODE as EmpID,
                        CODE as EmpCode,                      
                        fullnameTH as EmployeeName,
                        POSNAME as PositionName,
                        EmailAddr as Email,
                        [OFFICETEL] as [Phone],
                        CODE as LogOnName,
                        b.unitabbr as UnitName,
                        b.unitcode as unitcode,
                        b.longname as unitlongname,
                        a.jobgroup as [level],
                        b.DUMMY_RELATIONSHIP as unitdummy,
                        '' as DivisionName,'' as DepartmentName,'' as SectionName,0 as DivID,0 as DeptID,0 as SecID
                        FROM personel_info a		 
                        left join unit b on a.UNITCODE=b.unitcode where a.WSTCODE IN ('A', 'B', 'I', 'J')  AND " + ReplaceExpression(expression); // waiting for filter inactive employee 

                    }
                    else
                    {
                        //==== PTTEP =====*//

                        sql = @"   SELECT 
							EMPLOYEE_ID as EmpID,  
                            EMPLOYEE_ID as EmpCode,            
                            replace(isnull(FIRSTNAME,''),'NULL','')+' ' + replace(isnull(LASTNAME,''),'NULL','') as EmployeeName,
                            replace(isnull(POSITION,''),'NULL','') as PositionName,
                            CASE 
                            WHEN replace(isnull(Sec,''),'NULL','')='' and  replace(isnull(Dept,''),'NULL','')='' then  Div
                            WHEN replace(isnull(Sec,''),'NULL','')='' and  replace(isnull(Dept,''),'NULL','')<>'' then  Dept
                            WHEN replace(isnull(Sec,''),'NULL','')<>''  then  sec 
                            END as UnitName,
                            
                            replace(isnull(Div,''),'NULL','') as DivisionName,
                            replace(isnull(Dept,''),'NULL','') as DepartmentName,
                            replace(isnull(Sec,''),'NULL','') as SectionName,                            
            
                            replace(isnull(EMAIL_ID,''),'NULL','') as Email,
                            null as Phone,
                            replace(isnull(LOGON_NAME,''),'NULL','') as LogOnName   
                            FROM  V_AllEmployee_ORG where isnull([STATUS],1)=1  AND  " + ReplaceExpression(expression);


                    }
                    #endregion

                    dbResult =  ExecucteSql(sql);

                }
                else
                {
                    //==== Internal System ===========//
                    //clsDB objDB = new clsDB();
                    //sql = "select * from V_Employee_Master where isnull(IsActived,1)=1  AND " + expression;
                    //return objDB.ExecucteSql(sql);
                    //ยังไม่มี
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dbResult;

        }

        public string ReplaceExpression(string exp)
        {
            string result = exp;
            if (EmployeeType == "PTT")
            {
                result = result.Replace("X_1", "CODE");

                result = result.Replace("X_2_1", "unitabbr");
                result = result.Replace("X_2_2", "unitabbr");
                result = result.Replace("X_2_3", "unitabbr");

                result = result.Replace("X_3_1", "fullnameTH");
                result = result.Replace("X_3_2", "fullnameTH");
                result = result.Replace("X_4", "POSNAME");
                result = result.Replace("EmpCode", "CODE");

            }
            else
            {
                result = result.Replace("EmpCode", "EMPLOYEE_ID");
                result = result.Replace("EmployeeName", "FIRSTNAME");
                result = result.Replace("PositionName", "POSNAME");
                result = result.Replace("UnitName", "unitabbr");

                result = result.Replace("X_1", "EMPLOYEE_ID");

                result = result.Replace("X_2_1", "Div");
                result = result.Replace("X_2_2", "Dept");
                result = result.Replace("X_2_3", "Sec");

                result = result.Replace("X_3_1", "FIRSTNAME");
                result = result.Replace("X_3_2", "LASTNAME");
                result = result.Replace("X_4", "POSITION");

                result = result.Replace("EmpCode", "EMPLOYEE_ID");



            }
            return result;

        }

        private DataTable ExecucteSql(string queryString)
        {
            DataTable result = new DataTable();
            try
            { 
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                { 
                    result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result; 
        }

    }
}
