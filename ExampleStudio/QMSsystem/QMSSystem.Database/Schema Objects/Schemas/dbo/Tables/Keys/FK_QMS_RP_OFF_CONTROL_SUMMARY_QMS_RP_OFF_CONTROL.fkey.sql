﻿ALTER TABLE [dbo].[QMS_RP_OFF_CONTROL_SUMMARY]  WITH CHECK ADD  CONSTRAINT [FK_QMS_RP_OFF_CONTROL_SUMMARY_QMS_RP_OFF_CONTROL] FOREIGN KEY([RP_OFF_CONTROL_ID])
REFERENCES [dbo].[QMS_RP_OFF_CONTROL] ([ID])
ON DELETE CASCADE


GO
ALTER TABLE [dbo].[QMS_RP_OFF_CONTROL_SUMMARY] CHECK CONSTRAINT [FK_QMS_RP_OFF_CONTROL_SUMMARY_QMS_RP_OFF_CONTROL]

