﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.Model;
using System.Security.Principal;
using System.Web.Security;
using System.Web.Routing;
using QMSSystem.CoreDB.Services;
using QMSsystem.Models.Helper;
using System.Configuration;

namespace QMSsystem.Models.Privilege
{

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class LegacyAuthorize : AuthorizeAttribute
    {

        public string Model { get; set; }
        public string Action { get; set; }
        public PrivilegeModeEnum[] RoleTask { get; set; }
        public DataAccessEnum DataAccess { get; set; }

        private bool isAccessDenied = false;

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
            || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);

            isAccessDenied = false;

            if (!skipAuthorization)
            {
                try
                {
                    //Get Data From Cookie.... 
                    if ((HttpContext.Current.Session["SiteSession"] == null) && (HttpContext.Current.Request.Cookies["QmsCookie"] != null)) 
                    {
                        UserService usrService = new UserService("system");
                        SessionModel chkModel = new SessionModel();
                        String cookieDate_string = "";
                         
                        string userName = (HttpContext.Current.Request.Cookies["QmsCookie"].Values["QmsData"] != null) ? HttpContext.Current.Request.Cookies["QmsCookie"].Values["QmsData"].ToString() : "";
                        string szPassword = (HttpContext.Current.Request.Cookies["QmsCookie"].Values["QmsValue"] != null) ? HttpContext.Current.Request.Cookies["QmsCookie"].Values["QmsValue"].ToString() : "";
                        cookieDate_string = (HttpContext.Current.Request.Cookies["QmsCookie"].Values["Expiers"] != null) ? HttpContext.Current.Request.Cookies["QmsCookie"].Values["Expiers"].ToString() : "";

                        if ("" != cookieDate_string && DateTime.Now < Convert.ToDateTime(cookieDate_string))
                        {
                            if (userName != "")
                            {
                                userName = Encryption.Decrypt(userName);
                            }

                            if (szPassword != "")
                            {
                                szPassword = Encryption.Decrypt(szPassword);
                            }

                            // check login จาก cookkie
                            bool bResult = usrService.IsCheckUserExistEx(userName, szPassword); //ตรวจสอบ user domain or back door

                            if (true == bResult)
                            {
                                SiteSession siteSession = new SiteSession();
                                string szImagePath = "";
                                string szUserShowName = "";
                                string szDeptName = "";

                                //bool bIsAdminAccess = usrService.isAdminAuthenAccess(userName, ConfigurationManager.ConnectionStrings["PISSYTEM"].ToString(), out szImagePath, out szUserShowName, out szDeptName); //ตรวจสอบ PIS or back door สิทธิ์
                                //siteSession.m_Authen = usrService.getDefaultListAuthenAccess(bIsAdminAccess);
                                //siteSession.Username = userName;
                                //siteSession.UserShowName = szUserShowName;
                                //siteSession.ImagePath = szImagePath;
                                //siteSession.DeptName = szDeptName;


                                int bIsAdminAccess = usrService.isAdminAuthenAccess(userName, ConfigurationManager.ConnectionStrings["PISSYTEM"].ToString(), out szImagePath, out szUserShowName, out szDeptName); //ตรวจสอบ PIS or back door สิทธิ์

                                if (bIsAdminAccess == (int)AuthenResult.Admin || bIsAdminAccess == (int)AuthenResult.User)
                                {
                                    bool bIsAdmin = (bIsAdminAccess == (int)AuthenResult.Admin) ? true : false;

                                    siteSession.m_Authen = usrService.getDefaultListAuthenAccess(bIsAdmin);
                                    siteSession.Username = userName;
                                    siteSession.UserShowName = szUserShowName;
                                    siteSession.ImagePath = szImagePath;
                                    siteSession.DeptName = szDeptName;

                                    HttpContext.Current.Session["SiteSession"] = siteSession;
                                }
                                else //clear cookie
                                {
                                    HttpContext.Current.Request.Cookies["QmsCookie"].Secure = true;
                                    HttpContext.Current.Request.Cookies["QmsCookie"].SameSite = SameSiteMode.Strict;
                                    HttpContext.Current.Request.Cookies["QmsCookie"].HttpOnly = true;
                                    HttpContext.Current.Request.Cookies["QmsCookie"].Expires = DateTime.Now.AddDays(-1);
                                    HttpContext.Current.Request.Cookies["QmsCookie"].Values["Expiers"] = DateTime.Now.AddDays(-1).ToString();
                                    HttpContext.Current.Request.Cookies["QmsCookie"].Values["QmsData"] = "";
                                    HttpContext.Current.Request.Cookies["QmsCookie"].Values["QmsValue"] = "";
                                }

                                if (null != siteSession && null != siteSession.Username)
                                {
                                    filterContext.Controller.ViewBag.ImagePath = (null != siteSession.ImagePath && "" != siteSession.ImagePath) ? siteSession.ImagePath : "./Content/images/qms/img.jpg";
                                    filterContext.Controller.ViewBag.DeptName = (null != siteSession.DeptName && "" != siteSession.DeptName) ? siteSession.DeptName : "";
                                }
                            }
                             
                        }

                    }
                }
                catch (Exception ex)
                {
                    //do something
                }

                SiteSession checkLogin = (HttpContext.Current.Session["SiteSession"] == null )? null : (SiteSession)HttpContext.Current.Session["SiteSession"];

                if (checkLogin == null)
                {
                    HandleUnauthorizedRequest(filterContext);
                }
                else
                {
                    bool result = false;

                    if (null != RoleTask)
                    {
                        foreach (PrivilegeModeEnum role in RoleTask)
                        {
                            if (true == UserIdentity.IsCanAccess(role, DataAccess))
                            {
                                result = true;
                                break;
                            }
                        }
                    }

                    if (false == result)
                    {
                        isAccessDenied = true;
                        HandleUnauthorizedRequest(filterContext);
                    } 
                }
                //base.OnAuthorization(filterContext);
                //filterContext.Controller.ViewData["DataAccess"] = DataAccess;
            }
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)//, bool isAccessDenied)
        {

            try
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {

                    var viewResult = new JsonResult();
                    string redirectUrl = VirtualPathUtility.ToAbsolute("~/") ;

                    viewResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

                    if (true == isAccessDenied)
                    {
                        viewResult.Data = (new { result = "Error", message = @Resources.Strings.NoPrivilege });
                    }
                    else
                    {
                        viewResult.Data = (new { result = "Expire", message = @Resources.Strings.SessionExpire, redirect = redirectUrl });
                    }


                    filterContext.Result = viewResult;
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(
                                      new RouteValueDictionary 
                                       {
                                           { "action", "Index" },
                                           { "controller", "Login" }
                                       });
                    base.HandleUnauthorizedRequest(filterContext);
                }
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectToRouteResult(
                                      new RouteValueDictionary 
                                   {
                                       { "action", "Index" },
                                       { "controller", "Login" }
                                   });
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }

    

    public class UserIdentity : IIdentity, IPrincipal
    {
        private readonly FormsAuthenticationTicket _ticket;

        public UserIdentity(FormsAuthenticationTicket ticket)
        {
            _ticket = ticket;
        }

        public string AuthenticationType
        {
            get { return "User"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string Name
        {
            get { return _ticket.Name; }
        }

        public string UserId
        {
            get { return _ticket.UserData; }
        }

        public bool IsInRole(string role)
        {
            return Roles.IsUserInRole(role);
        }

        public IIdentity Identity
        {
            get { return this; }
        }

        public static bool IsCanAccess(PrivilegeModeEnum RoleTask, DataAccessEnum DataAccess)
        {
            bool result = false;
            List<AuthenAccess> lstAuthen = new List<AuthenAccess>();

            if (DataAccessEnum.Anonymous == DataAccess )
            {
                result = true;
            }
            else
            {
                SiteSession siteSession = (HttpContext.Current.Session["SiteSession"] == null ? null : (SiteSession)HttpContext.Current.Session["SiteSession"]);

                if (null != siteSession)
                {
                    if (RoleTask == (int)PrivilegeModeEnum.All)
                    {
                        return true;
                    }

                    lstAuthen = (List<AuthenAccess>)siteSession.getAuthenAccess(); //Actual 
                    try
                    {
                        if (null != lstAuthen)
                        {
                            AuthenAccess au = lstAuthen.Where(m => m.code == (int)RoleTask
                                                && m.access == true
                                                ).SingleOrDefault();

                            if (null != au)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                        result = false;
                    }
                }

            }
            return result;
        }





    } 
}
