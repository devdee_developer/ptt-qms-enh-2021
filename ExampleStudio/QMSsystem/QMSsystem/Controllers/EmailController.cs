﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using System.Configuration;
using QMSSystem.CoreDB;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_Email  }, DataAccess = DataAccessEnum.Authen)]
    public class EmailController : BaseController
    {
        //
        // GET: /Email/

        public ActionResult ListMasterEmail()
        {
            QMS_MA_EMAILSearchModel searchQMS_MA_EMAIL = new QMS_MA_EMAILSearchModel();

            searchQMS_MA_EMAIL.GROUP_TYPE = (byte)EMAIL_GROUP_TYPE.ADMIN_EMAIL;

            ViewBag.searchQMS_MA_EMAIL = searchQMS_MA_EMAIL;
            ViewBag.listPageSize = this.getPageSizeList();

            
            return View();
        }
          
        public ActionResult ListMasterManageEmail()
        {
            QMS_MA_EMAILSearchModel searchQMS_MA_EMAIL = new QMS_MA_EMAILSearchModel();

            searchQMS_MA_EMAIL.GROUP_TYPE = (byte)EMAIL_GROUP_TYPE.MANAGE_EMAIL;

            ViewBag.searchQMS_MA_EMAIL = searchQMS_MA_EMAIL;
            ViewBag.listPageSize = this.getPageSizeList();


            return View();
        }

        public ActionResult CreateMasterEmail()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            PersonalInfoSearchModel searchPersonalInfo = new PersonalInfoSearchModel();

            ViewBag.searchPersonalInfo = searchPersonalInfo;
            ViewBag.listPageSize = this.getPageSizeList();
            return View();
        }

        public ActionResult CreateMasterManageEmail()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            PersonalInfoSearchModel searchPersonalInfo = new PersonalInfoSearchModel();

            ViewBag.searchPersonalInfo = searchPersonalInfo;
            ViewBag.listPageSize = this.getPageSizeList();
            return View();
        }

        public JsonResult GetPersonInfoBySearch(PersonalInfoSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                PISSystem pisServices = new PISSystem(ConfigurationManager.ConnectionStrings["PISSYTEM"].ToString());

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<PersonalInfo> listData = new List<PersonalInfo>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = pisServices.searchPersonalInfo (model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_EMAIL(QMS_MA_EMAILSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_EMAIL> listData = new List<ViewQMS_MA_EMAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterServices.searchQMS_MA_EMAIL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateListMasterEmail(List<CreateQMS_MA_EMAIL> model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterServices.SaveListQMS_MA_EMAIL(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_EMAIL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterServices.DeleteQMS_MA_EMAILByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
