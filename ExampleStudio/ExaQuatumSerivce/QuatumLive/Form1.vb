﻿Option Explicit On
Imports System.IO


Public Class Form1
    Dim objSession As QUANTUMAUTOMATIONLib.Session2
    Dim WithEvents objDataAccess As QUANTUMAUTOMATIONLib.QDataAccess
    '  Attribute objDataAccess.VB_VarHelpID = -1
    Dim objBrowser As QUANTUMAUTOMATIONLib.Browse2
    Dim objQualityHelper As QUANTUMAUTOMATIONLib.QQualityHelper

    Dim rsBrowseResults As ADODB.Recordset
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'GSP2.GC ONLINE.780-QR-004 ETHANE.NORMALIZE.780QR-4NC4.PV.Value
        '"PTTGSP.GSP2.GC ONLINE.780-QR-004 ETHANE.NORMALIZE.780QR-4NC4.PV.Value:Value"
        'GSP2.782.FLOW.782FI083_TON.Valuedi
        Dim dateTime1 As Date 
        Dim dateTime2 As Date

        dateTime1 = DateTime.Now
        dateTime2 = dateTime1.AddDays(-1)

    End Sub
    Function getTagData() As String

        Dim szTagData As String
        szTagData = TextBox2.Text
        Try 
            Return Replace(szTagData, "PTTGSP", "Root")
        Catch ex As Exception
            Return szTagData
        End Try

    End Function

    Function getServerName() As String
        Dim szServerName As String

        If (InStr(TextBox2.Text, "GSP1") > 0) Then
            szServerName = "EXAQ-GSP1"
        ElseIf (InStr(TextBox2.Text, "GSP2") > 0) Then
            szServerName = "EXAQ-GSP2"
        ElseIf (InStr(TextBox2.Text, "GSP3") > 0) Then
            szServerName = "EXAQ-GSP3"
        ElseIf (InStr(TextBox2.Text, "GSP5") > 0) Then
            szServerName = "EXAQ-RYG5"
        ElseIf (InStr(TextBox2.Text, "GSP6") > 0) Then
            szServerName = "EXAQ-GSP6"
        ElseIf (InStr(TextBox2.Text, "ESP") > 0) Then
            szServerName = "EXAQ-ESP"
        ElseIf (InStr(TextBox2.Text, "RPLF") > 0) Then
            szServerName = "EXAQ-GSP1"
        Else 
            szServerName = "EXAQ-GSP1"
        End If
        lblServername.Text = szServerName

        Return szServerName
    End Function

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        ' Get a Session2 object for the first server.
        objSession = New QUANTUMAUTOMATIONLib.Session2
        ' Tell Session2 which Exaquantum server to connect to.
        objSession.ServerName = getServerName() ' "EXAQ-GSP1"
        Dim ExaqData As Object
        Dim tmpData
        Dim vItemID As Object

        Me.Enabled = False
        lblError.Text = ""

        Try
            ' Check that both Exaquantum servers are running.
            If Not objSession.IsConnected() Then
                lblError.Text = "Error: Server disconnect.. "
                Me.Enabled = True
                Exit Sub
            End If
            ' Get a DataAccess object for the first server.
            objDataAccess = New QUANTUMAUTOMATIONLib.QDataAccess
            ' Give the Session Pointer to the DataAccess object.

            objBrowser = objSession.Browser
            objDataAccess.SetSession(objSession) 'PTTGSP.GSP1.702.FLOW.702FR001.PV.Value

            'rsBrowseResults = objBrowser.PathToMetaData("Root.PTTGSP.GSP2.780.FLOW.780FI02.PV.Value", 1, QUANTUMAUTOMATIONLib.brFields.brDetail)
            rsBrowseResults = objBrowser.PathToMetaData(getTagData(), 1, QUANTUMAUTOMATIONLib.brFields.brDetail)

            vItemID = rsBrowseResults.Fields("ItemId").Value
            If Not IsDBNull(vItemID) Then
                objDataAccess.Add(vItemID)
                ExaqData = objDataAccess.ReadValue
                tmpData = ExaqData(1, 0)(0, 0)
                TextBox1.Text = tmpData
                TextBox3.Text = ExaqData(1, 0)(2, 0)
                objDataAccess.Remove(objDataAccess.Item(0))
            Else
                lblError.Text = "Error: Path is invalid "
            End If
        Catch ex As Exception
            'TextBox1.Text = ex.Message
            lblError.Text = "Error: " & ex.Message
        End Try
        Me.Enabled = True
    End Sub

    Private Sub Label3_Click(sender As System.Object, e As System.EventArgs) Handles Label3.Click

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        ' Get a Session2 object for the first server.
        objSession = New QUANTUMAUTOMATIONLib.Session2
        ' Tell Session2 which Exaquantum server to connect to.
        objSession.ServerName = getServerName() '"EXAQ-GSP1"
        Dim ExaqData As Object
        Dim tmpData
        Dim vItemID As Object


        Dim lQuality As Long
        Dim dTimeStamp As Date
        Dim dQueryTime As Date
        Dim dQueryTime1 As Date

        Dim vData As Object
        Dim vValue As Object

        Me.Enabled = False

        Try
            ' Check that both Exaquantum servers are running.
            If Not objSession.IsConnected() Then
                lblError.Text = "Error: Server disconnect.. "
                Me.Enabled = True
                Exit Sub
            End If
            ' Get a DataAccess object for the first server.
            objDataAccess = New QUANTUMAUTOMATIONLib.QDataAccess
            ' Give the Session Pointer to the DataAccess object.

            objBrowser = objSession.Browser
            objDataAccess.SetSession(objSession) 'PTTGSP.GSP1.702.FLOW.702FR001.PV.Value

            'rsBrowseResults = objBrowser.PathToMetaData("Root.GSP1.702.FLOW.702FR001.PV.Value", 1, QUANTUMAUTOMATIONLib.brFields.brDetail)
            rsBrowseResults = objBrowser.PathToMetaData(getTagData(), 1, QUANTUMAUTOMATIONLib.brFields.brDetail)

            vItemID = rsBrowseResults.Fields("ItemId").Value
            If Not IsDBNull(vItemID) Then
                objDataAccess.Add(vItemID)

                ''Pull history 5 minutes*********************************************
                ''Lets query for the last 5 minutes
                dQueryTime = DateAdd("n", -10, Now)
                '' Set the QueryTimes on DataAccess object
                'objDataAccess.SetQueryTimes(dQueryTime)
                '' Get the latest values of all Items
                '' Get the latest values of all Items in the collection.
                'vData = objDataAccess.ReadValue
                ''Pull history 5 minutes*********************************************


                'Pull range*********************************************

                ' Set time range from yesterday to now
                dQueryTime = dtpInputDate.Value.AddMinutes(-20)
                dQueryTime1 = dtpInputDate.Value
                'dQueryTime = "8/9/2016 20:50:59"
                'dQueryTime1 = "8/9/2016 20:59:59"
                objDataAccess.SetQueryTimes(StartTime:=dQueryTime, EndTime:=dQueryTime1)


                'objDataAccess.SetQueryTimes(Now, dQueryTime, Now)


                ' Dimension the filter parametrars to return interpolated data
                Dim vFilterParameters As Object
                Dim vFilterParms(1) As Object
                Dim vFilterParmsTest(2) As Object
                Dim lFilterId As Long
                Dim lNumPeriods As Long

                ' Set filter parameters to return interpolated data
                lFilterId = 1
                lNumPeriods = 200
                vFilterParms(0) = lFilterId
                vFilterParms(1) = lNumPeriods

                vFilterParmsTest(0) = 2
                vFilterParmsTest(1) = 120
                vFilterParmsTest(2) = True

                'vFilterParameters = vFilterParms
                vFilterParameters = vFilterParmsTest
                objDataAccess.SetFilterParameters(vFilterParameters)


                vData = objDataAccess.ReadValue
                'Pull range*********************************************

                TextBox4.Text = ""

                ' Loop through all the VTQs returned and store them
                If Not IsDBNull(vData) Then
                    ' Loop through the entire array and store the data
                    ' NB: In this sample only the last data in the Array is stored when we are out of the loop
                    For Count = 0 To UBound(vData(1, 0), 2)
                        vValue = vData(1, 0)(0, Count) ' Value
                        lQuality = vData(1, 0)(1, Count) ' Quality
                        dTimeStamp = vData(1, 0)(2, Count) ' Timestamp

                        TextBox4.Text &= "#" & (Count + 1).ToString() & "  :::::  " & vValue & " : " & lQuality & " : " & dTimeStamp & vbCrLf


                    Next Count
                End If

                'ExaqData = objDataAccess.ReadValue
                'tmpData = ExaqData(1, 0)(0, 0)
                'TextBox1.Text = tmpData
                'TextBox3.Text = ExaqData(1, 0)(2, 0)
                'objDataAccess.Remove(objDataAccess.Item(0))
            Else
                lblError.Text = "Error: Path is invalid "
            End If
        Catch ex As Exception
            lblError.Text = "Error: " & ex.Message
        End Try

        Me.Enabled = True
    End Sub

    Private Sub btnRBNS_Click(sender As System.Object, e As System.EventArgs) Handles btnRBNS.Click
        Dim sPath As String
        Dim sFilter As String
        Dim sItemPath As String
        Dim objRBNSBrowse As QUANTUMAUTOMATIONLib.RBNSBrowseEx
        Dim rsRBNSBrowseResults As ADODB.Recordset
        objRBNSBrowse = New QUANTUMAUTOMATIONLib.RBNSBrowseEx

        Me.Enabled = False
        TextBox4.Text = ""
        lblError.Text = ""
        ' Specify the retrieved folder
        sPath = txtRBNS.Text ' "PTTGSP.GSP1.GC ONLINE.SALES GAS" '"PTTGSP.GSP1" ' "PTTGSP.GSP1.GC ONLINE.SALES GAS.700QR-2N2.PV.Value" '"MyNamespace.Folder1"
        ' Specify the filter
        sFilter = "*"
        Try
            ' Get the tags in the specified folder
            rsRBNSBrowseResults = objRBNSBrowse.Browse(sPath, sFilter, QUANTUMAUTOMATIONLib.brFields.brDetail)
            rsRBNSBrowseResults.MoveFirst()

            ' Iterate through the returned Recordset
            While (rsRBNSBrowseResults.EOF <> True)
                sItemPath = rsRBNSBrowseResults.Fields("Path").Value
                TextBox4.Text &= "#" & sItemPath & vbCrLf
                ' Add the code using sItemPath here
                rsRBNSBrowseResults.MoveNext()
            End While
        Catch ex As Exception
            lblError.Text = "Error: " & ex.Message
        End Try
        Me.Enabled = True
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim objMDA As QUANTUMAUTOMATIONLib.QDataAccessMulti
        Dim sItem As String
        Dim dQueryTime As Date
        Dim dQueryTime1 As Date
        ' Dimension the filter parameters to return interpolated data
        Dim vFilterParameters As Object
        Dim vFilterParms(1) As Object
        Dim vFilterParmsTest(2) As Object
        Dim lFilterId As Long
        Dim lNumPeriods As Long
        ' Dimension variable to receive the data
        Dim vData As Object
        Dim vValue As Object
        Dim vQuality As Object
        Dim vTimestamp As Object

        TextBox4.Text = ""
        lblError.Text = ""
        Me.Enabled = False
        Try

            
            ' Items are requested by using Role-Based Namespace Access strings
            sItem = TextBox2.Text ' "PTTGSP.GSP1.GC ONLINE.SALES GAS.700QR-2N2.PV.Value" '"MyNamespace.Tag1.Value"

            ' Create new Exaquantum QDataAccessMulti object
            objMDA = New QUANTUMAUTOMATIONLib.QDataAccessMulti
            objMDA.Add(sItem)
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3504-QT-001 RVP.3504QT001GCIU.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.3504.FLOW.3504FI005.PV.Value")


            dQueryTime = dtpInputDate.Value.AddMinutes(-20)
            dQueryTime1 = dtpInputDate.Value
            objMDA.SetQueryTimes(StartTime:=dQueryTime, EndTime:=dQueryTime1)
            ' Set filter parameters to return interpolated data
            lFilterId = 1
            lNumPeriods = 200 

            vFilterParmsTest(0) = 2
            vFilterParmsTest(1) = 120
            vFilterParmsTest(2) = True

            vFilterParameters = vFilterParmsTest

            objMDA.SetFilterParameters(vFilterParameters)
            ' Read filtered data from yesterday to now
              

            'Dim data As Object
            vData = objMDA.ReadValue()
            'Iterate through the returned data retrieving the history values
            Dim lNumberOfPoints As Long
            Dim lPoint As Long
            lNumberOfPoints = UBound(vData(1, 0), 2)
            For lPoint = 0 To lNumberOfPoints
                vValue = vData(1, 0)(0, lPoint)
                vQuality = vData(1, 0)(1, lPoint)
                vTimestamp = vData(1, 0)(2, lPoint)

                TextBox4.Text &= "#" & (lPoint + 1).ToString() & "  :::::  " & vValue & " : " & vQuality & " : " & vTimestamp & vbCrLf
            Next
        Catch ex As Exception
            lblError.Text = "Error: " & ex.Message
        End Try

        objMDA = Nothing

        Me.Enabled = True
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        Dim objMDA As QUANTUMAUTOMATIONLib.QDataAccessMulti
        Dim sItem As String
        Dim dQueryTime As Date
        Dim dQueryTime1 As Date
        ' Dimension the filter parameters to return interpolated data
        Dim vFilterParameters As Object
        Dim vFilterParms(1) As Object
        Dim vFilterParmsTest(2) As Object
        Dim lFilterId As Long
        Dim lNumPeriods As Long
        ' Dimension variable to receive the data
        Dim vData As Object
        Dim vValue As Object
        Dim vQuality As Object
        Dim vValue1 As Object
        Dim vQuality1 As Object
        Dim vTimestamp As Object

        Dim dateTime1 As Date

        TextBox4.Text = ""
        lblError.Text = ""
        Me.Enabled = False

        WriteToFile("Started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Try


            ' Items are requested by using Role-Based Namespace Access strings
            sItem = TextBox2.Text ' "PTTGSP.GSP1.GC ONLINE.SALES GAS.700QR-2N2.PV.Value" '"MyNamespace.Tag1.Value"

            ' Create new Exaquantum QDataAccessMulti object
            objMDA = New QUANTUMAUTOMATIONLib.QDataAccessMulti
            'objMDA.Add(sItem)
            objMDA.Add("PTTGSP.GSP5.GC ONLINE.3504-QT-001 RVP.3504QT001GCIU.PV.Value")
            objMDA.Add("PTTGSP.GSP5.3504.FLOW.3504FI005.PV.Value")
            objMDA.Add("PTTGSP.GSP5.GC ONLINE.3504-QT-001 RVP.3504QT001GCIU.PV.Value")
            objMDA.Add("PTTGSP.GSP5.3504.FLOW.3504FI005.PV.Value")

            dateTime1 = DateTime.Today
            dQueryTime = dateTime1.AddDays(-1) ' dtpInputDate.Value.AddMinutes(-20) 'dateTime1.AddMinutes(-20) 'dtpInputDate.Value.AddMinutes(-20) '(-1440) '1440 = 1days
            dQueryTime1 = dateTime1 'dtpInputDate.Value ' dateTime1 ' dtpInputDate.Value
            objMDA.SetQueryTimes(StartTime:=dQueryTime, EndTime:=dQueryTime1)
            ' Set filter parameters to return interpolated data
            lFilterId = 1
            lNumPeriods = 200

            vFilterParmsTest(0) = 2
            vFilterParmsTest(1) = 120
            vFilterParmsTest(2) = True

            vFilterParameters = vFilterParmsTest

            objMDA.SetFilterParameters(vFilterParameters)
            ' Read filtered data from yesterday to now


            'Dim data As Object
            vData = objMDA.ReadValue()
            'Iterate through the returned data retrieving the history values
            Dim lNumberOfPoints As Long
            Dim lmaxColume As Long
            Dim lPoint As Long

            Dim writeData As String

            lNumberOfPoints = UBound(vData(1, 0), 2)
            lmaxColume = UBound(vData, 2)
            'Write header'
            writeData = "Order, Date"
            For ColumePoint = 0 To lmaxColume
                writeData = writeData & "," & vData(0, ColumePoint)
            Next
             
           
            For lPoint = 0 To lNumberOfPoints
                vValue = vData(1, 0)(0, lPoint)
                vQuality = vData(1, 0)(1, lPoint)
                vTimestamp = vData(1, 0)(2, lPoint)


                vValue1 = vData(1, 1)(0, lPoint)
                vQuality1 = vData(1, 1)(1, lPoint)

                'TextBox4.Text &= "#" & (lPoint + 1).ToString() & "  :::::  " & vValue & " : " & vQuality & " : " & vTimestamp & vbCrLf

                WriteToFile("#" & (lPoint + 1).ToString() & "::" & (vTimestamp).ToString() & "::" & vValue & "::" & vValue1)
            Next
        Catch ex As Exception
            lblError.Text = "Error: " & ex.Message
            WriteToFile(lblError.Text)
        End Try

        objMDA = Nothing
        WriteToFile("Stop at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.Enabled = True
    End Sub


    Private Sub WriteToFile(text As String)
        Dim dirName As String
        Dim path As String
        Try
            dirName = Directory.GetCurrentDirectory()

            path = dirName & "\\" & DateTime.Now.ToString("yyyyMMdd") & ".log"

            Using writer As New StreamWriter(path, True)
                writer.WriteLine(text)
                writer.Close()
            End Using
        Catch ex As Exception
            lblError.Text = "Error: " & ex.Message
        End Try

    End Sub
End Class
