﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.Model;
using QMSSystem.CoreDB.Services;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_CorrectData }, DataAccess = DataAccessEnum.Authen)]
    public class CorrectDataController : BaseController
    {
        //
        // GET: /CorrectData/

        public ActionResult ListMasterCorrect()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CorrectDataSearchModel searchCorrectData = new CorrectDataSearchModel();
            CreateQMS_MA_CORRECT_DATA createQMS_MA_CORRECT_DATA = new CreateQMS_MA_CORRECT_DATA();


            ViewBag.listMultiplicand = this.getMultiplicandList(false);
            ViewBag.listDenominator = this.getDenominatorList(false);
            ViewBag.listOFF_CONTROL_CAL_METHOD = this.getOFF_CONTROL_CAL_METHODList();

            ViewBag.listMultiplicand2 = this.getMultiplicandList(true);
            ViewBag.listDenominator2 = this.getDenominatorList(true);


            ViewBag.searchCorrectData = searchCorrectData;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_CORRECT_DATA = createQMS_MA_CORRECT_DATA;

            return View();
        }

        public ActionResult CreateMasterCorrect(long id=0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CORRECT_DATA createQMS_MA_CORRECT_DATA = new CreateQMS_MA_CORRECT_DATA();
            string actionName = Resources.Strings.AddNew;
             

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_CORRECT_DATA = masterDataServices.getQMS_MA_CORRECT_DATAById(_db, id);
            }

            ViewBag.listMultiplicand = this.getMultiplicandList(false);
            ViewBag.listDenominator = this.getDenominatorList(false);

            ViewBag.listMultiplicand2 = this.getMultiplicandList(true);
            ViewBag.listDenominator2 = this.getDenominatorList(true);

            ViewBag.listOFF_CONTROL_CAL_METHOD = this.getOFF_CONTROL_CAL_METHODList();
            ViewBag.createQMS_MA_CORRECT_DATA = createQMS_MA_CORRECT_DATA;
            ViewBag.ActionName = actionName;            
            return View();
        }

        public JsonResult CreateQMS_MA_CORRECT_DATA(CreateQMS_MA_CORRECT_DATA model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {

                    //long[] ids = new long[] { model.ID };
                    //bool bResult = false;
                    
                    //if(model.ID > 0)
                    //    bResult = masterDataServices.IsMasterCorrectData(ids);

                    if (model.PRIORITY > 6)
                    {
                        long nResult = masterDataServices.SaveQMS_MA_CORRECT_DATA(_db, model);

                        if (nResult > 0)
                        {
                            model = masterDataServices.getQMS_MA_CORRECT_DATAById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.PriorityLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_CORRECT_DATABySearch(CorrectDataSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username); 

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CORRECT_DATA> listData = new List<ViewQMS_MA_CORRECT_DATA>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_CORRECT_DATA(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteQMS_MA_CORRECT_DATA(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);
                TransactionService transService = new TransactionService(_username);

                try
                {
                    bool nResult = false;

                    nResult = masterDataServices.IsMasterCorrectData(ids);

                    if (true == nResult)
                    { 
                        result = new { result = ReturnStatus.ERROR, message = masterDataServices.getError() };
                    }
                    else
                    {
                        nResult = transService.IsCorrectDataInOffControlRecord(_db, ids);

                        if (true == nResult)
                        {

                            nResult = masterDataServices.DeleteQMS_MA_CORRECT_DATAByListId(_db, ids);

                            if (true == nResult)
                            {
                                result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                            }
                            else
                            {
                                result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                            }
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.CorrectDataInUse };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
