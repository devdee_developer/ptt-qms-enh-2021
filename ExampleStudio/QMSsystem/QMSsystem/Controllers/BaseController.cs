﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using QMSsystem.Models.Helper;
using QMSSystem.CoreDB;
using QMSSystem.Model;
using QMSSystem.CoreDB.Services;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Web.Routing;
using QMSsystem.Models.Privilege;
using System.Net;

namespace QMSsystem.Controllers
{
    public class BaseController : Controller
    {
        public string _username = "";
        protected BaseFont bf;

        protected BaseColor dBlue;
        protected BaseColor bWhite;
        protected BaseColor bGray;
        protected BaseColor bGreen;
        protected BaseColor bHeaderExcel;
        protected BaseColor bHeaderExcel2;

        // สร้าง Font จาก BaseFont
        protected Font fntHeader;
        protected Font fnt14;
        protected Font fnt12;
        protected Font fnt12White;
        protected Font fnt12Red;
        protected Font fnt11BOLD;
        protected Font fntUnderline;

        private int tblCount;

        protected QMSDBEntities _db = new QMSDBEntities();

        

        public BaseController()
        {
            try
            {
                //string url = Request.Url.AbsoluteUri;  // http://localhost:1302/TESTERS/Default6.aspx
                //string path =  Request.Url.AbsolutePath;  // /TESTERS/Default6.aspx
                //string host = Request.Url.Host; // localhost
               // string strSavePath = WebContext.GetImageBackgroudPath;

                SiteSession sessionData = (this.Session == null) ? null : (SiteSession)this.Session["SiteSession"];

                if (null != sessionData && null != sessionData.Username) {
                    _username =  sessionData.UserShowName ; 
                }

                ViewBag.UserLoginName = _username;
                ViewBag.ImagePath = "";
                ViewBag.DeptName = "";

                if (null != sessionData && null != sessionData.Username)
                {
                    ViewBag.ImagePath = (null != sessionData.ImagePath && "" != sessionData.ImagePath) ? sessionData.ImagePath : "./Content/images/qms/img.jpg";
                    ViewBag.DeptName = (null != sessionData.DeptName && "" != sessionData.DeptName) ? sessionData.DeptName : "";
                }
            }
            catch (Exception ex)
            {
                 
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
              
            //Redirect or return a view, but not both.
            //filterContext.Result = RedirectToAction("Index", "ErrorHandler");
            // OR 
            filterContext.Result = new ViewResult
            {
                ViewName = "~/Views/Shared/Error404.cshtml"
            };
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.UserLoginName = "";
            try
            {
                //string strSavePath = WebContext.GetImageBackgroudPath; // localhost
                SiteSession sessionData = (this.Session["SiteSession"] == null) ? null : (SiteSession)this.Session["SiteSession"];
                if (null != sessionData && null != sessionData.Username)
                {
                    _username = sessionData.UserShowName; 
                }

                ViewBag.UserLoginName = _username;
                ViewBag.ImagePath = "";
                ViewBag.DeptName = "";

                if (null != sessionData && null != sessionData.Username)
                {
                    ViewBag.ImagePath = (null != sessionData.ImagePath && "" != sessionData.ImagePath) ? sessionData.ImagePath :  "./Content/images/qms/img.jpg";
                    ViewBag.DeptName = (null != sessionData.DeptName && "" != sessionData.DeptName) ? sessionData.DeptName : "";
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void initialFont()
        {
            bf = BaseFont.CreateFont(Server.MapPath("~/Content/fonts/THSarabun.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            dBlue = new BaseColor(0, 0, 128);
            bWhite = new BaseColor(255, 255, 255);
            bGray = new BaseColor(219, 219, 219);
            bGreen = new BaseColor(0, 255, 0);
            bHeaderExcel = new BaseColor(66, 86, 104);
            bHeaderExcel2 = new BaseColor(51, 122, 183);
            // สร้าง Font จาก BaseFont
            fnt14 = new Font(bf, 14);
            fnt12 = new Font(bf, 12);
            fnt12Red = new Font(bf, 12); 
            fnt12Red.Color = BaseColor.RED; 
            fntHeader = new Font(bf, 14, Font.BOLD | Font.UNDERLINE);
            fnt11BOLD = new Font(bf, 14, Font.BOLD);
            fntUnderline = new Font(bf, 14, Font.UNDERLINE);

            fnt12White = new Font(bf, 12, Font.BOLD); 
            fnt12White.Color = BaseColor.WHITE;

            tblCount = 0;
        }
          
        protected string getModelStateError(ModelStateDictionary ModelState)
        {
            string result = "";
            try
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();
                result = "System failure : " + errorList[0].ToString();
            }
            catch (Exception ex)
            {
                result = this.getEexceptionError(ex);
            }

            return result;
        }

        public string getEexceptionError(Exception ex)
        {
            string result = "";

            if (ex.InnerException != null)
            {
                result = ex.Message + ex.InnerException.Message;
            }
            else
            {
                result = ex.Message;
            }

            LogHelper.Log(ex.Message);

            return result;
        }

        public void MakeFolderExist(String folderPath)
        {
            DirectoryInfo di = new DirectoryInfo(folderPath);
            try
            {
                if (!di.Exists) //not exitsts
                {
                    di.Create(); //create new....  
                }
            }
            catch
            {
                //do something........
            }
        }

        public void DeleteFile(String path)
        {
            try
            {
                System.IO.File.Delete(path);
            }
            catch (Exception ex)
            {
                string error = getEexceptionError(ex);
            }
        }

        public string MoveFile(string sourcePath, string desPath, string filename, string desName)
        {
            string strActualFile = "";
            try
            {

                string strTempFile = sourcePath + filename;

                if (System.IO.File.Exists(strTempFile))
                {
                    strActualFile = desPath + desName;

                    this.MakeFolderExist(desPath);

                    System.IO.File.Move(strTempFile, strActualFile);
                    System.IO.File.Delete(strTempFile);
                }
            }
            catch (Exception ex)
            {
                strActualFile = this.getEexceptionError(ex);
            }
            return strActualFile;
        }

        public string CopyTempToPrFolder(string sourcePath, string desPath, string filename)
        {
            string strActualFile = "";
            try
            {

                string strTempFile = sourcePath + filename;

                if (System.IO.File.Exists(strTempFile))
                {
                    strActualFile = desPath + filename;

                    this.MakeFolderExist(desPath);

                    System.IO.File.Move(strTempFile, strActualFile);
                    System.IO.File.Delete(strTempFile);
                }
            }
            catch (Exception ex)
            {
                strActualFile = this.getEexceptionError(ex);
            }
            return strActualFile;
        }

        public List<ViewQMS_MA_COA_PRODUCT> getMockProduct()
        {

            List<ViewQMS_MA_COA_PRODUCT> listProduct = new List<ViewQMS_MA_COA_PRODUCT>();

             for (int i = 1; i < 6; i++)
            {
                ViewQMS_MA_COA_PRODUCT temp = new ViewQMS_MA_COA_PRODUCT();
                temp.ID = i;
                temp.NAME = "GSP" + i;

                listProduct.Add(temp);
            }

             return listProduct;
        }

        protected List<SelectListItem> getPageSizeList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>(); 
            
            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = "20", Text = @Resources.Strings.Item20, Selected = true });
            listResult.Add(new SelectListItem { Value = "30", Text = @Resources.Strings.Item30 });
            listResult.Add(new SelectListItem { Value = "50", Text = @Resources.Strings.Item50 });
            listResult.Add(new SelectListItem { Value = "100", Text = @Resources.Strings.Item100 });

             
            return listResult;
        }

        protected List<SelectListItem> getROOT_CAUSE_TYPEList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)ROOT_CAUSE_TYPE.MAN).ToString(), Text = @Resources.Strings.MAN, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)ROOT_CAUSE_TYPE.MACHINE).ToString(), Text = @Resources.Strings.MACHINE });
            listResult.Add(new SelectListItem { Value = ((int)ROOT_CAUSE_TYPE.MATERIAL).ToString(), Text = @Resources.Strings.MATERIAL });
            listResult.Add(new SelectListItem { Value = ((int)ROOT_CAUSE_TYPE.METHOD).ToString(), Text = @Resources.Strings.METHOD });
            listResult.Add(new SelectListItem { Value = ((int)ROOT_CAUSE_TYPE.MEASUREMENT).ToString(), Text = @Resources.Strings.MEASUREMENT });


            return listResult;
        }

        protected List<SelectListItem> getMultiplicandList(bool bCrossVolume)
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)MULTIPLICAND.COUNT_OUPPUT).ToString(), Text = @Resources.Strings.COUNT_OUPPUT, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)MULTIPLICAND.UNCOUNT_OUPPUT).ToString(), Text = @Resources.Strings.UNCOUNT_OUPPUT });
            
            if(bCrossVolume)
                listResult.Add(new SelectListItem { Value = ((int)MULTIPLICAND.MOVED_OUPPUT).ToString(), Text = @Resources.Strings.MOVED_OUPPUT });
             
            return listResult;
        }

        protected List<SelectListItem> getDenominatorList(bool bCrossVolume)
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)DENOMINATOR.COUNT_INPUT).ToString(), Text = @Resources.Strings.COUNT_INPUT, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)DENOMINATOR.UNCOUNT_INPUT).ToString(), Text = @Resources.Strings.UNCOUNT_INPUT });

            if (bCrossVolume)
                listResult.Add(new SelectListItem { Value = ((int)DENOMINATOR.MOVED_INPUT).ToString(), Text = @Resources.Strings.MOVED_INPUT });


            return listResult;
        }

        protected List<SelectListItem> getOFF_CONTROL_CAL_METHODList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)OFF_CONTROL_CAL_METHOD.NOT_CALUCATE).ToString(), Text = @Resources.Strings.NOT_CALUCATE, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)OFF_CONTROL_CAL_METHOD.NORMAL_CALUCATE).ToString(), Text = @Resources.Strings.NORMAL_CALUCATE });
            listResult.Add(new SelectListItem { Value = ((int)OFF_CONTROL_CAL_METHOD.OFFCONTROL_ON).ToString(), Text = @Resources.Strings.OFFCONTROL_ON });
            listResult.Add(new SelectListItem { Value = ((int)OFF_CONTROL_CAL_METHOD.OFFCONTROL_OFF).ToString(), Text = @Resources.Strings.OFFCONTROL_OFF });


            return listResult;
        }

        protected List<ViewQMS_MA_PLANT> getPlantForSearch()
        {
            List<ViewQMS_MA_PLANT> listResutl = new List<ViewQMS_MA_PLANT>();
            MasterDataService masterDataServices = new MasterDataService(_username);
            listResutl = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            return listResutl;
        }

        protected List<ViewQMS_MA_PRODUCT> getProductForSearch()
        {
            List<ViewQMS_MA_PRODUCT> listResutl = new List<ViewQMS_MA_PRODUCT>();
            MasterDataService masterDataServices = new MasterDataService(_username);
            listResutl = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            return listResutl;
        }

        protected List<ViewQMS_MA_GRADE> getGradeForSearch()
        {
            List<ViewQMS_MA_GRADE> listResutl = new List<ViewQMS_MA_GRADE>();
            MasterDataService masterDataServices = new MasterDataService(_username);
            listResutl = masterDataServices.getQMS_MA_GRADEActiveList(_db);
            return listResutl;
        }

        protected List<SelectListItem> getDocStatusList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.PENDING).ToString(), Text = @Resources.Strings.PENDING, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.CONFIRM).ToString(), Text = @Resources.Strings.CONFIRM });
            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.REJECT).ToString(), Text = @Resources.Strings.REJECT });

            return listResult;
        }

        protected string getDocStatusName(int id)
        {
            string name = "";

            if (id == ((int)DOC_STATUS.PENDING))
            {
                name = @Resources.Strings.PENDING;
            }
            else if (id == ((int)DOC_STATUS.CONFIRM))
            {
                name = @Resources.Strings.CONFIRM;
            }
            else
            {
                name = @Resources.Strings.REJECT;
            }
             
            return name;
        }

        protected List<SelectListItem> getDocStatusListWithInitail()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.INITIAL).ToString(), Text = @Resources.Strings.INITIAL });
            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.PENDING).ToString(), Text = @Resources.Strings.PENDING, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.CONFIRM).ToString(), Text = @Resources.Strings.CONFIRM });
            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.REJECT).ToString(), Text = @Resources.Strings.REJECT });
           

            return listResult;
        }

        protected List<SelectListItem> getReportDocStatusList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)REPORT_DOC_STATUS.DRAFT).ToString(), Text = @Resources.Strings.DRAFT, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)REPORT_DOC_STATUS.PUBLIC).ToString(), Text = @Resources.Strings.PUBLIC });

            return listResult;
        }

        protected List<SelectListItem> getTypeReportList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)OFF_TYPE.CONTROL).ToString(), Text = @Resources.Strings.ReportOffControl, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)OFF_TYPE.SPEC).ToString(), Text = @Resources.Strings.ReportOffSpec });

            return listResult;
        }

        protected List<SelectListItem> getAutoOffControlList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)AUTO_OFF_TYPE.INITIAL).ToString(), Text = @Resources.Strings.INITIAL });
            listResult.Add(new SelectListItem { Value = ((int)AUTO_OFF_TYPE.ALL_PENDING).ToString(), Text = @Resources.Strings.PENDING_ONLY, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)AUTO_OFF_TYPE.ALL_CONFIRM).ToString(), Text = @Resources.Strings.CONFIRM_ONLY });
            //listResult.Add(new SelectListItem { Value = ((int)AUTO_OFF_TYPE.CONFIRM_AND_PENDING).ToString(), Text = @Resources.Strings.PEND_CONFIRM_BOTH });

            return listResult;
        }

        protected List<SelectListItem> getTypeOffControlList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)OFF_TYPE.CONTROL).ToString(), Text = @Resources.Strings.TypeOffControl, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)OFF_TYPE.SPEC).ToString(), Text = @Resources.Strings.TypeOffSpec });

            return listResult;
        }

        protected List<ViewQMS_MA_CONTROL> getControlGroupList(byte gruopType)
        {
            List<ViewQMS_MA_CONTROL> listResult = new List<ViewQMS_MA_CONTROL>();
            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearch model = new ControlValueSearch();

            model.PageIndex = 0;
            model.PageSize = -1; // get All
            model.SortColumn = "NAME";
            model.SortOrder = "desc";

            model.mSearch = new ControlValueSearchModel();
            model.mSearch.GROUP_TYPE = gruopType;


            long count = 0;
            long totalPage = 0; 
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listResult = masterDataServices.searchQMS_MA_CONTROL(_db, model, out count, out totalPage, out listPageIndex);

            return listResult;
        }

        protected List<SelectListItem> getExaScheduleStatusList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.PENDING).ToString(), Text = @Resources.Strings.PENDING, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.CONFIRM).ToString(), Text = @Resources.Strings.CONFIRM });
            listResult.Add(new SelectListItem { Value = ((int)DOC_STATUS.REJECT).ToString(), Text = @Resources.Strings.Failure });

            return listResult;
        }


        protected List<SelectListItem> getTemplateTrendCOAStatusList()
        {
            List<SelectListItem> listResult = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            listResult.Add(new SelectListItem { Value = ((int)TEMPLATE_COA_DOC_STATUS.PENDING).ToString(), Text = @Resources.Strings.PENDING, Selected = true });
            listResult.Add(new SelectListItem { Value = ((int)TEMPLATE_COA_DOC_STATUS.COMPLETE).ToString(), Text = @Resources.Strings.CONFIRM });
            
            return listResult;
        }

        protected DateTime getDummyDateTime()
        {
            DateTime mDummy = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);             
            return mDummy;
        }

        protected DateTime resetSecond(DateTime data)
        {
            DateTime mDummy = new DateTime(data.Year, data.Month, data.Day, data.Hour, data.Minute, 0);
            return mDummy;
        }

        protected DateTime resetStartTime(DateTime data)
        {
            DateTime mDummy = new DateTime(data.Year, data.Month, data.Day, 0, 0, 0);
            return mDummy;
        }

        protected DateTime resetEndTime(DateTime data)
        {
            DateTime mDummy = new DateTime(data.Year, data.Month, data.Day, 23, 59, 59);
            return mDummy;
        }

        protected bool IsUrlExist(string url, int timeOutMs = 1000)
        {
            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Method = "HEAD";
            webRequest.Timeout = timeOutMs;

            try
            {
                var response = webRequest.GetResponse();
                /* response is `200 OK` */
                response.Close();
            }
            catch
            {
                /* Any other response */
                return false;
            }

            return true;
        }
    } 
}
