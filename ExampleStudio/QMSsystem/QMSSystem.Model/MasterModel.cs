﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace QMSSystem.Model
{
    class MasterModels
    {
    }

    //-----QMS_MA_COA_CUSTOMER
    public class CreateQMS_MA_COA_CUSTOMER
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_COA_CUSTOMER
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_COA_ITEM
    public class CreateQMS_MA_COA_ITEM
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_COA_ITEM
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_COA_PRODUCT
    public class CreateQMS_MA_COA_PRODUCT
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_COA_PRODUCT
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_CONTROL
    public class CreateQMS_MA_CONTROL
    {
        public long ID { get; set; }
        public byte GROUP_TYPE { get; set; }
        public string NAME { get; set; }
        public string CONTROL_DESC { get; set; }
        //public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_CONTROL
    {
        public long ID { get; set; }
        public byte GROUP_TYPE { get; set; }
        public string NAME { get; set; }
        public string CONTROL_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }
        //public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_CONTROL_COLUMN
    public class CreateQMS_MA_CONTROL_COLUMN
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public string ITEM { get; set; }
        public long UNIT_ID { get; set; }
        public short POSITION { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_CONTROL_COLUMN
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public string ITEM { get; set; }
        public string UNIT_NAME { get; set; }
        public long UNIT_ID { get; set; }

        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }
        public short POSITION { get; set; }
        //public byte DELETE_FLAG { get; set; }
        public bool ITEM_SELECTED { get; set; }
    }

    //-----QMS_MA_CONTROL_DATA
    public class CreateQMS_MA_CONTROL_DATA
    {
        public long ID { get; set; }
        //public bool SHOW_ON_TREND_FLAG { get; set; }
        public long CONTROL_ROW_ID { get; set; }
        public long CONTROL_COLUMN_ID { get; set; }
        public long CONTROL_ID { get; set; }
        public bool MAX_FLAG { get; set; }
        public decimal MAX_VALUE { get; set; }
        public bool MIN_FLAG { get; set; }
        public decimal MIN_VALUE { get; set; }
        public bool CONC_FLAG { get; set; }
        public decimal CONC_VALUE { get; set; }
        public bool LOADING_FLAG { get; set; }
        public decimal LOADING_VALUE { get; set; }

        public bool MAX_NO_CAL { get; set; }
        public bool MIN_NO_CAL { get; set; }
        public bool CONC_NO_CAL { get; set; }
        public bool LOADING_NO_CAL { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_CONTROL_DATA
    {
        public long ID { get; set; }
        //public bool SHOW_ON_TREND_FLAG { get; set; }
        public long CONTROL_ID { get; set; }
        public long CONTROL_ROW_ID { get; set; }
        public long CONTROL_COLUMN_ID { get; set; }
        public bool MAX_NO_CAL { get; set; }
        public bool MAX_FLAG { get; set; }
        public decimal MAX_VALUE { get; set; }
        public bool MIN_NO_CAL { get; set; }
        public bool MIN_FLAG { get; set; }
        public decimal MIN_VALUE { get; set; }
        public bool CONC_NO_CAL { get; set; }
        public bool CONC_FLAG { get; set; }
        public decimal CONC_VALUE { get; set; }
        public bool LOADING_NO_CAL { get; set; }
        public bool LOADING_FLAG { get; set; }
        public decimal LOADING_VALUE { get; set; }

        public bool CONC_LOADING_FLAG { get; set; }
        public string CONTROL_ROW_NAME { get; set; }
        public string CONTROL_COLUMN_NAME { get; set; }
        public string CONTROL_DATA_NAME { get; set; }
        public string UNIT_NAME { get; set; }
        public byte ITEM_TYPE { get; set; }
        public bool ITEM_SELECTED { get; set; }


        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }

        // public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_CONTROL_DATACONF
    public class CreateQMS_MA_CONTROL_DATACONF
    {
        public long ID { get; set; }
        public long CONTROL_ROW_ID { get; set; }
        public long CONTROL_COLUMN_ID { get; set; }
        public long CONTROL_ID { get; set; }
        public bool? MAX_FLAG { get; set; }
        public decimal? MAX_VALUE { get; set; }
        public bool? MIN_FLAG { get; set; }
        public decimal? MIN_VALUE { get; set; }
        public bool? CONC_FLAG { get; set; }
        public decimal? CONC_VALUE { get; set; }
        public bool? LOADING_FLAG { get; set; }
        public decimal? LOADING_VALUE { get; set; }
        public bool? MAX_NO_CAL { get; set; }
        public bool? MIN_NO_CAL { get; set; }
        public bool? CONC_NO_CAL { get; set; }
        public bool? LOADING_NO_CAL { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }
    }

    public class ViewQMS_MA_CONTROL_DATACONF
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public long CONTROL_ROW_ID { get; set; }
        public long CONTROL_COLUMN_ID { get; set; }
        public bool? MAX_NO_CAL { get; set; }
        public bool? MAX_FLAG { get; set; }
        public decimal? MAX_VALUE { get; set; }
        public bool? MIN_NO_CAL { get; set; }
        public bool? MIN_FLAG { get; set; }
        public decimal? MIN_VALUE { get; set; }
        public bool? CONC_NO_CAL { get; set; }
        public bool? CONC_FLAG { get; set; }
        public decimal? CONC_VALUE { get; set; }
        public bool? LOADING_NO_CAL { get; set; }
        public bool? LOADING_FLAG { get; set; }
        public decimal? LOADING_VALUE { get; set; }
        public bool? CONC_LOADING_FLAG { get; set; }
        public string CONTROL_ROW_NAME { get; set; }
        public string CONTROL_COLUMN_NAME { get; set; }
        public string CONTROL_DATA_NAME { get; set; }
        public string UNIT_NAME { get; set; }
        public byte ITEM_TYPE { get; set; }
        public bool ITEM_SELECTED { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public string START_DATE_SHOW { get; set; }
        public DateTime? START_DATE { get; set; }
        public string EXPIRE_DATE_SHOW { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }

    }


    //-----QMS_MA_CONTROL_ROW
    public class CreateQMS_MA_CONTROL_ROW
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public string SAMPLE { get; set; }
        public long PLANT_ID { get; set; }
        public long GRADE_ID { get; set; }

        //public bool ARROW_UP { get; set; }
        //public bool ARROW_DOWN { get; set; }
        public short POSITION { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_CONTROL_ROW
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public string SAMPLE { get; set; }
        public long PLANT_ID { get; set; }
        public long GRADE_ID { get; set; }
        public bool ITEM_SELECTED { get; set; }
        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }
        public short POSITION { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_CORRECT_DATA
    public class CreateQMS_MA_CORRECT_DATA
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte CROSS_VOLUME_FLAG { get; set; }
        public byte DELETE_INPUT_QTY { get; set; }
        public byte DELETE_OUTPUT_QTY { get; set; }
        public byte CAL_OFF_CONTROL { get; set; }
        public byte SYSTEM_CONFIG_FLAG { get; set; }
        public Int32 PRIORITY { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_CORRECT_DATA
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte CROSS_VOLUME_FLAG { get; set; }
        public byte DELETE_INPUT_QTY { get; set; }
        public byte DELETE_OUTPUT_QTY { get; set; }
        public byte CAL_OFF_CONTROL { get; set; }


        public string CROSS_VOLUME_FLAG_NAME { get; set; }
        public string DELETE_INPUT_QTY_NAME { get; set; }
        public string DELETE_OUTPUT_QTY_NAME { get; set; }
        public string CAL_OFF_CONTROL_NAME { get; set; }

        public byte SYSTEM_CONFIG_FLAG { get; set; }
        public Int32 PRIORITY { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_DOWNTIME
    public class CreateQMS_MA_DOWNTIME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string EXCEL_NAME { get; set; }
        public decimal LIMIT_VALUE { get; set; }
        public long UNIT_ID { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string DOWNTIME_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public long DOWNTIME_DETAIL_ID { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public decimal CONVERT_VALUE { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_DOWNTIME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public string EXA_TAG_NAME { get; set; }//JOIN WITH QMS_MA_REDUCE_FEED_DETAIL
        public string EXCEL_NAME { get; set; }
        public decimal LIMIT_VALUE { get; set; }
        public long UNIT_ID { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string DOWNTIME_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

    }

    //-----QMS_MA_DOWNTIME_DETAIL
    public class CreateQMS_MA_DOWNTIME_DETAIL
    {
        public long ID { get; set; }
        public long DOWNTIME_ID { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public decimal CONVERT_VALUE { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_DOWNTIME_DETAIL
    {
        public long ID { get; set; }
        public long DOWNTIME_ID { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public decimal CONVERT_VALUE { get; set; }
        public byte DELETE_FLAG { get; set; }
        public decimal MIN_LIMIT { get; set; }
    }

    //-----QMS_MA_EMAIL
    public class CreateQMS_MA_EMAIL
    {
        public long ID { get; set; }
        public byte GROUP_TYPE { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string NAME { get; set; }
        public string POSITION { get; set; }
        public string EMAIL { get; set; }
        public string UNIT_NAME { get; set; }
        public string DEPARTMENT { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_EMAIL
    {
        public long ID { get; set; }
        public byte GROUP_TYPE { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string NAME { get; set; }
        public string POSITION { get; set; }
        public string EMAIL { get; set; }
        public string UNIT_NAME { get; set; }
        public string DEPARTMENT { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_EXQ_ACCUM_TAG
    public class CreateQMS_MA_EXQ_ACCUM_TAG
    {
        public long ID { get; set; }
        public long EXA_TAG_ID { get; set; }//master exatag
        public long EXA_TAG_VALUE { get; set; }//detail member
        public decimal CONVERT_VALUE { get; set; }

        public string EXCEL_NAME { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_EXQ_ACCUM_TAG
    {
        public long ID { get; set; }
        public long EXA_TAG_ID { get; set; } //master exatag
        public long EXA_TAG_VALUE { get; set; } //detail member
        public decimal CONVERT_VALUE { get; set; }

        public string EXA_TAG_NAME { get; set; }
        public bool ITEM_SELECTED { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_EXQ_TAG
    public class CreateQMS_MA_EXQ_TAG
    {
        public long ID { get; set; }
        public long PRODUCT_MAPPING_ID { get; set; }
        public long UNIT_ID { get; set; }
        public long CONTROL_ID { get; set; } //QMS_MA_CONTROL_DATA ID
        public long SPEC_ID { get; set; } //QMS_MA_CONTROL_DATA ID
        public string EXCEL_NAME { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public string LAKE_TAG_NAME { get; set; }//เพิ่มตอน pop ต่อ DataLeak
        public byte TAG_TYPE { get; set; }
        public decimal TAG_FLOW_CONVERT_VALUE { get; set; }
        public decimal TAG_FLOW_CHECK_VALUE { get; set; }
        public byte TAG_FLOW_CHECK { get; set; }
        public byte TAG_TARGET_CHECK { get; set; }
        public decimal TAG_TARGET_MIN { get; set; }
        public decimal TAG_TARGET_MAX { get; set; }
        public byte TAG_CORRECT { get; set; }
        public decimal TAG_CORRECT_MIN { get; set; }
        public decimal TAG_CORRECT_MAX { get; set; }

        public short POSITION { get; set; }
        // public byte DELETE_FLAG { get; set; }

        public List<CreateQMS_MA_EXQ_ACCUM_TAG> lstAccumTag { get; set; }
        public List<long> lstDelAccumTag { get; set; }


        public byte CHANGE_CONTROL_CHECK { get; set; }
        public byte CHANGE_SPEC_CHECK { get; set; }
        public long CHANGE_CONTROL_ID { get; set; } //QMS_MA_CONTROL_DATA ID
        public long CHANGE_SPEC_ID { get; set; } //QMS_MA_CONTROL_DATA ID 
    }

    public class ViewQMS_MA_EXQ_TAG
    {
        public long ID { get; set; }
        public long PRODUCT_MAPPING_ID { get; set; }
        public long UNIT_ID { get; set; }
        public long CONTROL_ID { get; set; } //QMS_MA_CONTROL_DATA ID
        public long SPEC_ID { get; set; }
        public string EXCEL_NAME { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public string LAKE_TAG_NAME { get; set; }//เพิ่มตอน pop ต่อ DataLeak
        public byte TAG_TYPE { get; set; }
        public string SHOW_TAG_TYPE { get; set; }
        public decimal TAG_FLOW_CONVERT_VALUE { get; set; }
        public byte TAG_FLOW_CHECK { get; set; }
        public decimal TAG_FLOW_CHECK_VALUE { get; set; }
        public byte TAG_TARGET_CHECK { get; set; }
        public decimal TAG_TARGET_MIN { get; set; }
        public decimal TAG_TARGET_MAX { get; set; }
        public string SHOW_TAG_TARGET_VALUE { get; set; }
        public byte TAG_CORRECT { get; set; }
        public decimal TAG_CORRECT_MIN { get; set; }
        public decimal TAG_CORRECT_MAX { get; set; }
        public byte CHANGE_CONTROL_CHECK { get; set; }
        public byte CHANGE_SPEC_CHECK { get; set; }
        public long CHANGE_CONTROL_ID { get; set; } //QMS_MA_CONTROL_DATA ID
        public long CHANGE_SPEC_ID { get; set; } //QMS_MA_CONTROL_DATA ID 


        public string UNIT_NAME { get; set; }

        public long CONTROL_GROUP_ID { get; set; } //QMS_MA_CONTROL ID
        public long CONTROL_GROUP_SMAPLE_ID { get; set; } //QMS_MA_CONTROL_ROW ID
        public long CONTROL_GROUP_ITEM_ID { get; set; } //QMS_MA_CONTROL_COLUMN ID

        public string CONTROL_VALUE { get; set; } // แสดงผล control ในรูปแบบ text

        public long SPEC_GROUP_ID { get; set; } //QMS_MA_CONTROL ID
        public long SPEC_GROUP_SMAPLE_ID { get; set; } //QMS_MA_CONTROL_ROW ID
        public long SPEC_GROUP_ITEM_ID { get; set; } //QMS_MA_CONTROL_COLUMN ID

        public string SPEC_VALUE { get; set; } // แสดงผล control ในรูปแบบ text


        public long CONTROL_GROUP_ID2 { get; set; } //QMS_MA_CONTROL ID2
        public long CONTROL_GROUP_SMAPLE_ID2 { get; set; } //QMS_MA_CONTROL_ROW ID2
        public long CONTROL_GROUP_ITEM_ID2 { get; set; } //QMS_MA_CONTROL_COLUMN ID2
        public long SPEC_GROUP_ID2 { get; set; } //QMS_MA_CONTROL ID2
        public long SPEC_GROUP_SMAPLE_ID2 { get; set; } //QMS_MA_CONTROL_ROW ID2
        public long SPEC_GROUP_ITEM_ID2 { get; set; } //QMS_MA_CONTROL_COLUMN ID2

        public string SHOW_PROOF_VALUE { get; set; } //แสดง proof value
        public string SHOW_CONVERT_VAlUE { get; set; } //แสดง convert value 

        public short POSITION { get; set; }
        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }

        public bool ITEM_SELECTED { get; set; }


        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_GRADE
    public class CreateQMS_MA_GRADE
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public Int16 EXCEL_FLAG { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_GRADE
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public Int16 EXCEL_FLAG { get; set; }
        public bool ITEM_SELECTED { get; set; }
        //public byte DELETE_FLAG { get; set; }

    }

    //-----QMS_MA_KPI
    public class CreateQMS_MA_KPI
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public decimal START_VALUE_5 { get; set; }
        public decimal END_VALUE_5 { get; set; }
        public decimal START_VALUE_4 { get; set; }
        public decimal END_VALUE_4 { get; set; }
        public decimal START_VALUE_3 { get; set; }
        public decimal END_VALUE_3 { get; set; }
        public decimal START_VALUE_2 { get; set; }
        public decimal END_VALUE_2 { get; set; }
        public decimal START_VALUE_1 { get; set; }
        public decimal END_VALUE_1 { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string KPI_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_KPI
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public decimal START_VALUE_5 { get; set; }
        public decimal END_VALUE_5 { get; set; }
        public decimal START_VALUE_4 { get; set; }
        public decimal END_VALUE_4 { get; set; }
        public decimal START_VALUE_3 { get; set; }
        public decimal END_VALUE_3 { get; set; }
        public decimal START_VALUE_2 { get; set; }
        public decimal END_VALUE_2 { get; set; }
        public decimal START_VALUE_1 { get; set; }
        public decimal END_VALUE_1 { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string KPI_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_OPERATION_SHIFT
    public class CreateQMS_MA_OPERATION_SHIFT
    {
        public long ID { get; set; }
        public short YEAR { get; set; }
        public byte MONTH { get; set; }
        public string SHIFT_1 { get; set; }
        public string SHIFT_2 { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_OPERATION_SHIFT
    {
        public long ID { get; set; }
        public Int16 YEAR { get; set; }
        public byte MONTH { get; set; }
        public string SHIFT_1 { get; set; }
        public string SHIFT_2 { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ColumnShiftData
    {
        public long ID { get; set; }
        public int YEAR { get; set; }
        public byte MONTH { get; set; }
        public byte SHIFT { get; set; } // 0:shift1 , 1:shift2

        public string SHIFT_NAME { get; set; }
        public string SHOW_DATE { get; set; }
        public byte DATE { get; set; } //วันที่
        public string columnData { get; set; }
        public byte columnType { get; set; } // 1: text , 2: data 
    }

    public class ShiftTableData
    {
        public List<List<ColumnShiftData>> tableData { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ShiftYear
    {
        public int YEAR { get; set; }
        public string YEAR_NAME { get; set; }
    }

    //public class ViewOperationShiftTable
    //{
    //    public long ID { get; set; } 
    //    public short YEAR { get; set; }
    //    public byte MONTH { get; set; }        
    //    public byte SHIFT { get; set; } // 0:shift1 , 1:shift2
    //    public List<ColumnShiftData> COLUMN_DATA { get; set; }
    //}

    //-----QMS_MA_PLANT
    public class CreateQMS_MA_PLANT
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public String ABBREVIATION { get; set; }
        public Int16 POSITION { get; set; }
        //public byte DELETE_FLAG { get; set; } // user ไม่จำเป็นต้องรู้ 
    }

    public class ViewQMS_MA_PLANT
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public String ABBREVIATION { get; set; }
        public Int16 POSITION { get; set; }

        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }

        public bool ITEM_SELECTED { get; set; }
        //public byte DELETE_FLAG { get; set; } // user ไม่จำเป็นต้องรู้ 
    }

    //-----QMS_MA_PRODUCT
    public class CreateQMS_MA_PRODUCT
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public Int16 POSITION { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_PRODUCT
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public Int16 POSITION { get; set; }

        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }

        public bool ITEM_SELECTED { get; set; }
        // public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_PRODUCT_MAPPING
    public class CreateQMS_MA_PRODUCT_MAPPING
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public long GRADE_ID { get; set; }
        public string PRODUCT_DESC { get; set; }

        public bool HIGHLOW_CHECK { get; set; }
        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_PRODUCT_MAPPING
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public long GRADE_ID { get; set; }

        public string PLANT_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string GRADE_NAME { get; set; }

        public string PRODUCT_DESC { get; set; }

        public bool HIGHLOW_CHECK { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public DateTime UPDATE_DATE { get; set; }

        public string SHOW_CREATE_DATE { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewPRODUCT_MAP_WITH_ORDERPRODUCT : ViewQMS_MA_PRODUCT_MAPPING
    {
        public short POSITION { get; set; }
    }


    public class ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING : ViewQMS_MA_EXQ_TAG
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public long CONTROL_ID { get; set; }
    }

    public class OFF_CONTROL_CAL_LIST
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public List<OFF_CONTROL_CAL_DATA> CONTROL_VALUE_DATA { get; set; }
        public decimal VOLUMN { get; set; }
        public decimal PV_FLOW { get; set; }
        public string CONTROL_VALUE { get; set; } //แยกเป็น control , spec, gc error ดีกว่า
        public string SPEC_VALUE { get; set; }
        public string GC_ERROR { get; set; }
        public string OFF_CONTROL_DESC { get; set; }

        public int RUN_NUMBBER { get; set; }
        public byte STATUS { get; set; }
        //public long ROOT_CAUSE_ID { get; set; } 
    }

    public class OFF_CONTROL_CAL_DATA
    {
        public string NAME { get; set; }
        public decimal TAG_VAL { get; set; }
        public byte ERROR { get; set; }
    }

    //-----QMS_MA_REDUCE_FEED
    public class CreateQMS_MA_REDUCE_FEED
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string EXCEL_NAME { get; set; }
        public decimal LIMIT_VALUE { get; set; }
        public long UNIT_ID { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string REDUCE_FEED_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }


        public long REDUCE_FEED_DETAIL_ID { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public decimal CONVERT_VALUE { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_REDUCE_FEED
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public string EXA_TAG_NAME { get; set; }//JOIN WITH QMS_MA_REDUCE_FEED_DETAIL
        public string EXCEL_NAME { get; set; }
        public decimal LIMIT_VALUE { get; set; }
        public long UNIT_ID { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string REDUCE_FEED_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public decimal CONVERT_VALUE { get; set; }
    }

    //-----QMS_MA_REDUCE_FEED_DETAIL
    public class CreateQMS_MA_REDUCE_FEED_DETAIL
    {
        public long ID { get; set; }
        public long REDUCE_FEED_ID { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public decimal CONVERT_VALUE { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_REDUCE_FEED_DETAIL
    {
        public long ID { get; set; }
        public long REDUCE_FEED_ID { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public decimal CONVERT_VALUE { get; set; }
        public byte DELETE_FLAG { get; set; }

        public decimal LIMIT_VALUE { get; set; }
    }

    //-----QMS_MA_ROOT_CAUSE
    public class CreateQMS_MA_ROOT_CAUSE
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public long ROOT_CAUSE_TYPE_ID { get; set; }
        public string ROOT_CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_ROOT_CAUSE
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public long ROOT_CAUSE_TYPE_ID { get; set; }
        public string ROOT_CAUSE_TYPE_NAME { get; set; }
        public string ROOT_CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_ROOT_CAUSE_TYPE
    public class CreateQMS_MA_ROOT_CAUSE_TYPE
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_ROOT_CAUSE_TYPE
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_TEMPLATE
    public class CreateQMS_MA_TEMPLATE
    {
        public long ID { get; set; }
        public byte TEMPLATE_TYPE { get; set; }
        public string TEMPLATE_AREA { get; set; }
        public string NAME { get; set; }
        public string FILE_NAME { get; set; }
        public string PATH_NAME { get; set; }
        public string SHEET_NAME { get; set; }
        public string EXCEL_DATE { get; set; }
        public string EXCEL_TIME { get; set; }
        public string TEMPLATE_DESC { get; set; }
        public bool? ALIGNMENT { get; set; }
        public string START_POINT { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public string EXCEL_AREA { get; set; }
    }

    public class ViewQMS_MA_TEMPLATE
    {
        public long ID { get; set; }
        public byte TEMPLATE_TYPE { get; set; }
        public string TEMPLATE_AREA { get; set; }
        //public string PLANT_NAME { get; set; }
        public string NAME { get; set; }
        public string FILE_NAME { get; set; }
        public string PATH_NAME { get; set; }
        public string SHEET_NAME { get; set; }
        public string EXCEL_DATE { get; set; }
        public string EXCEL_TIME { get; set; }
        public string TEMPLATE_DESC { get; set; }
        public bool? ALIGNMENT { get; set; }
        public string START_POINT { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public string EXCEL_AREA { get; set; }
    }

    //-----QMS_MA_TEMPLATE_COLUMN
    public class CreateQMS_MA_TEMPLATE_COLUMN
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string NAME { get; set; }
        public string EXCEL_FIELD { get; set; }
        public long UNIT_ID { get; set; }
        public long? SAMPLE_ID { get; set; }
        public string SAMPLE_NAME { get; set; }
        public string AREA_NAME { get; set; }
        ///public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_TEMPLATE_COLUMN
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string NAME { get; set; }
        public string EXCEL_FIELD { get; set; }
        public string EXCEL_COL { get; set; }
        public int EXCEL_ROW { get; set; }
        public long UNIT_ID { get; set; }
        public string UNIT_NAME { get; set; }
        public long? SAMPLE_ID { get; set; }
        public string SAMPLE_NAME { get; set; }
        public string AREA_NAME { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_TEMPLATE_CONTROL
    public class CreateQMS_MA_TEMPLATE_CONTROL
    {
        public long ID { get; set; }
        public long SAMPLE_ID { get; set; }
        public long ITEM_ID { get; set; }
        public string EXCEL_FIELD { get; set; }
        public long CONTROL_ID { get; set; }

        public long TEMPLATE_ID { get; set; }
        public bool SHOW_ON_TREND_FLAG { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_TEMPLATE_CONTROL
    {
        public long ID { get; set; }
        public long SAMPLE_ID { get; set; }
        public string SAMPLE_NAME { get; set; }
        public long ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public string EXCEL_FIELD { get; set; }
        public long CONTROL_ID { get; set; }

        public bool CONTROL_FLAG { get; set; } // true : มีค่า คอนโทรล false : ไม่มี ค่า คอนโทรล
        public bool SHOW_ON_TREND_FLAG { get; set; }

        public long TEMPLATE_ID { get; set; }

        public long CONTROL_GROUP_ID { get; set; } //QMS_MA_CONTROL ID
        public long CONTROL_GROUP_SMAPLE_ID { get; set; } //QMS_MA_CONTROL_ROW ID
        public long CONTROL_GROUP_ITEM_ID { get; set; } //QMS_MA_CONTROL_COLUMN ID

        public string CONTROL_GROUP_SMAPLE_NAME { get; set; }
        public string CONTROL_GROUP_ITEM_NAME { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    //-----QMS_MA_TEMPLATE_ROW
    public class CreateQMS_MA_TEMPLATE_ROW
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string NAME { get; set; }
        public string ITEM_NAME { get; set; }
        public string EXCEL_FIELD { get; set; }
        public string SAMPLING_POINT { get; set; }
        public string AREA_NAME { get; set; }
        public string EXCEL_FIELD_ITEM { get; set; }
        public string EXCEL_FIELD_AREA { get; set; }
        public long? ITEM_UNIT { get; set; }
        public string ITEM_UNIT_NAME { get; set; }
        public string EXCEL_DATE { get; set; }
        public string EXCEL_TIME { get; set; }
        public byte DELETE_FLAG { get; set; }
        public long TYPE_FLAG { get; set; }
    }

    public class ViewQMS_MA_TEMPLATE_ROW
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string NAME { get; set; }
        public string EXCEL_FIELD { get; set; }
        public string EXCEL_COL { get; set; }
        public int EXCEL_ROW { get; set; }
        public string EXCEL_COL_ITEM { get; set; }
        public int EXCEL_ROW_ITEM { get; set; }
        public string SAMPLING_POINT { get; set; }
        public byte bAREA_NAME { get; set; }
        public string AREA_NAME { get; set; }
        public string EXCEL_FIELD_ITEM { get; set; }
        public string EXCEL_FIELD_AREA { get; set; }
        public long? ITEM_UNIT { get; set; }
        public string ITEM_UNIT_NAME { get; set; }
        public string ITEM_NAME { get; set; }
        public string EXCEL_DATE { get; set; }
        public string EXCEL_TIME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }

    public class CreateQMS_MA_TEMPALTE_EXCEL
    {
        public CreateQMS_MA_TEMPLATE_ROW ROW_DATA { get; set; }
        public CreateQMS_MA_TEMPLATE_COLUMN COLUMN_DATA { get; set; }
        public CreateQMS_MA_TEMPLATE_CONTROL CONTROL_DATA { get; set; }

        public long TEMPLATE_ID { get; set; }
        public byte ITEM_TYPE { get; set; }
        public byte DATA_TYPE { get; set; }
        public string TD_NAME { get; set; }
    }

    public class ViewQMS_MA_TEMPALTE_EXCEL
    {
        public ViewQMS_MA_TEMPLATE_ROW ROW_DATA { get; set; }
        public ViewQMS_MA_TEMPLATE_COLUMN COLUMN_DATA { get; set; }
        public ViewQMS_MA_TEMPLATE_CONTROL CONTROL_DATA { get; set; }

        public byte ITEM_TYPE { get; set; }
        public byte DATA_TYPE { get; set; }
        public string TD_NAME { get; set; }
        public byte bAREA_NAME { get; set; }
    }

    //-----QMS_MA_UNIT
    public class CreateQMS_MA_UNIT
    {
        public long ID { get; set; }
        public string NAME { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        //public byte DELETE_FLAG { get; set; }
    }

    public class ViewQMS_MA_UNIT
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class NetColumnOffSummaryReport
    {
        public long ID { get; set; } // ID of column PLANT_ID
        public decimal VALUE { get; set; }
        public decimal COUNT { get; set; }
    }

    //------QMS_TR_PRODUCT_DATA
    public class AccumTagQMS_TR_PRODUCT_DATA
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public List<long> EXA_TAG_ID { get; set; } //ค่าที่จะนำมาบวกกัน -ขาดตัวคูณ
        public List<ViewQMS_MA_EXQ_ACCUM_TAG> ListAccum { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }

    //-----RESULT GROUP Accum  
    public class GROUP_OF_ACCUM
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime EXA_TAG_DATE { get; set; }
        public decimal EXA_TAG_VALUE { get; set; }
    }

    public class ViewQMS_RAW_DATA
    {
        public DateTime measured { get; set; }
        public DateTime sampling_time { get; set; }
        public string tag_name { get; set; }
        public string value { get; set; }
        public string status { get; set; }
    }

    public class ViewQMS_RAW_DATA_ENH
    {
        public DateTime sampling_time { get; set; }
        public DateTime recorded_time { get; set; } 
        public DateTime measured_time { get; set; }
        public string tag_name { get; set; }
        public string value { get; set; }
        public string status { get; set; }
    }

    public class ViewQMS_TR_EVENT_CAL
    {
        public long event_id { get; set; }
        public string event_type { get; set; }
        public string plant { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public decimal? total_event_time { get; set; }

    }

    public class ViewQMS_TR_EVENT_RAW_DATA
    {
        public long event_id { get; set; }
        public DateTime recorded_time { get; set; }
        public DateTime measured_time { get; set; }
        public string tag_name { get; set; }
        public string value { get; set; }
    }

    public class ViewRealTimeFromEVENT_RAW_DATA_NEW
    {
        public List<ViewQMS_TR_EVENT_RAW_DATA> viewQMS_TR_EVENT_RAW_DATAs = new List<ViewQMS_TR_EVENT_RAW_DATA>();
        public string error { get; set; }
    }

    public class CreateQMS_TR_EVENT_RAW_DATA
    {
        public long event_id { get; set; }
        public DateTime recorded_time { get; set; }
        public DateTime measured_time { get; set; }
        public string tag_name { get; set; }
        public string value { get; set; }
    }

    public class ViewQMS_TR_BLUEPRINT_ABNORMAL
    {
        public long BlueprintId { get; set; }
        public string Plant { get; set; }
        public string AbnormalType { get; set; }
        public int? EventId { get; set; }
        public string InputSource { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public float? TotalTimeHr { get; set; }
        public string Cause { get; set; }
        public string ShortCause { get; set; }
        public bool? Trip { get; set; }
        public bool? EquipmentFailure { get; set; }
        public string SD_Category { get; set; }
        public string Define { get; set; }
        public string SD_Reason { get; set; }
        public string EquipmentCode { get; set; }
        public string Source { get; set; }
        public string PlanType { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }


    //-----QMS_MA_IQC_RULE
    public class CreateQMS_MA_IQC_RULE
    {
        public long ID { get; set; }
        public string RULE_ID { get; set; }
        public string RULE_NAME { get; set; }
        public string RULE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_RULE
    {
        public long ID { get; set; }
        public string RULE_ID { get; set; }
        public string RULE_NAME { get; set; }
        public string RULE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

    }


    //-----QMS_MA_IQC_METHOD
    public class CreateQMS_MA_IQC_METHOD
    {
        public long ID { get; set; }
        public long METHOD_ID { get; set; }
        public string METHOD_NAME { get; set; }
        public string METHOD_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }
        
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_METHOD
    {
        public long ID { get; set; }
        public long METHOD_ID { get; set; }
        public string METHOD_NAME { get; set; }
        public string METHOD_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

    }


    //-----QMS_MA_IQC_POSSICAUSE
    public class CreateQMS_MA_IQC_POSSICAUSE
    {
        public long ID { get; set; }
        public byte POSSICAUSE_ID { get; set; }
        public string POSSICAUSE_NAME { get; set; }
        public string POSSICAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_POSSICAUSE
    {
        public long ID { get; set; }
        public byte POSSICAUSE_ID { get; set; }
        public string POSSICAUSE_NAME { get; set; }
        public string POSSICAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

        public bool ARROW_UP { get; set; }
        public bool ARROW_DOWN { get; set; }

        public bool ITEM_SELECTED { get; set; }

    }


    //-----QMS_MA_IQC_FORMULA
    public class CreateQMS_MA_IQC_FORMULA
    {
        public long ID { get; set; }
        public long METHOD_ID { get; set; }
        public string FORMULA_REV { get; set; }
        public string FILE_NAME { get; set; }
        public string METHOD_NAME { get; set; }
        public long FORMULA_PROD { get; set; }
        public string FORMULA_PRODNAME { get; set; }
        public decimal? FORMULA_SAMPLE { get; set; }
        public decimal? FORMULA_STD { get; set; }
        public decimal? FORMULA_UP { get; set; }
        public decimal? FORMULA_UCL { get; set; }
        public string UCL_PATTERN { get; set; }
        public decimal? FORMULA_UWL { get; set; }
        public string UWL_PATTERN { get; set; }
        public decimal? FORMULA_AVG { get; set; }
        public string AVG_PATTERN { get; set; }
        public decimal? FORMULA_LWL { get; set; }
        public string LWL_PATTERN { get; set; }
        public decimal? FORMULA_LCL { get; set; }
        public string LCL_PATTERN { get; set; }
        public decimal? FORMULA_LOW { get; set; }
        public string FORMULA_DESC { get; set; }

        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }

        public byte DELETE_FLAG { get; set; }


        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_FORMULA
    {
        public long ID { get; set; }
        public long METHOD_ID { get; set; }
        public string FORMULA_REV { get; set; }
        public string FORMULA_NAMEREV { get; set; }
        public string FILE_NAME { get; set; }
        public string METHOD_NAME { get; set; }
        public long FORMULA_PROD { get; set; }
        public string FORMULA_PRODNAME { get; set; }
        public decimal? FORMULA_SAMPLE { get; set; }
        public decimal? FORMULA_STD { get; set; }
        public decimal? FORMULA_UP { get; set; }
        public decimal? FORMULA_UCL { get; set; }
        public string UCL_PATTERN { get; set; }
        public decimal? FORMULA_UWL { get; set; }
        public string UWL_PATTERN { get; set; }
        public decimal? FORMULA_AVG { get; set; }
        public string AVG_PATTERN { get; set; }
        public decimal? FORMULA_LWL { get; set; }
        public string LWL_PATTERN { get; set; }
        public decimal? FORMULA_LCL { get; set; }
        public string LCL_PATTERN { get; set; }
        public decimal? FORMULA_LOW { get; set; }
        public string FORMULA_DESC { get; set; }

        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }

        public byte DELETE_FLAG { get; set; }

    }


    //-----QMS_MA_IQC_ALERT
    public class CreateQMS_MA_IQC_MAILALERT
    {
        public long ID { get; set; }
        public long EMAIL_ID { get; set; } // ID form QMS_MA_EMAIL
        public string EMPLOYEE_ID { get; set; }
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public long POSSICAUSE_ID { get; set; }// ID form QMS_MA_IQC_POSSICAUSE
        public string POSSICAUSE_NAME { get; set; }
        public string ALERT_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }


        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_MAILALERT
    {
        public long ID { get; set; }
        public long EMAIL_ID { get; set; }// ID form QMS_MA_EMAIL
        public string EMPLOYEE_ID { get; set; }
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public long POSSICAUSE_ID { get; set; }// ID form QMS_MA_IQC_POSSICAUSE
        public string POSSICAUSE_NAME { get; set; }

        public string ALERT_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

    }


    public class CreateQMS_MA_IQC_ADDEMAIL
    {
        public long ID { get; set; }
        public byte GROUP_TYPE { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string NAME { get; set; }
        public string POSITION { get; set; }
        public string EMAIL { get; set; }
        public string UNIT_NAME { get; set; }
        public string DEPARTMENT { get; set; }
        public byte DELETE_FLAG { get; set; }


        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_IQC_ITEM
    public class CreateQMS_MA_IQC_ITEM
    {
        public long ID { get; set; }
        public long ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public string ITEM_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_ITEM
    {
        public long ID { get; set; }
        public long ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public string ITEM_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

    }

    //-----QMS_MA_IQC_EQUIP
    public class CreateQMS_MA_IQC_EQUIP
    {
        public long ID { get; set; }
        public long EQUIP_ID { get; set; }
        public string EQUIP_NAME { get; set; }
        public string EQUIP_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_EQUIP
    {
        public long ID { get; set; }
        public long EQUIP_ID { get; set; }
        public string EQUIP_NAME { get; set; }
        public string EQUIP_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public Int16 POSITION { get; set; }
    }


    //-----QMS_MA_IQC_ITEMEEQUIP
    public class CreateQMS_MA_IQC_ITEMEQUIP
    {
        public long ID { get; set; }
        public long ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public long EQUIP_ID { get; set; }
        public string EQUIP_NAME { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_ITEMEQUIP
    {
        public long ID { get; set; }
        public long ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public long EQUIP_ID { get; set; }
        public string EQUIP_NAME { get; set; }
        public string ITEMEQUIP_NAME { get; set; }
        public byte DELETE_FLAG { get; set; }
    }


    //-----QMS_MA_IQC_TEMPLATE
    public class CreateQMS_MA_IQC_TEMPLATE
    {
        public long ID { get; set; }
        public byte TEMPLATE_TYPE { get; set; }
        public string NAME { get; set; }
        public string FILE_NAME { get; set; }
        public string PATH_NAME { get; set; }
        public string SHEET_NAME { get; set; }
        public string COLUMN_DATE { get; set; }
        public string COLUMN_DATA { get; set; }
        public byte COLUMN_UNIT { get; set; }
        public byte ROW_START { get; set; }

        public string TEMPLATE_DESC { get; set; }

        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_TEMPLATE
    {
        public long ID { get; set; }
        public byte TEMPLATE_TYPE { get; set; }
        public string FULL_NAME { get; set; }
        public string NAME { get; set; }
        public string FILE_NAME { get; set; }
        public string PATH_NAME { get; set; }
        public string SHEET_NAME { get; set; }
        public string COLUMN_DATE { get; set; }
        public string COLUMN_DATA { get; set; }
        public byte COLUMN_UNIT { get; set; }
        public byte ROW_START { get; set; }
        public string TEMPLATE_DESC { get; set; }

        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_IQC_CONTROLDATA
    public class CreateQMS_MA_IQC_CONTROLDATA
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public DateTime KEEPTIME { get; set; }
        public decimal KEEPVALUE { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_CONTROLDATA
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public DateTime KEEPTIME { get; set; }
        public decimal KEEPVALUE { get; set; }
        public long ITEM_ID { get; set; }
        public long METHOD_ID { get; set; }
        public decimal FORMULA_UCL { get; set; }
        public decimal FORMULA_UWL { get; set; }
        public decimal FORMULA_AVG { get; set; }
        public decimal FORMULA_LWL { get; set; }
        public decimal FORMULA_LCL { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_IQC_CONTROLHISTORY
    public class CreateQMS_MA_IQC_CONTROLHISTORY
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public DateTime STARTEDGE { get; set; }
        public DateTime ENDEDGE { get; set; } 
        public string TITLE { get; set; }
        public DateTime SENDEMAIL { get; set; } 
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_CONTROLHISTORY
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string ITEMEQUIP_NAME { get; set; }
        public string METHOD_NAME { get; set; }
        public DateTime STARTEDGE { get; set; }
        public DateTime ENDEDGE { get; set; }
        public string TITLE { get; set; }
        public DateTime SENDEMAIL { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }


     //-----QMS_MA_IQC_CONTROLRULE
    public class CreateQMS_MA_IQC_CONTROLRULE
    {
        public long ID { get; set; }
        public long HISTORY_ID { get; set; }
        public long RULE_ID { get; set; }        
        public DateTime POINT_START { get; set; }
        public DateTime? POINT_END { get; set; }
        public string POSSICAUSE_REASON { get; set; }
        
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_CONTROLRULE
    {
        public long ID { get; set; }
        public long HISTORY_ID { get; set; }
        public long RULE_ID { get; set; }
        public string RULE_NAME { get; set; }
        public DateTime POINT_START { get; set; }
        public DateTime? POINT_END { get; set; }
        public string POSSICAUSE_REASON { get; set; }

        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_IQC_CONTROLREASON
    public class CreateQMS_MA_IQC_CONTROLREASON
    {
        public long ID { get; set; }
        public long CONTROLRULE_ID { get; set; }
        public string POSSICAUSE_REASON { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string POSSICAUSE_NAME { get; set; }
        public string REASON { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_IQC_CONTROLREASON
    {
        public long ID { get; set; }
        public long CONTROLRULE_ID { get; set; }
        public string POSSICAUSE_REASON { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string POSSICAUSE_NAME { get; set; }
        public string REASON { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_MA_SORTING_AREA
    public class CreateQMS_MA_SORTING_AREA
    {
        public long ID { get; set; }
        public long TEMPLATE_TYPE { get; set; }
        public short POSITION { get; set; }
        public string AREA_NAME { get; set; }
    }
    public class ViewQMS_MA_SORTING_AREA
    {
        public long ID { get; set; }
        public long TEMPLATE_TYPE { get; set; }
        public short POSITION { get; set; }
        public string AREA_NAME { get; set; }
    }

    //-----QMS_MA_SORTING_SAMPLE
    public class CreateQMS_MA_SORTING_SAMPLE
    {
        public long ID { get; set; }
        public long TEMPLATE_TYPE { get; set; }
        public long TEMPLATE_ID { get; set; }
        public short POSITION { get; set; }
        public string AREA_NAME { get; set; }
        public string SAMPLE_NAME { get; set; }
    }
    public class ViewQMS_MA_SORTING_SAMPLE
    {
        public long ID { get; set; }
        public long TEMPLATE_TYPE { get; set; }
        public long TEMPLATE_ID { get; set; }
        public short POSITION { get; set; }
        public string AREA_NAME { get; set; }
        public string SAMPLE_NAME { get; set; }
    }

    //-----QMS_MA_SORTING_ITEM
    public class CreateQMS_MA_SORTING_ITEM
    {
        public long ID { get; set; }
        public long TEMPLATE_TYPE { get; set; }
        public long TEMPLATE_ID { get; set; }
        public short POSITION { get; set; }
        public string AREA_NAME { get; set; }
        public string SAMPLE_NAME { get; set; }
        public string ITEM_NAME { get; set; }
    }
    public class ViewQMS_MA_SORTING_ITEM
    {
        public long ID { get; set; }
        public long TEMPLATE_TYPE { get; set; }
        public long TEMPLATE_ID { get; set; }
        public short POSITION { get; set; }
        public string AREA_NAME { get; set; }
        public string SAMPLE_NAME { get; set; }
        public string ITEM_NAME { get; set; }
    }

    public class ViewAlignmentOption
    {
        public long ID { get; set; }
        public string NAME { get; set; }
    }

    public class TokenMod
    {

        public int code { get; set; }

        public Boolean status { get; set; }

        public String message { get; set; }

        public String access_token { get; set; }

        public String token_type { get; set; }

        public String expires_in { get; set; }
    }


    public class AccessToken
    {
        public String access_token { get; set; }

        public String empcode { get; set; }
    }



    public class KeyValueMod
    {
        public String name { get; set; }

        public String desc { get; set; }

        public Double value { get; set; }

        public String date { get; set; }

        public String regionCode { get; set; }

        public String groupName { get; set; }

        public Double groupValue { get; set; }

        public String typeName { get; set; }

        public String userGroup { get; set; }

        public String getName()
        {
            return this.name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getDesc()
        {
            return this.desc;
        }

        public void setDesc(String desc)
        {
            this.desc = desc;
        }

        public Double getValue()
        {
            return this.value;
        }

        public void setValue(Double value)
        {
            this.value = value;
        }

        public String getDate()
        {
            return this.date;
        }

        public void setDate(String date)
        {
            this.date = date;
        }

        public String getRegionCode()
        {
            return this.regionCode;
        }

        public void setRegionCode(String regionCode)
        {
            this.regionCode = regionCode;
        }

        public String getGroupName()
        {
            return this.groupName;
        }

        public void setGroupName(String groupName)
        {
            this.groupName = groupName;
        }

        public Double getGroupValue()
        {
            return this.groupValue;
        }

        public void setGroupValue(Double groupValue)
        {
            this.groupValue = groupValue;
        }

        public String getTypeName()
        {
            return this.typeName;
        }

        public void setTypeName(String typeName)
        {
            this.typeName = typeName;
        }

        public String getUserGroup()
        {
            return this.userGroup;
        }

        public void setUserGroup(String userGroup)
        {
            this.userGroup = userGroup;
        }
    }

    public class TagnameInfoListModel
    {
        public String[] tagNameList { get; set; }

    }


    public class WSO2TokenMod
    {
        public String access_token { get; set; }

        public String scope { get; set; }

        public String token_type { get; set; }

        public String expires_in { get; set; }
    }


    public class PersonnelInfoMod
    {
        public String LNAME { get; set; }
        public String AWARD_TEXT_TH { get; set; }
        public String ENTRYDATE { get; set; }
        public String ASSIGNDATE { get; set; }
        public String TIMEST { get; set; }
        public String RETIREDATE { get; set; }
        public String POSAGE2 { get; set; }
        public String TAXCODE { get; set; }
        public String POSAGE1 { get; set; }
        public String INAME5 { get; set; }
        public String INAME4 { get; set; }
        public String LNAME2 { get; set; }
        public String INAME_ENG { get; set; }
        public String JGAGE1 { get; set; }
        public String INAME3 { get; set; }
        public String INAME2 { get; set; }
        public String INAME1 { get; set; }
        public String BAND { get; set; }
        public String AGE1 { get; set; }
        public String CHANGE_DATE { get; set; }
        public String AGE2 { get; set; }
        public String UNITCODE { get; set; }
        public String FNAME { get; set; }
        public String ADDRESS { get; set; }
        public String OFFICETEL { get; set; }
        public String MNAME { get; set; }
        public String AWARD_TEXT_EN { get; set; }
        public String RETIREYEAR { get; set; }
        public String JGAGE2 { get; set; }
        public String AWARD_YEAR { get; set; }
        public String EmailAddr { get; set; }
        public String SEX { get; set; }
        public String POSNAME { get; set; }
        //public String UNITCODE { get; set; }
        public String HIRINGDATE { get; set; }
        public String BIRTHDATE { get; set; }
        public String JOBAGE2 { get; set; }
        public String Secondment_text { get; set; }
        public String STJGDATE { get; set; }
        public String HOMETEL { get; set; }
        public String JOBAGE1 { get; set; }
        public String POSCODE { get; set; }
        public String Secondment_ID { get; set; }
        public String FULLNAMETH { get; set; }
        public String STPOSDATE { get; set; }
        public String MGMT { get; set; }
        public String mobile { get; set; }
        public String LNAME_ENG { get; set; }
        public String PERSCODE { get; set; }
        public String AWARD_INSTRUCTYPE { get; set; }
        public String CODE { get; set; }
        public String OFFICECODE { get; set; }
        public String FNAME_ENG { get; set; }
        public String SALCODE { get; set; }
        public String WSTCODE { get; set; }
        public String QUADRANT { get; set; }
        public String INAME { get; set; }
    }

    public class SearchUnitMod
    {
        public String Search_EmployeeCode { get; set; }
        public String Search_UnitCode { get; set; }
        public String Search_UnitAbb { get; set; }
        public String Sort_UnitCode { get; set; }
        public String Search_UnitName { get; set; }
        public String Search_UnitAbbr { get; set; }
        public String Search_UnitLevelID { get; set; }
        public String Sort_UnitLevelID { get; set; }
        public String Search_UnitLevelName { get; set; }
        public String Search_BusinessUnitID { get; set; }
        public String Sort_BusinessUnitID { get; set; }
        public String Search_BusinessUnitName { get; set; }
        public String Search_DummyRelationship { get; set; }
        public String Sort_DummyRelationship { get; set; }
        public String Search_CostCenter { get; set; }
        public String Search_HeadPosition { get; set; }

        public int NumberOfRecordPerPage { get; set; }
        public int CurrentPage { get; set; }
    }

    public class SearchPersonnelInfoMod
    {
        public String Search_EmployeeCode { get; set; }
        public String Sort_EmployeeCode { get; set; }
        public String Search_UnitCode { get; set; }
        public String Sort_UnitCode { get; set; }
        public String Search_PositionCode { get; set; }
        public String Search_EmployeeStatus { get; set; }
        public String Search_EmployeeBand { get; set; }
        public String Sort_EmployeeBand { get; set; }
        public String NumberOfRecordPerPage { get; set; }
        public String CurrentPage { get; set; }
    }

    public class UnitMod
    {
        public String engname { get; set; }
        public String PROJECT_LEVEL_NAME { get; set; }
        public String unitabbr { get; set; }
        public String longname { get; set; }
        public String unitcode { get; set; }
        public String HEAD_POSITION { get; set; }
        public String HEAD_CODE { get; set; }
        public String DUMMY_RELATIONSHIP { get; set; }
        public String CHANGE_DATE { get; set; }
        public String BUSINESS_UNIT_ID { get; set; }
        public String BUSINESS_UNIT_NAME { get; set; }
        public String UNIT_LEVEL_NAME { get; set; }
        public String CostCenter { get; set; }
        public String unitname { get; set; }
        public String PROJECT_LEVEL_ID { get; set; }
        public String UNIT_LEVEL_ID { get; set; }
    }

    public class ResponseUnitListMod
    {
        public ResponseUnitListMod()
        {
            unitList = new List<UnitMod>();
        }
        public List<UnitMod> unitList { get; set; }
        public String resultMess { get; set; }
        public int resultCode { get; set; }
    }

    public class ResponsePersonInfoListMod
    {
        public ResponsePersonInfoListMod()
        {
            personList = new List<PersonnelInfoMod>();
        }
        public List<PersonnelInfoMod> personList { get; set; }
        public String resultMess { get; set; }
        public int resultCode { get; set; }
    }


    //-----QMS_MA_CPK_INTERVAL_SAMPLING
    public class CreateQMS_MA_CPK_INTERVAL_SAMPLING
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string COMPOSATION { get; set; }
        public string INTERVAL_SAMPLING { get; set; }
        public decimal UPPER_LIMIT { get; set; }
        public decimal LOWER_LIMIT { get; set; }
        public string REC_SAMPLING { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }


    public class ViewQMS_MA_CPK_INTERVAL_SAMPLING
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string COMPOSATION { get; set; }
        public string INTERVAL_SAMPLING { get; set; }
        public decimal UPPER_LIMIT { get; set; }
        public decimal LOWER_LIMIT { get; set; }
        public string REC_SAMPLING { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }


    //-----QMS_MA_PRODUCT_MERGE
    public class CreateQMS_MA_PRODUCT_MERGE
    {
        public long ID { get; set; }
        public string ITEM { get; set; }
        public string UNIT { get; set; }
        public string CONTROL_VALUE { get; set; }
        public string TEST_METHOD { get; set; }
        public string SPEC_VALUE { get; set; }
        public string DISPLAY_NAME { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public DateTime EXPIRE_DATE { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_PRODUCT_MERGE
    {
        public long ID { get; set; }
        public string ITEM { get; set; }
        public string UNIT { get; set; }
        public string CONTROL_VALUE { get; set; }
        public string TEST_METHOD { get; set; }
        public string SPEC_VALUE { get; set; }
        public string DISPLAY_NAME { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public DateTime EXPIRE_DATE { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }


    //-----QMS_MA_CUSTOMER_MERGE
    public class CreateQMS_MA_CUSTOMER_MERGE
    {
        public long ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public DateTime EXPIRE_DATE { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_CUSTOMER_MERGE
    {
        public long ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public DateTime EXPIRE_DATE { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }



    //-----QMS_MA_SIGMA_LEVEL
    public class CreateQMS_MA_SIGMA_LEVEL
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public decimal START_VALUE_6 { get; set; }
        public decimal END_VALUE_6 { get; set; }
        public string ALIAS_DESC_6 { get; set; }
        public decimal START_VALUE_5 { get; set; }
        public decimal END_VALUE_5 { get; set; }
        public string ALIAS_DESC_5 { get; set; }
        public decimal START_VALUE_4 { get; set; }
        public decimal END_VALUE_4 { get; set; }
        public string ALIAS_DESC_4 { get; set; }
        public decimal START_VALUE_3 { get; set; }
        public decimal END_VALUE_3 { get; set; }
        public string ALIAS_DESC_3 { get; set; }
        public decimal START_VALUE_2 { get; set; }
        public decimal END_VALUE_2 { get; set; }
        public string ALIAS_DESC_2 { get; set; }
        public decimal START_VALUE_1 { get; set; }
        public decimal END_VALUE_1 { get; set; }
        public string ALIAS_DESC_1 { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string SIGMA_LEVEL_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_MA_SIGMA_LEVEL
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public decimal START_VALUE_6 { get; set; }
        public decimal END_VALUE_6 { get; set; }
        public string ALIAS_DESC_6 { get; set; }
        public decimal START_VALUE_5 { get; set; }
        public decimal END_VALUE_5 { get; set; }
        public string ALIAS_DESC_5 { get; set; }
        public decimal START_VALUE_4 { get; set; }
        public decimal END_VALUE_4 { get; set; }
        public string ALIAS_DESC_4 { get; set; }
        public decimal START_VALUE_3 { get; set; }
        public decimal END_VALUE_3 { get; set; }
        public string ALIAS_DESC_3 { get; set; }
        public decimal START_VALUE_2 { get; set; }
        public decimal END_VALUE_2 { get; set; }
        public string ALIAS_DESC_2 { get; set; }
        public decimal START_VALUE_1 { get; set; }
        public decimal END_VALUE_1 { get; set; }
        public string ALIAS_DESC_1 { get; set; }
        public DateTime ACTIVE_DATE { get; set; }
        public string SIGMA_LEVEL_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    //-----QMS_TR_CPK_TEMP_DATA

    public class CreateQMS_TR_CPK_TEMP_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public DateTime SAMPLE_TIME { get; set; }
        public decimal SAMPLE_VALUE { get; set; }
        public long TOKEN_USER { get; set; }
        public long DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
    }
    public class ViewQMS_TR_CPK_TEMP_DATA
    {
        public long ID { get; set; }
        public long? PLANT_ID { get; set; }
        public long? PRODUCT_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public DateTime? SAMPLE_TIME { get; set; }
        public decimal? SAMPLE_VALUE { get; set; }
        public long? TOKEN_USER { get; set; }
        public long? DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
    }

    public class ProductDistributionChartMod
    {
        public List<ViewQMS_TR_CPK_TEMP_DATA> listCpkTempData = new List<ViewQMS_TR_CPK_TEMP_DATA>();
        public decimal? MIN { get; set; }
        public decimal? Q1 { get; set; }
        public decimal? Q2 { get; set; }
        public decimal? Q3 { get; set; }
        public decimal? MAX { get; set; }
        public decimal? IQR { get; set; }
        public decimal? UPPER_BOUND { get; set; }
        public decimal? LOWER_BOUND { get; set; }
        public decimal? STD { get; set; }
        public decimal? AVG { get; set; }
        public decimal? UPPER_LIMIT { get; set; }
        public decimal? LOWER_LIMIT { get; set; }
        public string INTERVAL_SAMPLING { get; set; }
        public decimal? CPO { get; set; }
        public decimal? CPU { get; set; }
        public decimal? CPK { get; set; }
        public int? SIGMA_LEVEL { get; set; }
        public decimal? SIGMA_START_VALUE { get; set; }
        public decimal? SIGMA_END_VALUE { get; set; }
        public string SIGMA_ALIAS_DESC { get; set; }
    }
    public class SigmaLevelChartMod
    {

        public List<SigmaLevelQuarterMod> quarters = new List<SigmaLevelQuarterMod>();
        public List<SigmaLevelProductMod> PRODUCT { get; set; } = new List<SigmaLevelProductMod>();
    }
    public class SigmaLevelQuarterMod
    { 
        public int? QUARTER { get; set; }
        public int? YEAR { get; set; }

        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
    }
    public class SigmaLevelProductMod
    {
        public int? QUARTER { get; set; }
        public int? YEAR { get; set; }
        public int? PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public decimal? VALUE { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
    }
    public class SigmaLevelPlantMod
    {
        public int? QUARTER { get; set; }
        public int? YEAR { get; set; }
        public int? PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public decimal? VALUE { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
    }
    public class SigmaLevelChartBarMod
    {
        public List<string> labels = new List<string>();
        public decimal? PRODUCTION { get; set; }
        public decimal? OFF_CONTROL { get; set; }
        public decimal? KPI { get; set; }
        public List<SigmaLevelDataBarMod> datasets { get; set; } = new List<SigmaLevelDataBarMod>();

        public List<CreateQMS_TR_PRODUCT_REMARK> listRemark { get; set; } = new List<CreateQMS_TR_PRODUCT_REMARK>();
    }
    public class SigmaLevelDataBarMod
    {
        public string label { get; set; }
        public decimal? sum { get; set; }

        public List<decimal?> data = new List<decimal?>();
    }
}