﻿using System;
using System.Collections.Generic;

using System.Linq;
using QMSSystem.Model;

namespace QMSSystem.CoreDB
{

    public partial class QMS_TR_ABNORMAL 
    {
        public static List<QMS_TR_ABNORMAL> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_ABNORMAL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_ABNORMAL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_ABNORMAL> GetByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate)
        {
            return dataContext.QMS_TR_ABNORMAL.Where(m => m.START_DATE >= startDate && m.END_DATE <= endDate).ToList();
        }

        public static IQueryable<QMS_TR_ABNORMAL> GetQueryableByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, List<long> PlantList, List<long> ProductList)
        {

            var query = dataContext.QMS_TR_ABNORMAL.AsQueryable();

            if (null != startDate)
            {
                query = query.Where(m => m.START_DATE >= startDate);
            }

            if (null != startDate)
            {
                query = query.Where(m => m.END_DATE <= endDate);
            }

            if (null != PlantList && PlantList.Count() > 0)
            {
                query = query.Where(m => PlantList.Contains(m.PLANT_ID));
            }

            if (null != ProductList && ProductList.Count() > 0)
            {
                query = query.Where(m => ProductList.Contains(m.PRODUCT_ID));
            }

            return query;
        }

        public static List<QMS_TR_ABNORMAL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_ABNORMAL.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_ABNORMAL> GetAllByPlantListProductListAndDateTime(QMSDBEntities dataContext, List<long> plantList, List<long> productList, DateTime startDate, DateTime endDate, byte docStatus = 0)
        {
            List<QMS_TR_ABNORMAL> listResult = new List<QMS_TR_ABNORMAL>();
            IQueryable<QMS_TR_ABNORMAL> query = dataContext.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            if (null != plantList && plantList.Count() > 0)
            {
                query = query.Where(m => plantList.Contains(m.PLANT_ID));
            }

            if (null != productList && productList.Count() > 0)
            {
                query = query.Where(m => productList.Contains(m.PRODUCT_ID));
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_ABNORMAL> GetAllByPlantAndDateTime(QMSDBEntities dataContext, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_ABNORMAL> listResult = new List<QMS_TR_ABNORMAL>();
            IQueryable<QMS_TR_ABNORMAL> query = dataContext.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID  );

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_ABNORMAL> GetAllByPlantProductAndDateTime(QMSDBEntities dataContext, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_ABNORMAL> listResult = new List<QMS_TR_ABNORMAL>();
            IQueryable<QMS_TR_ABNORMAL> query = dataContext.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID && m.PRODUCT_ID == productId);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }


        public static List<QMS_TR_ABNORMAL> GetAllByPlantAndDateTimeExpand(QMSDBEntities dataContext, long modelID, long plantID, long productId,  byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_ABNORMAL> listResult = new List<QMS_TR_ABNORMAL>();
            IQueryable<QMS_TR_ABNORMAL> query = dataContext.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0 && m.ID != modelID && m.PLANT_ID == plantID && m.PRODUCT_ID == productId);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            query = query.Where(m => m.END_DATE == startDate || m.START_DATE == endDate);

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_ABNORMAL> GetAllBySearchTRUN_AROUND(QMSDBEntities db, CreateQMS_TR_TURN_AROUND searchModel)
        {
            List<QMS_TR_ABNORMAL> lstResult = new List<QMS_TR_ABNORMAL>();

            IQueryable<QMS_TR_ABNORMAL> query = db.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {

                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_ABNORMAL> GetByListIdByCorrectData(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel )
        {
            List<QMS_TR_ABNORMAL> listResult = new List<QMS_TR_ABNORMAL>();
            IQueryable<QMS_TR_ABNORMAL> query = dataContext.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            //if (productId > 0)
            //{
            //    query = dataContext.QMS_TR_ABNORMAL.Where(m => m.PRODUCT_ID == productId);
            //}

            listResult = query.ToList();

            return listResult; 
        }

        public static List<QMS_TR_ABNORMAL> GetByListIdByCorrectDataEx(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_ABNORMAL> listResult = new List<QMS_TR_ABNORMAL>();
            IQueryable<QMS_TR_ABNORMAL> query = dataContext.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0 );

            try
            {
                if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }
                else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
                }
                else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }

                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                listResult = query.ToList();
            }
            catch
            {

            }

            return listResult;
        }

        public static List<QMS_TR_ABNORMAL> GetAllBySearch(QMSDBEntities db, AbnormalSearchModel searchModel)
        {
            List<QMS_TR_ABNORMAL> lstResult = new List<QMS_TR_ABNORMAL>();

            IQueryable<QMS_TR_ABNORMAL> query = db.QMS_TR_ABNORMAL.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (  searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID) ;
                }

                if ( searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }
                else
                {
                    query = query.Where(m => m.DOC_STATUS != (byte)QMSSystem.Model.DOC_STATUS.INITIAL); //default ไม่เอา inital
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      ));
                }
                else
                {
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    }
                }

                if (null != searchModel.VOLUME && searchModel.VOLUME >= 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.VOLUME) ;
                }

                if ( searchModel.CORRECT_DATA_TYPE > 0)
                {
                    query = query.Where(m => m.CORRECT_DATA_TYPE == searchModel.CORRECT_DATA_TYPE) ;
                }

                if (null != searchModel.CAUSE_DESC && "" != searchModel.CAUSE_DESC)
                {
                    query = query.Where(m => m.CAUSE_DESC.Contains(searchModel.CAUSE_DESC)) ;
                }

                lstResult = query.ToList();
            }
            catch
            {


            }
            return lstResult;
        } 
    }

    public partial class QMS_TR_CHANGE_GRADE 
    {
        public static List<QMS_TR_CHANGE_GRADE> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_CHANGE_GRADE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_CHANGE_GRADE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_CHANGE_GRADE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_CHANGE_GRADE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_CHANGE_GRADE> GetAllByPlantAndDateTime(QMSDBEntities dataContext, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_CHANGE_GRADE> listResult = new List<QMS_TR_CHANGE_GRADE>();
            IQueryable<QMS_TR_CHANGE_GRADE> query = dataContext.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CHANGE_GRADE> GetAllByPlantListProductListAndDateTime(QMSDBEntities dataContext, List<long> plantList, List<long> productList, DateTime startDate, DateTime endDate, byte docStatus = 0)
        {
            List<QMS_TR_CHANGE_GRADE> listResult = new List<QMS_TR_CHANGE_GRADE>();
            IQueryable<QMS_TR_CHANGE_GRADE> query = dataContext.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0  );

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            if (null != plantList && plantList.Count() > 0)
            {
                query = query.Where(m => plantList.Contains(m.PLANT_ID));
            }

            if (null != productList && productList.Count() > 0)
            {
                query = query.Where(m => productList.Contains(m.PRODUCT_ID));
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CHANGE_GRADE> GetAllByPlantProductAndDateTime(QMSDBEntities dataContext, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_CHANGE_GRADE> listResult = new List<QMS_TR_CHANGE_GRADE>();
            IQueryable<QMS_TR_CHANGE_GRADE> query = dataContext.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID && m.PRODUCT_ID == productId  );

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CHANGE_GRADE> GetByListIdByCorrectData(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel) //, long productId)
        {
            List<QMS_TR_CHANGE_GRADE> listResult = new List<QMS_TR_CHANGE_GRADE>();
            IQueryable<QMS_TR_CHANGE_GRADE> query = dataContext.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            //if (productId > 0)
            //{
            //    query = dataContext.QMS_TR_CHANGE_GRADE.Where(m => m.PRODUCT_ID == productId);
            //}

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CHANGE_GRADE> GetByListIdByCorrectDataEx(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_CHANGE_GRADE> listResult = new List<QMS_TR_CHANGE_GRADE>();
            IQueryable<QMS_TR_CHANGE_GRADE> query = dataContext.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CHANGE_GRADE> GetAllBySearchTRUN_AROUND(QMSDBEntities db, CreateQMS_TR_TURN_AROUND searchModel)
        {
            List<QMS_TR_CHANGE_GRADE> lstResult = new List<QMS_TR_CHANGE_GRADE>();

            IQueryable<QMS_TR_CHANGE_GRADE> query = db.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {

                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }
             
        public static List<QMS_TR_CHANGE_GRADE> GetAllBySearch(QMSDBEntities db, ChangeGradeSearchModel searchModel)
        {
            List<QMS_TR_CHANGE_GRADE> lstResult = new List<QMS_TR_CHANGE_GRADE>();

            IQueryable<QMS_TR_CHANGE_GRADE> query = db.QMS_TR_CHANGE_GRADE.Where(m => m.DELETE_FLAG == 0) ;

            try
            {

                if ( searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if ( searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (  searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      ));
                }
                else
                {
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    }
                }

                if (null != searchModel.GRADE_FROM && searchModel.GRADE_FROM > 0)
                {
                    query = query.Where(m => m.GRADE_FROM == searchModel.GRADE_FROM);
                }

                if (null != searchModel.GRADE_TO && searchModel.GRADE_TO > 0)
                {
                    query = query.Where(m => m.GRADE_TO == searchModel.GRADE_TO);
                }

                if (null != searchModel.VOLUME && searchModel.VOLUME >= 0)
                {
                    query = query.Where(m => m.VOLUME == searchModel.VOLUME);
                }

                if ( searchModel.CORRECT_DATA_TYPE > 0)
                {
                    query = query.Where(m => m.CORRECT_DATA_TYPE == searchModel.CORRECT_DATA_TYPE);
                }

                if (null != searchModel.CAUSE_DESC && "" != searchModel.CAUSE_DESC)
                {
                    query = query.Where(m => m.CAUSE_DESC.Contains(searchModel.CAUSE_DESC));
                }

                lstResult = query.ToList();
            }
            catch
            {


            }
            return lstResult;
        }
    }

    public partial class QMS_TR_COA_DATA 
    {
        public static List<QMS_TR_COA_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_COA_DATA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_COA_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_COA_DATA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_COA_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_COA_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_COA_DATA> GetByListDateTime(QMSDBEntities dataContext, DateTime startTime, DateTime endTime)
        {
            return dataContext.QMS_TR_COA_DATA.Where(m => m.SAMPLING_DATE >= startTime && m.SAMPLING_DATE <= endTime).ToList();
        }

        public static QMS_TR_COA_DATA CheckDuplicateData(QMSDBEntities db, CreateQMS_TR_COA_DATA searchModel)
        {
             
            IQueryable<QMS_TR_COA_DATA> query = db.QMS_TR_COA_DATA.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (null != searchModel.CUSTOMER_NAME && "" != searchModel.CUSTOMER_NAME)
                {
                    query = query.Where(m => m.CUSTOMER_NAME == searchModel.CUSTOMER_NAME);
                }

                if (null != searchModel.PRODUCT_NAME && "" != searchModel.PRODUCT_NAME)
                {
                    query = query.Where(m => m.PRODUCT_NAME == searchModel.PRODUCT_NAME);
                }

                if (null != searchModel.ITEM_NAME && "" != searchModel.ITEM_NAME)
                {
                    query = query.Where(m => m.ITEM_NAME == searchModel.ITEM_NAME);
                }

                if (null != searchModel.UNIT_NAME && "" != searchModel.UNIT_NAME)
                {
                    query = query.Where(m => m.UNIT_NAME == searchModel.UNIT_NAME);
                }

                if (null != searchModel.SAMPLING_DATE)
                {
                    query = query.Where(m => m.SAMPLING_DATE == searchModel.SAMPLING_DATE);
                }
  
            }
            catch
            {


            }
            return query.FirstOrDefault();
        }
        public static List<QMS_TR_COA_DATA> GetAllBySearch2(QMSDBEntities db, COADataSearchModel searchModel)
        {
            List<QMS_TR_COA_DATA> lstResult = new List<QMS_TR_COA_DATA>();
            try
            {
                var listTemplate = db.QMS_TR_TEMPLATE_COA_DETAIL.AsNoTracking().Where(m => m.DELETE_FLAG == 0 && m.PRODUCT_MERGE_ID == searchModel.PRODUCT_MERGE_ID && m.CUSTOMER_NAME == searchModel.CUSTOMER && m.PRODUCT_NAME == searchModel.PRODUCT).ToList();

                if (null != listTemplate && listTemplate.Count() > 0)
                {
                    foreach (var objTemplate in listTemplate)
                    {
                        searchModel.ITEM = objTemplate.ITEM_NAME;
                        searchModel.UNIT = objTemplate.UNIT_NAME;

                        //if (!string.IsNullOrEmpty(searchModel.SPECIFICATION))
                        //{
                        //    searchModel.SPECIFICATION = objTemplate.SPECIFICATION;
                        //}

                        //if (!string.IsNullOrEmpty(searchModel.TEST_METHOD))
                        //{
                        //    searchModel.TEST_METHOD = objTemplate.TEST_METHOD;
                        //}
                        searchModel.SPECIFICATION = null;
                        searchModel.TEST_METHOD = null;
                        var listData = GetAllBySearch(db, searchModel);

                        if(null != listData && listData.Count() > 0)
                        {
                            lstResult.AddRange(listData);
                        }
                    }
                }
            }
            catch
            {

            } 
               
            return lstResult;
        }

        public static List<QMS_TR_COA_DATA> GetAllBySearch(QMSDBEntities db, COADataSearchModel searchModel)
        {
            List<QMS_TR_COA_DATA> lstResult = new List<QMS_TR_COA_DATA>();

            IQueryable<QMS_TR_COA_DATA> query = db.QMS_TR_COA_DATA.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (null != searchModel.CUSTOMER && "" != searchModel.CUSTOMER)
                {
                    query = query.Where(m => m.CUSTOMER_NAME  == searchModel.CUSTOMER );
                }

                if (null != searchModel.PRODUCT && "" != searchModel.PRODUCT)
                {
                    query = query.Where(m => m.PRODUCT_NAME == searchModel.PRODUCT );
                }

                if (null != searchModel.ITEM && "" != searchModel.ITEM)
                {
                    query = query.Where(m => m.ITEM_NAME == searchModel.ITEM);
                }

                if (null != searchModel.UNIT && "" != searchModel.UNIT)
                {
                    query = query.Where(m => m.UNIT_NAME == searchModel.UNIT);
                }

                if (null != searchModel.SPECIFICATION && "" != searchModel.SPECIFICATION)
                {
                    query = query.Where(m => m.SPECIFICATION == searchModel.SPECIFICATION);
                }

                if (null != searchModel.TEST_METHOD && "" != searchModel.TEST_METHOD)
                {
                    query = query.Where(m => m.TEST_METHOD == searchModel.TEST_METHOD);
                }

                if (null != searchModel.TANK && "" != searchModel.TANK && "All" != searchModel.TANK) 
                {
                    query = query.Where(m => m.TANK_NAME == searchModel.TANK);
                }

                if (null != searchModel.StartTime)
                {
                    query = query.Where(m => m.SAMPLING_DATE >= searchModel.StartTime);
                }

                if (null != searchModel.EndTime)
                {
                    query = query.Where(m => m.SAMPLING_DATE <= searchModel.EndTime);
                }

                lstResult = query.ToList();
            }
            catch
            {


            }
            return lstResult;
        }

        public static List<QMS_TR_COA_DATA> GetByListDateTimeToDataLake(QMSDBEntities dataContext, DateTime startTime, DateTime endTime)
        {
            return dataContext.QMS_TR_COA_DATA.Where(m => m.PRODUCT_NAME.Contains("LPG PRODUCT") && m.ITEM_NAME.Contains("Density @ 15.0 C") && m.SAMPLING_DATE >= startTime && m.SAMPLING_DATE <= endTime).ToList();
        }
    }

    public partial class QMS_TR_CROSS_VOLUME 
    {
        public static List<QMS_TR_CROSS_VOLUME> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_CROSS_VOLUME GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_CROSS_VOLUME.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_CROSS_VOLUME> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_CROSS_VOLUME.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_CROSS_VOLUME> GetAllByPlantAndDateTime(QMSDBEntities dataContext, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_CROSS_VOLUME> listResult = new List<QMS_TR_CROSS_VOLUME>();
            IQueryable<QMS_TR_CROSS_VOLUME> query = dataContext.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID && m.DOC_STATUS == docStatus);

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CROSS_VOLUME> GetByListIdByCorrectData(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel )
        {
            List<QMS_TR_CROSS_VOLUME> listResult = new List<QMS_TR_CROSS_VOLUME>();
            IQueryable<QMS_TR_CROSS_VOLUME> query = dataContext.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.PRODUCT_ID > 0)
            {
                query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID || m.PRODUCT_TO == searchModel.PRODUCT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }
             

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CROSS_VOLUME> GetByListIdByCorrectDataEx(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_CROSS_VOLUME> listResult = new List<QMS_TR_CROSS_VOLUME>();
            IQueryable<QMS_TR_CROSS_VOLUME> query = dataContext.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0 );
            try
            {
                if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }
                else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
                }
                else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }

                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID || m.PRODUCT_TO == searchModel.PRODUCT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                listResult = query.ToList();
            }
            catch
            {

            }
           

            return listResult;
        }

        public static List<QMS_TR_CROSS_VOLUME> GetAllBySearchTRUN_AROUND(QMSDBEntities db, CreateQMS_TR_TURN_AROUND searchModel)
        {
            List<QMS_TR_CROSS_VOLUME> lstResult = new List<QMS_TR_CROSS_VOLUME>();

            IQueryable<QMS_TR_CROSS_VOLUME> query = db.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {

                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_CROSS_VOLUME> GetAllByPlantListProductListAndDateTime(QMSDBEntities dataContext, List<long> plantList, List<long> productList, DateTime startDate, DateTime endDate, byte docStatus = 0)
        {
            List<QMS_TR_CROSS_VOLUME> listResult = new List<QMS_TR_CROSS_VOLUME>();
            IQueryable<QMS_TR_CROSS_VOLUME> query = dataContext.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            if (null != plantList && plantList.Count() > 0)
            {
                query = query.Where(m => plantList.Contains(m.PLANT_ID));
            }

            if (null != productList && productList.Count() > 0)
            {
                query = query.Where(m => productList.Contains(m.PRODUCT_ID));
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CROSS_VOLUME> GetAllByPlantProductAndDateTime(QMSDBEntities dataContext, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_CROSS_VOLUME> listResult = new List<QMS_TR_CROSS_VOLUME>();
            IQueryable<QMS_TR_CROSS_VOLUME> query = dataContext.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID && m.PRODUCT_TO == productId );

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_CROSS_VOLUME> GetAllBySearch(QMSDBEntities db, CrossVolumeSearchModel searchModel)
        {
            List<QMS_TR_CROSS_VOLUME> lstResult = new List<QMS_TR_CROSS_VOLUME>();

            IQueryable<QMS_TR_CROSS_VOLUME> query = db.QMS_TR_CROSS_VOLUME.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (  searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if ( searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if ( searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      ));
                }
                else
                {
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    }
                }

                if (  searchModel.PRODUCT_FROM > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PRODUCT_FROM);
                }

                if ( searchModel.PRODUCT_TO > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PRODUCT_TO);
                }

                if (null != searchModel.VOLUME && searchModel.VOLUME >= 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.VOLUME);
                }

                if ( searchModel.CORRECT_DATA_TYPE > 0)
                {
                    query = query.Where(m => m.CORRECT_DATA_TYPE == searchModel.CORRECT_DATA_TYPE);
                }

                if (null != searchModel.CAUSE_DESC && "" != searchModel.CAUSE_DESC)
                {
                    query = query.Where(m => m.CAUSE_DESC.Contains(searchModel.CAUSE_DESC));
                }

                lstResult = query.ToList();

            }
            catch
            {


            }
            return lstResult;
        }
    }

    public partial class QMS_TR_DOWNTIME 
    {
        public static List<QMS_TR_DOWNTIME> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_DOWNTIME GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_DOWNTIME.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_DOWNTIME> GetByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate)
        {
            return dataContext.QMS_TR_DOWNTIME.Where(m => m.START_DATE >= startDate && m.END_DATE <= endDate).ToList();
        }

        public static IQueryable<QMS_TR_DOWNTIME> GetQueryableByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, List<long> PlantList)
        {

            var query = dataContext.QMS_TR_DOWNTIME.AsQueryable();

            if (null != startDate)
            {
                query = query.Where(m => m.START_DATE >= startDate);
            }

            if (null != startDate)
            {
                query = query.Where(m => m.END_DATE <= endDate);
            }

            if (null != PlantList && PlantList.Count() > 0)
            {
                query = query.Where(m => PlantList.Contains(m.PLANT_ID));
            }
 

            return query;
        }

        public static List<QMS_TR_DOWNTIME> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_DOWNTIME.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_DOWNTIME> GetAllByPlantListAndDateTime(QMSDBEntities dataContext, List<long> plantList, DateTime startDate, DateTime endDate, byte docStatus = 0)
        {
            List<QMS_TR_DOWNTIME> listResult = new List<QMS_TR_DOWNTIME>();
            IQueryable<QMS_TR_DOWNTIME> query = dataContext.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0 );

            if (null != plantList && plantList.Count() > 0)
            {
                query = query.Where(m => plantList.Contains(m.PLANT_ID));
            }

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_DOWNTIME> GetAllByPlantAndDateTimeExpand(QMSDBEntities dataContext, long modelID, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_DOWNTIME> listResult = new List<QMS_TR_DOWNTIME>();
            IQueryable<QMS_TR_DOWNTIME> query = dataContext.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0 && m.ID != modelID && m.PLANT_ID == plantID);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            query = query.Where(m => m.END_DATE == startDate || m.START_DATE == endDate);

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_DOWNTIME> GetAllByPlantAndDateTime(QMSDBEntities dataContext, long plantID, byte docStatus,  DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_DOWNTIME> listResult = new List<QMS_TR_DOWNTIME>();
            IQueryable<QMS_TR_DOWNTIME> query = dataContext.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID );

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_DOWNTIME> GetByListIdByCorrectData(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_DOWNTIME> listResult = new List<QMS_TR_DOWNTIME>();
            IQueryable<QMS_TR_DOWNTIME> query = dataContext.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_DOWNTIME> GetByListIdByCorrectDataEx(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_DOWNTIME> listResult = new List<QMS_TR_DOWNTIME>();
            IQueryable<QMS_TR_DOWNTIME> query = dataContext.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_DOWNTIME> GetAllBySearch(QMSDBEntities db, TRDowntimeSearchModel searchModel)
        {
            List<QMS_TR_DOWNTIME> lstResult = new List<QMS_TR_DOWNTIME>();

            IQueryable<QMS_TR_DOWNTIME> query = db.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (  searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }
                else
                {
                    query = query.Where(m => m.DOC_STATUS != (byte)QMSSystem.Model.DOC_STATUS.INITIAL); //default ไม่เอา inital
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      ));
                }
                else
                {
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    }
                }

                if (  searchModel.CORRECT_DATA_TYPE > 0)
                {
                    query = query.Where(m => m.CORRECT_DATA_TYPE == searchModel.CORRECT_DATA_TYPE);
                }

                if (null != searchModel.CAUSE_DESC && "" != searchModel.CAUSE_DESC)
                {
                    query = query.Where(m => m.CAUSE_DESC.Contains(searchModel.CAUSE_DESC));
                }
                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_TR_DOWNTIME> GetAllBySearchTRUN_AROUND(QMSDBEntities db, CreateQMS_TR_TURN_AROUND searchModel)
        {
            List<QMS_TR_DOWNTIME> lstResult = new List<QMS_TR_DOWNTIME>();

            IQueryable<QMS_TR_DOWNTIME> query = db.QMS_TR_DOWNTIME.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {

                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }
    }

    public partial class QMS_TR_EXCEPTION 
    {
        public static List<QMS_TR_EXCEPTION> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_EXCEPTION GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_EXCEPTION.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_EXCEPTION> GetAllBySearchCorrectDataIds(QMSDBEntities db, long[] Ids)
        {
            List<QMS_TR_EXCEPTION> lstResult = new List<QMS_TR_EXCEPTION>();

            IQueryable<QMS_TR_EXCEPTION> query = db.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                query = query.Where(m => Ids.Contains(m.CORRECT_DATA_TYPE) );
                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_EXCEPTION> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_EXCEPTION.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_EXCEPTION> GetAllByPlantAndDateTime(QMSDBEntities dataContext, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_EXCEPTION> listResult = new List<QMS_TR_EXCEPTION>();
            IQueryable<QMS_TR_EXCEPTION> query = dataContext.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID && m.DOC_STATUS == docStatus);

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_EXCEPTION> GetAllByPlantListProductListAndDateTime(QMSDBEntities dataContext, List<long> plantList, List<long> productList, DateTime startDate, DateTime endDate, byte docStatus = 0)
        {
            List<QMS_TR_EXCEPTION> listResult = new List<QMS_TR_EXCEPTION>();
            IQueryable<QMS_TR_EXCEPTION> query = dataContext.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            if (null != plantList && plantList.Count() > 0)
            {
                query = query.Where(m => plantList.Contains(m.PLANT_ID));
            }

            if (null != productList && productList.Count() > 0)
            {
                query = query.Where(m => productList.Contains(m.PRODUCT_ID));
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_EXCEPTION> GetAllByPlantProductAndDateTime(QMSDBEntities dataContext, long plantID, long productID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_EXCEPTION> listResult = new List<QMS_TR_EXCEPTION>();
            IQueryable<QMS_TR_EXCEPTION> query = dataContext.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID && m.PRODUCT_ID == productID && m.DOC_STATUS == docStatus);

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_EXCEPTION> GetByListIdByCorrectData(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel )
        {
            List<QMS_TR_EXCEPTION> listResult = new List<QMS_TR_EXCEPTION>();
            IQueryable<QMS_TR_EXCEPTION> query = dataContext.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            } 

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_EXCEPTION> GetByListIdByCorrectDataEx(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel )
        {
            List<QMS_TR_EXCEPTION> listResult = new List<QMS_TR_EXCEPTION>();
            IQueryable<QMS_TR_EXCEPTION> query = dataContext.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0 );
            try
            {
                if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }
                else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
                }
                else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }

                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                listResult = query.ToList();
            }
            catch
            {

            }
            return listResult;
        }

       

        public static List<QMS_TR_EXCEPTION> GetAllBySearch(QMSDBEntities db, ExceptionSearchModel searchModel)
        {
            List<QMS_TR_EXCEPTION> lstResult = new List<QMS_TR_EXCEPTION>();

            IQueryable<QMS_TR_EXCEPTION> query = db.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if ( searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (  searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      ));
                }
                else
                {
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    }
                }

                if (null != searchModel.VOLUME && searchModel.VOLUME >= 0)
                {
                    query = query.Where(m => m.VOLUME == searchModel.VOLUME);
                }

                if ( searchModel.CORRECT_DATA_TYPE > 0)
                {
                    query = query.Where(m => m.CORRECT_DATA_TYPE == searchModel.CORRECT_DATA_TYPE);
                }

                if (null != searchModel.CAUSE_DESC && "" != searchModel.CAUSE_DESC)
                {
                    query = query.Where(m => m.CAUSE_DESC.Contains(searchModel.CAUSE_DESC));
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_TR_EXCEPTION> GetAllBySearchTRUN_AROUND(QMSDBEntities db, CreateQMS_TR_TURN_AROUND searchModel)
        {
            List<QMS_TR_EXCEPTION> lstResult = new List<QMS_TR_EXCEPTION>();

            IQueryable<QMS_TR_EXCEPTION> query = db.QMS_TR_EXCEPTION.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {

                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }
    }

    public partial class QMS_TR_FILE_TREND_DATA 
    {
        public static List<QMS_TR_FILE_TREND_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_FILE_TREND_DATA.ToList(); //linq
        }

        public static QMS_TR_FILE_TREND_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_FILE_TREND_DATA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_FILE_TREND_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_FILE_TREND_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_FILE_TREND_DATA> GetByTemplateId(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_FILE_TREND_DATA.Where(m => m.TEMPLATE_ID == id).ToList();
        }

        public static List<QMS_TR_FILE_TREND_DATA> GetAllBySearch(QMSDBEntities db, FileTrendSearchModel searchModel)
        {
            List<QMS_TR_FILE_TREND_DATA> lstResult = new List<QMS_TR_FILE_TREND_DATA>();

            IQueryable<QMS_TR_FILE_TREND_DATA> query = db.QMS_TR_FILE_TREND_DATA;

            try
            {

                if ( searchModel.TEMPLATE_ID > 0)
                {
                    query = query.Where(m => m.TEMPLATE_ID == searchModel.TEMPLATE_ID);
                }


                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    query = query.Where(m => m.NAME.Contains(searchModel.NAME));
                }

                lstResult = query.ToList();
            }
            catch { 
            
            }

            return lstResult;
        }
    }

    public partial class QMS_TR_OFF_CONTROL_CAL 
    {
        public static List<QMS_TR_OFF_CONTROL_CAL> GetAll(QMSDBEntities db, byte controltype )
        {
            return db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.OFF_CONTROL_TYPE == controltype).ToList(); //linq 1 = Off control 2 = Off Spec
        }

        public static QMS_TR_OFF_CONTROL_CAL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate)
        {
            return dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => m.START_DATE >= startDate && m.END_DATE <= endDate).ToList();
        }

        public static IQueryable<QMS_TR_OFF_CONTROL_CAL> GetQueryableByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, List<long> PlantList, List<long> ProductList)
        {

            var query = dataContext.QMS_TR_OFF_CONTROL_CAL.AsQueryable();

            if (null != startDate)
            {
                query = query.Where(m => m.START_DATE >= startDate);
            }

            if (null != startDate)
            {
                query = query.Where(m => m.END_DATE <= endDate);
            }

            if (null != PlantList && PlantList.Count() > 0)
            {
                query = query.Where(m => PlantList.Contains(m.PLANT_ID));
            }

            if (null != ProductList && ProductList.Count() > 0)
            {
                query = query.Where(m => ProductList.Contains(m.PRODUCT_ID));
            }

            return query;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetByListIdByShiftReport(QMSDBEntities dataContext, ShiftAnalysisReportSearchModel searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> listResult = new List<QMS_TR_OFF_CONTROL_CAL>();
            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS == (byte)REPORT_DOC_STATUS.PUBLIC && m.OFF_CONTROL_TYPE == searchModel.OFF_TYPE);

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            } 

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetByListIdByOffControlCalAutoSearchModel(QMSDBEntities dataContext, OffControlCalAutoSearchModel searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> listResult = new List<QMS_TR_OFF_CONTROL_CAL>();
            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.OFF_CONTROL_TYPE == searchModel.OFF_TYPE );

            try
            {
                if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.ALL_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.INITIAL)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.INITIAL);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }

                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.ROOT_CAUSE_ID > 0)
                {
                    query = query.Where(m => m.ROOT_CAUSE_ID == searchModel.ROOT_CAUSE_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                listResult = query.ToList();
            }
            catch
            {

            }
            return listResult;
        }


        public static List<QMS_TR_OFF_CONTROL_CAL> GetByListIdByListOffControlCalAutoSearchModel(QMSDBEntities dataContext, DashOffControlCalAutoSearchModel searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> listResult = new List<QMS_TR_OFF_CONTROL_CAL>();
            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.OFF_CONTROL_TYPE == searchModel.OFF_TYPE);

            try
            {
                if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.ALL_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.INITIAL)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.INITIAL);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }

                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }


                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                listResult = query.ToList();
            }
            catch
            {

            }
            return listResult;
        }


        public static List<QMS_TR_OFF_CONTROL_CAL> GetByListIdByDashOffControlCalAutoSearchModel(QMSDBEntities dataContext, DashOffControlCalAutoSearchModel searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> listResult = new List<QMS_TR_OFF_CONTROL_CAL>();
            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.OFF_CONTROL_TYPE == searchModel.OFF_TYPE);

            try
            {
                if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.ALL_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.INITIAL)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.INITIAL);
                }
                else if (searchModel.AUTO_OFF_TYPE == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
                {
                    query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                listResult = query.ToList();
            }
            catch
            {

            }
            return listResult;
        }


        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearch(QMSDBEntities db, OffControlCalSearchModel searchModel, byte controltype)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.OFF_CONTROL_TYPE == controltype);

            try
            {

                if (  searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID );
                }

                if (  searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID );
                }

                if (  searchModel.OFF_CONTROL_TYPE > 0)
                {
                    query = query.Where(m => m.OFF_CONTROL_TYPE == searchModel.OFF_CONTROL_TYPE);
                }

                if (searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }
                else
                {
                    query = query.Where(m => m.DOC_STATUS != (byte)QMSSystem.Model.DOC_STATUS.INITIAL); //default ไม่เอา inital
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      ));
                }
                else
                { 
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    }
                }


                if (null != searchModel.VOLUME && searchModel.VOLUME >= 0)
                {
                    query = query.Where(m => m.VOLUME == searchModel.VOLUME);
                }

                if (null != searchModel.CONTROL_VALUE && "" != searchModel.CONTROL_VALUE)
                {
                    query = query.Where(m => m.CONTROL_VALUE.Contains(searchModel.CONTROL_VALUE));
                }

                if (  searchModel.ROOT_CAUSE_ID > 0)
                {
                    query = query.Where(m => m.ROOT_CAUSE_ID == searchModel.ROOT_CAUSE_ID);
                }

                if (null != searchModel.OFF_CONTROL_DESC && "" != searchModel.OFF_CONTROL_DESC)
                {
                    query = query.Where(m => m.OFF_CONTROL_DESC.Contains(searchModel.OFF_CONTROL_DESC));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }


        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchTRUN_AROUND(QMSDBEntities db, CreateQMS_TR_TURN_AROUND searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0){
                
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID );
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }


        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchDOWNTIME(QMSDBEntities db, CreateQMS_TR_DOWNTIME searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                { 
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchRecalRawData(QMSDBEntities db, long plant_id, long product_id, DateTime START_DATE, DateTime END_DATE)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (plant_id > 0)
                {
                    query = query.Where(m => m.PLANT_ID == plant_id);
                }

                if (product_id  > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == product_id);
                }

                if (START_DATE != null && END_DATE != null)
                {
                    query = query.Where(m => m.START_DATE >= START_DATE || m.END_DATE <= END_DATE);
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }


        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchCHANGE_GRADE(QMSDBEntities db, CreateQMS_TR_CHANGE_GRADE searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchCROSS_VOLUME_FROM(QMSDBEntities db, CreateQMS_TR_CROSS_VOLUME searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                            || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                            || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                            || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                        ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchCROSS_VOLUME_TO(QMSDBEntities db, CreateQMS_TR_CROSS_VOLUME searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_TO);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchAbnormal(QMSDBEntities db, CreateQMS_TR_ABNORMAL searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchReduceFeed(QMSDBEntities db, CreateQMS_TR_REDUCE_FEED searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                //if (searchModel.PRODUCT_ID > 0)
                //{
                //    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                //}

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllByPlantAndDateTimeExpand(QMSDBEntities dataContext, long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_OFF_CONTROL_CAL> listResult = new List<QMS_TR_OFF_CONTROL_CAL>();
            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = dataContext.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.ID != modelID && m.PLANT_ID == plantID && m.PRODUCT_ID == productId);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            query = query.Where(m => m.END_DATE == startDate || m.START_DATE == endDate);

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_OFF_CONTROL_CAL> GetAllBySearchException(QMSDBEntities db, CreateQMS_TR_EXCEPTION searchModel)
        {
            List<QMS_TR_OFF_CONTROL_CAL> lstResult = new List<QMS_TR_OFF_CONTROL_CAL>();

            IQueryable<QMS_TR_OFF_CONTROL_CAL> query = db.QMS_TR_OFF_CONTROL_CAL.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }
    }

    //public partial class QMS_TR_PRODUCT_DATA 
    //{
    //    public static List<QMS_TR_PRODUCT_DATA> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_TR_PRODUCT_DATA.ToList(); //linq
    //    }

    //    public static QMS_TR_PRODUCT_DATA GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_PRODUCT_DATA.Where(m => m.ID == id).FirstOrDefault();
    //    }

    //    public static List<QMS_TR_PRODUCT_DATA> GetAllByExaTagIdAndDateTime(QMSDBEntities db, long[] ids, DateTime startDate, DateTime endDate)
    //    {
    //        return db.QMS_TR_PRODUCT_DATA.Where(m => ids.Contains(m.EXA_TAG_ID) && m.TAG_DATE >= startDate && m.TAG_DATE <= endDate).ToList(); //linq
    //    }

    //    public static List<QMS_TR_PRODUCT_DATA> GetAllByPlantProductExaTagDateTime(QMSDBEntities db, AccumTagQMS_TR_PRODUCT_DATA searchModel)
    //    {
    //        return db.QMS_TR_PRODUCT_DATA.Where( m => m.PLANT_ID == searchModel.PLANT_ID && m.PRODUCT_ID == searchModel.PRODUCT_ID 
    //            && searchModel.EXA_TAG_ID.Contains( m.EXA_TAG_ID)
    //            && m.TAG_DATE >= searchModel.start && m.TAG_DATE <= searchModel.end).ToList(); //linq
    //    }

    //    public static List<QMS_TR_PRODUCT_DATA> GetListByExaQuatumService(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, long[] exaId)
    //    {
    //        return dataContext.QMS_TR_PRODUCT_DATA.Where(m => m.TAG_DATE >= startDate && m.TAG_DATE <= endDate && exaId.Contains(m.EXA_TAG_ID)).ToList();
    //    }

    //    public static List<QMS_TR_PRODUCT_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_PRODUCT_DATA.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static List<QMS_TR_PRODUCT_DATA> GetAllBySearch(QMSDBEntities db, ProductDataSearchModel searchModel)
    //    {
    //        List<QMS_TR_PRODUCT_DATA> lstResult = new List<QMS_TR_PRODUCT_DATA>();

    //        IQueryable<QMS_TR_PRODUCT_DATA> query = db.QMS_TR_PRODUCT_DATA;

    //        try
    //        {

    //            if (searchModel.PLANT_ID > 0)
    //            {
    //                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
    //            }

    //            if (searchModel.PRODUCT_ID > 0)
    //            {
    //                query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
    //            }

    //            if (searchModel.EXA_TAG_ID > 0)
    //            {
    //                query = query.Where(m => m.EXA_TAG_ID == searchModel.EXA_TAG_ID);
    //            }

    //            if (null != searchModel.TAG_DATE)
    //            {
    //                query = query.Where(m => m.TAG_DATE == searchModel.TAG_DATE);
    //            }

    //            if (null != searchModel.TAG_DATA && "" != searchModel.TAG_DATA)
    //            {
    //                query = query.Where(m => m.TAG_DATA.Contains(searchModel.TAG_DATA));
    //            }

    //            if (searchModel.EXA_TAG_ID > 0)
    //            {
    //                query = query.Where(m => m.EXA_TAG_ID == searchModel.EXA_TAG_ID);
    //            }

    //            if (null != searchModel.TAG_VALUE && searchModel.TAG_VALUE > 0)
    //            {
    //                query = query.Where(m => m.TAG_VALUE == searchModel.TAG_VALUE);
    //            }

    //            if (null != searchModel.OFF_CONTROL_FLAG_0 && searchModel.OFF_CONTROL_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.OFF_CONTROL_FLAG_0 == searchModel.OFF_CONTROL_FLAG_0);
    //            }

    //            if (null != searchModel.OFF_SPEC_FLAG_0 && searchModel.OFF_SPEC_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.OFF_SPEC_FLAG_0 == searchModel.OFF_SPEC_FLAG_0);
    //            }

    //            if (null != searchModel.TURN_AROUND_FLAG_0 && searchModel.TURN_AROUND_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.TURN_AROUND_FLAG_0 == searchModel.TURN_AROUND_FLAG_0);
    //            }

    //            if (null != searchModel.DOWNTIME_FLAG_0 && searchModel.DOWNTIME_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.DOWNTIME_FLAG_0 == searchModel.DOWNTIME_FLAG_0);
    //            }

    //            if (null != searchModel.CHANGE_GRADE_FLAG_0 && searchModel.CHANGE_GRADE_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.CHANGE_GRADE_FLAG_0 == searchModel.CHANGE_GRADE_FLAG_0);
    //            }

    //            if (null != searchModel.CROSS_VOLUME_FLAG_0 && searchModel.CROSS_VOLUME_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.CROSS_VOLUME_FLAG_0 == searchModel.CROSS_VOLUME_FLAG_0);
    //            }

    //            if (null != searchModel.GC_ERROR_FLAG_0 && searchModel.GC_ERROR_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.GC_ERROR_FLAG_0 == searchModel.GC_ERROR_FLAG_0);
    //            }

    //            if (null != searchModel.REDUCE_FEED_FLAG_0 && searchModel.REDUCE_FEED_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.REDUCE_FEED_FLAG_0 == searchModel.REDUCE_FEED_FLAG_0);
    //            }

    //            if (null != searchModel.OFF_CONTROL_FLAG && searchModel.OFF_CONTROL_FLAG > 0)
    //            {
    //                query = query.Where(m => m.OFF_CONTROL_FLAG == searchModel.OFF_CONTROL_FLAG);
    //            }

    //            if (null != searchModel.OFF_SPEC_FLAG && searchModel.OFF_SPEC_FLAG > 0)
    //            {
    //                query = query.Where(m => m.OFF_SPEC_FLAG == searchModel.OFF_SPEC_FLAG);
    //            }

    //            if (null != searchModel.TURN_AROUND_FLAG && searchModel.TURN_AROUND_FLAG > 0)
    //            {
    //                query = query.Where(m => m.TURN_AROUND_FLAG == searchModel.TURN_AROUND_FLAG);
    //            }

    //            if (null != searchModel.DOWNTIME_FLAG && searchModel.DOWNTIME_FLAG > 0)
    //            {
    //                query = query.Where(m => m.DOWNTIME_FLAG == searchModel.DOWNTIME_FLAG);
    //            }

    //            if (null != searchModel.CHANGE_GRADE_FLAG && searchModel.CHANGE_GRADE_FLAG > 0)
    //            {
    //                query = query.Where(m => m.CHANGE_GRADE_FLAG == searchModel.CHANGE_GRADE_FLAG);
    //            }

    //            if (null != searchModel.CROSS_VOLUME_FLAG && searchModel.CROSS_VOLUME_FLAG > 0)
    //            {
    //                query = query.Where(m => m.CROSS_VOLUME_FLAG == searchModel.CROSS_VOLUME_FLAG);
    //            }

    //            if (null != searchModel.GC_ERROR_FLAG && searchModel.GC_ERROR_FLAG > 0)
    //            {
    //                query = query.Where(m => m.GC_ERROR_FLAG == searchModel.GC_ERROR_FLAG);
    //            }

    //            if (null != searchModel.REDUCE_FEED_FLAG && searchModel.REDUCE_FEED_FLAG > 0)
    //            {
    //                query = query.Where(m => m.REDUCE_FEED_FLAG == searchModel.REDUCE_FEED_FLAG);
    //            }

    //            if (null != searchModel.EXCEPTION_FLAG && searchModel.EXCEPTION_FLAG > 0)
    //            {
    //                query = query.Where(m => m.EXCEPTION_FLAG == searchModel.EXCEPTION_FLAG);
    //            }

    //            if (null != searchModel.EXCEPTION_VALUE && searchModel.EXCEPTION_VALUE > 0)
    //            {
    //                query = query.Where(m => m.EXCEPTION_VALUE == searchModel.EXCEPTION_VALUE);
    //            }

    //            lstResult = query.ToList();
    //        }
    //        catch
    //        {

    //        }
    //        return lstResult;
    //    }
    //}

    //public partial class QMS_TR_PRODUCT_DOWNTIME_DATA 
    //{
    //    public static List<QMS_TR_PRODUCT_DOWNTIME_DATA> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_TR_PRODUCT_DOWNTIME_DATA.ToList(); //linq
    //    }

    //    public static QMS_TR_PRODUCT_DOWNTIME_DATA GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_PRODUCT_DOWNTIME_DATA.Where(m => m.ID == id).FirstOrDefault();
    //    }

    //    public static List<QMS_TR_PRODUCT_DOWNTIME_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_PRODUCT_DOWNTIME_DATA.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static List<QMS_TR_PRODUCT_DOWNTIME_DATA> GetListByStartEndDateAndDowntimeId(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, long[] ids)
    //    {
    //        return dataContext.QMS_TR_PRODUCT_DOWNTIME_DATA.Where(m => m.TAG_DATE >= startDate && m.TAG_DATE <= endDate && ids.Contains(m.DOWNTIME_DETAIL_ID)).ToList();
    //    }

    //    public static List<QMS_TR_PRODUCT_DOWNTIME_DATA> GetAllBySearch(QMSDBEntities db, ProductDowntimeDataSearchModel searchModel)
    //    {
    //        List<QMS_TR_PRODUCT_DOWNTIME_DATA> lstResult = new List<QMS_TR_PRODUCT_DOWNTIME_DATA>();

    //        IQueryable<QMS_TR_PRODUCT_DOWNTIME_DATA> query = db.QMS_TR_PRODUCT_DOWNTIME_DATA;

    //        try
    //        {

    //            if (searchModel.DOWNTIME_DETAIL_ID > 0)
    //            {
    //                query = query.Where(m => m.DOWNTIME_DETAIL_ID == searchModel.DOWNTIME_DETAIL_ID);
    //            }

    //            if (null != searchModel.TAG_DATE)
    //            {
    //                query = query.Where(m => m.TAG_DATE == searchModel.TAG_DATE);
    //            }

    //            if (null != searchModel.TAG_VALUE && searchModel.TAG_VALUE > 0)
    //            {
    //                query = query.Where(m => m.TAG_VALUE == searchModel.TAG_VALUE);
    //            }

    //            if (null != searchModel.DOWNTIME_FLAG_0 && searchModel.DOWNTIME_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.DOWNTIME_FLAG_0 == searchModel.DOWNTIME_FLAG_0);
    //            }

    //            if (null != searchModel.DOWNTIME_FLAG && searchModel.DOWNTIME_FLAG > 0)
    //            {
    //                query = query.Where(m => m.DOWNTIME_FLAG == searchModel.DOWNTIME_FLAG);
    //            }
    //            lstResult = query.ToList();
    //        }
    //        catch
    //        {

    //        }
    //        return lstResult;
    //    }
    //}
    
    public partial class QMS_TR_RAW_DATA 
    {
        public static List<QMS_TR_RAW_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_RAW_DATA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_RAW_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_RAW_DATA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_RAW_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_RAW_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_RAW_DATA> GetAllBySearch(QMSDBEntities db, RawDataSearchModel searchModel)
        {
            List<QMS_TR_RAW_DATA> lstResult = new List<QMS_TR_RAW_DATA>();

            IQueryable<QMS_TR_RAW_DATA> query = db.QMS_TR_RAW_DATA.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if ( searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }


                if (null != searchModel.DATE_START && null != searchModel.DATE_END)
                {
                    query = query.Where((m => (searchModel.DATE_START > m.DATE_START && searchModel.DATE_END < m.DATE_END)
                                          || (m.DATE_START >= searchModel.DATE_START && m.DATE_END <= searchModel.DATE_END)
                                          || (searchModel.DATE_END > m.DATE_START && searchModel.DATE_END < m.DATE_END)
                                          || (searchModel.DATE_START > m.DATE_START && searchModel.DATE_START < m.DATE_END)
                                      ));
                }
                else
                {
                    if (null != searchModel.DATE_START)
                    {
                        query = query.Where(m => m.DATE_START >= searchModel.DATE_START);
                    }

                    if (null != searchModel.DATE_END)
                    {
                        query = query.Where(m => m.DATE_END <= searchModel.DATE_END);
                    }
                }

                

                if (null != searchModel.FILE_PATH && "" != searchModel.FILE_PATH)
                {
                    query = query.Where(m => m.FILE_PATH.Contains(searchModel.FILE_PATH));
                }

                if (null != searchModel.RAW_DATA_DESC && "" != searchModel.RAW_DATA_DESC)
                {
                    query = query.Where(m => m.RAW_DATA_DESC.Contains(searchModel.RAW_DATA_DESC));
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        } 
    }

    public partial class QMS_TR_REDUCE_FEED 
    {

        public static List<QMS_TR_REDUCE_FEED> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_REDUCE_FEED GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_REDUCE_FEED.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_REDUCE_FEED> GetByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate)
        {
            return dataContext.QMS_TR_REDUCE_FEED.Where(m => m.START_DATE >= startDate && m.END_DATE <= endDate).ToList();
        }

        public static IQueryable<QMS_TR_REDUCE_FEED> GetQueryableByDateTime(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, List<long> PlantList )
        {

            var query = dataContext.QMS_TR_REDUCE_FEED.AsQueryable();

            if (null != startDate)
            {
                query = query.Where(m => m.START_DATE >= startDate);
            }

            if (null != startDate)
            {
                query = query.Where(m => m.END_DATE <= endDate);
            }

            if (null != PlantList && PlantList.Count() > 0)
            {
                query = query.Where(m => PlantList.Contains(m.PLANT_ID));
            }
             

            return query;
        }


        public static List<QMS_TR_REDUCE_FEED> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_REDUCE_FEED.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_REDUCE_FEED> GetAllByPlantListAndDateTime(QMSDBEntities dataContext, List<long> plantList,  DateTime startDate, DateTime endDate, byte docStatus = 0)
        {
            List<QMS_TR_REDUCE_FEED> listResult = new List<QMS_TR_REDUCE_FEED>();
            IQueryable<QMS_TR_REDUCE_FEED> query = dataContext.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            if (null != plantList && plantList.Count() > 0)
            {
                query = query.Where(m => plantList.Contains(m.PLANT_ID));
            }

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }


        public static List<QMS_TR_REDUCE_FEED> GetAllByPlantAndDateTimeExpand(QMSDBEntities dataContext, long modelID, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_REDUCE_FEED> listResult = new List<QMS_TR_REDUCE_FEED>();
            IQueryable<QMS_TR_REDUCE_FEED> query = dataContext.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0 && m.ID != modelID && m.PLANT_ID == plantID);

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            query = query.Where(m => m.END_DATE == startDate || m.START_DATE == endDate);

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_REDUCE_FEED> GetAllByPlantAndDateTime(QMSDBEntities dataContext, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_REDUCE_FEED> listResult = new List<QMS_TR_REDUCE_FEED>();
            IQueryable<QMS_TR_REDUCE_FEED> query = dataContext.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantID  );

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }


            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_REDUCE_FEED> GetByListIdByCorrectData(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel )
        {
            List<QMS_TR_REDUCE_FEED> listResult = new List<QMS_TR_REDUCE_FEED>();
            IQueryable<QMS_TR_REDUCE_FEED> query = dataContext.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            //if (productId > 0)
            //{
            //    query = dataContext.QMS_TR_REDUCE_FEED.Where(m => m.PRODUCT_ID == productId);
            //}

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_REDUCE_FEED> GetByListIdByCorrectDataEx(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_REDUCE_FEED> listResult = new List<QMS_TR_REDUCE_FEED>();
            IQueryable<QMS_TR_REDUCE_FEED> query = dataContext.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_REDUCE_FEED> GetAllBySearch(QMSDBEntities db, TRReduceFeedSearchModel searchModel)
        {
            List<QMS_TR_REDUCE_FEED> lstResult = new List<QMS_TR_REDUCE_FEED>();

            IQueryable<QMS_TR_REDUCE_FEED> query = db.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if ( searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if ( searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }
                else
                {
                    query = query.Where(m => m.DOC_STATUS != (byte)QMSSystem.Model.DOC_STATUS.INITIAL); //default ไม่เอา inital
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      )); 
                }
                else
                {
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    } 
                }

                if (searchModel.CORRECT_DATA_TYPE > 0)
                {
                    query = query.Where(m => m.CORRECT_DATA_TYPE == searchModel.CORRECT_DATA_TYPE);
                }

                if (null != searchModel.CAUSE_DESC && "" != searchModel.CAUSE_DESC)
                {
                    query = query.Where(m => m.CAUSE_DESC.Contains(searchModel.CAUSE_DESC));
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_TR_REDUCE_FEED> GetAllBySearchTRUN_AROUND(QMSDBEntities db, CreateQMS_TR_TURN_AROUND searchModel)
        {
            List<QMS_TR_REDUCE_FEED> lstResult = new List<QMS_TR_REDUCE_FEED>();

            IQueryable<QMS_TR_REDUCE_FEED> query = db.QMS_TR_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0 && m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);

            try
            {
                if (searchModel.PLANT_ID > 0)
                {

                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (searchModel.START_DATE != null && searchModel.END_DATE != null)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                           || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                           || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                       ));
                }

                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }

    }

    //public partial class QMS_TR_REDUCE_FEED_DATA 
    //{

    //    public static List<QMS_TR_REDUCE_FEED_DATA> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_TR_REDUCE_FEED_DATA.ToList(); //linq
    //    }

    //    public static QMS_TR_REDUCE_FEED_DATA GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_REDUCE_FEED_DATA.Where(m => m.ID == id).FirstOrDefault();
    //    }

    //    public static List<QMS_TR_REDUCE_FEED_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_REDUCE_FEED_DATA.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static List<QMS_TR_REDUCE_FEED_DATA> GetListByStartEndDateAndReduceId(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, long[] ids)
    //    {
    //        return dataContext.QMS_TR_REDUCE_FEED_DATA.Where(m => m.TAG_DATE >= startDate && m.TAG_DATE <= endDate && ids.Contains(m.REDUCE_FEED_DETAIL_ID)).ToList();
    //    }


    //    public static List<QMS_TR_REDUCE_FEED_DATA> GetAllBySearch(QMSDBEntities db, TRReduceFeedDataSearchModel searchModel)
    //    {
    //        List<QMS_TR_REDUCE_FEED_DATA> lstResult = new List<QMS_TR_REDUCE_FEED_DATA>();

    //        IQueryable<QMS_TR_REDUCE_FEED_DATA> query = db.QMS_TR_REDUCE_FEED_DATA;

    //        try
    //        {

    //            if (  searchModel.REDUCE_FEED_ID > 0)
    //            {
    //                query = query.Where(m => m.REDUCE_FEED_DETAIL_ID == searchModel.REDUCE_FEED_ID);
    //            }

    //            if (null != searchModel.TAG_DATE)
    //            {
    //                query = query.Where(m => m.TAG_DATE == searchModel.TAG_DATE);
    //            }

    //            if (null != searchModel.TAG_VALUE && searchModel.TAG_VALUE > 0)
    //            {
    //                query = query.Where(m => m.TAG_VALUE == searchModel.TAG_VALUE);
    //            }

    //            if (null != searchModel.REDUCE_FEED_FLAG_0 && searchModel.REDUCE_FEED_FLAG_0 > 0)
    //            {
    //                query = query.Where(m => m.REDUCE_FEED_FLAG_0 == searchModel.REDUCE_FEED_FLAG_0);
    //            }

    //            if (null != searchModel.REDUCE_FEED_FLAG && searchModel.REDUCE_FEED_FLAG > 0)
    //            {
    //                query = query.Where(m => m.REDUCE_FEED_FLAG == searchModel.REDUCE_FEED_FLAG);
    //            }

    //            lstResult = query.ToList();
    //        }
    //        catch
    //        {

    //        }
    //        return lstResult;
    //    }



    //}

    public partial class QMS_TR_TEMPLATE_COA 
    {

        public static List<QMS_TR_TEMPLATE_COA> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_TEMPLATE_COA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_TEMPLATE_COA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_TEMPLATE_COA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_TR_TEMPLATE_COA GetByCOA_ID(QMSDBEntities dataContext, string COA_ID)
        {
            return dataContext.QMS_TR_TEMPLATE_COA.Where(m => m.COA_ID == COA_ID).FirstOrDefault();
        } 

        public static List<QMS_TR_TEMPLATE_COA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_TEMPLATE_COA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_TEMPLATE_COA> GetAllBySearch(QMSDBEntities db, TemplateCOASearchModel searchModel)
        {
            List<QMS_TR_TEMPLATE_COA> lstResult = new List<QMS_TR_TEMPLATE_COA>();

            IQueryable<QMS_TR_TEMPLATE_COA> query = db.QMS_TR_TEMPLATE_COA.Where(m => m.DELETE_FLAG == 0);

            try
            {
 
                if ( searchModel.COA_DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.COA_DOC_STATUS);
                }

                if (null != searchModel.PRODUCT && searchModel.PRODUCT != "")
                {
                    query = query.Where(m => m.PRODUCT_NAME == searchModel.PRODUCT);
                }

                if (null != searchModel.CUSTOMER && searchModel.CUSTOMER != "")
                {
                    query = query.Where(m => m.CUSTOMER_NAME == searchModel.CUSTOMER);
                }

                if (null != searchModel.CUSTOMER_MERGE_ID && searchModel.CUSTOMER_MERGE_ID != 0)
                {
                    query = query.Where(m => m.CUSTOMER_MERGE_ID == searchModel.CUSTOMER_MERGE_ID);
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }
        
    }

    public partial class QMS_TR_TEMPLATE_COA_DETAIL 
    {

        public static List<QMS_TR_TEMPLATE_COA_DETAIL> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_TEMPLATE_COA_DETAIL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_TEMPLATE_COA_DETAIL> GetByCOAId(QMSDBEntities dataContext, string COA_ID)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.COA_ID == COA_ID).ToList();
        }

        public static List<QMS_TR_TEMPLATE_COA_DETAIL> GetByCustomerAndProduct(QMSDBEntities dataContext,string customer, string product)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.CUSTOMER_NAME == customer && m.PRODUCT_NAME == product ).ToList();
        }

        public static List<QMS_TR_TEMPLATE_COA_DETAIL> GetByCustomerAndProductEx(QMSDBEntities dataContext, string customer, string product)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.CUSTOMER_NAME == customer && m.PRODUCT_NAME == product && m.TREND_GRAPH_FLAG == 1).ToList();
        }

        public static List<QMS_TR_TEMPLATE_COA_DETAIL> GetByCustomerNewAndOldAndProductEx(QMSDBEntities dataContext, string[] customer, string product)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && customer.Contains(m.CUSTOMER_NAME) && m.PRODUCT_NAME == product && m.TREND_GRAPH_FLAG == 1).ToList();
        }

        public static List<QMS_TR_TEMPLATE_COA_DETAIL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && ids.Contains(m.ID)).ToList();
        }

        public static QMS_TR_TEMPLATE_COA_DETAIL GetByCOAIdAndItem(QMSDBEntities dataContext, string CoaId, string item)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.COA_ID == CoaId && m.ITEM_NAME == item).FirstOrDefault();
        }
        public static QMS_TR_TEMPLATE_COA_DETAIL GetByTrendCOA(QMSDBEntities dataContext, string customer, string product, string item, string unit, string specification, string testmethod)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.CUSTOMER_NAME.Trim() == customer && m.PRODUCT_NAME.Trim() == product.Trim() && m.ITEM_NAME.Trim() == item.Trim() && m.UNIT_NAME.Trim() == unit.Trim() && m.SPECIFICATION.Trim() == specification.Trim() && m.TEST_METHOD.Trim() == testmethod.Trim()).FirstOrDefault();
        }
        public static QMS_TR_TEMPLATE_COA_DETAIL GetByTrendCOA2(QMSDBEntities dataContext, string customer, string product, long productId)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.CUSTOMER_NAME.Trim() == customer && m.PRODUCT_NAME.Trim() == product.Trim() && m.PRODUCT_MERGE_ID == productId ).FirstOrDefault();
        }
        public static QMS_TR_TEMPLATE_COA_DETAIL GetByTrendCOA_NewAndOldCus(QMSDBEntities dataContext, string[] customer, string product, string item, string unit)
        {
            return dataContext.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0 && customer.Contains(m.CUSTOMER_NAME.Trim()) && m.PRODUCT_NAME.Trim() == product.Trim() && m.ITEM_NAME.Trim() == item.Trim() && m.UNIT_NAME.Trim() == unit.Trim()).FirstOrDefault();
        }

        public static List<QMS_TR_TEMPLATE_COA_DETAIL> GetAllBySearch(QMSDBEntities db, TemplateCOADetailSearchModel searchModel)
        {
            List<QMS_TR_TEMPLATE_COA_DETAIL> lstResult = new List<QMS_TR_TEMPLATE_COA_DETAIL>();

            IQueryable<QMS_TR_TEMPLATE_COA_DETAIL> query = db.QMS_TR_TEMPLATE_COA_DETAIL.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (null != searchModel.COA_ID && "" != searchModel.COA_ID)
                {
                    query = query.Where(m => m.COA_ID == searchModel.COA_ID);
                } 

                if (null != searchModel.SPECIFICATION && "" != searchModel.SPECIFICATION)
                {
                    query = query.Where(m => m.SPECIFICATION.Contains(searchModel.SPECIFICATION));
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }
    }

    public partial class QMS_TR_TREND_DATA 
    {

        public static List<QMS_TR_TREND_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_TREND_DATA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_TREND_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_TREND_DATA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_TR_TREND_DATA GetByTemplateIdSampleIdItemId(QMSDBEntities dataContext, long templateId, long SampleId, long ItemId, DateTime recordDate)
        {
            return dataContext.QMS_TR_TREND_DATA.Where(m => m.TEMPLATE_ID == templateId && m.SAMPLE_ID == SampleId && m.ITEM_ID == ItemId && m.RECORD_DATE == recordDate).FirstOrDefault();
        }

        public static List<QMS_TR_TREND_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_TREND_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_TREND_DATA> GetByTempIdAndDate(QMSDBEntities dataContext, long templateId, DateTime startDate, DateTime endDate)
        {
            return dataContext.QMS_TR_TREND_DATA.Where(m => m.TEMPLATE_ID == templateId && m.RECORD_DATE >= startDate && m.RECORD_DATE <= endDate).ToList();
        }

        public static List<QMS_TR_TREND_DATA> GetByStartEndDateAndSampleList(QMSDBEntities dataContext, DateTime startDate , DateTime endDate, long[] sampleIds)
        {
            return dataContext.QMS_TR_TREND_DATA.Where(m => m.RECORD_DATE >= startDate && m.RECORD_DATE <= endDate && sampleIds.Contains(m.SAMPLE_ID) && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_TR_TREND_DATA> GetAllBySearch(QMSDBEntities db, TrendDataSearchModel searchModel)
        {
            List<QMS_TR_TREND_DATA> lstResult = new List<QMS_TR_TREND_DATA>();

            IQueryable<QMS_TR_TREND_DATA> query = db.QMS_TR_TREND_DATA.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (  searchModel.TEMPLATE_ID > 0)
                {
                    query = query.Where(m => m.TEMPLATE_ID == searchModel.TEMPLATE_ID);
                }

                if ( searchModel.SAMPLE_ID > 0)
                {
                    query = query.Where(m => m.SAMPLE_ID == searchModel.SAMPLE_ID);
                }

                if ( searchModel.ITEM_ID > 0)
                {
                    query = query.Where(m => m.ITEM_ID == searchModel.ITEM_ID);
                }

                if (null != searchModel.RECORD_DATE)
                {
                    query = query.Where(m => m.RECORD_DATE == searchModel.RECORD_DATE);
                }

                if (null != searchModel.CONC_LOADING_FLAG && searchModel.CONC_LOADING_FLAG > 0)
                {
                    query = query.Where(m => m.CONC_LOADING_FLAG == searchModel.CONC_LOADING_FLAG);
                }

                if (null != searchModel.DATA_VALUE_TEXT && "" != searchModel.DATA_VALUE_TEXT)
                {
                    query = query.Where(m => m.DATA_VALUE_TEXT.Contains(searchModel.DATA_VALUE_TEXT));
                }

                if (null != searchModel.DATA_SIGN_1 && "" != searchModel.DATA_SIGN_1)
                {
                    query = query.Where(m => m.DATA_SIGN_1.Contains(searchModel.DATA_SIGN_1));
                }

                if (null != searchModel.DATA_VALUE_1 && searchModel.DATA_VALUE_1 > 0)
                {
                    query = query.Where(m => m.DATA_VALUE_1 == searchModel.DATA_VALUE_1);
                }

                if (null != searchModel.DATA_SIGN_2 && "" != searchModel.DATA_SIGN_2)
                {
                    query = query.Where(m => m.DATA_SIGN_2.Contains(searchModel.DATA_SIGN_2));
                }

                if (null != searchModel.DATA_VALUE_2 && searchModel.DATA_VALUE_2 > 0)
                {
                    query = query.Where(m => m.DATA_VALUE_2 == searchModel.DATA_VALUE_2);
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }
    }

    public partial class QMS_TR_TURN_AROUND 
    {

        public static List<QMS_TR_TURN_AROUND> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_TURN_AROUND.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_TURN_AROUND GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_TURN_AROUND.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_TURN_AROUND> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_TURN_AROUND.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_TURN_AROUND> GetAllByPlantAndDateTime(QMSDBEntities dataContext, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<QMS_TR_TURN_AROUND> listResult = new List<QMS_TR_TURN_AROUND>();
            IQueryable<QMS_TR_TURN_AROUND> query = dataContext.QMS_TR_TURN_AROUND.Where(m => m.DELETE_FLAG == 0 );

            if (plantID > 0)
            {
                query = query.Where(m => m.PLANT_ID == plantID);
            }

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL); 
            }

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_TURN_AROUND> GetAllByPlantListAndDateTime(QMSDBEntities dataContext, List<long> plantList, DateTime startDate, DateTime endDate, byte docStatus = 0)
        {
            List<QMS_TR_TURN_AROUND> listResult = new List<QMS_TR_TURN_AROUND>();
            IQueryable<QMS_TR_TURN_AROUND> query = dataContext.QMS_TR_TURN_AROUND.Where(m => m.DELETE_FLAG == 0);

            if (null != plantList && plantList.Count() > 0)
            {
                query = query.Where(m => plantList.Contains(m.PLANT_ID));
            }

            if (docStatus > 0)
            {
                query = query.Where(m => m.DOC_STATUS == docStatus);
            }
            else
            {
                query = query.Where(m => m.DOC_STATUS != (byte)AUTO_OFF_TYPE.INITIAL);
            }

            query = query.Where((m => (startDate > m.START_DATE && endDate < m.END_DATE)
                                          || (m.START_DATE >= startDate && m.END_DATE <= endDate)
                                          || (endDate > m.START_DATE && endDate < m.END_DATE)
                                          || (startDate > m.START_DATE && startDate < m.END_DATE)
                                      ));

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_TURN_AROUND> GetByListIdByCorrectData(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_TURN_AROUND> listResult = new List<QMS_TR_TURN_AROUND>();
            IQueryable<QMS_TR_TURN_AROUND> query = dataContext.QMS_TR_TURN_AROUND.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            } 

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_TURN_AROUND> GetByListIdByCorrectDataEx(QMSDBEntities dataContext, CorrectDataReportSearchModel searchModel)
        {
            List<QMS_TR_TURN_AROUND> listResult = new List<QMS_TR_TURN_AROUND>();
            IQueryable<QMS_TR_TURN_AROUND> query = dataContext.QMS_TR_TURN_AROUND.Where(m => m.DELETE_FLAG == 0 );

            if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.ALL_CONFIRM)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM);
            }
            else if (searchModel.DOC_STATUS == (byte)AUTO_OFF_TYPE.CONFIRM_AND_PENDING)
            {
                query = query.Where(m => m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.COMFRIM || m.DOC_STATUS == (byte)TRANSECTION_DOC_STATUS.PENDING);
            }

            if (searchModel.PLANT_ID > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
            }

            if (searchModel.START_DATE != null && searchModel.END_DATE != null)
            {
                query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
            }

            listResult = query.ToList();

            return listResult;
        }

        public static List<QMS_TR_TURN_AROUND> GetAllBySearch(QMSDBEntities db, TurnAroundSearchModel searchModel)
        {
            List<QMS_TR_TURN_AROUND> lstResult = new List<QMS_TR_TURN_AROUND>();

            IQueryable<QMS_TR_TURN_AROUND> query = db.QMS_TR_TURN_AROUND.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (  searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (  searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS);
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where((m => (searchModel.START_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE)
                                          || (searchModel.END_DATE > m.START_DATE && searchModel.END_DATE < m.END_DATE)
                                          || (searchModel.START_DATE > m.START_DATE && searchModel.START_DATE < m.END_DATE)
                                      ));
                }
                else
                {
                    if (null != searchModel.START_DATE)
                    {
                        query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                    }

                    if (null != searchModel.END_DATE)
                    {
                        query = query.Where(m => m.END_DATE <= searchModel.END_DATE);
                    }
                }

                if ( searchModel.CORRECT_DATA_TYPE > 0)
                {
                    query = query.Where(m => m.CORRECT_DATA_TYPE == searchModel.CORRECT_DATA_TYPE);
                }

                if (null != searchModel.CAUSE_DESC && "" != searchModel.CAUSE_DESC)
                {
                    query = query.Where(m => m.CAUSE_DESC.Contains(searchModel.CAUSE_DESC));
                }
                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }
    }


    //public partial class QMS_TR_EXQ_DOWNTIME_DATA
    //{
    //    public static List<QMS_TR_EXQ_DOWNTIME_DATA> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_TR_EXQ_DOWNTIME_DATA.ToList();
    //    }

    //    public static List<QMS_TR_EXQ_DOWNTIME_DATA> GetAllBySearch(QMSDBEntities db, ExaDowntimeSearchModel search)
    //    {
    //        List<QMS_TR_EXQ_DOWNTIME_DATA> listResult = new List<QMS_TR_EXQ_DOWNTIME_DATA>();

    //        IQueryable<QMS_TR_EXQ_DOWNTIME_DATA> query = db.QMS_TR_EXQ_DOWNTIME_DATA;

    //        try
    //        {
    //            if (search.PLANT_ID > 0)
    //            {
    //                query = query.Where(m => m.PLANT_ID == search.PLANT_ID);
    //            }


    //            if (search.START_DATE != null)
    //            {
    //                query = query.Where(m => m.TAG_DATE >= search.START_DATE);
    //            }

    //            if (search.END_DATE != null)
    //            {
    //                query = query.Where(m => m.TAG_DATE <= search.END_DATE);
    //            }

    //            listResult = query.ToList();
    //        }
    //        catch
    //        {

    //        }

    //        return listResult;
    //    } 

    //    public static List<QMS_TR_EXQ_DOWNTIME_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_EXQ_DOWNTIME_DATA.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static QMS_TR_EXQ_DOWNTIME_DATA GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_EXQ_DOWNTIME_DATA.Where(m => m.ID == id).FirstOrDefault();
    //    }
    //}

    //public partial class QMS_TR_EXQ_REDUCE_FEED_DATA
    //{
    //    public static List<QMS_TR_EXQ_REDUCE_FEED_DATA> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_TR_EXQ_REDUCE_FEED_DATA.ToList();
    //    }

    //    public static List<QMS_TR_EXQ_REDUCE_FEED_DATA> GetAllBySearch(QMSDBEntities db, ExaReduceFeedSearchModel search)
    //    {
    //        List<QMS_TR_EXQ_REDUCE_FEED_DATA> listResult = new List<QMS_TR_EXQ_REDUCE_FEED_DATA>();

    //        IQueryable<QMS_TR_EXQ_REDUCE_FEED_DATA> query = db.QMS_TR_EXQ_REDUCE_FEED_DATA;

    //        try
    //        {
    //            if (search.PLANT_ID > 0)
    //            {
    //                query = query.Where(m => m.PLANT_ID == search.PLANT_ID);
    //            }


    //            if (search.START_DATE != null)
    //            {
    //                query = query.Where(m => m.TAG_DATE >= search.START_DATE);
    //            }

    //            if (search.END_DATE != null)
    //            {
    //                query = query.Where(m => m.TAG_DATE <= search.END_DATE);
    //            }

    //            listResult = query.ToList();
    //        }
    //        catch
    //        {

    //        }

    //        return listResult;
    //    }

    //    public static List<QMS_TR_EXQ_REDUCE_FEED_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_EXQ_REDUCE_FEED_DATA.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static QMS_TR_EXQ_REDUCE_FEED_DATA GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_EXQ_REDUCE_FEED_DATA.Where(m => m.ID == id).FirstOrDefault();
    //    }
    //}

    //public partial class QMS_TR_EXQ_PRODUCT_DATA
    //{
    //    public static List<QMS_TR_EXQ_PRODUCT_DATA> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_TR_EXQ_PRODUCT_DATA.ToList();
    //    }

    //    public static List<QMS_TR_EXQ_PRODUCT_DATA> GetAllBySearch(QMSDBEntities db, ExaProductSearchModel search)
    //    {
    //        List<QMS_TR_EXQ_PRODUCT_DATA> listResult = new List<QMS_TR_EXQ_PRODUCT_DATA>();

    //        IQueryable<QMS_TR_EXQ_PRODUCT_DATA> query = db.QMS_TR_EXQ_PRODUCT_DATA;

    //        try
    //        {
    //            if (search.PLANT_ID > 0)
    //            {
    //                query = query.Where(m => m.PLANT_ID == search.PLANT_ID);
    //            }

    //            if (search.PRODUCT_ID > 0)
    //            {
    //                query = query.Where(m => m.PRODUCT_ID == search.PRODUCT_ID);
    //            }

    //            if (search.START_DATE != null)
    //            {
    //                query = query.Where(m => m.TAG_DATE >= search.START_DATE);
    //            }

    //            if (search.END_DATE != null)
    //            {
    //                query = query.Where(m => m.TAG_DATE <= search.END_DATE);
    //            }

    //            listResult = query.ToList();
    //        }
    //        catch
    //        {

    //        }

    //        return listResult;
    //    }

    //    public static List<QMS_TR_EXQ_PRODUCT_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_EXQ_PRODUCT_DATA.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static QMS_TR_EXQ_PRODUCT_DATA GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_EXQ_PRODUCT_DATA.Where(m => m.ID == id).FirstOrDefault();
    //    }
    //}

    public partial class QMS_TR_EXQ_DOWNTIME
    {
        public static List<QMS_TR_EXQ_DOWNTIME> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_EXQ_DOWNTIME.ToList();
        }

        public static List<QMS_TR_EXQ_DOWNTIME> GetAllBySearch(QMSDBEntities db, ExaDowntimeSearchModel search)
        {
            List<QMS_TR_EXQ_DOWNTIME> listResult = new List<QMS_TR_EXQ_DOWNTIME>();

            var query = db.QMS_TR_EXQ_DOWNTIME.Where( m => m.PLANT_ID == search.PLANT_ID) ;

            try
            {
                if (search.START_DATE != null)
                {
                    query = query.Where(m => m.TAG_DATE >= search.START_DATE);
                }

                if (search.END_DATE != null)
                {
                    query = query.Where(m => m.TAG_DATE <= search.END_DATE);
                }

                //if (search.START_DATE != null && search.END_DATE != null)
                //{
                //    query = query.Where((m => (search.START_DATE > m.TAG_DATE && search.END_DATE < m.END_DATE)
                //                           || (m.START_DATE >= search.START_DATE && m.END_DATE <= search.END_DATE)
                //                           || (search.END_DATE > m.START_DATE && search.END_DATE < m.END_DATE)
                //                           || (search.START_DATE > m.START_DATE && search.START_DATE < m.END_DATE)
                //                       ));
                //}

                listResult = query.ToList();
            }
            catch
            {

            }

            return listResult;
        }

        public static List<QMS_TR_EXQ_DOWNTIME> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_EXQ_DOWNTIME.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static QMS_TR_EXQ_DOWNTIME GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_EXQ_DOWNTIME.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_EXQ_DOWNTIME> GetByDate(QMSDBEntities dataContext, DateTime dateTime)
        {
            return dataContext.QMS_TR_EXQ_DOWNTIME.Where(m => m.TAG_DATE == dateTime).ToList();
        }
    }

    public partial class QMS_TR_EXQ_REDUCE_FEED
    {
        public static List<QMS_TR_EXQ_REDUCE_FEED> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_EXQ_REDUCE_FEED.ToList();
        }

        public static List<QMS_TR_EXQ_REDUCE_FEED> GetAllBySearch(QMSDBEntities db, ExaReduceFeedSearchModel search)
        {
            List<QMS_TR_EXQ_REDUCE_FEED> listResult = new List<QMS_TR_EXQ_REDUCE_FEED>();

            var query = db.QMS_TR_EXQ_REDUCE_FEED.Where(m => m.PLANT_ID == search.PLANT_ID);

            try
            {  
                if (search.START_DATE != null)
                {
                    query = query.Where(m => m.TAG_DATE >= search.START_DATE);
                }

                if (search.END_DATE != null)
                {
                    query = query.Where(m => m.TAG_DATE <= search.END_DATE);
                }

                listResult = query.ToList();
            }
            catch
            {

            }

            return listResult;
        }

        public static List<QMS_TR_EXQ_REDUCE_FEED> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_EXQ_REDUCE_FEED.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static QMS_TR_EXQ_REDUCE_FEED GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_EXQ_REDUCE_FEED.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_EXQ_REDUCE_FEED> GetByDate(QMSDBEntities dataContext, DateTime dateTime)
        {
            return dataContext.QMS_TR_EXQ_REDUCE_FEED.Where(m => m.TAG_DATE == dateTime).ToList();
        }
    }

    public partial class QMS_TR_EXQ_PRODUCT
    {
        public static List<QMS_TR_EXQ_PRODUCT> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_EXQ_PRODUCT.ToList();
        }

        public static List<QMS_TR_EXQ_PRODUCT> GetAllBySearch(QMSDBEntities db, ExaProductSearchModel search)
        {
            List<QMS_TR_EXQ_PRODUCT> listResult = new List<QMS_TR_EXQ_PRODUCT>();
            try
            {
                var queryData = db.QMS_TR_EXQ_PRODUCT.Where(m => m.PLANT_ID == search.PLANT_ID);

                //if (listResult.Count() > 0)
                //{
                if (search.PLANT_ID > 0)
                {
                    queryData = queryData.Where(m => m.PLANT_ID == search.PLANT_ID);
                }

                if (search.PRODUCT_ID > 0)
                {
                    queryData = queryData.Where(m => m.PRODUCT_ID == search.PRODUCT_ID);
                } 

                if (search.START_DATE != null)
                {
                    queryData = queryData.Where(m => m.TAG_DATE >= search.START_DATE);
                }

                if (search.END_DATE != null)
                {
                    queryData = queryData.Where(m => m.TAG_DATE <= search.END_DATE);
                }
                //}
                listResult = queryData.ToList();
            }
            catch (Exception ex)
            {

            } 

            return listResult;
        }

        public static List<QMS_TR_EXQ_PRODUCT> GetAllBySearchEx(QMSDBEntities db, ExaProductExSearchModel search)
        {
            List<QMS_TR_EXQ_PRODUCT> listResult = new List<QMS_TR_EXQ_PRODUCT>();
            try
            {
                var queryData = db.QMS_TR_EXQ_PRODUCT.Where(m => m.PLANT_ID == search.PLANT_ID);

                //if (listResult.Count() > 0)
                //{
                if (search.PLANT_ID > 0)
                {
                    queryData = queryData.Where(m => m.PLANT_ID == search.PLANT_ID);
                }

                if (search.PRODUCT_ID > 0)
                {
                    queryData = queryData.Where(m => m.PRODUCT_ID == search.PRODUCT_ID);
                }

                if (null != search.TAG_EXA_ID && search.TAG_EXA_ID.Count() > 0)
                {
                    queryData = queryData.Where(m => search.TAG_EXA_ID.Contains(m.TAG_EXA_ID));
                }

                if (search.START_DATE != null)
                {
                    queryData = queryData.Where(m => m.TAG_DATE >= search.START_DATE);
                }

                if (search.END_DATE != null)
                {
                    queryData = queryData.Where(m => m.TAG_DATE <= search.END_DATE);
                }
                //}
                listResult = queryData.OrderBy( m => m.TAG_DATE).ToList();
            }
            catch (Exception ex)
            {
               
            }
            return listResult;
        }

        public static List<QMS_TR_EXQ_PRODUCT> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_EXQ_PRODUCT.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static QMS_TR_EXQ_PRODUCT GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_EXQ_PRODUCT.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_EXQ_PRODUCT> GetByDateTime(QMSDBEntities dataContext, DateTime dateTime, long plantId, long productId)
        {
            return dataContext.QMS_TR_EXQ_PRODUCT.Where(m => m.TAG_DATE == dateTime && m.PLANT_ID == plantId && m.PRODUCT_ID == productId).ToList();
        }

        public static List<QMS_TR_EXQ_PRODUCT> GetListByPlantIdProductIdTagExa(QMSDBEntities db, long PlantId, long ProductId, long tagExa, DateTime startDate, DateTime endDate )
        {
            List<QMS_TR_EXQ_PRODUCT> listResult = new List<QMS_TR_EXQ_PRODUCT>();

            listResult = db.QMS_TR_EXQ_PRODUCT.Where( m => m.PLANT_ID == PlantId && m.PRODUCT_ID == ProductId && m.TAG_EXA_ID == tagExa && m.TAG_DATE >= startDate && m.TAG_DATE <= endDate ).ToList();

            return listResult;
        }

        public static QMS_TR_EXQ_PRODUCT GetPlantIdProductIdTagExa(QMSDBEntities db, long PlantId, long ProductId, long tagExa, DateTime startDate, DateTime endDate)
        {
            QMS_TR_EXQ_PRODUCT listResult = new QMS_TR_EXQ_PRODUCT();

            listResult = db.QMS_TR_EXQ_PRODUCT.Where(m => m.PLANT_ID == PlantId && m.PRODUCT_ID == ProductId && m.TAG_EXA_ID == tagExa && m.TAG_DATE >= startDate && m.TAG_DATE <= endDate).FirstOrDefault();

            return listResult;
        }

    }

    public partial class QMS_TR_EVENT_RAW_DATA 
    {
        public static List<QMS_TR_EVENT_RAW_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_EVENT_RAW_DATA.ToList(); //linq
        }

        public static List<QMS_TR_EVENT_RAW_DATA> GetAllByEventId(QMSDBEntities db, long eventId)
        {
            return db.QMS_TR_EVENT_RAW_DATA.Where( m => m.event_id == eventId).ToList(); //linq
        }

        public static List<QMS_TR_EVENT_RAW_DATA> GetAllByStartDateEndDate(QMSDBEntities db, DateTime startDate, DateTime endDate)
        {
            return db.QMS_TR_EVENT_RAW_DATA.Where(m => m.recorded_time >= startDate && m.recorded_time <= endDate).ToList(); //linq
        }
        public static QMS_TR_EVENT_RAW_DATA GetAllByRecordTimeTagName(QMSDBEntities db, DateTime recorded_time, string tag_name)
        {
            return db.QMS_TR_EVENT_RAW_DATA.Where(m => m.recorded_time == recorded_time && m.tag_name == tag_name).FirstOrDefault(); //linq
        }
    }

    public partial class QMS_TR_EVENT_CAL
    {
        public static List<QMS_TR_EVENT_CAL> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_EVENT_CAL.ToList();
        }

        public static QMS_TR_EVENT_CAL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_EVENT_CAL.Where(m => m.event_id == id).FirstOrDefault();
        }
    }


    //public partial class QMS_TR_IQC_CONTROL 
    //{
    //    public static List<QMS_TR_IQC_CONTROL> GetAll(QMSDBEntities db)
    //    {

    //        return db.QMS_TR_IQC_CONTROL.Where(m => m.DELETE_FLAG == 0).ToList();
    //    }

    //    public static QMS_TR_IQC_CONTROL GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_IQC_CONTROL.Where(m => m.ID == id).FirstOrDefault();
    //    }

    //    public static List<QMS_TR_IQC_CONTROL> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_IQC_CONTROL.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static List<QMS_TR_IQC_CONTROL> GetByListMailAlertId(QMSDBEntities dataContext, byte AlertId)
    //    {
    //        return dataContext.QMS_TR_IQC_CONTROL.Where(m => m.CONTROL_ID == AlertId && m.DELETE_FLAG == 0).ToList();
    //    }

    //    public static List<QMS_TR_IQC_CONTROL> GetAllBySearch(QMSDBEntities db, IQCControlSearchModel searchModel)
    //    {
    //        List<QMS_TR_IQC_CONTROL> lstResult = new List<QMS_TR_IQC_CONTROL>();

    //        var query = db.QMS_TR_IQC_CONTROL.Where(m => m.DELETE_FLAG == 0);

    //        try
    //        {

    //            if (searchModel.CONTROL_ID > 0)
    //            {
    //                query = query.Where(m => m.CONTROL_ID == searchModel.CONTROL_ID);
    //            }

    //            lstResult = query.ToList();

    //        }
    //        catch
    //        {

    //        }
    //        return lstResult;
    //    }

    //    public static List<QMS_TR_IQC_CONTROL> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
    //    {
    //        return db.QMS_TR_IQC_CONTROL.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
    //    }

    //}

    //public partial class QMS_TR_IQC_CONFIG 
    //{
    //    public static List<QMS_TR_IQC_CONFIG> GetAll(QMSDBEntities db)
    //    {

    //        return db.QMS_TR_IQC_CONFIG.Where(m => m.DELETE_FLAG == 0).ToList();
    //    }

    //    public static QMS_TR_IQC_CONFIG GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_IQC_CONFIG.Where(m => m.ID == id).FirstOrDefault();
    //    }

    //    public static List<QMS_TR_IQC_CONFIG> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_TR_IQC_CONFIG.Where(m => ids.Contains(m.ID)).ToList();
    //    }

    //    public static List<QMS_TR_IQC_CONFIG> GetByListMailAlertId(QMSDBEntities dataContext, byte AlertId)
    //    {
    //        return dataContext.QMS_TR_IQC_CONFIG.Where(m => m.CONTROL_ID == AlertId && m.DELETE_FLAG == 0).ToList();
    //    }

    //    public static List<QMS_TR_IQC_CONFIG> GetAllBySearch(QMSDBEntities db, IQCConfigSearchModel searchModel)
    //    {
    //        List<QMS_TR_IQC_CONFIG> lstResult = new List<QMS_TR_IQC_CONFIG>();

    //        var query = db.QMS_TR_IQC_CONFIG.Where(m => m.DELETE_FLAG == 0);

    //        try
    //        {

    //            if (searchModel.CONTROL_ID > 0)
    //            {
    //                query = query.Where(m => m.CONTROL_ID == searchModel.CONTROL_ID);
    //            }

    //            lstResult = query.ToList();

    //        }
    //        catch
    //        {

    //        }
    //        return lstResult;
    //    }

    //    public static List<QMS_TR_IQC_CONFIG> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
    //    {
    //        return db.QMS_TR_IQC_CONFIG.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
    //    }

    //}

}
