﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using QMSsystem.Models.Helper;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSSystem.CoreDB;
using QMSsystem.Models.Privilege;
using QMSsystem.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OfficeOpenXml;


namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Home, PrivilegeModeEnum.N_LabControlChart }, DataAccess = DataAccessEnum.Anonymous)]
    public class InternalQualityControlController : BaseController
    {
        //
        // GET: /InternalQualityControl/


        public ActionResult ListIQCMasterRule()//control ListMasterIQCRule.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCRuleSearchModel searchIQCRule = new IQCRuleSearchModel();
            CreateQMS_MA_IQC_RULE createQMS_MA_IQC_RULE = new CreateQMS_MA_IQC_RULE();
            ViewBag.listIQCRule = masterDataServices.getQMS_MA_IQC_RULEList(_db);
            ViewBag.searchIQCRule = searchIQCRule;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_IQC_RULE = createQMS_MA_IQC_RULE;
            
            return View("Master/Rule/ListMasterIQCRule");
        }

        public ActionResult CreateMasterIQCRule(long id = 0)//control CresteMasterIQCRule.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_IQC_RULE createQMS_MA_IQC_RULE = new CreateQMS_MA_IQC_RULE();
  
            string actionName = Resources.Strings.AddNew;

    
            ViewBag.createQMS_MA_IQC_RULE = createQMS_MA_IQC_RULE;
 
            ViewBag.ActionName = actionName;
            return View("Master/Rule/ListMasterIQCRule");
        }

        public JsonResult GetCreateQMS_MA_IQC_RULEById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_IQC_RULE createQMS_MA_IQC_RULE = masterDataServices.getQMS_MA_IQC_RULEById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_IQC_RULE };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_RULE(CreateQMS_MA_IQC_RULE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_RULE(_db, model);

                    if (nResult > 0)
                    {   
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult , model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_RULEBySearch(IQCRuleSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_RULE> listData = new List<ViewQMS_MA_IQC_RULE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_RULE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_RULE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_RULEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }





        public ActionResult ListIQCMasterPossibleCause()//control ListMasterIQCPossibleCause.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCPossibleCauseSearchModel searchIQCPossibleCause = new IQCPossibleCauseSearchModel();
            CreateQMS_MA_IQC_POSSICAUSE createQMS_MA_IQC_POSSICAUSE = new CreateQMS_MA_IQC_POSSICAUSE();
            ViewBag.listIQCPossibleCause = masterDataServices.getQMS_MA_IQC_POSSICAUSEList(_db);
            ViewBag.searchIQCPossibleCause = searchIQCPossibleCause;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_IQC_POSSICAUSE = createQMS_MA_IQC_POSSICAUSE;

            return View("Master/PossibleCause/ListMasterIQCPossibleCause");
        }

        public ActionResult CreateMasterPossibleCause(long id = 0)//control CresteMasterIQCPossibleCause.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_IQC_POSSICAUSE createQMS_MA_IQC_POSSICAUSE = new CreateQMS_MA_IQC_POSSICAUSE();

            string actionName = Resources.Strings.AddNew;


            ViewBag.createQMS_MA_IQC_POSSICAUSE = createQMS_MA_IQC_POSSICAUSE;

            ViewBag.ActionName = actionName;
            return View("Master/PossibleCause/ListMasterIQCPossibleCause");
        }

        public JsonResult GetCreateQMS_MA_IQC_POSSICAUSEById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_IQC_POSSICAUSE createQMS_MA_IQC_POSSICAUSE = masterDataServices.getQMS_MA_IQC_POSSICAUSEById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_IQC_POSSICAUSE };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_POSSICAUSE(CreateQMS_MA_IQC_POSSICAUSE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_POSSICAUSE(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_POSSICAUSEBySearch(IQCPossibleCauseSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_POSSICAUSE> listData = new List<ViewQMS_MA_IQC_POSSICAUSE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_POSSICAUSE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAll_QMS_MA_IQC_POSSICAUSE()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                
                    List<ViewQMS_MA_IQC_POSSICAUSE> listData = new List<ViewQMS_MA_IQC_POSSICAUSE>();
                   
                    listData = masterDataServices.getQMS_MA_IQC_POSSICAUSEList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData};
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_POSSICAUSE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_POSSICAUSEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }





        public ActionResult ListIQCMasterFormula()//control ListMasterIQCFormula.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCFormulaSearchModel searchIQCFormula = new IQCFormulaSearchModel();
            CreateQMS_MA_IQC_FORMULA createQMS_MA_IQC_FORMULA = new CreateQMS_MA_IQC_FORMULA();
            ViewBag.listIQCFormula = masterDataServices.getQMS_MA_IQC_FORMULAList(_db);
            ViewBag.searchIQCFormula = searchIQCFormula;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_IQC_FORMULA = createQMS_MA_IQC_FORMULA;

            ViewBag.listIQCEmail = masterDataServices.getQMS_MA_EMAILActiveList(_db);
            ViewBag.listIQCItem = masterDataServices.getQMS_MA_IQC_ITEMActiveList(_db);
            ViewBag.listIQCMethod = masterDataServices.getQMS_MA_IQC_METHODActiveList(_db);
            return View("Master/Formula/ListMasterIQCFormula");
        }

        public ActionResult CreateMasterIQCFormula(long id = 0)//control CreateMasterIQCFormula.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_IQC_FORMULA createQMS_MA_IQC_FORMULA = new CreateQMS_MA_IQC_FORMULA();

            string actionName = Resources.Strings.AddNew;


            ViewBag.createQMS_MA_IQC_FORMULA = createQMS_MA_IQC_FORMULA;

            ViewBag.ActionName = actionName;

            ViewBag.listIQCItem = masterDataServices.getQMS_MA_IQC_ITEMActiveList(_db);
            return View("Master/Formula/ListMasterIQCFormula");
        }

        public JsonResult GetCreateQMS_MA_IQC_FORMULAById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_IQC_FORMULA createQMS_MA_IQC_FORMULA = masterDataServices.getQMS_MA_IQC_FORMULAById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_IQC_FORMULA };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_FORMULA(CreateQMS_MA_IQC_FORMULA model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_FORMULA(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_FORMULABySearch(IQCFormulaSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_FORMULA> listData = new List<ViewQMS_MA_IQC_FORMULA>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_FORMULA(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_FORMULA(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_FORMULAByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_METHOD(CreateQMS_MA_IQC_METHOD model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_METHOD(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadIQCTemplateToCSC()
        {
            try
            {
           

                DateTime startDate = new DateTime(2020, 03, 01, 0, 0, 0);
                DateTime endDate = new DateTime(2020, 03, 02, 0, 0, 0).AddDays(1).AddMilliseconds(-1);


                DateTime startTime = startDate;
                DateTime endTime = startDate.AddDays(1);
                MasterDataService masterDataServices = new MasterDataService(_username);

                while (startTime <= endDate)
                {
                    endTime = startTime.AddDays(1);
                    //masterDataServices.DailyCOAWriteProductQualityToCSC(startTime, endTime, true);
                    startTime = startTime.AddDays(1);
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }
            return View("sendMail");
        }


        public ActionResult ListIQCMasterItem()//control ListMasterIQCItem.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCItemSearchModel searchIQCItem = new IQCItemSearchModel();
            CreateQMS_MA_IQC_ITEM createQMS_MA_IQC_ITEM = new CreateQMS_MA_IQC_ITEM();
            CreateQMS_MA_IQC_ITEMEQUIP createQMS_MA_IQC_ITEMEQUIP = new CreateQMS_MA_IQC_ITEMEQUIP();
            ViewBag.listIQCItem = masterDataServices.getQMS_MA_IQC_ITEMList(_db);
            ViewBag.searchIQCItem = searchIQCItem;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_IQC_ITEM = createQMS_MA_IQC_ITEM;
            ViewBag.createQMS_MA_IQC_ITEMEQUIP = createQMS_MA_IQC_ITEMEQUIP;

            ViewBag.listIQCItemEquip = masterDataServices.getQMS_MA_IQC_ITEMEQUIPActiveList(_db);            

            ViewBag.listIQCEquipment = masterDataServices.getQMS_MA_IQC_EQUIPActiveList(_db);

            return View("Master/Item/ListMasterIQCItem");
        }

        public ActionResult CreateMasterItem(long id = 0)//control CresteMasterIQCItem.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_IQC_ITEM createQMS_MA_IQC_ITEM = new CreateQMS_MA_IQC_ITEM();
            CreateQMS_MA_IQC_ITEMEQUIP createQMS_MA_IQC_ITEMEQUIP = new CreateQMS_MA_IQC_ITEMEQUIP();

            string actionName = Resources.Strings.AddNew;

            ViewBag.createQMS_MA_IQC_ITEM = createQMS_MA_IQC_ITEM;
            ViewBag.createQMS_MA_IQC_ITEMEQUIP = createQMS_MA_IQC_ITEMEQUIP;

            ViewBag.ActionName = actionName;
            return View("Master/Item/ListMasterIQCItem");
        }

        public JsonResult GetCreateQMS_MA_IQC_ITEMById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_IQC_ITEM createQMS_MA_IQC_ITEM = masterDataServices.getQMS_MA_IQC_ITEMById(_db, id);


                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_IQC_ITEM };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_ITEM(CreateQMS_MA_IQC_ITEM model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_ITEM(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_ITEMBySearch(IQCItemSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_ITEM> listData = new List<ViewQMS_MA_IQC_ITEM>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_ITEM(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_ITEM(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_ITEMByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

  
        public ActionResult ListIQCMasterEquip()//control ListMasterIQCEquip.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCEquipSearchModel searchIQCEquip = new IQCEquipSearchModel();
            CreateQMS_MA_IQC_EQUIP createQMS_MA_IQC_EQUIP = new CreateQMS_MA_IQC_EQUIP();
            ViewBag.listIQCEquip = masterDataServices.getQMS_MA_IQC_EQUIPList(_db);
            ViewBag.searchIQCEquip = searchIQCEquip;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_IQC_EQUIP = createQMS_MA_IQC_EQUIP;

            return View("Master/Equip/ListMasterIQCEquip");
        }

        public ActionResult CreateMasterEquip(long id = 0)//control CresteMasterIQCRule.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_IQC_EQUIP createQMS_MA_IQC_EQUIP = new CreateQMS_MA_IQC_EQUIP();

            string actionName = Resources.Strings.AddNew;


            ViewBag.createQMS_MA_IQC_EQUIP = createQMS_MA_IQC_EQUIP;

            ViewBag.ActionName = actionName;
            return View("Master/Equip/ListMasterIQCEquip");
        }

        public JsonResult GetCreateQMS_MA_IQC_EQUIPById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_IQC_EQUIP createQMS_MA_IQC_EQUIP = masterDataServices.getQMS_MA_IQC_EQUIPById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_IQC_EQUIP };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_EQUIP(CreateQMS_MA_IQC_EQUIP model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_EQUIP(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_EQUIPBySearch(IQCEquipSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_EQUIP> listData = new List<ViewQMS_MA_IQC_EQUIP>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_EQUIP(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_EQUIP(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_EQUIPByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

      
        public JsonResult GetListQMS_MA_IQC_ITEMEQUIPById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    //CreateQMS_MA_IQC_ITEMEQUIP createQMS_MA_IQC_ITEMEQUIP = masterDataServices.getQMS_MA_IQC_ITEMEQUIPById(_db, id);

                    List<ViewQMS_MA_IQC_ITEMEQUIP> listItemEquip = masterDataServices.getListQMS_MA_IQC_ITEMEQUIP(_db, id);

                    result = new { result = ReturnStatus.SUCCESS, message = listItemEquip };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_ITEMEQUIP(CreateQMS_MA_IQC_ITEMEQUIP model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_ITEMEQUIP(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_ITEMEQUIPBySearch(IQCItemEquipSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_ITEMEQUIP> listData = new List<ViewQMS_MA_IQC_ITEMEQUIP>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_ITEMEQUIP(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_ITEMEQUIP(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_ITEMEQUIPByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllQMS_MA_IQC_ITEMEQUIP()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_IQC_ITEMEQUIP> listData = new List<ViewQMS_MA_IQC_ITEMEQUIP>();
                    listData = masterDataServices.getQMS_MA_IQC_ITEMEQUIPList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public ActionResult ListIQCMasterMailAlert()//control ListMasterIQCMailAlert.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCMailAlertSearchModel searchIQCMailAlert = new IQCMailAlertSearchModel();
            CreateQMS_MA_IQC_MAILALERT createQMS_MA_IQC_MAILALERT = new CreateQMS_MA_IQC_MAILALERT();
            ViewBag.listIQCMailAlert = masterDataServices.getQMS_MA_IQC_MAILALERTList(_db);
            ViewBag.searchIQCMailAlert = searchIQCMailAlert;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_IQC_MAILALERT = createQMS_MA_IQC_MAILALERT;


            ViewBag.listIQCEmail = masterDataServices.getQMS_MA_EMAILActiveList(_db);
            ViewBag.listIQCPossibleCause = masterDataServices.getQMS_MA_IQC_POSSICAUSEActiveList(_db);

            ViewBag.listEmail = masterDataServices.getQMS_MA_EMAILActiveList(_db);

            return View("Master/MailAlert/ListMasterIQCMailAlert");
        }

        public ActionResult CreateMasterIQCMailAlert(long id = 0)//control CreateMasterIQCMailAlert.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_IQC_MAILALERT createQMS_MA_IQC_MAILALERT = new CreateQMS_MA_IQC_MAILALERT();

            string actionName = Resources.Strings.AddNew;

            ViewBag.listIQCPossibleCause = masterDataServices.getQMS_MA_IQC_POSSICAUSEActiveList(_db);

            ViewBag.createQMS_MA_IQC_MAILALERT = createQMS_MA_IQC_MAILALERT;

            ViewBag.ActionName = actionName;
            return View("Master/MailAlert/ListMasterIQCMailAlert");
        }

        public JsonResult GetCreateQMS_MA_IQC_MAILALERTById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_IQC_MAILALERT createQMS_MA_IQC_MAILALERT = masterDataServices.getQMS_MA_IQC_MAILALERTById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_IQC_MAILALERT };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_MAILALERT(CreateQMS_MA_IQC_MAILALERT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_MAILALERT(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_ADDEMAIL(CreateQMS_MA_EMAIL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_ADDEMAIL(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetQMS_MA_IQC_MAILALERTBySearch(IQCMailAlertSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_MAILALERT> listData = new List<ViewQMS_MA_IQC_MAILALERT>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_MAILALERT(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllQMS_MA_EMAIL()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_EMAIL> listData = new List<ViewQMS_MA_EMAIL>();
                    listData = masterDataServices.getQMS_MA_EMAILList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_MAILALERT(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_MAILALERTByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_EMAIL(CreateQMS_MA_EMAIL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_EMAIL(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        if (nResult == -1)
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.PlantNameDuplicate };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }

                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_EMAIL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_EMAILByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        public ActionResult IQCControlChart()
        {


            MasterDataService masterDataServices = new MasterDataService(_username);

            IQCMailAlertSearchModel searchIQCMailAlert = new IQCMailAlertSearchModel();
            ViewBag.searchIQCMailAlert = searchIQCMailAlert;
            ViewBag.listIQCMailAlert = masterDataServices.getQMS_MA_IQC_MAILALERTList(_db);

            CreateQMS_MA_IQC_MAILALERT createQMS_MA_IQC_MAILALERT = new CreateQMS_MA_IQC_MAILALERT();              
            ViewBag.createQMS_MA_IQC_MAILALERT = createQMS_MA_IQC_MAILALERT;
            ViewBag.listPageSize = this.getPageSizeList();


            ViewBag.listIQCEmail = masterDataServices.getQMS_MA_EMAILActiveList(_db);
            ViewBag.listIQCPossibleCause = masterDataServices.getQMS_MA_IQC_POSSICAUSEActiveList(_db);
            ViewBag.listIQCControlRule = masterDataServices.getQMS_MA_IQC_CONTROLRULEActiveList(_db);
            ViewBag.listIQCPossibleCauseReason = masterDataServices.getQMS_MA_IQC_CONTROLREASONActiveList(_db);

            ViewBag.listIQCTemplate = masterDataServices.getQMS_MA_IQC_TEMPLATEList(_db);
            CreateQMS_MA_IQC_TEMPLATE createQMS_MA_IQC_TEMPLATE = new CreateQMS_MA_IQC_TEMPLATE();
            ViewBag.createQMS_MA_IQC_TEMPLATE = createQMS_MA_IQC_TEMPLATE;

            IQCTemplateSearchModel searchIQCTemplate = new IQCTemplateSearchModel();
            ViewBag.searchIQCTemplate = searchIQCTemplate;

            IQCItemSearchModel searchIQCItem = new IQCItemSearchModel();
            ViewBag.searchIQCItem = searchIQCItem;

            IQCItemEquipSearchModel searchIQCEquipment = new IQCItemEquipSearchModel();
            ViewBag.searchIQCEquipment = searchIQCEquipment;

            ViewBag.listIQCMethod = masterDataServices.getQMS_MA_IQC_METHODActiveList(_db);

            IQCControlDataSearchModel searchIQCControlData = new IQCControlDataSearchModel();
            ViewBag.searchIQCControlData = searchIQCControlData;

            ViewBag.listIQCRule = masterDataServices.getQMS_MA_IQC_RULEList(_db);
            CreateQMS_MA_IQC_CONTROLRULE createQMS_MA_IQC_CONTROLRULE = new CreateQMS_MA_IQC_CONTROLRULE();
            ViewBag.createQMS_MA_IQC_CONTROLRULE = createQMS_MA_IQC_CONTROLRULE;

            CreateQMS_MA_IQC_CONTROLREASON createQMS_MA_IQC_CONTROLREASON = new CreateQMS_MA_IQC_CONTROLREASON();
            ViewBag.createQMS_MA_IQC_CONTROLREASON = createQMS_MA_IQC_CONTROLREASON;


            ViewBag.listIQCItem = masterDataServices.getQMS_MA_IQC_ITEMActiveList(_db);
            return View("LabControl/LabControlChart");
        }

        public ActionResult CreateMasterIQCControl(long id = 0)//control CreateMasterIQCControl.cshtml
        {          

            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_IQC_CONTROL createQMS_TR_IQC_CONTROL = new CreateQMS_TR_IQC_CONTROL();

            string actionName = Resources.Strings.AddNew;

            ViewBag.listIQCPossibleCause = masterDataServices.getQMS_MA_IQC_POSSICAUSEActiveList(_db);

            ViewBag.createQMS_TR_IQC_CONTROL = createQMS_TR_IQC_CONTROL;

            ViewBag.listIQCTemplate = masterDataServices.getQMS_MA_IQC_TEMPLATEList(_db);

            ViewBag.ActionName = actionName;
            return View("LabControl/LabControlChart");
        }

        public JsonResult GetCreateQMS_MA_IQC_TEMPLATEById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_IQC_TEMPLATE createQMS_MA_IQC_TEMPLATE = masterDataServices.getQMS_MA_IQC_TEMPLATEById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_IQC_TEMPLATE };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_ControlDataBySearch(IQCControlDataSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_IQC_CONTROLDATA> listData = new List<ViewQMS_MA_IQC_CONTROLDATA>();
                    listData = masterDataServices.searchQMS_MA_IQC_CONTROLDATA(_db, model);

                    result = new { result = ReturnStatus.SUCCESS, message = listData};
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveDraftControlRule(List<DraftControlRule> modelDraftControlRule)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices configDataServices = new ConfigServices(_username);
                MasterDataService MasterDataService = new MasterDataService(_username);
                long bResult = 0;
                int nSuccess = 0;
                int nFailure = 0;
                string szRULE_ID = "";
                string logText = "";
                try
                {
                    foreach (DraftControlRule model in modelDraftControlRule)
                    {
                        bResult = MasterDataService.SaveQMS_MA_IQC_DRAFTCONTROLRULE(_db, model);
                        if (0 == bResult)
                        {
                            nSuccess++;
                        }
                        else
                        {
                            szRULE_ID = model.RULE_ID;
                            nFailure++;
                            break;
                        }
                    }

                    if (0 == nFailure)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = MasterDataService.getError() };
                    }

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                    logText += string.Format(ex.Message);
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_IQC_CONTROLRULE(CreateQMS_MA_IQC_CONTROLRULE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_CONTROLRULE(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetQMS_MA_IQC_CONTROLREASONBySearch(IQCControlReasonSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_CONTROLREASON> listData = new List<ViewQMS_MA_IQC_CONTROLREASON>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();

                    listData = masterDataServices.searchQMS_MA_IQC_CONTROLREASON(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CreateQMS_MA_IQC_CONTROLREASON(CreateQMS_MA_IQC_CONTROLREASON model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_IQC_CONTROLREASON(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteQMS_MA_IQC_CONTROLREASON(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_CONTROLREASONByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteDraftQMS_MA_IQC_CONTROLREASON()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteDraftQMS_MA_IQC_CONTROLREASON(_db);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_IQC_CONTROLRULE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_CONTROLRULEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //public JsonResult GetCreateQMS_TR_IQC_CONTROLById(long id)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            CreateQMS_TR_IQC_CONTROL createQMS_TR_IQC_CONTROL = TransactionService.getQMS_TR_IQC_CONTROLById(_db, id);
        //            result = new { result = ReturnStatus.SUCCESS, message = createQMS_TR_IQC_CONTROL };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult CreateQMS_TR_IQC_CONTROL(CreateQMS_TR_IQC_CONTROL model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            long nResult = TransactionService.SaveQMS_TR_IQC_CONTROL(_db, model);

        //            if (nResult > 0)
        //            {
        //                result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
        //            }
        //            else
        //            {
        //                result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult GetQMS_TR_IQC_CONTROLBySearch(IQCControlSearch model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            long count = 0;
        //            long totalPage = 0;
        //            List<ViewQMS_TR_IQC_CONTROL> listData = new List<ViewQMS_TR_IQC_CONTROL>();
        //            List<PageIndexList> listPageIndex = new List<PageIndexList>();
        //            listData = TransactionService.searchQMS_TR_IQC_CONTROL(_db, model, out count, out totalPage, out listPageIndex);

        //            result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult DeleteQMS_TR_IQC_CONTROL(long[] ids)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            bool nResult = TransactionService.DeleteQMS_TR_IQC_CONTROLByListId(_db, ids);

        //            if (true == nResult)
        //            {
        //                result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
        //            }
        //            else
        //            {
        //                result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult GetCreateQMS_TR_IQC_CONFIGById(long id)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            CreateQMS_TR_IQC_CONFIG createQMS_TR_IQC_CONFIG = TransactionService.getQMS_TR_IQC_CONFIGById(_db, id);
        //            result = new { result = ReturnStatus.SUCCESS, message = createQMS_TR_IQC_CONFIG };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult CreateQMS_TR_IQC_CONFIG(CreateQMS_TR_IQC_CONFIG model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            long nResult = TransactionService.SaveQMS_TR_IQC_CONFIG(_db, model);

        //            if (nResult > 0)
        //            {
        //                result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
        //            }
        //            else
        //            {
        //                result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult GetQMS_TR_IQC_CONFIGBySearch(IQCConfigSearch model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            long count = 0;
        //            long totalPage = 0;
        //            List<ViewQMS_TR_IQC_CONFIG> listData = new List<ViewQMS_TR_IQC_CONFIG>();
        //            List<PageIndexList> listPageIndex = new List<PageIndexList>();
        //            listData = TransactionService.searchQMS_TR_IQC_CONFIG(_db, model, out count, out totalPage, out listPageIndex);

        //            result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult DeleteQMS_TR_IQC_CONFIG(long[] ids)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        TransactionService TransactionService = new TransactionService(_username);

        //        try
        //        {
        //            bool nResult = TransactionService.DeleteQMS_TR_IQC_CONFIGByListId(_db, ids);

        //            if (true == nResult)
        //            {
        //                result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
        //            }
        //            else
        //            {
        //                result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}





        public ActionResult ImportExportEXQTemplate()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);

            return View("Master/Template/ListIQCTemplateGAS");
        }

        public ActionResult ListIQCMasterTemplateGAS(long id)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCTemplateSearchModel searchIQCTemplate = new IQCTemplateSearchModel();
            CreateQMS_MA_IQC_TEMPLATE createQMS_MA_IQC_TEMPLATE = new CreateQMS_MA_IQC_TEMPLATE();
      
            IQCControlDataSearchModel searchItem = new IQCControlDataSearchModel();
          
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);// .getQMS_MA_UNITActiveList(_db);

            List<ViewQMS_MA_IQC_ITEMEQUIP> listIQCItemEquip = masterDataServices.getQMS_MA_IQC_ITEMEQUIPList(_db);

            searchIQCTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;
            createQMS_MA_IQC_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;

            ViewBag.searchIQCTemplate = searchIQCTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_IQC_TEMPLATE = createQMS_MA_IQC_TEMPLATE;
            ViewBag.searchItem = searchItem;

            ViewBag.listIQCItemEquip = listIQCItemEquip;
            ViewBag.listUnit = listUnit;

            

            return View("Master/Template/ListIQCTemplateGAS");
        }

        public ActionResult CreateIQCMasterTemplateGAS(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_IQC_TEMPLATE createQMS_MA_IQC_TEMPLATE = new CreateQMS_MA_IQC_TEMPLATE();
   
            IQCControlDataSearchModel searchItem = new IQCControlDataSearchModel();
 
           List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);// .getQMS_MA_UNITActiveList(_db);
     

            string actionName = Resources.Strings.AddNew;


            createQMS_MA_IQC_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_IQC_TEMPLATE = masterDataServices.getQMS_MA_IQC_TEMPLATEById(_db, id);
            }

  
            ViewBag.createQMS_MA_IQC_TEMPLATE = createQMS_MA_IQC_TEMPLATE;

            ViewBag.searchItem = searchItem;
 
            ViewBag.listUnit = listUnit;

            ViewBag.ActionName = actionName;
            return View("Master/Template/ListIQCTemplateGAS");
        }


        public ActionResult ListIQCTemplateGAS()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            IQCTemplateSearchModel searchIQCTemplate = new IQCTemplateSearchModel();
            CreateQMS_MA_IQC_TEMPLATE createQMS_MA_IQC_TEMPLATE = new CreateQMS_MA_IQC_TEMPLATE();
       
            IQCControlDataSearchModel searchItem = new IQCControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);// .getQMS_MA_UNITActiveList(_db);

            List<ViewQMS_MA_IQC_ITEMEQUIP> listIQCItemEquip = masterDataServices.getQMS_MA_IQC_ITEMEQUIPList(_db);

            searchIQCTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;
            createQMS_MA_IQC_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;

            ViewBag.searchIQCTemplate = searchIQCTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_IQC_TEMPLATE = createQMS_MA_IQC_TEMPLATE;
            ViewBag.searchItem = searchItem;

            ViewBag.listIQCItemEquip = listIQCItemEquip;
            ViewBag.listUnit = listUnit;
            ViewBag.listIQCFormula = masterDataServices.getQMS_MA_IQC_FORMULAList(_db);

            return View("Master/Template/ListIQCTemplateGAS");
        }

        public ActionResult CreateIQCTemplateGAS(long id = 0)
        {
            string actionName = Resources.Strings.AddNew;


            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
            }

            ViewBag.ActionName = actionName;
            return View("Master / Template / ListIQCTemplateGAS");
        }

   
        public JsonResult CreateQMS_MA_IQC_TEMPLATE(CreateQMS_MA_IQC_TEMPLATE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                        long nResult = masterDataServices.SaveQMS_MA_IQC_TEMPLATE(_db, model);

                        if (nResult > 0)
                        {
                            model = masterDataServices.getQMS_MA_IQC_TEMPLATEById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

     
        public JsonResult DeleteQMS_MA_IQC_TEMPLATE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_IQC_TEMPLATE_DATAByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_IQC_TEMPLATEBySearch(IQCTemplateSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_IQC_TEMPLATE> listData = new List<ViewQMS_MA_IQC_TEMPLATE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_IQC_TEMPLATE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex, error = masterDataServices.getError() };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }





        public JsonResult ExecuteFileTemplateToDB(List<FolderDataDetail> modelFolderDataDetail)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices configDataServices = new ConfigServices(_username);
                MasterDataService MasterDataService = new MasterDataService(_username);
                bool bResult = false;
                int nSuccess = 0;
                int nFailure = 0;
                string szFailName = "";
                string logText = "";
                try
                {
                    foreach (FolderDataDetail model in modelFolderDataDetail)
                    {
                        bResult = MasterDataService.executeFileTemplateIQCToDB(_db, model);
                        if (true == bResult)
                        {
                            nSuccess++;
                        }
                        else
                        {
                            szFailName = model.m_dtFile;
                            nFailure++;
                            break;
                        }
                    }

                    if (0 == nFailure)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = MasterDataService.getError() };
                        //result = new { result = ReturnStatus.ERROR, message = Resources.Strings.TotalSuccess + " " + nFailure + " " + Resources.Strings.ErrorFileName + " : " + szFailName + " " + configDataServices.getError() };
                    }

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                    logText += string.Format(ex.Message);
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

       
    }
}
