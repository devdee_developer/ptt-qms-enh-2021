﻿using System.Web;
using System.Web.Mvc;
using QMSsystem.Models.Privilege;

namespace QMSsystem
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LegacyAuthorize());
        }
    }
}