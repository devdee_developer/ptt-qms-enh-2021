﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QMSSystem.Model;
using QMSSystem.CoreDB.Helper.Grid;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Globalization;
using QMSSystem.CoreDB.Helper;

namespace QMSSystem.CoreDB.Services
{
    public class ReportServices : BaseService
    {
        public ReportServices(string userName) : base(userName) { }

        #region QMS_RP_OFF_CONTROL

        public List<ViewQMS_RP_OFF_CONTROL> searchQMS_RP_OFF_CONTROL(QMSDBEntities db, ReportOffControlSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_RP_OFF_CONTROL> objResult = new List<ViewQMS_RP_OFF_CONTROL>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "UPDATE_DATE";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_OFF_CONTROL> listData = new List<QMS_RP_OFF_CONTROL>();
                listData = QMS_RP_OFF_CONTROL.GetAllBySearch(db, searchModel.mSearch, searchModel.mSearch.OFF_CONTROL_TYPE);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_RP_OFF_CONTROL
                                 {
                                     ID = dt.ID,
                                     START_DATE = dt.START_DATE,
                                     END_DATE = dt.END_DATE,
                                     NAME = dt.NAME,
                                     OFF_CONTROL_TYPE = dt.OFF_CONTROL_TYPE,
                                     DOC_STATUS = dt.DOC_STATUS,
                                     REPORT_DOC_STATUS = reovleReportDocStatus(dt.DOC_STATUS),
                                     TITLE_DESC = dt.TITLE_DESC,
                                     DETAIL_DESC = dt.DETAIL_DESC,

                                     TITLE_GRAPH = dt.TITLE_GRAPH,
                                     TITLE_GRAPH_X = dt.TITLE_GRAPH_X,
                                     TITLE_GRAPH_Y = dt.TITLE_GRAPH_Y,

                                     SHOW_DASHBOARD = dt.SHOW_DASHBOARD,
                                     STATUS_RAW_DATA = dt.STATUS_RAW_DATA,

                                     SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                     SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),

                                     UPDATE_DATE = dt.UPDATE_DATE,
                                     CREATE_DATE = dt.CREATE_DATE,
                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER

                                 }).ToList();
                    objResult = grid.LoadGridData<ViewQMS_RP_OFF_CONTROL>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_RP_OFF_CONTROL getQMS_RP_OFF_CONTROLById(QMSDBEntities db, long id)
        {
            CreateQMS_RP_OFF_CONTROL objResult = new CreateQMS_RP_OFF_CONTROL();

            try
            {
                QMS_RP_OFF_CONTROL dt = new QMS_RP_OFF_CONTROL();
                dt = QMS_RP_OFF_CONTROL.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL> getQMS_RP_OFF_CONTROLList(QMSDBEntities db, byte controltype)
        {
            List<ViewQMS_RP_OFF_CONTROL> listResult = new List<ViewQMS_RP_OFF_CONTROL>();

            try
            {
                List<QMS_RP_OFF_CONTROL> list = QMS_RP_OFF_CONTROL.GetActive(db, controltype);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL
                                  {
                                      ID = dt.ID,
                                      START_DATE = dt.START_DATE,
                                      END_DATE = dt.END_DATE,
                                      NAME = dt.NAME,
                                      OFF_CONTROL_TYPE = dt.OFF_CONTROL_TYPE,
                                      DOC_STATUS = dt.DOC_STATUS,
                                      REPORT_DOC_STATUS = reovleReportDocStatus(dt.DOC_STATUS),
                                      TITLE_DESC = dt.TITLE_DESC,
                                      DETAIL_DESC = dt.DETAIL_DESC,

                                      TITLE_GRAPH = dt.TITLE_GRAPH,
                                      TITLE_GRAPH_X = dt.TITLE_GRAPH_X,
                                      TITLE_GRAPH_Y = dt.TITLE_GRAPH_Y,
                                      SHOW_DASHBOARD = dt.SHOW_DASHBOARD,
                                      STATUS_RAW_DATA = dt.STATUS_RAW_DATA

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_RP_OFF_CONTROL convertModelToDB(CreateQMS_RP_OFF_CONTROL model)
        {
            QMS_RP_OFF_CONTROL result = new QMS_RP_OFF_CONTROL();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.OFF_CONTROL_TYPE = model.OFF_CONTROL_TYPE;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.DOC_STATUS = model.DOC_STATUS;
            result.TITLE_DESC = (null == model.TITLE_DESC) ? "" : model.TITLE_DESC;
            result.DETAIL_DESC = (null == model.DETAIL_DESC) ? "" : model.DETAIL_DESC;

            result.TITLE_GRAPH = (null == model.TITLE_GRAPH) ? "" : model.TITLE_GRAPH;
            result.TITLE_GRAPH_X = (null == model.TITLE_GRAPH_X) ? "" : model.TITLE_GRAPH_X;
            result.TITLE_GRAPH_Y = (null == model.TITLE_GRAPH_Y) ? "" : model.TITLE_GRAPH_Y;

            result.AUTOGEN_FLAG = model.AUTOGEN_FLAG;
            result.SHOW_DASHBOARD = model.SHOW_DASHBOARD;
            result.STATUS_RAW_DATA = model.STATUS_RAW_DATA;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_RP_OFF_CONTROL convertDBToModel(QMS_RP_OFF_CONTROL model)
        {
            CreateQMS_RP_OFF_CONTROL result = new CreateQMS_RP_OFF_CONTROL();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.OFF_CONTROL_TYPE = model.OFF_CONTROL_TYPE;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.DOC_STATUS = model.DOC_STATUS;
            result.TITLE_DESC = (null == model.TITLE_DESC) ? "" : model.TITLE_DESC;
            result.DETAIL_DESC = (null == model.DETAIL_DESC) ? "" : model.DETAIL_DESC;

            result.TITLE_GRAPH = (null == model.TITLE_GRAPH) ? "" : model.TITLE_GRAPH;
            result.TITLE_GRAPH_X = (null == model.TITLE_GRAPH_X) ? "" : model.TITLE_GRAPH_X;
            result.TITLE_GRAPH_Y = (null == model.TITLE_GRAPH_Y) ? "" : model.TITLE_GRAPH_Y;

            result.SHOW_DASHBOARD = model.SHOW_DASHBOARD;
            result.STATUS_RAW_DATA = model.STATUS_RAW_DATA;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));

            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_RP_OFF_CONTROLById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_RP_OFF_CONTROL result = new QMS_RP_OFF_CONTROL();
            result = QMS_RP_OFF_CONTROL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_RP_OFF_CONTROLByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_RP_OFF_CONTROL> result = new List<QMS_RP_OFF_CONTROL>();
            result = QMS_RP_OFF_CONTROL.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m => {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_RP_OFF_CONTROL(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_RP_OFF_CONTROL(db, model);
            }
            else
            {
                result = AddQMS_RP_OFF_CONTROL(db, convertModelToDB(model));
                //create summary detail... 
                if (result > 0)
                {
                    List<CreateQMS_RP_OFF_CONTROL_SUMMARY> listSummaryT0 = new List<CreateQMS_RP_OFF_CONTROL_SUMMARY>();
                    List<CreateQMS_RP_OFF_CONTROL_SUMMARY> listSummaryT1 = new List<CreateQMS_RP_OFF_CONTROL_SUMMARY>();
                    List<ViewQMS_MA_PRODUCT> sortlistAllProduct = getProductHiAndLowList(db);
                    List<ViewQMS_MA_PLANT> listColumn = getQMS_MA_PLANTList(db);
                    CreateQMS_RP_OFF_CONTROL_SUMMARY tempT0 = null;
                    CreateQMS_RP_OFF_CONTROL_SUMMARY tempT1 = null;

                    if (sortlistAllProduct.Count() > 0 && listColumn.Count() > 0)
                    {
                        //Data Row ........ สร้าง row
                        foreach (ViewQMS_MA_PRODUCT dtRow in sortlistAllProduct)
                        {
                            foreach (ViewQMS_MA_PLANT dtColumn in listColumn) //Plant
                            {
                                // ค่า off control
                                tempT0 = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                tempT0.PLANT_ID = dtColumn.ID;
                                tempT0.PRODUCT_ID = dtRow.ID;
                                tempT0.RP_OFF_CONTROL_ID = result;
                                tempT0.VALUE = 0;
                                tempT0.DATA_TYPE = (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL_NO_DATA;
                                listSummaryT0.Add(tempT0);

                                //ค่า input 
                                tempT1 = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                tempT1.PLANT_ID = dtColumn.ID;
                                tempT1.PRODUCT_ID = dtRow.ID;
                                tempT1.RP_OFF_CONTROL_ID = result;
                                tempT1.VALUE = 0;
                                tempT1.DATA_TYPE = (byte)REPORT_SUMMARY_DATA_TYPE.INPUT_VOLUME_NO_DATA;
                                listSummaryT1.Add(tempT1);
                            }
                        }

                        SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listSummaryT0);
                        SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listSummaryT1);
                    }
                }
            }

            return result;
        }

        public long UpdateQMS_RP_OFF_CONTROL(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL model)
        {
            long result = 0;

            try
            {
                QMS_RP_OFF_CONTROL dt = new QMS_RP_OFF_CONTROL();
                dt = QMS_RP_OFF_CONTROL.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.OFF_CONTROL_TYPE = model.OFF_CONTROL_TYPE;
                dt.START_DATE = model.START_DATE;
                dt.END_DATE = model.END_DATE;
                dt.DOC_STATUS = model.DOC_STATUS;
                dt.TITLE_DESC = (null == model.TITLE_DESC) ? "" : model.TITLE_DESC;
                dt.DETAIL_DESC = (null == model.DETAIL_DESC) ? "" : model.DETAIL_DESC;

                dt.TITLE_GRAPH = (null == model.TITLE_GRAPH) ? "" : model.TITLE_GRAPH;
                dt.TITLE_GRAPH_X = (null == model.TITLE_GRAPH_X) ? "" : model.TITLE_GRAPH_X;
                dt.TITLE_GRAPH_Y = (null == model.TITLE_GRAPH_Y) ? "" : model.TITLE_GRAPH_Y;

                dt.SHOW_DASHBOARD = model.SHOW_DASHBOARD;
                dt.STATUS_RAW_DATA = model.STATUS_RAW_DATA;

                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_RP_OFF_CONTROL(QMSDBEntities _db, QMS_RP_OFF_CONTROL model)
        {
            long result = 0;
            try
            {
                QMS_RP_OFF_CONTROL dt = new QMS_RP_OFF_CONTROL();
                dt = QMS_RP_OFF_CONTROL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_RP_OFF_CONTROL.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_RP_OFF_CONTROL_DETAIL

        private List<ViewQMS_RP_OFF_CONTROL_DETAIL> convertListData(QMSDBEntities db, List<QMS_RP_OFF_CONTROL_DETAIL> listData)
        {
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> listResult = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            List<QMS_MA_ROOT_CAUSE> listRootCause = QMS_MA_ROOT_CAUSE.GetAll(db);
            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    listResult = (from dt in listData
                                  select new ViewQMS_RP_OFF_CONTROL_DETAIL
                                  {
                                      ID = dt.ID,
                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                      START_DATE = dt.START_DATE,
                                      END_DATE = dt.END_DATE,
                                      VOLUME = dt.VOLUME,
                                      CONTROL_VALUE = dt.CONTROL_VALUE,
                                      ROOT_CAUSE_ID = dt.ROOT_CAUSE_ID,
                                      ROOT_CAUSE_TYPE_NAME = getRootCauseName(listRootCause, dt.ROOT_CAUSE_ID),

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        private List<ViewQMS_RP_OFF_CONTROL_DETAIL> convertListData(QMSDBEntities db, List<ViewQMS_TR_OFF_CONTROL_CAL> listData, long rpId)
        {
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> listResult = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            List<QMS_MA_ROOT_CAUSE> listRootCause = QMS_MA_ROOT_CAUSE.GetAll(db);
            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    listResult = (from dt in listData
                                  select new ViewQMS_RP_OFF_CONTROL_DETAIL
                                  {
                                      ID = 0, //เป็น 0 เสมอ เพราะ เป็น รายการ ใหม่ dt.ID,
                                      RP_OFF_CONTROL_ID = rpId,
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                      START_DATE = dt.START_DATE,
                                      END_DATE = dt.END_DATE,
                                      VOLUME = dt.VOLUME,
                                      CONTROL_VALUE = dt.CONTROL_VALUE,
                                      ROOT_CAUSE_ID = dt.ROOT_CAUSE_ID,
                                      ROOT_CAUSE_TYPE_NAME = getRootCauseName(listRootCause, dt.ROOT_CAUSE_ID),

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_DETAIL> searchQMS_RP_OFF_CONTROL_DETAILEx(QMSDBEntities db, ReportOffControlDetailSearch searchModel, OffControlCalAutoSearchModel searchOffCalModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            TransactionService transService = new TransactionService(_currentUserName);
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> objResult0 = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> objResult1 = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> objResult = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_OFF_CONTROL_DETAIL> listData = new List<QMS_RP_OFF_CONTROL_DETAIL>();
                listData = QMS_RP_OFF_CONTROL_DETAIL.GetAllBySearch(db, searchModel.mSearch);

                List<ViewQMS_TR_OFF_CONTROL_CAL> listOffControl = transService.getQMS_TR_OFF_CONTROL_CALListByOffControlCalAutoSearch(db, searchOffCalModel);

                if (null != listOffControl && listOffControl.Count() > 0)
                {
                    objResult0 = convertListData(db, listOffControl, searchOffCalModel.RPOffControlId);
                }

                if (listData != null && listData.Count() > 0)
                {
                    objResult1 = convertListData(db, listData);
                }

                if (objResult1.Count() > 0)
                {
                    objResult.AddRange(objResult1);
                }

                if (objResult.Count() > 0 && objResult0.Count() > 0)
                {
                    foreach (ViewQMS_RP_OFF_CONTROL_DETAIL dt in objResult0)
                    {
                        ViewQMS_RP_OFF_CONTROL_DETAIL tempCheck = objResult.Where(m => m.START_DATE == dt.START_DATE && m.END_DATE == dt.END_DATE && m.PLANT_ID == dt.PLANT_ID && m.PRODUCT_ID == m.PRODUCT_ID).FirstOrDefault();
                        if (null == tempCheck) //add new 
                        {
                            objResult.Add(dt);
                        }
                    }
                }
                else if (objResult0.Count() > 0)
                {
                    objResult.AddRange(objResult0);
                }

                if (null != objResult && objResult.Count() > 0)
                {
                    objResult = grid.LoadGridData<ViewQMS_RP_OFF_CONTROL_DETAIL>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_DETAIL> searchDashQMS_RP_OFF_CONTROL_DETAILEx(QMSDBEntities db, ReportOffControlDetailSearch searchModel, DashOffControlCalAutoSearchModel searchOffCalModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            TransactionService transService = new TransactionService(_currentUserName);
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> objResult0 = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> objResult1 = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> objResult = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_OFF_CONTROL_DETAIL> listData = new List<QMS_RP_OFF_CONTROL_DETAIL>();
                listData = QMS_RP_OFF_CONTROL_DETAIL.GetAllBySearch(db, searchModel.mSearch);

                List<ViewQMS_TR_OFF_CONTROL_CAL> listOffControl = transService.getQMS_TR_OFF_CONTROL_CALListByDashOffControlCalAutoSearch(db, searchOffCalModel);

                //if (null != listOffControl && listOffControl.Count() > 0)
                //{
                //    objResult0 = convertListData(db, listOffControl);
                //}

                //if (listData != null && listData.Count() > 0)
                //{
                //    objResult1 = convertListData(db, listData);
                //}

                if (objResult1.Count() > 0)
                {
                    objResult.AddRange(objResult1);
                }

                if (objResult.Count() > 0 && objResult0.Count() > 0)
                {
                    foreach (ViewQMS_RP_OFF_CONTROL_DETAIL dt in objResult0)
                    {
                        ViewQMS_RP_OFF_CONTROL_DETAIL tempCheck = objResult.Where(m => m.START_DATE == dt.START_DATE && m.END_DATE == dt.END_DATE && m.PLANT_ID == dt.PLANT_ID && m.PRODUCT_ID == m.PRODUCT_ID).FirstOrDefault();
                        if (null == tempCheck) //add new 
                        {
                            objResult.Add(dt);
                        }
                    }
                }
                else if (objResult0.Count() > 0)
                {
                    objResult.AddRange(objResult0);
                }

                if (null != objResult && objResult.Count() > 0)
                {
                    objResult = grid.LoadGridData<ViewQMS_RP_OFF_CONTROL_DETAIL>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_DETAIL> searchQMS_RP_OFF_CONTROL_DETAIL(QMSDBEntities db, ReportOffControlDetailSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> objResult = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_OFF_CONTROL_DETAIL> listData = new List<QMS_RP_OFF_CONTROL_DETAIL>();
                listData = QMS_RP_OFF_CONTROL_DETAIL.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
                List<QMS_MA_ROOT_CAUSE> listRootCause = QMS_MA_ROOT_CAUSE.GetAll(db);

                if (listData != null)
                {
                    objResult = convertListData(db, listData);
                    objResult = grid.LoadGridData<ViewQMS_RP_OFF_CONTROL_DETAIL>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_RP_OFF_CONTROL_DETAIL getQMS_RP_OFF_CONTROL_DETAILById(QMSDBEntities db, long id)
        {
            CreateQMS_RP_OFF_CONTROL_DETAIL objResult = new CreateQMS_RP_OFF_CONTROL_DETAIL();

            try
            {
                QMS_RP_OFF_CONTROL_DETAIL dt = new QMS_RP_OFF_CONTROL_DETAIL();
                dt = QMS_RP_OFF_CONTROL_DETAIL.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_DETAIL> getQMS_RP_OFF_CONTROL_DETAILList(QMSDBEntities db)
        {
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> listResult = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            List<QMS_MA_ROOT_CAUSE> listRootCause = QMS_MA_ROOT_CAUSE.GetAll(db);

            try
            {
                List<QMS_RP_OFF_CONTROL_DETAIL> list = QMS_RP_OFF_CONTROL_DETAIL.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL_DETAIL
                                  {
                                      ID = dt.ID,
                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                      START_DATE = dt.START_DATE,
                                      END_DATE = dt.END_DATE,
                                      VOLUME = dt.VOLUME,
                                      CONTROL_VALUE = dt.CONTROL_VALUE,
                                      ROOT_CAUSE_ID = dt.ROOT_CAUSE_ID,
                                      ROOT_CAUSE_TYPE_NAME = getRootCauseName(listRootCause, dt.ROOT_CAUSE_ID),

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_RP_OFF_CONTROL_DETAIL convertModelToDB(CreateQMS_RP_OFF_CONTROL_DETAIL model)
        {
            QMS_RP_OFF_CONTROL_DETAIL result = new QMS_RP_OFF_CONTROL_DETAIL();

            result.ID = model.ID;

            result.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.VOLUME = model.VOLUME;
            result.CONTROL_VALUE = (null == model.CONTROL_VALUE) ? "" : model.CONTROL_VALUE;
            result.ROOT_CAUSE_ID = model.ROOT_CAUSE_ID;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_RP_OFF_CONTROL_DETAIL convertDBToModel(QMS_RP_OFF_CONTROL_DETAIL model)
        {
            CreateQMS_RP_OFF_CONTROL_DETAIL result = new CreateQMS_RP_OFF_CONTROL_DETAIL();

            result.ID = model.ID;

            result.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.VOLUME = model.VOLUME;
            result.CONTROL_VALUE = (null == model.CONTROL_VALUE) ? "" : model.CONTROL_VALUE;
            result.ROOT_CAUSE_ID = model.ROOT_CAUSE_ID;

            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_DETAILById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_RP_OFF_CONTROL_DETAIL result = new QMS_RP_OFF_CONTROL_DETAIL();
            result = QMS_RP_OFF_CONTROL_DETAIL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_DETAILByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_RP_OFF_CONTROL_DETAIL> result = new List<QMS_RP_OFF_CONTROL_DETAIL>();
            result = QMS_RP_OFF_CONTROL_DETAIL.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m => {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_RP_OFF_CONTROL_DETAIL(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_DETAIL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_RP_OFF_CONTROL_DETAIL(db, model);
            }
            else
            {
                result = AddQMS_RP_OFF_CONTROL_DETAIL(db, convertModelToDB(model));
            }

            return result;
        }

        public long SaveQMS_RP_OFF_CONTROL_DETAILEx(QMSDBEntities db, List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDetail)
        {
            long result = 0;

            try
            {
                CreateQMS_RP_OFF_CONTROL_DETAIL model = new CreateQMS_RP_OFF_CONTROL_DETAIL();
                foreach (ViewQMS_RP_OFF_CONTROL_DETAIL dt in listDetail)
                {
                    model = new CreateQMS_RP_OFF_CONTROL_DETAIL();

                    model.ID = dt.ID;

                    model.RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID;
                    model.PLANT_ID = dt.PLANT_ID;
                    model.PRODUCT_ID = dt.PRODUCT_ID;
                    model.START_DATE = dt.START_DATE;
                    model.END_DATE = dt.END_DATE;
                    model.VOLUME = dt.VOLUME;
                    model.CONTROL_VALUE = (null == dt.CONTROL_VALUE) ? "" : dt.CONTROL_VALUE;
                    model.ROOT_CAUSE_ID = dt.ROOT_CAUSE_ID;

                    SaveQMS_RP_OFF_CONTROL_DETAIL(db, model);
                }

                result = 1;
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public long UpdateQMS_RP_OFF_CONTROL_DETAIL(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_DETAIL model)
        {
            long result = 0;

            try
            {
                QMS_RP_OFF_CONTROL_DETAIL dt = new QMS_RP_OFF_CONTROL_DETAIL();
                dt = QMS_RP_OFF_CONTROL_DETAIL.GetById(db, model.ID);

                dt.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
                dt.PLANT_ID = model.PLANT_ID;
                dt.PRODUCT_ID = model.PRODUCT_ID;
                dt.START_DATE = model.START_DATE;
                dt.END_DATE = model.END_DATE;
                dt.VOLUME = model.VOLUME;
                dt.CONTROL_VALUE = (null == model.CONTROL_VALUE) ? "" : model.CONTROL_VALUE;
                dt.ROOT_CAUSE_ID = model.ROOT_CAUSE_ID;

                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_RP_OFF_CONTROL_DETAIL(QMSDBEntities _db, QMS_RP_OFF_CONTROL_DETAIL model)
        {
            long result = 0;
            try
            {
                QMS_RP_OFF_CONTROL dt = new QMS_RP_OFF_CONTROL();
                dt = QMS_RP_OFF_CONTROL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_RP_OFF_CONTROL_DETAIL.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion

        #region QMS_RP_OFF_CONTROL_GRAPH

        public List<ViewQMS_RP_OFF_CONTROL_GRAPH> searchQMS_RP_OFF_CONTROL_GRAPH(QMSDBEntities db, ReportOffControlGraphSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_RP_OFF_CONTROL_GRAPH> objResult = new List<ViewQMS_RP_OFF_CONTROL_GRAPH>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PREVIOUS";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_OFF_CONTROL_GRAPH> listData = new List<QMS_RP_OFF_CONTROL_GRAPH>();
                listData = QMS_RP_OFF_CONTROL_GRAPH.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_RP_OFF_CONTROL_GRAPH
                                 {
                                     ID = dt.ID,
                                     RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                     PREVIOUS = dt.PREVIOUS,
                                     OFF_CONTROL = dt.OFF_CONTROL,
                                     GRAPH_DESC = dt.GRAPH_DESC,
                                     POSITION = dt.POSITION,

                                     ARROW_DOWN = true,
                                     ARROW_UP = true

                                 }).ToList();
                    objResult = grid.LoadGridData<ViewQMS_RP_OFF_CONTROL_GRAPH>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    objResult = objResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                    objResult = setArrowUPAndDown(objResult);

                    listPageIndex = getPageIndexList(totalPage);
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_RP_OFF_CONTROL_GRAPH getQMS_RP_OFF_CONTROL_GRAPHById(QMSDBEntities db, long id)
        {
            CreateQMS_RP_OFF_CONTROL_GRAPH objResult = new CreateQMS_RP_OFF_CONTROL_GRAPH();

            try
            {
                QMS_RP_OFF_CONTROL_GRAPH dt = new QMS_RP_OFF_CONTROL_GRAPH();
                dt = QMS_RP_OFF_CONTROL_GRAPH.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private QMS_RP_OFF_CONTROL_GRAPH convertModelToDB(CreateQMS_RP_OFF_CONTROL_GRAPH model)
        {
            QMS_RP_OFF_CONTROL_GRAPH result = new QMS_RP_OFF_CONTROL_GRAPH();

            result.ID = model.ID;

            result.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
            result.PREVIOUS = (null == model.PREVIOUS) ? "" : model.PREVIOUS;
            result.OFF_CONTROL = model.OFF_CONTROL;
            result.POSITION = model.POSITION;
            result.GRAPH_DESC = (null == model.GRAPH_DESC) ? "" : model.GRAPH_DESC;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_RP_OFF_CONTROL_GRAPH convertDBToModel(QMS_RP_OFF_CONTROL_GRAPH model)
        {
            CreateQMS_RP_OFF_CONTROL_GRAPH result = new CreateQMS_RP_OFF_CONTROL_GRAPH();

            result.ID = model.ID;

            result.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
            result.PREVIOUS = (null == model.PREVIOUS) ? "" : model.PREVIOUS;
            result.OFF_CONTROL = model.OFF_CONTROL;
            result.POSITION = model.POSITION;
            result.GRAPH_DESC = (null == model.GRAPH_DESC) ? "" : model.GRAPH_DESC;

            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_GRAPHById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_RP_OFF_CONTROL_GRAPH result = new QMS_RP_OFF_CONTROL_GRAPH();
            result = QMS_RP_OFF_CONTROL_GRAPH.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_GRAPHByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_RP_OFF_CONTROL_GRAPH> result = new List<QMS_RP_OFF_CONTROL_GRAPH>();
            result = QMS_RP_OFF_CONTROL_GRAPH.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m => {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_RP_OFF_CONTROL_GRAPH(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_GRAPH model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_RP_OFF_CONTROL_GRAPH(db, model);
            }
            else
            {
                result = AddQMS_RP_OFF_CONTROL_GRAPH(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_RP_OFF_CONTROL_GRAPH(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_GRAPH model)
        {
            long result = 0;

            try
            {
                QMS_RP_OFF_CONTROL_GRAPH dt = new QMS_RP_OFF_CONTROL_GRAPH();
                dt = QMS_RP_OFF_CONTROL_GRAPH.GetById(db, model.ID);

                dt.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
                dt.PREVIOUS = (null == model.PREVIOUS) ? "" : model.PREVIOUS;
                dt.OFF_CONTROL = model.OFF_CONTROL;
                dt.POSITION = model.POSITION;
                dt.GRAPH_DESC = (null == model.GRAPH_DESC) ? "" : model.GRAPH_DESC;

                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        private short getMaxControlGraphPostion(QMSDBEntities db, long RP_OFF_CONTROL_ID)
        {
            short bResult = 1;

            try
            {
                var list = QMS_RP_OFF_CONTROL_GRAPH.GetActiveAllByRP_OFF_CONTROL_ID(db, RP_OFF_CONTROL_ID);

                if (list.Count() > 0)
                {
                    short max = list.Max(m => m.POSITION);

                    if (max < 32767)
                        bResult += max;
                    else
                        bResult = 1;
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long AddQMS_RP_OFF_CONTROL_GRAPH(QMSDBEntities _db, QMS_RP_OFF_CONTROL_GRAPH model)
        {
            long result = 0;
            try
            {
                QMS_RP_OFF_CONTROL_GRAPH dt = new QMS_RP_OFF_CONTROL_GRAPH();
                dt = QMS_RP_OFF_CONTROL_GRAPH.GetById(_db, model.ID);

                if (null == dt)
                {
                    model.POSITION = this.getMaxControlGraphPostion(_db, model.RP_OFF_CONTROL_ID); //model.POSITION; //หาค่าสูงสุดสร้างใหม่จะไปต่อท้าย
                                                                                                   //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_RP_OFF_CONTROL_GRAPH.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_RP_OFF_CONTROL_GRAPH> getQMS_RP_OFF_CONTROL_GRAPHList(QMSDBEntities db)
        {
            List<ViewQMS_RP_OFF_CONTROL_GRAPH> listResult = new List<ViewQMS_RP_OFF_CONTROL_GRAPH>();

            try
            {
                List<QMS_RP_OFF_CONTROL_GRAPH> list = QMS_RP_OFF_CONTROL_GRAPH.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL_GRAPH
                                  {
                                      ID = dt.ID,
                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      PREVIOUS = dt.PREVIOUS,
                                      OFF_CONTROL = dt.OFF_CONTROL,
                                      GRAPH_DESC = dt.GRAPH_DESC,
                                      POSITION = dt.POSITION

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        //public List<ViewQMS_RP_OFF_CONTROL_GRAPH> setQMS_RP_OFF_CONTROL_GRAPHPosition(QMSDBEntities db, SetOffControlGraphPosition model)
        public int setQMS_RP_OFF_CONTROL_GRAPHPosition(QMSDBEntities db, SetOffControlGraphPosition model)
        {
            try
            {
                List<QMS_RP_OFF_CONTROL_GRAPH> list = QMS_RP_OFF_CONTROL_GRAPH.GetActiveAllByRP_OFF_CONTROL_ID(db, model.RP_OFF_CONTROL_ID);

                QMS_RP_OFF_CONTROL_GRAPH LastId = new QMS_RP_OFF_CONTROL_GRAPH();
                QMS_RP_OFF_CONTROL_GRAPH CurrentId = new QMS_RP_OFF_CONTROL_GRAPH();
                QMS_RP_OFF_CONTROL_GRAPH NextId = new QMS_RP_OFF_CONTROL_GRAPH();

                CreateQMS_RP_OFF_CONTROL_GRAPH currentData = new CreateQMS_RP_OFF_CONTROL_GRAPH();
                CreateQMS_RP_OFF_CONTROL_GRAPH swapData = new CreateQMS_RP_OFF_CONTROL_GRAPH();

                bool ExitLoop = false;

                list = list.OrderBy(m => m.POSITION).ToList();

                foreach (QMS_RP_OFF_CONTROL_GRAPH dt in list)
                {
                    NextId = dt; // get ค่า ตัวต่อไป

                    if (true == ExitLoop)
                    {
                        break;
                    }

                    if (model.ID == dt.ID) // get ค่า ปัจจุบัน
                    {
                        CurrentId = dt;
                        ExitLoop = true;
                    }
                    else
                    {
                        LastId = dt; // get ค่า สุดท้าย
                    }
                }

                if (model.DIRECTION == (int)ARROW_STATUS.ARROW_UP)
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;

                    currentData.RP_OFF_CONTROL_ID = CurrentId.RP_OFF_CONTROL_ID;
                    currentData.PREVIOUS = CurrentId.PREVIOUS;
                    currentData.OFF_CONTROL = CurrentId.OFF_CONTROL;
                    currentData.GRAPH_DESC = CurrentId.GRAPH_DESC;

                    currentData.POSITION = LastId.POSITION; //สลับตำแหน่งกับค่า บน

                    swapData.ID = LastId.ID;

                    swapData.RP_OFF_CONTROL_ID = LastId.RP_OFF_CONTROL_ID;
                    swapData.PREVIOUS = LastId.PREVIOUS;
                    swapData.OFF_CONTROL = LastId.OFF_CONTROL;
                    swapData.GRAPH_DESC = LastId.GRAPH_DESC;

                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                }
                else
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;

                    currentData.RP_OFF_CONTROL_ID = CurrentId.RP_OFF_CONTROL_ID;
                    currentData.PREVIOUS = CurrentId.PREVIOUS;
                    currentData.OFF_CONTROL = CurrentId.OFF_CONTROL;
                    currentData.GRAPH_DESC = CurrentId.GRAPH_DESC;

                    currentData.POSITION = NextId.POSITION; //สลับตำแหน่งกับค่า ล่าง

                    swapData.ID = NextId.ID;

                    swapData.RP_OFF_CONTROL_ID = NextId.RP_OFF_CONTROL_ID;
                    swapData.PREVIOUS = NextId.PREVIOUS;
                    swapData.OFF_CONTROL = NextId.OFF_CONTROL;
                    swapData.GRAPH_DESC = NextId.GRAPH_DESC;

                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                }

                UpdateQMS_RP_OFF_CONTROL_GRAPH(db, currentData);
                UpdateQMS_RP_OFF_CONTROL_GRAPH(db, swapData);

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return 0;
        }

        public List<ViewQMS_RP_OFF_CONTROL_GRAPH> getQMS_RP_OFF_CONTROL_GRAPHActiveList(QMSDBEntities db)
        {
            List<ViewQMS_RP_OFF_CONTROL_GRAPH> listResult = new List<ViewQMS_RP_OFF_CONTROL_GRAPH>();

            try
            {
                List<QMS_RP_OFF_CONTROL_GRAPH> list = QMS_RP_OFF_CONTROL_GRAPH.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL_GRAPH
                                  {
                                      ID = dt.ID,
                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      PREVIOUS = dt.PREVIOUS,
                                      OFF_CONTROL = dt.OFF_CONTROL,
                                      GRAPH_DESC = dt.GRAPH_DESC,
                                      POSITION = dt.POSITION,
                                      ARROW_UP = true,
                                      ARROW_DOWN = true
                                  }).ToList();

                    listResult = listResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                    listResult = setArrowUPAndDown(listResult);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        #endregion

        #region QMS_RP_OFF_CONTROL_ATTACH

        public List<ViewQMS_RP_OFF_CONTROL_ATTACH> searchQMS_RP_OFF_CONTROL_ATTACH(QMSDBEntities db, ReportOffControlAttachSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_RP_OFF_CONTROL_ATTACH> objResult = new List<ViewQMS_RP_OFF_CONTROL_ATTACH>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "NAME";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_OFF_CONTROL_ATTACH> listData = new List<QMS_RP_OFF_CONTROL_ATTACH>();
                listData = QMS_RP_OFF_CONTROL_ATTACH.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_RP_OFF_CONTROL_ATTACH
                                 {
                                     ID = dt.ID,
                                     RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                     NAME = dt.NAME,
                                     PATH = dt.PATH,
                                     //POSITION = dt.POSITION,

                                     ARROW_DOWN = true,
                                     ARROW_UP = true

                                 }).ToList();
                    objResult = grid.LoadGridData<ViewQMS_RP_OFF_CONTROL_ATTACH>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    objResult = objResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                                                                             //objResult = setArrowUPAndDown(objResult);

                    listPageIndex = getPageIndexList(totalPage);
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_RP_OFF_CONTROL_ATTACH getQMS_RP_OFF_CONTROL_ATTACHById(QMSDBEntities db, long id)
        {
            CreateQMS_RP_OFF_CONTROL_ATTACH objResult = new CreateQMS_RP_OFF_CONTROL_ATTACH();

            try
            {
                QMS_RP_OFF_CONTROL_ATTACH dt = new QMS_RP_OFF_CONTROL_ATTACH();
                dt = QMS_RP_OFF_CONTROL_ATTACH.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private QMS_RP_OFF_CONTROL_ATTACH convertModelToDB(CreateQMS_RP_OFF_CONTROL_ATTACH model)
        {
            QMS_RP_OFF_CONTROL_ATTACH result = new QMS_RP_OFF_CONTROL_ATTACH();

            result.ID = model.ID;

            result.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            //result.POSITION = model.POSITION;
            result.PATH = (null == model.PATH) ? "" : model.PATH;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_RP_OFF_CONTROL_ATTACH convertDBToModel(QMS_RP_OFF_CONTROL_ATTACH model)
        {
            CreateQMS_RP_OFF_CONTROL_ATTACH result = new CreateQMS_RP_OFF_CONTROL_ATTACH();

            result.ID = model.ID;

            result.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            //result.POSITION = model.POSITION;
            result.PATH = (null == model.PATH) ? "" : model.PATH;

            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_ATTACHById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_RP_OFF_CONTROL_ATTACH result = new QMS_RP_OFF_CONTROL_ATTACH();
            result = QMS_RP_OFF_CONTROL_ATTACH.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_ATTACHByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_RP_OFF_CONTROL_ATTACH> result = new List<QMS_RP_OFF_CONTROL_ATTACH>();
            result = QMS_RP_OFF_CONTROL_ATTACH.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m => {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_RP_OFF_CONTROL_ATTACH(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_ATTACH model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_RP_OFF_CONTROL_ATTACH(db, model);
            }
            else
            {
                result = AddQMS_RP_OFF_CONTROL_ATTACH(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_RP_OFF_CONTROL_ATTACH(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_ATTACH model)
        {
            long result = 0;

            try
            {
                QMS_RP_OFF_CONTROL_ATTACH dt = new QMS_RP_OFF_CONTROL_ATTACH();
                dt = QMS_RP_OFF_CONTROL_ATTACH.GetById(db, model.ID);

                dt.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
                dt.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                //dt.POSITION = model.POSITION;
                dt.PATH = (null == model.PATH) ? "" : model.PATH;

                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_RP_OFF_CONTROL_ATTACH(QMSDBEntities _db, QMS_RP_OFF_CONTROL_ATTACH model)
        {
            long result = 0;
            try
            {
                QMS_RP_OFF_CONTROL_ATTACH dt = new QMS_RP_OFF_CONTROL_ATTACH();
                dt = QMS_RP_OFF_CONTROL_ATTACH.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_RP_OFF_CONTROL_ATTACH.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_RP_OFF_CONTROL_ATTACH> getQMS_RP_OFF_CONTROL_ATTACHList(QMSDBEntities db)
        {
            List<ViewQMS_RP_OFF_CONTROL_ATTACH> listResult = new List<ViewQMS_RP_OFF_CONTROL_ATTACH>();

            try
            {
                List<QMS_RP_OFF_CONTROL_ATTACH> list = QMS_RP_OFF_CONTROL_ATTACH.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL_ATTACH
                                  {
                                      ID = dt.ID,
                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      NAME = dt.NAME,
                                      PATH = dt.PATH,
                                      //POSITION = dt.POSITION

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_ATTACH> getQMS_RP_OFF_CONTROL_ATTACHActiveList(QMSDBEntities db)
        {
            List<ViewQMS_RP_OFF_CONTROL_ATTACH> listResult = new List<ViewQMS_RP_OFF_CONTROL_ATTACH>();

            try
            {
                List<QMS_RP_OFF_CONTROL_ATTACH> list = QMS_RP_OFF_CONTROL_ATTACH.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL_ATTACH
                                  {
                                      ID = dt.ID,
                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      NAME = dt.NAME,
                                      PATH = dt.PATH,
                                      //POSITION = dt.POSITION,
                                      ARROW_UP = true,
                                      ARROW_DOWN = true
                                  }).ToList();

                    listResult = listResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                                                                               //listResult = setArrowUPAndDown(listResult);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        #endregion

        #region QMS_RP_OFF_CONTROL_SUMMARY

        public List<ViewQMS_MA_PRODUCT> getQMS_MA_PRODUCTList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PRODUCT> listResult = new List<ViewQMS_MA_PRODUCT>();

            try
            {
                List<QMS_MA_PRODUCT> list = QMS_MA_PRODUCT.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PRODUCT
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      POSITION = dt.POSITION,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_PLANT> getQMS_MA_PLANTList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PLANT> listResult = new List<ViewQMS_MA_PLANT>();

            try
            {
                List<QMS_MA_PLANT> list = QMS_MA_PLANT.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PLANT
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      ABBREVIATION = dt.ABBREVIATION,
                                      POSITION = dt.POSITION,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_KPI> getQMS_MA_KPIActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_KPI> listResult = new List<ViewQMS_MA_KPI>();

            try
            {
                List<QMS_MA_KPI> list = QMS_MA_KPI.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_KPI
                                  {
                                      ID = dt.ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      START_VALUE_5 = dt.START_VALUE_5,
                                      END_VALUE_5 = dt.END_VALUE_5,
                                      START_VALUE_4 = dt.START_VALUE_4,
                                      END_VALUE_4 = dt.END_VALUE_4,
                                      START_VALUE_3 = dt.START_VALUE_3,
                                      END_VALUE_3 = dt.END_VALUE_3,
                                      START_VALUE_2 = dt.START_VALUE_2,
                                      END_VALUE_2 = dt.END_VALUE_2,
                                      START_VALUE_1 = dt.START_VALUE_1,
                                      END_VALUE_1 = dt.END_VALUE_1,
                                      ACTIVE_DATE = dt.ACTIVE_DATE,
                                      KPI_DESC = dt.KPI_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_PRODUCT> getProductHiAndLowList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PRODUCT> listAllProduct = new List<ViewQMS_MA_PRODUCT>();
            try
            {
                List<ViewQMS_MA_PRODUCT> listRow = getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PRODUCT_MAPPING> listFlagLOW = getQMS_MA_PRODUCT_MAPPING_LOW(db);
                foreach (ViewQMS_MA_PRODUCT dtRowLOW in listRow)
                {
                    ViewQMS_MA_PRODUCT tempLow = new ViewQMS_MA_PRODUCT();
                    ViewQMS_MA_PRODUCT_MAPPING dataItem = listFlagLOW.Where(m => m.PRODUCT_ID == dtRowLOW.ID).FirstOrDefault();
                    if (null != dataItem && dtRowLOW.ID > 0)
                    {
                        tempLow.ID = dtRowLOW.ID;
                        tempLow.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.HighControl;
                        tempLow.POSITION = dtRowLOW.POSITION;
                        listAllProduct.Add(tempLow);
                        ViewQMS_MA_PRODUCT tempLow1 = new ViewQMS_MA_PRODUCT();
                        tempLow1.ID = dtRowLOW.ID + 90000;
                        tempLow1.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.LowControl;
                        tempLow1.POSITION = dtRowLOW.POSITION;
                        listAllProduct.Add(tempLow1);
                    }
                    else
                    {
                        listAllProduct.Add(dtRowLOW);
                    }
                }

                listAllProduct = listAllProduct.OrderBy(x => x.POSITION).ThenBy(x => x.NAME).ToList();
            }
            catch (Exception ex)
            {

            }

            return listAllProduct;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getColumnOffControlSummary(List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

            try
            {
                long currentProduct = 0;
                if (listOffControlVolume.Count() > 0)
                {
                    //Default 
                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    listResult.Add(tempData);

                    for (int i = 0; i < listOffControlVolume.Count(); i++)
                    {
                        if (0 == currentProduct)
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                        }
                        else if (currentProduct != listOffControlVolume[i].PRODUCT_ID) //ตรวจสอบดูว่า ชื่อ ซ้ำไหม อยู่ใน column เดียวกัน
                        {
                            break; //exit ; already get all column
                        }
                        else
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                        }

                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                        tempData.SHOW_OUTPUT = listOffControlVolume[i].PLANT_NAME;
                        listResult.Add(tempData);
                    }

                    //Default  
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listResult.Add(tempData);

                }
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getRowsOffControlSummary(List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

            try
            {
                long currentProduct = 0;
                if (listOffControlVolume.Count() > 0)
                {
                    for (int i = 0; i < listOffControlVolume.Count(); i++)
                    {
                        if (0 == currentProduct)
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;

                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempData.SHOW_OUTPUT = listOffControlVolume[i].PRODUCT_NAME;
                            listResult.Add(tempData);
                        }
                        else if (currentProduct != listOffControlVolume[i].PRODUCT_ID) //ตรวจสอบดูว่า ชื่อ ซ้ำไหม อยู่ใน column เดียวกัน
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempData.SHOW_OUTPUT = listOffControlVolume[i].PRODUCT_NAME;
                            listResult.Add(tempData);
                        }
                        else
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID; //next;
                        }
                    }

                    //Default  
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listResult.Add(tempData);
                }
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public ViewMatrixSummary getTotalSummary(List<ViewMatrixSummary> listData, int nCol, int nRow, int mode)
        {
            ViewMatrixSummary totalValue = new ViewMatrixSummary();

            try
            {
                if (mode == (int)REPORT_SUMMARY_MODE.SUM_COL)
                {
                    totalValue.inputVol = listData.Where(m => m.nCols == nCol).Sum(m => m.inputVol);
                    totalValue.offControl = listData.Where(m => m.nCols == nCol).Sum(m => m.offControl);

                    totalValue.percentage = (totalValue.offControl / totalValue.inputVol) *100;
                    //totalValue.percentage = listData.Where(m => m.nCols == nCol).Sum(m => m.percentage);
                }
                else
                {
                    totalValue.inputVol = listData.Where(m => m.nRows == nRow).Sum(m => m.inputVol);
                    totalValue.offControl = listData.Where(m => m.nRows == nRow).Sum(m => m.offControl);
                    totalValue.percentage = (totalValue.offControl / totalValue.inputVol) * 100;
                }
            }
            catch (Exception ex)
            {

            }

            return totalValue;

        }

        public ViewMatrixSummary getTotalSummary(List<ViewMatrixSummary> listData)
        {
            ViewMatrixSummary totalValue = new ViewMatrixSummary();

            try
            {
                totalValue.inputVol = listData.Sum(m => m.inputVol);
                totalValue.offControl = listData.Sum(m => m.offControl);
                totalValue.percentage = (totalValue.offControl / totalValue.inputVol) * 100;
            }
            catch (Exception ex)
            {

            }

            return totalValue;

        }

        public ViewALL_QMS_RP_OFF_CONTROL_SUMMARY getTemplateOffControlSummaryWithAutoCal(QMSDBEntities db, OffControlCalAutoSearchModel searchModel)
        {
            ViewALL_QMS_RP_OFF_CONTROL_SUMMARY matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
            TransactionService transServices = new TransactionService(_currentUserName);
            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();
            ViewMatrixSummary tempSummary;

            try
            {
                //matrixResult = getTemplateOffControlSummary(db, searchModel.RPOffControlId );
                matrixResult = getTemplateOffControlSummaryEx(db, searchModel.RPOffControlId);
                OffControlAutoCalResult autoCalResult = new OffControlAutoCalResult();

                if (null != matrixResult.matrixOffControl && matrixResult.matrixOffControl.Count() > 0)
                {
                    for (int i = 0; i < matrixResult.matrixOffControl.Count(); i++) //ถึงค่าในแต่ละ Rows
                    {

                        if (matrixResult.matrixOffControl[i].Count() == matrixResult.matrixInputVolume[i].Count()) //ตรวจสอบ เพื่อความถูกต้อง
                        {
                            for (int point = 0; point < matrixResult.matrixInputVolume[i].Count(); point++) //ดึงค่าในแต่ละจุด
                            {
                                if (matrixResult.matrixInputVolume[i][point].PLANT_ID > 0 && matrixResult.matrixInputVolume[i][point].PRODUCT_ID > 0) //ตรวจสอบว่าเป็นดาต้าหรือว่าเป็น summary
                                {
                                    searchModel.PLANT_ID = matrixResult.matrixInputVolume[i][point].PLANT_ID;
                                    searchModel.PRODUCT_ID = matrixResult.matrixInputVolume[i][point].PRODUCT_ID;
                                    autoCalResult = transServices.GetOffControlByOffControlCalAutoSearchEx(db, searchModel);

                                    if (null != autoCalResult) // assign value ...
                                    {
                                        matrixResult.matrixInputVolume[i][point].VALUE = autoCalResult.VOLUME_INPUT;
                                        matrixResult.matrixInputVolume[i][point].SHOW_OUTPUT = matrixResult.matrixInputVolume[i][point].VALUE.ToString("#,##0.#0");

                                        matrixResult.matrixOffControl[i][point].VALUE = autoCalResult.VOULUME_OFFCONTROL;
                                        matrixResult.matrixOffControl[i][point].SHOW_OUTPUT = matrixResult.matrixOffControl[i][point].VALUE.ToString("#,##0.#0");

                                        if (matrixResult.matrixInputVolume[i][point].VALUE > 0)
                                        {
                                            matrixResult.matrixPercentage[i][point].VALUE = (autoCalResult.VOULUME_OFFCONTROL / autoCalResult.VOLUME_INPUT) * 100;
                                            matrixResult.matrixPercentage[i][point].SHOW_OUTPUT = matrixResult.matrixPercentage[i][point].VALUE.ToString("#,##0.#0");
                                        }
                                        else
                                        {
                                            matrixResult.matrixPercentage[i][point].VALUE = 0;
                                            matrixResult.matrixPercentage[i][point].SHOW_OUTPUT = matrixResult.matrixPercentage[i][point].VALUE.ToString("#,##0.#0");
                                        }
                                    }
                                    else //reset data to 0
                                    {
                                        matrixResult.matrixInputVolume[i][point].VALUE = 0;
                                        matrixResult.matrixInputVolume[i][point].SHOW_OUTPUT = matrixResult.matrixInputVolume[i][point].VALUE.ToString("#,##0.#0");

                                        matrixResult.matrixOffControl[i][point].VALUE = 0;
                                        matrixResult.matrixOffControl[i][point].SHOW_OUTPUT = matrixResult.matrixOffControl[i][point].VALUE.ToString("#,##0.#0");

                                        matrixResult.matrixPercentage[i][point].VALUE = 0;
                                        matrixResult.matrixPercentage[i][point].SHOW_OUTPUT = matrixResult.matrixPercentage[i][point].VALUE.ToString("#,##0.#0");
                                    }

                                    //addsummary
                                    tempSummary = new ViewMatrixSummary();
                                    tempSummary.nCols = point;
                                    tempSummary.nRows = i;
                                    tempSummary.offControl = matrixResult.matrixOffControl[i][point].VALUE;
                                    tempSummary.inputVol = matrixResult.matrixInputVolume[i][point].VALUE;
                                    tempSummary.percentage = matrixResult.matrixPercentage[i][point].VALUE;
                                    listSummary.Add(tempSummary);
                                }
                                else if (matrixResult.matrixInputVolume[i][point].ITEM_TYPE == (byte)ITEM_TYPE.SUMCOL_HEADER ||
                                matrixResult.matrixInputVolume[i][point].ITEM_TYPE == (byte)ITEM_TYPE.SUMROW_HEADER ||
                                matrixResult.matrixInputVolume[i][point].ITEM_TYPE == (byte)ITEM_TYPE.SUMALL
                              ) //Sum Row, Sum Col
                                {
                                    try
                                    {
                                        if (matrixResult.matrixInputVolume[i][point].ITEM_TYPE == (byte)ITEM_TYPE.SUMROW_HEADER)
                                        {
                                            tempSummary = getTotalSummary(listSummary, point, i, (int)REPORT_SUMMARY_MODE.SUM_ROW);
                                        }
                                        else if (matrixResult.matrixInputVolume[i][point].ITEM_TYPE == (byte)ITEM_TYPE.SUMCOL_HEADER)
                                        {
                                            tempSummary = getTotalSummary(listSummary, point, i, (int)REPORT_SUMMARY_MODE.SUM_COL);
                                        }
                                        else //SUM ALL
                                        {
                                            tempSummary = getTotalSummary(listSummary);
                                        }

                                        matrixResult.matrixInputVolume[i][point].VALUE = tempSummary.inputVol;
                                        matrixResult.matrixInputVolume[i][point].SHOW_OUTPUT = matrixResult.matrixInputVolume[i][point].VALUE.ToString("#,##0.#0");

                                        matrixResult.matrixOffControl[i][point].VALUE = tempSummary.offControl;
                                        matrixResult.matrixOffControl[i][point].SHOW_OUTPUT = matrixResult.matrixOffControl[i][point].VALUE.ToString("#,##0.#0");

                                        matrixResult.matrixPercentage[i][point].VALUE = tempSummary.percentage;
                                        matrixResult.matrixPercentage[i][point].SHOW_OUTPUT = matrixResult.matrixPercentage[i][point].VALUE.ToString("#,##0.#0");
                                    }
                                    catch
                                    {

                                        matrixResult.matrixInputVolume[i][point].VALUE = 0;
                                        matrixResult.matrixInputVolume[i][point].SHOW_OUTPUT = matrixResult.matrixInputVolume[i][point].VALUE.ToString("#,##0.#0");

                                        matrixResult.matrixOffControl[i][point].VALUE = 0;
                                        matrixResult.matrixOffControl[i][point].SHOW_OUTPUT = matrixResult.matrixOffControl[i][point].VALUE.ToString("#,##0.#0");

                                        matrixResult.matrixPercentage[i][point].VALUE = 0;
                                        matrixResult.matrixPercentage[i][point].SHOW_OUTPUT = matrixResult.matrixPercentage[i][point].VALUE.ToString("#,##0.#0");
                                    }
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public ViewALL_QMS_RP_OFF_CONTROL_SUMMARY getTemplateOffControlSummaryWithAutoCalEx(QMSDBEntities db, OffControlCalAutoSearchModel searchModel)
        {
            ViewALL_QMS_RP_OFF_CONTROL_SUMMARY matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
            TransactionService transServices = new TransactionService(_currentUserName);
            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            try
            {
                matrixResult = getTemplateOffControlSummaryEx(db, searchModel.RPOffControlId, true, searchModel);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public ViewALL_QMS_RP_OFF_CONTROL_DETAIL getTemplateProductCompoOffControlWithAutoCalEx(QMSDBEntities db, DashOffControlCalAutoSearchModel searchModel)
        {
            ViewALL_QMS_RP_OFF_CONTROL_DETAIL matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_DETAIL();
            TransactionService transServices = new TransactionService(_currentUserName);
            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            try
            {
                matrixResult = getTemplateProductCompoOffControlEx(db, true, searchModel);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public OffControlStaticByYearChartBarMod getTemplateByYearOffControlWithAutoCalEx(QMSDBEntities db, DashOffControlCalAutoSearchModel searchModel)
        {
            OffControlStaticByYearChartBarMod matrixResult = new OffControlStaticByYearChartBarMod();
            TransactionService transServices = new TransactionService(_currentUserName);
            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            try
            {
                matrixResult = getTemplateByYearOffControlAutoCalEx(db, true, searchModel);
                //getTemplateByYearOffControlAutoCalEx(db, true, searchModel);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public OffControlStaticByYearChartBarMod getTemplateProductOffControlbyGSP(QMSDBEntities db, DashOffControlCalAutoSearchModel searchModel)
        {
            OffControlStaticByYearChartBarMod matrixResult = new OffControlStaticByYearChartBarMod();
            TransactionService transServices = new TransactionService(_currentUserName);
            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            try
            {
                //matrixResult = getTemplateByYearOffControlAutoCalEx(db, true, searchModel);
                matrixResult = getTemplateProductOffControlbyGSPPercent(db, true, searchModel);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public ViewALL_QMS_RP_OFF_CONTROL_SUMMARY getTemplateDashOffControlSummaryWithAutoCalEx(QMSDBEntities db, DashOffControlCalAutoSearchModel searchModel)
        {
            ViewALL_QMS_RP_OFF_CONTROL_SUMMARY matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
            TransactionService transServices = new TransactionService(_currentUserName);
            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            try
            {
                matrixResult = getTemplateDashOffControlSummaryEx(db, false, searchModel);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public ViewALL_QMS_RP_OFF_CONTROL_SUMMARY getTemplateOffControlSummary(QMSDBEntities db, long rp_off_control_id)
        {
            MasterDataService masterService = new MasterDataService(_currentUserName);
            ViewALL_QMS_RP_OFF_CONTROL_SUMMARY matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            matrixResult.matrixOffControl = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
            matrixResult.matrixInputVolume = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
            matrixResult.matrixPercentage = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            decimal tempPercentage = 0;
            decimal totalOffConrolRowsSum = 0;
            decimal totalVolRowsSum = 0;
            bool bFoundPercentage = false;
            bool bInitial = false;
            bool bSkipVolume = false;
            ViewMatrixSummary tempSummary = null;
            string productName = "";

            try
            {
                List<ViewQMS_MA_PRODUCT> listProduct = masterService.getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PLANT> listPlant = masterService.getQMS_MA_PLANTList(db);

                //Step 1. ดึงค่า offcontrol ทั้งหมด T0
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume = getSumaryOffControlListByRPOffControlId(db, rp_off_control_id);
                //Step 2. ดึงค่า input volume ทั้งหมด T1
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listInputVolume = getSumaryInputVolumeListByRPOffControlId(db, rp_off_control_id);

                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> lastPosition = null;
                int nCol = 0;
                int nRow = 0;
                long currentProduct = 0;

                if (listOffControlVolume.Count() > 0 && listOffControlVolume.Count() == listInputVolume.Count())
                {
                    //Step 0. prepare column and rows
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listColumnHeader = getColumnOffControlSummary(listOffControlVolume);
                    //List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listRowsHeader = getRowsOffControlSummary(listOffControlVolume);

                    //ใส่ column ก่อน
                    matrixResult.matrixOffControl.Add(listColumnHeader);
                    matrixResult.matrixInputVolume.Add(listColumnHeader);
                    matrixResult.matrixPercentage.Add(listColumnHeader);

                    for (int i = 0; i < listOffControlVolume.Count(); i++)
                    {
                        #region condition สำหรั บคิดบรรทั ดใหม่
                        if (0 == currentProduct)
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                            bInitial = true;
                        }

                        if (currentProduct != listOffControlVolume[i].PRODUCT_ID)
                        {

                            tempSummary = getTotalSummary(listSummary, nCol, nRow, (int)REPORT_SUMMARY_MODE.SUM_ROW);

                            //เพิ่ม summary off control
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                            tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");
                            listResult.Add(tempData);
                            matrixResult.matrixOffControl.Add(listResult);

                            //เพิ่ม summary off volume
                            tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                            tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");
                            listVolumeResult.Add(tempVolData);
                            matrixResult.matrixInputVolume.Add(listVolumeResult);

                            //เพิ่ม summary off percentage
                            tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                            if (tempSummary.inputVol > 0)
                            {
                                tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");
                            }
                            else
                            {
                                tempPercentData.VALUE = 0;
                                tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                            }
                            listPercentResult.Add(tempPercentData);
                            matrixResult.matrixPercentage.Add(listPercentResult);

                            //reset
                            totalOffConrolRowsSum = 0;
                            totalVolRowsSum = 0;
                            nRow++;
                            bInitial = true;
                        }
                        else
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                        }

                        if (bInitial == true)
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                            listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                            listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                            listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                            if (currentProduct > 90000) //check low
                            {
                                productName = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.LowControl;

                                //revise name on (Hi)
                                int lastPos = matrixResult.matrixOffControl.Count() - 1;

                                //control
                                lastPosition = matrixResult.matrixOffControl[lastPos];
                                lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                //Volume ยกเลิก volume rows กรณี hi-low
                                //lastPosition = matrixResult.matrixInputVolume[lastPos];
                                //lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                //Percentage
                                lastPosition = matrixResult.matrixPercentage[lastPos];
                                lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                bSkipVolume = true;

                            }
                            else
                            {
                                productName = listOffControlVolume[i].PRODUCT_NAME;
                                bSkipVolume = false;
                            }

                            //เพิ่ม rows offcontrol
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempData.SHOW_OUTPUT = productName;

                            //เพิ่ม rows volume
                            tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempVolData.SHOW_OUTPUT = productName;

                            //เพิ่ม rows percentage
                            tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempPercentData.SHOW_OUTPUT = productName;

                            if (bSkipVolume == true)
                            {
                                tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HIDE;
                            }

                            listResult.Add(tempData);
                            listVolumeResult.Add(tempVolData);
                            listPercentResult.Add(tempPercentData);

                            bInitial = false;
                            nCol = 0; //initial
                        }
                        #endregion

                        #region ค่ าดาต้ าที่ ดึงมาจาก database
                        //offcontrol
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData = listOffControlVolume[i];
                        if (tempData.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL)
                        {
                            tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                            totalOffConrolRowsSum += tempVolData.VALUE;
                        }
                        else
                        {
                            tempData.VALUE = 0;
                            tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                        }

                        //volume
                        tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        if (currentProduct > 90000) //check low
                        {
                            int lastPos = matrixResult.matrixOffControl.Count() - 1;
                            lastPosition = matrixResult.matrixInputVolume[lastPos];
                            tempVolData = lastPosition[nCol + 1];
                        }
                        else
                        {
                            tempVolData = listInputVolume[i];
                        }

                        if (tempVolData.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.INPUT_VOLUME)
                        {

                            tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                            totalVolRowsSum += tempVolData.VALUE;
                            bFoundPercentage = true;

                            if (tempVolData.VALUE > 0)
                            {
                                tempPercentage = tempData.VALUE / tempVolData.VALUE;
                            }
                            else
                            {
                                tempPercentage = 0;
                            }
                        }
                        else
                        {
                            tempVolData.VALUE = 0;
                            tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                        }

                        //percentage
                        tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        //tempPercentData = listOffControlVolume[i];
                        if (true == bFoundPercentage)
                        {
                            tempPercentData.VALUE = tempPercentage;
                            tempPercentData.SHOW_OUTPUT = tempPercentage.ToString("#,##0.#0");
                            bFoundPercentage = false; //reset
                            tempPercentage = 0;
                        }
                        else
                        {
                            tempPercentData.VALUE = 0;
                            tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                        }

                        //addsummary
                        tempSummary = new ViewMatrixSummary();
                        tempSummary.nCols = nCol;
                        tempSummary.nRows = nRow;
                        tempSummary.offControl = tempData.VALUE;
                        tempSummary.inputVol = tempVolData.VALUE;
                        tempSummary.percentage = tempPercentData.VALUE;

                        if (bSkipVolume == true)
                        {
                            tempSummary.inputVol = 0; //reset to 0 เพราะ ข้าม volume
                            tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HIDE;
                        }

                        listResult.Add(tempData);
                        listVolumeResult.Add(tempVolData);
                        listPercentResult.Add(tempPercentData);
                        listSummary.Add(tempSummary);

                        nCol++;

                        #endregion
                    }

                    #region total Summary last row
                    tempSummary = getTotalSummary(listSummary, nCol, nRow, (int)REPORT_SUMMARY_MODE.SUM_ROW);
                    //เพิ่ม summary off control
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                    tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");

                    //เพิ่ม summary off volume
                    tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                    tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");

                    //เพิ่ม summary off percentage
                    tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                    if (tempSummary.inputVol > 0)
                    {
                        tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");
                    }
                    else
                    {
                        tempPercentData.VALUE = 0;
                        tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                    }

                    if (bSkipVolume == true)
                    {
                        bSkipVolume = false;

                        //listResult.Add(tempData);
                        //listPercentResult.Add(tempPercentData);

                        //matrixResult.matrixOffControl.Add(listResult);
                        //matrixResult.matrixPercentage.Add(listPercentResult);
                    }
                    else
                    {
                        listResult.Add(tempData);
                        listVolumeResult.Add(tempVolData);
                        listPercentResult.Add(tempPercentData);

                        matrixResult.matrixOffControl.Add(listResult);
                        matrixResult.matrixInputVolume.Add(listVolumeResult);
                        matrixResult.matrixPercentage.Add(listPercentResult);
                    }

                    #endregion

                    #region total Summary

                    //เพิ่มบรรทัดสุดท้ายที่เป็น ยอดรวม
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //เพิ่ม rows summary
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listResult.Add(tempData);

                    //เพิ่ม summary off volume
                    tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempVolData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listVolumeResult.Add(tempVolData);

                    //เพิ่ม summary off percentage
                    tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempPercentData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listPercentResult.Add(tempPercentData);

                    for (int i = 0; i < nCol; i++)
                    {
                        tempSummary = getTotalSummary(listSummary, i, nRow, (int)REPORT_SUMMARY_MODE.SUM_COL);
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.VALUE = tempSummary.offControl;
                        tempVolData.VALUE = tempSummary.inputVol;
                        tempPercentData.VALUE = tempSummary.percentage;

                        tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");
                        tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");
                        tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");

                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                        tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                        tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;

                        listResult.Add(tempData);
                        listVolumeResult.Add(tempVolData);
                        listPercentResult.Add(tempPercentData);
                    }

                    //add Total col and row
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempSummary = getTotalSummary(listSummary);

                    tempData.VALUE = tempSummary.offControl; // listResult.Sum(m => m.VALUE);
                    tempVolData.VALUE = tempSummary.inputVol; //listVolumeResult.Sum(m => m.VALUE);
                    tempPercentData.VALUE = tempSummary.percentage; //listPercentResult.Sum(m => m.VALUE);

                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                    tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                    tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");

                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;

                    listResult.Add(tempData);
                    listVolumeResult.Add(tempVolData);
                    listPercentResult.Add(tempPercentData);

                    matrixResult.matrixOffControl.Add(listResult);
                    matrixResult.matrixInputVolume.Add(listVolumeResult);
                    matrixResult.matrixPercentage.Add(listPercentResult);

                    #endregion
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public void recalWithOutReportId(QMSDBEntities db, OffControlCalAutoSearchModel search)
        {
            try
            {
                bool isRecalCheck = true;
                List<ViewQMS_MA_PRODUCT> sortlistAllProduct = getProductHiAndLowList(db);
                List<ViewQMS_MA_PLANT> listPlant = getQMS_MA_PLANTList(db);
                TransactionService transService = new TransactionService(_currentUserName);

                foreach (ViewQMS_MA_PLANT plantData in listPlant)
                {
                    isRecalCheck = true;
                    //ต้อง filter เฉพาะ plant & product ที่สนใจ
                    if (false == search.CHK_ALL_PLANT)
                    {
                        isRecalCheck = false;
                        for (int i = 0; i < search.LIST_PLANT_ID.Count(); i++)
                        {
                            if (plantData.ID == search.LIST_PLANT_ID[i])
                            {
                                isRecalCheck = true;
                                break;
                            }
                        }
                    }

                    foreach (ViewQMS_MA_PRODUCT ptData in sortlistAllProduct)
                    {
                        if (false == search.CHK_ALL_PRODUCT && true == isRecalCheck)
                        {
                            isRecalCheck = false;
                            for (int i = 0; i < search.LIST_PRODUCT_ID.Count(); i++)
                            {
                                if (ptData.ID == search.LIST_PRODUCT_ID[i])
                                {
                                    isRecalCheck = true;
                                    break;
                                }
                            }
                        }

                        if (true == isRecalCheck)
                        {
                            //recal raw data
                            transService.reCalRawData(db, plantData.ID, ptData.ID, search.START_DATE, search.END_DATE, false);
                            ExaProductSearchModel searchEx = new ExaProductSearchModel();
                            search.PLANT_ID = plantData.ID;
                            search.PRODUCT_ID = ptData.ID;

                            searchEx.PLANT_ID = plantData.ID;
                            searchEx.PRODUCT_ID = ptData.ID;
                            searchEx.START_DATE = search.START_DATE;
                            searchEx.END_DATE = search.END_DATE;
                            searchEx.bExceptAbnormal = false;

                            //recal off control
                            transService.CalOffControlOffSpec(db, searchEx, (byte)DOC_STATUS.PENDING);

                            //cal Reduce Feed
                            ExaReduceFeedSearchModel searchReduceFeed = new ExaReduceFeedSearchModel();
                            searchReduceFeed.PLANT_ID = plantData.ID;
                            searchReduceFeed.START_DATE = search.START_DATE;
                            searchReduceFeed.END_DATE = search.END_DATE;

                            //cal Downtime
                            ExaDowntimeSearchModel searchDowntime = new ExaDowntimeSearchModel();
                            searchDowntime.PLANT_ID = plantData.ID;
                            searchDowntime.START_DATE = search.START_DATE;
                            searchDowntime.END_DATE = search.END_DATE;

                            //recal raw data
                            transService.recheckRawReduceFeed(db, searchReduceFeed);
                            transService.recheckRawDowntime(db, searchDowntime);

                            //cal ReduceFeed and downtime
                            transService.CalReduceFeed(db, searchReduceFeed);
                            transService.CalDowntime(db, searchDowntime);

                            #region recheck turn around
                            List<ViewQMS_TR_TURN_AROUND> listTurnAround = transService.getQMS_TR_TURN_AROUNDListByPlantAndDateTime(db, plantData.ID, search.AUTO_OFF_TYPE, search.START_DATE, search.END_DATE);

                            if (listTurnAround.Count() > 0 && null != listTurnAround)
                            {

                                foreach (ViewQMS_TR_TURN_AROUND dtTurnAround in listTurnAround)
                                {
                                    CreateQMS_TR_TURN_AROUND model = new CreateQMS_TR_TURN_AROUND();

                                    model.ID = dtTurnAround.ID;
                                    model.PLANT_ID = dtTurnAround.PLANT_ID;
                                    model.START_DATE = dtTurnAround.START_DATE;
                                    model.END_DATE = dtTurnAround.END_DATE;
                                    model.DOC_STATUS = dtTurnAround.DOC_STATUS;
                                    model.CORRECT_DATA_TYPE = dtTurnAround.CORRECT_DATA_TYPE;

                                    transService.SaveQMS_TR_OFF_CONTROL_CAL_With_TRUN_AROUD(db, model);
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LogWriter.WriteErrorLog("2749 " + ex.Message);
                this.writeErrorLog(ex.Message);
            }
        }

        public void reCalRawDataByRPOffControlId(QMSDBEntities db, OffControlCalAutoSearchModel search)
        {
            try
            {
                long rp_off_control_id = search.RPOffControlId;
                TransactionService transService = new TransactionService(_currentUserName);
                bool isRecalCheck = true;

                //Step 0. Clean Manage data //monchai
                //0.1 clean off control/spec 
                //clear all off control manage data 
                transService.DeleteQMS_TR_OFF_CONTROL_CALByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.2 clean turn around 
                transService.DeleteQMS_TR_TURN_AROUNDByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                //0.3 clean downtime
                transService.DeleteQMS_TR_DOWNTIMEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                //0.4 change grade
                transService.DeleteQMS_TR_CHANGE_GRADEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.5 cross volume
                transService.DeleteQMS_TR_CROSS_VOLUMEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.6 clean abnormal
                transService.DeleteQMS_TR_ABNORMALByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.7 clean reduce feed
                transService.DeleteQMS_TR_REDUCE_FEEDyDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                //0.8 clean exception case
                transService.DeleteQMS_TR_EXCEPTIONByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                LogWriter.WriteErrorLog("2787 " + rp_off_control_id);
                //Step 1. ดึงค่า offcontrol ทั้งหมด T0
                if (rp_off_control_id > 0)
                {
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume = getSumaryOffControlListByRPOffControlId(db, rp_off_control_id);

                    LogWriter.WriteErrorLog("2793 " + listOffControlVolume.Count());
                    try
                    {
                        foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dt in listOffControlVolume)
                        {
                            isRecalCheck = true;
                            //ต้อง filter เฉพาะ plant & product ที่สนใจ
                            if (false == search.CHK_ALL_PLANT)
                            {
                                isRecalCheck = false;
                                for (int i = 0; i < search.LIST_PLANT_ID.Count(); i++)
                                {
                                    if (dt.PLANT_ID == search.LIST_PLANT_ID[i])
                                    {
                                        isRecalCheck = true;
                                        break;
                                    }
                                }
                            }

                            if (false == search.CHK_ALL_PRODUCT && true == isRecalCheck)
                            {
                                isRecalCheck = false;
                                for (int i = 0; i < search.LIST_PRODUCT_ID.Count(); i++)
                                {
                                    if (dt.PRODUCT_ID == search.LIST_PRODUCT_ID[i])
                                    {
                                        isRecalCheck = true;
                                        break;
                                    }
                                }
                            }

                            if (true == isRecalCheck)
                            {
                                //recal raw data
                                transService.reCalRawData(db, dt.PLANT_ID, dt.PRODUCT_ID, search.START_DATE, search.END_DATE, false);
                                ExaProductSearchModel searchEx = new ExaProductSearchModel();
                                search.PLANT_ID = dt.PLANT_ID;
                                search.PRODUCT_ID = dt.PRODUCT_ID;

                                searchEx.PLANT_ID = dt.PLANT_ID;
                                searchEx.PRODUCT_ID = dt.PRODUCT_ID;
                                searchEx.START_DATE = search.START_DATE;
                                searchEx.END_DATE = search.END_DATE;
                                searchEx.bExceptAbnormal = false;

                                //recal off control
                                transService.CalOffControlOffSpec(db, searchEx, (byte)DOC_STATUS.PENDING);
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                        LogWriter.WriteErrorLog("2801 " + ex.Message);
                    }

                    #region recheck downtime and redcue feed

                    try
                    {
                        LogWriter.WriteErrorLog("2799 ");
                        if (listOffControlVolume.Count() > 0)
                        {

                            LogWriter.WriteErrorLog("2803 ");
                            List<long> listPlant = listOffControlVolume.GroupBy(m => m.PLANT_ID).Select(m => m.Key).ToList();

                            LogWriter.WriteErrorLog("2806 " + listPlant.Count());
                            if (listPlant.Count() > 0)
                            {
                                foreach (long plantId in listPlant)
                                {

                                    //ต้อง filter เฉพาะ plant & product ที่สนใจ
                                    isRecalCheck = true;
                                    //ต้อง filter เฉพาะ plant & product ที่สนใจ
                                    if (false == search.CHK_ALL_PLANT)
                                    {
                                        isRecalCheck = false;
                                        for (int i = 0; i < search.LIST_PLANT_ID.Count(); i++)
                                        {
                                            if (plantId == search.LIST_PLANT_ID[i])
                                            {
                                                isRecalCheck = true;
                                                break;
                                            }
                                        }
                                    }

                                    if (true == isRecalCheck)
                                    {
                                        //cal Reduce Feed
                                        ExaReduceFeedSearchModel searchReduceFeed = new ExaReduceFeedSearchModel();
                                        searchReduceFeed.PLANT_ID = plantId;
                                        searchReduceFeed.START_DATE = search.START_DATE;
                                        searchReduceFeed.END_DATE = search.END_DATE;

                                        //cal Downtime
                                        ExaDowntimeSearchModel searchDowntime = new ExaDowntimeSearchModel();
                                        searchDowntime.PLANT_ID = plantId;
                                        searchDowntime.START_DATE = search.START_DATE;
                                        searchDowntime.END_DATE = search.END_DATE;

                                        //recal raw data
                                        transService.recheckRawReduceFeed(db, searchReduceFeed);
                                        transService.recheckRawDowntime(db, searchDowntime);

                                        //cal ReduceFeed and downtime
                                        transService.CalReduceFeed(db, searchReduceFeed);
                                        transService.CalDowntime(db, searchDowntime);

                                        #region recheck turn around
                                        List<ViewQMS_TR_TURN_AROUND> listTurnAround = transService.getQMS_TR_TURN_AROUNDListByPlantAndDateTime(db, plantId, search.AUTO_OFF_TYPE, search.START_DATE, search.END_DATE);

                                        if (listTurnAround.Count() > 0 && null != listTurnAround)
                                        {

                                            foreach (ViewQMS_TR_TURN_AROUND dtTurnAround in listTurnAround)
                                            {
                                                CreateQMS_TR_TURN_AROUND model = new CreateQMS_TR_TURN_AROUND();

                                                model.ID = dtTurnAround.ID;
                                                model.PLANT_ID = dtTurnAround.PLANT_ID;
                                                model.START_DATE = dtTurnAround.START_DATE;
                                                model.END_DATE = dtTurnAround.END_DATE;
                                                model.DOC_STATUS = dtTurnAround.DOC_STATUS;
                                                model.CORRECT_DATA_TYPE = dtTurnAround.CORRECT_DATA_TYPE;

                                                transService.SaveQMS_TR_OFF_CONTROL_CAL_With_TRUN_AROUD(db, model);
                                            }
                                        }

                                        #endregion
                                    }
                                }
                            }

                        }

                    }
                    catch (Exception ex)
                    {

                        LogWriter.WriteErrorLog("2881 " + ex.Message);
                    }
                    #endregion
                }
                else
                {

                    LogWriter.WriteErrorLog("2927");
                    //กรณี ที่ไม่ใช่ report  
                    recalWithOutReportId(db, search);
                }

            }
            catch (Exception ex)
            {

                LogWriter.WriteErrorLog("2886 " + ex.Message);
            }
        }

        public void reCalRawDataByDashRPOffControlId(QMSDBEntities db, OffControlCalAutoSearchModel search)
        {
            try
            {
                long rp_off_control_id = search.RPOffControlId;
                TransactionService transService = new TransactionService(_currentUserName);
                bool isRecalCheck = true;

                //Step 0. Clean Manage data //monchai
                //0.1 clean off control/spec 
                //clear all off control manage data 
                transService.DeleteQMS_TR_OFF_CONTROL_CALByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.2 clean turn around 
                transService.DeleteQMS_TR_TURN_AROUNDByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                //0.3 clean downtime
                transService.DeleteQMS_TR_DOWNTIMEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                //0.4 change grade
                transService.DeleteQMS_TR_CHANGE_GRADEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.5 cross volume
                transService.DeleteQMS_TR_CROSS_VOLUMEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.6 clean abnormal
                transService.DeleteQMS_TR_ABNORMALByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //0.7 clean reduce feed
                transService.DeleteQMS_TR_REDUCE_FEEDyDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                //0.8 clean exception case
                transService.DeleteQMS_TR_EXCEPTIONByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                //Step 1. ดึงค่า offcontrol ทั้งหมด T0
                if (rp_off_control_id > 0)
                {
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume = getSumaryOffControlListByRPOffControlId(db, rp_off_control_id);
                    foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dt in listOffControlVolume)
                    {
                        isRecalCheck = true;
                        //ต้อง filter เฉพาะ plant & product ที่สนใจ
                        if (false == search.CHK_ALL_PLANT)
                        {
                            isRecalCheck = false;
                            for (int i = 0; i < search.LIST_PLANT_ID.Count(); i++)
                            {
                                if (dt.PLANT_ID == search.LIST_PLANT_ID[i])
                                {
                                    isRecalCheck = true;
                                    break;
                                }
                            }
                        }

                        if (false == search.CHK_ALL_PRODUCT && true == isRecalCheck)
                        {
                            isRecalCheck = false;
                            for (int i = 0; i < search.LIST_PRODUCT_ID.Count(); i++)
                            {
                                if (dt.PRODUCT_ID == search.LIST_PRODUCT_ID[i])
                                {
                                    isRecalCheck = true;
                                    break;
                                }
                            }
                        }

                        if (true == isRecalCheck)
                        {
                            //recal raw data
                            transService.reCalRawData(db, dt.PLANT_ID, dt.PRODUCT_ID, search.START_DATE, search.END_DATE, false);
                            ExaProductSearchModel searchEx = new ExaProductSearchModel();
                            search.PLANT_ID = dt.PLANT_ID;
                            search.PRODUCT_ID = dt.PRODUCT_ID;

                            searchEx.PLANT_ID = dt.PLANT_ID;
                            searchEx.PRODUCT_ID = dt.PRODUCT_ID;
                            searchEx.START_DATE = search.START_DATE;
                            searchEx.END_DATE = search.END_DATE;
                            searchEx.bExceptAbnormal = false;

                            //recal off control
                            transService.CalOffControlOffSpec(db, searchEx, (byte)DOC_STATUS.PENDING);
                        }
                    }

                    #region recheck downtime and redcue feed
                    if (null != listOffControlVolume && listOffControlVolume.Count() > 0)
                    {

                        List<long> listPlant = listOffControlVolume.GroupBy(m => m.PLANT_ID).Select(m => m.Key).ToList();

                        if (null != listPlant && listPlant.Count() > 0)
                        {
                            foreach (long plantId in listPlant)
                            {

                                //ต้อง filter เฉพาะ plant & product ที่สนใจ
                                isRecalCheck = true;
                                //ต้อง filter เฉพาะ plant & product ที่สนใจ
                                if (false == search.CHK_ALL_PLANT)
                                {
                                    isRecalCheck = false;
                                    for (int i = 0; i < search.LIST_PLANT_ID.Count(); i++)
                                    {
                                        if (plantId == search.LIST_PLANT_ID[i])
                                        {
                                            isRecalCheck = true;
                                            break;
                                        }
                                    }
                                }

                                if (true == isRecalCheck)
                                {
                                    //cal Reduce Feed
                                    ExaReduceFeedSearchModel searchReduceFeed = new ExaReduceFeedSearchModel();
                                    searchReduceFeed.PLANT_ID = plantId;
                                    searchReduceFeed.START_DATE = search.START_DATE;
                                    searchReduceFeed.END_DATE = search.END_DATE;

                                    //cal Downtime
                                    ExaDowntimeSearchModel searchDowntime = new ExaDowntimeSearchModel();
                                    searchDowntime.PLANT_ID = plantId;
                                    searchDowntime.START_DATE = search.START_DATE;
                                    searchDowntime.END_DATE = search.END_DATE;

                                    //recal raw data
                                    transService.recheckRawReduceFeed(db, searchReduceFeed);
                                    transService.recheckRawDowntime(db, searchDowntime);

                                    //cal ReduceFeed and downtime
                                    transService.CalReduceFeed(db, searchReduceFeed);
                                    transService.CalDowntime(db, searchDowntime);

                                    #region recheck turn around
                                    List<ViewQMS_TR_TURN_AROUND> listTurnAround = transService.getQMS_TR_TURN_AROUNDListByPlantAndDateTime(db, plantId, search.AUTO_OFF_TYPE, search.START_DATE, search.END_DATE);

                                    if (listTurnAround.Count() > 0 && null != listTurnAround)
                                    {

                                        foreach (ViewQMS_TR_TURN_AROUND dtTurnAround in listTurnAround)
                                        {
                                            CreateQMS_TR_TURN_AROUND model = new CreateQMS_TR_TURN_AROUND();

                                            model.ID = dtTurnAround.ID;
                                            model.PLANT_ID = dtTurnAround.PLANT_ID;
                                            model.START_DATE = dtTurnAround.START_DATE;
                                            model.END_DATE = dtTurnAround.END_DATE;
                                            model.DOC_STATUS = dtTurnAround.DOC_STATUS;
                                            model.CORRECT_DATA_TYPE = dtTurnAround.CORRECT_DATA_TYPE;

                                            transService.SaveQMS_TR_OFF_CONTROL_CAL_With_TRUN_AROUD(db, model);
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }

                    }
                    #endregion
                }
                else
                { //กรณี ที่ไม่ใช่ report  
                    recalWithOutReportId(db, search);
                }

            }
            catch (Exception ex)
            {

            }
        }

        public void reCalRawDataByDashRPOffControlId(QMSDBEntities db, DashOffControlCalAutoSearchModel search)
        {
            try
            {
                //long rp_off_control_id = search.RPOffControlId;
                //TransactionService transService = new TransactionService(_currentUserName);
                //bool isRecalCheck = true;

                ////Step 0. Clean Manage data //monchai
                ////0.1 clean off control/spec 
                ////clear all off control manage data 
                //transService.DeleteQMS_TR_OFF_CONTROL_CALByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                ////0.2 clean turn around 
                //transService.DeleteQMS_TR_TURN_AROUNDByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                ////0.3 clean downtime
                //transService.DeleteQMS_TR_DOWNTIMEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                ////0.4 change grade
                //transService.DeleteQMS_TR_CHANGE_GRADEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                ////0.5 cross volume
                //transService.DeleteQMS_TR_CROSS_VOLUMEByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                ////0.6 clean abnormal
                //transService.DeleteQMS_TR_ABNORMALByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                ////0.7 clean reduce feed
                //transService.DeleteQMS_TR_REDUCE_FEEDyDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID);

                ////0.8 clean exception case
                //transService.DeleteQMS_TR_EXCEPTIONByDatetime(db, search.START_DATE, search.END_DATE, search.LIST_PLANT_ID, search.LIST_PRODUCT_ID);

                ////Step 1. ดึงค่า offcontrol ทั้งหมด T0
                //if (rp_off_control_id > 0)
                //{
                //    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume = getSumaryOffControlListByRPOffControlId(db, rp_off_control_id);
                //    foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dt in listOffControlVolume)
                //    {
                //        isRecalCheck = true;
                //        //ต้อง filter เฉพาะ plant & product ที่สนใจ
                //        if (false == search.CHK_ALL_PLANT)
                //        {
                //            isRecalCheck = false;
                //            for (int i = 0; i < search.LIST_PLANT_ID.Count(); i++)
                //            {
                //                if (dt.PLANT_ID == search.LIST_PLANT_ID[i])
                //                {
                //                    isRecalCheck = true;
                //                    break;
                //                }
                //            }
                //        }

                //        if (false == search.CHK_ALL_PRODUCT && true == isRecalCheck)
                //        {
                //            isRecalCheck = false;
                //            for (int i = 0; i < search.LIST_PRODUCT_ID.Count(); i++)
                //            {
                //                if (dt.PRODUCT_ID == search.LIST_PRODUCT_ID[i])
                //                {
                //                    isRecalCheck = true;
                //                    break;
                //                }
                //            }
                //        }

                //        if (true == isRecalCheck)
                //        {
                //            //recal raw data
                //            transService.reCalRawData(db, dt.PLANT_ID, dt.PRODUCT_ID, search.START_DATE, search.END_DATE, false);
                //            ExaProductSearchModel searchEx = new ExaProductSearchModel();
                //            search.PLANT_ID = dt.PLANT_ID;
                //            search.PRODUCT_ID = dt.PRODUCT_ID;

                //            searchEx.PLANT_ID = dt.PLANT_ID;
                //            searchEx.PRODUCT_ID = dt.PRODUCT_ID;
                //            searchEx.START_DATE = search.START_DATE;
                //            searchEx.END_DATE = search.END_DATE;
                //            searchEx.bExceptAbnormal = false;

                //            //recal off control
                //            transService.CalOffControlOffSpec(db, searchEx, (byte)DOC_STATUS.PENDING);
                //        }
                //    }

                //    #region recheck downtime and redcue feed
                //    if (null != listOffControlVolume && listOffControlVolume.Count() > 0)
                //    {

                //        List<long> listPlant = listOffControlVolume.GroupBy(m => m.PLANT_ID).Select(m => m.Key).ToList();

                //        if (null != listPlant && listPlant.Count() > 0)
                //        {
                //            foreach (long plantId in listPlant)
                //            {

                //                //ต้อง filter เฉพาะ plant & product ที่สนใจ
                //                isRecalCheck = true;
                //                //ต้อง filter เฉพาะ plant & product ที่สนใจ
                //                if (false == search.CHK_ALL_PLANT)
                //                {
                //                    isRecalCheck = false;
                //                    for (int i = 0; i < search.LIST_PLANT_ID.Count(); i++)
                //                    {
                //                        if (plantId == search.LIST_PLANT_ID[i])
                //                        {
                //                            isRecalCheck = true;
                //                            break;
                //                        }
                //                    }
                //                }

                //                if (true == isRecalCheck)
                //                {
                //                    //cal Reduce Feed
                //                    ExaReduceFeedSearchModel searchReduceFeed = new ExaReduceFeedSearchModel();
                //                    searchReduceFeed.PLANT_ID = plantId;
                //                    searchReduceFeed.START_DATE = search.START_DATE;
                //                    searchReduceFeed.END_DATE = search.END_DATE;

                //                    //cal Downtime
                //                    ExaDowntimeSearchModel searchDowntime = new ExaDowntimeSearchModel();
                //                    searchDowntime.PLANT_ID = plantId;
                //                    searchDowntime.START_DATE = search.START_DATE;
                //                    searchDowntime.END_DATE = search.END_DATE;

                //                    //recal raw data
                //                    transService.recheckRawReduceFeed(db, searchReduceFeed);
                //                    transService.recheckRawDowntime(db, searchDowntime);

                //                    //cal ReduceFeed and downtime
                //                    transService.CalReduceFeed(db, searchReduceFeed);
                //                    transService.CalDowntime(db, searchDowntime);

                //                    #region recheck turn around
                //                    List<ViewQMS_TR_TURN_AROUND> listTurnAround = transService.getQMS_TR_TURN_AROUNDListByPlantAndDateTime(db, plantId, search.AUTO_OFF_TYPE, search.START_DATE, search.END_DATE);

                //                    if (listTurnAround.Count() > 0 && null != listTurnAround)
                //                    {

                //                        foreach (ViewQMS_TR_TURN_AROUND dtTurnAround in listTurnAround)
                //                        {
                //                            CreateQMS_TR_TURN_AROUND model = new CreateQMS_TR_TURN_AROUND();

                //                            model.ID = dtTurnAround.ID;
                //                            model.PLANT_ID = dtTurnAround.PLANT_ID;
                //                            model.START_DATE = dtTurnAround.START_DATE;
                //                            model.END_DATE = dtTurnAround.END_DATE;
                //                            model.DOC_STATUS = dtTurnAround.DOC_STATUS;
                //                            model.CORRECT_DATA_TYPE = dtTurnAround.CORRECT_DATA_TYPE;

                //                            transService.SaveQMS_TR_OFF_CONTROL_CAL_With_TRUN_AROUD(db, model);
                //                        }
                //                    }

                //                    #endregion
                //                }
                //            }
                //        }

                //    }
                //    #endregion
                //}
                //else
                //{//กรณี ที่ไม่ใช่ report  
                //    recalWithOutReportId(db, search);
                //}

            }
            catch (Exception ex)
            {

            }
        }
        public ViewALL_QMS_RP_OFF_CONTROL_SUMMARY getTemplateOffControlSummaryEx(QMSDBEntities db, long rp_off_control_id, bool bAutoCal = false, OffControlCalAutoSearchModel searchModel = null)
        {
            MasterDataService masterService = new MasterDataService(_currentUserName);
            TransactionService transServices = new TransactionService(_currentUserName);
            ViewALL_QMS_RP_OFF_CONTROL_SUMMARY matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);

            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            matrixResult.matrixOffControl = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
            matrixResult.matrixInputVolume = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
            matrixResult.matrixPercentage = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            decimal tempPercentage = 0;
            decimal totalOffConrolRowsSum = 0;
            decimal totalVolRowsSum = 0;
            bool bFoundPercentage = false;
            bool bInitial = false;
            bool bSkipVolume = false;
            ViewMatrixSummary tempSummary = null;
            string productName = "";

            OffControlAutoCalResult autoCalResult = new OffControlAutoCalResult();
            try
            {

                List<ViewQMS_MA_PRODUCT> listProduct = masterService.getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PLANT> listPlant = masterService.getQMS_MA_PLANTList(db);

                //Step 1. ดึงค่า offcontrol ทั้งหมด T0
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume = getSumaryOffControlListByRPOffControlId(db, rp_off_control_id);
                //Step 2. ดึงค่า input volume ทั้งหมด T1
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listInputVolume = getSumaryInputVolumeListByRPOffControlId(db, rp_off_control_id);

                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> lastPosition = null;
                int nCol = 0;
                int nRow = 0;
                long currentProduct = 0;

                if (listOffControlVolume.Count() > 0 && listOffControlVolume.Count() == listInputVolume.Count())
                {
                    //Step 0. prepare column and rows
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listColumnHeader = getColumnOffControlSummary(listOffControlVolume);
                    //List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listRowsHeader = getRowsOffControlSummary(listOffControlVolume);

                    //ใส่ column ก่อน
                    matrixResult.matrixOffControl.Add(listColumnHeader);
                    matrixResult.matrixInputVolume.Add(listColumnHeader);
                    matrixResult.matrixPercentage.Add(listColumnHeader);

                    for (int i = 0; i < listOffControlVolume.Count(); i++)
                    {
                        #region condition สำหรั บคิดบรรทั ดใหม่
                        if (0 == currentProduct)
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                            bInitial = true;
                        }

                        if (currentProduct != listOffControlVolume[i].PRODUCT_ID)
                        {

                            tempSummary = getTotalSummary(listSummary, nCol, nRow, (int)REPORT_SUMMARY_MODE.SUM_ROW);

                            //เพิ่ม summary off control
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                            tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");
                            listResult.Add(tempData);
                            //matrixResult.matrixOffControl.Add(listResult);

                            //เพิ่ม summary off volume
                            tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                            tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");
                            listVolumeResult.Add(tempVolData);
                            //matrixResult.matrixInputVolume.Add(listVolumeResult);

                            //เพิ่ม summary off percentage
                            tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                            if (tempSummary.inputVol > 0)
                            {
                                tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");
                            }
                            else
                            {
                                tempPercentData.VALUE = 0;
                                tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                            }
                            listPercentResult.Add(tempPercentData);
                            //matrixResult.matrixPercentage.Add(listPercentResult);

                            if (bSkipVolume == true)
                            {
                                bSkipVolume = false;
                                //matrixResult.matrixOffControl.Add(listResult);
                                //matrixResult.matrixPercentage.Add(listPercentResult);
                            }
                            else
                            {
                                matrixResult.matrixOffControl.Add(listResult);
                                matrixResult.matrixInputVolume.Add(listVolumeResult);
                                matrixResult.matrixPercentage.Add(listPercentResult);
                            }

                            //reset
                            totalOffConrolRowsSum = 0;
                            totalVolRowsSum = 0;
                            nRow++;
                            bInitial = true;
                        }
                        else
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                        }

                        if (bInitial == true)
                        {
                            currentProduct = listOffControlVolume[i].PRODUCT_ID;
                            listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                            listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                            listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                            if (currentProduct > 90000) //check low
                            {
                                productName = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.LowControl;

                                //revise name on (Hi)
                                int lastPos = matrixResult.matrixOffControl.Count() - 1;

                                //control
                                lastPosition = matrixResult.matrixOffControl[lastPos];
                                lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                //Volume ยกเลิก volume rows กรณี hi-low
                                //lastPosition = matrixResult.matrixInputVolume[lastPos];
                                //lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                //Percentage
                                lastPosition = matrixResult.matrixPercentage[lastPos];
                                lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                bSkipVolume = true;

                            }
                            else
                            {
                                productName = listOffControlVolume[i].PRODUCT_NAME;
                                bSkipVolume = false;
                            }

                            //เพิ่ม rows offcontrol
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempData.SHOW_OUTPUT = productName;

                            //เพิ่ม rows volume
                            tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempVolData.SHOW_OUTPUT = productName;

                            //เพิ่ม rows percentage
                            tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                            tempPercentData.SHOW_OUTPUT = productName;

                            if (bSkipVolume == true)
                            {
                                tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HIDE;
                            }

                            listResult.Add(tempData);
                            listVolumeResult.Add(tempVolData);
                            listPercentResult.Add(tempPercentData);

                            bInitial = false;
                            nCol = 0; //initial
                        }
                        #endregion

                        #region ค่ าดาต้ าที่ ดึงมาจาก database

                        if (true == bAutoCal)
                        {
                            searchModel.PLANT_ID = listOffControlVolume[i].PLANT_ID;
                            searchModel.PRODUCT_ID = listOffControlVolume[i].PRODUCT_ID;
                            autoCalResult = transServices.GetOffControlByOffControlCalAutoSearchEx(db, searchModel);
                        }

                        //offcontrol
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData = listOffControlVolume[i];

                        if (true == bAutoCal)
                        {
                            tempData.VALUE = autoCalResult.VOULUME_OFFCONTROL;
                            tempData.SHOW_OUTPUT = autoCalResult.VOULUME_OFFCONTROL.ToString("#,##0.#0");
                            totalOffConrolRowsSum += autoCalResult.VOULUME_OFFCONTROL;
                        }
                        else
                        {
                            if (tempData.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL)
                            {
                                tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                                totalOffConrolRowsSum += tempVolData.VALUE;
                            }
                            else
                            {
                                tempData.VALUE = 0;
                                tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                            }
                        }

                        //volume
                        tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        if (currentProduct > 90000) //check low
                        {
                            int lastPos = matrixResult.matrixOffControl.Count() - 1;
                            lastPosition = matrixResult.matrixInputVolume[lastPos];
                            tempVolData = lastPosition[nCol + 1];
                        }
                        else
                        {
                            tempVolData = listInputVolume[i];
                        }

                        if (true == bAutoCal && currentProduct < 90000)
                        {
                            tempVolData.VALUE = autoCalResult.VOLUME_INPUT;
                            tempVolData.SHOW_OUTPUT = autoCalResult.VOLUME_INPUT.ToString("#,##0.#0");
                            totalOffConrolRowsSum += autoCalResult.VOLUME_INPUT;
                            bFoundPercentage = true;
                            tempPercentage = (tempVolData.VALUE > 0) ? (tempData.VALUE / tempVolData.VALUE) * 100 : 0;
                        }
                        else
                        {
                            if (tempVolData.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.INPUT_VOLUME || tempVolData.VALUE > 0)
                            {
                                tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                                totalVolRowsSum += tempVolData.VALUE;
                                bFoundPercentage = true;
                                tempPercentage = (tempVolData.VALUE > 0) ? (tempData.VALUE / tempVolData.VALUE) * 100 : 0;
                            }
                            else
                            {
                                tempVolData.VALUE = 0;
                                tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                            }
                        }

                        //percentage
                        tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        //tempPercentData = listOffControlVolume[i];
                        if (true == bFoundPercentage)
                        {
                            tempPercentData.VALUE = tempPercentage;
                            tempPercentData.SHOW_OUTPUT = tempPercentage.ToString("#,##0.#0");
                            bFoundPercentage = false; //reset
                            tempPercentage = 0;
                        }
                        else
                        {
                            tempPercentData.VALUE = 0;
                            tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                        }

                        //addsummary
                        tempSummary = new ViewMatrixSummary();
                        tempSummary.nCols = nCol;
                        tempSummary.nRows = nRow;
                        tempSummary.offControl = tempData.VALUE;
                        tempSummary.inputVol = (currentProduct > 90000) ? 0 : tempVolData.VALUE;
                        tempSummary.percentage = tempPercentData.VALUE;

                        if (bSkipVolume == true)
                        {
                            tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HIDE;
                        }

                        ViewQMS_MA_KPI dataKPI = listKPI.Where(m => m.PRODUCT_ID == listOffControlVolume[i].PRODUCT_ID && m.PLANT_ID == listOffControlVolume[i].PLANT_ID).FirstOrDefault();

                        tempData.SHOW_RANK = getSHOW_RANKEx(tempPercentData.VALUE, listKPI, listOffControlVolume[i].PLANT_ID, listOffControlVolume[i].PRODUCT_ID);
                        tempVolData.SHOW_RANK = tempData.SHOW_RANK;
                        tempPercentData.SHOW_RANK = tempData.SHOW_RANK;

                        listResult.Add(tempData);
                        listVolumeResult.Add(tempVolData);
                        listPercentResult.Add(tempPercentData);
                        listSummary.Add(tempSummary);

                        nCol++;

                        #endregion
                    }

                    #region total Summary last row
                    tempSummary = getTotalSummary(listSummary, nCol, nRow, (int)REPORT_SUMMARY_MODE.SUM_ROW);
                    //เพิ่ม summary off control
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                    tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");

                    //เพิ่ม summary off volume
                    tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                    tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");

                    //เพิ่ม summary off percentage
                    tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                    if (tempSummary.inputVol > 0)
                    {
                        tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");
                    }
                    else
                    {
                        tempPercentData.VALUE = 0;
                        tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                    }

                    if (bSkipVolume == true)
                    {
                        bSkipVolume = false;

                        //listResult.Add(tempData); 
                        //listPercentResult.Add(tempPercentData);

                        //matrixResult.matrixOffControl.Add(listResult); 
                        //matrixResult.matrixPercentage.Add(listPercentResult); 
                    }
                    else
                    {
                        listResult.Add(tempData);
                        listVolumeResult.Add(tempVolData);
                        listPercentResult.Add(tempPercentData);

                        matrixResult.matrixOffControl.Add(listResult);
                        matrixResult.matrixInputVolume.Add(listVolumeResult);
                        matrixResult.matrixPercentage.Add(listPercentResult);
                    }

                    #endregion

                    #region total Summary

                    //เพิ่มบรรทัดสุดท้ายที่เป็น ยอดรวม
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //เพิ่ม rows summary
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listResult.Add(tempData);

                    //เพิ่ม summary off volume
                    tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempVolData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listVolumeResult.Add(tempVolData);

                    //เพิ่ม summary off percentage
                    tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempPercentData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                    listPercentResult.Add(tempPercentData);

                    for (int i = 0; i < nCol; i++)
                    {
                        tempSummary = getTotalSummary(listSummary, i, nRow, (int)REPORT_SUMMARY_MODE.SUM_COL);
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.VALUE = tempSummary.offControl;
                        tempVolData.VALUE = tempSummary.inputVol;
                        tempPercentData.VALUE = tempSummary.percentage;

                        tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");
                        tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");
                        tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");

                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                        tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                        tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;

                        listResult.Add(tempData);
                        listVolumeResult.Add(tempVolData);
                        listPercentResult.Add(tempPercentData);
                    }

                    //add Total col and row
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempSummary = getTotalSummary(listSummary);

                    tempData.VALUE = tempSummary.offControl; // listResult.Sum(m => m.VALUE);
                    tempVolData.VALUE = tempSummary.inputVol; //listVolumeResult.Sum(m => m.VALUE);
                    tempPercentData.VALUE = tempSummary.percentage; //listPercentResult.Sum(m => m.VALUE);

                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                    tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                    tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");

                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;

                    listResult.Add(tempData);
                    listVolumeResult.Add(tempVolData);
                    listPercentResult.Add(tempPercentData);

                    matrixResult.matrixOffControl.Add(listResult);
                    matrixResult.matrixInputVolume.Add(listVolumeResult);
                    matrixResult.matrixPercentage.Add(listPercentResult);

                    #endregion
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public ViewALL_QMS_RP_OFF_CONTROL_SUMMARY getTemplateDashOffControlSummaryEx(QMSDBEntities db, bool bAutoCal = false, DashOffControlCalAutoSearchModel searchModel = null)
        {
            MasterDataService masterService = new MasterDataService(_currentUserName);
            TransactionService transServices = new TransactionService(_currentUserName);
            ViewALL_QMS_RP_OFF_CONTROL_SUMMARY matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);

            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            matrixResult.matrixOffControl = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
            matrixResult.matrixInputVolume = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
            matrixResult.matrixPercentage = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            decimal tempPercentage = 0;
            decimal totalOffConrolRowsSum = 0;
            decimal totalVolRowsSum = 0;
            bool bFoundPercentage = false;
            bool bInitial = false;
            bool bSkipVolume = false;
            ViewMatrixSummary tempSummary = null;
            string productName = "";

            OffControlAutoCalResult autoCalResult = new OffControlAutoCalResult();
            try
            {

                List<ViewQMS_MA_PRODUCT> listProduct = masterService.getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PLANT> listPlant = masterService.getQMS_MA_PLANTList(db);

                List<QMS_RP_OFF_CONTROL> listDataRP = new List<QMS_RP_OFF_CONTROL>();
                listDataRP = QMS_RP_OFF_CONTROL.GetAllDashBySearch(db, searchModel);

                if (listDataRP != null)
                {

                    //objResult;

                    //Step 1. ดึงค่า offcontrol ทั้งหมด T0
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolumeBeforegroup = getSumaryDashOffControlListByRPOffControlId(db, listDataRP);
                    

                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listOffControlVolume = listOffControlVolumeBeforegroup.GroupBy(x => new { x.PLANT_ID, x.PRODUCT_ID }).Select(g => g.First()).ToList();
                    //Step 2. ดึงค่า input volume ทั้งหมด T1
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listInputVolumeBeforegroup = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listInputVolume = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    if (listDataRP.Count() > 1)
                    {
                        listInputVolumeBeforegroup = getSumaryInputVolumeListByListRPOffControlId(db, listDataRP);

                       listInputVolume = listInputVolumeBeforegroup.GroupBy(x => new { x.PLANT_ID, x.PRODUCT_ID }).Select(g => g.First()).ToList();

                    }else
                    {
                         listInputVolume = getSumaryInputVolumeListByListRPOffControlId(db, listDataRP);
                    }




                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> lastPosition = null;

                    int nCol = 0;
                    int nRow = 0;
                    long currentProduct = 0;

                    if (listOffControlVolume.Count() > 0 && listOffControlVolume.Count() == listInputVolume.Count())
                    {
                        //Step 0. prepare column and rows
                        List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listColumnHeader = getColumnOffControlSummary(listOffControlVolume);
                        //List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listRowsHeader = getRowsOffControlSummary(listOffControlVolume);

                        //ใส่ column ก่อน
                        matrixResult.matrixOffControl.Add(listColumnHeader);
                        matrixResult.matrixInputVolume.Add(listColumnHeader);
                        matrixResult.matrixPercentage.Add(listColumnHeader);

                        for (int i = 0; i < listOffControlVolume.Count(); i++)
                        {
                            #region condition สำหรั บคิดบรรทั ดใหม่
                            if (0 == currentProduct)
                            {
                                currentProduct = listOffControlVolume[i].PRODUCT_ID;
                                bInitial = true;
                            }

                            if (currentProduct != listOffControlVolume[i].PRODUCT_ID)
                            {

                                tempSummary = getTotalSummary(listSummary, nCol, nRow, (int)REPORT_SUMMARY_MODE.SUM_ROW);

                                //เพิ่ม summary off control
                                tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                                tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                                tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");
                                listResult.Add(tempData);
                                //matrixResult.matrixOffControl.Add(listResult);

                                //เพิ่ม summary off volume
                                tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                                tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                                tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");
                                listVolumeResult.Add(tempVolData);
                                //matrixResult.matrixInputVolume.Add(listVolumeResult);

                                //เพิ่ม summary off percentage
                                tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                                tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                                if (tempSummary.inputVol > 0)
                                {
                                    tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");
                                }
                                else
                                {
                                    tempPercentData.VALUE = 0;
                                    tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                                }
                                listPercentResult.Add(tempPercentData);
                                //matrixResult.matrixPercentage.Add(listPercentResult);

                                if (bSkipVolume == true)
                                {
                                    bSkipVolume = false;
                                    //matrixResult.matrixOffControl.Add(listResult);
                                    //matrixResult.matrixPercentage.Add(listPercentResult);
                                }
                                else
                                {
                                    matrixResult.matrixOffControl.Add(listResult);
                                    matrixResult.matrixInputVolume.Add(listVolumeResult);
                                    matrixResult.matrixPercentage.Add(listPercentResult);
                                }

                                //reset
                                totalOffConrolRowsSum = 0;
                                totalVolRowsSum = 0;
                                nRow++;
                                bInitial = true;
                            }
                            else
                            {
                                currentProduct = listOffControlVolume[i].PRODUCT_ID;
                            }

                            if (bInitial == true)
                            {
                                currentProduct = listOffControlVolume[i].PRODUCT_ID;
                                listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                                listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                                listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                                if (currentProduct > 90000) //check low
                                {
                                    productName = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.LowControl;

                                    //revise name on (Hi)
                                    int lastPos = matrixResult.matrixOffControl.Count() - 1;

                                    //control
                                    lastPosition = matrixResult.matrixOffControl[lastPos];
                                    lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                    //Volume ยกเลิก volume rows กรณี hi-low
                                    //lastPosition = matrixResult.matrixInputVolume[lastPos];
                                    //lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                    //Percentage
                                    lastPosition = matrixResult.matrixPercentage[lastPos];
                                    lastPosition[0].SHOW_OUTPUT = listOffControlVolume[i - 1].PRODUCT_NAME + " " + @Resource.ResourceString.HighControl;

                                    bSkipVolume = true;

                                }
                                else
                                {
                                    productName = listOffControlVolume[i].PRODUCT_NAME;
                                    bSkipVolume = false;
                                }

                                //เพิ่ม rows offcontrol
                                tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                                tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                                tempData.SHOW_OUTPUT = productName;

                                //เพิ่ม rows volume
                                tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                                tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                                tempVolData.SHOW_OUTPUT = productName;

                                //เพิ่ม rows percentage
                                tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                                tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                                tempPercentData.SHOW_OUTPUT = productName;

                                if (bSkipVolume == true)
                                {
                                    tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HIDE;
                                }

                                listResult.Add(tempData);
                                listVolumeResult.Add(tempVolData);
                                listPercentResult.Add(tempPercentData);

                                bInitial = false;
                                nCol = 0; //initial
                            }
                            #endregion

                            #region ค่ าดาต้ าที่ ดึงมาจาก database

                            if (true == bAutoCal)
                            {
                                searchModel.PLANT_ID = listOffControlVolume[i].PLANT_ID;
                                searchModel.PRODUCT_ID = listOffControlVolume[i].PRODUCT_ID;
                                autoCalResult = transServices.GetOffControlByListOffControlCalAutoSearchEx(db, searchModel);
                            }

                            //offcontrol


                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempData = listOffControlVolume[i];
                            tempData.VALUE = listOffControlVolumeBeforegroup.Where(x => x.PLANT_ID == listOffControlVolume[i].PLANT_ID && x.PRODUCT_ID == listOffControlVolume[i].PRODUCT_ID).Sum(x => x.VALUE);
                            if (true == bAutoCal)
                            {
                                tempData.VALUE = autoCalResult.VOULUME_OFFCONTROL;
                                tempData.SHOW_OUTPUT = autoCalResult.VOULUME_OFFCONTROL.ToString("#,##0.#0");
                                totalOffConrolRowsSum += autoCalResult.VOULUME_OFFCONTROL;
                            }
                            else
                            {
                                if (tempData.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL)
                                {
                                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                                    totalOffConrolRowsSum += tempVolData.VALUE;
                                }
                                else
                                {
                                    tempData.VALUE = 0;
                                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                                }
                            }

                            //volume
                            tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            if (currentProduct > 90000) //check low
                            {
                                int lastPos = matrixResult.matrixOffControl.Count() - 1;
                                lastPosition = matrixResult.matrixInputVolume[lastPos];
                                tempVolData = lastPosition[nCol + 1];
                            }
                            else
                            {
                                if(listDataRP.Count() > 1)
                                {
                                    tempVolData = listInputVolume[i];
                                    tempVolData.VALUE = listInputVolumeBeforegroup.Where(x => x.PLANT_ID == listInputVolume[i].PLANT_ID && x.PRODUCT_ID == listInputVolume[i].PRODUCT_ID).Sum(x => x.VALUE);
                                }
                                else
                                {
                                    tempVolData = listInputVolume[i];
                                }
                                
                            }

                            if (true == bAutoCal && currentProduct < 90000)
                            {
                                tempVolData.VALUE = autoCalResult.VOLUME_INPUT;
                                tempVolData.SHOW_OUTPUT = autoCalResult.VOLUME_INPUT.ToString("#,##0.#0");
                                totalOffConrolRowsSum += autoCalResult.VOLUME_INPUT;
                                bFoundPercentage = true;
                                tempPercentage = (tempVolData.VALUE > 0) ? (tempData.VALUE / tempVolData.VALUE) * 100 : 0;
                            }
                            else
                            {
                                if (tempVolData.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.INPUT_VOLUME || tempVolData.VALUE > 0)
                                {
                                    tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                                    totalVolRowsSum += tempVolData.VALUE;
                                    bFoundPercentage = true;
                                    tempPercentage = (tempVolData.VALUE > 0) ? (tempData.VALUE / tempVolData.VALUE) * 100 : 0;
                                }
                                else
                                {
                                    tempVolData.VALUE = 0;
                                    tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                                }
                            }

                            //percentage
                            tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            //tempPercentData = listOffControlVolume[i];
                            if (true == bFoundPercentage)
                            {
                                tempPercentData.VALUE = tempPercentage;
                                tempPercentData.SHOW_OUTPUT = tempPercentage.ToString("#,##0.#0");
                                bFoundPercentage = false; //reset
                                tempPercentage = 0;
                            }
                            else
                            {
                                tempPercentData.VALUE = 0;
                                tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                            }

                            //addsummary
                            tempSummary = new ViewMatrixSummary();
                            tempSummary.nCols = nCol;
                            tempSummary.nRows = nRow;
                            tempSummary.offControl = tempData.VALUE;
                            tempSummary.inputVol = (currentProduct > 90000) ? 0 : tempVolData.VALUE;
                            tempSummary.percentage = tempPercentData.VALUE;

                            if (bSkipVolume == true)
                            {
                                tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HIDE;
                            }

                            ViewQMS_MA_KPI dataKPI = listKPI.Where(m => m.PRODUCT_ID == listOffControlVolume[i].PRODUCT_ID && m.PLANT_ID == listOffControlVolume[i].PLANT_ID).FirstOrDefault();

                            tempData.SHOW_RANK = getSHOW_RANKEx(tempPercentData.VALUE, listKPI, listOffControlVolume[i].PLANT_ID, listOffControlVolume[i].PRODUCT_ID);
                            tempVolData.SHOW_RANK = tempData.SHOW_RANK;
                            tempPercentData.SHOW_RANK = tempData.SHOW_RANK;

                            listResult.Add(tempData);
                            listVolumeResult.Add(tempVolData);
                            listPercentResult.Add(tempPercentData);
                            listSummary.Add(tempSummary);

                            nCol++;

                            #endregion
                        }

                        #region total Summary last row
                        tempSummary = getTotalSummary(listSummary, nCol, nRow, (int)REPORT_SUMMARY_MODE.SUM_ROW);
                        //เพิ่ม summary off control
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                        tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");

                        //เพิ่ม summary off volume
                        tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                        tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");

                        //เพิ่ม summary off percentage
                        tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                        if (tempSummary.inputVol > 0)
                        {
                            tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");
                        }
                        else
                        {
                            tempPercentData.VALUE = 0;
                            tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");
                        }

                        if (bSkipVolume == true)
                        {
                            bSkipVolume = false;

                            //listResult.Add(tempData); 
                            //listPercentResult.Add(tempPercentData);

                            //matrixResult.matrixOffControl.Add(listResult); 
                            //matrixResult.matrixPercentage.Add(listPercentResult); 
                        }
                        else
                        {
                            listResult.Add(tempData);
                            listVolumeResult.Add(tempVolData);
                            listPercentResult.Add(tempPercentData);

                            matrixResult.matrixOffControl.Add(listResult);
                            matrixResult.matrixInputVolume.Add(listVolumeResult);
                            matrixResult.matrixPercentage.Add(listPercentResult);
                        }

                        #endregion

                        #region total Summary

                        //เพิ่มบรรทัดสุดท้ายที่เป็น ยอดรวม
                        listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                        //เพิ่ม rows summary
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                        listResult.Add(tempData);

                        //เพิ่ม summary off volume
                        tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempVolData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                        listVolumeResult.Add(tempVolData);

                        //เพิ่ม summary off percentage
                        tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempPercentData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.Total;
                        listPercentResult.Add(tempPercentData);

                        for (int i = 0; i < nCol; i++)
                        {
                            tempSummary = getTotalSummary(listSummary, i, nRow, (int)REPORT_SUMMARY_MODE.SUM_COL);
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                            tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.VALUE = tempSummary.offControl;
                            tempVolData.VALUE = tempSummary.inputVol;
                            tempPercentData.VALUE = tempSummary.percentage;

                            tempData.SHOW_OUTPUT = tempSummary.offControl.ToString("#,##0.#0");
                            tempVolData.SHOW_OUTPUT = tempSummary.inputVol.ToString("#,##0.#0");
                            tempPercentData.SHOW_OUTPUT = tempSummary.percentage.ToString("#,##0.#0");

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                            tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                            tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;

                            listResult.Add(tempData);
                            listVolumeResult.Add(tempVolData);
                            listPercentResult.Add(tempPercentData);
                        }

                        //add Total col and row
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempSummary = getTotalSummary(listSummary);

                        tempData.VALUE = tempSummary.offControl; // listResult.Sum(m => m.VALUE);
                        tempVolData.VALUE = tempSummary.inputVol; //listVolumeResult.Sum(m => m.VALUE);
                        tempPercentData.VALUE = tempSummary.percentage; //listPercentResult.Sum(m => m.VALUE);

                        tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                        tempVolData.SHOW_OUTPUT = tempVolData.VALUE.ToString("#,##0.#0");
                        tempPercentData.SHOW_OUTPUT = tempPercentData.VALUE.ToString("#,##0.#0");

                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                        tempVolData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                        tempPercentData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;

                        listResult.Add(tempData);
                        listVolumeResult.Add(tempVolData);
                        listPercentResult.Add(tempPercentData);

                        matrixResult.matrixOffControl.Add(listResult);
                        matrixResult.matrixInputVolume.Add(listVolumeResult);
                        matrixResult.matrixPercentage.Add(listPercentResult);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public OffControlStaticByYearChartBarMod getTemplateProductOffControlbyGSPPercent(QMSDBEntities db, bool bAutoCal = false, DashOffControlCalAutoSearchModel searchModel = null)
        {
            MasterDataService masterDataServices = new MasterDataService(_currentUserName);
            MasterDataService masterService = new MasterDataService(_currentUserName);
            TransactionService transServices = new TransactionService(_currentUserName);
            OffControlStaticByYearChartBarMod matrixResult = new OffControlStaticByYearChartBarMod();

            List<QMS_RP_OFF_CONTROL> listDataRP = new List<QMS_RP_OFF_CONTROL>();
            List<QMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<QMS_RP_OFF_CONTROL_DETAIL>();
            List<QMS_RP_OFF_CONTROL_DETAIL> list = new List<QMS_RP_OFF_CONTROL_DETAIL>();
            List<QMS_RP_OFF_CONTROL_DETAIL> listdata = new List<QMS_RP_OFF_CONTROL_DETAIL>();

            List<ViewQMS_MA_PLANT> listPlant = masterService.getQMS_MA_PLANTList(db);

            try
            {

                listDataRP = QMS_RP_OFF_CONTROL.GetAllDashBySearch(db, searchModel);

                foreach (var obj in listDataRP)
                {

                    list = QMS_RP_OFF_CONTROL_DETAIL.GetActiveAllByRP_OFF_CONTROL_ID(db, obj.ID);

                    if (list.Count > 0)
                    {
                        foreach (var dataDetail in list)
                        {
                            listDataDetail.Add(dataDetail);
                        }
                    }

                }

                foreach (var Plant in listPlant)
                {
                    OffControlStaticByYearDataBarMod OffControlStaticByYearDataBarMod = new OffControlStaticByYearDataBarMod();
                    listdata = listDataDetail.Where(m => m.PLANT_ID == Plant.ID).ToList();
                    decimal VolumeTotal = 0;
                    foreach (var data in listdata)
                    {
                        VolumeTotal = VolumeTotal + data.VOLUME;
                    }

                    if (listdata.Count() == 0)
                    {
                        OffControlStaticByYearDataBarMod.data.Add(null);
                    }
                    else
                    {

                        OffControlStaticByYearDataBarMod.data.Add(VolumeTotal);

                    }

                }

                //decimal VolumeTotal = 0;
                //var year = "";
                //if (listDataDetail.Count > 0)
                //{
                //    var datecom = listDataDetail.GroupBy(x => x.START_DATE.Year).Select(x => x.Key).ToList();
                //    var groupPlan = listDataDetail.GroupBy(x => x.PLANT_ID).Select(x => x.Key).ToList();
                //    //foreach (var addYear in datecom)
                //    //{ 
                //    //    matrixResult.labels.Add(addYear.ToString());
                //    //}

                //    foreach (var objPlan in groupPlan)
                //    {
                //        OffControlStaticByYearDataBarMod OffControlStaticByYearDataBarMod = new OffControlStaticByYearDataBarMod();
                //        QMS_MA_PLANT maPlant = QMS_MA_PLANT.GetById(db, (long)objPlan);
                //        string PLANT_NAME = maPlant != null ? maPlant.NAME : "";
                //        OffControlStaticByYearDataBarMod.label = PLANT_NAME.ToString();

                //        foreach (var objYear in years)
                //        {
                //            listByYear = listDataDetail.Where(m => m.START_DATE.Year == objYear && m.PLANT_ID == objPlan).ToList();
                //            foreach (var objByYear in listByYear)
                //            {
                //                VolumeTotal = VolumeTotal + objByYear.VOLUME;
                //            }

                //            if (listByYear.Count() == 0)
                //            {
                //                OffControlStaticByYearDataBarMod.data.Add(null);
                //            }
                //            else
                //            {

                //                OffControlStaticByYearDataBarMod.data.Add(VolumeTotal);

                //            }
                //        }
                //        matrixResult.datasets.Add(OffControlStaticByYearDataBarMod);
                //    }
                //    matrixResult.datasets = matrixResult.datasets.Where(x => x.data.Where(m => m.HasValue).Count() > 0).ToList();
                //}

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public OffControlStaticByYearChartBarMod getTemplateByYearOffControlAutoCalEx(QMSDBEntities db, bool bAutoCal = false, DashOffControlCalAutoSearchModel searchModel = null)
        {
            MasterDataService masterDataServices = new MasterDataService(_currentUserName);
            MasterDataService masterService = new MasterDataService(_currentUserName);
            TransactionService transServices = new TransactionService(_currentUserName);
            OffControlStaticByYearChartBarMod matrixResult = new OffControlStaticByYearChartBarMod();

            List<QMS_RP_OFF_CONTROL> listDataRP = new List<QMS_RP_OFF_CONTROL>();
            List<QMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<QMS_RP_OFF_CONTROL_DETAIL>();
            List<QMS_RP_OFF_CONTROL_DETAIL> list = new List<QMS_RP_OFF_CONTROL_DETAIL>();
            List<QMS_RP_OFF_CONTROL_DETAIL> listByYear = new List<QMS_RP_OFF_CONTROL_DETAIL>();

            List<ViewQMS_MA_PLANT> listPlant = masterService.getQMS_MA_PLANTList(db);

            try
            {

                if (searchModel.END_DATE != null)
                {
                    searchModel.END_DATE = new DateTime(searchModel.END_DATE.Year, 12, 31, 23, 59, 59);
                    searchModel.START_DATE = new DateTime(searchModel.END_DATE.Year - 9, 01, 01, 00, 00, 00);
                }

                List<int> years = new List<int>();
                for (int i = 0; i < 10; i++)
                {
                    years.Add(searchModel.START_DATE.Year + i);
                    matrixResult.labels.Add((searchModel.START_DATE.Year + i).ToString());
                }

                listDataRP = QMS_RP_OFF_CONTROL.GetAllDashByFlag(db, searchModel);

                foreach (var obj in listDataRP)
                {

                    list = QMS_RP_OFF_CONTROL_DETAIL.GetActiveAllByRP_OFF_CONTROL_ID(db, obj.ID);

                    if (list.Count > 0)
                    {
                        foreach (var dataDetail in list)
                        {
                            listDataDetail.Add(dataDetail);
                        }
                    }

                }

                decimal VolumeTotal = 0;
                var year = "";
                if (listDataDetail.Count > 0)
                {
                    var datecom = listDataDetail.GroupBy(x => x.START_DATE.Year).Select(x => x.Key).ToList();
                    //var groupPlan = listDataDetail.GroupBy(x => x.PLANT_ID).Select(x => x.Key).ToList();
                    //foreach (var addYear in datecom)
                    //{ 
                    //    matrixResult.labels.Add(addYear.ToString());
                    //}
                    List<QMS_MA_PLANT> maPlantAll = QMS_MA_PLANT.GetAll(db);
                    maPlantAll = maPlantAll.Where(x => x.NAME != "None").ToList();

                    foreach (var objPlan in maPlantAll)
                    {
                        OffControlStaticByYearDataBarMod OffControlStaticByYearDataBarMod = new OffControlStaticByYearDataBarMod();
                        //QMS_MA_PLANT maPlant = QMS_MA_PLANT.GetById(db, (long)objPlan);
                        //string PLANT_NAME = maPlant != null ? maPlant.NAME : "";
                        OffControlStaticByYearDataBarMod.label = objPlan.NAME.ToString();
                        
                        foreach (var objYear in years)
                        {
                            decimal PlanVolumeTotal = listDataDetail.Where(m => m.START_DATE.Year == objYear).Select(x => x.VOLUME).Sum();
                            VolumeTotal = 0;
                            listByYear = listDataDetail.Where(m => m.START_DATE.Year == objYear && m.PLANT_ID == objPlan.ID).ToList();
                            foreach (var objByYear in listByYear)
                            {
                                VolumeTotal = VolumeTotal + objByYear.VOLUME;
                            }

                            if (listByYear.Count() == 0 || PlanVolumeTotal == 0)
                            {
                                OffControlStaticByYearDataBarMod.data.Add(null);
                            }
                            else
                            {
                                decimal valume = (VolumeTotal * 100) / PlanVolumeTotal;
                                OffControlStaticByYearDataBarMod.data.Add(valume);

                            }
                        }
                        matrixResult.datasets.Add(OffControlStaticByYearDataBarMod);
                    }
                    //matrixResult.datasets = matrixResult.datasets.Where(x => x.data.Where(m => m.HasValue).Count() > 0).ToList();
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public ViewALL_QMS_RP_OFF_CONTROL_DETAIL getTemplateProductCompoOffControlEx(QMSDBEntities db, bool bAutoCal = false, DashOffControlCalAutoSearchModel searchModel = null)
        {
            MasterDataService masterDataServices = new MasterDataService(_currentUserName);
            MasterDataService masterService = new MasterDataService(_currentUserName);
            TransactionService transServices = new TransactionService(_currentUserName);
            ViewALL_QMS_RP_OFF_CONTROL_DETAIL matrixResult = new ViewALL_QMS_RP_OFF_CONTROL_DETAIL();

            matrixResult.matrixComposition = new List<List<ViewResultQMS_RP_OFF_CONTROL_DETAIL>>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listVolumeResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listPercentResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);

            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempVolData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempPercentData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

            List<ViewMatrixSummary> listSummary = new List<ViewMatrixSummary>();

            decimal tempPercentage = 0;
            decimal totalOffConrolRowsSum = 0;
            decimal totalVolRowsSum = 0;
            bool bFoundPercentage = false;
            bool bInitial = false;
            bool bSkipVolume = false;
            ViewMatrixSummary tempSummary = null;
            string productName = "";

            OffControlAutoCalResult autoCalResult = new OffControlAutoCalResult();
            try
            {

                //long PlantId = 4;
                //long Product = 4;
                long PlantId = searchModel.PLANT_ID;
                long Product = searchModel.PRODUCT_ID;

                List<ViewQMS_MA_PRODUCT> listProduct = masterService.getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PLANT> listPlant = masterService.getQMS_MA_PLANTList(db);
                List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING> listComposition = masterDataServices.GetComposationbyplanIdproId(db, PlantId, Product);

                List<QMS_RP_OFF_CONTROL> listDataRP = new List<QMS_RP_OFF_CONTROL>();
                ViewResultQMS_RP_OFF_CONTROL_DETAIL objDetail = new ViewResultQMS_RP_OFF_CONTROL_DETAIL();
                List<ViewResultQMS_RP_OFF_CONTROL_DETAIL> listResults = new List<ViewResultQMS_RP_OFF_CONTROL_DETAIL>();

                listDataRP = QMS_RP_OFF_CONTROL.GetAllDashBySearch(db, searchModel);

                if (listDataRP != null)
                {

                    //Step 2. ดึงค่า Composition ทั้งหมด 
                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listCompositionValue = getCompositionListByListRPOffControlId(db, listDataRP, searchModel);
                    var resultB = listCompositionValue.GroupBy(x => x.CONTROL_VALUE).ToList();

                    int nCol = 0;
                    int nRow = 0;
                    long currentProduct = 0;

                    if (listCompositionValue.Count() > 0)
                    {

                        //List<ViewQMS_RP_OFF_CONTROL_DETAIL> queryC2 = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();

                        //var dateC1 = listCompositionValue.Where(m => m.CONTROL_VALUE.Contains("C1")).GroupBy(m => m.START_DATE).ToList();

                        var groupCompo = listCompositionValue.GroupBy(m => m.CONTROL_VALUE).ToList();
                        foreach (var compo in listComposition)
                        {
                            string compareString = compo.EXCEL_NAME.Contains("C4") ? "C4" : compo.EXCEL_NAME;
                            var datecom = listCompositionValue.Where(m => m.CONTROL_VALUE.Contains(compareString) && m.CONTROL_VALUE != "").GroupBy(m => m.START_DATE).ToList();
                            DateTime newDate = new DateTime();

                            decimal volume = 0;
                            foreach (var date in datecom)
                            {
                                List<ViewQMS_RP_OFF_CONTROL_DETAIL> result = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                                result = listCompositionValue.Where(m => m.CONTROL_VALUE.Contains(compareString) && m.START_DATE == date.Key).ToList();

                                foreach (var data in result)
                                {
                                    volume = volume + data.VOLUME;
                                }

                                newDate = date.Key;
                            }

                            if (volume > 0)
                            {
                                int percent = 0;
                                string objDate = newDate.ToString("yyyy-MM-dd");

                                objDetail.START_DATE = objDate;
                                objDetail.COMPOSITION = compareString;
                                objDetail.TOTAL_VOLUME = volume;
                                objDetail.PLANT_ID = PlantId;
                                objDetail.PRODUCT_ID = Product;
                                objDetail.PERCENT = percent;

                                listResults.Add(objDetail);

                            }

                            decimal sumVolume = 0;
                            int i = 0;
                            foreach (var composition in listResults)
                            {
                                sumVolume = sumVolume + composition.TOTAL_VOLUME;
                            }

                            foreach (var com in listResults)
                            {
                                if (sumVolume != 0)
                                {
                                    listResults[i].PERCENT = listResults[i].TOTAL_VOLUME * (100 / sumVolume);
                                }
                                i++;
                            }
                        }

                        //matrixComposition.Add(listResults);
                        matrixResult.matrixComposition.Add(listResults);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> getTemplateOffControlSummary_T0(QMSDBEntities db, long rp_off_control_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixResult = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            try
            {
                List<ViewQMS_MA_PLANT> listColumn = getQMS_MA_PLANTList(db);
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlId(db, rp_off_control_id);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
                List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);
                List<ViewQMS_MA_PRODUCT> listRow = getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PRODUCT> sortlistAllProduct = getProductHiAndLowList(db);

                if (listRow.Count() > 0 && listColumn.Count() > 0)
                {
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    listResult.Add(tempData);
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.RP_OFF_CONTROL_ID = dtColumn.ID;
                        tempData.PLANT_ID = dtColumn.ID;
                        tempData.PLANT_NAME = dtColumn.NAME;
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                        tempData.SHOW_OUTPUT = dtColumn.NAME;
                        listResult.Add(tempData);
                    }
                    matrixResult.Add(listResult);

                    //Data Row ........ สร้าง row
                    foreach (ViewQMS_MA_PRODUCT dtRow in sortlistAllProduct)
                    {

                        // theader row
                        listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = dtRow.ID;
                        tempData.PRODUCT_NAME = dtRow.NAME;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempData.SHOW_OUTPUT = dtRow.NAME;
                        listResult.Add(tempData);

                        foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                        {
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.PLANT_ID = dtColumn.ID;
                            tempData.PLANT_NAME = getPlantName(listPlant, dtColumn.ID);
                            tempData.RP_OFF_CONTROL_ID = dtRow.ID;
                            tempData.PRODUCT_ID = dtRow.ID;
                            tempData.PRODUCT_NAME = getProductName(listProduct, dtRow.ID);

                            tempData.VALUE = 0;
                            tempData.SHOW_OUTPUT = tempData.VALUE.ToString("0.##");

                            if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                            {
                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 0).FirstOrDefault();
                                ViewQMS_MA_KPI dataKPI = listKPI.Where(m => m.PRODUCT_ID == dtRow.ID && m.PLANT_ID == dtColumn.ID).FirstOrDefault();

                                if (null != dataItem)
                                {
                                    dataItem.SHOW_RANK = getSHOW_RANK(listData, listKPI, dtRow, dtColumn);
                                    dataItem.SHOW_OUTPUT = dataItem.VALUE.ToString("0.##");
                                    tempData = dataItem;

                                }
                            }
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                            listResult.Add(tempData);
                        }

                        matrixResult.Add(listResult);
                    }
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> getTemplateOffControlSummary_T1(QMSDBEntities db, long rp_off_control_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixResult = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            try
            {
                List<ViewQMS_MA_PLANT> listColumn = getQMS_MA_PLANTList(db);
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlId(db, rp_off_control_id);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
                List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);
                List<ViewQMS_MA_PRODUCT> listRow = getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PRODUCT> sortlistAllProduct = getProductHiAndLowList(db);

                if (listRow.Count() > 0 && listColumn.Count() > 0)
                {
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    listResult.Add(tempData);

                    foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.RP_OFF_CONTROL_ID = dtColumn.ID;
                        tempData.PLANT_ID = dtColumn.ID;
                        tempData.PLANT_NAME = dtColumn.NAME;
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                        tempData.SHOW_OUTPUT = dtColumn.NAME;

                        listResult.Add(tempData);
                    }
                    matrixResult.Add(listResult);

                    //Data Row ........ สร้าง row
                    foreach (ViewQMS_MA_PRODUCT dtRow in sortlistAllProduct)
                    {

                        // theader row
                        listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = dtRow.ID;
                        tempData.PRODUCT_NAME = dtRow.NAME;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempData.SHOW_OUTPUT = dtRow.NAME;
                        listResult.Add(tempData);

                        foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                        {
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.PLANT_ID = dtColumn.ID;
                            tempData.PLANT_NAME = getPlantName(listPlant, dtColumn.ID);
                            tempData.RP_OFF_CONTROL_ID = dtRow.ID;
                            tempData.PRODUCT_ID = dtRow.ID;
                            tempData.PRODUCT_NAME = getProductName(listProduct, dtRow.ID);

                            if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                            {
                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 1).FirstOrDefault();
                                ViewQMS_MA_KPI dataKPI = listKPI.Where(m => m.PRODUCT_ID == dtRow.ID && m.PLANT_ID == dtColumn.ID).FirstOrDefault();

                                if (null != dataItem)
                                {
                                    dataItem.SHOW_RANK = getSHOW_RANK(listData, listKPI, dtRow, dtColumn);
                                    dataItem.SHOW_OUTPUT = dataItem.VALUE.ToString("0.##");
                                    tempData = dataItem;

                                }
                            }

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                            listResult.Add(tempData);
                        }

                        matrixResult.Add(listResult);
                    }
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> getTemplateOffControlSummary_T2(QMSDBEntities db, long rp_off_control_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixResult = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            try
            {
                List<ViewQMS_MA_PLANT> listColumn = getQMS_MA_PLANTList(db);
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlId(db, rp_off_control_id);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
                List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);

                List<ViewQMS_MA_PRODUCT> listRow = getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PRODUCT> sortlistAllProduct = getProductHiAndLowList(db);

                if (listRow.Count() > 0 && listColumn.Count() > 0)
                {
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    listResult.Add(tempData);

                    foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.RP_OFF_CONTROL_ID = dtColumn.ID;
                        tempData.PLANT_ID = dtColumn.ID;
                        tempData.PLANT_NAME = dtColumn.NAME;
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                        tempData.SHOW_OUTPUT = dtColumn.NAME;
                        listResult.Add(tempData);
                    }
                    matrixResult.Add(listResult);

                    //Data Row ........ สร้าง row
                    foreach (ViewQMS_MA_PRODUCT dtRow in sortlistAllProduct)
                    {

                        // theader row
                        listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = dtRow.ID;
                        tempData.PRODUCT_NAME = dtRow.NAME;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempData.SHOW_OUTPUT = dtRow.NAME;
                        listResult.Add(tempData);

                        foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                        {
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.PLANT_ID = dtColumn.ID;
                            tempData.PLANT_NAME = getPlantName(listPlant, dtColumn.ID);
                            tempData.RP_OFF_CONTROL_ID = dtRow.ID;
                            tempData.PRODUCT_ID = dtRow.ID;
                            tempData.PRODUCT_NAME = getProductName(listProduct, dtRow.ID);

                            if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                            {
                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem0 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 0 && m.VALUE > 0).FirstOrDefault();

                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem1 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 1 && m.VALUE > 0).FirstOrDefault();

                                ViewQMS_MA_KPI dataKPI = listKPI.Where(m => m.PRODUCT_ID == dtRow.ID && m.PLANT_ID == dtColumn.ID).FirstOrDefault();

                                if (null != dataItem1 && null != dataItem0)
                                {

                                    tempData = dataItem0;
                                    tempData.VALUE = (dataItem0.VALUE / dataItem1.VALUE) * 100;
                                    tempData.SHOW_RANK = getSHOW_RANK(listData, listKPI, dtRow, dtColumn);
                                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("0.##") + "%";

                                }

                            }

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                            listResult.Add(tempData);
                        }

                        matrixResult.Add(listResult);
                    }
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> searchQMS_RP_OFF_CONTROL_SUMMARY(QMSDBEntities db, ReportOffControlSummarySearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> objResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_OFF_CONTROL_SUMMARY> listData = new List<QMS_RP_OFF_CONTROL_SUMMARY>();
                listData = QMS_RP_OFF_CONTROL_SUMMARY.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);

                if (listData.Count() > 0)
                {
                    objResult = (from data in listData
                                 select new ViewQMS_RP_OFF_CONTROL_SUMMARY
                                 {
                                     ID = data.ID,
                                     RP_OFF_CONTROL_ID = data.RP_OFF_CONTROL_ID,
                                     PLANT_ID = data.PLANT_ID,
                                     PLANT_NAME = getPlantName(listPlant, data.PLANT_ID),
                                     PRODUCT_ID = data.PRODUCT_ID,
                                     PRODUCT_NAME = getProductName(listProduct, data.PRODUCT_ID),
                                     VALUE = data.VALUE

                                 }).ToList();

                    if (objResult != null)
                    {
                        objResult = grid.LoadGridData<ViewQMS_RP_OFF_CONTROL_SUMMARY>(objResult.AsQueryable(), out count, out totalPage).ToList();
                        listPageIndex = getPageIndexList(totalPage);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlProductId(QMSDBEntities db, long RPOffControlId, long ProductId)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> list = QMS_RP_OFF_CONTROL_SUMMARY.GetAllistByRPOffControlProductId(db, RPOffControlId, ProductId);
                //List<QMS_MA_CONTROL_COLUMN> listColumn = QMS_MA_CONTROL_COLUMN.GetAllByControlId(db, RPOffControlId);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL_SUMMARY
                                  {
                                      ID = dt.ID,
                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      DATA_TYPE = dt.DATA_TYPE,
                                      VALUE = dt.VALUE

                                      // DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();

                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private List<ViewQMS_RP_OFF_CONTROL_SUMMARY> convertListData(QMSDBEntities db, List<QMS_RP_OFF_CONTROL_SUMMARY> list)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_OFF_CONTROL_SUMMARY
                                  {
                                      ID = dt.ID,

                                      RP_OFF_CONTROL_ID = dt.RP_OFF_CONTROL_ID,
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      DATA_TYPE = dt.DATA_TYPE,
                                      VALUE = dt.VALUE
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getQMS_RP_OFF_CONTROL_SUMMARYList(QMSDBEntities db)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> list = QMS_RP_OFF_CONTROL_SUMMARY.GetAll(db);
                listResult = convertListData(db, list);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getQMS_RP_OFF_CONTROL_SUMMARYByListRPOffControlId(QMSDBEntities db, long[] RPOffControl_ids)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> list = QMS_RP_OFF_CONTROL_SUMMARY.GetByListRPOffControlId(db, RPOffControl_ids);

                listResult = convertListData(db, list);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlId(QMSDBEntities db, long RPOffControl_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> list = QMS_RP_OFF_CONTROL_SUMMARY.GetAllByRPOffControlId(db, RPOffControl_id);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getSumaryOffControlListByRPOffControlId(QMSDBEntities db, long RPOffControl_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> list = QMS_RP_OFF_CONTROL_SUMMARY.GetAllOffControlByRPOffControlId(db, RPOffControl_id);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getSumaryDashOffControlListByRPOffControlId(QMSDBEntities db, List<QMS_RP_OFF_CONTROL> listDataRP)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            //List<QMS_RP_OFF_CONTROL_SUMMARY> list = new List<QMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> listData = new List<QMS_RP_OFF_CONTROL_SUMMARY>();
                foreach (var item in listDataRP)
                {
                    List<QMS_RP_OFF_CONTROL_SUMMARY> data = QMS_RP_OFF_CONTROL_SUMMARY.GetAllOffControlByDateAutoGen(db, item.ID);
                    listData.AddRange(data);
                }

                if (null != listData && listData.Count() > 0)
                {
                    listResult = convertListData(db, listData);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_DETAIL> getCompositionListByListRPOffControlId(QMSDBEntities db, List<QMS_RP_OFF_CONTROL> listDataRP, DashOffControlCalAutoSearchModel searchModel)
        {
            List<ViewQMS_RP_OFF_CONTROL_DETAIL> listResult = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
            //List<QMS_RP_OFF_CONTROL_SUMMARY> list = new List<QMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_DETAIL> listData = new List<QMS_RP_OFF_CONTROL_DETAIL>();
                foreach (var item in listDataRP)
                {
                    List<QMS_RP_OFF_CONTROL_DETAIL> data = QMS_RP_OFF_CONTROL_DETAIL.GetAllBySearchForDash(db, item.ID, searchModel);
                    listData.AddRange(data);
                }

                if (null != listData && listData.Count() > 0)
                {
                    listResult = convertListData(db, listData);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getSumaryInputVolumeListByListRPOffControlId(QMSDBEntities db, List<QMS_RP_OFF_CONTROL> listDataRP)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> listData = new List<QMS_RP_OFF_CONTROL_SUMMARY>();
                foreach (var item in listDataRP)
                {
                    List<QMS_RP_OFF_CONTROL_SUMMARY> data = QMS_RP_OFF_CONTROL_SUMMARY.GetAllInputVolumeByRPOffControlId(db, item.ID);
                    listData.AddRange(data);
                }

                if (null != listData && listData.Count() > 0)
                {
                    listResult = convertListData(db, listData);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getSumaryInputVolumeListByRPOffControlId(QMSDBEntities db, long RPOffControl_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

            try
            {
                List<QMS_RP_OFF_CONTROL_SUMMARY> list = QMS_RP_OFF_CONTROL_SUMMARY.GetAllInputVolumeByRPOffControlId(db, RPOffControl_id);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_RP_OFF_CONTROL_SUMMARY convertModelToDB(CreateQMS_RP_OFF_CONTROL_SUMMARY model)
        {
            QMS_RP_OFF_CONTROL_SUMMARY result = new QMS_RP_OFF_CONTROL_SUMMARY();

            result.ID = model.ID;

            result.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.PLANT_ID = model.PLANT_ID;
            result.DATA_TYPE = model.DATA_TYPE;
            result.VALUE = model.VALUE;

            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_SUMMARYByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_RP_OFF_CONTROL_SUMMARY> result = new List<QMS_RP_OFF_CONTROL_SUMMARY>();
            result = QMS_RP_OFF_CONTROL_SUMMARY.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m => {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_RP_OFF_CONTROL_SUMMARYById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_RP_OFF_CONTROL_SUMMARY result = new QMS_RP_OFF_CONTROL_SUMMARY();
            result = QMS_RP_OFF_CONTROL_SUMMARY.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_RP_OFF_CONTROL_SUMMARY(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_SUMMARY model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_RP_OFF_CONTROL_SUMMARY(db, model);
            }
            else
            {
                result = AddQMS_RP_OFF_CONTROL_SUMMARY(db, convertModelToDB(model));
            }

            return result;
        }

        public long SaveListQMS_RP_OFF_CONTROL_SUMMARY(QMSDBEntities db, List<CreateQMS_RP_OFF_CONTROL_SUMMARY> listModel)
        {
            long result = 0;

            foreach (CreateQMS_RP_OFF_CONTROL_SUMMARY model in listModel)
            {
                result = SaveQMS_RP_OFF_CONTROL_SUMMARY(db, model);
            }

            return result;
        }

        public long UpdateQMS_RP_OFF_CONTROL_SUMMARY(QMSDBEntities db, CreateQMS_RP_OFF_CONTROL_SUMMARY model)
        {
            long result = 0;

            try
            {
                QMS_RP_OFF_CONTROL_SUMMARY dt = new QMS_RP_OFF_CONTROL_SUMMARY();
                dt = QMS_RP_OFF_CONTROL_SUMMARY.GetById(db, model.ID);

                dt.RP_OFF_CONTROL_ID = model.RP_OFF_CONTROL_ID;
                dt.PRODUCT_ID = model.PRODUCT_ID;
                dt.PLANT_ID = model.PLANT_ID;
                dt.DATA_TYPE = model.DATA_TYPE;
                dt.VALUE = model.VALUE;
                //dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_RP_OFF_CONTROL_SUMMARY(QMSDBEntities _db, QMS_RP_OFF_CONTROL_SUMMARY model)
        {
            long result = 0;
            try
            {
                QMS_RP_OFF_CONTROL_SUMMARY dt = new QMS_RP_OFF_CONTROL_SUMMARY();
                dt = QMS_RP_OFF_CONTROL_SUMMARY.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_RP_OFF_CONTROL_SUMMARY.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_PRODUCT_MAPPING> getQMS_MA_PRODUCT_MAPPING_HI(QMSDBEntities db) //HIGHLOW_CHECK == 0
        {
            List<ViewQMS_MA_PRODUCT_MAPPING> listResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            try
            {
                List<QMS_MA_PRODUCT_MAPPING> list = QMS_MA_PRODUCT_MAPPING.GetAllHI(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PRODUCT_MAPPING
                                  {
                                      ID = dt.ID,
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID)

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_PRODUCT_MAPPING> getQMS_MA_PRODUCT_MAPPING_LOW(QMSDBEntities db)
        {
            List<ViewQMS_MA_PRODUCT_MAPPING> listResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            try
            {
                List<QMS_MA_PRODUCT_MAPPING> list = QMS_MA_PRODUCT_MAPPING.GetAllLOW(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PRODUCT_MAPPING
                                  {
                                      ID = dt.ID,
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID)

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        } //HIGHLOW_CHECK == 1

        public byte getSHOW_RANKEx(decimal dtKpiCheck, List<ViewQMS_MA_KPI> listKPI, long plantId, long productId)
        {
            byte bResult = 0;
            try
            {
                ViewQMS_MA_KPI dataKPI = listKPI.Where(m => (m.PRODUCT_ID == productId || m.PRODUCT_ID == productId - 9000) && m.PLANT_ID == plantId).FirstOrDefault();

                if (null != dataKPI)
                {
                    if (dataKPI.START_VALUE_5 <= dtKpiCheck && dtKpiCheck <= dataKPI.END_VALUE_5)
                    {
                        bResult = 5;
                    }
                    if (dataKPI.START_VALUE_4 <= dtKpiCheck && dtKpiCheck <= dataKPI.END_VALUE_4)
                    {
                        bResult = 4;
                    }
                    if (dataKPI.START_VALUE_3 <= dtKpiCheck && dtKpiCheck <= dataKPI.END_VALUE_3)
                    {
                        bResult = 3;
                    }
                    if (dataKPI.START_VALUE_2 <= dtKpiCheck && dtKpiCheck <= dataKPI.END_VALUE_2)
                    {
                        bResult = 2;
                    }
                    if (dataKPI.START_VALUE_1 <= dtKpiCheck && dtKpiCheck <= dataKPI.END_VALUE_1)
                    {
                        bResult = 1;
                    }
                }
            }
            catch
            {

            }

            return bResult;
        }

        public byte getSHOW_RANK(List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData, List<ViewQMS_MA_KPI> listKPI, ViewQMS_MA_PRODUCT dtRow, ViewQMS_MA_PLANT dtColumn)
        {
            byte bResult = 0;
            try
            {
                if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                {
                    ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem0 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 0 && m.VALUE > 0).FirstOrDefault();
                    ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem1 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 1 && m.VALUE > 0).FirstOrDefault();

                    if (null != dataItem1 && null != dataItem0)
                    {
                        decimal dtKpiCheck = (dataItem0.VALUE / dataItem1.VALUE) * 100;
                        bResult = getSHOW_RANKEx(dtKpiCheck, listKPI, dtColumn.ID, dtRow.ID);
                    }
                }
            }
            catch
            {

            }

            return bResult;
        }

        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> getDashboardOffControlSummary_T0(QMSDBEntities db, long rp_off_control_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixResult = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
            List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);

            try
            {
                List<ViewQMS_MA_PRODUCT> listRow = getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PLANT> listColumn = getQMS_MA_PLANTList(db);
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlId(db, rp_off_control_id);

                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);

                List<ViewQMS_MA_PRODUCT> listAllProduct = new List<ViewQMS_MA_PRODUCT>();
                List<ViewQMS_MA_PRODUCT_MAPPING> listFlagHI = getQMS_MA_PRODUCT_MAPPING_HI(db);
                foreach (ViewQMS_MA_PRODUCT dtRowHI in listRow)
                {
                    ViewQMS_MA_PRODUCT tempHi = new ViewQMS_MA_PRODUCT();
                    ViewQMS_MA_PRODUCT_MAPPING dataItem = listFlagHI.Where(m => m.PRODUCT_ID == dtRowHI.ID).FirstOrDefault();
                    if (null != dataItem && dtRowHI.ID > 0)
                    {
                        tempHi.ID = dtRowHI.ID;
                        tempHi.NAME = dtRowHI.NAME;
                        tempHi.POSITION = dtRowHI.POSITION;
                        listAllProduct.Add(tempHi);
                    }

                }
                List<ViewQMS_MA_PRODUCT_MAPPING> listFlagLOW = getQMS_MA_PRODUCT_MAPPING_LOW(db);
                foreach (ViewQMS_MA_PRODUCT dtRowLOW in listRow)
                {
                    ViewQMS_MA_PRODUCT tempLow = new ViewQMS_MA_PRODUCT();
                    ViewQMS_MA_PRODUCT_MAPPING dataItem = listFlagLOW.Where(m => m.PRODUCT_ID == dtRowLOW.ID).FirstOrDefault();
                    if (null != dataItem && dtRowLOW.ID > 0)
                    {
                        for (int i = 0; i < listAllProduct.Count(); i++)
                        {
                            if (dtRowLOW.ID == listAllProduct[i].ID)
                            {
                                listAllProduct[i].NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.HighControl;
                                break;
                            }
                        }

                        //tempLow.ID = dtRowLOW.ID;
                        //tempLow.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.HighControl;
                        //tempLow.POSITION = dtRowLOW.POSITION;
                        //listAllProduct.Add(tempLow);
                        //ViewQMS_MA_PRODUCT tempLow1 = new ViewQMS_MA_PRODUCT();
                        //tempLow1.ID = dtRowLOW.ID + 90000;
                        //tempLow1.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.LowControl;
                        //tempLow1.POSITION = dtRowLOW.POSITION;
                        //listAllProduct.Add(tempLow1);
                    }

                }

                var sortlistAllProduct = listAllProduct.OrderBy(x => x.POSITION).ThenBy(x => x.NAME).ToList();

                decimal NetRow = 0;

                List<NetColumnOffSummaryReport> ListNetColumn = new List<NetColumnOffSummaryReport>();

                if (listRow.Count() > 0 && listColumn.Count() > 0)
                {
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    listResult.Add(tempData);
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.RP_OFF_CONTROL_ID = dtColumn.ID;
                        tempData.PLANT_ID = dtColumn.ID;
                        tempData.PLANT_NAME = dtColumn.NAME;
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                        tempData.SHOW_OUTPUT = dtColumn.NAME;
                        listResult.Add(tempData);
                    }
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = "";
                    tempData.RP_OFF_CONTROL_ID = 0;
                    tempData.PLANT_ID = 0;
                    tempData.PLANT_NAME = QMSSystem.CoreDB.Resource.ResourceString.NetProduct;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.NetProduct;
                    listResult.Add(tempData);
                    matrixResult.Add(listResult);

                    //Data Row ........ สร้าง row
                    foreach (ViewQMS_MA_PRODUCT dtRow in sortlistAllProduct)
                    {

                        // theader row
                        listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = dtRow.ID;
                        tempData.PRODUCT_NAME = dtRow.NAME;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempData.SHOW_OUTPUT = dtRow.NAME;
                        listResult.Add(tempData);

                        foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                        {
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.PLANT_ID = dtColumn.ID;
                            tempData.PLANT_NAME = getPlantName(listPlant, dtColumn.ID);
                            tempData.RP_OFF_CONTROL_ID = dtRow.ID;
                            tempData.PRODUCT_ID = dtRow.ID;
                            tempData.PRODUCT_NAME = getProductName(listProduct, dtRow.ID);
                            tempData.VALUE = 0;
                            tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");

                            if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                            {
                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 0).FirstOrDefault();

                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem0 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 0 && m.VALUE > 0).FirstOrDefault();
                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem1 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 1 && m.VALUE > 0).FirstOrDefault();

                                NetColumnOffSummaryReport temp = new NetColumnOffSummaryReport();
                                ViewQMS_MA_KPI dataKPI = listKPI.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID).FirstOrDefault();

                                temp.ID = tempData.PLANT_ID;
                                temp.VALUE = 0;
                                if (null != dataItem)
                                {
                                    dataItem.SHOW_RANK = getSHOW_RANK(listData, listKPI, dtRow, dtColumn);
                                    dataItem.SHOW_OUTPUT = dataItem.VALUE.ToString("#,##0.#0");
                                    tempData = dataItem;
                                    NetRow = NetRow + tempData.VALUE;
                                    temp.VALUE = tempData.VALUE;
                                }

                                ListNetColumn.Add(temp);
                            }

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                            listResult.Add(tempData);

                        }

                        //Add sum data @ column
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.VALUE = NetRow;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                        tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                        listResult.Add(tempData);

                        //Summary of net value
                        NetColumnOffSummaryReport tempColumn = new NetColumnOffSummaryReport();
                        tempColumn.ID = 0;
                        tempColumn.VALUE = NetRow;

                        ListNetColumn.Add(tempColumn);

                        NetRow = 0;

                        matrixResult.Add(listResult);

                    }

                    //Add Row Sumdata
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    //Total
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.NetGSP;
                    tempData.PLANT_ID = 0;
                    tempData.PLANT_NAME = "";
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.NetGSP;
                    listResult.Add(tempData);

                    List<NetColumnOffSummaryReport> ListSumary = ListNetColumn.GroupBy(m => m.ID)
                      .Select(m => new NetColumnOffSummaryReport()
                      {
                          ID = m.Key,
                          VALUE = m.Sum(d => d.VALUE)
                      }).ToList();
                    ViewQMS_RP_OFF_CONTROL_SUMMARY dataLastItem;
                    foreach (NetColumnOffSummaryReport dtSummary in ListSumary)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.PLANT_ID = 0;
                        tempData.VALUE = dtSummary.VALUE;
                        tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;

                        listResult.Add(tempData);
                        dataLastItem = listResult.LastOrDefault();
                    }

                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    listResult.Remove(dataLastItem = tempData);
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = "";
                    tempData.PLANT_ID = 0;
                    tempData.VALUE = dataLastItem.VALUE;
                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                    tempData.PLANT_NAME = "";
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    listResult.Add(tempData);

                    matrixResult.Add(listResult);
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> getDashboardOffControlSummary_T1(QMSDBEntities db, long rp_off_control_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixResult = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            try
            {
                List<ViewQMS_MA_PRODUCT> listRow = getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PLANT> listColumn = getQMS_MA_PLANTList(db);
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlId(db, rp_off_control_id);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
                List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);

                List<ViewQMS_MA_PRODUCT> listAllProduct = new List<ViewQMS_MA_PRODUCT>();
                List<ViewQMS_MA_PRODUCT_MAPPING> listFlagHI = getQMS_MA_PRODUCT_MAPPING_HI(db);
                foreach (ViewQMS_MA_PRODUCT dtRowHI in listRow)
                {
                    ViewQMS_MA_PRODUCT tempHi = new ViewQMS_MA_PRODUCT();
                    ViewQMS_MA_PRODUCT_MAPPING dataItem = listFlagHI.Where(m => m.PRODUCT_ID == dtRowHI.ID).FirstOrDefault();
                    if (null != dataItem && dtRowHI.ID > 0)
                    {
                        tempHi.ID = dtRowHI.ID;
                        tempHi.NAME = dtRowHI.NAME;
                        tempHi.POSITION = dtRowHI.POSITION;
                        listAllProduct.Add(tempHi);
                    }

                }
                List<ViewQMS_MA_PRODUCT_MAPPING> listFlagLOW = getQMS_MA_PRODUCT_MAPPING_LOW(db);
                foreach (ViewQMS_MA_PRODUCT dtRowLOW in listRow)
                {
                    ViewQMS_MA_PRODUCT tempLow = new ViewQMS_MA_PRODUCT();
                    ViewQMS_MA_PRODUCT_MAPPING dataItem = listFlagLOW.Where(m => m.PRODUCT_ID == dtRowLOW.ID).FirstOrDefault();
                    if (null != dataItem && dtRowLOW.ID > 0)
                    {

                        for (int i = 0; i < listAllProduct.Count(); i++)
                        {
                            if (dtRowLOW.ID == listAllProduct[i].ID)
                            {
                                listAllProduct[i].NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.HighControl;
                                break;
                            }
                        }

                        //tempLow.ID = dtRowLOW.ID;
                        //tempLow.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.HighControl;
                        //tempLow.POSITION = dtRowLOW.POSITION;
                        //listAllProduct.Add(tempLow);
                        //ViewQMS_MA_PRODUCT tempLow1 = new ViewQMS_MA_PRODUCT();
                        //tempLow1.ID = dtRowLOW.ID + 90000;
                        //tempLow1.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.LowControl;
                        //tempLow1.POSITION = dtRowLOW.POSITION;
                        //listAllProduct.Add(tempLow1);
                    }

                }

                var sortlistAllProduct = listAllProduct.OrderBy(x => x.POSITION).ThenBy(x => x.NAME).ToList();

                decimal NetRow = 0;
                List<NetColumnOffSummaryReport> ListNetColumn = new List<NetColumnOffSummaryReport>();

                if (listRow.Count() > 0 && listColumn.Count() > 0)
                {
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    listResult.Add(tempData);

                    foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.RP_OFF_CONTROL_ID = dtColumn.ID;
                        tempData.PLANT_ID = dtColumn.ID;
                        tempData.PLANT_NAME = dtColumn.NAME;
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                        tempData.SHOW_OUTPUT = dtColumn.NAME;
                        listResult.Add(tempData);
                    }
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = "";
                    tempData.RP_OFF_CONTROL_ID = 0;
                    tempData.PLANT_ID = 0;
                    tempData.PLANT_NAME = QMSSystem.CoreDB.Resource.ResourceString.NetProduct;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.NetProduct;
                    listResult.Add(tempData);
                    matrixResult.Add(listResult);

                    //Data Row ........ สร้าง row
                    foreach (ViewQMS_MA_PRODUCT dtRow in sortlistAllProduct)
                    {

                        // theader row
                        listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = dtRow.ID;
                        tempData.PRODUCT_NAME = dtRow.NAME;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempData.SHOW_OUTPUT = dtRow.NAME;
                        listResult.Add(tempData);

                        foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                        {
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.PLANT_ID = dtColumn.ID;
                            tempData.PLANT_NAME = getPlantName(listPlant, dtColumn.ID);
                            tempData.RP_OFF_CONTROL_ID = dtRow.ID;
                            tempData.PRODUCT_ID = dtRow.ID;
                            tempData.PRODUCT_NAME = getProductName(listProduct, dtRow.ID);
                            tempData.VALUE = 0;
                            tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                            if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                            {

                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 1).FirstOrDefault();
                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem0 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 0 && m.VALUE > 0).FirstOrDefault();
                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem1 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 1 && m.VALUE > 0).FirstOrDefault();

                                NetColumnOffSummaryReport temp = new NetColumnOffSummaryReport();
                                ViewQMS_MA_KPI dataKPI = listKPI.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID).FirstOrDefault();

                                temp.ID = tempData.PLANT_ID;
                                temp.VALUE = 0;
                                if (null != dataItem)
                                {
                                    dataItem.SHOW_RANK = getSHOW_RANK(listData, listKPI, dtRow, dtColumn);
                                    dataItem.SHOW_OUTPUT = dataItem.VALUE.ToString("#,##0.#0");
                                    tempData = dataItem;
                                    NetRow = NetRow + tempData.VALUE;
                                    temp.VALUE = tempData.VALUE;

                                }

                                ListNetColumn.Add(temp);
                            }

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                            listResult.Add(tempData);
                        }

                        //Add sum data @ column
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.VALUE = NetRow;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                        tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                        listResult.Add(tempData);

                        //Summary of net value
                        NetColumnOffSummaryReport tempColumn = new NetColumnOffSummaryReport();
                        tempColumn.ID = 0;
                        tempColumn.VALUE = NetRow;
                        ListNetColumn.Add(tempColumn);

                        NetRow = 0;

                        matrixResult.Add(listResult);

                    }

                    //Add Row Sumdata
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    //Total
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.NetGSP;
                    tempData.PLANT_ID = 0;
                    tempData.PLANT_NAME = "";
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.NetGSP;
                    listResult.Add(tempData);

                    List<NetColumnOffSummaryReport> ListSumary = ListNetColumn.GroupBy(m => m.ID)
                      .Select(m => new NetColumnOffSummaryReport()
                      {
                          ID = m.Key,
                          VALUE = m.Sum(d => d.VALUE)
                      }).ToList();

                    ViewQMS_RP_OFF_CONTROL_SUMMARY dataLastItem;
                    foreach (NetColumnOffSummaryReport dtSummary in ListSumary)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.PLANT_ID = 0;
                        tempData.VALUE = dtSummary.VALUE;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                        tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                        listResult.Add(tempData);
                        dataLastItem = listResult.LastOrDefault();
                    }

                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    listResult.Remove(dataLastItem = tempData);
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = "";
                    tempData.PLANT_ID = 0;
                    tempData.VALUE = dataLastItem.VALUE;
                    tempData.PLANT_NAME = "";
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");
                    listResult.Add(tempData);

                    matrixResult.Add(listResult);
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> getDashboardOffControlSummary_T2(QMSDBEntities db, long rp_off_control_id)
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            ViewQMS_RP_OFF_CONTROL_SUMMARY tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> matrixResult = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();

            try
            {
                List<ViewQMS_MA_PRODUCT> listRow = getQMS_MA_PRODUCTList(db);
                List<ViewQMS_MA_PLANT> listColumn = getQMS_MA_PLANTList(db);
                List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlId(db, rp_off_control_id);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
                List<ViewQMS_MA_KPI> listKPI = getQMS_MA_KPIActiveList(db);

                List<ViewQMS_MA_PRODUCT> listAllProduct = new List<ViewQMS_MA_PRODUCT>();
                List<ViewQMS_MA_PRODUCT_MAPPING> listFlagHI = getQMS_MA_PRODUCT_MAPPING_HI(db);
                foreach (ViewQMS_MA_PRODUCT dtRowHI in listRow)
                {
                    ViewQMS_MA_PRODUCT tempHi = new ViewQMS_MA_PRODUCT();
                    ViewQMS_MA_PRODUCT_MAPPING dataItem = listFlagHI.Where(m => m.PRODUCT_ID == dtRowHI.ID).FirstOrDefault();
                    if (null != dataItem && dtRowHI.ID > 0)
                    {
                        tempHi.ID = dtRowHI.ID;
                        tempHi.NAME = dtRowHI.NAME;
                        tempHi.POSITION = dtRowHI.POSITION;
                        listAllProduct.Add(tempHi);
                    }

                }
                List<ViewQMS_MA_PRODUCT_MAPPING> listFlagLOW = getQMS_MA_PRODUCT_MAPPING_LOW(db);
                foreach (ViewQMS_MA_PRODUCT dtRowLOW in listRow)
                {
                    ViewQMS_MA_PRODUCT tempLow = new ViewQMS_MA_PRODUCT();
                    ViewQMS_MA_PRODUCT_MAPPING dataItem = listFlagLOW.Where(m => m.PRODUCT_ID == dtRowLOW.ID).FirstOrDefault();
                    if (null != dataItem && dtRowLOW.ID > 0)
                    {

                        for (int i = 0; i < listAllProduct.Count(); i++)
                        {
                            if (dtRowLOW.ID == listAllProduct[i].ID)
                            {
                                listAllProduct[i].NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.HighControl;
                                break;
                            }
                        }

                        //tempLow.ID = dtRowLOW.ID;
                        //tempLow.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.HighControl;
                        //tempLow.POSITION = dtRowLOW.POSITION;
                        //listAllProduct.Add(tempLow);
                        //ViewQMS_MA_PRODUCT tempLow1 = new ViewQMS_MA_PRODUCT();
                        //tempLow1.ID = dtRowLOW.ID + 90000;
                        //tempLow1.NAME = dtRowLOW.NAME + " " + @Resource.ResourceString.LowControl;
                        //tempLow1.POSITION = dtRowLOW.POSITION;
                        //listAllProduct.Add(tempLow1);
                    }

                }

                var sortlistAllProduct = listAllProduct.OrderBy(x => x.POSITION).ThenBy(x => x.NAME).ToList();

                decimal NetRow = 0;
                decimal countColumn = 0;
                decimal countRow = 0;
                List<NetColumnOffSummaryReport> ListNetColumn = new List<NetColumnOffSummaryReport>();

                if (listRow.Count() > 0 && listColumn.Count() > 0)
                {
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();

                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.ProductPlant;
                    listResult.Add(tempData);

                    foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.RP_OFF_CONTROL_ID = dtColumn.ID;
                        tempData.PLANT_ID = dtColumn.ID;
                        tempData.PLANT_NAME = dtColumn.NAME;
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                        tempData.SHOW_OUTPUT = dtColumn.NAME;
                        listResult.Add(tempData);
                    }
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = "";
                    tempData.RP_OFF_CONTROL_ID = 0;
                    tempData.PLANT_ID = 0;
                    tempData.PLANT_NAME = QMSSystem.CoreDB.Resource.ResourceString.NetProduct;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.NetProduct;
                    listResult.Add(tempData);
                    matrixResult.Add(listResult);

                    //Data Row ........ สร้าง row
                    foreach (ViewQMS_MA_PRODUCT dtRow in sortlistAllProduct)
                    {

                        // theader row
                        listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = dtRow.ID;
                        tempData.PRODUCT_NAME = dtRow.NAME;
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        tempData.SHOW_OUTPUT = dtRow.NAME;
                        listResult.Add(tempData);

                        countColumn = 0;
                        foreach (ViewQMS_MA_PLANT dtColumn in listColumn)
                        {
                            tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                            tempData.PLANT_ID = dtColumn.ID;
                            tempData.PLANT_NAME = getPlantName(listPlant, dtColumn.ID);
                            tempData.RP_OFF_CONTROL_ID = dtRow.ID;
                            tempData.PRODUCT_ID = dtRow.ID;
                            tempData.PRODUCT_NAME = getProductName(listProduct, dtRow.ID);
                            tempData.VALUE = 0;
                            tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0");

                            if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                            {

                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem0 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 0 && m.VALUE > 0).FirstOrDefault();

                                ViewQMS_RP_OFF_CONTROL_SUMMARY dataItem1 = listData.Where(m => (m.PRODUCT_ID == dtRow.ID || m.PRODUCT_ID == dtRow.ID - 9000) && m.PLANT_ID == dtColumn.ID && m.DATA_TYPE == 1 && m.VALUE > 0).FirstOrDefault();

                                NetColumnOffSummaryReport temp = new NetColumnOffSummaryReport();

                                temp.ID = tempData.PLANT_ID;
                                temp.VALUE = 0;
                                if (null != dataItem1 && null != dataItem0)
                                {
                                    dataItem0.SHOW_RANK = 0;
                                    tempData = dataItem1;
                                    tempData.SHOW_RANK = getSHOW_RANK(listData, listKPI, dtRow, dtColumn);
                                    tempData.VALUE = (dataItem0.VALUE / dataItem1.VALUE) * 100;
                                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0") + "%";
                                    NetRow = NetRow + tempData.VALUE;
                                    temp.VALUE = tempData.VALUE;
                                    if (tempData.VALUE > 0) countColumn++;
                                }

                                ListNetColumn.Add(temp);

                            }

                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                            listResult.Add(tempData);
                        }

                        //Add sum data @ column
                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        if (countColumn > 0)
                        {
                            tempData.VALUE = NetRow / countColumn;
                        }
                        else
                        {
                            tempData.VALUE = 0;
                        }
                        tempData.PLANT_ID = 0;
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMROW_HEADER;
                        tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0") + "%";
                        listResult.Add(tempData);

                        //Summary of net value
                        NetColumnOffSummaryReport tempColumn = new NetColumnOffSummaryReport();
                        tempColumn.ID = 0;
                        tempColumn.VALUE = NetRow;
                        ListNetColumn.Add(tempColumn);

                        NetRow = 0;

                        matrixResult.Add(listResult);

                    }

                    //Add Row Sumdata
                    listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();

                    //Total
                    tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = QMSSystem.CoreDB.Resource.ResourceString.NetGSP;
                    tempData.PLANT_ID = 0;
                    tempData.PLANT_NAME = "";
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                    tempData.SHOW_OUTPUT = QMSSystem.CoreDB.Resource.ResourceString.NetGSP;
                    listResult.Add(tempData);

                    List<NetColumnOffSummaryReport> ListSumary = ListNetColumn.GroupBy(m => m.ID)
                      .Select(m => new NetColumnOffSummaryReport()
                      {
                          ID = m.Key,
                          VALUE = m.Sum(d => d.VALUE),
                          COUNT = m.Count(d => d.VALUE > 0)
                      }).ToList();

                    ViewQMS_RP_OFF_CONTROL_SUMMARY dataLastItem;
                    foreach (NetColumnOffSummaryReport dtSummary in ListSumary)
                    {

                        tempData = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        tempData.PRODUCT_ID = 0;
                        tempData.PRODUCT_NAME = "";
                        tempData.PLANT_ID = 0;
                        if (dtSummary.COUNT > 0)
                        {
                            tempData.VALUE = dtSummary.VALUE / dtSummary.COUNT;
                        }
                        else
                        {
                            tempData.VALUE = 0;
                        }
                        tempData.PLANT_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMCOL_HEADER;
                        tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0") + "%";
                        if (tempData.VALUE > 0) countRow++;
                        listResult.Add(tempData);
                        dataLastItem = listResult.LastOrDefault();

                    }

                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    listResult.Remove(dataLastItem = tempData);
                    tempData.PRODUCT_ID = 0;
                    tempData.PRODUCT_NAME = "";
                    tempData.PLANT_ID = 0;
                    tempData.VALUE = dataLastItem.VALUE;
                    tempData.PLANT_NAME = "";
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.SUMALL;
                    tempData.SHOW_OUTPUT = tempData.VALUE.ToString("#,##0.#0") + "%";
                    listResult.Add(tempData);

                    matrixResult.Add(listResult);
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public ExcelPackage OffControlGraphDataExcel(QMSDBEntities db, long rp_off_control_id)
        {
            MemoryStream ms = new MemoryStream();
            //Random t = new Random(5);
            //FileInfo fileInfo = new FileInfo("C:\\test\\" + t.ToString() + ".xlsx");
            ExcelPackage excel = new ExcelPackage();
            var wsData = excel.Workbook.Worksheets.Add("ReportOffControl");

            //get data;
            #region getData
            ReportServices reportService = new ReportServices(_currentUserName);
            CreateQMS_RP_OFF_CONTROL createQMS_RP_OFF_CONTROL = reportService.getQMS_RP_OFF_CONTROLById(db, rp_off_control_id);

            long count = 0;
            long totalPage = 0;
            ReportOffControlGraphSearch model = new ReportOffControlGraphSearch();
            model.mSearch = new ReportOffControlGraphSearchModel();
            model.mSearch.RP_OFF_CONTROL_ID = rp_off_control_id;
            model.PageSize = -1;

            List<ViewQMS_RP_OFF_CONTROL_GRAPH> listData = new List<ViewQMS_RP_OFF_CONTROL_GRAPH>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listData = reportService.searchQMS_RP_OFF_CONTROL_GRAPH(db, model, out count, out totalPage, out listPageIndex);

            #endregion getData

            int iRow = 1;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = @Resource.ResourceString.GraphDetail;
            iRow++;

            //set header 
            wsData.Cells[getchColumnExcel(1) + iRow].Value = @Resource.ResourceString.OffcontrolPrevious;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = @Resource.ResourceString.OffcontrolValue;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = @Resource.ResourceString.OffcontrolDesc;
            iRow++;

            foreach (ViewQMS_RP_OFF_CONTROL_GRAPH reportData in listData)
            {
                wsData.Cells[getchColumnExcel(1) + iRow].Value = reportData.PREVIOUS;
                wsData.Cells[getchColumnExcel(2) + iRow].Value = reportData.OFF_CONTROL;
                wsData.Cells[getchColumnExcel(3) + iRow].Value = reportData.GRAPH_DESC;

                iRow++;
            }

            return excel;
        }

        public List<TEMPLATE_IMP_EXP_DATA> getTemplateDataExport(QMSDBEntities db, int downloadType, long plantId, long productId, out long mappingId)
        {
            List<TEMPLATE_IMP_EXP_DATA> listData = new List<TEMPLATE_IMP_EXP_DATA>();
            mappingId = 0;
            MasterDataService masterService = new MasterDataService(_currentUserName);

            try
            {
                //Step 1. get Map Id. by product and plant 
                QMS_MA_PRODUCT_MAPPING productMapping = masterService.getPlantIdProductId(db, plantId, productId);

                if (downloadType == (byte)TEMPLATE_IMP_EXP_TYPE.PRODUCT)
                {
                    #region product tag data
                    if (null != productMapping)
                    {
                        mappingId = productMapping.ID;

                        List<ViewQMS_MA_EXQ_TAG> listEXQ_TAG_Data = new List<ViewQMS_MA_EXQ_TAG>();

                        //Step 3. Get Exa Data ID 
                        //3.1 TagQuality 
                        List<ViewQMS_MA_EXQ_TAG> listQuality = masterService.getQualityQMS_MA_EXQ_TAGListByProductMapId(db, productMapping.ID);
                        //3.2 Accum
                        List<ViewQMS_MA_EXQ_TAG> listAccum = masterService.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, productMapping.ID);
                        //3.3 Qty
                        List<ViewQMS_MA_EXQ_TAG> listQuantity = masterService.getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, productMapping.ID);

                        if (null != listQuality && listQuality.Count() > 0)
                        {
                            listEXQ_TAG_Data.AddRange(listQuality);
                        }

                        if (null != listAccum && listAccum.Count() > 0)
                        {
                            listEXQ_TAG_Data.AddRange(listAccum);
                        }

                        if (null != listQuantity && listQuantity.Count() > 0)
                        {
                            listEXQ_TAG_Data.AddRange(listQuantity);
                        }

                        if (null != listEXQ_TAG_Data && listEXQ_TAG_Data.Count() > 0)
                        {
                            listData = (from dt in listEXQ_TAG_Data
                                        select new TEMPLATE_IMP_EXP_DATA
                                        {
                                            TAG_ID = dt.ID,
                                            EXCEL_NAME = dt.EXCEL_NAME
                                        }).ToList();
                        }
                    }
                    #endregion
                }
                else if (downloadType == (byte)TEMPLATE_IMP_EXP_TYPE.DOWNTIME)
                {
                    #region tag downtime
                    List<ViewQMS_MA_DOWNTIME_DETAIL> listDownTimeDetail = masterService.getQMS_MA_DOWNTIME_DETAILActiveListByPlantAndActiveDate(db, plantId, DateTime.Now);

                    if (null != listDownTimeDetail && listDownTimeDetail.Count() > 0)
                    {
                        listData = (from dt in listDownTimeDetail
                                    select new TEMPLATE_IMP_EXP_DATA
                                    {
                                        TAG_ID = dt.ID,
                                        EXCEL_NAME = dt.EXA_TAG_NAME
                                    }).ToList();
                    }
                    #endregion
                }
                else
                {
                    #region tag reduce feed
                    List<ViewQMS_MA_REDUCE_FEED_DETAIL> listReduceFeedDetail = masterService.getQMS_MA_REDUCE_FEED_DETAILActiveListByPlantAndActiveDate(db, plantId, DateTime.Now);

                    if (null != listReduceFeedDetail && listReduceFeedDetail.Count() > 0)
                    {
                        listData = (from dt in listReduceFeedDetail
                                    select new TEMPLATE_IMP_EXP_DATA
                                    {
                                        TAG_ID = dt.ID,
                                        EXCEL_NAME = dt.EXA_TAG_NAME
                                    }).ToList();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listData;
        }

        public ExcelPackage TemplateDataExcel(QMSDBEntities db, int downloadType, long plantId, long productId)
        {
            MemoryStream ms = new MemoryStream();
            //Random t = new Random(5);
            //FileInfo fileInfo = new FileInfo("C:\\test\\" + t.ToString() + ".xlsx");
            ExcelPackage excel = new ExcelPackage();
            var wsData = excel.Workbook.Worksheets.Add("Template");
            long mappingId = 0;

            //get data;
            #region getData
            List<TEMPLATE_IMP_EXP_DATA> listData = this.getTemplateDataExport(db, downloadType, plantId, productId, out mappingId);
            #endregion getData

            int iRow = 1;
            int iCol = 1;
            wsData.Cells["A" + iRow].Value = @Resource.ResourceString.TemplateType;
            wsData.Cells["B" + iRow].Value = downloadType;
            wsData.Cells["C" + iRow].Value = @Resource.ResourceString.Plant;
            wsData.Cells["D" + iRow].Value = plantId;
            wsData.Cells["E" + iRow].Value = @Resource.ResourceString.Product;
            wsData.Cells["F" + iRow].Value = ((byte)TEMPLATE_IMP_EXP_TYPE.PRODUCT == downloadType) ? productId : 0;
            wsData.Cells["G" + iRow].Value = @Resource.ResourceString.PlantAndProductMapping;
            wsData.Cells["H" + iRow].Value = ((byte)TEMPLATE_IMP_EXP_TYPE.PRODUCT == downloadType) ? mappingId : 0;

            iRow++;
            wsData.Cells["A" + iRow].Value = @Resource.ResourceString.TagID;
            wsData.Cells["A" + (iRow + 1)].Value = @Resource.ResourceString.ExcelName;
            iCol++;

            foreach (TEMPLATE_IMP_EXP_DATA tagData in listData)
            {
                wsData.Cells[getchColumnExcel(iCol) + iRow].Value = tagData.TAG_ID;
                wsData.Cells[getchColumnExcel(iCol) + (iRow + 1)].Value = tagData.EXCEL_NAME;
                iCol++;
            }

            return excel;
        }

        public ExcelPackage OffControlDataExcel(QMSDBEntities db, long rp_off_control_id)
        {
            MemoryStream ms = new MemoryStream();
            //Random t = new Random(5);
            //FileInfo fileInfo = new FileInfo("C:\\test\\" + t.ToString() + ".xlsx");
            ExcelPackage excel = new ExcelPackage();
            var wsData = excel.Workbook.Worksheets.Add("ReportOffControl");

            //get data;
            #region getData
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData0 = getDashboardOffControlSummary_T0(db, rp_off_control_id);
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData1 = getDashboardOffControlSummary_T1(db, rp_off_control_id);
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData2 = getDashboardOffControlSummary_T2(db, rp_off_control_id);

            #endregion getData

            int iRow = 1;
            int i = 0;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Product Of Control";
            iRow++;
            foreach (List<ViewQMS_RP_OFF_CONTROL_SUMMARY> reportData in listData0)
            {
                i = 0;
                foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dtReport in reportData)
                {

                    for (int ch = 1 + i; ch <= reportData.Count(); ch++)
                    {
                        wsData.Cells[getchColumnExcel(ch) + iRow].Value = dtReport.SHOW_OUTPUT;
                    }

                    i++;
                }
                iRow++;

            }
            iRow = iRow + 2;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total Product";
            iRow++;
            foreach (List<ViewQMS_RP_OFF_CONTROL_SUMMARY> reportData in listData1)
            {
                i = 0;
                foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dtReport in reportData)
                {

                    for (int ch = 1 + i; ch <= reportData.Count(); ch++)
                    {
                        wsData.Cells[getchColumnExcel(ch) + iRow].Value = dtReport.SHOW_OUTPUT;
                    }

                    i++;
                }
                iRow++;

            }
            iRow = iRow + 2;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "% Product Off Control (compare to total production)";
            iRow++;
            foreach (List<ViewQMS_RP_OFF_CONTROL_SUMMARY> reportData in listData2)
            {
                i = 0;
                foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dtReport in reportData)
                {

                    for (int ch = 1 + i; ch <= reportData.Count(); ch++)
                    {
                        wsData.Cells[getchColumnExcel(ch) + iRow].Value = dtReport.SHOW_OUTPUT;
                    }

                    i++;
                }
                iRow++;

            }

            //wsData.Cells["F" + iRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            //wsData.Cells["G" + iRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            return excel;
        }

        public ExcelPackage OffSpecDataExcel(QMSDBEntities db, long rp_off_control_id)
        {
            MemoryStream ms = new MemoryStream();
            //Random t = new Random(5);
            //FileInfo fileInfo = new FileInfo("C:\\test\\" + t.ToString() + ".xlsx");
            ExcelPackage excel = new ExcelPackage();
            var wsData = excel.Workbook.Worksheets.Add("ReportOffSpec");

            //get data;
            #region getData
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData0 = getDashboardOffControlSummary_T0(db, rp_off_control_id);
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData1 = getDashboardOffControlSummary_T1(db, rp_off_control_id);
            List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData2 = getDashboardOffControlSummary_T2(db, rp_off_control_id);

            #endregion getData

            int iRow = 1;
            int i = 0;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Product Of Spec";
            iRow++;
            foreach (List<ViewQMS_RP_OFF_CONTROL_SUMMARY> reportData in listData0)
            {
                i = 0;
                foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dtReport in reportData)
                {

                    for (int ch = 1 + i; ch <= reportData.Count(); ch++)
                    {
                        wsData.Cells[getchColumnExcel(ch) + iRow].Value = dtReport.SHOW_OUTPUT;
                    }

                    i++;
                }
                iRow++;

            }
            iRow = iRow + 2;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total Product";
            iRow++;
            foreach (List<ViewQMS_RP_OFF_CONTROL_SUMMARY> reportData in listData1)
            {
                i = 0;
                foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dtReport in reportData)
                {

                    for (int ch = 1 + i; ch <= reportData.Count(); ch++)
                    {
                        wsData.Cells[getchColumnExcel(ch) + iRow].Value = dtReport.SHOW_OUTPUT;
                    }

                    i++;
                }
                iRow++;

            }
            iRow = iRow + 2;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "% Product Off Spec (compare to total production)";
            iRow++;
            foreach (List<ViewQMS_RP_OFF_CONTROL_SUMMARY> reportData in listData2)
            {
                i = 0;
                foreach (ViewQMS_RP_OFF_CONTROL_SUMMARY dtReport in reportData)
                {

                    for (int ch = 1 + i; ch <= reportData.Count(); ch++)
                    {
                        wsData.Cells[getchColumnExcel(ch) + iRow].Value = dtReport.SHOW_OUTPUT;
                    }

                    i++;
                }
                iRow++;

            }

            //wsData.Cells["F" + iRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            //wsData.Cells["G" + iRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            return excel;
        }

        #endregion

        #region QMS_RP_TREND_DATA

        public List<ViewQMS_RP_TREND> searchQMS_RP_TREND(QMSDBEntities db, ReportTrendSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_RP_TREND> objResult = new List<ViewQMS_RP_TREND>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                List<QMS_MA_CONTROL_COLUMN> list = QMS_MA_CONTROL_COLUMN.GetAll(db);
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_RP_TREND_DATA> listData = new List<QMS_RP_TREND_DATA>();
                List<QMS_MA_UNIT> listUnit = QMS_MA_UNIT.GetAll(db);
                listData = QMS_RP_TREND_DATA.GetAllBySearch(db, searchModel.mSearch);

                if (listData.Count() > 0)
                {
                    objResult = (from data in listData
                                 select new ViewQMS_RP_TREND
                                 {
                                     ID = data.ID,
                                     NAME = data.NAME,
                                     TREND_DESC = data.TREND_DESC,
                                     START_DATE = data.START_DATE,
                                     END_DATE = data.END_DATE,
                                     TREND_TYPE = data.TREND_TYPE,

                                     CREATE_USER = data.CREATE_USER,
                                     UPDATE_USER = data.UPDATE_USER,
                                     SHOW_UPDATE_DATE = data.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                     SHOW_CREATE_DATE = data.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),

                                     //ITEM = "", //
                                     //SAMPLE = "",
                                 }).ToList();

                    if (objResult != null)
                    {
                        objResult = grid.LoadGridData<ViewQMS_RP_TREND>(objResult.AsQueryable(), out count, out totalPage).ToList();
                        listPageIndex = getPageIndexList(totalPage);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_RP_TREND_DATA getQMS_RP_TREND_DATAById(QMSDBEntities db, long id)
        {
            CreateQMS_RP_TREND_DATA objResult = new CreateQMS_RP_TREND_DATA();

            try
            {
                QMS_RP_TREND_DATA dt = new QMS_RP_TREND_DATA();
                dt = QMS_RP_TREND_DATA.GetById(db, id);

                if(dt != null)
                    objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_RP_TREND_DATA> getQMS_RP_TREND_DATAList(QMSDBEntities db)
        {
            List<ViewQMS_RP_TREND_DATA> listResult = new List<ViewQMS_RP_TREND_DATA>();

            try
            {
                List<QMS_RP_TREND_DATA> list = QMS_RP_TREND_DATA.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_TREND_DATA
                                  {
                                      ID = dt.ID,
                                      START_DATE = dt.START_DATE,
                                      END_DATE = dt.END_DATE,
                                      NAME = dt.NAME,
                                      TREND_DESC = dt.TREND_DESC,
                                      TREND_TYPE = dt.TREND_TYPE
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_TREND_DATA_ComboList> getQMS_RP_TREND_DATAComboList(QMSDBEntities db)
        {
            List<ViewQMS_RP_TREND_DATA_ComboList> listResult = new List<ViewQMS_RP_TREND_DATA_ComboList>();

            try
            {
                List<QMS_RP_TREND_DATA> list = QMS_RP_TREND_DATA.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_RP_TREND_DATA_ComboList
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private CreateQMS_RP_TREND_DATA convertDBToModel(QMS_RP_TREND_DATA model)
        {
            CreateQMS_RP_TREND_DATA result = new CreateQMS_RP_TREND_DATA();

            result.ID = model.ID;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.TREND_DESC = (null == model.TREND_DESC) ? "" : model.TREND_DESC;
            result.TREND_TYPE = model.TREND_TYPE;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));

            return result;
        }

        private QMS_RP_TREND_DATA convertModelToDB(CreateQMS_RP_TREND_DATA model)
        {
            QMS_RP_TREND_DATA result = new QMS_RP_TREND_DATA();

            result.ID = model.ID;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.TREND_DESC = (null == model.TREND_DESC) ? "" : model.TREND_DESC;
            result.TREND_TYPE = model.TREND_TYPE;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_RP_TREND_DATAById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_RP_TREND_DATA result = new QMS_RP_TREND_DATA();
            result = QMS_RP_TREND_DATA.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_RP_TREND_DATAByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_RP_TREND_DATA> result = new List<QMS_RP_TREND_DATA>();
            result = QMS_RP_TREND_DATA.GetByListId(db, ids); //.GetById(db, id);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m => {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public SaveResultQMS_RP_TREND SaveQMS_RP_TREND(QMSDBEntities db, CreateQMS_RP_TREND model)
        {
            SaveResultQMS_RP_TREND result = new SaveResultQMS_RP_TREND();

            result.REPORT_MASTER = SaveQMS_RP_TREND_DATA(db, model.REPORT_MASTER);

            if (result.REPORT_MASTER > 0)
            {
                model.REPORT_DETAIL_1.TREND_DATA_ID = result.REPORT_MASTER;
                model.REPORT_DETAIL_2.TREND_DATA_ID = result.REPORT_MASTER;
                result.REPORT_DETAIL_1 = SaveQMS_RP_TREND_DATA_DETAIL(db, model.REPORT_DETAIL_1);
                result.REPORT_DETAIL_2 = SaveQMS_RP_TREND_DATA_DETAIL(db, model.REPORT_DETAIL_2);
            }

            return result;
        }

        public long SaveQMS_RP_TREND_DATA(QMSDBEntities db, CreateQMS_RP_TREND_DATA model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_RP_TREND_DATA(db, model);
            }
            else
            {
                result = AddQMS_RP_TREND_DATA(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_RP_TREND_DATA(QMSDBEntities db, CreateQMS_RP_TREND_DATA model)
        {
            long result = 0;

            try
            {
                QMS_RP_TREND_DATA dt = new QMS_RP_TREND_DATA();
                dt = QMS_RP_TREND_DATA.GetById(db, model.ID);

                dt.ID = model.ID;
                dt.START_DATE = model.START_DATE;
                dt.END_DATE = model.END_DATE;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.TREND_DESC = (null == model.TREND_DESC) ? "" : model.TREND_DESC;
                dt.TREND_TYPE = model.TREND_TYPE;

                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_RP_TREND_DATA(QMSDBEntities _db, QMS_RP_TREND_DATA model)
        {
            long result = 0;
            try
            {
                QMS_RP_TREND_DATA dt = new QMS_RP_TREND_DATA();
                dt = QMS_RP_TREND_DATA.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_RP_TREND_DATA.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_RP_TREND_DATA_DETAIL
        public CreateQMS_RP_TREND_DATA_DETAIL convertViewToModel(ViewQMS_RP_TREND_DATA_DETAIL model)
        {
            CreateQMS_RP_TREND_DATA_DETAIL result = new CreateQMS_RP_TREND_DATA_DETAIL();

            result.ID = model.ID;
            //result.PLANT_ID = model.PLANT_ID;
            result.CONTROL_ID = model.CONTROL_ID;
            result.CUSTOMER_ID = (model.CUSTOMER_ID == null) ? "" : model.CUSTOMER_ID;
            result.DATE_TYPE = model.DATE_TYPE;
            result.ITEM_ID = (model.ITEM_ID == null) ? "" : model.ITEM_ID;
            result.PRODUCT_ID = (model.PRODUCT_ID == null) ? "" : model.PRODUCT_ID;
            result.SAMPLE_ID = (model.SAMPLE_ID == null) ? "" : model.SAMPLE_ID;
            result.TEMPLATE_AREA = (model.TEMPLATE_AREA == null) ? "" : model.TEMPLATE_AREA;
            result.TREND_DATA_ID = model.TREND_DATA_ID;
            result.TANK_ID = (model.TANK_ID == null) ? "" : model.TANK_ID;

            result.TICK_MAX = model.TICK_MAX;
            result.TICK_MIN = model.TICK_MIN;
            result.TICK_STEP = model.TICK_STEP;

            return result;
        }

        private List<ViewQMS_RP_TREND_DATA_DETAIL> convertListData(QMSDBEntities db, List<QMS_RP_TREND_DATA_DETAIL> listData)
        {
            List<ViewQMS_RP_TREND_DATA_DETAIL> listResult = new List<ViewQMS_RP_TREND_DATA_DETAIL>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            List<QMS_MA_ROOT_CAUSE> listRootCause = QMS_MA_ROOT_CAUSE.GetAll(db);
            try
            {
                if (null != listData && listData.Count() > 0)
                {
                    listResult = (from dt in listData
                                  select new ViewQMS_RP_TREND_DATA_DETAIL
                                  {
                                      ID = dt.ID,
                                      CONTROL_ID = dt.CONTROL_ID,
                                      CUSTOMER_ID = dt.CUSTOMER_ID,
                                      DATE_TYPE = dt.DATE_TYPE,
                                      TANK_ID = dt.TANK_ID,
                                      ITEM_ID = dt.ITEM_ID,
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      SAMPLE_ID = dt.SAMPLE_ID,
                                      TEMPLATE_AREA = dt.TEMPLATE_AREA,
                                      TREND_DATA_ID = dt.TREND_DATA_ID,
                                      TICK_MIN = dt.TICK_MIN,
                                      TICK_MAX = dt.TICK_MAX,
                                      TICK_STEP = dt.TICK_STEP
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<ViewQMS_RP_TREND_DATA_DETAIL> getQMS_RP_TREND_DATA_DETAILListByReportId(QMSDBEntities db, long id)
        {
            List<ViewQMS_RP_TREND_DATA_DETAIL> listResult = new List<ViewQMS_RP_TREND_DATA_DETAIL>();

            try
            {
                List<QMS_RP_TREND_DATA_DETAIL> list = QMS_RP_TREND_DATA_DETAIL.GetAllByReportId(db, id);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_RP_TREND_DATA_DETAIL> getQMS_RP_TREND_DATA_DETAILList(QMSDBEntities db)
        {
            List<ViewQMS_RP_TREND_DATA_DETAIL> listResult = new List<ViewQMS_RP_TREND_DATA_DETAIL>();

            try
            {
                List<QMS_RP_TREND_DATA_DETAIL> list = QMS_RP_TREND_DATA_DETAIL.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_RP_TREND_DATA_DETAIL convertModelToDB(CreateQMS_RP_TREND_DATA_DETAIL model)
        {
            QMS_RP_TREND_DATA_DETAIL result = new QMS_RP_TREND_DATA_DETAIL();

            result.ID = model.ID;
            //result.PLANT_ID = model.PLANT_ID;
            result.CONTROL_ID = model.CONTROL_ID;
            result.CUSTOMER_ID = (model.CUSTOMER_ID == null) ? "" : model.CUSTOMER_ID;
            result.DATE_TYPE = model.DATE_TYPE;
            result.ITEM_ID = (model.ITEM_ID == null) ? "" : model.ITEM_ID;
            result.TANK_ID = (model.TANK_ID == null) ? "" : model.TANK_ID;
            result.PRODUCT_ID = (model.PRODUCT_ID == null) ? "" : model.PRODUCT_ID;
            result.SAMPLE_ID = (model.SAMPLE_ID == null) ? "" : model.SAMPLE_ID;
            result.TEMPLATE_AREA = (model.TEMPLATE_AREA == null) ? "" : model.TEMPLATE_AREA;
            result.TREND_DATA_ID = model.TREND_DATA_ID;

            result.TICK_MAX = model.TICK_MAX;
            result.TICK_MIN = model.TICK_MIN;
            result.TICK_STEP = model.TICK_STEP;

            if (getDecimalLenght(model.TICK_MAX.ToString()) > 6)
            {
                result.TICK_MAX = TruncateToDecimalPlace((double)model.TICK_MAX, 6);
            }

            if (getDecimalLenght(model.TICK_MIN.ToString()) > 6)
            {
                result.TICK_MIN = TruncateToDecimalPlace((double)model.TICK_MIN, 6);
            }

            if (getDecimalLenght(model.TICK_STEP.ToString()) > 6)
            {
                result.TICK_STEP = TruncateToDecimalPlace((double)model.TICK_STEP, 6);
            }

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_RP_TREND_DATA_DETAILById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_RP_TREND_DATA_DETAIL result = new QMS_RP_TREND_DATA_DETAIL();
            result = QMS_RP_TREND_DATA_DETAIL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_RP_TREND_DATA_DETAIL(QMSDBEntities db, CreateQMS_RP_TREND_DATA_DETAIL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_RP_TREND_DATA_DETAIL(db, model);
            }
            else
            {
                result = AddQMS_RP_TREND_DATA_DETAIL(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_RP_TREND_DATA_DETAIL(QMSDBEntities db, CreateQMS_RP_TREND_DATA_DETAIL model)
        {
            long result = 0;

            try
            {
                QMS_RP_TREND_DATA_DETAIL dt = new QMS_RP_TREND_DATA_DETAIL();
                dt = QMS_RP_TREND_DATA_DETAIL.GetById(db, model.ID);

                dt.ID = model.ID;
                //dt.PLANT_ID = model.PLANT_ID;
                dt.CONTROL_ID = model.CONTROL_ID;
                dt.CUSTOMER_ID = (model.CUSTOMER_ID == null) ? "" : model.CUSTOMER_ID;
                dt.DATE_TYPE = model.DATE_TYPE;
                dt.ITEM_ID = (model.ITEM_ID == null) ? "" : model.ITEM_ID;
                dt.TANK_ID = (model.TANK_ID == null) ? "" : model.TANK_ID;
                dt.PRODUCT_ID = (model.PRODUCT_ID == null) ? "" : model.PRODUCT_ID;
                dt.SAMPLE_ID = (model.SAMPLE_ID == null) ? "" : model.SAMPLE_ID;
                dt.TEMPLATE_AREA = (model.TEMPLATE_AREA == null) ? "" : model.TEMPLATE_AREA;
                dt.TREND_DATA_ID = model.TREND_DATA_ID;

                dt.TICK_MAX = model.TICK_MAX;
                dt.TICK_MIN = model.TICK_MIN;
                dt.TICK_STEP = model.TICK_STEP;

                if (getDecimalLenght(model.TICK_MAX.ToString()) > 6)
                {
                    dt.TICK_MAX = TruncateToDecimalPlace((double)model.TICK_MAX, 6);
                }

                if (getDecimalLenght(model.TICK_MIN.ToString()) > 6)
                {
                    dt.TICK_MIN = TruncateToDecimalPlace((double)model.TICK_MIN, 6);
                }

                if (getDecimalLenght(model.TICK_STEP.ToString()) > 6)
                {
                    dt.TICK_STEP = TruncateToDecimalPlace((double)model.TICK_STEP, 6);
                }

                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_RP_TREND_DATA_DETAIL(QMSDBEntities _db, QMS_RP_TREND_DATA_DETAIL model)
        {
            long result = 0;
            try
            {
                QMS_RP_TREND_DATA_DETAIL dt = new QMS_RP_TREND_DATA_DETAIL();
                dt = QMS_RP_TREND_DATA_DETAIL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_RP_TREND_DATA_DETAIL.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region Report CorrectData

        public List<VIEW_CORRECT_REPORT> loadEXCEPTION_REPORT(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, List<ViewQMS_TR_EXCEPTION> listData)
        {
            List<VIEW_CORRECT_REPORT> listResult = new List<VIEW_CORRECT_REPORT>();
            MasterDataService masterService = new MasterDataService(_currentUserName);
            try
            {
                if (listData.Count() > 0)
                {
                    foreach (ViewQMS_TR_EXCEPTION dtData in listData)
                    {

                        VIEW_CORRECT_REPORT temp = new VIEW_CORRECT_REPORT();

                        temp.START_DATE = dtData.START_DATE;
                        temp.END_DATE = dtData.END_DATE;
                        temp.PLANT_ID = dtData.PLANT_ID;
                        temp.PLANT_NAME = dtData.PLANT_NAME;
                        temp.PRODUCT_ID = dtData.PRODUCT_ID;
                        temp.PRODUCT_NAME = dtData.PRODUCT_NAME;
                        temp.CORRECT_NAME = @Resource.ResourceString.EXCEPTION;
                        temp.CORRECT_TYPE = (byte)CORRECT_DATA_TYPE.EXCEPTION;

                        temp.SHIFT = masterService.getShiftNameByDateTime(db, listShift, dtData.START_DATE, dtData.END_DATE);
                        temp.DESC = dtData.CAUSE_DESC;
                        temp.VOLUME = dtData.VOLUME;
                        listResult.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<VIEW_CORRECT_REPORT> loadREDUCE_FEED_REPORT(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, List<ViewQMS_TR_REDUCE_FEED> listData)
        {
            List<VIEW_CORRECT_REPORT> listResult = new List<VIEW_CORRECT_REPORT>();
            MasterDataService masterService = new MasterDataService(_currentUserName);
            try
            {
                if (listData.Count() > 0)
                {
                    foreach (ViewQMS_TR_REDUCE_FEED dtData in listData)
                    {

                        VIEW_CORRECT_REPORT temp = new VIEW_CORRECT_REPORT();

                        temp.START_DATE = dtData.START_DATE;
                        temp.END_DATE = dtData.END_DATE;
                        temp.PLANT_ID = dtData.PLANT_ID;
                        temp.PLANT_NAME = dtData.PLANT_NAME;
                        //temp.PRODUCT_ID = 0
                        temp.CORRECT_NAME = @Resource.ResourceString.REDUCE_FEED;
                        temp.CORRECT_TYPE = (byte)CORRECT_DATA_TYPE.REDUCE_FEED;

                        temp.SHIFT = masterService.getShiftNameByDateTime(db, listShift, dtData.START_DATE, dtData.END_DATE);
                        temp.DESC = dtData.CAUSE_DESC;
                        temp.VOLUME = 0;
                        listResult.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<VIEW_CORRECT_REPORT> loadABNORMAL_REPORT(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, List<ViewQMS_TR_ABNORMAL> listData)
        {
            List<VIEW_CORRECT_REPORT> listResult = new List<VIEW_CORRECT_REPORT>();
            MasterDataService masterService = new MasterDataService(_currentUserName);
            try
            {
                if (listData.Count() > 0)
                {
                    foreach (ViewQMS_TR_ABNORMAL dtData in listData)
                    {

                        VIEW_CORRECT_REPORT temp = new VIEW_CORRECT_REPORT();

                        temp.START_DATE = dtData.START_DATE;
                        temp.END_DATE = dtData.END_DATE;
                        temp.PLANT_ID = dtData.PLANT_ID;
                        temp.PLANT_NAME = dtData.PLANT_NAME;
                        temp.PRODUCT_ID = dtData.PRODUCT_ID;
                        temp.PRODUCT_NAME = dtData.PRODUCT_NAME;
                        temp.CORRECT_NAME = @Resource.ResourceString.ABNORMAL;
                        temp.CORRECT_TYPE = (byte)CORRECT_DATA_TYPE.ABNORMAL;

                        temp.SHIFT = masterService.getShiftNameByDateTime(db, listShift, dtData.START_DATE, dtData.END_DATE);
                        temp.DESC = dtData.CAUSE_DESC;
                        temp.VOLUME = dtData.VOLUME;
                        listResult.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        private string getCrossDesc(string szDesc, string productFrom, string productTo)
        {
            string szResult = "";
            try
            {
                if (null != productFrom && "" != productFrom && null != productTo && productTo != "")
                {
                    szResult = "Cross volume from " + productFrom + " to " + productTo + " " + szDesc;
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public List<VIEW_CORRECT_REPORT> loadCROSS_VOLUME_REPORT(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, List<ViewQMS_TR_CROSS_VOLUME> listData)
        {
            List<VIEW_CORRECT_REPORT> listResult = new List<VIEW_CORRECT_REPORT>();
            MasterDataService masterService = new MasterDataService(_currentUserName);
            try
            {
                if (listData.Count() > 0)
                {
                    foreach (ViewQMS_TR_CROSS_VOLUME dtData in listData)
                    {

                        VIEW_CORRECT_REPORT temp = new VIEW_CORRECT_REPORT();

                        temp.START_DATE = dtData.START_DATE;
                        temp.END_DATE = dtData.END_DATE;
                        temp.PLANT_ID = dtData.PLANT_ID;
                        temp.PLANT_NAME = dtData.PLANT_NAME;
                        temp.PRODUCT_ID = dtData.PRODUCT_ID;
                        temp.PRODUCT_NAME = dtData.PRODUCT_NAME;
                        temp.CORRECT_NAME = @Resource.ResourceString.CROSS_VOLUME;
                        temp.CORRECT_TYPE = (byte)CORRECT_DATA_TYPE.CROSS_VOLUME;

                        temp.SHIFT = masterService.getShiftNameByDateTime(db, listShift, dtData.START_DATE, dtData.END_DATE);
                        temp.DESC = getCrossDesc(dtData.CAUSE_DESC, dtData.PRODUCT_FROM_NAME, dtData.PRODUCT_TO_NAME);
                        temp.VOLUME = dtData.VOLUME;
                        listResult.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        private string getChangeGradeDesc(string szDesc, string gradeFrom, string gradeTo)
        {
            string szResult = "";
            try
            {
                if (null != gradeFrom && "" != gradeFrom && null != gradeTo && gradeTo != "")
                {
                    szResult = "Change from " + gradeFrom + " to " + gradeTo + " " + szDesc;
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public List<VIEW_CORRECT_REPORT> loadCHANGE_GRADE_REPORT(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, List<ViewQMS_TR_CHANGE_GRADE> listData)
        {
            List<VIEW_CORRECT_REPORT> listResult = new List<VIEW_CORRECT_REPORT>();
            MasterDataService masterService = new MasterDataService(_currentUserName);
            try
            {
                if (listData.Count() > 0)
                {
                    foreach (ViewQMS_TR_CHANGE_GRADE dtData in listData)
                    {

                        VIEW_CORRECT_REPORT temp = new VIEW_CORRECT_REPORT();

                        temp.START_DATE = dtData.START_DATE;
                        temp.END_DATE = dtData.END_DATE;
                        temp.PLANT_ID = dtData.PLANT_ID;
                        temp.PLANT_NAME = dtData.PLANT_NAME;
                        temp.PRODUCT_ID = dtData.PRODUCT_ID;
                        temp.PRODUCT_NAME = dtData.PRODUCT_NAME;
                        temp.CORRECT_NAME = @Resource.ResourceString.CHANGE_GRADE;
                        temp.CORRECT_TYPE = (byte)CORRECT_DATA_TYPE.CHANGE_GRADE;

                        temp.SHIFT = masterService.getShiftNameByDateTime(db, listShift, dtData.START_DATE, dtData.END_DATE);
                        temp.DESC = getChangeGradeDesc(dtData.CAUSE_DESC, dtData.GRADE_FROM_NAME, dtData.GRADE_TO_NAME);
                        temp.VOLUME = dtData.VOLUME;
                        listResult.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<VIEW_CORRECT_REPORT> loadDOWNTIME_REPORT(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, List<ViewQMS_TR_DOWNTIME> listData)
        {
            List<VIEW_CORRECT_REPORT> listResult = new List<VIEW_CORRECT_REPORT>();
            MasterDataService masterService = new MasterDataService(_currentUserName);
            try
            {
                if (listData.Count() > 0)
                {
                    foreach (ViewQMS_TR_DOWNTIME dtData in listData)
                    {

                        VIEW_CORRECT_REPORT temp = new VIEW_CORRECT_REPORT();

                        temp.START_DATE = dtData.START_DATE;
                        temp.END_DATE = dtData.END_DATE;
                        temp.PLANT_ID = dtData.PLANT_ID;
                        temp.PLANT_NAME = dtData.PLANT_NAME;
                        //temp.PRODUCT_ID = 0
                        temp.CORRECT_NAME = @Resource.ResourceString.DOWN_TIME;
                        temp.CORRECT_TYPE = (byte)CORRECT_DATA_TYPE.DOWN_TIME;

                        temp.SHIFT = masterService.getShiftNameByDateTime(db, listShift, dtData.START_DATE, dtData.END_DATE);
                        temp.DESC = dtData.CAUSE_DESC;
                        temp.VOLUME = 0;
                        listResult.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<VIEW_CORRECT_REPORT> loadCORRECT_REPORT(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, List<ViewQMS_TR_TURN_AROUND> listTurnAroundResult)
        {
            List<VIEW_CORRECT_REPORT> listResult = new List<VIEW_CORRECT_REPORT>();
            MasterDataService masterService = new MasterDataService(_currentUserName);
            try
            {
                if (listTurnAroundResult.Count() > 0)
                {
                    foreach (ViewQMS_TR_TURN_AROUND dtTurnAround in listTurnAroundResult)
                    {

                        VIEW_CORRECT_REPORT temp = new VIEW_CORRECT_REPORT();

                        temp.START_DATE = dtTurnAround.START_DATE;
                        temp.END_DATE = dtTurnAround.END_DATE;
                        temp.PLANT_ID = dtTurnAround.PLANT_ID;
                        temp.PLANT_NAME = dtTurnAround.PLANT_NAME;
                        //temp.PRODUCT_ID = 0
                        temp.CORRECT_NAME = @Resource.ResourceString.TURN_AROUND;
                        temp.CORRECT_TYPE = (byte)CORRECT_DATA_TYPE.TRUN_AROUND;

                        temp.SHIFT = masterService.getShiftNameByDateTime(db, listShift, dtTurnAround.START_DATE, dtTurnAround.END_DATE);
                        temp.DESC = dtTurnAround.CAUSE_DESC;
                        temp.VOLUME = 0;
                        listResult.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public VIEW_CORRECT_SUM_REPORT getCORRECT_REPORTByCorrectDataSearch(QMSDBEntities db, CorrectDataReportSearchModel search)
        {
            MasterDataService masService = new MasterDataService(_currentUserName);
            TransactionService transService = new TransactionService(_currentUserName);
            VIEW_CORRECT_SUM_REPORT listResult = new VIEW_CORRECT_SUM_REPORT();
            List<VIEW_CORRECT_REPORT> listTempResult = new List<VIEW_CORRECT_REPORT>();

            List<ViewQMS_MA_PLANT> listPlant = masService.getQMS_MA_PLANTList(db);

            List<ViewQMS_TR_TURN_AROUND> listTurnAroundResult = new List<ViewQMS_TR_TURN_AROUND>();
            List<ViewQMS_TR_DOWNTIME> listDownTimeResult = new List<ViewQMS_TR_DOWNTIME>();
            List<ViewQMS_TR_CHANGE_GRADE> listChangeGradeResult = new List<ViewQMS_TR_CHANGE_GRADE>();
            List<ViewQMS_TR_CROSS_VOLUME> listCrossVolumeResult = new List<ViewQMS_TR_CROSS_VOLUME>();
            List<ViewQMS_TR_ABNORMAL> listAbnormalResult = new List<ViewQMS_TR_ABNORMAL>();
            List<ViewQMS_TR_REDUCE_FEED> listReduceFeedResult = new List<ViewQMS_TR_REDUCE_FEED>();
            List<ViewQMS_TR_EXCEPTION> listExceptionResult = new List<ViewQMS_TR_EXCEPTION>();

            listResult.listTurnAroundResult = new List<VIEW_CORRECT_REPORT>();
            listResult.listDownTimeResult = new List<VIEW_CORRECT_REPORT>();
            listResult.listChangeGradeResult = new List<VIEW_CORRECT_REPORT>();
            listResult.listCrossVolumeResult = new List<VIEW_CORRECT_REPORT>();
            listResult.listAbnormalResult = new List<VIEW_CORRECT_REPORT>();
            listResult.listReduceFeedResult = new List<VIEW_CORRECT_REPORT>();
            listResult.listExceptionResult = new List<VIEW_CORRECT_REPORT>();

            //reset HH:mm:ss
            search.START_DATE = new DateTime(search.START_DATE.Year, search.START_DATE.Month, search.START_DATE.Day, 0, 0, 0);
            search.END_DATE = new DateTime(search.END_DATE.Year, search.END_DATE.Month, search.END_DATE.Day, 0, 0, 0).AddDays(1).AddMilliseconds(-1);
            search.DOC_STATUS = (byte)REPORT_DOC_STATUS.PUBLIC;

            listResult.header1 = @Resource.ResourceString.ReportOffControl;
            listResult.header2 = @Resource.ResourceString.Plant + " : " + getPlantName(listPlant, search.PLANT_ID) +
              " " + @Resource.ResourceString.FromDate + " " +
              search.START_DATE.ToString("dd/MM/yyyy") + " " +
              @Resource.ResourceString.ToDate + " " + search.END_DATE.ToString("dd/MM/yyyy");

            try
            {
                int startYear = search.START_DATE.Year;
                int endYear = search.END_DATE.Year;
                List<QMS_MA_OPERATION_SHIFT> listShift = masService.getAllByStartEndYear(db, startYear, endYear);

                //Step 1.1 list Turn Around
                List<ViewQMS_TR_TURN_AROUND> listTempTurnAround = transService.getQMS_TR_TURN_AROUNDListByCorrectData(db, search);
                if (listTempTurnAround.Count() > 0)
                {
                    listTempTurnAround = listTempTurnAround.OrderBy(m => m.START_DATE).ToList();
                    listTurnAroundResult.AddRange(listTempTurnAround);
                    listTempResult = loadCORRECT_REPORT(db, listShift, listTurnAroundResult);
                    listResult.listTurnAroundResult = listTempResult;
                }

                //Step 1.2 list Downtime
                List<ViewQMS_TR_DOWNTIME> listTempDowntime = transService.getQMS_TR_DOWNTIMEListByCorrectData(db, search);
                if (listTempDowntime.Count() > 0)
                {
                    listDownTimeResult = listDownTimeResult.OrderBy(m => m.START_DATE).ToList();
                    listDownTimeResult.AddRange(listTempDowntime);
                    listTempResult = loadDOWNTIME_REPORT(db, listShift, listTempDowntime);
                    listResult.listDownTimeResult = listTempResult;
                }

                //Step 1.3 list CHANGE GRADE
                List<ViewQMS_TR_CHANGE_GRADE> listTempCHANGE_GRADE = transService.getQMS_TR_CHANGE_GRADEListByCorrectData(db, search);
                if (listTempCHANGE_GRADE.Count() > 0)
                {
                    listTempCHANGE_GRADE = listTempCHANGE_GRADE.OrderBy(m => m.START_DATE).ToList();
                    listChangeGradeResult.AddRange(listTempCHANGE_GRADE);
                    listTempResult = loadCHANGE_GRADE_REPORT(db, listShift, listTempCHANGE_GRADE);
                    listResult.listChangeGradeResult = listTempResult;
                    listResult.totalVolChangeGrade = listTempResult.Sum(m => m.VOLUME);
                }

                //Step 1.4 list CROSS_VOLUME
                List<ViewQMS_TR_CROSS_VOLUME> listTempCROSS_VOLUME = transService.getQMS_TR_CROSS_VOLUMEListByCorrectData(db, search);
                if (listTempCROSS_VOLUME.Count() > 0)
                {
                    listTempCROSS_VOLUME = listTempCROSS_VOLUME.OrderBy(m => m.START_DATE).ToList();
                    listCrossVolumeResult.AddRange(listTempCROSS_VOLUME);
                    listTempResult = loadCROSS_VOLUME_REPORT(db, listShift, listTempCROSS_VOLUME);
                    listResult.listCrossVolumeResult = listTempResult;
                    listResult.totalVolCross = listTempResult.Sum(m => m.VOLUME);
                }

                //Step 1.5 list Abnormal
                List<ViewQMS_TR_ABNORMAL> listTempAbnormal = transService.getQMS_TR_ABNORMALListByCorrectData(db, search);
                if (listTempAbnormal.Count() > 0)
                {
                    listAbnormalResult = listAbnormalResult.OrderBy(m => m.START_DATE).ToList();
                    listAbnormalResult.AddRange(listTempAbnormal);
                    listTempResult = loadABNORMAL_REPORT(db, listShift, listTempAbnormal);
                    listResult.listAbnormalResult = listTempResult;
                    listResult.totalVolAbnormal = listTempResult.Sum(m => m.VOLUME);
                }

                //Step 1.5 list Reduce Feed
                List<ViewQMS_TR_REDUCE_FEED> listTempReduceFeed = transService.getQMS_TR_REDUCE_FEEDListByCorrectData(db, search);
                if (listTempReduceFeed.Count() > 0)
                {
                    listReduceFeedResult = listReduceFeedResult.OrderBy(m => m.START_DATE).ToList();
                    listReduceFeedResult.AddRange(listTempReduceFeed);
                    listTempResult = loadREDUCE_FEED_REPORT(db, listShift, listTempReduceFeed);
                    listResult.listReduceFeedResult = listTempResult;
                }

                //Step 1.6 list EXCEPTION
                List<ViewQMS_TR_EXCEPTION> listTempEXCEPTION = transService.getQMS_TR_EXCEPTIONListByCorrectData(db, search);
                if (listTempEXCEPTION.Count() > 0)
                {
                    listExceptionResult = listExceptionResult.OrderBy(m => m.START_DATE).ToList();
                    listExceptionResult.AddRange(listTempEXCEPTION);
                    listTempResult = loadEXCEPTION_REPORT(db, listShift, listTempEXCEPTION);
                    listResult.listExceptionResult = listTempResult;
                    listResult.totalVolException = listTempResult.Sum(m => m.VOLUME);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<VIEW_SHIFT_REPORT> getQMS_TR_OFF_CONTROL_CALListByShiftReport(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShift, ShiftAnalysisReportSearchModel searchModel)
        {
            MasterDataService masterService = new MasterDataService(_currentUserName);
            List<VIEW_SHIFT_REPORT> listResult = new List<VIEW_SHIFT_REPORT>();

            try
            {
                List<QMS_TR_OFF_CONTROL_CAL> list = QMS_TR_OFF_CONTROL_CAL.GetByListIdByShiftReport(db, searchModel);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dtData in list
                                  select new VIEW_SHIFT_REPORT
                                  {
                                      START_DATE = dtData.START_DATE,
                                      END_DATE = dtData.END_DATE,
                                      PRODUCT_NAME = getProductName(listProduct, dtData.PRODUCT_ID),
                                      VOLUME = dtData.VOLUME,
                                      SHIFT_NAME = masterService.getShiftNameByDateTime(db, listShift, dtData.START_DATE, dtData.END_DATE)
                                  }).ToList();

                    listResult = listResult.OrderBy(m => m.START_DATE).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public ExcelPackage OffControlCorrrectDataExcel(QMSDBEntities db, VIEW_CORRECT_SUM_REPORT listData)
        {
            MemoryStream ms = new MemoryStream();
            //Random t = new Random(5);
            //FileInfo fileInfo = new FileInfo("C:\\test\\" + t.ToString() + ".xlsx");
            ExcelPackage excel = new ExcelPackage();
            var wsData = excel.Workbook.Worksheets.Add("OffControlCorrectData");

            //get data;
            #region getData

            //VIEW_CORRECT_SUM_REPORT listData

            #endregion getData

            wsData.Column(1).Width = 18;
            wsData.Column(2).Width = 18;
            wsData.Column(3).Width = 10;
            wsData.Column(4).Width = 8;
            wsData.Column(5).Width = 30;
            wsData.Column(6).Width = 15;

            int iRow = 1;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Size = 16;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = listData.header1;
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Size = 16;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = listData.header2;
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Turn Around";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Description";
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(6) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listTurnAroundResult.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_CORRECT_REPORT dtReport in listData.listTurnAroundResult)
                {

                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.SHIFT;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.DESC;
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = listData.totalVolTurnAround;
                    iRow++;
                };
            }
            iRow++;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Downtime";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Description";
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(6) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listDownTimeResult.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_CORRECT_REPORT dtReport in listData.listDownTimeResult)
                {
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.SHIFT;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.DESC;
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = listData.totalVolDownTime;
                    iRow++;
                };
            };
            iRow++;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Change Grade";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Product";
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(4) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Description";
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(6) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listChangeGradeResult.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_CORRECT_REPORT dtReport in listData.listChangeGradeResult)
                {
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.PRODUCT_NAME;
                    wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(4) + iRow].Value = dtReport.SHIFT;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.DESC;
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = listData.totalVolChangeGrade;
                    iRow++;
                };
            };
            iRow++;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Cross Volume";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Product";
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(4) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Description";
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(6) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listCrossVolumeResult.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_CORRECT_REPORT dtReport in listData.listCrossVolumeResult)
                {
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.PRODUCT_NAME;
                    wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(4) + iRow].Value = dtReport.SHIFT;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.DESC;
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = listData.totalVolCross;
                    iRow++;
                };
            };
            iRow++;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Abnormal";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Product";
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(4) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Description";
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(6) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listAbnormalResult.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_CORRECT_REPORT dtReport in listData.listAbnormalResult)
                {
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.PRODUCT_NAME;
                    wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(4) + iRow].Value = dtReport.SHIFT;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.DESC;
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = listData.totalVolAbnormal;
                    iRow++;
                };
            };
            iRow++;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "ReduceFeed";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Description";
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(6) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listReduceFeedResult.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_CORRECT_REPORT dtReport in listData.listReduceFeedResult)
                {
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.SHIFT;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.DESC;
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = listData.totalVolReduceFeed;
                    iRow++;
                };
            };
            iRow++;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Exception";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Product";
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(4) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Description";
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(6) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listExceptionResult.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(6) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_CORRECT_REPORT dtReport in listData.listExceptionResult)
                {

                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.PRODUCT_NAME;
                    wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(4) + iRow].Value = dtReport.SHIFT;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.DESC;
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = "Total";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(6) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(6) + iRow].Value = listData.totalVolException;
                    iRow++;
                };
            };
            iRow++;

            return excel;
        }

        #endregion

        #region Report ShiftAnalysis

        public VIEW_OPERATION_SHIFT_REPORT getOffControlShiftAnalysisBySearch(QMSDBEntities db, ShiftAnalysisReportSearchModel search)
        {
            MasterDataService masService = new MasterDataService(_currentUserName);
            TransactionService transService = new TransactionService(_currentUserName);
            VIEW_OPERATION_SHIFT_REPORT listResult = new VIEW_OPERATION_SHIFT_REPORT();

            List<ViewQMS_MA_PLANT> listPlant = masService.getQMS_MA_PLANTList(db);

            List<ViewQMS_TR_TURN_AROUND> listTurnAroundResult = new List<ViewQMS_TR_TURN_AROUND>();
            List<ViewQMS_TR_DOWNTIME> listDownTimeResult = new List<ViewQMS_TR_DOWNTIME>();
            listResult.listShiftDetail = new List<VIEW_SHIFT_REPORT>();
            listResult.listSummary = new List<VIEW_SHIFT_SUM_REPORT>();

            listResult.header1 = @Resource.ResourceString.ReportOffControl;
            listResult.header2 = @Resource.ResourceString.Plant + " : " + getPlantName(listPlant, search.PLANT_ID) +
              " " + @Resource.ResourceString.FromDate + " " +
              search.START_DATE.ToString("dd/MM/yyyy") + " " +
              @Resource.ResourceString.ToDate + " " + search.END_DATE.ToString("dd/MM/yyyy");

            try
            {
                int startYear = search.START_DATE.Year;
                int endYear = search.END_DATE.Year;
                bool bFound = false;
                List<QMS_MA_OPERATION_SHIFT> listShift = masService.getAllByStartEndYear(db, startYear, endYear);
                List<VIEW_SHIFT_REPORT> listTempShiftReport = getQMS_TR_OFF_CONTROL_CALListByShiftReport(db, listShift, search);

                if (null != listTempShiftReport && listTempShiftReport.Count() > 0)
                {
                    listResult.listShiftDetail = listTempShiftReport;
                    listTempShiftReport = listTempShiftReport.OrderBy(m => m.SHIFT_NAME).OrderBy(m => m.START_DATE).ToList();

                    List<VIEW_SHIFT_SUM_REPORT> listTempSummary = new List<VIEW_SHIFT_SUM_REPORT>();

                    foreach (VIEW_SHIFT_REPORT dtReport in listTempShiftReport)
                    {
                        VIEW_SHIFT_SUM_REPORT temp = new VIEW_SHIFT_SUM_REPORT();

                        if (listTempSummary.Count() > 0)
                        {
                            bFound = false;
                            for (int i = 0; i < listTempSummary.Count(); i++)
                            {
                                if (listTempSummary[i].SHIFT_NAME == dtReport.SHIFT_NAME && listTempSummary[i].PRODUCT_NAME == dtReport.PRODUCT_NAME)
                                {
                                    bFound = true;
                                    listTempSummary[i].VOLUME += dtReport.VOLUME;
                                }
                            }

                            if (false == bFound)
                            {
                                temp.SHIFT_NAME = dtReport.SHIFT_NAME;
                                temp.PRODUCT_NAME = dtReport.PRODUCT_NAME;
                                temp.VOLUME = dtReport.VOLUME;
                                listTempSummary.Add(temp);
                            }

                        }
                        else
                        {
                            temp.SHIFT_NAME = dtReport.SHIFT_NAME;
                            temp.PRODUCT_NAME = dtReport.PRODUCT_NAME;
                            temp.VOLUME = dtReport.VOLUME;
                            listTempSummary.Add(temp);
                        }

                    }

                    listResult.listSummary = listTempSummary;

                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public ExcelPackage OffControlShiftAnalysisExcel(QMSDBEntities db, VIEW_OPERATION_SHIFT_REPORT listData)
        {
            MemoryStream ms = new MemoryStream();
            //Random t = new Random(5);
            //FileInfo fileInfo = new FileInfo("C:\\test\\" + t.ToString() + ".xlsx");
            ExcelPackage excel = new ExcelPackage();
            var wsData = excel.Workbook.Worksheets.Add("OffControlShiftAnalysis");

            //get data;
            #region getData

            //VIEW_CORRECT_SUM_REPORT listData

            #endregion getData

            wsData.Column(1).Width = 18;
            wsData.Column(2).Width = 18;
            wsData.Column(3).Width = 18;
            wsData.Column(4).Width = 18;
            wsData.Column(5).Width = 15;

            int iRow = 1;
            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Size = 16;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = listData.header1;
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Size = 16;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = listData.header2;
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Shift Detail";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Start Date";
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(2) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(2) + iRow].Value = "End Date";
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(4) + iRow].Value = "Product";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listShiftDetail.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_SHIFT_REPORT dtReport in listData.listShiftDetail)
                {

                    wsData.Cells[getchColumnExcel(1) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.START_DATE;
                    wsData.Cells[getchColumnExcel(2) + iRow].Style.Numberformat.Format = "mm/dd/yyyy HH:mm:ss";
                    wsData.Cells[getchColumnExcel(2) + iRow].Value = dtReport.END_DATE;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.SHIFT_NAME;
                    wsData.Cells[getchColumnExcel(4) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(4) + iRow].Value = dtReport.PRODUCT_NAME;
                    wsData.Cells[getchColumnExcel(5) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                };
            }
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Bold = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Summary";
            iRow++;

            wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(2) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(1) + iRow].Value = "Shift";
            wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(3) + iRow].Value = "Product";
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#0080C0"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
            wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            wsData.Cells[getchColumnExcel(5) + iRow].Value = "Volume(TON)";
            iRow++;
            if (listData.listSummary.Count == 0)
            {
                wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(5) + iRow].Merge = true;
                wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                wsData.Cells[getchColumnExcel(1) + iRow].Value = "NoData";
                iRow++;
            }
            else
            {
                foreach (VIEW_SHIFT_SUM_REPORT dtReport in listData.listSummary)
                {
                    wsData.Cells[getchColumnExcel(1) + iRow + ":" + getchColumnExcel(2) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(1) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(1) + iRow].Value = dtReport.SHIFT_NAME;
                    wsData.Cells[getchColumnExcel(3) + iRow + ":" + getchColumnExcel(4) + iRow].Merge = true;
                    wsData.Cells[getchColumnExcel(3) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    wsData.Cells[getchColumnExcel(3) + iRow].Value = dtReport.PRODUCT_NAME;
                    wsData.Cells[getchColumnExcel(5) + iRow].Style.Numberformat.Format = "###,##0.00";
                    wsData.Cells[getchColumnExcel(5) + iRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    wsData.Cells[getchColumnExcel(5) + iRow].Value = dtReport.VOLUME;
                    iRow++;
                };
            };

            return excel;
        }

        #endregion

        public CreateQMS_RP_OFF_CONTROL ApiQMS_RP_OFF_CONTROL(QMSDBEntities db)
        {

            CreateQMS_RP_OFF_CONTROL CreateQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();
            OffControlCalAutoSearchModel OffControlCalAutoSearchModel = new OffControlCalAutoSearchModel();

            ReportOffControlSearch searchModel = new ReportOffControlSearch();
            searchModel.mSearch = new ReportOffControlSearchModel();

            List<QMS_RP_OFF_CONTROL> listResult = new List<QMS_RP_OFF_CONTROL>();

            searchModel.mSearch.OFF_CONTROL_TYPE = 1;


            searchModel.mSearch.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            searchModel.mSearch.END_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            searchModel.mSearch.AUTOGEN_FLAG = 1;
            searchModel.mSearch.SHOW_DASHBOARD = 1;



            //DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); // ปีที่ไม่เป็นปีอธิปไตย
            //DateTime currentDateEND_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59); // ปีที่ไม่เป็นปีอธิปไตย

            //// เพิ่มวันที่ 1
            //DateTime nextDay = currentDate.AddDays(1);
            //DateTime nextDayEnd = currentDateEND_DATE.AddDays(1);

            //searchModel.mSearch.START_DATE = nextDay;
            //searchModel.mSearch.END_DATE = nextDayEnd;
            //searchModel.mSearch.AUTOGEN_FLAG = 1;
            //searchModel.mSearch.SHOW_DASHBOARD = 1;

            listResult = QMS_RP_OFF_CONTROL.GetAllBySearch(db, searchModel.mSearch, searchModel.mSearch.OFF_CONTROL_TYPE);

            if (listResult.Count > 0)
            {
                this.writeErrorLog("Duplicate Generate");
            }
            else
            {

                var DT = DateTime.Now.ToString("yyyy-MM-dd");
                CreateQMS_RP_OFF_CONTROL.NAME = "AUTO_GEN_REPORT_" + DT;
                CreateQMS_RP_OFF_CONTROL.OFF_CONTROL_TYPE = 1;
                CreateQMS_RP_OFF_CONTROL.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                CreateQMS_RP_OFF_CONTROL.END_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                CreateQMS_RP_OFF_CONTROL.DOC_STATUS = 1;
                CreateQMS_RP_OFF_CONTROL.TITLE_DESC = null;
                CreateQMS_RP_OFF_CONTROL.DETAIL_DESC = null;
                CreateQMS_RP_OFF_CONTROL.TITLE_GRAPH = null;
                CreateQMS_RP_OFF_CONTROL.TITLE_GRAPH_X = null;
                CreateQMS_RP_OFF_CONTROL.TITLE_GRAPH_Y = null;
                CreateQMS_RP_OFF_CONTROL.AUTOGEN_FLAG = 1;
                CreateQMS_RP_OFF_CONTROL.SHOW_DASHBOARD = 1;
                CreateQMS_RP_OFF_CONTROL.STATUS_RAW_DATA = 1;

                CreateQMS_RP_OFF_CONTROL.DELETE_FLAG = 0;
                CreateQMS_RP_OFF_CONTROL.CREATE_USER = null;
                CreateQMS_RP_OFF_CONTROL.SHOW_CREATE_DATE = null;
                CreateQMS_RP_OFF_CONTROL.UPDATE_USER = null;
                CreateQMS_RP_OFF_CONTROL.SHOW_UPDATE_DATE = null;
                CreateQMS_RP_OFF_CONTROL.UPDATE_DATE = null;

                long RP_OFF_CONTROL_ID = 0;
                try
                {

                    RP_OFF_CONTROL_ID = SaveQMS_RP_OFF_CONTROL(db, CreateQMS_RP_OFF_CONTROL);

                    if (RP_OFF_CONTROL_ID > 0)
                    {
                        CreateQMS_RP_OFF_CONTROL = getQMS_RP_OFF_CONTROLById(db, RP_OFF_CONTROL_ID);
                    }
                }
                catch (Exception ex)
                {
                    ex.StackTrace.ToString();

                }

                // Create Off Control Summary 
                OffControlCalAutoSearchModel.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                OffControlCalAutoSearchModel.END_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                OffControlCalAutoSearchModel.AUTO_OFF_TYPE = 1;
                OffControlCalAutoSearchModel.CHK_ALL_PLANT = true;
                OffControlCalAutoSearchModel.CHK_ALL_PRODUCT = true;
                OffControlCalAutoSearchModel.CHK_CORRECTDATA = true;
                OffControlCalAutoSearchModel.CHK_RECALRAW = true;
                OffControlCalAutoSearchModel.OFF_TYPE = 1;
                OffControlCalAutoSearchModel.PLANT_ID = 0;
                OffControlCalAutoSearchModel.PRODUCT_ID = 0;
                OffControlCalAutoSearchModel.ROOT_CAUSE_ID = 0;
                OffControlCalAutoSearchModel.RPOffControlId = 0;

                try
                {
                    ViewALL_QMS_RP_OFF_CONTROL_SUMMARY listData = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();

                    if (OffControlCalAutoSearchModel.CHK_RECALRAW)
                    {
                        reCalRawDataByRPOffControlId(db, OffControlCalAutoSearchModel);
                    }

                    listData = getTemplateOffControlSummaryWithAutoCalEx(db, OffControlCalAutoSearchModel);

                    long count = 0;
                    long totalPage = 0;
                    ReportOffControlDetailSearch modelSearch = new ReportOffControlDetailSearch();
                    modelSearch.PageIndex = 1;
                    modelSearch.PageSize = 20; //เอาทั้งหมด
                    modelSearch.SortOrder = "";
                    modelSearch.SortOrder = "PLANT_ID";
                    modelSearch.mSearch = new ReportOffControlDetailSearchModel();
                    modelSearch.mSearch.RP_OFF_CONTROL_ID = OffControlCalAutoSearchModel.RPOffControlId;
                    modelSearch.mSearch.START_DATE = OffControlCalAutoSearchModel.START_DATE;
                    modelSearch.mSearch.END_DATE = OffControlCalAutoSearchModel.END_DATE;

                    //reset plant & product id 
                    OffControlCalAutoSearchModel.PLANT_ID = 0;
                    OffControlCalAutoSearchModel.PRODUCT_ID = 0;

                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listDataDetail = searchQMS_RP_OFF_CONTROL_DETAILEx(db, modelSearch, OffControlCalAutoSearchModel, out count, out totalPage, out listPageIndex);

                    List<CreateQMS_RP_OFF_CONTROL_SUMMARY> listModel = new List<CreateQMS_RP_OFF_CONTROL_SUMMARY>();

                    //save matrixOffControl
                    long pResult = 0;
                    try
                    {

                        int x = listData.matrixOffControl.Count;
                        int i = 0;
                        while (i < x)
                        {
                            if (i == 0)
                            {
                                i++;
                                continue;
                            }
                            int a = listData.matrixOffControl[i].Count;
                            int y = 0;
                            while (y < a)
                            {
                                if (y == 0)
                                {
                                    y++;
                                    continue;
                                }
                                int t = 0;
                                try
                                {
                                    if (decimal.Parse(listData.matrixOffControl[i][y].SHOW_OUTPUT) >= 0 && listData.matrixOffControl[i][y].ID > 0)
                                    {
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY CreateQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.ID = listData.matrixOffControl[i][y].ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.PLANT_ID = listData.matrixOffControl[i][y].PLANT_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.PRODUCT_ID = listData.matrixOffControl[i][y].PRODUCT_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.VALUE = decimal.Parse(listData.matrixOffControl[i][y].SHOW_OUTPUT.Replace(",", ""));
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.DATA_TYPE = 0;
                                        listModel.Add(CreateQMS_RP_OFF_CONTROL_SUMMARY);
                                        t++;
                                    }
                                    y++;
                                }
                                catch (Exception e)
                                {
                                    e.StackTrace.ToString();
                                }

                            }
                            i++;
                        }

                        pResult = SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listModel);
                    }
                    catch (Exception ex)
                    {
                        ex.StackTrace.ToString();
                    }

                    //save matrixInputVolume
                    long uResult = 0;
                    try
                    {

                        int x = listData.matrixInputVolume.Count;
                        int i = 0;
                        while (i < x)
                        {
                            if (i == 0)
                            {
                                i++;
                                continue;
                            }
                            int a = listData.matrixInputVolume[i].Count;
                            int y = 0;
                            while (y < a)
                            {
                                if (y == 0)
                                {
                                    y++;
                                    continue;
                                }
                                int t = 0;
                                try
                                {
                                    if (decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT) >= 0 && listData.matrixInputVolume[i][y].ID > 0)
                                    {
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY CreateQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.ID = listData.matrixInputVolume[i][y].ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.PLANT_ID = listData.matrixInputVolume[i][y].PLANT_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.PRODUCT_ID = listData.matrixInputVolume[i][y].PRODUCT_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.VALUE = decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT.Replace(",", ""));
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.DATA_TYPE = 1;
                                        listModel.Add(CreateQMS_RP_OFF_CONTROL_SUMMARY);
                                        t++;
                                    }
                                    y++;
                                }
                                catch (Exception e)
                                {
                                    e.StackTrace.ToString();
                                }

                            }
                            i++;
                        }

                        uResult = SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listModel);
                    }
                    catch (Exception ex)
                    {
                        ex.StackTrace.ToString();
                    }

                    //save matrixInputVolume
                    long qResult = 0;
                    try
                    {

                        int x = listData.matrixInputVolume.Count;
                        int i = 0;
                        while (i < x)
                        {
                            if (i == 0)
                            {
                                i++;
                                continue;
                            }
                            int a = listData.matrixInputVolume[i].Count;
                            int y = 0;
                            while (y < a)
                            {
                                if (y == 0)
                                {
                                    y++;
                                    continue;
                                }
                                int t = 0;
                                try
                                {
                                    if (decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT) >= 0 && listData.matrixInputVolume[i][y].ID > 0)
                                    {
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY CreateQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.ID = listData.matrixInputVolume[i][y].ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.PLANT_ID = listData.matrixInputVolume[i][y].PLANT_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.PRODUCT_ID = listData.matrixInputVolume[i][y].PRODUCT_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.VALUE = decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT.Replace(",", ""));
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                                        CreateQMS_RP_OFF_CONTROL_SUMMARY.DATA_TYPE = 1;
                                        listModel.Add(CreateQMS_RP_OFF_CONTROL_SUMMARY);
                                        t++;
                                    }
                                    y++;
                                }
                                catch (Exception e)
                                {
                                    e.StackTrace.ToString();
                                }

                            }
                            i++;
                        }

                        qResult = SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listModel);
                    }
                    catch (Exception ex)
                    {
                        ex.StackTrace.ToString();
                    }

                    try
                    {
                        //save off comtrol Detail
                        ReportOffControlDetailSearch modelDetailEx = new ReportOffControlDetailSearch();
                        modelDetailEx.PageIndex = 1;
                        modelDetailEx.PageSize = -1;
                        modelDetailEx.SortColumn = "UPDATE_DATE";
                        modelDetailEx.SortOrder = "asc";
                        modelDetailEx.mSearch = new ReportOffControlDetailSearchModel();
                        modelDetailEx.mSearch.AUTO_END_DATE = CreateQMS_RP_OFF_CONTROL.START_DATE;
                        modelDetailEx.mSearch.AUTO_OFF_TYPE = 1;
                        modelDetailEx.mSearch.AUTO_START_DATE = CreateQMS_RP_OFF_CONTROL.END_DATE;
                        modelDetailEx.mSearch.CHK_AUTO_CAL = true;
                        modelDetailEx.mSearch.CONTROL_VALUE = null;
                        modelDetailEx.mSearch.END_DATE = null;
                        modelDetailEx.mSearch.OFF_TYPE = 1;
                        modelDetailEx.mSearch.PLANT_ID = 0;
                        modelDetailEx.mSearch.PRODUCT_ID = 0;
                        modelDetailEx.mSearch.ROOT_CAUSE_ID = 0;
                        modelDetailEx.mSearch.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                        modelDetailEx.mSearch.START_DATE = null;
                        modelDetailEx.mSearch.VOLUME = null;

                        long icount = 0;
                        long itotalPage = 0;
                        List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetailEx = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                        List<PageIndexList> listPageIndexDetailEx = new List<PageIndexList>();

                        if (modelDetailEx.mSearch.CHK_AUTO_CAL == true && null != modelDetailEx.mSearch.AUTO_START_DATE && null != modelDetailEx.mSearch.AUTO_END_DATE)
                        {
                            OffControlCalAutoSearchModel searchOffCalModel = new OffControlCalAutoSearchModel();
                            searchOffCalModel.PLANT_ID = OffControlCalAutoSearchModel.PLANT_ID;
                            searchOffCalModel.PRODUCT_ID = OffControlCalAutoSearchModel.PRODUCT_ID;
                            searchOffCalModel.ROOT_CAUSE_ID = 0;
                            searchOffCalModel.OFF_TYPE = 1;
                            searchOffCalModel.START_DATE = CreateQMS_RP_OFF_CONTROL.START_DATE;
                            searchOffCalModel.END_DATE = CreateQMS_RP_OFF_CONTROL.END_DATE;
                            searchOffCalModel.RPOffControlId = RP_OFF_CONTROL_ID;
                            searchOffCalModel.AUTO_OFF_TYPE = 1;

                            listDataDetailEx = searchQMS_RP_OFF_CONTROL_DETAILEx(db, modelDetailEx, searchOffCalModel, out icount, out itotalPage, out listPageIndexDetailEx);
                        }
                        else
                        {
                            //ข้ามได้เลยไม่มี กรณีนี้
                        }

                        if (null != listDataDetailEx && listDataDetailEx.Count() > 0)
                        {
                            long iResult = SaveQMS_RP_OFF_CONTROL_DETAILEx(db, listDataDetailEx);

                        }

                    }
                    catch (Exception e)
                    {
                        e.StackTrace.ToString();
                    }

                }
                catch (Exception ex)
                {
                    ex.StackTrace.ToString();
                }

            }

            return CreateQMS_RP_OFF_CONTROL;
        }

        public List<QMS_RP_OFF_CONTROL> ApireloadQMS_RP_OFF_CONTROL(QMSDBEntities db)
        {

            CreateQMS_RP_OFF_CONTROL CreateQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();
            OffControlCalAutoSearchModel OffControlCalAutoSearchModel = new OffControlCalAutoSearchModel();

            List<QMS_RP_OFF_CONTROL> listResult = new List<QMS_RP_OFF_CONTROL>();

            ReportOffControlSearch searchModel = new ReportOffControlSearch();
            searchModel.mSearch = new ReportOffControlSearchModel();

            searchModel.mSearch.OFF_CONTROL_TYPE = 1;
            searchModel.mSearch.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            searchModel.mSearch.END_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            searchModel.mSearch.AUTOGEN_FLAG = 1;
            searchModel.mSearch.SHOW_DASHBOARD = 1;
            searchModel.mSearch.STATUS_RAW_DATA = 1;
            LogWriter.WriteErrorLog("START_DATE" + searchModel.mSearch.START_DATE.ToString());
            LogWriter.WriteErrorLog("END_DATE" + searchModel.mSearch.END_DATE.ToString());
            long RP_OFF_CONTROL_ID = 0;
            try
            {

                listResult = QMS_RP_OFF_CONTROL.GetAllBySearch(db, searchModel.mSearch, searchModel.mSearch.OFF_CONTROL_TYPE);
                RP_OFF_CONTROL_ID = listResult[0].ID;

                if (RP_OFF_CONTROL_ID > 0)
                {
                    CreateQMS_RP_OFF_CONTROL = getQMS_RP_OFF_CONTROLById(db, RP_OFF_CONTROL_ID);
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteErrorLog("8766 " + ex.Message);
            }

            // Create Off Control Summary 
            OffControlCalAutoSearchModel.AUTOGEN_FLAG = 3;
            OffControlCalAutoSearchModel.AUTO_OFF_TYPE = 1;
            OffControlCalAutoSearchModel.CHK_ALL_PLANT = true;
            OffControlCalAutoSearchModel.CHK_ALL_PRODUCT = true;
            OffControlCalAutoSearchModel.CHK_CORRECTDATA = true;
            OffControlCalAutoSearchModel.CHK_RECALRAW = true;

            OffControlCalAutoSearchModel.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            OffControlCalAutoSearchModel.END_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            OffControlCalAutoSearchModel.OFF_TYPE = 1;
            OffControlCalAutoSearchModel.PLANT_ID = 0;
            OffControlCalAutoSearchModel.PRODUCT_ID = 0;
            OffControlCalAutoSearchModel.ROOT_CAUSE_ID = 0;
            OffControlCalAutoSearchModel.RPOffControlId = RP_OFF_CONTROL_ID;

            try
            {
                ViewALL_QMS_RP_OFF_CONTROL_SUMMARY listData = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();

                if (OffControlCalAutoSearchModel.CHK_RECALRAW)
                {
                    reCalRawDataByRPOffControlId(db, OffControlCalAutoSearchModel);
                }

                listData = getTemplateOffControlSummaryWithAutoCalEx(db, OffControlCalAutoSearchModel);

                string listDataJson = Newtonsoft.Json.JsonConvert.SerializeObject(listData);
                long count = 0;
                long totalPage = 0;
                ReportOffControlDetailSearch modelSearch = new ReportOffControlDetailSearch();
                modelSearch.PageIndex = 1;
                modelSearch.PageSize = 20; //เอาทั้งหมด
                modelSearch.SortOrder = "";
                modelSearch.SortOrder = "PLANT_ID";
                modelSearch.mSearch = new ReportOffControlDetailSearchModel();
                modelSearch.mSearch.RP_OFF_CONTROL_ID = OffControlCalAutoSearchModel.RPOffControlId;
                modelSearch.mSearch.START_DATE = OffControlCalAutoSearchModel.START_DATE;
                modelSearch.mSearch.END_DATE = OffControlCalAutoSearchModel.END_DATE;

                //reset plant & product id 
                OffControlCalAutoSearchModel.PLANT_ID = 0;
                OffControlCalAutoSearchModel.PRODUCT_ID = 0;

                List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                List<PageIndexList> listPageIndex = new List<PageIndexList>();
                listDataDetail = searchQMS_RP_OFF_CONTROL_DETAILEx(db, modelSearch, OffControlCalAutoSearchModel, out count, out totalPage, out listPageIndex);

                List<CreateQMS_RP_OFF_CONTROL_SUMMARY> listModel = new List<CreateQMS_RP_OFF_CONTROL_SUMMARY>();

                //save matrixOffControl
                long pResult = 0;
                try
                {
                    int x = listData.matrixOffControl.Count;
                    int i = 0;
                    while (i < x)
                    {
                        if (i == 0)
                        {
                            i++;
                            continue;
                        }
                        int a = listData.matrixOffControl[i].Count;
                        int y = 0;
                        while (y < a)
                        {
                            if (y == 0)
                            {
                                y++;
                                continue;
                            }
                            int t = 0;
                            try
                            {

                                if (decimal.Parse(listData.matrixOffControl[i][y].SHOW_OUTPUT) >= 0 && listData.matrixOffControl[i][y].ID > 0)
                                {
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY CreateQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.ID = listData.matrixOffControl[i][y].ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.PLANT_ID = listData.matrixOffControl[i][y].PLANT_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.PRODUCT_ID = listData.matrixOffControl[i][y].PRODUCT_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.VALUE = decimal.Parse(listData.matrixOffControl[i][y].SHOW_OUTPUT.Replace(",", ""));
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.DATA_TYPE = 0;
                                    listModel.Add(CreateQMS_RP_OFF_CONTROL_SUMMARY);
                                    t++;
                                }
                                y++;
                            }
                            catch (Exception e)
                            {
                                LogWriter.WriteErrorLog("8873 " + e.Message);
                            }

                        }
                        i++;
                    }

                    pResult = SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listModel);

                }
                catch (Exception ex)
                {
                    LogWriter.WriteErrorLog("8888 " + ex.Message);
                }

                //save matrixInputVolume
                long uResult = 0;
                try
                {
                    int x = listData.matrixInputVolume.Count;
                    int i = 0;
                    while (i < x)
                    {
                        if (i == 0)
                        {
                            i++;
                            continue;
                        }
                        int a = listData.matrixInputVolume[i].Count;
                        int y = 0;
                        while (y < a)
                        {
                            if (y == 0)
                            {
                                y++;
                                continue;
                            }
                            int t = 0;
                            try
                            {
                                if (decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT) >= 0 && listData.matrixInputVolume[i][y].ID > 0)
                                {
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY CreateQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.ID = listData.matrixInputVolume[i][y].ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.PLANT_ID = listData.matrixInputVolume[i][y].PLANT_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.PRODUCT_ID = listData.matrixInputVolume[i][y].PRODUCT_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.VALUE = decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT.Replace(",", ""));
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.DATA_TYPE = 1;
                                    listModel.Add(CreateQMS_RP_OFF_CONTROL_SUMMARY);
                                    t++;
                                }
                                y++;
                            }
                            catch (Exception e)
                            {
                                LogWriter.WriteErrorLog("8937 " + e.Message);
                            }

                        }
                        i++;
                    }

                    uResult = SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listModel);
                }
                catch (Exception ex)
                {
                    LogWriter.WriteErrorLog("8950 " + ex.Message);
                }

                //save matrixInputVolume
                long qResult = 0;
                try
                {
                    int x = listData.matrixInputVolume.Count;
                    int i = 0;
                    while (i < x)
                    {
                        if (i == 0)
                        {
                            i++;
                            continue;
                        }
                        int a = listData.matrixInputVolume[i].Count;
                        int y = 0;
                        while (y < a)
                        {
                            if (y == 0)
                            {
                                y++;
                                continue;
                            }
                            int t = 0;
                            try
                            {

                                if (decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT) >= 0 && listData.matrixInputVolume[i][y].ID > 0)
                                {
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY CreateQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.ID = listData.matrixInputVolume[i][y].ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.PLANT_ID = listData.matrixInputVolume[i][y].PLANT_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.PRODUCT_ID = listData.matrixInputVolume[i][y].PRODUCT_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.VALUE = decimal.Parse(listData.matrixInputVolume[i][y].SHOW_OUTPUT.Replace(",", ""));
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                                    CreateQMS_RP_OFF_CONTROL_SUMMARY.DATA_TYPE = 1;
                                    listModel.Add(CreateQMS_RP_OFF_CONTROL_SUMMARY);
                                    t++;
                                }
                                y++;
                            }
                            catch (Exception e)
                            {
                                LogWriter.WriteErrorLog("9002 " + e.Message);
                            }

                        }
                        i++;
                    }

                    qResult = SaveListQMS_RP_OFF_CONTROL_SUMMARY(db, listModel);
                }
                catch (Exception ex) { 
                    LogWriter.WriteErrorLog("9015 " + ex.Message);
                }

                try
                {
                    //save off comtrol Detail
                    ReportOffControlDetailSearch modelDetailEx = new ReportOffControlDetailSearch();
                    modelDetailEx.PageIndex = 1;
                    modelDetailEx.PageSize = -1;
                    modelDetailEx.SortColumn = "UPDATE_DATE";
                    modelDetailEx.SortOrder = "asc";
                    modelDetailEx.mSearch = new ReportOffControlDetailSearchModel();
                    modelDetailEx.mSearch.AUTO_END_DATE = CreateQMS_RP_OFF_CONTROL.START_DATE;
                    modelDetailEx.mSearch.AUTO_OFF_TYPE = 1;
                    modelDetailEx.mSearch.AUTO_START_DATE = CreateQMS_RP_OFF_CONTROL.END_DATE;
                    modelDetailEx.mSearch.CHK_AUTO_CAL = true;
                    modelDetailEx.mSearch.CONTROL_VALUE = null;
                    modelDetailEx.mSearch.END_DATE = null;
                    modelDetailEx.mSearch.OFF_TYPE = 1;
                    modelDetailEx.mSearch.PLANT_ID = 0;
                    modelDetailEx.mSearch.PRODUCT_ID = 0;
                    modelDetailEx.mSearch.ROOT_CAUSE_ID = 0;
                    modelDetailEx.mSearch.RP_OFF_CONTROL_ID = RP_OFF_CONTROL_ID;
                    modelDetailEx.mSearch.START_DATE = null;
                    modelDetailEx.mSearch.VOLUME = null;


                    long icount = 0;
                    long itotalPage = 0;
                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetailEx = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndexDetailEx = new List<PageIndexList>();

                    if (modelDetailEx.mSearch.CHK_AUTO_CAL == true && null != modelDetailEx.mSearch.AUTO_START_DATE && null != modelDetailEx.mSearch.AUTO_END_DATE)
                    {
                        OffControlCalAutoSearchModel searchOffCalModel = new OffControlCalAutoSearchModel();
                        searchOffCalModel.PLANT_ID = OffControlCalAutoSearchModel.PLANT_ID;
                        searchOffCalModel.PRODUCT_ID = OffControlCalAutoSearchModel.PRODUCT_ID;
                        searchOffCalModel.ROOT_CAUSE_ID = 0;
                        searchOffCalModel.OFF_TYPE = 1;
                        searchOffCalModel.START_DATE = CreateQMS_RP_OFF_CONTROL.START_DATE;
                        searchOffCalModel.END_DATE = CreateQMS_RP_OFF_CONTROL.END_DATE;
                        searchOffCalModel.RPOffControlId = RP_OFF_CONTROL_ID;
                        searchOffCalModel.AUTO_OFF_TYPE = 1;

                        listDataDetailEx = searchQMS_RP_OFF_CONTROL_DETAILEx(db, modelDetailEx, searchOffCalModel, out icount, out itotalPage, out listPageIndexDetailEx);
                    }
                    else
                    {
                        //ข้ามได้เลยไม่มี กรณีนี้
                    }

                    if (null != listDataDetailEx && listDataDetailEx.Count() > 0)
                    {

                        long iResult = SaveQMS_RP_OFF_CONTROL_DETAILEx(db, listDataDetailEx);

                    }

                }
                catch (Exception e)
                {

                    LogWriter.WriteErrorLog("9084 " + e.Message);
                }

            }
            catch (Exception ex)
            {

                LogWriter.WriteErrorLog("9093 " + ex.Message);
            }

            return listResult;
        }

    }

}