﻿ALTER TABLE [dbo].[QMS_MA_DOWNTIME_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_QMS_MA_ DOWNTIME_DETAIL_QMS_MA_DOWNTIME] FOREIGN KEY([DOWNTIME_ID])
REFERENCES [dbo].[QMS_MA_DOWNTIME] ([ID])
ON DELETE CASCADE


GO
ALTER TABLE [dbo].[QMS_MA_DOWNTIME_DETAIL] CHECK CONSTRAINT [FK_QMS_MA_ DOWNTIME_DETAIL_QMS_MA_DOWNTIME]

