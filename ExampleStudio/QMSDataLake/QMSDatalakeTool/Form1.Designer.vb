﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dpkStart = New System.Windows.Forms.DateTimePicker()
        Me.dpkEnd = New System.Windows.Forms.DateTimePicker()
        Me.btLoadDataLake = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btQmsRpOffControlStamp = New System.Windows.Forms.Button()
        Me.btQmsRpOffControl = New System.Windows.Forms.Button()
        Me.btEventRawData = New System.Windows.Forms.Button()
        Me.btQmsRpOffControlSummary = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnCheckTotalEXQProduct = New System.Windows.Forms.Button()
        Me.lblStatus2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btEventCal = New System.Windows.Forms.Button()
        Me.btAbnormal = New System.Windows.Forms.Button()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker3 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker4 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker5 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker6 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker7 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker8 = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 41)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Start Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 82)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "End Date"
        '
        'dpkStart
        '
        Me.dpkStart.Location = New System.Drawing.Point(105, 41)
        Me.dpkStart.Margin = New System.Windows.Forms.Padding(2)
        Me.dpkStart.Name = "dpkStart"
        Me.dpkStart.Size = New System.Drawing.Size(203, 20)
        Me.dpkStart.TabIndex = 2
        '
        'dpkEnd
        '
        Me.dpkEnd.Location = New System.Drawing.Point(105, 82)
        Me.dpkEnd.Margin = New System.Windows.Forms.Padding(2)
        Me.dpkEnd.Name = "dpkEnd"
        Me.dpkEnd.Size = New System.Drawing.Size(203, 20)
        Me.dpkEnd.TabIndex = 3
        '
        'btLoadDataLake
        '
        Me.btLoadDataLake.Location = New System.Drawing.Point(28, 177)
        Me.btLoadDataLake.Margin = New System.Windows.Forms.Padding(2)
        Me.btLoadDataLake.Name = "btLoadDataLake"
        Me.btLoadDataLake.Size = New System.Drawing.Size(313, 24)
        Me.btLoadDataLake.TabIndex = 4
        Me.btLoadDataLake.Text = "Load Data Lake"
        Me.btLoadDataLake.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 123)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(81, 123)
        Me.lblStatus.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 6
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btQmsRpOffControlStamp)
        Me.GroupBox1.Controls.Add(Me.btQmsRpOffControl)
        Me.GroupBox1.Controls.Add(Me.btEventRawData)
        Me.GroupBox1.Controls.Add(Me.btQmsRpOffControlSummary)
        Me.GroupBox1.Controls.Add(Me.dpkStart)
        Me.GroupBox1.Controls.Add(Me.lblStatus)
        Me.GroupBox1.Controls.Add(Me.btLoadDataLake)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dpkEnd)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 10)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(373, 419)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Load Data By Date"
        '
        'btQmsRpOffControlStamp
        '
        Me.btQmsRpOffControlStamp.Location = New System.Drawing.Point(29, 367)
        Me.btQmsRpOffControlStamp.Name = "btQmsRpOffControlStamp"
        Me.btQmsRpOffControlStamp.Size = New System.Drawing.Size(312, 24)
        Me.btQmsRpOffControlStamp.TabIndex = 7
        Me.btQmsRpOffControlStamp.Text = "Load QMS_RP_OFF_CONTROL_STAMP To Data Lake"
        Me.btQmsRpOffControlStamp.UseVisualStyleBackColor = True
        '
        'btQmsRpOffControl
        '
        Me.btQmsRpOffControl.Location = New System.Drawing.Point(28, 271)
        Me.btQmsRpOffControl.Margin = New System.Windows.Forms.Padding(2)
        Me.btQmsRpOffControl.Name = "btQmsRpOffControl"
        Me.btQmsRpOffControl.Size = New System.Drawing.Size(313, 24)
        Me.btQmsRpOffControl.TabIndex = 3
        Me.btQmsRpOffControl.Text = "Load QMS_RP_OFF_CONTROL To Data Lake"
        Me.btQmsRpOffControl.UseVisualStyleBackColor = True
        '
        'btEventRawData
        '
        Me.btEventRawData.Location = New System.Drawing.Point(28, 224)
        Me.btEventRawData.Margin = New System.Windows.Forms.Padding(2)
        Me.btEventRawData.Name = "btEventRawData"
        Me.btEventRawData.Size = New System.Drawing.Size(313, 24)
        Me.btEventRawData.TabIndex = 2
        Me.btEventRawData.Text = "Load EVENT_RAW_DATA TO DB"
        Me.btEventRawData.UseVisualStyleBackColor = True
        '
        'btQmsRpOffControlSummary
        '
        Me.btQmsRpOffControlSummary.Location = New System.Drawing.Point(28, 321)
        Me.btQmsRpOffControlSummary.Margin = New System.Windows.Forms.Padding(2)
        Me.btQmsRpOffControlSummary.Name = "btQmsRpOffControlSummary"
        Me.btQmsRpOffControlSummary.Size = New System.Drawing.Size(313, 24)
        Me.btQmsRpOffControlSummary.TabIndex = 2
        Me.btQmsRpOffControlSummary.Text = "Load QMS_RP_OFF_CONTROL_SUMMARY To Data Lake"
        Me.btQmsRpOffControlSummary.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnCheckTotalEXQProduct)
        Me.GroupBox2.Controls.Add(Me.lblStatus2)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.btEventCal)
        Me.GroupBox2.Controls.Add(Me.btAbnormal)
        Me.GroupBox2.Location = New System.Drawing.Point(386, 10)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Size = New System.Drawing.Size(384, 419)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Load Data"
        '
        'btnCheckTotalEXQProduct
        '
        Me.btnCheckTotalEXQProduct.Location = New System.Drawing.Point(33, 224)
        Me.btnCheckTotalEXQProduct.Name = "btnCheckTotalEXQProduct"
        Me.btnCheckTotalEXQProduct.Size = New System.Drawing.Size(313, 23)
        Me.btnCheckTotalEXQProduct.TabIndex = 8
        Me.btnCheckTotalEXQProduct.Text = "Check total in QMS_TR_EXQ_PRODUCT with Start Date"
        Me.btnCheckTotalEXQProduct.UseVisualStyleBackColor = True
        '
        'lblStatus2
        '
        Me.lblStatus2.AutoSize = True
        Me.lblStatus2.Location = New System.Drawing.Point(93, 41)
        Me.lblStatus2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblStatus2.Name = "lblStatus2"
        Me.lblStatus2.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus2.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 41)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Status"
        '
        'btEventCal
        '
        Me.btEventCal.Location = New System.Drawing.Point(33, 146)
        Me.btEventCal.Margin = New System.Windows.Forms.Padding(2)
        Me.btEventCal.Name = "btEventCal"
        Me.btEventCal.Size = New System.Drawing.Size(314, 24)
        Me.btEventCal.TabIndex = 1
        Me.btEventCal.Text = "Load EVENT_CAL To DB"
        Me.btEventCal.UseVisualStyleBackColor = True
        '
        'btAbnormal
        '
        Me.btAbnormal.Location = New System.Drawing.Point(33, 88)
        Me.btAbnormal.Margin = New System.Windows.Forms.Padding(2)
        Me.btAbnormal.Name = "btAbnormal"
        Me.btAbnormal.Size = New System.Drawing.Size(314, 24)
        Me.btAbnormal.TabIndex = 0
        Me.btAbnormal.Text = "Load Abnormal To DB"
        Me.btAbnormal.UseVisualStyleBackColor = True
        '
        'BackgroundWorker2
        '
        Me.BackgroundWorker2.WorkerReportsProgress = True
        Me.BackgroundWorker2.WorkerSupportsCancellation = True
        '
        'BackgroundWorker3
        '
        Me.BackgroundWorker3.WorkerReportsProgress = True
        Me.BackgroundWorker3.WorkerSupportsCancellation = True
        '
        'BackgroundWorker4
        '
        Me.BackgroundWorker4.WorkerReportsProgress = True
        Me.BackgroundWorker4.WorkerSupportsCancellation = True
        '
        'BackgroundWorker5
        '
        Me.BackgroundWorker5.WorkerReportsProgress = True
        Me.BackgroundWorker5.WorkerSupportsCancellation = True
        '
        'BackgroundWorker6
        '
        Me.BackgroundWorker6.WorkerReportsProgress = True
        Me.BackgroundWorker6.WorkerSupportsCancellation = True
        '
        'BackgroundWorker7
        '
        Me.BackgroundWorker7.WorkerReportsProgress = True
        Me.BackgroundWorker7.WorkerSupportsCancellation = True
        '
        'BackgroundWorker8
        '
        Me.BackgroundWorker8.WorkerReportsProgress = True
        Me.BackgroundWorker8.WorkerSupportsCancellation = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 440)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.Text = "Load Data Lake"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dpkStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents dpkEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents btLoadDataLake As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btQmsRpOffControl As System.Windows.Forms.Button
    Friend WithEvents btEventRawData As System.Windows.Forms.Button
    Friend WithEvents btQmsRpOffControlSummary As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btEventCal As System.Windows.Forms.Button
    Friend WithEvents btAbnormal As System.Windows.Forms.Button
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker3 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker4 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblStatus2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker5 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker6 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btQmsRpOffControlStamp As System.Windows.Forms.Button
    Friend WithEvents BackgroundWorker7 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnCheckTotalEXQProduct As System.Windows.Forms.Button
    Friend WithEvents BackgroundWorker8 As System.ComponentModel.BackgroundWorker

End Class
