﻿Imports System.IO
Imports System.Threading
Imports System.Configuration
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Data.EntityClient 
Imports QMSSystem.CoreDB.Services
Imports QMSSystem.Model
Imports Oracle.DataAccess.Client

Public Class QMSTemplateCoAServices
    Private Schedular As Timer
    Private m_nCount As Integer = 3
    Private m_bError As Boolean = False
    Protected Overrides Sub OnStart(ByVal args() As String)
        'System.Diagnostics.Debugger.Launch()
        Me.WriteToFile("Service started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.ScheduleService()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Me.WriteToFile("Service stopped at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.Schedular.Dispose()
    End Sub
    Private Sub SchedularCallback(e As Object)

        Me.WriteToFile("Start : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))


        Try
            Dim activeDate As DateTime = Date.Now
            Dim startDate As DateTime
            Dim endDate As DateTime 'start date

            startDate = New DateTime(activeDate.Year, activeDate.Month, activeDate.Day, 0, 0, 0).AddDays(-1)
            endDate = startDate.AddDays(1).AddMilliseconds(-1)


            'Try
            '    loadCOADataToDB(startDate, endDate)
            'Catch ex As Exception
            '    Me.WriteToFile(ex.Message)
            '    If ex.InnerException IsNot Nothing Then
            '        Me.WriteToFile(ex.InnerException.Message)
            '    End If
            'End Try

            'Threading.Thread.Sleep(5000)
            Try
                loadTemplateFileToDB()
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)

            Try
                ClearAppData()
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

        Catch ex As Exception
            Me.WriteToFile(ex.Message)
        End Try


        Me.WriteToFile("End : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))

        Me.ScheduleService()
    End Sub
    Public Function ClearAppData() As Boolean

        Dim appData As String = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).ToString() & "\Local\Temp"

        If (System.IO.Directory.Exists(appData)) Then
            'System.IO.Directory.Delete(appData)
            For Each DirName As String In Directory.GetDirectories(appData)
                If DirName.Contains("TMP_") Then
                    If (System.IO.Directory.Exists(DirName)) Then
                        Console.WriteLine(DirName)
                        DeleteDirectory(DirName)
                        'System.IO.Directory.Delete(DirName)
                    End If
                End If
                'Console.WriteLine(Dir)
            Next
        End If

        Return True
    End Function


    Private Sub DeleteDirectory(path As String)
        If Directory.Exists(path) Then
            'Delete all files from the Directory
            For Each filepath As String In Directory.GetFiles(path)
                File.Delete(filepath)
            Next
            'Delete all child Directories
            For Each dir As String In Directory.GetDirectories(path)
                DeleteDirectory(dir)
            Next
            'Delete a Directory
            Directory.Delete(path)
        End If
    End Sub
    Public Sub loadTemplateFileToDB()

        Try
            Me.WriteToFile("Start function .. loadTemplateFileToDB")
            Dim transServices As New TransactionService("Service")
            Dim connectionString As String
            'connectionString = GetConnectionString()
            Dim db As New QMSSystem.CoreDB.QMSDBEntities() 'QMSSystem.CoreDB.QMSDBEntities(connectionString)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.GAS)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.Utility)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.Emission)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.WatseWater)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.ObservePonds)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.OilyWaters)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.HotOilFlashPoint)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.HotOilPhysical)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.ClarifiedSystem)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.TwoHundredThousandPond)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.HgInPLMonthly)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.HgStab)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.SulfurInGasMonthly)
            Threading.Thread.Sleep(500)

            transServices.ReadTemplateFileDataToDatabaseByTemplateType(db, TEMPLATE_TYPE.AcidOffGas)
            Threading.Thread.Sleep(500)
        Catch ex As Exception
            Me.WriteToFile("Function loadTemplateFileToDB " & ex.Message)
        End Try
    End Sub
    Public Sub loadCOADataToDB(startDate As DateTime, endDate As DateTime)

        Try
            Me.WriteToFile("Start function .. loadCOADataToDB")
            Dim transServices As New TransactionService("Service")
            'Dim connectionString As String
            'connectionString = GetConnectionString()
            Dim db As New QMSSystem.CoreDB.QMSDBEntities() 'QMSSystem.CoreDB.QMSDBEntities(connectionString)
            Dim coaService = New COAServices(db, "Service")
 
            Dim startTime As DateTime = startDate
            Dim endTime As DateTime = startDate.AddDays(1)

            Do While (startTime <= endDate)
                endTime = startTime.AddDays(1)
                Me.WriteToFile("Start function .. DailyCOAMasterDataCheck")
                coaService.DailyCOAMasterDataCheck(startTime, endTime)
                Me.WriteToFile("Stop function .. DailyCOAMasterDataCheck")
                startTime = startTime.AddDays(1)
            Loop

            'Dim conn As OracleConnection = OracleModule.GetOrcacleDBConnection()

            'conn.Open()
            'Dim SQLcommand As String = OracleModule.getViewDataSQLMaster(startDate, endDate)
            'Dim cmd As OracleCommand = New OracleCommand(SQLcommand, conn)

            'Dim reader As OracleDataReader = cmd.ExecuteReader()
            ''Dim dataTable As New DataTable()
            ''dataTable.Load(reader)

            'Do While reader.Read()
            '    Me.WriteToFile(reader.GetOracleString(1) + " " + reader.GetOracleString(2)) 
            'Loop

            'conn.Close()
        Catch ex As Exception
            Me.WriteToFile("Function loadCOADataToDB " & ex.Message)
        End Try
    End Sub
    Public Sub loadProductQualityToCSC(startDate As DateTime, endDate As DateTime)

        Try
            Me.WriteToFile("Start function .. loadProductQualityToCSC")
            Dim transServices As New TransactionService("Service")
            'Dim connectionString As String
            'connectionString = GetConnectionString()
            Dim db As New QMSSystem.CoreDB.QMSDBEntities() 'QMSSystem.CoreDB.QMSDBEntities(connectionString)
            Dim coaService = New COAServices(db, "Service")

            Dim startTime As DateTime = startDate
            Dim endTime As DateTime = startDate.AddDays(1)

            Do While (startTime <= endDate)
                endTime = startTime.AddDays(1)
                coaService.DailyCOAWriteProductQualityToCSC(startTime, endTime, False)
                startTime = startTime.AddDays(1)
            Loop
             
        Catch ex As Exception
            WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)
        End Try
    End Sub
    Public Function GetConnectionString() As String

        ' Return System.Configuration.ConfigurationManager.ConnectionStrings("QMSDBEntities")

        Dim DBConnection As New SqlConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings("QMSDBEntities").ConnectionString)

        Return DBConnection.ToString()

        '' Specify the provider name, server and database.
        ''Server Test
        ''Dim providerName As String = "System.Data.SqlClient"
        ''Dim serverName As String = "ptt-db-t03.ptt.corp"
        ''Dim databaseName As String = "PTT-GSPQualityManagementSystem_test"
        ''Dim UserID As String = "pttgspqtstusr"
        ''Dim Password As String = "cpttgspqtstusr"
        ''Me.WriteToFile("test")

        ''Server Production
        'Dim providerName As String = "System.Data.SqlClient"
        'Dim serverName As String = "ptt-db-p03.ptt.corp"
        'Dim databaseName As String = "PTT-GSPQualityManagementSystem"
        'Dim UserID As String = "pttgspqprdusr"
        'Dim Password As String = "cpttgspqprdusr"
        'Me.WriteToFile("production")

        '' Initialize the connection string builder for the
        '' underlying provider.
        'Dim sqlBuilder As New SqlConnectionStringBuilder

        '' Set the properties for the data source.
        'sqlBuilder.DataSource = serverName
        'sqlBuilder.InitialCatalog = databaseName
        'sqlBuilder.PersistSecurityInfo = True
        'sqlBuilder.UserID = UserID
        'sqlBuilder.Password = Password


        '' Build the SqlConnection connection string.
        'Dim providerString As String = sqlBuilder.ToString

        '' Initialize the EntityConnectionStringBuilder.
        'Dim entityBuilder As New EntityConnectionStringBuilder

        ''Set the provider name.
        'entityBuilder.Provider = providerName
        '' Set the provider-specific connection string.
        'entityBuilder.ProviderConnectionString = providerString
        '' Set the Metadata location to the current directory.
        'entityBuilder.Metadata = "res://*//QMSSystem.csdl|" & _
        '                            "res://*//QMSSystem.ssdl|" & _
        '                            "res://*//QMSSystem.msl"

        'Return entityBuilder.ToString()
    End Function
    Private Sub WriteToFile(text As String)
        Dim dirName As String
        Dim path As String
        Try
            dirName = AppDomain.CurrentDomain.BaseDirectory & "\\servicelogs" ' [Assembly].GetExecutingAssembly.Location 'Directory.GetCurrentDirectory()

            If (Not System.IO.Directory.Exists(dirName)) Then
                System.IO.Directory.CreateDirectory(dirName)
            End If

            path = dirName & "\\" & DateTime.Now.ToString("yyyyMMdd") & ".log"

            Using writer As New StreamWriter(path, True)
                writer.WriteLine(text)
                writer.Close()
            End Using
        Catch
        End Try

    End Sub
    Public Sub ScheduleService()
        Try

            'Try

            '    Dim activeDate As DateTime = Date.Now
            '    Dim startDate As DateTime
            '    Dim endDate As DateTime 'start date

            '    startDate = New DateTime(activeDate.Year, activeDate.Month, 11, 0, 0, 0)
            '    endDate = New DateTime(activeDate.Year, activeDate.Month, 14, 0, 0, 0).AddDays(1).AddMilliseconds(-1)

            '    'loadTemplateFileToDB()
            '    loadCOADataToDB(startDate, endDate)

            'Catch ex As Exception
            '    Me.WriteToFile(ex.Message)
            'End Try
            'System.Diagnostics.Debugger.Launch()
            'Dim activeDate As DateTime = Date.Now
            'Dim startDate As DateTime
            'Dim endDate As DateTime 'start date

            'startDate = New DateTime(activeDate.Year, activeDate.Month, activeDate.Day, 0, 0, 0).AddDays(-12)
            'endDate = startDate.AddDays(1).AddMilliseconds(-1)
            'loadTemplateFileToDB()
            'loadCOADataToDB(startDate, endDate)
            'loadProductQualityToCSC(startDate, endDate)

            Schedular = New Timer(New TimerCallback(AddressOf SchedularCallback))
            Dim mode As String = ConfigurationManager.AppSettings("Mode").ToUpper()
            Me.WriteToFile((Convert.ToString("Service Mode: ") & mode) + " {0}")

            m_nCount = 3
            'Set the Default Time.
            Dim scheduledTime As DateTime = DateTime.MinValue

            If mode = "DAILY" Then
                'Get the Scheduled Time from AppSettings.
                'System.Diagnostics.Debugger.Launch()
                scheduledTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings("ScheduledTime"))

                Try
                    Dim connectionString As String
                    Me.WriteToFile("Before get conn")
                    'Dim connectionString As String
                    'connectionString = GetConnectionString()
                    Dim db As New QMSSystem.CoreDB.QMSDBEntities() 'QMSSystem.CoreDB.QMSDBEntities(connectionString)
                    Dim cogService = New ConfigServices("Service")
                    'Dim createLimsConnect = New CreateQMS_ST_LIMS_CONNECT()
                    'Dim createExaConnect = New CreateQMS_ST_EXA_CONNECT()
                    'createLimsConnect = cogService.getLIMS_CONNECTION(db)
                    'createExaConnect = cogService.getEXA_SCHEDULE(db)


                    Dim createTemplateConnect = New CreateQMS_ST_TEMPLATE_CONNECT()
                    createTemplateConnect = cogService.getTEMP_SCHEDULE(db)
                    Me.WriteToFile("After get conn")


                    If createTemplateConnect.TEMPLATE_SCHEDULE.HasValue Then
                        Me.WriteToFile(createTemplateConnect.TEMPLATE_SCHEDULE.Value.ToString())
                        scheduledTime = New DateTime(scheduledTime.Year, scheduledTime.Month, scheduledTime.Day, createTemplateConnect.TEMPLATE_SCHEDULE.Value.Hour, createTemplateConnect.TEMPLATE_SCHEDULE.Value.Minute, 0)
                    End If

                Catch ex As Exception
                    WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)
                End Try

                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next day.
                    scheduledTime = scheduledTime.AddDays(1)
                End If
            End If

            If mode.ToUpper() = "INTERVAL" Then
                'Get the Interval in Minutes from AppSettings.
                Dim intervalMinutes As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))

                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(intervalMinutes)
                End If



            End If

            Dim ScheduledDelayTime As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("ScheduledDelayTime"))

            If (ScheduledDelayTime > 0) Then
                scheduledTime = scheduledTime.AddHours(ScheduledDelayTime)
            End If

            Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
            Dim schedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s) ScheduledDelayTime : {4}", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, ScheduledDelayTime)

            Me.WriteToFile((Convert.ToString("1Service scheduled to run after: ") & schedule) + " {0}")

            'Get the difference in Minutes between the Scheduled and Current Time.
            Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)

            'Change the Timer's Due Time.
            Schedular.Change(dueTime, Timeout.Infinite)
        Catch ex As Exception
            WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)

            'Stop the Windows Service.
            Using serviceController As New System.ServiceProcess.ServiceController("SimpleService")
                serviceController.[Stop]()
            End Using
        End Try
    End Sub
End Class
