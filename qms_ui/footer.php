<footer style="background:#666666;color:white;border-top: 4px solid #98c207;">
  <div class="pull-left">
    <div class="col-md-2">
      <img src="images/qms/logo-footer.png"/ style="width:100%;">

    </div>
    <div class="col-md-10" style="font-size:small;">
      Rayong Gas Separation Plant, PTT Public Company Limited <br>
    555 Sukumvit Road, Mabtaput, Muang, Rayong 21150 Tel: 038-676531-2
    </div>

  </div>
  <div class="pull-right">
    © COPYRIGHT 2016, PTT PUBLIC COMPANY LIMITED.
  </div>
  <div class="clearfix"></div>
</footer>
