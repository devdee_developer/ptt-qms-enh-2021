﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using QMSSystem.CoreDB.Services;
using QMSSystem.CoreDB;
using QMSSystem.Model;
using System.Data.SqlClient;
using QMSDatalakeTool;
namespace LoadDataFromLIMSToQMS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            show_ToolTip_button1();
            show_ToolTip_button2();
            show_ToolTip_button3();
            show_ToolTip_button4();
            show_ToolTip_button5();
        }
        //public string GetConnectionString(){
        //    // Initialize the connection string builder for the
        //   // underlying provider.
        //    SqlConnectionStringBuilder sqlBuilder =new SqlConnectionStringBuilder(); 
        //    // Specify the provider name, server and database.
        //    string providerName  = "System.Data.SqlClient";
        //    string serverName = "ptt-db-t03.ptt.corp";
        //    string databaseName = "PTT-GSPQualityManagementSystem_test";


        //    sqlBuilder.DataSource = serverName;
        //    sqlBuilder.InitialCatalog = databaseName;
        //    sqlBuilder.PersistSecurityInfo = true;
        //    sqlBuilder.UserID = "pttgspqtstusr";
        //    sqlBuilder.Password = "cpttgspqtstusr";


        //    string providerString = sqlBuilder.ToString();

        ////    EntityConnectionStringBuilder entityBuilder =new EntityConnectionStringBuilder(); 

        ////'Set the provider name.
        ////entityBuilder.Provider = providerName
        ////' Set the provider-specific connection string.
        ////entityBuilder.ProviderConnectionString = providerString
        ////' Set the Metadata location to the current directory.
        ////entityBuilder.Metadata = "res://*//QMSSystem.csdl|" & _
        ////                            "res://*//QMSSystem.ssdl|" & _
        ////                            "res://*//QMSSystem.msl"
        //}

        private void show_ToolTip_button1()
        {
            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip1 = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 500;
            toolTip1.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip1.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip1.SetToolTip(this.button1, "โหลดข้อมูล LIMS Data ตามช่วงวันที่");
        }
        private void show_ToolTip_button2()
        {
            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip2 = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip2.AutoPopDelay = 5000;
            toolTip2.InitialDelay = 500;
            toolTip2.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip2.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip2.SetToolTip(this.button2, "โหลดข้อมูล CSC Product ตามช่วงวันที่");
        }
        private void show_ToolTip_button3()
        {
            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip3 = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip3.AutoPopDelay = 5000;
            toolTip3.InitialDelay = 500;
            toolTip3.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip3.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip3.SetToolTip(this.btnCOALake, "โหลดข้อมูล COA ที่เป็น Density ไปยัง Data Lake ตามช่วงวันที่");
        }
        private void show_ToolTip_button4()
        {
            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip4 = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip4.AutoPopDelay = 5000;
            toolTip4.InitialDelay = 500;
            toolTip4.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip4.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip4.SetToolTip(this.btnLoadTrendData, "โหลดข้อมูล Trend Data ตามช่วงวันที่");
        }
        private void show_ToolTip_button5()
        {
            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip5 = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip5.AutoPopDelay = 5000;
            toolTip5.InitialDelay = 500;
            toolTip5.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip5.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip5.SetToolTip(this.btnDataLake, "Open Tool โหลดข้อมูล Data Lake");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //this.Enabled = false;

            this.button1.Enabled = false;
            backgroundWorker1 = new BackgroundWorker();

            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            lblStatus.Text = "0 %";
            if (backgroundWorker1.IsBusy != true)
            {
                backgroundWorker1.RunWorkerAsync();
            }

            //if (dateTimePicker1.Value > dateTimePicker2.Value)
            //{
            //    MessageBox.Show("Start more than end");
            //}
            //else
            //{
            //    QMSDBEntities db = new QMSDBEntities();
            //    COAServices coaService = new COAServices(db, "System");

            //    DateTime startDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            //    DateTime endDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);

            //    DateTime startTime = startDate;
            //    DateTime endTime = startDate.AddDays(1); 

            //    while (startTime <= endDate)
            //    {
            //        endTime = startTime.AddDays(1);
            //        coaService.DailyCOAMasterDataCheck(startTime, endTime);
            //        startTime = startTime.AddDays(1);
            //    }
            //}

            //this.button1.Enabled = true;
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            if (dateTimePicker1.Value > dateTimePicker2.Value)
            {
                MessageBox.Show("Start more than end");

            }
            else
            {
                QMSDBEntities db = new QMSDBEntities();
                COAServices coaService = new COAServices(db, "System");

                DateTime startDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                DateTime endDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);

                Double stepValue = 1;
                Double Total = 0;

                backgroundWorker1.ReportProgress(1);

                var interval = (endDate - startDate).TotalDays + 1;

                if (interval > 0)
                {
                    stepValue = 100 / (interval);
                }
                else
                {
                    stepValue = 100 / 1;
                }

                DateTime startTime = startDate;
                DateTime endTime = startDate.AddDays(1);

                while (startTime <= endDate)
                {
                    endTime = startTime.AddDays(1);
                    coaService.DailyCOAMasterDataCheck(startTime, endTime);
                    startTime = startTime.AddDays(1);

                    Total += stepValue;
                    if (Total > 100)
                    {
                        Total = 100;
                    }
                    backgroundWorker1.ReportProgress(Convert.ToInt32(Total));
                    System.Threading.Thread.Sleep(500);
                }
            }
            backgroundWorker1.ReportProgress(100);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblStatus.Text = (e.ProgressPercentage.ToString() + " %");
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                lblStatus.Text = "Canceled!";
                this.button1.Enabled = true;
            }
            else if (e.Error != null)
            {
                lblStatus.Text = "Error: " + e.Error.Message;
                this.button1.Enabled = true;
            }
            else
            {
                lblStatus.Text = "Done!";
                this.button1.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //this.Enabled = false;

            this.button2.Enabled = false;
            backgroundWorker2 = new BackgroundWorker();

            backgroundWorker2.WorkerReportsProgress = true;
            backgroundWorker2.WorkerSupportsCancellation = true;
            backgroundWorker2.DoWork += new DoWorkEventHandler(backgroundWorker2_DoWork);
            backgroundWorker2.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker2_RunWorkerCompleted);
            backgroundWorker2.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker2_ProgressChanged);
            lblStatus.Text = "0 %";
            if (backgroundWorker2.IsBusy != true)
            {
                backgroundWorker2.RunWorkerAsync();
            }

            //this.button2.Enabled = true;

            //if (dateTimePicker1.Value > dateTimePicker2.Value)
            //{
            //    MessageBox.Show("Start more than end");
            //}
            //else
            //{
            //    QMSDBEntities db = new QMSDBEntities();
            //    COAServices coaService = new COAServices(db, "System");

            //    DateTime startDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            //    DateTime endDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);

            //    DateTime startTime = startDate;
            //    DateTime endTime = startDate.AddDays(1);

            //    while (startTime <= endDate)
            //    {
            //        endTime = startTime.AddDays(1);
            //        coaService.DailyCOAWriteProductQualityToCSC(startTime, endTime, false);
            //        startTime = startTime.AddDays(1);
            //    }
            //}

            //this.Enabled = true;
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            if (dateTimePicker1.Value > dateTimePicker2.Value)
            {
                MessageBox.Show("Start more than end");
            }
            else
            {
                QMSDBEntities db = new QMSDBEntities();
                COAServices coaService = new COAServices(db, "System");

                DateTime startDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                DateTime endDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);

                Double stepValue = 1;
                Double Total = 0;

                backgroundWorker2.ReportProgress(1);

                var interval = (endDate - startDate).TotalDays + 1;

                if (interval > 0)
                {
                    stepValue = 100 / (interval);
                }
                else
                {
                    stepValue = 100 / 1;
                }

                DateTime startTime = startDate;
                DateTime endTime = startDate.AddDays(1);

                while (startTime <= endDate)
                {
                    endTime = startTime.AddDays(1);
                    coaService.DailyCOAWriteProductQualityToCSC(startTime, endTime, true);
                    startTime = startTime.AddDays(1);

                    Total += stepValue;
                    if (Total > 100)
                    {
                        Total = 100;
                    }
                    backgroundWorker2.ReportProgress(Convert.ToInt32(Total));
                    System.Threading.Thread.Sleep(500);
                }
            }
            backgroundWorker2.ReportProgress(100);
        }
        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblStatus.Text = (e.ProgressPercentage.ToString() + " %");
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                lblStatus.Text = "Canceled!";
                this.button2.Enabled = true;
            }
            else if (e.Error != null)
            {
                lblStatus.Text = "Error: " + e.Error.Message;
                this.button2.Enabled = true;
            }
            else
            {
                lblStatus.Text = "Done!";
                this.button2.Enabled = true;
            }
        }

        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        { 
            if (dateTimePicker1.Value > dateTimePicker2.Value)
            {
                MessageBox.Show("Start more than end");
            }
            else
            {
                QMSDBEntities db = new QMSDBEntities();
                TransactionService coaService = new TransactionService( "System");

                DateTime startDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                DateTime endDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);

                Double stepValue = 1;
                Double Total = 0;

                backgroundWorker3.ReportProgress(1);

                var interval = (endDate - startDate).TotalDays + 1;

                if (interval > 0)
                {
                    stepValue = 100 / (interval);
                }
                else
                {
                    stepValue = 100 / 1;
                }

                DateTime startTime = startDate;
                DateTime endTime = startDate.AddDays(1);

                while (startTime <= endDate)
                {
                    endTime = startTime.AddDays(1);
                    coaService.LoadQMS_TR_COA_DATAFromDBTODataLake(db, startTime, endTime);
                    startTime = startTime.AddDays(1);

                    Total += stepValue;
                    if (Total > 100)
                    {
                        Total = 100;
                    }
                    backgroundWorker3.ReportProgress(Convert.ToInt32(Total));
                    System.Threading.Thread.Sleep(500);
                }
            }
            backgroundWorker3.ReportProgress(100); 
        }

        private void btnCOALake_Click(object sender, EventArgs e)
        {

            this.btnCOALake.Enabled = false;
            backgroundWorker3 = new BackgroundWorker();

            backgroundWorker3.WorkerReportsProgress = true;
            backgroundWorker3.WorkerSupportsCancellation = true;
            backgroundWorker3.DoWork += new DoWorkEventHandler(backgroundWorker3_DoWork);
            backgroundWorker3.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker3_RunWorkerCompleted);
            backgroundWorker3.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker3_ProgressChanged);
            lblStatus.Text = "0 %";
            if (backgroundWorker3.IsBusy != true)
            {
                backgroundWorker3.RunWorkerAsync();
            }
        }

        private void backgroundWorker3_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblStatus.Text = (e.ProgressPercentage.ToString() + " %");
        }

        private void backgroundWorker3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                lblStatus.Text = "Canceled!";
                this.btnCOALake.Enabled = true;
            }
            else if (e.Error != null)
            {
                lblStatus.Text = "Error: " + e.Error.Message;
                this.btnCOALake.Enabled = true;
            }
            else
            {
                lblStatus.Text = "Done!";
                this.btnCOALake.Enabled = true;
            }
        }

        private void btnLoadTrendData_Click(object sender, EventArgs e)
        {
            this.btnLoadTrendData.Enabled = false;
            backgroundWorker4 = new BackgroundWorker();

            backgroundWorker4.WorkerReportsProgress = true;
            backgroundWorker4.WorkerSupportsCancellation = true;
            backgroundWorker4.DoWork += new DoWorkEventHandler(backgroundWorker4_DoWork);
            backgroundWorker4.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker4_RunWorkerCompleted);
            backgroundWorker4.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker4_ProgressChanged);
            lblStatus.Text = "0 %";
            if (backgroundWorker4.IsBusy != true)
            {
                backgroundWorker4.RunWorkerAsync();
            }
        }

        private void backgroundWorker4_DoWork(object sender, DoWorkEventArgs e)
        {
            if (dateTimePicker1.Value > dateTimePicker2.Value)
            {
                MessageBox.Show("Start more than end");
            }
            else
            {
                QMSDBEntities db = new QMSDBEntities();
                TransactionService coaService = new TransactionService("System");

                DateTime startDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                DateTime endDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);

                Double stepValue = 1;
                Double Total = 0;

                backgroundWorker4.ReportProgress(1);

                var interval = (endDate - startDate).TotalDays + 1;

                if (interval > 0)
                {
                    stepValue = 100 / (interval);
                }
                else
                {
                    stepValue = 100 / 1;
                }

                DateTime startTime = startDate;
                DateTime endTime = startDate.AddDays(1);

                while (startTime <= endDate)
                {
                    endTime = startTime.AddDays(1);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.GAS);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.Utility);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.Emission);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.WatseWater);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.ObservePonds);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.OilyWaters);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.HotOilFlashPoint);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.HotOilPhysical);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.ClarifiedSystem);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.TwoHundredThousandPond);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.HgInPLMonthly);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.HgStab);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.SulfurInGasMonthly);
                    System.Threading.Thread.Sleep(500);

                    coaService.ReadTemplateFileDataToDatabaseByDate(db, startTime, endTime, (byte)TEMPLATE_TYPE.AcidOffGas);
                    System.Threading.Thread.Sleep(500);

                    startTime = startTime.AddDays(1);

                    Total += stepValue;
                    if (Total > 100)
                    {
                        Total = 100;
                    }
                    backgroundWorker4.ReportProgress(Convert.ToInt32(Total));
                    System.Threading.Thread.Sleep(500);
                }
            }
            backgroundWorker4.ReportProgress(100);
        }
        private void backgroundWorker4_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblStatus.Text = (e.ProgressPercentage.ToString() + " %");
        }

        private void backgroundWorker4_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                lblStatus.Text = "Canceled!";
                this.btnLoadTrendData.Enabled = true;
            }
            else if (e.Error != null)
            {
                lblStatus.Text = "Error: " + e.Error.Message;
                this.btnLoadTrendData.Enabled = true;
            }
            else
            {
                lblStatus.Text = "Done!";
                this.btnLoadTrendData.Enabled = true;
            }
        }

        private void btnDataLake_Click(object sender, EventArgs e)
        {
            QMSDatalakeTool.Form1 frmDataLake = new QMSDatalakeTool.Form1();
            frmDataLake.Show();
        }
    }
}
