﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;

namespace TrendDataTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void runDirectory()
        { 
            List<FolderData> listData = new List<FolderData>();

            string[] files = Directory.GetFiles(textBox1.Text);
            string[] dirs = Directory.GetDirectories(textBox1.Text);
            FileData dtFileData = new FileData();

            //foreach (string txtFile in files)
            //{
            //    FolderData temp = new FolderData(txtFile, "");
            //    listData.Add(temp);
            //}

            foreach (string txtDir in dirs)
            {
                FolderData temp = new FolderData("", txtDir);

                temp.IsFolderInRanageChecking(dtpStart.Value, dtpEnd.Value);
                
                listData.Add(temp);
                dtFileData.getAllDataInDirectory(temp.m_dtFolder, dtpStart.Value, dtpEnd.Value);

            }

            var source = new BindingSource();
            source.DataSource = dtFileData.m_listData;
            dataGridView1.DataSource = source;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Enabled = false;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
                runDirectory();
            }
            Enabled = true;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            Enabled = false;
            runDirectory();
            Enabled = true;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Enabled = false;
            IDataObject objectSave = Clipboard.GetDataObject();

            string filename = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\data.csv";

            

            // Choose whether to write header. Use EnableWithoutHeaderText instead to omit header.
            dataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            // Select all the cells
            dataGridView1.SelectAll();
            // Copy (set clipboard)
            Clipboard.SetDataObject(dataGridView1.GetClipboardContent());
            // Paste (get the clipboard and serialize it to a file)
            File.WriteAllText(filename, Clipboard.GetText(TextDataFormat.CommaSeparatedValue));

            // Restore the current state of the clipboard so the effect is seamless
            if (objectSave != null) // If we try to set the Clipboard to an object that is null, it will throw...
            {
                Clipboard.SetDataObject(objectSave);
            }
            Enabled = true;
        } 
    }

    public class FileData
    {
       public  List<OutLetRawTank> m_listData = new List<OutLetRawTank>();

        public void getAllDataInDirectory(string filePath, DateTime start, DateTime end)
        {
            try
            {
                string supportedExtensions = "*.xls,*.xlsx";
                foreach (string excelFile in Directory.GetFiles(filePath, "*.*", SearchOption.AllDirectories).Where(s => supportedExtensions.Contains(Path.GetExtension(s).ToLower())))
                {
                    //do work here
                    ExcelPackage excel = null;
                    try
                    {
                        string filename = Path.GetFileName(excelFile);

                        //FileInfo fileInfo = new FileInfo(excelFile);
                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        excel = new ExcelPackage(fileStream);

                       

                        var worksheet = excel.Workbook.Worksheets["utility"];

                        //Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                        //Microsoft.Office.Interop.Excel.Range range;
                        //wb = excel.Workbooks.Open(excelFile);
                        //Worksheet excelSheet = wb.ActiveSheet;
                        //range = excelSheet.UsedRange;
                        OutLetRawTank tempData = new OutLetRawTank();
                        //Read the first cell 
                        tempData.m_pH = Convert.ToDouble( worksheet.Cells["D8"].Value);
                        tempData.m_TURB = Convert.ToDouble(worksheet.Cells["F8"].Value);
                        tempData.m_SS = Convert.ToDouble(worksheet.Cells["U8"].Value);
                        long dateNum = long.Parse(worksheet.Cells["X4"].Value.ToString());
                        tempData.m_measureDate = DateTime.FromOADate(dateNum);

                        m_listData.Add(tempData);

                    }
                    catch (Exception ex)
                    {

                    }

                    if (null != excel)
                        excel.Dispose();
                }
               

            }
            catch (Exception ex)
            {

            }
        }


        //public bool IsFolderInRanageChecking(DateTime start, DateTime end)
        //{
        //    m_isFolderInRange = false;

        //    try
        //    {
        //        if (null != m_dtDirectory && "" != m_dtDirectory)
        //        {
        //            //string[] dtMonthYear = dtDirectory.Split('-');//แยก โดย - 
        //            DateTime folderDate = DateTime.ParseExact("01-" + m_dtDirectory, "dd-MM-yyyy", CultureInfo.InvariantCulture);

        //            DateTime firstDayOfMonth = new DateTime(start.Year, start.Month, 1);
        //            DateTime TempDayOfMonth = new DateTime(end.Year, end.Month, 1);
        //            DateTime lastDayOfMonth = TempDayOfMonth.AddMonths(1).AddDays(-1);

        //            if (folderDate >= firstDayOfMonth && folderDate <= lastDayOfMonth)
        //            {
        //                m_isFolderInRange = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return m_isFolderInRange;
        //} 
    }

    public class OutLetRawTank
    {
        public DateTime m_measureDate { get; set; }
        public double m_pH {get; set;}
        public double m_TURB {get; set;}
        public double m_SS { get; set; } 
    }

    public class FolderData
    {
        public string m_dtFile { get; set; }
        public string m_dtFolder { get; set; }
        public string m_dtDirectory { get; set; }
        public bool m_isFolderInRange { get; set; }

        public FolderData(string szFile, string szFolder)
        {
            m_dtFile = szFile;
            m_dtFolder = szFolder;

            try
            {
                if (null != szFolder && "" != szFolder)
                {
                    m_dtDirectory = szFolder.Replace(Path.GetDirectoryName(szFolder) + Path.DirectorySeparatorChar, ""); //Path.GetDirectoryName(dtFolder);                    
                }
                else
                {
                    m_dtDirectory = "";
                }
            }
            catch (Exception ex)
            {
                m_dtDirectory = "";
            }
        }

        public bool IsFolderInRanageChecking(DateTime start, DateTime end)
        {
            m_isFolderInRange = false;

            try
            {
                if (null != m_dtDirectory && "" != m_dtDirectory)
                {
                    //string[] dtMonthYear = dtDirectory.Split('-');//แยก โดย - 
                    DateTime folderDate = DateTime.ParseExact("01-" + m_dtDirectory, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    DateTime firstDayOfMonth = new DateTime(start.Year, start.Month, 1);
                    DateTime TempDayOfMonth = new DateTime(end.Year, end.Month, 1);
                    DateTime lastDayOfMonth = TempDayOfMonth.AddMonths(1).AddDays(-1);

                    if (folderDate >= firstDayOfMonth && folderDate <= lastDayOfMonth)
                    {
                        m_isFolderInRange = true;
                    }
                } 
            }
            catch (Exception ex)
            {

            } 
            return m_isFolderInRange;
        } 
    }
}
