﻿using System;
using System.Collections.Generic; 
using System.Text;

namespace QMSSystem.Model
{ 
    public enum TRANSECTION_DOC_STATUS
    {
        PENDING = 1,
        COMFRIM = 2,
        REJECT = 3,
        INITIAL = 4
    };

    public enum REPORT_DOC_STATUS
    {
        DRAFT =1,// ร่างเอกสาร  
        PUBLIC = 2,// Public 
    };

    public enum REPORT_SUMMARY_DATA_TYPE
    {
        OFF_CONTROL = 0,// แสดงค่า off control  
        INPUT_VOLUME = 1,// แสดงปริมาณ off control
        OFF_CONTROL_NO_DATA = 2, //แสดงค่า off control without data
        INPUT_VOLUME_NO_DATA = 3, //แสดงค่า input volume without data
    };

    public enum REPORT_SUMMARY_MODE
    {
        SUM_COL = 0,// แสดงค่า off control  
        SUM_ROW = 1
    };

    public enum TEMPLATE_COA_DOC_STATUS
    {
        PENDING = 1, //: ยังไม่ตั้งค่า control
        COMPLETE = 2, //: ตั้งค่า control เรียบร้อยล่ะ 
    };

    public enum EXASCHEDULE_QUES_DOC_STATUS
    {
        QUEUE =1,// อยู่ใน คิว  
        SUCCESS = 2,// สำเร็จ
        FAILURE = 3 // ไม่สำเร็จ
    };

    public enum GROUP_AUTHORIZATION
    {
        USER = 1,// normal user
        ADMIN = 2, // admin
    }

    public enum ReturnStatus
    {
        SUCCESS = 1, // ok 
        ERROR = 2 // Error
    }

    public enum ROOT_CAUSE_TYPE
    {
        MAN = 1,  
        MACHINE = 2, 
        MATERIAL = 3,
        METHOD = 4,
        MEASUREMENT = 5
    }

    //1: Quality, 2:Quantity-PV, 3: Quantity -SUM, 4: Accum
    public enum EXA_TAG_TYPE
    {
        ALL = 0,
        QUALITY_TAG = 1,
        QUANTITY_PV = 2,
        QUANTITY_SUM = 3,
        ACCUM = 4,
    }

    public enum MULTIPLICAND
    {
        COUNT_OUPPUT = 1,
        UNCOUNT_OUPPUT = 2,
        MOVED_OUPPUT = 3
    }

    public enum TEMPLATE_IMP_EXP_TYPE
    {
        PRODUCT = 1,
        DOWNTIME = 2,
        REDUCE_FEED = 3
    }

    public enum DENOMINATOR
    {
        COUNT_INPUT = 1,
        UNCOUNT_INPUT = 2,
        MOVED_INPUT = 3
    }

    public enum OFF_CONTROL_CAL_METHOD
    {
        NOT_CALUCATE = 1,
        NORMAL_CALUCATE = 2,
        OFFCONTROL_ON = 3,
        OFFCONTROL_OFF = 4
    }

    public enum CONTROL_GROUP_TYPE
    {
        SPEC = 1,
        GAS = 2,
        Utility = 3,
        PRODUCT = 4,
        EMISSION = 5,
        ClarifiedSystem = 6,
        WatseWater = 7,
        HotOilPhysical = 8,
        HotOilFlashPoint = 9,
        HgInPLMonthly = 10,
        SulfurInGasMonthly = 11,
        HgInPL = 12,
        HgOutletMRU = 13,
        SulfurInGasWeekly = 14,
        ObservePonds = 15,
        OilyWaters = 16,
        TwoHundredThousandPond = 17,
        HgStab = 18,
        AcidOffGas = 19
    }

    public enum DOC_STATUS
    { 
        PENDING = 1,
        CONFIRM = 2,
        REJECT = 3,
        INITIAL = 4
    }

    public enum SIGMA_STATUS
    {
        Standard = 1,
        Repeatability = 2,
        Reproducibility = 3
    }

    public enum TREND_GRAPH_ENUM
    {
        OFF = 0,
        ON = 1, 
    }
    

    public enum OFF_TYPE
    {
        CONTROL = 1,
        SPEC = 2
    }

	 public enum ITEM_TYPE
    {
        COL_HEADER = 1,
        ROW_HEADER = 2,
        DATA = 3,
        SUMCOL_HEADER = 4,
        SUMROW_HEADER = 5,
        SUMALL = 6,
        ROW_HIDE = 7
    }

    public enum TEMPLATE_TYPE
    {
        GAS = 1,
        Utility = 2,
        Emission = 3,
        ClarifiedSystem = 4,
        WatseWater = 5,
        HotOilPhysical = 6,
        HotOilFlashPoint = 7,
        HgInPLMonthly = 8,
        SulfurInGasMonthly = 9,
        HgInPL = 10,
        HgOutletMRU = 11,
        SulfurInGasWeekly = 12,
        ObservePonds = 13,
        OilyWaters = 14,
        TwoHundredThousandPond = 15,
        HgStab = 16,
        AcidOffGas = 17
    }

    public enum EXCEL_DATA_TYPE
    {
        SAMPLE = 1,
        ITEM = 2,
        CONTROL_VALUE = 3,
        AREA = 4
    }

    public enum RP_TREND_TYPE
    {
        GAS = 1,
        UTILITY = 2,
        PRODUCT = 3
    }

    public enum RP_TREND_DETAIL_TYPE
    {
        SERIES1 = 1,
        SERIES2 = 2
    }

    public enum ARROW_STATUS
    {
        ARROW_UP = 1,// ขึ้น  
        ARROW_DOWN = 2,// ลง 
    };

    public enum OFF_CONTROL
    {
        OFF_HIGH =1 ,
        OFF_LOW = 2,
    }

    public enum COLUMN_SHIFT_TYPE
    {
        COLUMN_NAME = 1,
        COLUMN_DATA = 2,
    }

    public enum OPERATION_SHIFT_TIME
    {
        TIME1 = 1,
        TIME2 = 2,
    }

    public enum EXA_SCHEDULE_STATUS
    {
        PENDING = 1,
        CONFIRM = 2,
        FAIL = 3
    }

    public enum CAL_OFF_STATUS
    {
        NORMAL = 0,
        OFF_CONTROL_HIGH = 1,
        OFF_CONTROL_LOW = 2,        
        GC_ERROR = 3,
        GC_ERROR_FLOWRATE = 4,
        OFF_SPEC_HIGH = 5,
        OFF_SPEC_LOW = 6,
        OFF_CONTROL_AND_SPEC_HIGH = 7,
        OFF_CONTORL_AND_SPEC_LOW = 8,
        TAG_CORRECT_ERR = 9,
        TAG_TARGET_ERR = 10,
        FEED_REDUCE_ERR = 11,
        DOWNTIME_ERR = 12,
        GC_ERROR_FREEZ = 13,
        INITIAL = 99
    }

    public enum CORRECT_DATA_TYPE
    {
        TRUN_AROUND = 1,
        DOWN_TIME = 2,
        CHANGE_GRADE = 3,
        CROSS_VOLUME = 4,
        REDUCE_FEED = 5, //change from 6 --> 5
        ABNORMAL = 6, //GC_ERROR change from 5 --> 6 
        EXCEPTION = 7
    }

    public enum EMAIL_GROUP_TYPE
    {
        ADMIN_EMAIL = 1,
        MANAGE_EMAIL = 2
    }

    public enum AUTO_OFF_TYPE
    {
        ALL_PENDING = 1,
        ALL_CONFIRM = 2,
        CONFIRM_AND_PENDING = 3,
        INITIAL = 4
    }

    public enum EXA_DATA_TYPE
    {
        PRODUCT_DATA = 0,
        DOWNTIME_DATA = 1,
        REDUCE_FEED_DATA = 2
    }

    [Serializable]
    public class AuthorizationData
    {
        public int code { get; set; }
        public bool access { get; set; }
    }


    public enum SectionEnum
    {
        All = 0,
        USER = 1,
        CLIENT = 2,
        SCHEDULE = 3,
        ONLINE_FORM = 4,
        REPORT = 5,
        SETTING = 6,
    }

    public enum DataAccessEnum
    {
        Anonymous = 0,// เข้าได้ทุกคน
        Authen = 1
    }

    public enum OFF_CAL_ROOTCAUSE
    {
        DEFAULT =1
    }
     
    public enum PrivilegeModeEnum
    {
        //All
        All = 0, //check only login

        //DASHBOARD 
        D_Home = 100,
        D_Dashboard = 341,

        //Off Control & spec
        D_OffControl = 101,
        D_OffSpec = 102,

        //Trend Data
        D_TrendHighlight = 103,
        D_TrendGAS = 104,
        D_TrendUtility = 105,
        D_TrendProduct = 106,
        D_ProductCSC = 107,
        D_TrendWasteWater = 108,
        D_TrendHotOilFlashPoint = 109,
        D_TrendHotOilPhysical = 110,
        D_TrendSulfurInGasMonthly = 111,
        D_TrendHgInPLMonthly = 112,
        D_TrendHgInPL = 113,
        D_TrendHgOutletMRU = 114,
        D_TrendSulfurInGasWeekly = 115,
        D_TrendTwoHundredThousandPond = 116,
        D_TrendHgStab = 117,
        D_TrendAcidOffGas = 118,
        D_TrenddObservePondsAndOilyWaters = 119,
        D_TrendEmission = 120,
        D_TrenddOilyWaters = 121,
        D_TrenddObservePonds = 122,
        D_TrendLabControlChart = 123,
        //Master Data
        M_PlantAndProduct = 201,
        M_ReduceFeed = 202,
        M_DownTime = 203,
        M_CorrectData = 204,
        M_RootCause = 205,
        M_KPI = 206,
        M_Shift = 207,
        M_ControlGas = 208,
        M_SpecGas = 209,
        M_ControlUtility = 210,
        M_ControlProdcut = 211,
        M_TemplateGas = 212,
        M_TemplateUtility = 213,
        M_Email = 214,
        M_Unit = 215,
        M_ControlEmission = 216,
        M_TemplateEmission = 217,
        M_TemplateClarifiedSystem = 218,
        M_ControlClarifiedSystem = 219,
        M_ControlWatseWater = 220,
        M_TemplateWatseWater = 221,
        M_ControlHotOilFlashPoint = 222,
        M_TemplateHotOilFlashPoint = 223,
        M_ControlHotOilPhysical = 224,
        M_TemplateHotOilPhysical = 225,
        M_ControlSulfurInGasMonthly = 226,
        M_TemplateSulfurInGasMonthly = 227,
        M_ControlHgInPLMonthly = 228,
        M_TemplateHgInPLMonthly = 229,
        M_ControlHgInPL = 230,
        M_TemplateHgInPL = 231,
        M_ControlHgOutletMRU = 232,
        M_TemplateHgOutletMRU = 233,
        M_ControlSulfurInGasWeekly = 234,
        M_TemplateSulfurInGasWeekly = 235,
        M_TemplateObservePonds = 236,
        M_TemplateOilyWaters = 237,
        M_ControlObservePonds = 238,
        M_ControlOilyWaters = 239,
        M_ControlTwoHundredThousandPond = 240,
        M_TemplateTwoHundredThousandPond = 241,
        M_ControlHgStab = 242,
        M_TemplateHgStab = 243,
        M_ControlAcidOffGas = 244,
        M_TemplateAcidOffGas = 245,
        M_IntervalSampling = 342,
        M_ProductMerge = 343,
        M_CustomerMerge = 344,
        M_SigmaLevel = 345,

        //Mansge Data
        N_RawDataExport = 301,
        N_OffControlCal = 302,
        N_OffSpecCal = 303,
        N_TurnAround = 304,
        N_DownTime = 305,
        N_ChangeGrade = 306,
        N_CrossVolume = 307,
        N_Abnormal = 308,
        N_ReduceFeed = 309,
        N_Exception = 310,
        N_TemplateGas = 311,
        N_TemplateUtility = 312,
        N_TemplateCOA = 313,
        N_ImpExpTemplate = 314,
        N_TemplateClarifiedSystem = 315,
        N_TemplateWasteWater = 316,
        N_TemplateHotOilFlashPoint = 317,
        N_TemplateHotOilPhysical = 318,
        N_TemplateSulfurInGasMonthly = 319,
        N_TemplateHgInPLMonthly = 320,
        N_TemplateHgInPL = 321,
        N_TemplateHgOutletMRU = 322,
        N_TemplateSulfurInGasWeekly = 323,
        N_TemplateObservePonds = 324,
        N_TemplateOilyWaters = 325,
        N_TemplateTwoHundredThousandPond = 326,
        N_TemplateHgStab = 327,
        N_TemplateAcidOffGas = 328,
        N_TemplateEmission = 329,
        N_LabControlChart = 330,
        N_TemplateCSCProduct = 331,
        N_TemplateCOAToDataLake = 332,
        N_DataLake = 333,
        //N_LoadDataLake = 333,
        //N_LoadEventRawDataToDB = 334,
        //N_LoadQmsRpOffControlToDataLake = 335,
        //N_LoadQmsRpOffControlSummaryToDataLake = 336,
        //N_LoadQmsRpOffControlStampToDataLake = 337,
        //N_LoadAbnormalToDB = 338,
        //N_LoadEventCalToDB = 339,
        //N_CheckTotalEXQProduct = 340,


        //Report
        R_OffControlSum = 401,
        R_OffSpecSum = 402,
        R_CorrectData = 403,
        R_Shift = 404,
        R_TrendGas = 405,
        R_TrendUtitliy = 406,
        R_TrendProduct = 407,


        //Setting 
        S_Home = 501,
        S_Abnormal = 502,
        S_ExportData = 503,
        S_ReportOff = 504,
        S_ExtraSchedule = 505,
        S_TrendHightlight = 506,
        S_Connect = 507,
        S_Log = 508,
        S_Authen = 509,
        S_ProductCSCPath = 510,
        S_EXA_CONNNECT = 511,
        S_LIMS_Connect = 512,
        S_TEMP_Connect = 513,
        S_SortOption = 514,

    }

    public enum ST_CONFIG_STAMP
    { 
        S_Home = 501,
        S_Abnormal = 502,
        S_ExportData = 503,
        S_ReportOff = 504,
        S_ExtraSchedule = 505,
        S_TrendHightlight = 506,
        S_Connect = 507,
        S_Log = 508,
        S_Authen = 509,
        S_ProductCSCPath = 510,
        S_EXA_CONNNECT = 511,
        S_LIMS_CONNECT = 512,
        S_TEMP_CONNECT = 513,
    }
     

    public enum PrivilegeEnum
    {
        Deny = 0,
        Read = 1,
        Write = 2,
        Edit = 3,
        Delete = 4,
        Approve = 5,
        All = 6
    }

    public enum AuthenResult
    {
        Deny = 0,
        User = 1,
        Admin = 9, 
    }
     
}
