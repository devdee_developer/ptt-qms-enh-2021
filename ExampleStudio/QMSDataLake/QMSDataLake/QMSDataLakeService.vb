﻿Imports System.Threading
Imports System.IO
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data.EntityClient
Imports QMSSystem.CoreDB.Services
Imports QMSSystem.Model

Public Class QMSDataLakeService
    Private Schedular As Timer
    Private m_nCount As Integer = 3
    Private m_bError As Boolean = False
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        'System.Diagnostics.Debugger.Break()
        Me.WriteToFile("Service started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.ScheduleService()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Me.WriteToFile("Service stopped at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.Schedular.Dispose()
    End Sub
    Private Sub WriteToFile(text As String)
        Dim dirName As String
        Dim path As String
        Try
            dirName = AppDomain.CurrentDomain.BaseDirectory & "\\servicelogs" ' [Assembly].GetExecutingAssembly.Location 'Directory.GetCurrentDirectory()

            If (Not System.IO.Directory.Exists(dirName)) Then
                System.IO.Directory.CreateDirectory(dirName)
            End If

            path = dirName & "\\" & DateTime.Now.ToString("yyyyMMdd") & ".log"

            Using writer As New StreamWriter(path, True)
                writer.WriteLine(text)
                writer.Close()
            End Using
        Catch
        End Try

    End Sub
    Public Sub ScheduleService()
        Try
            Schedular = New Timer(New TimerCallback(AddressOf SchedularCallback))
            Dim mode As String = ConfigurationManager.AppSettings("Mode").ToUpper()
            Me.WriteToFile((Convert.ToString("Service Mode: ") & mode) + " {0}")

            m_nCount = 3
            'Set the Default Time.
            Dim scheduledTime As DateTime = DateTime.MinValue

            If mode = "DAILY" Then
                'Get the Scheduled Time from AppSettings.
                'System.Diagnostics.Debugger.Launch()
                scheduledTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings("ScheduledTime"))

                Try
                    Dim connectionString As String
                    Me.WriteToFile("Before get conn ScheduleService")
                    connectionString = GetConnectionString()
                    Dim db As New QMSSystem.CoreDB.QMSDBEntities(connectionString)
                    Dim cogService = New ConfigServices("Service")
                    Dim createTemplateConnect = New CreateQMS_ST_EXA_CONNECT()
                    createTemplateConnect = cogService.getEXA_SCHEDULE(db)
                    Me.WriteToFile("After get conn ScheduleService")

                    If createTemplateConnect.EXA_SCHEDULE.HasValue Then
                        Me.WriteToFile(createTemplateConnect.EXA_SCHEDULE.Value.ToString())
                        scheduledTime = New DateTime(scheduledTime.Year, scheduledTime.Month, scheduledTime.Day, createTemplateConnect.EXA_SCHEDULE.Value.Hour, createTemplateConnect.EXA_SCHEDULE.Value.Minute, 0)
                    End If

                Catch ex As Exception
                    WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)
                End Try

                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next day.
                    scheduledTime = scheduledTime.AddDays(1)
                End If
            End If

            If mode.ToUpper() = "INTERVAL" Then
                'Get the Interval in Minutes from AppSettings.
                Dim intervalMinutes As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))

                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(intervalMinutes)
                End If
            End If

            Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
            Dim schedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds)

            Me.WriteToFile((Convert.ToString("1Service scheduled to run after: ") & schedule) + " {0}")

            'Get the difference in Minutes between the Scheduled and Current Time.
            Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)

            'Change the Timer's Due Time.
            Schedular.Change(dueTime, Timeout.Infinite)
        Catch ex As Exception
            WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)

            'Stop the Windows Service.
            Using serviceController As New System.ServiceProcess.ServiceController("SimpleService")
                serviceController.[Stop]()
            End Using
        End Try
    End Sub
    Private Sub SchedularCallback(e As Object)

        Me.WriteToFile("Start : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))

        Try
            Dim activeDate As DateTime = Date.Now
            Dim startDate As DateTime
            Dim endDate As DateTime 'start date

            startDate = New DateTime(activeDate.Year, activeDate.Month, activeDate.Day, 0, 0, 0).AddDays(-1)
            endDate = startDate.AddDays(1)

            Dim connectionString As String
            Me.WriteToFile("Before get conn SchedularCallback")
            connectionString = GetConnectionString()
            Dim db As New QMSSystem.CoreDB.QMSDBEntities(connectionString)
            Dim MasterDataService As New MasterDataService("Service")
            Dim TransactionService As New TransactionService("Service")
            Dim szSubject As String
            Dim szBody As String


            Try
                Dim Totaltag As String
                Totaltag = MasterDataService.LoadDataLakeTODB(db, startDate, endDate)
                Me.WriteToFile("Total tag from data lake : " + Totaltag)
                szBody = szBody & "<br/>Total tag from data lake : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)

            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadAbnormalFromDataLakeTODB(db, startDate, endDate)
                Me.WriteToFile("Load Abnormal From DataLake TO DB : " + Totaltag)
                szBody = szBody & "<br/>Load Abnormal From DataLake TO DB : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)

            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadEventRawDataFromDataLakeTODB(db, startDate, endDate)
                Me.WriteToFile("Total tag from EVENT_RAW_DATA : " + Totaltag)
                szBody = szBody & "<br/>Total tag from EVENT_RAW_DATA : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)


            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadDataLake_EVENT_DATA_TODB(db)
                Me.WriteToFile("Total tag from EVENT CAL : " + Totaltag)
                szBody = szBody & "<br/>Total tag from EVENT CAL : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)

            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadRP_OFF_CONTROLFromDBTODataLake(db, startDate, endDate)
                Me.WriteToFile("Total Row from QMS_RP_OFF_CONTROL : " + Totaltag)
                szBody = szBody & "<br/>Total Row from QMS_RP_OFF_CONTROL : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)

            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadRP_OFF_CONTROL_SUMMARYFromDBTODataLake(db, startDate, endDate)
                Me.WriteToFile("Total Row from QMS_RP_OFF_CONTROL_SUMMARY : " + Totaltag)
                szBody = szBody & "<br/>Total Row from QMS_RP_OFF_CONTROL_SUMMARY : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)

            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadOffControlFromDBTODataLake(db, startDate, endDate)
                Me.WriteToFile("Total Row from QMS_RP_OFF_CONTROL_STAMP : " + Totaltag)
                szBody = szBody & "<br/>Total Row from QMS_RP_OFF_CONTROL_STAMP : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)


            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadQMS_TR_COA_DATAFromDBTODataLake(db, startDate, endDate)
                Me.WriteToFile("Total Row from LoadQMS_TR_COA_DATAFromDBTODataLake : " + Totaltag)
                szBody = szBody & "<br/>Total Row from LoadQMS_TR_COA_DATAFromDBTODataLake : " + Totaltag
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If
            End Try

            Threading.Thread.Sleep(5000)

            Me.WriteToFile("After get conn SchedularCallback")


            'write email .... 
            Dim emailObj As New EmailService()
            Dim hostName As String = Convert.ToString(ConfigurationManager.AppSettings("SMTP_SERVER"))
            Dim port As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("SMTP_SERVER_PORT"))
            Dim fromName As String = Convert.ToString(ConfigurationManager.AppSettings("SMTP_SENDER_EMAIL"))
            Dim toName As String = Convert.ToString(ConfigurationManager.AppSettings("DummyMail")) 
            szSubject = "Data lake result " 
            emailObj.SendSmtpMail(hostName, port, "", "", fromName, "", toName, "", szSubject, szBody, True)
            'write email.....
        Catch ex As Exception
            Me.WriteToFile(ex.Message)
        End Try


        Me.WriteToFile("End : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))

        Me.ScheduleService()
    End Sub
    Public Function GetConnectionString() As String

        Dim DBConnection As String
        DBConnection = System.Configuration.ConfigurationManager.ConnectionStrings("QMSDBEntities").ToString()

        Return DBConnection

        Dim providerName As String = "System.Data.SqlClient"

        ' Server Localhost
        ' Specify the provider name, server and database.
        'Dim serverName As String = ".\SQLEXPRESS2008"
        'Dim databaseName As String = "PTT-GSPQualityManagementSystem_test"

        ' Server Test
        ' Specify the provider name, server and database.
        Dim serverName As String = "ptt-db-t03.ptt.corp"
        Dim databaseName As String = "PTT-GSPQualityManagementSystem_test"
        Dim UserID As String = "pttgspqprdusr"
        Dim Password As String = "cpttgspqprdusr"

        ' Server Production
        ' Specify the provider name, server and database.
        'Dim serverName As String = "ptt-db-p03.ptt.corp"
        'Dim databaseName As String = "PTT-GSPQualityManagementSystem"
        'Dim UserID As String = "pttgspqprdusr"
        'Dim Password As String = "cpttgspqprdusr"

        ' Initialize the connection string builder for the
        ' underlying provider.
        Dim sqlBuilder As New SqlConnectionStringBuilder

        ' Server Localhost
        ' Set the properties for the data source.
        'sqlBuilder.DataSource = serverName
        'sqlBuilder.InitialCatalog = databaseName
        'sqlBuilder.IntegratedSecurity = True
        'sqlBuilder.MultipleActiveResultSets = True

        'Server Test
        'Set the properties for the data source.
        sqlBuilder.DataSource = serverName
        sqlBuilder.InitialCatalog = databaseName
        sqlBuilder.PersistSecurityInfo = True
        sqlBuilder.UserID = UserID
        sqlBuilder.Password = Password

        'Server Production
        'Set the properties for the data source.
        'sqlBuilder.DataSource = serverName
        'sqlBuilder.InitialCatalog = databaseName
        'sqlBuilder.PersistSecurityInfo = True
        'sqlBuilder.UserID = UserID
        'sqlBuilder.Password = Password

        ' Build the SqlConnection connection string.
        Dim providerString As String = sqlBuilder.ToString

        ' Initialize the EntityConnectionStringBuilder.
        Dim entityBuilder As New EntityConnectionStringBuilder

        'Set the provider name.
        entityBuilder.Provider = providerName
        ' Set the provider-specific connection string.
        entityBuilder.ProviderConnectionString = providerString
        ' Set the Metadata location to the current directory.
        entityBuilder.Metadata = "res://*//QMSSystem.csdl|" & _
                                    "res://*//QMSSystem.ssdl|" & _
                                    "res://*//QMSSystem.msl"

        Return entityBuilder.ToString()
    End Function
End Class
