﻿using System.ComponentModel.DataAnnotations;

public class CreateSTUDENT
{
    public long STUDENT_ID { get; set; }

    [Required(ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "Required")]
    [StringLength(60, MinimumLength = 1, ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "StringLength")]
    public string STUDENT_NAME { get; set; }
    [Required(ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "Required")]
    [StringLength(60, MinimumLength = 1, ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "StringLength")]
    public string STUDENT_SNAME { get; set; }
}