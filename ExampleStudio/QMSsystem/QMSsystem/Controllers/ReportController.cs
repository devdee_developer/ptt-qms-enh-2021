﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using System.IO;
using QMSSystem.CoreDB.Helper;
using QMSSystem.Model;
using QMSsystem.Models;
using OfficeOpenXml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Globalization;
using QMSsystem.Models.Privilege;



namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { 
      PrivilegeModeEnum.D_OffControl ,  PrivilegeModeEnum.D_OffSpec ,  PrivilegeModeEnum.D_Home 
    , PrivilegeModeEnum.R_CorrectData, PrivilegeModeEnum.R_OffControlSum 
    , PrivilegeModeEnum.R_OffSpecSum , PrivilegeModeEnum.R_Shift  , PrivilegeModeEnum.R_TrendGas 
    , PrivilegeModeEnum.R_TrendProduct  , PrivilegeModeEnum.R_TrendUtitliy}, DataAccess = DataAccessEnum.Authen)]
    public class ReportController : BaseController
    {
        //
        // GET: /Report/

        //Off Control
        public ActionResult ListOffControlSummary()//control ListOffControlSummary.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ReportOffControlSearchModel searchRPOffControl = new ReportOffControlSearchModel();

            CreateQMS_RP_OFF_CONTROL createQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();
            CreateQMS_RP_OFF_CONTROL_DETAIL createQMS_RP_OFF_CONTROL_DETAIL = new CreateQMS_RP_OFF_CONTROL_DETAIL();
            CreateQMS_RP_OFF_CONTROL_GRAPH createQMS_RP_OFF_CONTROL_GRAPH = new CreateQMS_RP_OFF_CONTROL_GRAPH();
            CreateQMS_RP_OFF_CONTROL_ATTACH createQMS_RP_OFF_CONTROL_ATTACH = new CreateQMS_RP_OFF_CONTROL_ATTACH();
            CreateQMS_RP_OFF_CONTROL_SUMMARY createQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();

            ReportOffControlDetailSearchModel searchOffControlDetail = new ReportOffControlDetailSearchModel();
            ReportOffControlGraphSearchModel searchOffControlGraph = new ReportOffControlGraphSearchModel();
            ReportOffControlAttachSearchModel searchOffControlAttach = new ReportOffControlAttachSearchModel();
            ReportOffControlSummarySearchModel searchOffControlSummary = new ReportOffControlSummarySearchModel();

            RPOffControlSummaryRowSearchModel searchRPProduct = new RPOffControlSummaryRowSearchModel();
            RPOffControlSummaryColumnSearchModel searchRPPlant = new RPOffControlSummaryColumnSearchModel();
            RPOffControlSummarySearchModel searchRPOffControlSummary = new RPOffControlSummarySearchModel();

            OffControlCalAutoSearchModel searchAutoCal = new OffControlCalAutoSearchModel();

            createQMS_RP_OFF_CONTROL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.CONTROL;

            createQMS_RP_OFF_CONTROL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL.END_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.END_DATE = getDummyDateTime();

            searchOffControlDetail.RP_OFF_CONTROL_ID = 0;
            searchOffControlDetail.OFF_TYPE = (byte)OFF_TYPE.CONTROL;
            searchOffControlGraph.RP_OFF_CONTROL_ID = 0;
            searchOffControlAttach.RP_OFF_CONTROL_ID = 0;
            searchOffControlSummary.RP_OFF_CONTROL_ID = 0;
            searchAutoCal.RPOffControlId = 0;
            searchAutoCal.OFF_TYPE = (byte)OFF_TYPE.CONTROL;
            searchAutoCal.AUTO_OFF_TYPE = (byte)AUTO_OFF_TYPE.ALL_PENDING;
            searchAutoCal.CHK_CORRECTDATA = true;

            searchAutoCal.LIST_PLANT_ID = new List<long>();
            searchAutoCal.LIST_PRODUCT_ID = new List<long>();
            searchAutoCal.CHK_ALL_PLANT = true;
            searchAutoCal.CHK_ALL_PRODUCT = true;
            searchRPOffControl.OFF_CONTROL_TYPE = (byte)OFF_TYPE.CONTROL;
             
            ViewBag.listReportDocStatus = this.getReportDocStatusList();

            ViewBag.searchRPOffControl = searchRPOffControl;
            ViewBag.listPageSize = this.getPageSizeList();


             

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listAutoOffType = getAutoOffControlList();

            ViewBag.createQMS_RP_OFF_CONTROL = createQMS_RP_OFF_CONTROL;
            ViewBag.createQMS_RP_OFF_CONTROL_DETAIL = createQMS_RP_OFF_CONTROL_DETAIL;
            ViewBag.createQMS_RP_OFF_CONTROL_GRAPH = createQMS_RP_OFF_CONTROL_GRAPH;
            ViewBag.createQMS_RP_OFF_CONTROL_ATTACH = createQMS_RP_OFF_CONTROL_ATTACH;
            ViewBag.createQMS_RP_OFF_CONTROL_SUMMARY = createQMS_RP_OFF_CONTROL_SUMMARY;

            ViewBag.searchOffControlDetail = searchOffControlDetail;
            ViewBag.searchOffControlGraph = searchOffControlGraph;
            ViewBag.searchOffControlAttach = searchOffControlAttach;
            ViewBag.searchOffControlSummary = searchOffControlSummary;

            ViewBag.searchRPProduct = searchRPProduct;
            ViewBag.searchRPPlant = searchRPPlant;
            ViewBag.searchRPOffControlSummary = searchRPOffControlSummary;
            ViewBag.searchAutoCal = searchAutoCal;

            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.DateNow = getDummyDateTime();
            
            return View();
        }

        public ActionResult ApireloadQMS_RP_OFF_CONTROL()
        {
            ReportServices ReportServices = new ReportServices(_username);
            ReportServices.ApireloadQMS_RP_OFF_CONTROL(_db);

            return View();
        }

        public ActionResult ApiQMS_RP_OFF_CONTROL()
        {
            ReportServices ReportServices = new ReportServices(_username);
            ReportServices.ApiQMS_RP_OFF_CONTROL(_db);
            return View();
        }

        public ActionResult CreateOffControlSummary(long id = 0)//control CreateOffControlSummary.cshtml
        {
            ReportServices ReportServices = new ReportServices(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_RP_OFF_CONTROL createQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();
            CreateQMS_RP_OFF_CONTROL_DETAIL createQMS_RP_OFF_CONTROL_DETAIL = new CreateQMS_RP_OFF_CONTROL_DETAIL();
            CreateQMS_RP_OFF_CONTROL_GRAPH createQMS_RP_OFF_CONTROL_GRAPH = new CreateQMS_RP_OFF_CONTROL_GRAPH();
            CreateQMS_RP_OFF_CONTROL_ATTACH createQMS_RP_OFF_CONTROL_ATTACH = new CreateQMS_RP_OFF_CONTROL_ATTACH();
            CreateQMS_RP_OFF_CONTROL_SUMMARY createQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();
              
            ReportOffControlDetailSearchModel searchOffControlDetail = new ReportOffControlDetailSearchModel();
            ReportOffControlGraphSearchModel searchOffControlGraph = new ReportOffControlGraphSearchModel();
            ReportOffControlAttachSearchModel searchOffControlAttach = new ReportOffControlAttachSearchModel();
            ReportOffControlSummarySearchModel searchOffControlSummary = new ReportOffControlSummarySearchModel();

            RPOffControlSummaryRowSearchModel searchRPProduct = new RPOffControlSummaryRowSearchModel();
            RPOffControlSummaryColumnSearchModel searchRPPlant = new RPOffControlSummaryColumnSearchModel();
            RPOffControlSummarySearchModel searchRPOffControlSummary = new RPOffControlSummarySearchModel();

            OffControlCalAutoSearchModel searchAutoCal = new OffControlCalAutoSearchModel();
            
            string actionName = Resources.Strings.AddNew;

            createQMS_RP_OFF_CONTROL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.CONTROL;

            createQMS_RP_OFF_CONTROL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL.END_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.END_DATE = getDummyDateTime();

            searchOffControlDetail.RP_OFF_CONTROL_ID = id;
            searchOffControlDetail.OFF_TYPE = (byte)OFF_TYPE.CONTROL;
            searchOffControlGraph.RP_OFF_CONTROL_ID = id;
            searchOffControlAttach.RP_OFF_CONTROL_ID = id;
            searchOffControlSummary.RP_OFF_CONTROL_ID = id;
            searchAutoCal.RPOffControlId = id;
            searchAutoCal.OFF_TYPE = (byte)OFF_TYPE.CONTROL;
            searchAutoCal.AUTO_OFF_TYPE = (byte)AUTO_OFF_TYPE.ALL_PENDING;
            searchAutoCal.CHK_CORRECTDATA = true;

            searchAutoCal.LIST_PLANT_ID = new List<long>();
            searchAutoCal.LIST_PRODUCT_ID = new List<long>();
            searchAutoCal.CHK_ALL_PLANT = true;
            searchAutoCal.CHK_ALL_PRODUCT = true;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_RP_OFF_CONTROL = ReportServices.getQMS_RP_OFF_CONTROLById(_db, id);

            }

            ViewBag.listReportDocStatus = this.getReportDocStatusList();

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listAutoOffType = getAutoOffControlList();

            ViewBag.createQMS_RP_OFF_CONTROL = createQMS_RP_OFF_CONTROL;
            ViewBag.createQMS_RP_OFF_CONTROL_DETAIL = createQMS_RP_OFF_CONTROL_DETAIL;
            ViewBag.createQMS_RP_OFF_CONTROL_GRAPH = createQMS_RP_OFF_CONTROL_GRAPH;
            ViewBag.createQMS_RP_OFF_CONTROL_ATTACH = createQMS_RP_OFF_CONTROL_ATTACH;
            ViewBag.createQMS_RP_OFF_CONTROL_SUMMARY = createQMS_RP_OFF_CONTROL_SUMMARY;

            ViewBag.searchOffControlDetail = searchOffControlDetail;
            ViewBag.searchOffControlGraph = searchOffControlGraph;
            ViewBag.searchOffControlAttach = searchOffControlAttach;
            ViewBag.searchOffControlSummary = searchOffControlSummary;

            ViewBag.searchRPProduct = searchRPProduct;
            ViewBag.searchRPPlant = searchRPPlant;
            ViewBag.searchRPOffControlSummary = searchRPOffControlSummary;
            ViewBag.searchAutoCal = searchAutoCal;

            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.DateNow = getDummyDateTime();
            
            ViewBag.ActionName = actionName;
            return View();
        }



        public JsonResult CreateQMS_RP_OFF_CONTROL(CreateQMS_RP_OFF_CONTROL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    //reset datetime
                    //model.START_DATE = new DateTime(model.START_DATE.Year, model.START_DATE.Month, model.START_DATE.Day, 0, 0, 0);
                    //model.END_DATE = new DateTime(model.END_DATE.Year, model.END_DATE.Month, model.END_DATE.Day, 0, 0, 0).AddDays(1).AddMilliseconds(-1);

                    long nResult = ReportServices.SaveQMS_RP_OFF_CONTROL(_db, model);

                    if (nResult > 0)
                    {
                        model = ReportServices.getQMS_RP_OFF_CONTROLById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBarGRAPHData(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    ReportOffControlGraphSearch model = new ReportOffControlGraphSearch();
                    model.mSearch = new ReportOffControlGraphSearchModel();
                    model.mSearch.RP_OFF_CONTROL_ID = id;
                    model.PageSize = -1;

                    List<ViewQMS_RP_OFF_CONTROL_GRAPH> listData = new List<ViewQMS_RP_OFF_CONTROL_GRAPH>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = ReportServices.searchQMS_RP_OFF_CONTROL_GRAPH(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_RP_OFF_CONTROLById(long ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    CreateQMS_RP_OFF_CONTROL listData = new CreateQMS_RP_OFF_CONTROL();
                    listData = ReportServices.getQMS_RP_OFF_CONTROLById(_db, ids);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_RP_OFF_CONTROLBySearch(ReportOffControlSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_RP_OFF_CONTROL> listData = new List<ViewQMS_RP_OFF_CONTROL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = ReportServices.searchQMS_RP_OFF_CONTROL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_RP_OFF_CONTROL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    bool nResult = ReportServices.DeleteQMS_RP_OFF_CONTROLByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public JsonResult CreateQMS_RP_OFF_CONTROL_DETAIL(CreateQMS_RP_OFF_CONTROL_DETAIL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long nResult = ReportServices.SaveQMS_RP_OFF_CONTROL_DETAIL(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_RP_OFF_CONTROL_DETAILEx(ReportOffControlDetailSearch model) //(List<ViewQMS_RP_OFF_CONTROL_DETAIL> model)
        {
            Object result;
            //if (ModelState.IsValid)
            //{
                ReportServices ReportServices = new ReportServices(_username);

                try
                {

                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listData = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();

                    if (model.mSearch.CHK_AUTO_CAL == true && null != model.mSearch.AUTO_START_DATE && null != model.mSearch.AUTO_END_DATE)
                    {
                        OffControlCalAutoSearchModel searchOffCalModel = new OffControlCalAutoSearchModel();
                        searchOffCalModel.PLANT_ID = model.mSearch.PLANT_ID;
                        searchOffCalModel.PRODUCT_ID = model.mSearch.PRODUCT_ID;
                        searchOffCalModel.ROOT_CAUSE_ID = model.mSearch.ROOT_CAUSE_ID;
                        searchOffCalModel.OFF_TYPE = model.mSearch.OFF_TYPE;
                        searchOffCalModel.START_DATE = model.mSearch.AUTO_START_DATE.Value;
                        searchOffCalModel.END_DATE = model.mSearch.AUTO_END_DATE.Value;
                        searchOffCalModel.RPOffControlId = model.mSearch.RP_OFF_CONTROL_ID;
                        searchOffCalModel.AUTO_OFF_TYPE = model.mSearch.AUTO_OFF_TYPE;

                        listData = ReportServices.searchQMS_RP_OFF_CONTROL_DETAILEx(_db, model, searchOffCalModel, out count, out totalPage, out listPageIndex);
                    }
                    else
                    {
                        //ข้ามได้เลยไม่มี กรณีนี้
                        //listData = ReportServices.searchQMS_RP_OFF_CONTROL_DETAIL(_db, model, out count, out totalPage, out listPageIndex);
                    }

                    if (null != listData && listData.Count() > 0)
                    {
                        long nResult = ReportServices.SaveQMS_RP_OFF_CONTROL_DETAILEx(_db, listData);

                        if (nResult > 0)
                        {
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            //}
            //else
            //{
            //    result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            //}
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_RP_OFF_CONTROL_DETAILBySearch(ReportOffControlDetailSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listData = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();

                    if (model.mSearch.CHK_AUTO_CAL == true && null != model.mSearch.AUTO_START_DATE && null != model.mSearch.AUTO_END_DATE)
                    {
                        OffControlCalAutoSearchModel searchOffCalModel = new OffControlCalAutoSearchModel();
                        searchOffCalModel.PLANT_ID = model.mSearch.PLANT_ID;
                        searchOffCalModel.PRODUCT_ID = model.mSearch.PRODUCT_ID;
                        searchOffCalModel.ROOT_CAUSE_ID = model.mSearch.ROOT_CAUSE_ID;
                        searchOffCalModel.OFF_TYPE = model.mSearch.OFF_TYPE;
                        searchOffCalModel.START_DATE = model.mSearch.AUTO_START_DATE.Value;
                        searchOffCalModel.END_DATE = model.mSearch.AUTO_END_DATE.Value;
                        searchOffCalModel.RPOffControlId = model.mSearch.RP_OFF_CONTROL_ID;
                        searchOffCalModel.AUTO_OFF_TYPE = model.mSearch.AUTO_OFF_TYPE;

                        listData = ReportServices.searchQMS_RP_OFF_CONTROL_DETAILEx(_db, model, searchOffCalModel, out count, out totalPage, out listPageIndex);
                    }
                    else
                    {
                        listData = ReportServices.searchQMS_RP_OFF_CONTROL_DETAIL(_db, model, out count, out totalPage, out listPageIndex);
                    }
                     
                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_RP_OFF_CONTROL_DETAIL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    bool nResult = ReportServices.DeleteQMS_RP_OFF_CONTROL_DETAILByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_RP_OFF_CONTROL_GRAPH(CreateQMS_RP_OFF_CONTROL_GRAPH model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long nResult = ReportServices.SaveQMS_RP_OFF_CONTROL_GRAPH(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_RP_OFF_CONTROL_GRAPHBySearch(ReportOffControlGraphSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_RP_OFF_CONTROL_GRAPH> listData = new List<ViewQMS_RP_OFF_CONTROL_GRAPH>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = ReportServices.searchQMS_RP_OFF_CONTROL_GRAPH(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_RP_OFF_CONTROL_GRAPH(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    bool nResult = ReportServices.DeleteQMS_RP_OFF_CONTROL_GRAPHByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActivelistOffControlGraphList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    List<ViewQMS_RP_OFF_CONTROL_GRAPH> listData = new List<ViewQMS_RP_OFF_CONTROL_GRAPH>();
                    listData = ReportServices.getQMS_RP_OFF_CONTROL_GRAPHActiveList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetOffControlGraphPosition(SetOffControlGraphPosition model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                { 
                    int listData = reportServices.setQMS_RP_OFF_CONTROL_GRAPHPosition(_db, model);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_RP_OFF_CONTROL_ATTACH(CreateQMS_RP_OFF_CONTROL_ATTACH model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long nResult = ReportServices.SaveQMS_RP_OFF_CONTROL_ATTACH(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_RP_OFF_CONTROL_ATTACHBySearch(ReportOffControlAttachSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_RP_OFF_CONTROL_ATTACH> listData = new List<ViewQMS_RP_OFF_CONTROL_ATTACH>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = ReportServices.searchQMS_RP_OFF_CONTROL_ATTACH(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_RP_OFF_CONTROL_ATTACH(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    bool nResult = ReportServices.DeleteQMS_RP_OFF_CONTROL_ATTACHByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActivelistOffControlAttachList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices ReportServices = new ReportServices(_username);

                try
                {
                    List<ViewQMS_RP_OFF_CONTROL_ATTACH> listData = new List<ViewQMS_RP_OFF_CONTROL_ATTACH>();
                    listData = ReportServices.getQMS_RP_OFF_CONTROL_ATTACHActiveList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }        

        public JsonResult CreateQMS_RP_OFF_CONTROL_SUMMARY(CreateQMS_RP_OFF_CONTROL_SUMMARY model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    long nResult = reportServices.SaveQMS_RP_OFF_CONTROL_SUMMARY(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateLISTQMS_RP_OFF_CONTROL_SUMMARY(List<CreateQMS_RP_OFF_CONTROL_SUMMARY> listModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);
                long nResult = 0;
                try
                {

                    nResult = reportServices.SaveListQMS_RP_OFF_CONTROL_SUMMARY(_db, listModel);
                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_RP_OFF_CONTROL_SUMMARY(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    bool nResult = reportServices.DeleteQMS_RP_OFF_CONTROL_SUMMARYByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_RP_OFF_CONTROL_SUMMARYBySearch(ReportOffControlSummarySearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = reportServices.searchQMS_RP_OFF_CONTROL_SUMMARY(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getOffControlByOffControlCalAutoSearch(OffControlCalAutoSearchModel search)
        {
            Object result;
            if (ModelState.IsValid)
            { 
                ReportServices reportServices = new ReportServices(_username);
                TransactionService transService = new TransactionService(_username);
                try
                {
                    ViewALL_QMS_RP_OFF_CONTROL_SUMMARY listData = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
                    //reset date
                    search.START_DATE = new DateTime(search.START_DATE.Year, search.START_DATE.Month, search.START_DATE.Day, 0, 0, 0);
                    search.END_DATE = new DateTime(search.END_DATE.Year, search.END_DATE.Month, search.END_DATE.Day, 23, 59, 59);


                    if (search.CHK_RECALRAW)
                    {
                        reportServices.reCalRawDataByRPOffControlId(_db, search);
                    }
                    
                    listData = reportServices.getTemplateOffControlSummaryWithAutoCalEx(_db, search);
                     
                    long count = 0;
                    long totalPage = 0;
                    ReportOffControlDetailSearch modelSearch = new ReportOffControlDetailSearch();
                    modelSearch.PageIndex = 1;
                    modelSearch.PageSize = 20; //เอาทั้งหมด
                    modelSearch.SortOrder = "";
                    modelSearch.SortOrder = "PLANT_ID";
                    modelSearch.mSearch = new ReportOffControlDetailSearchModel();
                    modelSearch.mSearch.RP_OFF_CONTROL_ID = search.RPOffControlId;
                    modelSearch.mSearch.START_DATE = search.START_DATE;
                    modelSearch.mSearch.END_DATE = search.END_DATE;

                    //reset plant & product id 
                    search.PLANT_ID = 0;
                    search.PRODUCT_ID = 0;

                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listDataDetail = reportServices.searchQMS_RP_OFF_CONTROL_DETAILEx(_db, modelSearch, search,  out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, listDataDetail = listDataDetail, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        } 

        public JsonResult GetTemplateOffControlSummary(long RPOffControlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    ViewALL_QMS_RP_OFF_CONTROL_SUMMARY listData = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
                    //listData = reportServices.getTemplateOffControlSummary(_db, RPOffControlId);
                    listData = reportServices.getTemplateOffControlSummaryEx(_db, RPOffControlId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTemplateOffControlSummary_T0(long RPOffControlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getTemplateOffControlSummary_T0(_db, RPOffControlId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
 
        public JsonResult GetTemplateOffControlSummary_T1(long RPOffControlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getTemplateOffControlSummary_T1(_db, RPOffControlId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTemplateOffControlSummary_T2(long RPOffControlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getTemplateOffControlSummary_T2(_db, RPOffControlId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemOffControlListByProdcutId(long RPOffControlId, long ProductId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    listData = reportServices.getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlProductId(_db, RPOffControlId, ProductId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemOffControlListByProductIdEx(long ProductId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listData = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
                    listData = reportServices.getQMS_RP_OFF_CONTROL_SUMMARYListByRPOffControlProductId(_db, 0, ProductId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult GraphOffControlSummary(long id =0)
        {
            ReportServices reportService = new ReportServices(_username);
            CreateQMS_RP_OFF_CONTROL createQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();
            
            if (id > 0)
            {
                createQMS_RP_OFF_CONTROL = reportService.getQMS_RP_OFF_CONTROLById(_db, id);
            }

            ViewBag.createQMS_RP_OFF_CONTROL = createQMS_RP_OFF_CONTROL;
            ViewBag.id = id;
            return View();
        }


        //OffSpec
        public ActionResult ListOffSpecSummary()//control ListOffSpecSummary.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ReportOffControlSearchModel searchRPOffControl = new ReportOffControlSearchModel();

            CreateQMS_RP_OFF_CONTROL createQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();
            CreateQMS_RP_OFF_CONTROL_DETAIL createQMS_RP_OFF_CONTROL_DETAIL = new CreateQMS_RP_OFF_CONTROL_DETAIL();
            CreateQMS_RP_OFF_CONTROL_GRAPH createQMS_RP_OFF_CONTROL_GRAPH = new CreateQMS_RP_OFF_CONTROL_GRAPH();
            CreateQMS_RP_OFF_CONTROL_ATTACH createQMS_RP_OFF_CONTROL_ATTACH = new CreateQMS_RP_OFF_CONTROL_ATTACH();
            CreateQMS_RP_OFF_CONTROL_SUMMARY createQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();

            ReportOffControlDetailSearchModel searchOffControlDetail = new ReportOffControlDetailSearchModel();
            ReportOffControlGraphSearchModel searchOffControlGraph = new ReportOffControlGraphSearchModel();
            ReportOffControlAttachSearchModel searchOffControlAttach = new ReportOffControlAttachSearchModel();
            ReportOffControlSummarySearchModel searchOffControlSummary = new ReportOffControlSummarySearchModel();

            RPOffControlSummaryRowSearchModel searchRPProduct = new RPOffControlSummaryRowSearchModel();
            RPOffControlSummaryColumnSearchModel searchRPPlant = new RPOffControlSummaryColumnSearchModel();
            RPOffControlSummarySearchModel searchRPOffControlSummary = new RPOffControlSummarySearchModel();

            OffControlCalAutoSearchModel searchAutoCal = new OffControlCalAutoSearchModel();

            createQMS_RP_OFF_CONTROL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.SPEC;

            createQMS_RP_OFF_CONTROL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL.END_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.END_DATE = getDummyDateTime();

            searchOffControlDetail.RP_OFF_CONTROL_ID = 0;
            searchOffControlDetail.OFF_TYPE = (byte)OFF_TYPE.SPEC;
            searchOffControlGraph.RP_OFF_CONTROL_ID = 0;
            searchOffControlAttach.RP_OFF_CONTROL_ID = 0;
            searchOffControlSummary.RP_OFF_CONTROL_ID = 0;
            searchAutoCal.RPOffControlId = 0;
            searchAutoCal.OFF_TYPE = (byte)OFF_TYPE.SPEC;
            searchAutoCal.AUTO_OFF_TYPE = (byte)AUTO_OFF_TYPE.ALL_PENDING;
            searchAutoCal.CHK_CORRECTDATA = true;

            searchAutoCal.LIST_PLANT_ID = new List<long>();
            searchAutoCal.LIST_PRODUCT_ID = new List<long>();
            searchAutoCal.CHK_ALL_PLANT = true;
            searchAutoCal.CHK_ALL_PRODUCT = true;
            searchRPOffControl.OFF_CONTROL_TYPE = (byte)OFF_TYPE.SPEC;

            ViewBag.listReportDocStatus = this.getReportDocStatusList();

            ViewBag.searchRPOffControl = searchRPOffControl;
            ViewBag.listPageSize = this.getPageSizeList();




            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listAutoOffType = getAutoOffControlList();

            ViewBag.createQMS_RP_OFF_CONTROL = createQMS_RP_OFF_CONTROL;
            ViewBag.createQMS_RP_OFF_CONTROL_DETAIL = createQMS_RP_OFF_CONTROL_DETAIL;
            ViewBag.createQMS_RP_OFF_CONTROL_GRAPH = createQMS_RP_OFF_CONTROL_GRAPH;
            ViewBag.createQMS_RP_OFF_CONTROL_ATTACH = createQMS_RP_OFF_CONTROL_ATTACH;
            ViewBag.createQMS_RP_OFF_CONTROL_SUMMARY = createQMS_RP_OFF_CONTROL_SUMMARY;

            ViewBag.searchOffControlDetail = searchOffControlDetail;
            ViewBag.searchOffControlGraph = searchOffControlGraph;
            ViewBag.searchOffControlAttach = searchOffControlAttach;
            ViewBag.searchOffControlSummary = searchOffControlSummary;

            ViewBag.searchRPProduct = searchRPProduct;
            ViewBag.searchRPPlant = searchRPPlant;
            ViewBag.searchRPOffControlSummary = searchRPOffControlSummary;
            ViewBag.searchAutoCal = searchAutoCal;

            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.DateNow = getDummyDateTime();

            return View();
        }

        public ActionResult CreateOffSpecSummary(long id = 0)//control CreateOffControlSummary.cshtml
        {
            ReportServices ReportServices = new ReportServices(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_RP_OFF_CONTROL createQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();
            CreateQMS_RP_OFF_CONTROL_DETAIL createQMS_RP_OFF_CONTROL_DETAIL = new CreateQMS_RP_OFF_CONTROL_DETAIL();
            CreateQMS_RP_OFF_CONTROL_GRAPH createQMS_RP_OFF_CONTROL_GRAPH = new CreateQMS_RP_OFF_CONTROL_GRAPH();
            CreateQMS_RP_OFF_CONTROL_ATTACH createQMS_RP_OFF_CONTROL_ATTACH = new CreateQMS_RP_OFF_CONTROL_ATTACH();
            CreateQMS_RP_OFF_CONTROL_SUMMARY createQMS_RP_OFF_CONTROL_SUMMARY = new CreateQMS_RP_OFF_CONTROL_SUMMARY();

            ReportOffControlDetailSearchModel searchOffControlDetail = new ReportOffControlDetailSearchModel();
            ReportOffControlGraphSearchModel searchOffControlGraph = new ReportOffControlGraphSearchModel();
            ReportOffControlAttachSearchModel searchOffControlAttach = new ReportOffControlAttachSearchModel();
            ReportOffControlSummarySearchModel searchOffControlSummary = new ReportOffControlSummarySearchModel();

            RPOffControlSummaryRowSearchModel searchRPProduct = new RPOffControlSummaryRowSearchModel();
            RPOffControlSummaryColumnSearchModel searchRPPlant = new RPOffControlSummaryColumnSearchModel();
            RPOffControlSummarySearchModel searchRPOffControlSummary = new RPOffControlSummarySearchModel();

            OffControlCalAutoSearchModel searchAutoCal = new OffControlCalAutoSearchModel();

            string actionName = Resources.Strings.AddNew;

            createQMS_RP_OFF_CONTROL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.SPEC;

            createQMS_RP_OFF_CONTROL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL.END_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.START_DATE = getDummyDateTime();
            createQMS_RP_OFF_CONTROL_DETAIL.END_DATE = getDummyDateTime();

            searchOffControlDetail.RP_OFF_CONTROL_ID = id;
            searchOffControlDetail.OFF_TYPE = (byte)OFF_TYPE.SPEC;
            searchOffControlGraph.RP_OFF_CONTROL_ID = id;
            searchOffControlAttach.RP_OFF_CONTROL_ID = id;
            searchOffControlSummary.RP_OFF_CONTROL_ID = id;
            searchAutoCal.RPOffControlId = id;
            searchAutoCal.OFF_TYPE = (byte)OFF_TYPE.SPEC;
            searchAutoCal.AUTO_OFF_TYPE = (byte)AUTO_OFF_TYPE.ALL_PENDING;
            searchAutoCal.CHK_CORRECTDATA = true;

            searchAutoCal.LIST_PLANT_ID = new List<long>();
            searchAutoCal.LIST_PRODUCT_ID = new List<long>();
            searchAutoCal.CHK_ALL_PLANT = true;
            searchAutoCal.CHK_ALL_PRODUCT = true;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_RP_OFF_CONTROL = ReportServices.getQMS_RP_OFF_CONTROLById(_db, id);

            }

            ViewBag.listReportDocStatus = this.getReportDocStatusList();

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listAutoOffType = getAutoOffControlList();

            ViewBag.createQMS_RP_OFF_CONTROL = createQMS_RP_OFF_CONTROL;
            ViewBag.createQMS_RP_OFF_CONTROL_DETAIL = createQMS_RP_OFF_CONTROL_DETAIL;
            ViewBag.createQMS_RP_OFF_CONTROL_GRAPH = createQMS_RP_OFF_CONTROL_GRAPH;
            ViewBag.createQMS_RP_OFF_CONTROL_ATTACH = createQMS_RP_OFF_CONTROL_ATTACH;
            ViewBag.createQMS_RP_OFF_CONTROL_SUMMARY = createQMS_RP_OFF_CONTROL_SUMMARY;

            ViewBag.searchOffControlDetail = searchOffControlDetail;
            ViewBag.searchOffControlGraph = searchOffControlGraph;
            ViewBag.searchOffControlAttach = searchOffControlAttach;
            ViewBag.searchOffControlSummary = searchOffControlSummary;

            ViewBag.searchRPProduct = searchRPProduct;
            ViewBag.searchRPPlant = searchRPPlant;
            ViewBag.searchRPOffControlSummary = searchRPOffControlSummary;
            ViewBag.searchAutoCal = searchAutoCal;

            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.DateNow = getDummyDateTime();

            ViewBag.ActionName = actionName;
            return View();
        }



        public ActionResult GraphOffSpecSummary(long id = 0)
        {
            ReportServices reportService = new ReportServices(_username);
            CreateQMS_RP_OFF_CONTROL createQMS_RP_OFF_CONTROL = new CreateQMS_RP_OFF_CONTROL();

            if (id > 0)
            {
                createQMS_RP_OFF_CONTROL = reportService.getQMS_RP_OFF_CONTROLById(_db, id);
            }

            ViewBag.createQMS_RP_OFF_CONTROL = createQMS_RP_OFF_CONTROL;
            ViewBag.id = id;
            return View();
        }

        public ActionResult OffControlCorrectData()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CorrectDataReportSearchModel searchModel = new CorrectDataReportSearchModel();

            ViewBag.searchModel = searchModel;
            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db); 
            return View();
        }

        public JsonResult GetOffControlCorrectDataBySearch(CorrectDataReportSearchModel search)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    VIEW_CORRECT_SUM_REPORT listData = reportServices.getCORRECT_REPORTByCorrectDataSearch(_db, search); 
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOffControlShiftAnalysisBySearch(ShiftAnalysisReportSearchModel search)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    VIEW_OPERATION_SHIFT_REPORT listData = reportServices.getOffControlShiftAnalysisBySearch(_db, search);
                    result = new { result = ReturnStatus.SUCCESS , message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowOffControlCorrectData()
        {
            return View();
        } 

        public ActionResult OffControlShiftAnalysis()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ShiftAnalysisReportSearchModel searchModel = new ShiftAnalysisReportSearchModel();

            searchModel.OFF_TYPE = (int)OFF_TYPE.CONTROL;

            ViewBag.searchModel = searchModel;
            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listTypeReport = this.getTypeReportList();
            return View();
        }

        public ActionResult ShowOffControlShiftAnalysis()
        {
            return View();
        }

        public ActionResult ListTrendGAS()
        { 
            ReportServices reportDataServices = new ReportServices(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            ReportTrendSearchModel searchTrendReport = new ReportTrendSearchModel();
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();
            CreateQMS_RP_TREND createQMS_RP_TREND = new CreateQMS_RP_TREND();
            List<TrendSample> TrendSampleData1 = new List<TrendSample>();
            List<TrendSample> TrendSampleData2 = new List<TrendSample>();
            List<TrendItem> listItemData1 = new List<TrendItem>();
            List<TrendItem> listItemData2 = new List<TrendItem>();
            string actionName = Resources.Strings.AddNew;

            List<TemplateName_COMBO> listTemplate = new List<TemplateName_COMBO>();
            listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.GAS);

            //createQMS_RP_TREND.ID = id;
            createQMS_RP_TREND.REPORT_MASTER = new CreateQMS_RP_TREND_DATA();
            createQMS_RP_TREND.REPORT_DETAIL_1 = new CreateQMS_RP_TREND_DATA_DETAIL();
            createQMS_RP_TREND.REPORT_DETAIL_2 = new CreateQMS_RP_TREND_DATA_DETAIL();

            createQMS_RP_TREND.REPORT_MASTER.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            createQMS_RP_TREND.REPORT_MASTER.END_DATE = createQMS_RP_TREND.REPORT_MASTER.START_DATE.AddMonths(1).AddMinutes(-1);
             


            searchTrendReport.TREND_TYPE = (byte)RP_TREND_TYPE.GAS;

            ViewBag.searchTrendReport = searchTrendReport;
            ViewBag.listPageSize = this.getPageSizeList();

            createQMS_RP_TREND.REPORT_DETAIL_1.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES1;
            createQMS_RP_TREND.REPORT_DETAIL_2.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES2;
            createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE = (byte)RP_TREND_TYPE.GAS;
            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;

            ViewBag.TrendSampleData1 = TrendSampleData1;
            ViewBag.TrendSampleData2 = TrendSampleData2;
            ViewBag.listItemData1 = listItemData1;
            ViewBag.listItemData2 = listItemData2;
            ViewBag.createQMS_RP_TREND = createQMS_RP_TREND;
            ViewBag.listTemplate = listTemplate;
            ViewBag.ActionName = actionName;
            ViewBag.searhTrendData = searhTrendData;

            return View();
        }

        public ActionResult CreateTrendGAS(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ReportServices repServices = new ReportServices(_username);
            //List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();
            CreateQMS_RP_TREND createQMS_RP_TREND = new CreateQMS_RP_TREND();
            List<TrendSample> TrendSampleData1 = new List<TrendSample>();
            List<TrendSample> TrendSampleData2 = new List<TrendSample>();
            List<TrendItem> listItemData1 = new List<TrendItem>();
            List<TrendItem> listItemData2 = new List<TrendItem>();
            string actionName = Resources.Strings.AddNew;

            List<TemplateName_COMBO> listTemplate = new List<TemplateName_COMBO>();
            listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.GAS);

            createQMS_RP_TREND.ID = id;
            createQMS_RP_TREND.REPORT_MASTER = new CreateQMS_RP_TREND_DATA();
            createQMS_RP_TREND.REPORT_DETAIL_1 = new CreateQMS_RP_TREND_DATA_DETAIL();
            createQMS_RP_TREND.REPORT_DETAIL_2 = new CreateQMS_RP_TREND_DATA_DETAIL();

            createQMS_RP_TREND.REPORT_MASTER.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            createQMS_RP_TREND.REPORT_MASTER.END_DATE = createQMS_RP_TREND.REPORT_MASTER.START_DATE.AddMonths(1).AddMinutes(-1);
             
             
            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_RP_TREND.REPORT_MASTER = repServices.getQMS_RP_TREND_DATAById(_db, id); 
                List<ViewQMS_RP_TREND_DATA_DETAIL> listDetail = repServices.getQMS_RP_TREND_DATA_DETAILListByReportId(_db, id);

                if (listDetail.Count() > 0)
                {
                    
                    foreach (ViewQMS_RP_TREND_DATA_DETAIL dtData in listDetail)
                    {
                        if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES1)
                        {
                            createQMS_RP_TREND.REPORT_DETAIL_1 = repServices.convertViewToModel(dtData);
                        }
                        else if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES2)
                        {
                            createQMS_RP_TREND.REPORT_DETAIL_2 = repServices.convertViewToModel(dtData);
                        }
                    }

                    if (null != createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA )
                    {
                        TrendSampleData1 = masterDataServices.getTrendSampleListByStringTemplateId(_db, createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                        listItemData1 = masterDataServices.getTrendItemListBySampleEx(_db, createQMS_RP_TREND.REPORT_DETAIL_1.SAMPLE_ID, createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                    }

                    if (null != createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA)
                    {
                        TrendSampleData2 = masterDataServices.getTrendSampleListByStringTemplateId(_db, createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                        listItemData2 = masterDataServices.getTrendItemListBySampleEx(_db, createQMS_RP_TREND.REPORT_DETAIL_2.SAMPLE_ID, createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                    }

                }

                
            }

            createQMS_RP_TREND.REPORT_DETAIL_1.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES1;
            createQMS_RP_TREND.REPORT_DETAIL_2.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES2;
            createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE = (byte)RP_TREND_TYPE.GAS;
            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;

            ViewBag.TrendSampleData1 = TrendSampleData1;
            ViewBag.TrendSampleData2 = TrendSampleData2;
            ViewBag.listItemData1 = listItemData1;
            ViewBag.listItemData2 = listItemData2;
            ViewBag.createQMS_RP_TREND = createQMS_RP_TREND;
            ViewBag.listTemplate = listTemplate;
            ViewBag.ActionName = actionName;
            ViewBag.searhTrendData = searhTrendData;
            return View();

        }

        public JsonResult GetcreateQMS_RP_TRENDById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices repServices = new ReportServices(_username);

                try
                {
                    CreateQMS_RP_TREND createQMS_RP_TREND = new CreateQMS_RP_TREND();
                    createQMS_RP_TREND.ID = id;
                    createQMS_RP_TREND.REPORT_MASTER = new CreateQMS_RP_TREND_DATA();
                    createQMS_RP_TREND.REPORT_DETAIL_1 = new CreateQMS_RP_TREND_DATA_DETAIL();
                    createQMS_RP_TREND.REPORT_DETAIL_2 = new CreateQMS_RP_TREND_DATA_DETAIL();

                    createQMS_RP_TREND.REPORT_MASTER.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                    createQMS_RP_TREND.REPORT_MASTER.END_DATE = createQMS_RP_TREND.REPORT_MASTER.START_DATE.AddMonths(1).AddMinutes(-1);
          
                    createQMS_RP_TREND.REPORT_MASTER = repServices.getQMS_RP_TREND_DATAById(_db, id);
                    List<ViewQMS_RP_TREND_DATA_DETAIL> listDetail = repServices.getQMS_RP_TREND_DATA_DETAILListByReportId(_db, id);

                    if (listDetail.Count() > 0)
                    {

                        foreach (ViewQMS_RP_TREND_DATA_DETAIL dtData in listDetail)
                        {
                            if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES1)
                            {
                                createQMS_RP_TREND.REPORT_DETAIL_1 = repServices.convertViewToModel(dtData);
                            }
                            else if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES2)
                            {
                                createQMS_RP_TREND.REPORT_DETAIL_2 = repServices.convertViewToModel(dtData);
                            }
                        }

                        //if (null != createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA)
                        //{
                        //    TrendSampleData1 = masterDataServices.getTrendSampleListByStringTemplateId(_db, createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                        //    listItemData1 = masterDataServices.getTrendItemListBySampleEx(_db, createQMS_RP_TREND.REPORT_DETAIL_1.SAMPLE_ID, createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                        //}

                        //if (null != createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA)
                        //{
                        //    TrendSampleData2 = masterDataServices.getTrendSampleListByStringTemplateId(_db, createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                        //    listItemData2 = masterDataServices.getTrendItemListBySampleEx(_db, createQMS_RP_TREND.REPORT_DETAIL_2.SAMPLE_ID, createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA, (byte)RP_TREND_TYPE.GAS);
                        //}

                    }


                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_RP_TREND };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListTrendUtility()
        {
            ReportServices reportDataServices = new ReportServices(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);

            List<TemplateName_COMBO> listTemplate = new List<TemplateName_COMBO>();
            listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.Utility);

            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();
            CreateQMS_RP_TREND createQMS_RP_TREND = new CreateQMS_RP_TREND();
            List<TrendSample> TrendSampleData1 = new List<TrendSample>();
            List<TrendSample> TrendSampleData2 = new List<TrendSample>();
            List<TrendItem> listItemData1 = new List<TrendItem>();
            List<TrendItem> listItemData2 = new List<TrendItem>();
            ReportTrendSearchModel searchTrendReport = new ReportTrendSearchModel();


            //createQMS_RP_TREND.ID = id;
            createQMS_RP_TREND.REPORT_MASTER = new CreateQMS_RP_TREND_DATA();
            createQMS_RP_TREND.REPORT_DETAIL_1 = new CreateQMS_RP_TREND_DATA_DETAIL();
            createQMS_RP_TREND.REPORT_DETAIL_2 = new CreateQMS_RP_TREND_DATA_DETAIL();
            createQMS_RP_TREND.REPORT_MASTER.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            createQMS_RP_TREND.REPORT_MASTER.END_DATE = createQMS_RP_TREND.REPORT_MASTER.START_DATE.AddMonths(1).AddMinutes(-1);

            createQMS_RP_TREND.REPORT_DETAIL_1.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES1;
            createQMS_RP_TREND.REPORT_DETAIL_2.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES2;
            createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE = (byte)RP_TREND_TYPE.UTILITY;

            searchTrendReport.TREND_TYPE = (byte)RP_TREND_TYPE.UTILITY;
            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;

            ViewBag.searchTrendReport = searchTrendReport;
            ViewBag.listPageSize = this.getPageSizeList();


            ViewBag.TrendSampleData1 = TrendSampleData1;
            ViewBag.TrendSampleData2 = TrendSampleData2;
            ViewBag.listItemData1 = listItemData1;
            ViewBag.listItemData2 = listItemData2;
            ViewBag.createQMS_RP_TREND = createQMS_RP_TREND;
            ViewBag.listTemplate = listTemplate; 
            ViewBag.searhTrendData = searhTrendData;

            return View();
        }

        public ActionResult CreateTrendUtility(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ReportServices repServices = new ReportServices(_username);

            TemplateSearch searchTemplate = new TemplateSearch();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;
            searchTemplate.PageSize = -1;
             
            List<PageIndexList> listPageIndex = new List<PageIndexList>();

            List<TemplateName_COMBO> listTemplate = new List<TemplateName_COMBO>();
            listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.Utility);

            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();
            CreateQMS_RP_TREND createQMS_RP_TREND = new CreateQMS_RP_TREND();
            List<TrendSample> TrendSampleData1 = new List<TrendSample>();
            List<TrendSample> TrendSampleData2 = new List<TrendSample>();
            List<TrendItem> listItemData1 = new List<TrendItem>();
            List<TrendItem> listItemData2 = new List<TrendItem>();
            string actionName = Resources.Strings.AddNew;

            createQMS_RP_TREND.ID = id;
            createQMS_RP_TREND.REPORT_MASTER = new CreateQMS_RP_TREND_DATA();
            createQMS_RP_TREND.REPORT_DETAIL_1 = new CreateQMS_RP_TREND_DATA_DETAIL();
            createQMS_RP_TREND.REPORT_DETAIL_2 = new CreateQMS_RP_TREND_DATA_DETAIL();

            createQMS_RP_TREND.REPORT_MASTER.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            createQMS_RP_TREND.REPORT_MASTER.END_DATE = createQMS_RP_TREND.REPORT_MASTER.START_DATE.AddMonths(1).AddMinutes(-1);
            createQMS_RP_TREND.REPORT_DETAIL_1.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES1;
            createQMS_RP_TREND.REPORT_DETAIL_2.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES2;
            createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE = (byte)RP_TREND_TYPE.UTILITY;
            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;  

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_RP_TREND.REPORT_MASTER = repServices.getQMS_RP_TREND_DATAById(_db, id);
                List<ViewQMS_RP_TREND_DATA_DETAIL> listDetail = repServices.getQMS_RP_TREND_DATA_DETAILListByReportId(_db, id);

                if (listDetail.Count() > 0)
                {

                    foreach (ViewQMS_RP_TREND_DATA_DETAIL dtData in listDetail)
                    {
                        if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES1)
                        {
                            createQMS_RP_TREND.REPORT_DETAIL_1 = repServices.convertViewToModel(dtData);
                        }
                        else if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES2)
                        {
                            createQMS_RP_TREND.REPORT_DETAIL_2 = repServices.convertViewToModel(dtData);
                        }
                    }


                    if (null != createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA  )
                    {
                        TrendSampleData1 = masterDataServices.getTrendSampleListByStringTemplateId(_db, createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA, (byte)RP_TREND_TYPE.UTILITY);
                        listItemData1 = masterDataServices.getTrendItemListBySampleEx(_db, createQMS_RP_TREND.REPORT_DETAIL_1.SAMPLE_ID, createQMS_RP_TREND.REPORT_DETAIL_1.TEMPLATE_AREA, createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE);
                    }

                    if (null != createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA)
                    {
                        TrendSampleData2 = masterDataServices.getTrendSampleListByStringTemplateId(_db, createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA, (byte)RP_TREND_TYPE.UTILITY);
                        listItemData2 = masterDataServices.getTrendItemListBySampleEx(_db, createQMS_RP_TREND.REPORT_DETAIL_2.SAMPLE_ID, createQMS_RP_TREND.REPORT_DETAIL_2.TEMPLATE_AREA, createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE);
                    }

                }


            }

            createQMS_RP_TREND.REPORT_DETAIL_1.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES1;
            createQMS_RP_TREND.REPORT_DETAIL_2.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES2;
            createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE = (byte)RP_TREND_TYPE.UTILITY;
            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;  

            ViewBag.TrendSampleData1 = TrendSampleData1;
            ViewBag.TrendSampleData2 = TrendSampleData2;
            ViewBag.listItemData1 = listItemData1;
            ViewBag.listItemData2 = listItemData2;
            ViewBag.createQMS_RP_TREND = createQMS_RP_TREND;
            ViewBag.listTemplate = listTemplate;
            ViewBag.ActionName = actionName;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }

        public ActionResult ListTrendProduct()
        {

            TransactionService transServices = new TransactionService(_username);

            ReportTrendSearchModel searchTrendReport = new ReportTrendSearchModel();
            TrendCOAGraphSearchModel searhTrendData = new TrendCOAGraphSearchModel();
            CreateQMS_RP_TREND createQMS_RP_TREND = new CreateQMS_RP_TREND();
            List<PRODUCT_COA> listProductCOAData1 = new List<PRODUCT_COA>();
            List<PRODUCT_COA> listProductCOAData2 = new List<PRODUCT_COA>();

            List<CUSTOMER_COA> listCustomerData1 = new List<CUSTOMER_COA>();
            List<CUSTOMER_COA> listCustomerData2 = new List<CUSTOMER_COA>();

            List<ITEM_COA> listItemCOA1 = new List<ITEM_COA>();
            List<ITEM_COA> listItemCOA2 = new List<ITEM_COA>();


            //createQMS_RP_TREND.ID = 0;
            createQMS_RP_TREND.REPORT_MASTER = new CreateQMS_RP_TREND_DATA();
            createQMS_RP_TREND.REPORT_DETAIL_1 = new CreateQMS_RP_TREND_DATA_DETAIL();
            createQMS_RP_TREND.REPORT_DETAIL_2 = new CreateQMS_RP_TREND_DATA_DETAIL();

            createQMS_RP_TREND.REPORT_MASTER.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            createQMS_RP_TREND.REPORT_MASTER.END_DATE = createQMS_RP_TREND.REPORT_MASTER.START_DATE.AddMonths(1).AddMinutes(-1);

            searchTrendReport.TREND_TYPE = (byte)RP_TREND_TYPE.PRODUCT;

            
            createQMS_RP_TREND.REPORT_DETAIL_1.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES1;
            createQMS_RP_TREND.REPORT_DETAIL_2.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES2;
            createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE = (byte)RP_TREND_TYPE.PRODUCT;

            ViewBag.listCustomerData1 = listCustomerData1;
            ViewBag.listCustomerData2 = listCustomerData2;
            ViewBag.listProductCOAData1 = listProductCOAData1;
            ViewBag.listProductCOAData2 = listProductCOAData2;
            ViewBag.listItemCOA1 = listItemCOA1;
            ViewBag.listItemCOA2 = listItemCOA2;
            ViewBag.createQMS_RP_TREND = createQMS_RP_TREND;
            ViewBag.listCustomer = transServices.getCustomerCOAList(_db);
            ViewBag.searhTrendData = searhTrendData; 
            ViewBag.listProduct = transServices.getProductCOAList(_db);

            ViewBag.searchTrendReport = searchTrendReport;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        } 

        public ActionResult CreateTrendProduct(long id = 0)
        {
            TransactionService transServices = new TransactionService(_username);
            ReportServices repServices  = new ReportServices(_username);
            TrendCOAGraphSearchModel searhTrendData = new TrendCOAGraphSearchModel();
            CreateQMS_RP_TREND createQMS_RP_TREND = new CreateQMS_RP_TREND();
            List<PRODUCT_COA> listProductCOAData1 = new List<PRODUCT_COA>();
            List<PRODUCT_COA> listProductCOAData2 = new List<PRODUCT_COA>();

            List<CUSTOMER_COA> listCustomerData1 = new List<CUSTOMER_COA>();
            List<CUSTOMER_COA> listCustomerData2 = new List<CUSTOMER_COA>();

            List<ITEM_COA> listItemCOA1 = new List<ITEM_COA>();
            List<ITEM_COA> listItemCOA2 = new List<ITEM_COA>();

            string actionName = Resources.Strings.AddNew;

            createQMS_RP_TREND.ID = id;
            createQMS_RP_TREND.REPORT_MASTER = new CreateQMS_RP_TREND_DATA();
            createQMS_RP_TREND.REPORT_DETAIL_1 = new CreateQMS_RP_TREND_DATA_DETAIL();
            createQMS_RP_TREND.REPORT_DETAIL_2 = new CreateQMS_RP_TREND_DATA_DETAIL();

            createQMS_RP_TREND.REPORT_MASTER.START_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            createQMS_RP_TREND.REPORT_MASTER.END_DATE = createQMS_RP_TREND.REPORT_MASTER.START_DATE.AddMonths(1).AddMinutes(-1);

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_RP_TREND.REPORT_MASTER = repServices.getQMS_RP_TREND_DATAById(_db, id);
                List<ViewQMS_RP_TREND_DATA_DETAIL> listDetail = repServices.getQMS_RP_TREND_DATA_DETAILListByReportId(_db, id);

                if (listDetail.Count() > 0)
                {
                    foreach(ViewQMS_RP_TREND_DATA_DETAIL dtData in listDetail){
                        if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES1)
                        {
                            createQMS_RP_TREND.REPORT_DETAIL_1 = repServices.convertViewToModel(dtData);
                        }
                        else if (dtData.DATE_TYPE == (byte)RP_TREND_DETAIL_TYPE.SERIES2)
                        {
                            createQMS_RP_TREND.REPORT_DETAIL_2 = repServices.convertViewToModel(dtData);
                        }
                    }

                    if (createQMS_RP_TREND.REPORT_DETAIL_1.CUSTOMER_ID != null)
                    {
                        listCustomerData1 = transServices.getCustomerCOAListByProduct(_db, createQMS_RP_TREND.REPORT_DETAIL_1.PRODUCT_ID);
                        listProductCOAData1 = transServices.getProductCOAListByCustomer(_db, createQMS_RP_TREND.REPORT_DETAIL_1.CUSTOMER_ID);
                        listItemCOA1 = transServices.getItemListByCustomerAndProduct(_db, createQMS_RP_TREND.REPORT_DETAIL_1.PRODUCT_ID, createQMS_RP_TREND.REPORT_DETAIL_1.CUSTOMER_ID);
                    }

                    if (createQMS_RP_TREND.REPORT_DETAIL_2.CUSTOMER_ID != null)
                    {
                        listCustomerData2 = transServices.getCustomerCOAListByProduct(_db, createQMS_RP_TREND.REPORT_DETAIL_2.PRODUCT_ID);
                        listProductCOAData2 = transServices.getProductCOAListByCustomer(_db, createQMS_RP_TREND.REPORT_DETAIL_2.CUSTOMER_ID);
                        listItemCOA2 = transServices.getItemListByCustomerAndProduct(_db, createQMS_RP_TREND.REPORT_DETAIL_2.PRODUCT_ID, createQMS_RP_TREND.REPORT_DETAIL_2.CUSTOMER_ID);
                    }
                     
                }
            }

            createQMS_RP_TREND.REPORT_DETAIL_1.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES1;
            createQMS_RP_TREND.REPORT_DETAIL_2.DATE_TYPE = (byte)RP_TREND_DETAIL_TYPE.SERIES2;
            createQMS_RP_TREND.REPORT_MASTER.TREND_TYPE = (byte)RP_TREND_TYPE.PRODUCT;

            ViewBag.listCustomerData1 = listCustomerData1;
            ViewBag.listCustomerData2 = listCustomerData2;
            ViewBag.listProductCOAData1 = listProductCOAData1;
            ViewBag.listProductCOAData2 = listProductCOAData2;
            ViewBag.listItemCOA1 = listItemCOA1;
            ViewBag.listItemCOA2 = listItemCOA2;
            ViewBag.createQMS_RP_TREND = createQMS_RP_TREND;
            ViewBag.listCustomer = transServices.getCustomerCOAList(_db);
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.ActionName = actionName;
            ViewBag.listProduct = transServices.getProductCOAList(_db); 
            return View();
        }

        public JsonResult CreateQMS_RP_TREND(CreateQMS_RP_TREND model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportDataServices = new ReportServices(_username);

                try
                {
                    SaveResultQMS_RP_TREND nResult = reportDataServices.SaveQMS_RP_TREND(_db, model);

                    if (nResult.REPORT_MASTER > 0)
                    {
                        model.REPORT_MASTER = reportDataServices.getQMS_RP_TREND_DATAById(_db, nResult.REPORT_MASTER);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model.REPORT_MASTER };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_RP_TRENDBySearch(ReportTrendSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportDataServices = new ReportServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_RP_TREND> listData = new List<ViewQMS_RP_TREND>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();

                    listData = reportDataServices.searchQMS_RP_TREND(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_RP_TREND_DATA(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    bool nResult = reportServices.DeleteQMS_RP_TREND_DATAByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getVolumeByVoulmeEXQSearch(VoulmeEXQSearchModel search)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);

                try
                {
                    decimal volume = transServices.GetVolumeByVoulmeEXQSearch(_db, search);
                    result = new { result = ReturnStatus.SUCCESS, message =  String.Format("{0:#0.000}", volume) };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
         
        public JsonResult getAutoOffControlCalByOffControlCalAutoReport(OffControlCalAutoReportSearchModel search)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);

                try
                {
                    decimal volume = 0;// transServices.GetVolumeByVoulmeEXQSearch(_db, search);
                    result = new { result = ReturnStatus.SUCCESS, message = String.Format("{0:#0.000}", volume) };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult OffControlCorrectDataExcel(long PLANT_ID, DateTime START_DATE, DateTime END_DATE)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    
                    CorrectDataReportSearchModel search = new CorrectDataReportSearchModel();
                    search.PLANT_ID = PLANT_ID;
                    search.START_DATE = START_DATE;
                    search.END_DATE = END_DATE;
                    VIEW_CORRECT_SUM_REPORT listData = reportServices.getCORRECT_REPORTByCorrectDataSearch(_db, search);

                    ExcelPackage excel = reportServices.OffControlCorrrectDataExcel(_db, listData);

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.OffControlCorrectData + ".xlsx");
                    Response.BinaryWrite(excel.GetAsByteArray());

                    result = new { result = ReturnStatus.SUCCESS, message = listData };

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OffControlShiftAnalysisExcel(long PLANT_ID, DateTime START_DATE, DateTime END_DATE, byte OFF_TYPE)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {

                    ShiftAnalysisReportSearchModel search = new ShiftAnalysisReportSearchModel();
                    search.PLANT_ID = PLANT_ID;
                    search.START_DATE = START_DATE;
                    search.END_DATE = END_DATE;
                    search.OFF_TYPE = OFF_TYPE;
                    VIEW_OPERATION_SHIFT_REPORT listData = reportServices.getOffControlShiftAnalysisBySearch(_db, search);

                    ExcelPackage excel = reportServices.OffControlShiftAnalysisExcel(_db, listData);

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.OffControlShiftAnalysis + ".xlsx");
                    Response.BinaryWrite(excel.GetAsByteArray());

                    result = new { result = ReturnStatus.SUCCESS, message = listData };

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public void GetTemplateShiftAnalysisPDF(int bDownload, long PLANT_ID, DateTime START_DATE, DateTime END_DATE, byte OFF_TYPE)
        {
            MemoryStream ms = new MemoryStream();
            PrintPDFHeader ePdf = new PrintPDFHeader();
            Phrase phrase = new Phrase();
            Document pdfDoc = new Document();//(new Rectangle(288f, 144f), 22, 22, 75, 70); 
            pdfDoc.SetMargins(22, 22, 75, 40);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);

            
            #region getData
                ReportServices reportServices = new ReportServices(_username);
                ShiftAnalysisReportSearchModel search = new ShiftAnalysisReportSearchModel();
                search.PLANT_ID = PLANT_ID;
                search.START_DATE = START_DATE;
                search.END_DATE = END_DATE;
                search.OFF_TYPE = OFF_TYPE;
                VIEW_OPERATION_SHIFT_REPORT listData = reportServices.getOffControlShiftAnalysisBySearch(_db, search);
            #endregion


            try
            {
                initialFont();

                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);


                #region setHeader

                String szHead = "\n\n" + @Resources.Strings.OffControlShiftAnalysis + "\n";
                szHead += listData.header1 + "\n";
                szHead += listData.header2 + "\n";

                Paragraph p1 = new Paragraph(szHead, fnt14);
                PdfPTable head = new PdfPTable(5);
                PdfPCell dataCol = new PdfPCell();
                dataCol = new PdfPCell(p1);
                dataCol.Colspan = 5;
                dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                dataCol.Border = PdfPCell.NO_BORDER;
                head.HorizontalAlignment = Element.ALIGN_CENTER;
                head.AddCell(dataCol);
                head.CompleteRow();
                                
                Rectangle page = pdfDoc.PageSize;
                head.TotalWidth = page.Width - 22f; //magin right 
                head.HorizontalAlignment = Element.ALIGN_CENTER;

                writer.PageEvent = ePdf;

                float nTop = pdfDoc.GetTop(0) + 140f;
                //ePdf.ImageHeader = image;
                ePdf.ScalePercent = 50f;
                ePdf.xImage = 20f;
                ePdf.yImage = nTop;

                ePdf.fontHeader = fntHeader;
                ePdf.head = head;
                ePdf.bsFont = bf;

                #endregion

                pdfDoc.Open();

                try
                {
                    
                    if (listData.listShiftDetail.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();

                        szHead = "\n" + "Shift Detail" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt14));
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 5;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f ,80f, 80f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);

                        body2.CompleteRow();

                        foreach (VIEW_SHIFT_REPORT dtReport in listData.listShiftDetail)
                        {

                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.SHIFT_NAME, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.PRODUCT_NAME, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol5 = new PdfPCell(new Phrase(showData, fnt12));
                                      
                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);
                            
                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                    }
                    else
                    {
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();

                        szHead = "\n" + "Shift Detail" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt14));
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 5;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 80f, 80f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        dataCol = new PdfPCell(new Phrase("No data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        PdfPTable body0 = new PdfPTable(1); //820/42

                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;

                        dataCol = new PdfPCell(new Phrase(szHead, fnt14));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol.Border = PdfPCell.NO_BORDER;
                        body0.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);

                        
                    }


                    
                    if (listData.listSummary.Count() > 0)
                    {
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();

                        szHead = "\n" + "Summary" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42
                        
                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt14));
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 3;
                        
                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 140f, 140f, 100f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;


                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);

                        body2.CompleteRow();

                        foreach (VIEW_SHIFT_SUM_REPORT dtReport in listData.listSummary)
                        {

                            string showData = "";
                            dataCol1 = new PdfPCell(new Phrase(dtReport.SHIFT_NAME, fnt12));

                            dataCol2 = new PdfPCell(new Phrase(dtReport.PRODUCT_NAME, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol3 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);

                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                    }
                    else
                    {
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();

                        szHead = "\n" + "Summary" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt14));
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 3;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 80f, 80f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;                        

                        dataCol1 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        dataCol = new PdfPCell(new Phrase("No data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        PdfPTable body0 = new PdfPTable(1); //820/42

                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;

                        dataCol = new PdfPCell(new Phrase(szHead, fnt14));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol.Border = PdfPCell.NO_BORDER;
                        body0.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);
                    }
                }
                catch (Exception ex)
                {
                    phrase.Add(new Chunk(ex.Message));
                    pdfDoc.Add(phrase);
                }
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();

            }
            catch (Exception ex)
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);
                pdfDoc.Open();
                phrase.Add(new Chunk(ex.Message));
                pdfDoc.Add(phrase);
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();
            }



            // กำหนด ContentType เป็น application/pdf เพื่อ Response เป็น Pdf
            Response.ContentType = "application/pdf";

            // กำหนดชื่อไฟล์ที่ต้องการ Export
            if (bDownload > 0)
                Response.AddHeader("content-disposition", "attachment; filename=" + "UserReport_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf");
            // Export Pdf
            Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            Response.End();
        }
        
        [HttpGet]
        public void GetTemplateCorrectDataPDF(int bDownload, long PLANT_ID, DateTime START_DATE, DateTime END_DATE)
        {
            MemoryStream ms = new MemoryStream();
            PrintPDFHeader ePdf = new PrintPDFHeader();
            Phrase phrase = new Phrase();
            Document pdfDoc = new Document();//(new Rectangle(288f, 144f), 22, 22, 75, 70); 
            pdfDoc.SetMargins(22, 22, 75, 40);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);


            #region getData
            ReportServices reportServices = new ReportServices(_username);
            CorrectDataReportSearchModel search = new CorrectDataReportSearchModel();
            search.PLANT_ID = PLANT_ID;
            search.START_DATE = START_DATE;
            search.END_DATE = END_DATE;
            VIEW_CORRECT_SUM_REPORT listData = reportServices.getCORRECT_REPORTByCorrectDataSearch(_db, search);
            #endregion


            try
            {
                initialFont();

                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);


                #region setHeader

                String szHead = "\n\n" + @Resources.Strings.OffControlCorrectData + "\n";
                szHead += listData.header1 + "\n";
                szHead += listData.header2 + "\n";

                Paragraph p1 = new Paragraph(szHead, fnt14);
                PdfPTable head = new PdfPTable(5);
                PdfPCell dataCol = new PdfPCell();
                dataCol = new PdfPCell(p1);
                dataCol.Colspan = 6;
                dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                dataCol.Border = PdfPCell.NO_BORDER;
                head.HorizontalAlignment = Element.ALIGN_CENTER;
                head.AddCell(dataCol);
                head.CompleteRow();

                Rectangle page = pdfDoc.PageSize;
                head.TotalWidth = page.Width - 22f; //magin right 
                head.HorizontalAlignment = Element.ALIGN_CENTER;

                writer.PageEvent = ePdf;

                float nTop = pdfDoc.GetTop(0) + 140f;
                //ePdf.ImageHeader = image;
                ePdf.ScalePercent = 50f;
                ePdf.xImage = 20f;
                ePdf.yImage = nTop;

                ePdf.fontHeader = fntHeader;
                ePdf.head = head;
                ePdf.bsFont = bf;

                #endregion

                pdfDoc.Open();

                try
                {

                    if (listData.listTurnAroundResult.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();

                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        szHead = "Turn Around" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 5;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 180f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);

                        body2.CompleteRow();

                        foreach (VIEW_CORRECT_REPORT dtReport in listData.listTurnAroundResult)
                        {

                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.SHIFT, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.DESC, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol5 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);

                            body2.CompleteRow();

                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                        PdfPCell dataColTotal1 = new PdfPCell();
                        PdfPCell dataColTotal2 = new PdfPCell();
                        PdfPTable bodyTotal = new PdfPTable(2); //820/42
                        float[] widthsTotal = new float[] { 380f , 80f };
                        bodyTotal.SetWidths(widthsTotal);
                        bodyTotal.WidthPercentage = 100;

                        dataColTotal1 = new PdfPCell(new Phrase("Total" + "\n\n", fnt12));
                        dataColTotal1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        dataColTotal2 = new PdfPCell(new Phrase(listData.totalVolTurnAround.ToString("###,##0.00"), fnt12));
                        dataColTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        //dataColTotal1.Border = PdfPCell.NO_BORDER;
                        bodyTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        bodyTotal.AddCell(dataColTotal1);
                        bodyTotal.AddCell(dataColTotal2);
                        bodyTotal.CompleteRow();
                        pdfDoc.Add(bodyTotal);

                    }
                    else
                    {
                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();

                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Turn Around" + "\n\n", fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        PdfPTable body2 = new PdfPTable(5); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 180f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;
                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        PdfPTable body0 = new PdfPTable(1); //820/42
                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;
                        dataCol = new PdfPCell(new Phrase("no data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);
                    }


                    if (listData.listDownTimeResult.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();

                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        szHead = "DownTime" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 5;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 180f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);

                        body2.CompleteRow();

                        foreach (VIEW_CORRECT_REPORT dtReport in listData.listDownTimeResult)
                        {

                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.SHIFT, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.DESC, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol5 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);

                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                        PdfPCell dataColTotal1 = new PdfPCell();
                        PdfPCell dataColTotal2 = new PdfPCell();
                        PdfPTable bodyTotal = new PdfPTable(2); //820/42
                        float[] widthsTotal = new float[] { 380f, 80f };
                        bodyTotal.SetWidths(widthsTotal);
                        bodyTotal.WidthPercentage = 100;

                        dataColTotal1 = new PdfPCell(new Phrase("Total" + "\n\n", fnt12));
                        dataColTotal1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        dataColTotal2 = new PdfPCell(new Phrase(listData.totalVolDownTime.ToString("###,##0.00"), fnt12));
                        dataColTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        //dataColTotal1.Border = PdfPCell.NO_BORDER;
                        bodyTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        bodyTotal.AddCell(dataColTotal1);
                        bodyTotal.AddCell(dataColTotal2);
                        bodyTotal.CompleteRow();
                        pdfDoc.Add(bodyTotal);
                    }
                    else
                    {
                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();


                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("DownTime" + "\n\n", fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);
                      
                        PdfPTable body2 = new PdfPTable(5); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 180f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;
                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        PdfPTable body0 = new PdfPTable(1); //820/42
                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;
                        dataCol = new PdfPCell(new Phrase("no data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);
                    }


                    if (listData.listChangeGradeResult.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        szHead = "Change Grade" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 6;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f , 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);

                        body2.CompleteRow();

                        foreach (VIEW_CORRECT_REPORT dtReport in listData.listChangeGradeResult)
                        {

                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.PRODUCT_NAME, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.SHIFT, fnt12));

                            dataCol5 = new PdfPCell(new Phrase(dtReport.DESC, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol6 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol6.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);
                            body2.AddCell(dataCol6);
                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                        PdfPCell dataColTotal1 = new PdfPCell();
                        PdfPCell dataColTotal2 = new PdfPCell();
                        PdfPTable bodyTotal = new PdfPTable(2); //820/42
                        float[] widthsTotal = new float[] { 380f, 80f };
                        bodyTotal.SetWidths(widthsTotal);
                        bodyTotal.WidthPercentage = 100;

                        dataColTotal1 = new PdfPCell(new Phrase("Total" + "\n\n", fnt12));
                        dataColTotal1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        dataColTotal2 = new PdfPCell(new Phrase(listData.totalVolChangeGrade.ToString("###,##0.00"), fnt12));
                        dataColTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        //dataColTotal1.Border = PdfPCell.NO_BORDER;
                        bodyTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        bodyTotal.AddCell(dataColTotal1);
                        bodyTotal.AddCell(dataColTotal2);
                        bodyTotal.CompleteRow();
                        pdfDoc.Add(bodyTotal);
                    }
                    else
                    {
                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Change Grade" + "\n\n", fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        PdfPTable body2 = new PdfPTable(6); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;
                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        PdfPTable body0 = new PdfPTable(1); //820/42
                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;
                        dataCol = new PdfPCell(new Phrase("no data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);
                    }


                    if (listData.listCrossVolumeResult.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        szHead = "Cross Volume" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 6;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);

                        body2.CompleteRow();

                        foreach (VIEW_CORRECT_REPORT dtReport in listData.listCrossVolumeResult)
                        {
                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.PRODUCT_NAME, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.SHIFT, fnt12));

                            dataCol5 = new PdfPCell(new Phrase(dtReport.DESC, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol6 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol6.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);
                            body2.AddCell(dataCol6);
                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                        PdfPCell dataColTotal1 = new PdfPCell();
                        PdfPCell dataColTotal2 = new PdfPCell();
                        PdfPTable bodyTotal = new PdfPTable(2); //820/42
                        float[] widthsTotal = new float[] { 380f, 80f };
                        bodyTotal.SetWidths(widthsTotal);
                        bodyTotal.WidthPercentage = 100;

                        dataColTotal1 = new PdfPCell(new Phrase("Total" + "\n\n", fnt12));
                        dataColTotal1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        dataColTotal2 = new PdfPCell(new Phrase(listData.totalVolCross.ToString("###,##0.00"), fnt12));
                        dataColTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        //dataColTotal1.Border = PdfPCell.NO_BORDER;
                        bodyTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        bodyTotal.AddCell(dataColTotal1);
                        bodyTotal.AddCell(dataColTotal2);
                        bodyTotal.CompleteRow();
                        pdfDoc.Add(bodyTotal);
                    }
                    else
                    {
                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Cross Volume" + "\n\n", fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        PdfPTable body2 = new PdfPTable(6); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;
                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        PdfPTable body0 = new PdfPTable(1); //820/42
                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;
                        dataCol = new PdfPCell(new Phrase("no data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);
                    }


                    if (listData.listAbnormalResult.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        szHead = "Abnormal" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 6;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);

                        body2.CompleteRow();

                        foreach (VIEW_CORRECT_REPORT dtReport in listData.listAbnormalResult)
                        {

                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.PRODUCT_NAME, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.SHIFT, fnt12));

                            dataCol5 = new PdfPCell(new Phrase(dtReport.DESC, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol6 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol6.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);
                            body2.AddCell(dataCol6);
                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                        PdfPCell dataColTotal1 = new PdfPCell();
                        PdfPCell dataColTotal2 = new PdfPCell();
                        PdfPTable bodyTotal = new PdfPTable(2); //820/42
                        float[] widthsTotal = new float[] { 380f, 80f };
                        bodyTotal.SetWidths(widthsTotal);
                        bodyTotal.WidthPercentage = 100;

                        dataColTotal1 = new PdfPCell(new Phrase("Total" + "\n\n", fnt12));
                        dataColTotal1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        dataColTotal2 = new PdfPCell(new Phrase(listData.totalVolAbnormal.ToString("###,##0.00"), fnt12));
                        dataColTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        //dataColTotal1.Border = PdfPCell.NO_BORDER;
                        bodyTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        bodyTotal.AddCell(dataColTotal1);
                        bodyTotal.AddCell(dataColTotal2);
                        bodyTotal.CompleteRow();
                        pdfDoc.Add(bodyTotal);
                    }
                    else
                    {
                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Abnormal" + "\n\n", fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        PdfPTable body2 = new PdfPTable(6); //820/42


                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;
                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        PdfPTable body0 = new PdfPTable(1); //820/42
                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;
                        dataCol = new PdfPCell(new Phrase("no data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);
                    }


                    if (listData.listReduceFeedResult.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();

                        szHead = "Reduce Feed" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 5;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 180f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);

                        body2.CompleteRow();

                        foreach (VIEW_CORRECT_REPORT dtReport in listData.listReduceFeedResult)
                        {

                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.SHIFT, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.DESC, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol5 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);
                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                        PdfPCell dataColTotal1 = new PdfPCell();
                        PdfPCell dataColTotal2 = new PdfPCell();
                        PdfPTable bodyTotal = new PdfPTable(2); //820/42
                        float[] widthsTotal = new float[] { 380f, 80f };
                        bodyTotal.SetWidths(widthsTotal);
                        bodyTotal.WidthPercentage = 100;

                        dataColTotal1 = new PdfPCell(new Phrase("Total" + "\n\n", fnt12));
                        dataColTotal1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        dataColTotal2 = new PdfPCell(new Phrase(listData.totalVolReduceFeed.ToString("###,##0.00"), fnt12));
                        dataColTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        //dataColTotal1.Border = PdfPCell.NO_BORDER;
                        bodyTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        bodyTotal.AddCell(dataColTotal1);
                        bodyTotal.AddCell(dataColTotal2);
                        bodyTotal.CompleteRow();
                        pdfDoc.Add(bodyTotal);
                    }
                    else
                    {
                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();

                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Reduce Feed" + "\n\n", fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        PdfPTable body2 = new PdfPTable(5); //820/42

                        float[] widths2 = new float[] { 70f, 70f,  60f, 180f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;
                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        PdfPTable body0 = new PdfPTable(1); //820/42
                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;
                        dataCol = new PdfPCell(new Phrase("no data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);  
                    }



                    if (listData.listExceptionResult.Count() > 0)
                    {

                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        szHead = "Exception" + "\n\n";

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase(szHead, fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        int maxWidth = 6;

                        PdfPTable body2 = new PdfPTable(maxWidth); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);

                        body2.CompleteRow();

                        foreach (VIEW_CORRECT_REPORT dtReport in listData.listExceptionResult)
                        {

                            string showData = "";
                            showData = dtReport.START_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol1 = new PdfPCell(new Phrase(showData, fnt12));

                            showData = dtReport.END_DATE.ToString("mm/dd/yyyy HH:mm:ss",new CultureInfo("en-US"));
                            dataCol2 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol3 = new PdfPCell(new Phrase(dtReport.PRODUCT_NAME, fnt12));

                            dataCol4 = new PdfPCell(new Phrase(dtReport.SHIFT, fnt12));

                            dataCol5 = new PdfPCell(new Phrase(dtReport.DESC, fnt12));

                            showData = dtReport.VOLUME.ToString("###,##0.00");
                            dataCol6 = new PdfPCell(new Phrase(showData, fnt12));

                            dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                            dataCol6.HorizontalAlignment = Element.ALIGN_RIGHT;
                            body2.AddCell(dataCol1);
                            body2.AddCell(dataCol2);
                            body2.AddCell(dataCol3);
                            body2.AddCell(dataCol4);
                            body2.AddCell(dataCol5);
                            body2.AddCell(dataCol6);
                            body2.CompleteRow();
                        }
                        body2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body2);

                        PdfPCell dataColTotal1 = new PdfPCell();
                        PdfPCell dataColTotal2 = new PdfPCell();
                        PdfPTable bodyTotal = new PdfPTable(2); //820/42
                        float[] widthsTotal = new float[] { 380f, 80f };
                        bodyTotal.SetWidths(widthsTotal);
                        bodyTotal.WidthPercentage = 100;

                        dataColTotal1 = new PdfPCell(new Phrase("Total" + "\n\n", fnt12));
                        dataColTotal1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        dataColTotal2 = new PdfPCell(new Phrase(listData.totalVolException.ToString("###,##0.00"), fnt12));
                        dataColTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        //dataColTotal1.Border = PdfPCell.NO_BORDER;
                        bodyTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        bodyTotal.AddCell(dataColTotal1);
                        bodyTotal.AddCell(dataColTotal2);
                        bodyTotal.CompleteRow();
                        pdfDoc.Add(bodyTotal);
                    }
                    else
                    {
                        PdfPCell dataCol0 = new PdfPCell();
                        PdfPCell dataCol1 = new PdfPCell();
                        PdfPCell dataCol2 = new PdfPCell();
                        PdfPCell dataCol3 = new PdfPCell();
                        PdfPCell dataCol4 = new PdfPCell();
                        PdfPCell dataCol5 = new PdfPCell();
                        PdfPCell dataCol6 = new PdfPCell();

                        PdfPTable body = new PdfPTable(1); //820/42
                        float[] widths = new float[] { 140f };
                        body.SetWidths(widths);
                        body.WidthPercentage = 100;

                        dataCol0 = new PdfPCell(new Phrase("" + "\n\n"));
                        dataCol0.Border = PdfPCell.NO_BORDER;
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        body.AddCell(dataCol0);
                        body.CompleteRow();
                        pdfDoc.Add(body);

                        PdfPTable body1 = new PdfPTable(1); //820/42

                        float[] widths1 = new float[] { 140f };
                        body1.SetWidths(widths1);
                        body1.WidthPercentage = 100;

                        dataCol1 = new PdfPCell(new Phrase("Exception" + "\n\n", fnt12White));
                        dataCol1.BackgroundColor = dBlue;
                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol1.Border = PdfPCell.NO_BORDER;
                        body1.HorizontalAlignment = Element.ALIGN_CENTER;
                        body1.AddCell(dataCol1);
                        body1.CompleteRow();
                        pdfDoc.Add(body1);

                        PdfPTable body2 = new PdfPTable(6); //820/42

                        float[] widths2 = new float[] { 70f, 70f, 60f, 60f, 120f, 80f };
                        body2.SetWidths(widths2);
                        body2.WidthPercentage = 100;
                        dataCol1 = new PdfPCell(new Phrase("Start Date", fnt12White));
                        dataCol1.BackgroundColor = dBlue;

                        dataCol2 = new PdfPCell(new Phrase("End Date", fnt12White));
                        dataCol2.BackgroundColor = dBlue;

                        dataCol3 = new PdfPCell(new Phrase("Product", fnt12White));
                        dataCol3.BackgroundColor = dBlue;

                        dataCol4 = new PdfPCell(new Phrase("Shift", fnt12White));
                        dataCol4.BackgroundColor = dBlue;

                        dataCol5 = new PdfPCell(new Phrase("Description", fnt12White));
                        dataCol5.BackgroundColor = dBlue;

                        dataCol6 = new PdfPCell(new Phrase("Volume(TON)", fnt12White));
                        dataCol6.BackgroundColor = dBlue;

                        dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol2.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol3.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol4.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol5.HorizontalAlignment = Element.ALIGN_CENTER;
                        dataCol6.HorizontalAlignment = Element.ALIGN_CENTER;
                        body2.AddCell(dataCol1);
                        body2.AddCell(dataCol2);
                        body2.AddCell(dataCol3);
                        body2.AddCell(dataCol4);
                        body2.AddCell(dataCol5);
                        body2.AddCell(dataCol6);
                        body2.CompleteRow();
                        pdfDoc.Add(body2);

                        PdfPTable body0 = new PdfPTable(1); //820/42
                        float[] widths0 = new float[] { 140f };
                        body0.SetWidths(widths0);
                        body0.WidthPercentage = 100;
                        dataCol = new PdfPCell(new Phrase("no data", fnt12));
                        dataCol.HorizontalAlignment = Element.ALIGN_CENTER;
                        body0.AddCell(dataCol);
                        body0.CompleteRow();
                        pdfDoc.Add(body0);                        
                    }

                }
                catch (Exception ex)
                {
                    phrase.Add(new Chunk(ex.Message));
                    pdfDoc.Add(phrase);
                }
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();

            }
            catch (Exception ex)
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);
                pdfDoc.Open();
                phrase.Add(new Chunk(ex.Message));
                pdfDoc.Add(phrase);
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();
            }



            // กำหนด ContentType เป็น application/pdf เพื่อ Response เป็น Pdf
            Response.ContentType = "application/pdf";

            // กำหนดชื่อไฟล์ที่ต้องการ Export
            if (bDownload > 0)
                Response.AddHeader("content-disposition", "attachment; filename=" + "UserReport_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf");
            // Export Pdf
            Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            Response.End();
        }


    }
}
