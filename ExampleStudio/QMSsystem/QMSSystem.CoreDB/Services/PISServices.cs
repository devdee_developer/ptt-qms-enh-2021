﻿using Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using QMSSystem.CoreDB.Helper;
using QMSSystem.CoreDB.Helper.Grid;
using QMSSystem.ExaDB;
using QMSSystem.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace QMSSystem.CoreDB.Services
{
    public class PISServices : BaseService
    {
        protected string m_last_msg_api = "";
        public int m_totalPage = 1;
        public WSO2TokenMod getToken(String apiUrl, String param) //Telnet .net
        {
            WSO2TokenMod responseResult = new WSO2TokenMod();
            string szResult;
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                string postData = "grant_type=client_credentials";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
                // sendMessage.hostname);// (HttpWebRequest)WebRequest.Create(sendMessage.hostname + param);
                request.Method = "POST"; // POST
                byte[] paramAsBytes = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = paramAsBytes.Length;
                request.ContentType = "application/x-www-form-urlencoded";// "application /json;charset=UTF-8";
                //request.Headers.Add("X-TOKEN", token);
                request.Headers.Add("Authorization", "Basic " + param);
                using (Stream stream = request.GetRequestStream())
                {

                    stream.Write(paramAsBytes, 0, paramAsBytes.Count());
                    stream.Close();
                }
                HttpWebResponse response;
                using (response = (HttpWebResponse)request.GetResponse())
                {
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    szResult = responseString.ToString();
                }

                responseResult = JsonConvert.DeserializeObject<WSO2TokenMod>(szResult);
            }
            catch (WebException ex)
            {
                if (null != ex.Response)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            szResult = "Error: " + ex.Message + " : " + reader.ToString();
                        }
                    }
                }
                else
                {
                    szResult = "Error: " + ex.Message;
                }
            }
            catch (Exception ex)
            {
                szResult = ex.Message;
            }

            return responseResult;
        }
        public List<UnitMod> getUnitInfo(SearchUnitMod searchModel, string apiTokenUrl, String basicAuthen, String unitUrl) //Telnet .net
        {
            List<UnitMod> listResult = new List<UnitMod>();
            string szResult = "";
            m_last_msg_api = "";
            WSO2TokenMod wso2Token = new WSO2TokenMod();
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });


                wso2Token = this.getToken(apiTokenUrl, basicAuthen);
                string szQuery = GetQueryString(searchModel);

                string jsonData = JsonConvert.SerializeObject(searchModel);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(unitUrl + "?" + szQuery);
                // sendMessage.hostname);// (HttpWebRequest)WebRequest.Create(sendMessage.hostname + param);
                // request.Method = "POST";
                request.Method = "GET";
                //byte[] paramAsBytes = Encoding.UTF8.GetBytes(jsonData);
                //request.ContentLength = paramAsBytes.Length;
                //request.ContentType = "application/json;charset=UTF-8";

                request.Headers.Add("Authorization", "Bearer " + wso2Token.access_token);
                //request.Headers.Add("X-TOKEN", token);

                //using (Stream stream = request.GetRequestStream())
                //{

                //     stream.Write(paramAsBytes, 0, paramAsBytes.Count());
                //    stream.Close();
                //}
                HttpWebResponse response;
                using (response = (HttpWebResponse)request.GetResponse())
                {
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    szResult = responseString.ToString();
                }
                m_last_msg_api = szResult;
                dynamic jsonResult = JsonConvert.DeserializeObject(szResult);
                foreach (var unitData in jsonResult.Entries.Entry)
                {

                    //foreach (var unitObject in unitData)
                    //{
                    UnitMod dataUnitMod = JsonConvert.DeserializeObject<UnitMod>(unitData.ToString());
                    listResult.Add(dataUnitMod);
                    //var value = meterData;
                    //}
                    // check if the value is not null or empty.

                }

                m_totalPage = Convert.ToInt32(jsonResult.Entries.PageInfo.TotalPage);

                //listResult = JsonConvert.DeserializeObject<List<UnitMod>>(szResult);
            }
            catch (WebException ex)
            {
                if (null != ex.Response)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            m_last_msg_api = "Error: " + ex.Message + " : " + reader.ToString();
                        }
                    }
                }
                else
                {
                    m_last_msg_api = "Error: " + ex.Message + " token : " + wso2Token.access_token + "url " + apiTokenUrl;
                }
            }
            catch (Exception ex)
            {
                m_last_msg_api = ex.Message;
            }

            return listResult;
        }

        public List<PersonnelInfoMod> getPersonnelInfo(SearchPersonnelInfoMod searchModel, string apiTokenUrl, String basicAuthen, string personInfoUrl) //Telnet .net
        {
            List<PersonnelInfoMod> listResult = new List<PersonnelInfoMod>();
            string szResult = "SUCCESS";
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });

                //String apiTokenUrl = ConfigurationManager.AppSettings["API_PIS_TOKEN"];
                //String basicAuthen = ConfigurationManager.AppSettings["API_PIS_BASIC_AUTHEN"];
                //String personInfoUrl = ConfigurationManager.AppSettings["API_PIS_PERSONINFO"];

                WSO2TokenMod wso2Token = this.getToken(apiTokenUrl, basicAuthen);

                string szQuery = GetQueryString(searchModel);

                string jsonData = JsonConvert.SerializeObject(searchModel);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(personInfoUrl + "?" + szQuery);
                // sendMessage.hostname);// (HttpWebRequest)WebRequest.Create(sendMessage.hostname + param);
                // request.Method = "POST";
                request.Method = "GET";
                //byte[] paramAsBytes = Encoding.UTF8.GetBytes(jsonData);
                //request.ContentLength = paramAsBytes.Length;
                //request.ContentType = "application/json;charset=UTF-8";

                request.Headers.Add("Authorization", "Bearer " + wso2Token.access_token);
                //request.Headers.Add("X-TOKEN", token);

                //using (Stream stream = request.GetRequestStream())
                //{

                //     stream.Write(paramAsBytes, 0, paramAsBytes.Count());
                //    stream.Close();
                //}
                HttpWebResponse response;
                using (response = (HttpWebResponse)request.GetResponse())
                {
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    szResult = responseString.ToString();
                }

                //listResult = JsonConvert.DeserializeObject<List<PersonnelInfoMod>>(szResult);
                dynamic jsonResult = JsonConvert.DeserializeObject(szResult);
                foreach (var personData in jsonResult.Entries.Entry)
                {

                    //foreach (var unitObject in unitData)
                    //{
                    PersonnelInfoMod dataPersonMod = JsonConvert.DeserializeObject<PersonnelInfoMod>(personData.ToString());
                    listResult.Add(dataPersonMod);
                    //var value = meterData;
                    //}
                    // check if the value is not null or empty.

                }
            }
            catch (WebException ex)
            {
                if (null != ex.Response)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            szResult = "Error: " + ex.Message + " : " + reader.ToString();
                        }
                    }
                }
                else
                {
                    szResult = "Error: " + ex.Message;
                }
            }
            catch (Exception ex)
            {
                szResult = ex.Message;
            }

            return listResult;
        }

        public string GetQueryString(object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }
    }
}

