/**
 * angular-chosen-localytics - Angular Chosen directive is an AngularJS Directive that brings the Chosen jQuery in a Angular way
 * @version v1.3.0
 * @link http://github.com/leocaseiro/angular-chosen
 * @license MIT
 */
//Number.prototype.padLeft = function(base,chr){
//    var  len = (String(base || 10).length - String(this).length)+1;
//    return len > 0? new Array(len).join(chr || '0')+this : this;
//}

//const { FastClick } = require("fastclick");

function getDateFormatShow(dDate) {
    try {
        var year = dDate.getFullYear();
        var month = (1 + dDate.getMonth()).toString();
        month = month.length > 1 ? month : "0" + month;
        var day = dDate.getDate().toString();
        day = day.length > 1 ? day : "0" + day;

        var dHour = dDate.getHours().toString();
        var dMin = dDate.getMinutes().toString();
        var dSec = dDate.getSeconds().toString();

        dHour = dHour.length > 1 ? dHour : "0" + dHour;
        dMin = dMin.length > 1 ? dMin : "0" + dMin;
        dSec = dSec.length > 1 ? dSec : "0" + dSec;

        return (
            day + "/" + month + "/" + year + "  " + dHour + ":" + dMin + ":" + dSec
        );
    } catch (e) {
        return dDate;
    }
}
function getDateFormatShowNew(dDate) {
    try {
        const d = new Date(dDate);
        var year = d.getFullYear();
        var month = (1 + d.getMonth()).toString();
        month = month.length > 1 ? month : "0" + month;
        var day = d.getDate().toString();
        day = day.length > 1 ? day : "0" + day;

        var dHour = d.getHours().toString();
        var dMin = d.getMinutes().toString();
        var dSec = d.getSeconds().toString();

        dHour = dHour.length > 1 ? dHour : "0" + dHour;
        dMin = dMin.length > 1 ? dMin : "0" + dMin;
        dSec = dSec.length > 1 ? dSec : "0" + dSec;

        return (
            day + "/" + month + "/" + year + "  " + dHour + ":" + dMin + ":" + dSec
        );
    } catch (e) {
        return dDate;
    }
}
(function () {
    var indexOf =
        [].indexOf ||
        function (item) {
            for (var i = 0, l = this.length; i < l; i++) {
                if (i in this && this[i] === item) return i;
            }
            return -1;
        };

    angular.module("localytics.directives", []);

    angular.module("localytics.directives").directive("chosen", [
        "$timeout",
        function ($timeout) {
            var CHOSEN_OPTION_WHITELIST, NG_OPTIONS_REGEXP, isEmpty, snakeCase;
            NG_OPTIONS_REGEXP =
                /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/;
            CHOSEN_OPTION_WHITELIST = [
                "persistentCreateOption",
                "createOptionText",
                "createOption",
                "skipNoResults",
                "noResultsText",
                "allowSingleDeselect",
                "disableSearchThreshold",
                "disableSearch",
                "enableSplitWordSearch",
                "inheritSelectClasses",
                "maxSelectedOptions",
                "placeholderTextMultiple",
                "placeholderTextSingle",
                "searchContains",
                "singleBackstrokeDelete",
                "displayDisabledOptions",
                "displaySelectedOptions",
                "width",
                "includeGroupLabelInSelected",
                "maxShownResults",
            ];
            snakeCase = function (input) {
                return input.replace(/[A-Z]/g, function ($1) {
                    return "_" + $1.toLowerCase();
                });
            };
            isEmpty = function (value) {
                var key;
                if (angular.isArray(value)) {
                    return value.length === 0;
                } else if (angular.isObject(value)) {
                    for (key in value) {
                        if (value.hasOwnProperty(key)) {
                            return false;
                        }
                    }
                }
                return true;
            };
            return {
                restrict: "A",
                require: "?ngModel",
                priority: 1,
                link: function (scope, element, attr, ngModel) {
                    var chosen,
                        empty,
                        initOrUpdate,
                        match,
                        options,
                        origRender,
                        startLoading,
                        stopLoading,
                        updateMessage,
                        valuesExpr,
                        viewWatch;
                    scope.disabledValuesHistory = scope.disabledValuesHistory
                        ? scope.disabledValuesHistory
                        : [];
                    element = $(element);
                    element.addClass("localytics-chosen");
                    options = scope.$eval(attr.chosen) || {};
                    angular.forEach(attr, function (value, key) {
                        if (indexOf.call(CHOSEN_OPTION_WHITELIST, key) >= 0) {
                            return attr.$observe(key, function (value) {
                                options[snakeCase(key)] =
                                    String(element.attr(attr.$attr[key])).slice(0, 2) === "{{"
                                        ? value
                                        : scope.$eval(value);
                                return updateMessage();
                            });
                        }
                    });
                    startLoading = function () {
                        return element
                            .addClass("loading")
                            .attr("disabled", true)
                            .trigger("chosen:updated");
                    };
                    stopLoading = function () {
                        element.removeClass("loading");
                        if (angular.isDefined(attr.disabled)) {
                            element.attr("disabled", attr.disabled);
                        } else {
                            element.attr("disabled", false);
                        }
                        return element.trigger("chosen:updated");
                    };
                    chosen = null;
                    empty = false;
                    initOrUpdate = function () {
                        var defaultText;
                        if (chosen) {
                            return element.trigger("chosen:updated");
                        } else {
                            $timeout(function () {
                                chosen = element.chosen(options).data("chosen");
                                // chosen = element.chosen({enable_split_word_search:true}).data('chosen');
                            });
                            if (angular.isObject(chosen)) {
                                return (defaultText = chosen.default_text);
                            }
                        }
                    };
                    updateMessage = function () {
                        if (empty) {
                            element
                                .attr("data-placeholder", chosen.results_none_found)
                                .attr("disabled", true);
                        } else {
                            element.removeAttr("data-placeholder");
                        }
                        return element.trigger("chosen:updated");
                    };
                    if (ngModel) {
                        origRender = ngModel.$render;
                        ngModel.$render = function () {
                            origRender();
                            return initOrUpdate();
                        };
                        element.on("chosen:hiding_dropdown", function () {
                            return scope.$apply(function () {
                                return ngModel.$setTouched();
                            });
                        });
                        if (attr.multiple) {
                            viewWatch = function () {
                                return ngModel.$viewValue;
                            };
                            scope.$watch(viewWatch, ngModel.$render, true);
                        }
                    } else {
                        initOrUpdate();
                    }
                    attr.$observe("disabled", function () {
                        return element.trigger("chosen:updated");
                    });
                    if (attr.ngOptions && ngModel) {
                        match = attr.ngOptions.match(NG_OPTIONS_REGEXP);
                        valuesExpr = match[7];
                        scope.$watchCollection(valuesExpr, function (newVal, oldVal) {
                            var timer;
                            return (timer = $timeout(function () {
                                if (angular.isUndefined(newVal)) {
                                    return startLoading();
                                } else {
                                    empty = isEmpty(newVal);
                                    stopLoading();
                                    return updateMessage();
                                }
                            }));
                        });
                        return scope.$on("$destroy", function (event) {
                            if (typeof timer !== "undefined" && timer !== null) {
                                return $timeout.cancel(timer);
                            }
                        });
                    }
                },
            };
        },
    ]);
}.call(this));
function addCommas(nStr) {
    try {
        nStr += "";
        x = nStr.split(".");
        x1 = x[0];
        x2 = x.length > 1 ? "." + x[1] : "";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, "$1" + "," + "$2");
        }
        return x1 + x2;
    } catch (e) { }
    return nStr;
}
function removeCommas(dataStr) {
    try {
        return dataStr.replace(/,/g, "");
    } catch (e) {
        return dataStr;
    }
}
var getAcrobatInfo = function () {
    var getBrowserName = function () {
        return (this.name =
            this.name ||
            (function () {
                var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";

                if (userAgent.indexOf("chrome") > -1) return "chrome";
                else if (userAgent.indexOf("safari") > -1) return "safari";
                else if (userAgent.indexOf("msie") > -1) return "ie";
                else if (userAgent.indexOf("firefox") > -1) return "firefox";
                return userAgent;
            })());
    };

    var getActiveXObject = function (name) {
        try {
            return new ActiveXObject(name);
        } catch (e) { }
    };

    var getNavigatorPlugin = function (name) {
        for (key in navigator.plugins) {
            var plugin = navigator.plugins[key];
            if (plugin.name == name) return plugin;
        }
    };

    var getPDFPlugin = function () {
        return (this.plugin =
            this.plugin ||
            (function () {
                if (getBrowserName() == "ie") {
                    //
                    // load the activeX control
                    // AcroPDF.PDF is used by version 7 and later
                    // PDF.PdfCtrl is used by version 6 and earlier
                    return (
                        getActiveXObject("AcroPDF.PDF") || getActiveXObject("PDF.PdfCtrl")
                    );
                } else {
                    return (
                        getNavigatorPlugin("Adobe Acrobat") ||
                        getNavigatorPlugin("Chrome PDF Viewer") ||
                        getNavigatorPlugin("WebKit built-in PDF")
                    );
                }
            })());
    };

    var isAcrobatInstalled = function () {
        return !!getPDFPlugin();
    };

    var getAcrobatVersion = function () {
        try {
            var plugin = getPDFPlugin();

            if (getBrowserName() == "ie") {
                var versions = plugin.GetVersions().split(",");
                var latest = versions[0].split("=");
                return parseFloat(latest[1]);
            }

            if (plugin.version) return parseInt(plugin.version);
            return plugin.name;
        } catch (e) {
            return null;
        }
    };

    //
    // The returned object
    //
    return {
        browser: getBrowserName(),
        acrobat: isAcrobatInstalled() ? "installed" : false,
        acrobatVersion: getAcrobatVersion(),
    };
};
function setDataDescription(labels, listTrend, color, yAxisID) {
    //console.log(labels)
    //console.log(listTrend)
    var newDataset = {
        label: "",
        backgroundColor: color,
        borderColor: color,
        data: [],
        radius: 10,
        fill: false,
        tension: 0,
        showLine: false,
        borderWidth: 1,
        pointStyle: "rectRot",
        pointRadius: 10,
        pointBorderColor: "rgb(0, 0, 0)",
        datalabels: {
            color: color,
            data: [],
        },
        yAxisID: yAxisID
    };
    for (var index = 0; index < labels.length; ++index) {
        //var finddateInArr = listTrend.find((o) => new Date(o.SHOW_DATE) == labels[index]);
        //console.log(finddateInArr)
        //console.log(labels[index].getDate() + '-' + labels[index].getMonth() + '-' + labels[index].getFullYear() + " " + labels[index].getHours() + " " + labels[index].getMinutes() + " " + labels[index].getSeconds());
        if (index == 0) {
            newDataset.data.push(null);
            newDataset.datalabels.data.push(null);
            continue;
        }
        if (index == labels.length - 1) {
            newDataset.data.push(null);
            newDataset.datalabels.data.push(null);
            continue;
        }
        //console.log(listTrend[index])
        if (typeof listTrend[index] === 'undefined' || listTrend[index].DESCRIPTION == null || listTrend[index].DESCRIPTION == " ") {
            newDataset.data.push(null);
            newDataset.datalabels.data.push(null);
        } else {
            newDataset.data.push(0);
            if (listTrend[index].DESCRIPTION == "-") {
                newDataset.datalabels.data.push("ไม่มีตัวอย่างที่จุดเก็บ");
            } else {
                newDataset.datalabels.data.push(listTrend[index].DESCRIPTION);
            }
        }
    }
    return newDataset;
}
function chkDataYAxis(ListAllItem, defaultYAxisID, NoOfData) {
    var result = { falgYAxis: false, yAxisID: "y-axis-1" }
    ListAllItem.forEach(function (entry, index) {
        var findYAxisInArr = entry.AxesJoin.find(o => o == NoOfData);
        if (typeof findYAxisInArr != 'undefined') {
            if (entry.No == 1) result.yAxisID = "y-axis-1";
            if (entry.No == 2) result.yAxisID = "y-axis-2";
            if (entry.No == 3) result.yAxisID = "y-axis-3";
            if (entry.No == 4) result.yAxisID = "y-axis-4";
            if (entry.No == 5) result.yAxisID = "y-axis-5";
            if (entry.No == 6) result.yAxisID = "y-axis-6";
        }
    });
    if (defaultYAxisID != result.yAxisID) {
        result.falgYAxis = true;
    };
    return result;
}
function setDataControlConfGraph(arr, color, listSummary) {
    console.log(listSummary)
    var Dataset = {
        newDataset: null,
        newDataset2: null,
    };
    if (arr.length > 0) {
        var flagControl = false;
        for (var i = 0; i < arr.length; ++i) {
            if (arr[i].bMaxValue == true) {
                flagControl = true;
                break;
            }
        }
        var txtLabel = "";
        if (listSummary.TEMPLATE_TYPE == 5 || listSummary.TEMPLATE_TYPE == 15 || listSummary.TEMPLATE_TYPE == 16) {
            txtLabel = listSummary.SampleName;
        } else if (listSummary.TEMPLATE_TYPE == 8) {
            txtLabel = listSummary.SampleName + ", " + listSummary.AreaName;
        } else if (listSummary.TEMPLATE_TYPE == 7) {
            txtLabel = listSummary.SampleName + ", " + listSummary.ItemsName + " (1 Month)";
        } else if (listSummary.TEMPLATE_TYPE == 6) {
            txtLabel = listSummary.SampleName + ", " + listSummary.ItemsName;
        } else if (listSummary.TEMPLATE_TYPE == 10 || listSummary.TEMPLATE_TYPE == 11) {
            txtLabel = listSummary.AreaName + ", " + listSummary.SampleName + " (Weekly)";
        } else if (listSummary.TEMPLATE_TYPE == 12) {
            txtLabel = listSummary.SampleName + " (Weekly)";
        } else if (listSummary.TEMPLATE_TYPE == 13 || listSummary.TEMPLATE_TYPE == 14 || listSummary.TEMPLATE_TYPE == 3 || listSummary.TEMPLATE_TYPE == 9) {
            txtLabel = listSummary.AreaName + ", " + listSummary.SampleName;
        } else {
            txtLabel = listSummary.ItemsName;
        }
        //console.log(arr)
        if (flagControl == true) {
            var colorName = "red"; // colorNames[config.data.datasets.length % colorNames.length];
            var newColor = window.chartColors[colorName];
            var newDataset = {
                label: "Control Max " + txtLabel, //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                backgroundColor: newColor,
                borderColor: newColor,
                pointHoverRadius: 0,
                data: [],
                radius: 0,
                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                fill: false,
                tension: 0,
                datalabels: {
                    display: false,
                },
                spanGaps: true,
            };
            arr.forEach(function (entry, index) {
                if (entry.bMaxValue == true) {
                    newDataset.data.push(entry.MaxValue);
                } else {
                    newDataset.data.push(null);
                }
            });
            Dataset.newDataset = newDataset;
        }
        var flagControl2 = false;
        for (var i = 0; i < arr.length; ++i) {
            if (arr[i].bMinValue == true) {
                flagControl2 = true;
                break;
            }
        }
        if (flagControl2 == true) {
            var colorName = "orange"; //orange// colorNames[config.data.datasets.length % colorNames.length];
            var newColor = window.chartColors[colorName]; //'#FF4500';
            var newDataset2 = {
                label: "Control Min " + txtLabel,
                backgroundColor: newColor,
                borderColor: newColor,
                pointHoverRadius: 0,
                data: [],
                radius: 0,
                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                fill: false,
                tension: 0,
                datalabels: {
                    display: false,
                },
                spanGaps: true,
            };
            arr.forEach(function (entry, index) {
                if (entry.bMinValue == true) {
                    newDataset2.data.push(entry.MinValue);
                } else {
                    newDataset2.data.push(null);
                }
            });
            Dataset.newDataset2 = newDataset2;
        }
        return Dataset;
    }
}
angular.module("siyfion.sfTypeahead", []).directive("sfTypeahead", function () {
    return {
        restrict: "AC", // Only apply on an attribute or class
        require: "?ngModel", // The two-way data bound value that is returned by the directive
        scope: {
            options: "=", // The typeahead configuration options (https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md#options)
            datasets: "=", // The typeahead datasets to use (https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md#datasets)
            suggestionKey: "@",
        },
        link: function (scope, element, attrs, ngModel) {
            var options = scope.options || {},
                datasets =
                    (angular.isArray(scope.datasets)
                        ? scope.datasets
                        : [scope.datasets]) || [], // normalize to array
                init = true;

            // Create the typeahead on the element
            initialize();

            scope.$watch("options", initialize);

            if (angular.isArray(scope.datasets)) {
                scope.$watchCollection("datasets", initialize);
            } else {
                scope.$watch("datasets", initialize);
            }

            // Parses and validates what is going to be set to model (called when: ngModel.$setViewValue(value))
            ngModel.$parsers.push(function (fromView) {
                // In Firefox, when the typeahead field loses focus, it fires an extra
                // angular input update event.  This causes the stored model to be
                // replaced with the search string.  If the typeahead search string
                // hasn't changed at all (the 'val' property doesn't update until
                // after the event loop finishes), then we can bail out early and keep
                // the current model value.
                if (
                    angular.isObject(ngModel.$modelValue) &&
                    fromView === element.typeahead("val")
                )
                    return ngModel.$modelValue;

                // Assuming that all objects are datums
                // See typeahead basics: https://gist.github.com/jharding/9458744#file-the-basics-js-L15
                var isDatum = angular.isObject(fromView);
                if (options.editable === false) {
                    ngModel.$setValidity("typeahead", isDatum);
                    return isDatum ? fromView : undefined;
                }

                return fromView;
            });

            // Formats what is going to be displayed (called when: $scope.model = { object })
            ngModel.$formatters.push(function (fromModel) {
                if (angular.isObject(fromModel)) {
                    var found = false;
                    $.each(datasets, function (index, dataset) {
                        var query = dataset.source,
                            displayKey = dataset.displayKey || "value",
                            value =
                                (angular.isFunction(displayKey)
                                    ? displayKey(fromModel)
                                    : fromModel[displayKey]) || "";

                        if (found) return false; // break

                        if (!value) {
                            // Fakes a request just to use the same function logic
                            search([]);
                            return;
                        }

                        // Get suggestions by asynchronous request and updates the view
                        query(value, search);
                        return;

                        function search(suggestions) {
                            var exists = inArray(suggestions, fromModel);
                            if (exists) {
                                ngModel.$setViewValue(fromModel);
                                found = true;
                            } else {
                                ngModel.$setViewValue(
                                    options.editable === false ? undefined : fromModel
                                );
                            }

                            // At this point, digest could be running (local, prefetch) or could not be (remote)
                            // As bloodhound object is inaccessible to know that, simulates an async to not conflict
                            // with possible running digest
                            if (found || index === datasets.length - 1) {
                                setTimeout(function () {
                                    scope.$apply(function () {
                                        element.typeahead("val", value);
                                    });
                                }, 0);
                            }
                        }
                    });

                    return ""; // loading
                } else if (fromModel == null) {
                    //fromModel has been set to null or undefined
                    element.typeahead("val", null);
                }
                return fromModel;
            });

            function initialize() {
                if (init) {
                    element.typeahead(scope.options, scope.datasets);
                    init = false;
                } else {
                    // If datasets or options change, hang onto user input until we reinitialize
                    var value = element.val();
                    element.typeahead("destroy");
                    element.typeahead(scope.options, scope.datasets);
                    ngModel.$setViewValue(value);
                    element.triggerHandler("typeahead:opened");
                }
            }

            function inArray(array, element) {
                var found = -1;
                angular.forEach(array, function (value, key) {
                    if (angular.equals(element, value)) {
                        found = key;
                    }
                });
                return found >= 0;
            }

            function updateScope(object, suggestion, dataset) {
                scope.$apply(function () {
                    var newViewValue = angular.isDefined(scope.suggestionKey)
                        ? suggestion[scope.suggestionKey]
                        : suggestion;
                    ngModel.$setViewValue(newViewValue);
                });
            }

            // Update the value binding when a value is manually selected from the dropdown.
            element.bind(
                "typeahead:selected",
                function (object, suggestion, dataset) {
                    updateScope(object, suggestion, dataset);
                    scope.$emit("typeahead:selected", suggestion, dataset);
                }
            );

            // Update the value binding when a query is autocompleted.
            element.bind(
                "typeahead:autocompleted",
                function (object, suggestion, dataset) {
                    updateScope(object, suggestion, dataset);
                    scope.$emit("typeahead:autocompleted", suggestion, dataset);
                }
            );

            // Propagate the opened event
            element.bind("typeahead:opened", function () {
                scope.$emit("typeahead:opened");
            });

            // Propagate the closed event
            element.bind("typeahead:closed", function () {
                scope.$emit("typeahead:closed");
            });

            // Propagate the asyncrequest event
            element.bind("typeahead:asyncrequest", function () {
                scope.$emit("typeahead:asyncrequest");
            });

            // Propagate the asynccancel event
            element.bind("typeahead:asynccancel", function () {
                scope.$emit("typeahead:asynccancel");
            });

            // Propagate the asyncreceive event
            element.bind("typeahead:asyncreceive", function () {
                scope.$emit("typeahead:asyncreceive");
            });

            // Propagate the cursorchanged event
            element.bind(
                "typeahead:cursorchanged",
                function (event, suggestion, dataset) {
                    scope.$emit("typeahead:cursorchanged", event, suggestion, dataset);
                }
            );
        },
    };
});
angular
    .module("ui.date", [])
    .constant("uiDateConfig", { dateFormat: "dd-mm-yy" })
    .directive("uiDate", [
        "uiDateConfig",
        "$timeout",
        function (uiDateConfig, $timeout) {
            "use strict";
            var options;
            options = {};
            angular.extend(options, uiDateConfig);
            return {
                require: "?ngModel",
                link: function (scope, element, attrs, controller) {
                    var getOptions = function () {
                        return angular.extend({}, uiDateConfig, scope.$eval(attrs.uiDate));
                    };
                    var initDateWidget = function () {
                        var showing = false;
                        var opts = getOptions();
                        // If we have a controller (i.e. ngModelController) then wire it up
                        if (controller) {
                            // Set the view value in a $apply block when users selects
                            // (calling directive user's function too if provided)
                            var _onSelect = opts.onSelect || angular.noop;
                            opts.onSelect = function (value, picker) {
                                scope.$apply(function () {
                                    showing = true;
                                    controller.$setViewValue(element.datepicker("getDate"));
                                    _onSelect(value, picker);
                                    element.blur();
                                });
                            };
                            opts.beforeShow = function () {
                                showing = true;
                            };
                            opts.onClose = function (value, picker) {
                                showing = false;
                            };
                            element.on("blur", function () {
                                if (!showing) {
                                    scope.$apply(function () {
                                        element.datepicker(
                                            "setDate",
                                            element.datepicker("getDate")
                                        );
                                        controller.$setViewValue(element.datepicker("getDate"));
                                    });
                                }
                            });

                            // Update the date picker when the model changes
                            controller.$render = function () {
                                var date = controller.$viewValue;
                                if (null !== date) {
                                    date = new Date(date);
                                }
                                if (
                                    angular.isDefined(date) &&
                                    date !== null &&
                                    !angular.isDate(date)
                                ) {
                                    throw new Error(
                                        "ng-Model value must be a Date object - currently it is a " +
                                        typeof date +
                                        " - use ui-date-format to convert it from a string"
                                    );
                                }
                                element.datepicker("setDate", date);
                            };
                        }
                        // If we don't destroy the old one it doesn't update properly when the config changes
                        element.datepicker("destroy");
                        // Create the new datepicker widget
                        element.datepicker(opts);
                        if (controller) {
                            // Force a render to override whatever is in the input text box
                            controller.$render();
                        }
                    };
                    // Watch for changes to the directives options
                    scope.$watch(getOptions, initDateWidget, true);
                },
            };
        },
    ])
    .constant("uiDateFormatConfig", "")
    .directive("uiDateFormat", [
        "uiDateFormatConfig",
        function (uiDateFormatConfig) {
            var directive = {
                require: "ngModel",
                link: function (scope, element, attrs, modelCtrl) {
                    var dateFormat = attrs.uiDateFormat || uiDateFormatConfig;

                    if (dateFormat) {
                        // Use the datepicker with the attribute value as the dateFormat string to convert to and from a string
                        modelCtrl.$formatters.push(function (value) {
                            if (angular.isString(value)) {
                                return jQuery.datepicker.parseDate(dateFormat, value);
                            }
                            return null;
                        });
                        modelCtrl.$parsers.push(function (value) {
                            if (value) {
                                return jQuery.datepicker.formatDate(dateFormat, value);
                            }
                            return null;
                        });
                    } else {
                        // Default to ISO formatting
                        modelCtrl.$formatters.push(function (value) {
                            if (angular.isString(value)) {
                                return new Date(value);
                            }
                            return null;
                        });
                        modelCtrl.$parsers.push(function (value) {
                            if (value) {
                                return value.toISOString();
                            }
                            return null;
                        });
                    }
                },
            };
            return directive;
        },
    ]);
var app = angular.module("myApp", [
    "mgcrea.ngStrap",
    "ngAnimate",
    "ui.date",
    "localytics.directives",
    "siyfion.sfTypeahead",
    "ngSanitize",
    "ngFileUpload",
]);
app.directive("myEnter", function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});
app.directive("customDatepicker", function ($compile) {
    return {
        replace: true,
        templateUrl: "custom-datepicker.html",
        scope: {
            ngModel: "=",
            dateOptions: "=",
        },
        link: function ($scope, $element, $attrs, $controller) {
            var $button = $element.find("button");
            var $input = $element.find("input");
            $button.on("click", function () {
                if ($input.is(":focus")) {
                    $input.trigger("blur");
                } else {
                    $input.trigger("focus");
                }
            });
        },
    };
});
app.directive("myConfirmClick", [
    function () {
        return {
            priority: -1,
            restrict: "A",
            scope: { confirmFunction: "&myConfirmClick" },
            link: function (scope, element, attrs) {
                element.bind("click", function (e) {
                    // message defaults to "Are you sure?"
                    var message = attrs.myConfirmClickMessage
                        ? attrs.myConfirmClickMessage
                        : "Are you sure?";

                    $("#dialog-confirm")
                        .modal({
                            // wire up the actual modal functionality and show the dialog
                            backdrop: "static",
                            keyboard: true,
                            show: true, // ensure the modal is shown immediately
                        })
                        .one("click", "#btnYes", function () {
                            // just as an example...
                            scope.confirmFunction();
                            $("#dialog-confirm").modal("hide");
                        });
                });
            },
        };
    },
]);
app.directive("numberOnlyInput", function () {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue
                    ? inputValue.replace(/[^\d.-]/g, "")
                    : null;
                //var transformedInput = inputValue ? inputValue.replace("^\d+(\.\d+)+$",'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        },
    };
});
app.directive("numberOnlyInputEx", function () {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = null;

                if (inputValue) {
                    transformedInput = removeCommas(inputValue);
                    transformedInput = transformedInput
                        ? transformedInput.replace(/[^\d.-]/g, "")
                        : null;

                    if (transformedInput) {
                        transformedInput = addCommas(transformedInput);
                    }
                }
                //var transformedInput = inputValue ? inputValue.replace("^\d+(\.\d+)+$",'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        },
    };
});
app.directive("mySelection", function () {
    return {
        require: "ngModel",
        link: function (scope, elm, attr, ctrl) {
            if (!ctrl) return;
            attr.requiredSelect = true; // force truthy in case we are on non input element

            var validator = function (value) {
                if ((attr.requiredSelect && ctrl.$isEmpty(value)) || value == 0) {
                    ctrl.$setValidity("mySelection", false);
                    return;
                } else {
                    ctrl.$setValidity("mySelection", true);
                    return value;
                }
            };

            ctrl.$formatters.push(validator);
            ctrl.$parsers.unshift(validator);

            attr.$observe("mySelection", function () {
                validator(ctrl.$viewValue);
            });
        },
    };
});
app.directive("fileModel", [
    "$parse",
    function ($parse) {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind("change", function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            },
        };
    },
]);
app.service("fileUpload", [
    "$http",
    function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
            $("#overlay").show(); //$('#myWaitModal').modal('show');
            var fd = new FormData();
            fd.append("file", file); //file เป็นชื่อ parameter ที่ส่งขึ้นไป

            $http
                .post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { "Content-Type": undefined },
                })
                .success(function (response) {
                    if (true == response.status) {
                        location.reload();
                    } else {
                        $("#error_message").html(response.message);
                        $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                        $("#myErrorModal").modal("show");
                    }
                })
                .error(function (response) { });
        };
    },
]);
app.filter("showdateformat", function () {
    return function (input) {
        try {
            var numb = input.replace("/Date(", "").replace(")/", "");
            var dDate = new Date(parseFloat(numb));

            var year = dDate.getFullYear();
            var month = (1 + dDate.getMonth()).toString();
            month = month.length > 1 ? month : "0" + month;
            var day = dDate.getDate().toString();
            day = day.length > 1 ? day : "0" + day;

            return day + "-" + month + "-" + year;
        } catch (e) {
            return input;
        }
        //return month + '/' + day + '/' + year;
    };
});
app.filter("showdatetimeformat", function () {
    return function (input) {
        var numb = input.replace("/Date(", "").replace(")/", "");
        var dDate = new Date(parseFloat(numb));

        var year = dDate.getFullYear();
        var month = (1 + dDate.getMonth()).toString();
        month = month.length > 1 ? month : "0" + month;
        var day = dDate.getDate().toString();
        day = day.length > 1 ? day : "0" + day;

        var dHour = dDate.getHours().toString();
        var dMin = dDate.getMinutes().toString();
        var dSec = dDate.getSeconds().toString();

        dHour = dHour.length > 1 ? dHour : "0" + dHour;
        dMin = dMin.length > 1 ? dMin : "0" + dMin;
        dSec = dSec.length > 1 ? dSec : "0" + dSec;

        return (
            day + "-" + month + "-" + year + "  " + dHour + ":" + dMin + ":" + dSec
        );
    };
});
app.filter("insertspace", function () {
    return function (input) {
        var ret = input.replace(/(.{10})/g, "$1 ");

        return ret;
    };
});
app.filter("addcomma", function () {
    return function (nStr) {
        try {
            nStr += "";
            x = nStr.split(".");
            x1 = x[0];
            x2 = x.length > 1 ? "." + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "," + "$2");
            }
            return x1 + x2;
        } catch (e) { }
        return nStr;
    };
});
app.factory(
    "Fact",
    function ($http, $httpParamSerializer, $httpParamSerializerJQLike) {
        var urlTableName = "";
        var urlTableNameEx = "";
        var urlDeleteName = "";
        var urlUpdateName = "";

        var PageIndex = 1;
        var PageSize = 20;
        var SortColumn = "";
        var SortOrder = "asc";

        var PageIndexEx = 1;
        var PageSizeEx = 20;
        var SortColumnEx = "";
        var SortOrderEx = "asc";

        var modelSearch;

        var listDataTable;
        var templates = [];
        var totalPage;
        var totalRecords;

        var totalPageEx;
        var totalRecordsEx;
        var listDataTableEx;

        var ticksFactOption1 = {};
        var ticksFactOption2 = {};
        var ticksFactOption3 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var ticksFactOption4 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var ticksFactOption5 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var ticksFactOption6 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item

        var positionFactOption1 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var positionFactOption2 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var positionFactOption3 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var positionFactOption4 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var positionFactOption5 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        var positionFactOption6 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item

        var tempChkItemIsSame = {
            disabledEditYAxis1: false,
            disabledEditYAxis2: false,
            disabledEditYAxis3: false,
            disabledEditYAxis4: false,
            disabledEditYAxis5: false,
            disabledEditYAxis6: false,
        };

        var objFact = {};
        var overlay = 0;

        objFact.showOverlay = function (overlay) {
            //console.log(overlay);
            if (overlay > 0) {
                $("#overlay").show();
            } else {
                $("#overlay").hide(); 
            }
        };

        objFact.loadtoList = function (url) {
            $("#overlay").show();
            window.location = url;
        };
        objFact.showError = function (msg) {
            $("#overlay").hide(); //$('#myWaitModal').modal('hide');
            $("#error_message").html(msg);
            $("#myErrorModal").modal("show");
        };
        objFact.showMessage = function (msg) {
            $("#overlay").hide(); //$('#myWaitModal').modal('hide');
            $("#show_message").html(msg);
            $("#myMessageModal").modal("show");
        };
        objFact.showReloadMessage = function (msg) {
            $("#overlay").hide(); //$('#myWaitModal').modal('hide');
            $("#show_reload_message").html(msg);
            $("#myRelaodMessageModal").modal("show");
        };
        objFact.setDataTableUrl = function (url) {
            urlTableName = url;
        };
        objFact.setUpdateTableUrl = function (url) {
            urlUpdateName = url;
        };
        objFact.setDelelteUrl = function (url) {
            urlDeleteName = url;
        };
        objFact.setSearchModel = function (mSearch) {
            modelSearch = mSearch;
        };
        objFact.setSortColumn = function (sortColumn) {
            SortColumn = sortColumn;
            return SortColumn;
        };
        objFact.setSortOrder = function (sortOrder) {
            SortOrder = sortOrder;
            return SortOrder;
        };
        objFact.getSortColumn = function () {
            return SortColumn;
        };
        objFact.getSortOrder = function () {
            return SortOrder;
        };
        objFact.getFirstPage = function () {
            if (parseInt(PageIndex, 10) != 1) {
                PageIndex = 1;
            }
            return PageIndex;
        };
        objFact.getNextPage = function () {
            if (parseInt(PageIndex, 10) < parseInt(totalPage, 10)) {
                PageIndex += 1;
            }
            return PageIndex;
        };
        objFact.getBackPage = function () {
            if (parseInt(PageIndex, 10) > 1) {
                PageIndex -= 1;
            }
            return PageIndex;
        };
        objFact.getLastPage = function () {
            if (parseInt(PageIndex, 10) <= parseInt(totalPage, 10)) {
                PageIndex = totalPage;
            }
            return PageIndex;
        };
        objFact.searchByPage = function () {
            try {
                if ($.isNumeric(PageIndex) == false) {
                    PageIndex = 1;
                } else if (PageIndex < 1) {
                    PageIndex = 1;
                    getFirstPage();
                } else if (PageIndex > totalPage) {
                    PageIndex = totalPage;
                } else {
                    //loadExportPrefix();
                }
            } catch (e) {
                return false;
            }
            return PageIndex;
        };
        objFact.setPageSize = function (size) {
            PageSize = size;
            return PageSize;
        };
        objFact.setPageIndex = function (page) {
            PageIndex = page;
            return PageIndex;
        };
        objFact.setSortIcon = function () {
            $(".icon").remove();
            $(".sorting").append('<i class="icon fa fa-sort"></i>');
            $(".sorting_asc").append('<i class="icon fa fa-sort-asc"></i>');
            $(".sorting_desc").append('<i class="icon fa fa-sort-desc"></i>');
        };
        objFact.sort = function (event) {
            var nextSortColumn = $(event.target).attr("sort");
            $(".sorting_asc").removeClass("sorting_asc").addClass("sorting");
            $(".sorting_desc").removeClass("sorting_desc").addClass("sorting");

            if (SortColumn == nextSortColumn) {
                if (SortOrder == "desc") {
                    SortOrder = "asc";
                    $(event.target).removeClass("sorting").addClass("sorting_asc");
                } else {
                    SortOrder = "desc";
                    $(event.target).removeClass("sorting").addClass("sorting_desc");
                }
            } else {
                SortColumn = nextSortColumn;
                SortOrder = "desc";
                $(event.target).removeClass("sorting").addClass("sorting_desc");
            }

            objFact.setSortIcon();
        };
        /********* Soluve 2 search in page **********/
        objFact.setDataTableUrlEx = function (url) {
            urlTableNameEx = url;
        };
        objFact.setSortColumnEx = function (sortColumn) {
            SortColumnEx = sortColumn;
            return SortColumn;
        };
        objFact.setSortOrderEx = function (sortOrder) {
            SortOrderEx = sortOrder;
            return SortOrder;
        };
        objFact.getSortColumnEx = function () {
            return SortColumnEx;
        };
        objFact.getSortOrderEx = function () {
            return SortOrderEx;
        };
        objFact.getFirstPageEx = function () {
            if (parseInt(PageIndexEx, 10) != 1) {
                PageIndexEx = 1;
            }
            return PageIndexEx;
        };
        objFact.getNextPageEx = function () {
            if (parseInt(PageIndexEx, 10) < parseInt(totalPageEx, 10)) {
                PageIndexEx += 1;
            }
            return PageIndexEx;
        };
        objFact.getBackPageEx = function () {
            if (parseInt(PageIndexEx, 10) > 1) {
                PageIndexEx -= 1;
            }
            return PageIndexEx;
        };
        objFact.getLastPageEx = function () {
            if (parseInt(PageIndexEx, 10) <= parseInt(totalPageEx, 10)) {
                PageIndex = totalPageEx;
            }
            return PageIndexEx;
        };
        objFact.searchByPageEx = function () {
            try {
                if ($.isNumeric(PageIndexEx) == false) {
                    PageIndexEx = 1;
                } else if (PageIndexEx < 1) {
                    PageIndexEx = 1;
                    getFirstPage();
                } else if (PageIndexEx > totalPageEx) {
                    PageIndexEx = totalPageEx;
                }
            } catch (e) {
                return false;
            }
            return PageIndex;
        };
        objFact.setPageSizeEx = function (size) {
            PageSizeEx = size;
            return PageSizeEx;
        };
        objFact.setPageIndexEx = function (page) {
            PageIndexEx = page;
            return PageIndexEx;
        };
        objFact.setSortIconEx = function () {
            $(".iconEx").remove();
            $(".sortingEx").append('<i class="iconEx fa fa-sort"></i>');
            $(".sorting_ascEx").append('<i class="iconEx fa fa-sort-asc"></i>');
            $(".sorting_descEx").append('<i class="iconEx fa fa-sort-desc"></i>');
        };
        objFact.sortEx = function (event) {
            var nextSortColumn = $(event.target).attr("sort");
            $(".sorting_ascEx").removeClass("sorting_ascEx").addClass("sorting");
            $(".sorting_descEx").removeClass("sorting_descEx").addClass("sorting");

            if (SortColumnEx == nextSortColumn) {
                if (SortOrderEx == "desc") {
                    SortOrderEx = "asc";
                    $(event.target).removeClass("sorting").addClass("sorting_asc");
                } else {
                    SortOrderEx = "desc";
                    $(event.target).removeClass("sorting").addClass("sorting_desc");
                }
            } else {
                SortColumnEx = nextSortColumn;
                SortOrderEx = "desc";
                $(event.target).removeClass("sorting").addClass("sorting_desc");
            }

            objFact.setSortIconEx();
        };
        /************* *****************************/
        objFact.deleteMultiData = function (listData, msgError, callback) {
            var ids = [];
            var isValid = false;

            listData.forEach(function (entry, index) {
                if (true == entry.ITEM_SELECTED) {
                    ids.push(entry.ID);
                    isValid = true;
                }
            });

            if (isValid) {
                try {
                    $("#overlay").show(); //$('#myWaitModal').modal('show');

                    var jsonData = {
                        ids: ids,
                    };

                    $http({
                        url: urlDeleteName,
                        method: "POST",
                        //data: $httpParamSerializerJQLike(jsonData),
                        data: jsonData,
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "Content-Type": "application/json;charset=UTF-8",
                        },
                    }).then(
                        function (response) {
                            if ("1" == response.data.result) {
                                callback(response);
                            } else {
                                objFact.showError(response.data.message);
                            }
                        },
                        function (response) {
                            // optional
                            objFact.showError(response);
                        }
                    );
                } catch (e) {
                    objFact.showError(e.message);
                }
            } else {
                objFact.showError(msgError);
            }
        };
        objFact.deleteMultiDataEx = function (
            urlName,
            listData,
            msgError,
            callback
        ) {
            var ids = [];
            var isValid = false;

            listData.forEach(function (entry, index) {
                if (true == entry.ITEM_SELECTED) {
                    ids.push(entry.ID);
                    isValid = true;
                }
            });

            if (isValid) {
                try {
                    $("#overlay").show(); //$('#myWaitModal').modal('show');

                    var jsonData = {
                        ids: ids,
                    };

                    $http({
                        url: urlName,
                        method: "POST",
                        //data: $httpParamSerializerJQLike(jsonData),
                        data: jsonData,
                        //headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "Content-Type": "application/json;charset=UTF-8",
                        },
                    }).then(
                        function (response) {
                            if ("1" == response.data.result) {
                                callback();
                            } else {
                                objFact.showError(response.data.message);
                            }
                        },
                        function (response) {
                            // optional
                            objFact.showError(response);
                        }
                    );
                } catch (e) {
                    objFact.showError(e.message);
                }
            } else {
                objFact.showError(msgError);
            }
        };
        objFact.deleteDataEx = function (urlName, jsonData, callback) {
            try {
                $("#overlay").show(); //$('#myWaitModal').modal('show');
                $http({
                    url: urlName,
                    method: "POST",
                    //data: $httpParamSerializerJQLike(jsonData),
                    data: jsonData,
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                        "Content-Type": "application/json;charset=UTF-8",
                    },
                }).then(
                    function (response) {
                        if ("1" == response.data.result) {
                            callback();
                        } else {
                            objFact.showError(response.data.message);
                        }
                    },
                    function (response) {
                        // optional
                        objFact.showError(response);
                    }
                );
            } catch (e) {
                objFact.showError(e.message);
            }
        };
        objFact.UpdateDocStatus = function (
            listData,
            doc_status_value,
            msgError,
            callback
        ) {
            var ids = [];
            var isValid = false;

            listData.forEach(function (entry, index) {
                if (true == entry.ITEM_SELECTED) {
                    ids.push(entry.ID);
                    isValid = true;
                }
            });

            if (isValid) {
                try {
                    $("#overlay").show(); //$('#myWaitModal').modal('show');

                    var jsonData = {
                        ids: ids,
                        doc_status_value: doc_status_value,
                    };

                    $http({
                        url: urlUpdateName,
                        method: "POST",
                        //data: $httpParamSerializerJQLike(jsonData),
                        data: jsonData,
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "Content-Type": "application/json;charset=UTF-8",
                        },
                    }).then(
                        function (response) {
                            if ("1" == response.data.result) {
                                callback();
                            } else {
                                objFact.showError(response.data.message);
                            }
                        },
                        function (response) {
                            // optional
                            objFact.showError(response);
                        }
                    );
                } catch (e) {
                    objFact.showError(e.message);
                }
            } else {
                objFact.showError(msgError);
            }
        };
        objFact.UpdateTrendStatus = function (
            urlUpdateTrend,
            listData,
            msgError,
            callback
        ) {
            var ids = [];
            var isValid = false;

            listData.forEach(function (entry, index) {
                if (true == entry.ITEM_SELECTED) {
                    ids.push(entry.ID);
                    isValid = true;
                }
            });

            if (isValid) {
                try {
                    $("#overlay").show(); //$('#myWaitModal').modal('show');

                    var jsonData = {
                        ids: ids,
                        listData: listData,
                    };

                    $http({
                        url: urlUpdateTrend,
                        method: "POST",
                        //data: $httpParamSerializerJQLike(jsonData),
                        data: jsonData,
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "Content-Type": "application/json;charset=UTF-8",
                        },
                    }).then(
                        function (response) {
                            if ("1" == response.data.result) {
                                callback();
                            } else {
                                objFact.showError(response.data.message);
                            }
                        },
                        function (response) {
                            // optional
                            objFact.showError(response);
                        }
                    );
                } catch (e) {
                    objFact.showError(e.message);
                }
            } else {
                objFact.showError(msgError);
            }
        };
        objFact.UpdateTrendDetailStatus = function (
            urlUpdateTrend,
            userId,
            listData,
            msgError,
            callback
        ) {
            var ids = [];
            var isValid = false; 
            listData.forEach(function (entry, index) {
                if (true == entry.ITEM_SELECTED) {
                    ids.push(entry.ID);
                    isValid = true;
                }
            });

            if (isValid) {
                try {
                    $("#overlay").show(); //$('#myWaitModal').modal('show');

                    var jsonData = {
                        userId: userId,
                        ids: ids,
                        listData: listData,
                    };

                    $http({
                        url: urlUpdateTrend,
                        method: "POST",
                        //data: $httpParamSerializerJQLike(jsonData),
                        data: jsonData,
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "Content-Type": "application/json;charset=UTF-8",
                        },
                    }).then(
                        function (response) {
                            $("#overlay").hide();
                            if ("1" == response.data.result) {
                                callback(response);
                            } else {
                                objFact.showError(response.data.message);
                            }
                        },
                        function (response) {
                            // optional
                            $("#overlay").hide();
                            objFact.showError(response);
                        }
                    );
                } catch (e) {
                    objFact.showError(e.message);
                }
            } else {
                objFact.showError(msgError);
            }
        };
        objFact.loadDataTableEx = function (mSearch, callback) {
            var jsonData = {
                PageIndex: PageIndexEx,
                PageSize: PageSizeEx,
                SortColumn: SortColumnEx,
                SortOrder: SortOrderEx,
                mSearch: mSearch,
            };

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlTableNameEx,
                method: "POST",
                data: jsonData,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        totalPageEx = response.data.toTalPage; //init;
                        totalRecordsEx = response.data.totalRecords;
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadDataTable = function (mSearch, callback) {
            var jsonData = {
                PageIndex: PageIndex,
                PageSize: PageSize,
                SortColumn: SortColumn,
                SortOrder: SortOrder,
                mSearch: mSearch,
            };

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlTableName,
                method: "POST",
                //data: $httpParamSerializerJQLike(jsonData),
                data: jsonData,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        totalPage = response.data.toTalPage; //init;
                        totalRecords = response.data.totalRecords;
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.saveData = function (urlName, jsonData, isReload) {
            $("#overlay").show();
            $http({
                url: urlName,
                method: "POST",
                //data: $httpParamSerializerJQLike(jsonData),
                data: jsonData,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        if (isReload) {
                            objFact.showReloadMessage(response.data.message);
                        } else {
                            objFact.showMessage(response.data.message);
                        }
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    // optional
                    objFact.showError(response);
                }
            );
        };
        objFact.saveDataCallback = function (urlName, jsonData, callback) {
            $("#overlay").show();
            $http({
                url: urlName,
                method: "POST",
                //data: $httpParamSerializerJQLike(jsonData),
                data: jsonData,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    // optional
                    objFact.showError(response);
                }
            );
        };
        objFact.getDateFromDoubleEx = function (data) {
            if (null == data) {
                return null;
            }

            var numb = data.replace("/Date(", "").replace(")/", "");
            var dDate = new Date(parseFloat(numb));
            //var dDate = new Date(parseFloat(data.substr(6)));
            return dDate;
        };
        objFact.showDateTextFromDoubleEx = function getDateFromDouble(dataDate) {
            //            var numb = data.replace("/Date(", "").replace(")/", "");
            //            var monthStr = ["ม.ค", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย", "ก.ค", "ส.ค", "ก.ย", "ต.ค", "พ.ย", "ธ.ค"];
            //            var dDate = new Date(parseFloat(numb));

            var data = dataDate.replace("/Date(", "").replace(")/", "");
            var dDate = new Date(parseInt(data.substr(6)));
            var year = date.getFullYear();
            var month = (1 + date.getMonth()).toString();
            month = month.length > 1 ? month : "0" + month;
            var day = date.getDate().toString();
            day = day.length > 1 ? day : "0" + day;
            return month + "/" + day + "/" + year;
        };
        objFact.SelectAllCheck = function (listData, selected) {
            listData.forEach(function (entry) {
                entry.ITEM_SELECTED = selected;
            });
            return true;
        };
        objFact.editData = function (urlName) {
            //window.location.replace(urlName); //don't have history
            window.location = urlName;
        };
        objFact.loadAllPlant = function (callback) {
            var urlPlant = get_base_url("PlantProductMapping/GetActivePlantList");

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlPlant,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadAllEmail = function (callback) {
            var urlEmail = get_base_url("InternalQualityControl/GetAllQMS_MA_EMAIL");

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlEmail,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadAllProduct = function (callback) {
            var urlPlant = get_base_url("PlantProductMapping/GetActiveProductList");

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlPlant,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadAllPossibleCause = function (callback) {
            var url = get_base_url(
                "InternalQualityControl/GetAll_QMS_MA_IQC_POSSICAUSE"
            );

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: url,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadAllGrade = function (callback) {
            var urlPlant = get_base_url("PlantProductMapping/GetActiveGradeList");

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlPlant,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadAllUnit = function (callback) {
            var urlPlant = get_base_url("Unit/GetActiveUnitList");

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlPlant,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadAllRootCauseType = function (callback) {
            var urlPlant = get_base_url("RootCause/GetActiveRootCauseTypeList");

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlPlant,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadAllRootCause = function (callback) {
            var urlPlant = get_base_url("RootCause/GetActiveRootCauseList");

            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlPlant,
                method: "POST",
                //data: null,
                data: null,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadlistData = function (urlName, jsonData, callback) {
            overlay = overlay + 1;
            objFact.showOverlay(overlay);
            //$("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlName,
                method: "POST",
                //data: $httpParamSerializerJQLike(jsonData),
                data: jsonData,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    //$("#overlay").hide(); //$('#myWaitModal').modal('hide');
                 
                    if ("1" == response.data.result) {
                        try {
                            totalPage = response.data.toTalPage; //init;
                            totalRecords = response.data.totalRecords;
                        } catch (e) { }
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                    overlay = overlay - 1;
                    objFact.showOverlay(overlay);

                },
                function (response) {
                    overlay = overlay - 1;
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.loadlistDataEx = function (urlName, jsonData, callback) {
            $("#overlay").show(); //$('#myWaitModal').modal('show');

            $http({
                url: urlName,
                method: "POST",
                //data: $httpParamSerializerJQLike(jsonData),
                data: jsonData,
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json;charset=UTF-8",
                },
            }).then(
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    if ("1" == response.data.result) {
                        try {
                            totalPageEx = response.data.toTalPage; //init;
                            totalRecordsWx = response.data.totalRecords;
                        } catch (e) { }
                        callback(response);
                    } else {
                        objFact.showError(response.data.message);
                    }
                },
                function (response) {
                    $("#overlay").hide(); //$('#myWaitModal').modal('hide');
                    objFact.showError(response.data.message);
                }
            );
        };
        objFact.resetTicksOption = function () {
            ticksFactOption1 = {};
            ticksFactOption2 = {};
            ticksFactOption3 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
            ticksFactOption4 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
            ticksFactOption5 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
            ticksFactOption6 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
        };
        objFact.resetpositionOption = function () {
            //เพิ่มตอน pop ทำ graph mutiple product & item
            positionFactOption1 = {};
            positionFactOption2 = {};
            positionFactOption3 = {};
            positionFactOption4 = {};
            positionFactOption5 = {};
            positionFactOption6 = {};
        };
        objFact.getTicksOption = function (pos) {
            if (pos == "1") {
                return ticksFactOption1;
            } else if (pos == "2") {
                return ticksFactOption2;
            } else if (pos == "3") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                return ticksFactOption3;
            } else if (pos == "4") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                return ticksFactOption4;
            } else if (pos == "5") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                return ticksFactOption5;
            } else if (pos == "6") {
                //เพิ่มตอน pop ทำ graph mutiple y axis
                return ticksFactOption6;
            }
        };
        objFact.SetPositionOption = function (pos, value) {
            //เพิ่มตอน pop ทำ graph mutiple product & item
            if (pos == "1") {
                positionFactOption1 = value;
                return positionFactOption1;
            } else if (pos == "2") {
                positionFactOption2 = value;
                return positionFactOption2;
            } else if (pos == "3") {
                positionFactOption3 = value;
                return positionFactOption3;
            } else if (pos == "4") {
                positionFactOption4 = value;
                return positionFactOption4;
            } else if (pos == "5") {
                positionFactOption5 = value;
                return positionFactOption5;
            } else if (pos == "6") {
                positionFactOption6 = value;
                return positionFactOption6;
            }
        };
        objFact.setTicksOption = function (pos, value) {
            if (pos == "1") {
                ticksFactOption1 = value;
                return ticksFactOption1;
            } else if (pos == "2") {
                ticksFactOption2 = value;
                return ticksFactOption2;
            } else if (pos == "3") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                ticksFactOption3 = value;
                return ticksFactOption3;
            } else if (pos == "4") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                ticksFactOption4 = value;
                return ticksFactOption4;
            } else if (pos == "5") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                ticksFactOption5 = value;
                return ticksFactOption5;
            } else if (pos == "6") {
                ticksFactOption6 = value;
                return ticksFactOption6;
            }
        };
        objFact.getpositionOption = function (pos) {
            if (pos == "1") {
                return positionFactOption1;
            } else if (pos == "2") {
                return positionFactOption2;
            } else if (pos == "3") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                return positionFactOption3;
            } else if (pos == "4") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                return positionFactOption4;
            } else if (pos == "5") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                return positionFactOption5;
            } else if (pos == "6") {
                //เพิ่มตอน pop ทำ graph mutiple product & item
                return positionFactOption6;
            }
        };
        objFact.chkItemIsSame = function (data, value) {
            try {
                tempChkItemIsSame = {
                    disabledEditYAxis1: false,
                    disabledEditYAxis2: false,
                    disabledEditYAxis3: false,
                    disabledEditYAxis4: false,
                    disabledEditYAxis5: false,
                    disabledEditYAxis6: false,
                };
                var ListAllItem = [];
                var tempListAllItem = [];
                if (data.listSummary1 != null && data.listSummary1.ItemsName != null) {
                    data.listSummary1.No = 1;
                    data.listSummary1.AxesJoin = [];
                    ListAllItem.push(data.listSummary1);
                }
                if (data.listSummary2 != null && data.listSummary2.ItemsName != null) {
                    data.listSummary2.No = 2;
                    data.listSummary2.AxesJoin = [];
                    ListAllItem.push(data.listSummary2);
                }
                if (data.listSummary3 != null && data.listSummary3.ItemsName != null) {
                    data.listSummary3.No = 3;
                    data.listSummary3.AxesJoin = [];
                    ListAllItem.push(data.listSummary3);
                }
                if (data.listSummary4 != null && data.listSummary4.ItemsName != null) {
                    data.listSummary4.No = 4;
                    data.listSummary4.AxesJoin = [];
                    ListAllItem.push(data.listSummary4);
                }
                if (data.listSummary5 != null && data.listSummary5.ItemsName != null) {
                    data.listSummary5.No = 5;
                    data.listSummary5.AxesJoin = [];
                    ListAllItem.push(data.listSummary5);
                }
                if (data.listSummary6 != null && data.listSummary6.ItemsName != null) {
                    data.listSummary6.No = 6;
                    data.listSummary6.AxesJoin = [];
                    ListAllItem.push(data.listSummary6);
                }

                ListAllItem.forEach(function (entry, index) {
                    var findItemInArr = tempListAllItem.find(o => o.ItemsName == entry.ItemsName && o.UnitName == entry.UnitName);
                    if (typeof findItemInArr === 'undefined') {
                        entry.AxesJoin.push(entry.No);
                        tempListAllItem.push(entry);
                    } else {
                        if (entry.No == 1) tempChkItemIsSame.disabledEditYAxis1 = true;
                        if (entry.No == 2) tempChkItemIsSame.disabledEditYAxis2 = true;
                        if (entry.No == 3) tempChkItemIsSame.disabledEditYAxis3 = true;
                        if (entry.No == 4) tempChkItemIsSame.disabledEditYAxis4 = true;
                        if (entry.No == 5) tempChkItemIsSame.disabledEditYAxis5 = true;
                        if (entry.No == 6) tempChkItemIsSame.disabledEditYAxis6 = true;
                        let i = tempListAllItem.findIndex(i => i.ItemsName == entry.ItemsName);
                        tempListAllItem[i]['AxesJoin'].push(entry.No);
                        if (tempListAllItem[i]['SCALEMAX'] < entry.SCALEMAX) tempListAllItem[i]['SCALEMAX'] = entry.SCALEMAX;
                        if (tempListAllItem[i]['SCALEMIN'] > entry.SCALEMIN) tempListAllItem[i]['SCALEMIN'] = entry.SCALEMIN;
                        tempListAllItem[i]['STEPSIZE'] = (tempListAllItem[i]['SCALEMAX'] - tempListAllItem[i]['SCALEMIN']) / 10;
                    }
                });
                tempChkItemIsSame.tempListAllItem = tempListAllItem;
                return tempChkItemIsSame;
            } catch (e) {
                console.log(e);
            }
        };
        objFact.getoverScale = function () {
            return overScaleFact;
        };
        objFact.setBarGraphConfig = function (listGraphData, modelGraph) {
            var dateTime = [];
            var values1 = [];

            listGraphData.forEach(function (entry, index) {
                dateTime.push(entry.PREVIOUS);
                values1.push(entry.OFF_CONTROL);
            });

            var barChartData = {
                labels: dateTime,
                datasets: [
                    {
                        label: "OFF Control",
                        backgroundColor: window.chartColors.red,
                        borderColor: window.chartColors.red,
                        borderWidth: 1,
                        data: values1,
                    },
                ],
            };

            var graphConfig = {
                type: "bar",
                data: barChartData,
                options: {
                    responsive: true,
                    legend: { position: "top" },
                    title: { display: true, text: modelGraph.TITLE_GRAPH },
                    scales: {
                        xAxes: [
                            {
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: modelGraph.TITLE_GRAPH_X, //'Month'
                                },
                            },
                        ],
                        yAxes: [
                            {
                                ticks: {
                                    beginAtZero: true,
                                },
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: modelGraph.TITLE_GRAPH_Y, // '% Product Off Control'
                                },
                            },
                        ],
                    },
                },
            };

            return graphConfig;
        };
        objFact.setGraphConfig = function (
            graphDatas,
            ticksCheck,
            canvas,
            isFullName,
            controlConf,
            ConfTProduct
        ) {
            try {
                if (
                    typeof graphDatas.listTrend1 === "undefined" ||
                    graphDatas.listTrend1.length == 0
                ) {
                    return;
                }
                var dateTime = [];
                var dateTime2 = [];
                var values1 = [];
                var values2 = [];
                var values3 = []; //เพิ่มตอน pop ทำ graph mutiple product & item
                var values4 = []; //เพิ่มตอน pop ทำ graph mutiple product & item
                var values5 = []; //เพิ่มตอน pop ทำ graph mutiple product & item
                var values6 = []; //เพิ่มตอน pop ทำ graph mutiple product & item

                var dateDescription = [];

                var testvalues1 = [];
                var testvalues2 = [];

                var titleChart = graphDatas.listSummary1.ItemsName;

                if (isFullName == true) {
                    titleChart =
                        graphDatas.listSummary1.CustomerName +
                        " " +
                        graphDatas.listSummary1.ProductName +
                        " (" +
                        graphDatas.listSummary1.TankName +
                        ") " +
                        graphDatas.listSummary1.ItemsName +
                        " (" +
                        graphDatas.listSummary1.UnitName +
                        ")";
                }

                var decimal1 = 1;
                var decimal2 = 2;
                var decimal3 = 3; //เพิ่มตอน pop ทำ graph mutiple product & item
                var decimal4 = 4; //เพิ่มตอน pop ทำ graph mutiple product & item
                var decimal5 = 5; //เพิ่มตอน pop ทำ graph mutiple product & item
                var decimal6 = 6; //เพิ่มตอน pop ทำ graph mutiple product & item
                try {
                    var pieces = graphDatas.listSummary1.STEPSIZE.toString().split(".");
                    decimal1 = pieces[1].length;
                    graphDatas.listTrend1.forEach(function (entry, index) {
                        try {
                            var tmpPieces = entry.DATA_VALUE.toString().split(".");
                            if (tmpPieces[1].length > decimal1) {
                                decimal1 = tmpPieces[1].length;
                            }
                        } catch (e) { }
                    });
                } catch (e) { }
                var ticksOption = {
                    callback: function (label, index, labels) {
                        try {
                            return label.toFixed(decimal1);
                        } catch (e) {
                            return label;
                        }
                    },
                    suggestedMax: graphDatas.listSummary1.SCALEMAX.toFixed(decimal1),
                    suggestedMin: graphDatas.listSummary1.SCALEMIN.toFixed(decimal1),
                    stepSize: graphDatas.listSummary1.STEPSIZE.toFixed(decimal1),
                    fontColor: window.chartColors.blue,
                };
                var positionOption = "left";
                var positionOption2 = "left";
                var positionOption3 = "left";
                var positionOption4 = "left";
                var positionOption5 = "left";
                var positionOption6 = "left";
                var ticksOption2 = {};
                var ticksOption3 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
                var ticksOption4 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
                var ticksOption5 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
                var ticksOption6 = {}; //เพิ่มตอน pop ทำ graph mutiple product & item
                var lineChartData = {};
                var yAxesData = [];
                //start : เก็บ dateDiscription
                if (graphDatas.listTrend1.length > 0) {
                    graphDatas.listTrend1.forEach(function (entry, index) {
                        if (entry.DESCRIPTION != null && entry.DESCRIPTION != " ") {
                            dateDescription.push(entry.SHOW_DATE);
                        }
                    });
                }
                if (graphDatas.listTrend2.length > 0) {
                    graphDatas.listTrend2.forEach(function (entry, index) {
                        if (entry.DESCRIPTION != null && entry.DESCRIPTION != " ") {
                            dateDescription.push(entry.SHOW_DATE);
                        }
                    });
                }
                if (graphDatas.listTrend3.length > 0) {
                    graphDatas.listTrend3.forEach(function (entry, index) {
                        if (entry.DESCRIPTION != null && entry.DESCRIPTION != " ") {
                            dateDescription.push(entry.SHOW_DATE);
                        }
                    });
                }
                if (graphDatas.listTrend4.length > 0) {
                    graphDatas.listTrend4.forEach(function (entry, index) {
                        if (entry.DESCRIPTION != null && entry.DESCRIPTION != " ") {
                            dateDescription.push(entry.SHOW_DATE);
                        }
                    });
                }
                if (graphDatas.listTrend5.length > 0) {
                    graphDatas.listTrend5.forEach(function (entry, index) {
                        if (entry.DESCRIPTION != null && entry.DESCRIPTION != " ") {
                            dateDescription.push(entry.SHOW_DATE);
                        }
                    });
                }
                if (graphDatas.listTrend6.length > 0) {
                    graphDatas.listTrend6.forEach(function (entry, index) {
                        if (entry.DESCRIPTION != null && entry.DESCRIPTION != " ") {
                            dateDescription.push(entry.SHOW_DATE);
                        }
                    });
                }

                //var currentDate = graphDatas.listTrend1[0]['SHOW_DATE'];
                //var stopDate = graphDatas.listTrend1[graphDatas.listTrend1.length - 1]['SHOW_DATE'];
                //console.log(currentDate)
                //console.log(stopDate)
                //var dStart = currentDate.substr(0, 10).split("-");
                //var tStart = currentDate.substr(11, 8).split(":");
                //var start = new Date(dStart[2], dStart[1] - 1, dStart[0], tStart[0], tStart[1], tStart[2]);

                //var dStop = stopDate.substr(0, 10).split("-");
                //var tStop = stopDate.substr(11, 8).split(":");
                //var stop = new Date(dStop[2], dStop[1] - 1, dStop[0], tStop[0], tStop[1], tStop[2]);

                //while (start <= stop) {
                //    dateTime.push(start)
                //    start = new Date(start.setDate(start.getDate() + 1));
                //}
                //console.log(dateTime)

                //end : เก็บ dateDiscription
                //console.log(dateDescription)
                graphDatas.listTrend1.forEach(function (entry, index) {
                    var d = entry.SHOW_DATE;
                    var t = entry.SHOW_DATE;

                    d = d.substr(0, 10).split("-");
                    t = t.substr(11, 8).split(":");

                    dateTime.push(new Date(d[2], d[1] - 1, d[0], t[0], t[1], t[2]));
                    //if (entry.DESCRIPTION != null) {
                    //    values1.push(null);
                    //} else {
                    //    values1.push(entry.DATA_VALUE);
                    //}
                    var dt = { x: "", y: null, id: "A" }
                    dt.x = new Date(d[2], d[1] - 1, d[0], t[0], t[1], t[2]);
                    dt.id = "A" + index;
                    //var findDateInArr = dateDescription.find(o => o == entry.SHOW_DATE);
                    //if (typeof findDateInArr != 'undefined') {
                    //    values1.push(dt);
                    //}
                    //else
                    if (entry.DESCRIPTION != null) {
                        values1.push(dt);
                    } else {
                        if (entry.DISPLAY_VALUE != "NaN") {
                            dt.y = entry.DATA_VALUE;
                            values1.push(dt);
                        }
                    }

                });
                graphDatas.listTrend2.forEach(function (entry, index) {
                    var d = entry.SHOW_DATE;
                    var t = entry.SHOW_DATE;

                    d = d.substr(0, 10).split("-");
                    t = t.substr(11, 8).split(":");

                    //if (entry.DESCRIPTION != null) {
                    //    values2.push(null);
                    //} else {
                    //    values2.push(entry.DATA_VALUE);
                    //}
                    var dt = { x: "", y: null, id: "A" }
                    dt.x = new Date(d[2], d[1] - 1, d[0], t[0], t[1], t[2]);
                    dt.id = "A" + index;
                    //var findDateInArr = dateDescription.find(o => o == entry.SHOW_DATE);
                    //if (typeof findDateInArr != 'undefined') {
                    //    values2.push(dt);
                    //}
                    //else
                    if (entry.DESCRIPTION != null) {
                        values2.push(dt);
                    } else {
                        if (entry.DISPLAY_VALUE != "NaN") {
                            dt.y = entry.DATA_VALUE;
                            values2.push(dt);
                        }
                    }
                });
                //console.log(values2)
                //เพิ่มตอน pop ทำ graph mutiple product & item
                graphDatas.listTrend3.forEach(function (entry, index) {
                    var d = entry.SHOW_DATE;
                    var t = entry.SHOW_DATE;

                    d = d.substr(0, 10).split("-");
                    t = t.substr(11, 8).split(":");
                    //if (entry.DESCRIPTION != null) {
                    //    values3.push(null);
                    //} else {
                    //    values3.push(entry.DATA_VALUE);
                    //}
                    var dt = { x: "", y: null, id: "A" }
                    dt.x = new Date(d[2], d[1] - 1, d[0], t[0], t[1], t[2]);
                    dt.id = "A" + index;
                    //var findDateInArr = dateDescription.find(o => o == entry.SHOW_DATE);
                    //if (typeof findDateInArr != 'undefined') {
                    //    values3.push(dt);
                    //}
                    //else
                    if (entry.DESCRIPTION != null) {
                        values3.push(dt);
                    } else {
                        if (entry.DISPLAY_VALUE != "NaN") {
                            dt.y = entry.DATA_VALUE;
                            values3.push(dt);
                        }
                    }
                });
                //เพิ่มตอน pop ทำ graph mutiple product & item
                graphDatas.listTrend4.forEach(function (entry, index) {
                    var d = entry.SHOW_DATE;
                    var t = entry.SHOW_DATE;

                    d = d.substr(0, 10).split("-");
                    t = t.substr(11, 8).split(":");
                    //if (entry.DESCRIPTION != null) {
                    //    values4.push(null);
                    //} else {
                    //    values4.push(entry.DATA_VALUE);
                    //}
                    var dt = { x: "", y: null, id: "A" }
                    dt.x = new Date(d[2], d[1] - 1, d[0], t[0], t[1], t[2]);
                    dt.id = "A" + index;

                    //var findDateInArr = dateDescription.find(o => o == entry.SHOW_DATE);
                    //if (typeof findDateInArr != 'undefined') {
                    //    values4.push(dt);
                    //}
                    //else
                    if (entry.DESCRIPTION != null) {
                        values4.push(dt);
                    } else {
                        if (entry.DISPLAY_VALUE != "NaN") {
                            dt.y = entry.DATA_VALUE;
                            values4.push(dt);
                        }
                    }
                });
                //เพิ่มตอน pop ทำ graph mutiple product & item
                graphDatas.listTrend5.forEach(function (entry, index) {
                    var d = entry.SHOW_DATE;
                    var t = entry.SHOW_DATE;

                    d = d.substr(0, 10).split("-");
                    t = t.substr(11, 8).split(":");
                    //if (entry.DESCRIPTION != null) {
                    //    values5.push(null);
                    //} else {
                    //    values5.push(entry.DATA_VALUE);
                    //}
                    var dt = { x: "", y: null, id: "A" }
                    dt.x = new Date(d[2], d[1] - 1, d[0], t[0], t[1], t[2]);
                    dt.id = "A" + index;

                    //var findDateInArr = dateDescription.find(o => o == entry.SHOW_DATE);
                    //if (typeof findDateInArr != 'undefined') {
                    //    values5.push(dt);
                    //}
                    //else
                    if (entry.DESCRIPTION != null) {
                        values5.push(dt);
                    } else {
                        if (entry.DISPLAY_VALUE != "NaN") {
                            dt.y = entry.DATA_VALUE;
                            values5.push(dt);
                        }
                    }
                });
                //เพิ่มตอน pop ทำ graph mutiple product & item
                graphDatas.listTrend6.forEach(function (entry, index) {
                    var d = entry.SHOW_DATE;
                    var t = entry.SHOW_DATE;

                    d = d.substr(0, 10).split("-");
                    t = t.substr(11, 8).split(":");
                    //if (entry.DESCRIPTION != null) {
                    //    values6.push(null);
                    //} else {
                    //    values6.push(entry.DATA_VALUE);
                    //}
                    var dt = { x: "", y: null, id: "A" }
                    dt.x = new Date(d[2], d[1] - 1, d[0], t[0], t[1], t[2]);
                    dt.id = "A" + index;

                    //var findDateInArr = dateDescription.find(o => o == entry.SHOW_DATE);
                    //if (typeof findDateInArr != 'undefined') {
                    //    values6.push(dt);
                    //}
                    //else
                    if (entry.DESCRIPTION != null) {
                        values6.push(dt);
                    } else {
                        if (entry.DISPLAY_VALUE != "NaN") {
                            dt.y = entry.DATA_VALUE;
                            values6.push(dt);
                        }
                    }
                });

                if (ticksCheck) {
                    if ($.isEmptyObject(ticksFactOption1)) {
                        ticksFactOption1 = ticksOption;
                    } else {
                        ticksOption = ticksFactOption1;
                    }
                    if ($.isEmptyObject(positionFactOption1)) {
                        positionFactOption1 = positionOption;
                    } else {
                        positionOption = positionFactOption1;
                    }
                }

                var minDateTime = dateTime[0];
                var maxDateTime = dateTime[dateTime.length - 1];

                minDateTime = new Date(
                    minDateTime.getFullYear(),
                    minDateTime.getMonth(),
                    minDateTime.getDate(),
                    0,
                    0,
                    0,
                    0
                );
                maxDateTime = new Date(
                    maxDateTime.getFullYear(),
                    maxDateTime.getMonth(),
                    maxDateTime.getDate(),
                    23,
                    59,
                    59,
                    0
                );
                if (graphDatas.listTrend1.length > 0) {
                    lineChartData = {
                        labels: dateTime,
                        datasets: [
                            {
                                label: graphDatas.listSummary1.ItemsName,
                                backgroundColor: window.chartColors.blue,
                                borderColor: window.chartColors.blue,
                                fill: false,
                                pointHoverRadius: 10,
                                data: values1,
                                fill: false,
                                yAxisID: "y-axis-1",
                                spanGaps: false,
                                tension: 0,
                                datalabels: {
                                    display: false,
                                },
                            },
                        ],
                    };
                    yAxesData = [
                        {
                            display: true,
                            position: positionOption,
                            id: "y-axis-1",
                            scaleLabel: {
                                display: true,
                                labelString: graphDatas.listSummary1.ItemsName + ' (' + graphDatas.listSummary1.UnitName + ')',
                            },
                            ticks: ticksOption,
                        },
                    ];
                }
                if (graphDatas.listTrend2.length > 0) {

                    titleChart =
                        titleChart +
                        " VS " +
                        graphDatas.listSummary2.ItemsName;
                    if (isFullName == true) {
                        if (
                            graphDatas.listSummary1.ItemsName ==
                            graphDatas.listSummary2.ItemsName
                        ) {
                            if (
                                graphDatas.listSummary1.UnitName !=
                                graphDatas.listSummary2.UnitName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ") VS " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                            } else if (
                                graphDatas.listSummary1.TankName !=
                                graphDatas.listSummary2.TankName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                            } else {
                                titleChart =
                                    graphDatas.listSummary1.CustomerName +
                                    " " +
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.CustomerName +
                                    " " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                            }
                        }
                    }
                    try {
                        var pieces2 =
                            graphDatas.listSummary2.STEPSIZE.toString().split(".");
                        decimal2 = pieces2[1].length;

                        graphDatas.listTrend2.forEach(function (entry, index) {
                            try {
                                var tmpPieces = entry.DATA_VALUE.toString().split(".");
                                if (tmpPieces[1].length > decimal2) {
                                    decimal2 = tmpPieces[1].length;
                                }
                            } catch (e) { }
                        });
                    } catch (e) { }
                    ticksOption2 = {
                        //mode: 'label',
                        callback: function (label, index, labels) {
                            try {
                                return label.toFixed(decimal2);
                            } catch (e) {
                                return label;
                            }
                        },
                        suggestedMax: graphDatas.listSummary2.SCALEMAX.toFixed(decimal2),
                        suggestedMin: graphDatas.listSummary2.SCALEMIN.toFixed(decimal2),
                        stepSize: graphDatas.listSummary2.STEPSIZE.toFixed(decimal2),
                        fontColor: window.chartColors.green,
                    };
                    if (ticksCheck) {
                        if ($.isEmptyObject(ticksFactOption2)) {
                            ticksFactOption2 = ticksOption2;
                        } else {
                            ticksOption2 = ticksFactOption2;
                        }
                        if ($.isEmptyObject(positionFactOption2)) {
                            positionFactOption2 = positionOption2;
                        } else {
                            positionOption2 = positionFactOption2;
                        }
                    }
                    var yAxisID1 = "y-axis-1";
                    var yAxisID2 = "y-axis-2";
                    var overScale = false;
                    if (
                        graphDatas.listSummary1.SCALEMAX > graphDatas.listSummary2.SCALEMAX
                    ) {
                        if (
                            graphDatas.listSummary1.SCALEMAX >
                            graphDatas.listSummary2.SCALEMAX * 2
                        ) {
                            overScale = true;
                        }
                    } else {
                        if (
                            graphDatas.listSummary2.SCALEMAX >
                            graphDatas.listSummary1.SCALEMAX * 2
                        ) {
                            overScale = true;
                        }
                    }
                    overScaleFact = overScale;
                    var newTrendDataSet = {
                        label: graphDatas.listSummary2.ItemsName,
                        backgroundColor: window.chartColors.green,
                        borderColor: window.chartColors.green,
                        pointHoverRadius: 10,
                        data: values2,
                        fill: false,
                        yAxisID: yAxisID2,
                        spanGaps: false,
                        tension: 0,
                        datalabels: {
                            display: false,
                        },
                    };
                    var newYAxesData = {
                        type: "linear",
                        display: true,
                        position: positionOption2,
                        id: "y-axis-2",
                        scaleLabel: {
                            display: true,
                            labelString: graphDatas.listSummary2.ItemsName + ' (' + graphDatas.listSummary2.UnitName + ')',
                        },
                        ticks: ticksOption2,
                        gridLines: {
                            drawOnChartArea: false,
                        },
                    }
                    if (
                        controlConf == true || ConfTProduct == true
                    ) {
                        var nResult = chkDataYAxis(
                            tempChkItemIsSame.tempListAllItem,
                            yAxisID2,
                            2
                        );

                        if (nResult.falgYAxis == true) {
                            yAxisID2 = nResult.yAxisID;
                            newTrendDataSet.yAxisID = yAxisID2
                        } else {
                            yAxesData.push(newYAxesData);
                        }
                    } else if (
                        graphDatas.listSummary1.UnitName ==
                        graphDatas.listSummary2.UnitName &&
                        graphDatas.control1.SpecView == graphDatas.control2.SpecView &&
                        false == overScale
                    ) {
                        yAxisID2 = "y-axis-1"; //ใช้แกนร่วมกัน
                        newTrendDataSet.yAxisID = yAxisID2
                    } else {
                        yAxesData.push(newYAxesData);
                    }
                    lineChartData.datasets.push(newTrendDataSet);

                }
                if (graphDatas.listTrend3.length > 0) {
                    titleChart =
                        titleChart +
                        " VS " +
                        graphDatas.listSummary3.ItemsName;
                    if (isFullName == true) {
                        if (
                            (graphDatas.listSummary1.ItemsName ==
                                graphDatas.listSummary2.ItemsName) ==
                            graphDatas.listSummary3.ItemsName
                        ) {
                            if (
                                (graphDatas.listSummary1.UnitName !=
                                    graphDatas.listSummary2.UnitName) !=
                                graphDatas.listSummary3.UnitName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ") VS " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")" +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                            } else if (
                                (graphDatas.listSummary1.TankName !=
                                    graphDatas.listSummary2.TankName) !=
                                graphDatas.listSummary3.TankName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                            } else {
                                titleChart =
                                    graphDatas.listSummary1.CustomerName +
                                    " " +
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.CustomerName +
                                    " " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.CustomerName +
                                    " " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                            }
                        }
                    }
                    try {
                        var pieces3 =
                            graphDatas.listSummary3.STEPSIZE.toString().split(".");
                        decimal3 = pieces3[1].length;
                        graphDatas.listTrend3.forEach(function (entry, index) {
                            try {
                                var tmpPieces = entry.DATA_VALUE.toString().split(".");
                                if (tmpPieces[1].length > decimal3) {
                                    decimal3 = tmpPieces[1].length;
                                }
                            } catch (e) { }
                        });
                    } catch (e) { }
                    ticksOption3 = {
                        callback: function (label, index, labels) {
                            try {
                                return label.toFixed(decimal2);
                            } catch (e) {
                                return label;
                            }
                        },
                        suggestedMax: graphDatas.listSummary3.SCALEMAX.toFixed(decimal3),
                        suggestedMin: graphDatas.listSummary3.SCALEMIN.toFixed(decimal3),
                        stepSize: graphDatas.listSummary3.STEPSIZE.toFixed(decimal3),
                        fontColor: "#A0522D",
                    };
                    if (ticksCheck) {
                        if ($.isEmptyObject(ticksFactOption3)) {
                            ticksFactOption3 = ticksOption3;
                        } else {
                            ticksOption3 = ticksFactOption3;
                        }
                        if ($.isEmptyObject(positionFactOption3)) {
                            positionFactOption3 = positionOption3;
                        } else {
                            positionOption3 = positionFactOption3;
                        }
                    }
                    var yAxisID3 = "y-axis-3";
                    var newTrendDataSet = {
                        label: graphDatas.listSummary3.ItemsName,
                        backgroundColor: "#A0522D",
                        borderColor: "#A0522D",
                        fill: false,
                        pointHoverRadius: 10,
                        data: values3,
                        fill: false,
                        yAxisID: yAxisID3,
                        spanGaps: false,
                        tension: 0,
                        datalabels: {
                            display: false,
                        },
                    };
                    var newYAxesData = {
                        type: "linear",
                        display: true,
                        position: positionOption3,
                        id: "y-axis-3",
                        scaleLabel: {
                            display: true,
                            labelString: graphDatas.listSummary3.ItemsName + ' (' + graphDatas.listSummary3.UnitName + ')',
                        },
                        ticks: ticksOption3,
                        gridLines: {
                            drawOnChartArea: false,
                        },
                    }
                    if (
                        controlConf == true || ConfTProduct == true
                    ) {
                        var nResult = chkDataYAxis(
                            tempChkItemIsSame.tempListAllItem,
                            yAxisID3,
                            3
                        );
                        if (nResult.falgYAxis == true) {
                            yAxisID3 = nResult.yAxisID;
                            newTrendDataSet.yAxisID = yAxisID3
                        } else {
                            yAxesData.push(newYAxesData);
                        }
                    } else {
                        yAxesData.push(newYAxesData);
                    }
                    lineChartData.datasets.push(newTrendDataSet);
                }
                if (graphDatas.listTrend4.length > 0) {
                    titleChart =
                        titleChart +
                        " VS " +
                        graphDatas.listSummary4.ItemsName;
                    if (isFullName == true) {
                        if (
                            ((graphDatas.listSummary1.ItemsName ==
                                graphDatas.listSummary2.ItemsName) ==
                                graphDatas.listSummary3.ItemsName) ==
                            graphDatas.listSummary4.ItemsName
                        ) {
                            if (
                                ((graphDatas.listSummary1.UnitName !=
                                    graphDatas.listSummary2.UnitName) !=
                                    graphDatas.listSummary3.UnitName) !=
                                graphDatas.listSummary4.UnitName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ") VS " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")" +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")" +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")";
                            } else if (
                                ((graphDatas.listSummary1.TankName !=
                                    graphDatas.listSummary2.TankName) !=
                                    graphDatas.listSummary3.TankName) !=
                                graphDatas.listSummary4.TankName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary4.ProductName +
                                    " (" +
                                    graphDatas.listSummary4.TankName +
                                    ") " +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")";
                            } else {
                                titleChart =
                                    graphDatas.listSummary1.CustomerName +
                                    " " +
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.CustomerName +
                                    " " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.CustomerName +
                                    " " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary4.CustomerName +
                                    " " +
                                    graphDatas.listSummary4.ProductName +
                                    " (" +
                                    graphDatas.listSummary4.TankName +
                                    ") " +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")";
                            }
                        }
                    }
                    try {
                        var pieces4 =
                            graphDatas.listSummary4.STEPSIZE.toString().split(".");
                        decimal4 = pieces4[1].length;
                        graphDatas.listTrend4.forEach(function (entry, index) {
                            try {
                                var tmpPieces = entry.DATA_VALUE.toString().split(".");
                                if (tmpPieces[1].length > decimal4) {
                                    decimal4 = tmpPieces[1].length;
                                }
                            } catch (e) { }
                        });
                    } catch (e) { }
                    ticksOption4 = {
                        callback: function (label, index, labels) {
                            try {
                                return label.toFixed(decimal2);
                            } catch (e) {
                                return label;
                            }
                        },
                        suggestedMax: graphDatas.listSummary4.SCALEMAX.toFixed(decimal4),
                        suggestedMin: graphDatas.listSummary4.SCALEMIN.toFixed(decimal4),
                        stepSize: graphDatas.listSummary4.STEPSIZE.toFixed(decimal4),
                        fontColor: "#80ced6",
                    };
                    if (ticksCheck) {
                        if ($.isEmptyObject(ticksFactOption4)) {
                            ticksFactOption4 = ticksOption4;
                        } else {
                            ticksOption4 = ticksFactOption4;
                        }
                        if ($.isEmptyObject(positionFactOption4)) {
                            positionFactOption4 = positionOption4;
                        } else {
                            positionOption4 = positionFactOption4;
                        }
                    }
                    var yAxisID4 = "y-axis-4";
                    var newTrendDataSet = {
                        label: graphDatas.listSummary4.ItemsName,
                        backgroundColor: "#80ced6",
                        borderColor: "#80ced6",
                        fill: false,
                        pointHoverRadius: 10,
                        data: values4,
                        fill: false,
                        yAxisID: yAxisID4,
                        spanGaps: false,
                        tension: 0,
                        datalabels: {
                            display: false,
                        },
                    };
                    var newYAxesData = {
                        type: "linear",
                        display: true,
                        position: positionOption4,
                        id: "y-axis-4",
                        scaleLabel: {
                            display: true,
                            labelString: graphDatas.listSummary4.ItemsName + ' (' + graphDatas.listSummary4.UnitName + ')',
                        },
                        ticks: ticksOption4,
                        gridLines: {
                            drawOnChartArea: false,
                        },
                    }
                    if (
                        controlConf == true || ConfTProduct == true
                    ) {
                        var nResult = chkDataYAxis(
                            tempChkItemIsSame.tempListAllItem,
                            yAxisID4,
                            4
                        );
                        if (nResult.falgYAxis == true) {
                            yAxisID4 = nResult.yAxisID;
                            newTrendDataSet.yAxisID = yAxisID4
                        } else {
                            yAxesData.push(newYAxesData);
                        }
                    } else {
                        yAxesData.push(newYAxesData);
                    }
                    lineChartData.datasets.push(newTrendDataSet);
                }
                if (graphDatas.listTrend5.length > 0) {
                    titleChart =
                        titleChart +
                        " VS " +
                        graphDatas.listSummary5.ItemsName;
                    if (isFullName == true) {
                        if (
                            (((graphDatas.listSummary1.ItemsName ==
                                graphDatas.listSummary2.ItemsName) ==
                                graphDatas.listSummary3.ItemsName) ==
                                graphDatas.listSummary4.ItemsName) ==
                            graphDatas.listSummary5.ItemsName
                        ) {
                            if (
                                (((graphDatas.listSummary1.UnitName !=
                                    graphDatas.listSummary2.UnitName) !=
                                    graphDatas.listSummary3.UnitName) !=
                                    graphDatas.listSummary4.UnitName) !=
                                graphDatas.listSummary5.UnitName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ") VS " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")" +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")" +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")" +
                                    graphDatas.listSummary5.ItemsName +
                                    " (" +
                                    graphDatas.listSummary5.UnitName +
                                    ")";
                            } else if (
                                (((graphDatas.listSummary1.TankName !=
                                    graphDatas.listSummary2.TankName) !=
                                    graphDatas.listSummary3.TankName) !=
                                    graphDatas.listSummary4.TankName) !=
                                graphDatas.listSummary5.TankName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary4.ProductName +
                                    " (" +
                                    graphDatas.listSummary4.TankName +
                                    ") " +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary5.ProductName +
                                    " (" +
                                    graphDatas.listSummary5.TankName +
                                    ") " +
                                    graphDatas.listSummary5.ItemsName +
                                    " (" +
                                    graphDatas.listSummary5.UnitName +
                                    ")";
                            } else {
                                titleChart =
                                    graphDatas.listSummary1.CustomerName +
                                    " " +
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.CustomerName +
                                    " " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.CustomerName +
                                    " " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary4.CustomerName +
                                    " " +
                                    graphDatas.listSummary4.ProductName +
                                    " (" +
                                    graphDatas.listSummary4.TankName +
                                    ") " +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary5.CustomerName +
                                    " " +
                                    graphDatas.listSummary5.ProductName +
                                    " (" +
                                    graphDatas.listSummary5.TankName +
                                    ") " +
                                    graphDatas.listSummary5.ItemsName +
                                    " (" +
                                    graphDatas.listSummary5.UnitName +
                                    ")";
                            }
                        }
                    }
                    try {
                        var pieces5 =
                            graphDatas.listSummary5.STEPSIZE.toString().split(".");
                        decimal5 = pieces5[1].length;
                        graphDatas.listTrend5.forEach(function (entry, index) {
                            try {
                                var tmpPieces = entry.DATA_VALUE.toString().split(".");
                                if (tmpPieces[1].length > decimal5) {
                                    decimal5 = tmpPieces[1].length;
                                }
                            } catch (e) { }
                        });
                    } catch (e) { }
                    ticksOption5 = {
                        callback: function (label, index, labels) {
                            try {
                                return label.toFixed(decimal2);
                            } catch (e) {
                                return label;
                            }
                        },
                        suggestedMax: graphDatas.listSummary5.SCALEMAX.toFixed(decimal5),
                        suggestedMin: graphDatas.listSummary5.SCALEMIN.toFixed(decimal5),
                        stepSize: graphDatas.listSummary5.STEPSIZE.toFixed(decimal5),
                        fontColor: window.chartColors.purple,
                    };
                    if (ticksCheck) {
                        if ($.isEmptyObject(ticksFactOption5)) {
                            ticksFactOption5 = ticksOption5;
                        } else {
                            ticksOption5 = ticksFactOption5;
                        }
                        if ($.isEmptyObject(positionFactOption5)) {
                            positionFactOption5 = positionOption5;
                        } else {
                            positionOption5 = positionFactOption5;
                        }
                    }
                    var yAxisID5 = "y-axis-5";
                    var newTrendDataSet = {
                        label: graphDatas.listSummary5.ItemsName,
                        backgroundColor: window.chartColors.purple,
                        borderColor: window.chartColors.purple,
                        fill: false,
                        pointHoverRadius: 10,
                        data: values5,
                        fill: false,
                        yAxisID: yAxisID5,
                        spanGaps: false,
                        tension: 0,
                        datalabels: {
                            display: false,
                        },
                    };
                    var newYAxesData = {
                        type: "linear",
                        display: true,
                        position: positionOption5,
                        id: "y-axis-5",
                        scaleLabel: {
                            display: true,
                            labelString: graphDatas.listSummary5.ItemsName + ' (' + graphDatas.listSummary5.UnitName + ')',
                        },
                        ticks: ticksOption5,
                        gridLines: {
                            drawOnChartArea: false,
                        },
                    }
                    if (
                        controlConf == true || ConfTProduct == true
                    ) {
                        var nResult = chkDataYAxis(
                            tempChkItemIsSame.tempListAllItem,
                            yAxisID5,
                            5
                        );
                        if (nResult.falgYAxis == true) {
                            yAxisID5 = nResult.yAxisID;
                            newTrendDataSet.yAxisID = yAxisID5
                        } else {
                            yAxesData.push(newYAxesData);
                        }
                    } else {
                        yAxesData.push(newYAxesData);
                    }
                    lineChartData.datasets.push(newTrendDataSet);
                }
                if (graphDatas.listTrend6.length > 0) {
                    titleChart =
                        titleChart +
                        " VS " +
                        graphDatas.listSummary6.ItemsName;
                    if (isFullName == true) {
                        if (
                            ((((graphDatas.listSummary1.ItemsName ==
                                graphDatas.listSummary2.ItemsName) ==
                                graphDatas.listSummary3.ItemsName) ==
                                graphDatas.listSummary4.ItemsName) ==
                                graphDatas.listSummary5.ItemsName) ==
                            graphDatas.listSummary6.ItemsName
                        ) {
                            if (
                                ((((graphDatas.listSummary1.UnitName !=
                                    graphDatas.listSummary2.UnitName) !=
                                    graphDatas.listSummary3.UnitName) !=
                                    graphDatas.listSummary4.UnitName) !=
                                    graphDatas.listSummary5.UnitName) !=
                                graphDatas.listSummary6.UnitName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ") VS " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")" +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")" +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")" +
                                    graphDatas.listSummary5.ItemsName +
                                    " (" +
                                    graphDatas.listSummary5.UnitName +
                                    ")" +
                                    graphDatas.listSummary6.ItemsName +
                                    " (" +
                                    graphDatas.listSummary6.UnitName +
                                    ")";
                            } else if (
                                ((((graphDatas.listSummary1.TankName !=
                                    graphDatas.listSummary2.TankName) !=
                                    graphDatas.listSummary3.TankName) !=
                                    graphDatas.listSummary4.TankName) !=
                                    graphDatas.listSummary5.TankName) !=
                                graphDatas.listSummary6.TankName
                            ) {
                                titleChart =
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary4.ProductName +
                                    " (" +
                                    graphDatas.listSummary4.TankName +
                                    ") " +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary5.ProductName +
                                    " (" +
                                    graphDatas.listSummary5.TankName +
                                    ") " +
                                    graphDatas.listSummary5.ItemsName +
                                    " (" +
                                    graphDatas.listSummary5.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary6.ProductName +
                                    " (" +
                                    graphDatas.listSummary6.TankName +
                                    ") " +
                                    graphDatas.listSummary6.ItemsName +
                                    " (" +
                                    graphDatas.listSummary6.UnitName +
                                    ")";
                            } else {
                                titleChart =
                                    graphDatas.listSummary1.CustomerName +
                                    " " +
                                    graphDatas.listSummary1.ProductName +
                                    " (" +
                                    graphDatas.listSummary1.TankName +
                                    ") " +
                                    graphDatas.listSummary1.ItemsName +
                                    " (" +
                                    graphDatas.listSummary1.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary2.CustomerName +
                                    " " +
                                    graphDatas.listSummary2.ProductName +
                                    " (" +
                                    graphDatas.listSummary2.TankName +
                                    ") " +
                                    graphDatas.listSummary2.ItemsName +
                                    " (" +
                                    graphDatas.listSummary2.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary3.CustomerName +
                                    " " +
                                    graphDatas.listSummary3.ProductName +
                                    " (" +
                                    graphDatas.listSummary3.TankName +
                                    ") " +
                                    graphDatas.listSummary3.ItemsName +
                                    " (" +
                                    graphDatas.listSummary3.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary4.CustomerName +
                                    " " +
                                    graphDatas.listSummary4.ProductName +
                                    " (" +
                                    graphDatas.listSummary4.TankName +
                                    ") " +
                                    graphDatas.listSummary4.ItemsName +
                                    " (" +
                                    graphDatas.listSummary4.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary5.CustomerName +
                                    " " +
                                    graphDatas.listSummary5.ProductName +
                                    " (" +
                                    graphDatas.listSummary5.TankName +
                                    ") " +
                                    graphDatas.listSummary5.ItemsName +
                                    " (" +
                                    graphDatas.listSummary5.UnitName +
                                    ")";
                                titleChart +=
                                    " vs " +
                                    graphDatas.listSummary6.CustomerName +
                                    " " +
                                    graphDatas.listSummary6.ProductName +
                                    " (" +
                                    graphDatas.listSummary6.TankName +
                                    ") " +
                                    graphDatas.listSummary6.ItemsName +
                                    " (" +
                                    graphDatas.listSummary6.UnitName +
                                    ")";
                            }
                        }
                    }
                    try {
                        var pieces6 =
                            graphDatas.listSummary6.STEPSIZE.toString().split(".");
                        decimal6 = pieces6[1].length;
                        graphDatas.listTrend6.forEach(function (entry, index) {
                            try {
                                var tmpPieces = entry.DATA_VALUE.toString().split(".");
                                if (tmpPieces[1].length > decimal6) {
                                    decimal6 = tmpPieces[1].length;
                                }
                            } catch (e) { }
                        });
                    } catch (e) { }
                    ticksOption6 = {
                        callback: function (label, index, labels) {
                            try {
                                return label.toFixed(decimal2);
                            } catch (e) {
                                return label;
                            }
                        },
                        suggestedMax: graphDatas.listSummary6.SCALEMAX.toFixed(decimal6),
                        suggestedMin: graphDatas.listSummary6.SCALEMIN.toFixed(decimal6),
                        stepSize: graphDatas.listSummary6.STEPSIZE.toFixed(decimal6),
                        fontColor: '#2F4F4F',
                    };
                    if (ticksCheck) {
                        if ($.isEmptyObject(ticksFactOption6)) {
                            ticksFactOption6 = ticksOption6;
                        } else {
                            ticksOption6 = ticksFactOption6;
                        }
                        if ($.isEmptyObject(positionFactOption6)) {
                            positionFactOption6 = positionOption6;
                        } else {
                            positionOption6 = positionFactOption6;
                        }
                    }
                    var yAxisID6 = "y-axis-6";
                    var newTrendDataSet = {
                        label: graphDatas.listSummary6.ItemsName,
                        backgroundColor: '#2F4F4F',
                        borderColor: '#2F4F4F',
                        fill: false,
                        pointHoverRadius: 10,
                        data: values6,
                        fill: false,
                        yAxisID: yAxisID6,
                        spanGaps: false,
                        tension: 0,
                        datalabels: {
                            display: false,
                        },
                    };
                    var newYAxesData = {
                        type: "linear",
                        display: true,
                        position: positionOption6,
                        id: "y-axis-6",
                        scaleLabel: {
                            display: true,
                            labelString: graphDatas.listSummary6.ItemsName + ' (' + graphDatas.listSummary6.UnitName + ')',
                        },
                        ticks: ticksOption6,
                        gridLines: {
                            drawOnChartArea: false,
                        },
                    }
                    if (
                        controlConf == true || ConfTProduct == true
                    ) {
                        var nResult = chkDataYAxis(
                            tempChkItemIsSame.tempListAllItem,
                            yAxisID6,
                            6
                        );
                        if (nResult.falgYAxis == true) {
                            yAxisID6 = nResult.yAxisID;
                            newTrendDataSet.yAxisID = yAxisID6
                        } else {
                            yAxesData.push(newYAxesData);
                        }
                    } else {
                        yAxesData.push(newYAxesData);
                    }
                    lineChartData.datasets.push(newTrendDataSet);
                }
                var graphConfig = {
                    type: "line",
                    data: lineChartData,
                    options: {
                        legend: {
                            paddingBottom: 25,
                        },
                        //tooltips: {
                        //    enabled: true,
                        //    mode: "single",
                        //    intersect: false,
                        //    callbacks: {
                        //        title: function (tooltipItem, data) {
                        //            //console.log(tooltipItem)
                        //            //console.log(data)
                        //            try {
                        //                var d = data.labels[tooltipItem[0].index];
                        //                //console.log(d)
                        //                return getDateFormatShowNew(tooltipItem[0].xLabel);
                        //                //return getDateFormatShow(d);
                        //                //return tooltipItem[0].xLabel;
                        //            } catch (e) {
                        //                return tooltipItem[0].xLabel;
                        //            }
                        //        },
                        //    },
                        //},
                        tooltips: {
                            enabled: true,
                            mode: 'nearest',
                            intersect: false,
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    try {
                                        if (data.datasets[tooltipItem[0].datasetIndex]['label'].substring(0, 7) == "Control") {
                                            return "";
                                        }
                                        return getDateFormatShowNew(tooltipItem[0].xLabel);
                                    } catch (e) {
                                        return tooltipItem[0].xLabel;
                                    }
                                },
                            },
                        },
                        responsive: true,
                        title: {
                            display: true,
                            text: titleChart,
                        },
                        hover: {
                            mode: "single",
                            intersect: false,
                        },
                        scales: {
                            xAxes: [
                                {
                                    type: "time",
                                    time: {
                                        displayFormats: {
                                            millisecond: "SSS [ms]",
                                            second: "h:mm:ss a",
                                            minute: "h:mm:ss a",
                                            hour: "D MMM YYYY  hA", // 'D MMM YYYY  hA',
                                            day: "D MMM YYYY",
                                            week: "D MMM YYYY",
                                            month: "D MMM YYYY",
                                            quarter: "D MMM YYYY",
                                            year: "D MMM YYYY",
                                        },
                                        min: minDateTime, //dateTime[0],
                                        max: maxDateTime, //dateTime[dateTime.length-1]
                                    },
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: "Date",
                                    },
                                    ticks: {
                                        autoSkip: true,
                                        maxRotation: 30,
                                        minRotation: 30,
                                        maxTicksLimit: 20,
                                        source: "auto",
                                    },
                                },
                            ],
                            yAxes: yAxesData,
                        },
                        legend: {
                            display: true,
                            labels: {
                                boxWidth: 10,
                                filter: (item) => {
                                    if (item.text != "Control 23456" && item.text != "") {
                                        return true;
                                    }
                                    //return item.text != "Control 23456"
                                },
                            },
                        },
                        plugins: {
                            datalabels: {
                                color: window.chartColors.blue,
                                align: "end",
                                anchor: "center",
                                rotation: -90,
                                display: true,
                                formatter: function (value, ctx) {
                                    //try {
                                    if ("undefined" !== typeof ctx.dataset.datalabels) {
                                        return ctx.dataset.datalabels.data[ctx.dataIndex];
                                    } else {
                                        return "";
                                    }

                                    //}
                                    //catch(e) {
                                    //console.log(e);
                                    //}

                                },
                            },
                        },
                    },
                };

                if (
                    graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length == 0 &&
                    graphDatas.listTrend3.length == 0 &&
                    graphDatas.listTrend4.length == 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0
                ) {
                    var newDataset = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend1,
                        window.chartColors.blue,
                        graphConfig.data.datasets[0]['yAxisID']
                    );
                    graphConfig.data.datasets.push(newDataset);
                } else if (
                    graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length == 0 &&
                    graphDatas.listTrend4.length == 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0
                ) {
                    var newDataset = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend1,
                        window.chartColors.blue,
                        graphConfig.data.datasets[0]['yAxisID']
                    );
                    var newDataset2 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend2,
                        window.chartColors.green,
                        graphConfig.data.datasets[1]['yAxisID']
                    );
                    graphConfig.data.datasets.push(newDataset);
                    graphConfig.data.datasets.push(newDataset2);
                } else if (
                    graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length == 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0
                ) {
                    var newDataset = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend1,
                        window.chartColors.blue,
                        graphConfig.data.datasets[0]['yAxisID']
                    );
                    var newDataset2 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend2,
                        window.chartColors.green,
                        graphConfig.data.datasets[1]['yAxisID']
                    );
                    var newDataset3 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend3,
                        "#A0522D",
                        graphConfig.data.datasets[2]['yAxisID']
                    );
                    graphConfig.data.datasets.push(newDataset);
                    graphConfig.data.datasets.push(newDataset2);
                    graphConfig.data.datasets.push(newDataset3);
                } else if (
                    graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length > 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0
                ) {
                    var newDataset = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend1,
                        window.chartColors.blue,
                        graphConfig.data.datasets[0]['yAxisID']
                    );
                    var newDataset2 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend2,
                        window.chartColors.green,
                        graphConfig.data.datasets[1]['yAxisID']
                    );
                    var newDataset3 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend3,
                        "#A0522D",
                        graphConfig.data.datasets[2]['yAxisID']
                    );
                    var newDataset4 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend4,
                        "#80ced6",
                        graphConfig.data.datasets[3]['yAxisID']
                    );
                    graphConfig.data.datasets.push(newDataset);
                    graphConfig.data.datasets.push(newDataset2);
                    graphConfig.data.datasets.push(newDataset3);
                    graphConfig.data.datasets.push(newDataset4);
                } else if (
                    graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length > 0 &&
                    graphDatas.listTrend5.length > 0 &&
                    graphDatas.listTrend6.length == 0
                ) {
                    var newDataset = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend1,
                        window.chartColors.blue,
                        graphConfig.data.datasets[0]['yAxisID']
                    );
                    var newDataset2 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend2,
                        window.chartColors.green,
                        graphConfig.data.datasets[1]['yAxisID']
                    );
                    var newDataset3 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend3,
                        "#A0522D",
                        graphConfig.data.datasets[2]['yAxisID']
                    );
                    var newDataset4 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend4,
                        "#80ced6",
                        graphConfig.data.datasets[3]['yAxisID']
                    );
                    var newDataset5 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend5,
                        window.chartColors.purple,
                        graphConfig.data.datasets[4]['yAxisID']
                    );
                    graphConfig.data.datasets.push(newDataset);
                    graphConfig.data.datasets.push(newDataset2);
                    graphConfig.data.datasets.push(newDataset3);
                    graphConfig.data.datasets.push(newDataset4);
                    graphConfig.data.datasets.push(newDataset5);
                } else if (
                    graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length > 0 &&
                    graphDatas.listTrend5.length > 0 &&
                    graphDatas.listTrend6.length > 0
                ) {
                    var newDataset = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend1,
                        window.chartColors.blue,
                        graphConfig.data.datasets[0]['yAxisID']
                    );
                    var newDataset2 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend2,
                        window.chartColors.green,
                        graphConfig.data.datasets[1]['yAxisID']
                    );
                    var newDataset3 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend3,
                        "#A0522D",
                        graphConfig.data.datasets[2]['yAxisID']
                    );
                    var newDataset4 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend4,
                        "#80ced6",
                        graphConfig.data.datasets[3]['yAxisID']
                    );
                    var newDataset5 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend5,
                        window.chartColors.purple,
                        graphConfig.data.datasets[4]['yAxisID']
                    );
                    var newDataset6 = setDataDescription(
                        graphConfig.data.labels,
                        graphDatas.listTrend6,
                        '#2F4F4F',
                        graphConfig.data.datasets[5]['yAxisID']
                    );
                    graphConfig.data.datasets.push(newDataset);
                    graphConfig.data.datasets.push(newDataset2);
                    graphConfig.data.datasets.push(newDataset3);
                    graphConfig.data.datasets.push(newDataset4);
                    graphConfig.data.datasets.push(newDataset5);
                    graphConfig.data.datasets.push(newDataset6);
                }

                /************************ draw control 1 *****************/

                //draw control

                /************************ draw control 1 *****************/

                if (controlConf == true) {
                    if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length == 0 &&
                        graphDatas.listTrend3.length == 0 &&
                        graphDatas.listTrend4.length == 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0
                    ) {
                        //console.log(graphConfig)
                        graphDatas.listControlConf1.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary1
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length == 0 &&
                        graphDatas.listTrend4.length == 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName
                    ) {
                        graphDatas.listControlConf1.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary1
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf2.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary2
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length == 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName
                    ) {
                        graphDatas.listControlConf1.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary1
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf2.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary2
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf3.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary3
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length > 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary4.ItemsName
                    ) {
                        graphDatas.listControlConf1.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary1
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf2.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary2
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf3.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary3
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf4.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary4
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length > 0 &&
                        graphDatas.listTrend5.length > 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary4.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary5.ItemsName
                    ) {
                        graphDatas.listControlConf1.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary1
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf2.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary2
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf3.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary3
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf4.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary4
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf5.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary5
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length > 0 &&
                        graphDatas.listTrend5.length > 0 &&
                        graphDatas.listTrend6.length > 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary4.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary5.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary6.ItemsName
                    ) {
                        graphDatas.listControlConf1.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary1
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf2.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary2
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf3.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary3
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf4.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary4
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf5.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary5
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                        graphDatas.listControlConf6.forEach(function (Arr, index) {
                            var nDataset = setDataControlConfGraph(
                                Arr,
                                window.chartColors.blue, graphDatas.listSummary6
                            );
                            if (nDataset.newDataset != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset);
                            }
                            if (nDataset.newDataset2 != null) {
                                graphConfig.data.datasets.push(nDataset.newDataset2);
                            }
                        });
                    }
                } else {
                    //console.log("sadasdasdasd")
                    if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length == 0 &&
                        graphDatas.listTrend3.length == 0 &&
                        graphDatas.listTrend4.length == 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0
                    ) {
                        if (null != graphDatas.control1 && graphDatas.control1.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset = {
                                label: "Control Max ", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset.data.push(graphDatas.control1.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset);
                        }

                        if (null != graphDatas.control1 && graphDatas.control1.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset2 = {
                                label: "Control Min ",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset2.data.push(graphDatas.control1.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset2);
                        }
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length == 0 &&
                        graphDatas.listTrend4.length == 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName
                    ) {
                        if (null != graphDatas.control1 && graphDatas.control1.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset = {
                                label: "Control Max ", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset.data.push(graphDatas.control1.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset);
                        }

                        if (null != graphDatas.control1 && graphDatas.control1.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset2 = {
                                label: "Control Min ",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset2.data.push(graphDatas.control1.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset2);
                        }
                        if (null != graphDatas.control2 && graphDatas.control2.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset3 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset3.data.push(graphDatas.control2.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset3);
                        }

                        if (null != graphDatas.control && graphDatas.control.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset4 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset4.data.push(graphDatas.control2.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset4);
                        }
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length == 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName
                    ) {
                        if (null != graphDatas.control1 && graphDatas.control1.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset = {
                                label: "Control Max ", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset.data.push(graphDatas.control1.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset);
                        }

                        if (null != graphDatas.control1 && graphDatas.control1.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset2 = {
                                label: "Control Min ",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset2.data.push(graphDatas.control1.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset2);
                        }
                        if (null != graphDatas.control2 && graphDatas.control2.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset3 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset3.data.push(graphDatas.control2.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset3);
                        }

                        if (null != graphDatas.control2 && graphDatas.control2.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset4 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset4.data.push(graphDatas.control2.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset4);
                        }
                        if (null != graphDatas.control3 && graphDatas.control3.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset5 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset5.data.push(graphDatas.control3.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset5);
                        }

                        if (null != graphDatas.control3 && graphDatas.control3.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset6 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset6.data.push(graphDatas.control3.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset6);
                        }
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length > 0 &&
                        graphDatas.listTrend5.length == 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary4.ItemsName
                    ) {
                        if (null != graphDatas.control1 && graphDatas.control1.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset = {
                                label: "Control Max ", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset.data.push(graphDatas.control1.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset);
                        }

                        if (null != graphDatas.control1 && graphDatas.control1.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset2 = {
                                label: "Control Min ",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset2.data.push(graphDatas.control1.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset2);
                        }
                        if (null != graphDatas.control2 && graphDatas.control2.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset3 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset3.data.push(graphDatas.control2.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset3);
                        }

                        if (null != graphDatas.control2 && graphDatas.control2.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset4 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset4.data.push(graphDatas.control2.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset4);
                        }
                        if (null != graphDatas.control3 && graphDatas.control3.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset5 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset5.data.push(graphDatas.control3.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset5);
                        }

                        if (null != graphDatas.control3 && graphDatas.control3.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset6 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset6.data.push(graphDatas.control3.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset6);
                        }
                        if (null != graphDatas.control4 && graphDatas.control4.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset7 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset7.data.push(graphDatas.control4.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset7);
                        }

                        if (null != graphDatas.control4 && graphDatas.control4.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset8 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset8.data.push(graphDatas.control4.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset8);
                        }
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length > 0 &&
                        graphDatas.listTrend5.length > 0 &&
                        graphDatas.listTrend6.length == 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary4.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary5.ItemsName
                    ) {
                        if (null != graphDatas.control1 && graphDatas.control1.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset = {
                                label: "Control Max ", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset.data.push(graphDatas.control1.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset);
                        }

                        if (null != graphDatas.control1 && graphDatas.control1.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset2 = {
                                label: "Control Min ",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset2.data.push(graphDatas.control1.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset2);
                        }
                        if (null != graphDatas.control2 && graphDatas.control2.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset3 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset3.data.push(graphDatas.control2.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset3);
                        }

                        if (null != graphDatas.control2 && graphDatas.control2.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset4 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset4.data.push(graphDatas.control2.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset4);
                        }
                        if (null != graphDatas.control3 && graphDatas.control3.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset5 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset5.data.push(graphDatas.control3.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset5);
                        }

                        if (null != graphDatas.control3 && graphDatas.control3.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset6 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset6.data.push(graphDatas.control3.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset6);
                        }
                        if (null != graphDatas.control4 && graphDatas.control4.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset7 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset7.data.push(graphDatas.control4.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset7);
                        }

                        if (null != graphDatas.control4 && graphDatas.control4.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset8 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset8.data.push(graphDatas.control4.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset8);
                        }
                        if (null != graphDatas.control5 && graphDatas.control5.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset9 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset9.data.push(graphDatas.control5.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset9);
                        }

                        if (null != graphDatas.control5 && graphDatas.control5.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset10 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset10.data.push(graphDatas.control5.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset10);
                        }
                    } else if (
                        graphDatas.listTrend1.length > 0 &&
                        graphDatas.listTrend2.length > 0 &&
                        graphDatas.listTrend3.length > 0 &&
                        graphDatas.listTrend4.length > 0 &&
                        graphDatas.listTrend5.length > 0 &&
                        graphDatas.listTrend6.length > 0 &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary2.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary3.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary4.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary5.ItemsName &&
                        graphDatas.listSummary1.ItemsName ==
                        graphDatas.listSummary6.ItemsName
                    ) {
                        if (null != graphDatas.control1 && graphDatas.control1.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset = {
                                label: "Control Max ", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset.data.push(graphDatas.control1.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset);
                        }

                        if (null != graphDatas.control1 && graphDatas.control1.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset2 = {
                                label: "Control Min ",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset2.data.push(graphDatas.control1.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset2);
                        }
                        if (null != graphDatas.control2 && graphDatas.control2.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset3 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset3.data.push(graphDatas.control2.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset3);
                        }

                        if (null != graphDatas.control2 && graphDatas.control2.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset4 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset4.data.push(graphDatas.control2.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset4);
                        }
                        if (null != graphDatas.control3 && graphDatas.control3.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset5 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset5.data.push(graphDatas.control3.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset5);
                        }

                        if (null != graphDatas.control3 && graphDatas.control3.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset6 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset6.data.push(graphDatas.control3.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset6);
                        }
                        if (null != graphDatas.control4 && graphDatas.control4.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset7 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset7.data.push(graphDatas.control4.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset7);
                        }

                        if (null != graphDatas.control4 && graphDatas.control4.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset8 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset8.data.push(graphDatas.control4.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset8);
                        }
                        if (null != graphDatas.control5 && graphDatas.control5.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset9 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset9.data.push(graphDatas.control5.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset9);
                        }

                        if (null != graphDatas.control5 && graphDatas.control5.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset10 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset10.data.push(graphDatas.control5.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset10);
                        }
                        if (null != graphDatas.control6 && graphDatas.control6.bMaxValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset11 = {
                                label: "Control 23456", //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset11.data.push(graphDatas.control6.MaxValue);
                            }
                            graphConfig.data.datasets.push(newDataset11);
                        }

                        if (null != graphDatas.control6 && graphDatas.control6.bMinValue) {
                            var colorName = "orange"; // colorNames[config.data.datasets.length % colorNames.length];
                            var newColor = window.chartColors[colorName];
                            var newDataset12 = {
                                label: "Control 23456",
                                backgroundColor: newColor,
                                borderColor: newColor,
                                pointHoverRadius: 0,
                                data: [],
                                radius: 0,
                                point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                                fill: false,
                                datalabels: {
                                    display: false,
                                },
                            };
                            for (
                                var index = 0;
                                index < graphConfig.data.labels.length;
                                ++index
                            ) {
                                newDataset12.data.push(graphDatas.control6.MinValue);
                            }

                            graphConfig.data.datasets.push(newDataset12);
                        }
                    }
                }
                //if (graphDatas.listTrend2.length == 0) {
                //    /************************ draw spec 1 *****************/
                //    //draw control
                //    if(graphDatas.spec1.bMaxValue)
                //    {
                //        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                //        var newColor = window.chartColors[colorName];
                //        var newSpecDataset = {
                //            label: 'Spec Max ', //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                //            backgroundColor: newColor,
                //            borderColor: newColor,
                //            pointHoverRadius: 0,
                //            data: [],
                //            radius: 0,
                //            point: { radius: 0,  hitRadius: 1, hoverRadius: 1 },
                //            fill: false
                //        };
                //        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                //            newSpecDataset.data.push(graphDatas.spec1.MaxValue);
                //        }
                //        graphConfig.data.datasets.push(newSpecDataset);
                //    }
                //    if(graphDatas.spec1.bMinValue)
                //    {
                //        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                //        var newColor = window.chartColors[colorName];
                //        var newSpecDataset2 = {
                //            label: 'Spec Min ',
                //            backgroundColor: newColor,
                //            borderColor: newColor,
                //            pointHoverRadius: 0,
                //            data: [],
                //            radius: 0,
                //            point: { radius: 0,  hitRadius: 1, hoverRadius: 1 },
                //            fill: false
                //        };
                //        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                //            newSpecDataset2.data.push(graphDatas.spec1.MinValue);
                //        }
                //        graphConfig.data.datasets.push(newSpecDataset2);
                //    }
                //    /************************ draw spec 1 *****************/
                //}
                if (graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length == 0 &&
                    graphDatas.listTrend3.length == 0 &&
                    graphDatas.listTrend4.length == 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0) {
                    //draw control
                    if (graphDatas.spec1.bMaxValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset = {
                            label: 'Spec Max ', //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset.data.push(graphDatas.spec1.MaxValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset);
                    }
                    if (graphDatas.spec1.bMinValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset2 = {
                            label: 'Spec Min ',
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset2.data.push(graphDatas.spec1.MinValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset2);
                    }
                } else if (graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length == 0 &&
                    graphDatas.listTrend4.length == 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0 &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary2.ItemsName) {
                    //draw control
                    if (graphDatas.spec2.bMaxValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset = {
                            label: 'Spec Max ', //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset.data.push(graphDatas.spec2.MaxValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset);
                    }
                    if (graphDatas.spec2.bMinValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset2 = {
                            label: 'Spec Min ',
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset2.data.push(graphDatas.spec2.MinValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset2);
                    }
                }
                else if (graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length == 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0 &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary2.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary3.ItemsName) {
                    //draw control
                    if (graphDatas.spec3.bMaxValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset = {
                            label: 'Spec Max ', //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset.data.push(graphDatas.spec3.MaxValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset);
                    }
                    if (graphDatas.spec3.bMinValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset2 = {
                            label: 'Spec Min ',
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset2.data.push(graphDatas.spec3.MinValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset2);
                    }
                }
                else if (graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length > 0 &&
                    graphDatas.listTrend5.length == 0 &&
                    graphDatas.listTrend6.length == 0 &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary2.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary3.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary4.ItemsName) {
                    //draw control
                    if (graphDatas.spec4.bMaxValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset = {
                            label: 'Spec Max ', //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset.data.push(graphDatas.spec4.MaxValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset);
                    }
                    if (graphDatas.spec4.bMinValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset2 = {
                            label: 'Spec Min ',
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset2.data.push(graphDatas.spec4.MinValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset2);
                    }
                }
                else if (graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length > 0 &&
                    graphDatas.listTrend5.length > 0 &&
                    graphDatas.listTrend6.length == 0 &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary2.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary3.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary4.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary5.ItemsNamee) {
                    //draw control
                    if (graphDatas.spec5.bMaxValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset = {
                            label: 'Spec Max ', //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset.data.push(graphDatas.spec5.MaxValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset);
                    }
                    if (graphDatas.spec5.bMinValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset2 = {
                            label: 'Spec Min ',
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset2.data.push(graphDatas.spec5.MinValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset2);
                    }
                }
                else if (graphDatas.listTrend1.length > 0 &&
                    graphDatas.listTrend2.length > 0 &&
                    graphDatas.listTrend3.length > 0 &&
                    graphDatas.listTrend4.length > 0 &&
                    graphDatas.listTrend5.length > 0 &&
                    graphDatas.listTrend6.length > 0 &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary2.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary3.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary4.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary5.ItemsName &&
                    graphDatas.listSummary1.ItemsName ==
                    graphDatas.listSummary6.ItemsName) {
                    //draw control
                    if (graphDatas.spec6.bMaxValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset = {
                            label: 'Spec Max ', //ตอนหลังมีการเพิ่ม ค่า spec ใน trend COA
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset.data.push(graphDatas.spec6.MaxValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset);
                    }
                    if (graphDatas.spec6.bMinValue) {
                        var colorName = 'red';// colorNames[config.data.datasets.length % colorNames.length];
                        var newColor = window.chartColors[colorName];
                        var newSpecDataset2 = {
                            label: 'Spec Min ',
                            backgroundColor: newColor,
                            borderColor: newColor,
                            pointHoverRadius: 0,
                            data: [],
                            radius: 0,
                            point: { radius: 0, hitRadius: 1, hoverRadius: 1 },
                            fill: false
                        };
                        for (var index = 0; index < graphConfig.data.labels.length; ++index) {
                            newSpecDataset2.data.push(graphDatas.spec6.MinValue);
                        }
                        graphConfig.data.datasets.push(newSpecDataset2);
                    }
                }
            } catch (e) {
                console.log(e);
            }
            return graphConfig;
        };
        return objFact;
    }
);
function htmlEntities(str) {
    return String(str)
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;");
}
