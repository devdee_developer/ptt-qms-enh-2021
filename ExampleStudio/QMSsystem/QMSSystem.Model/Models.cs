﻿using System;
using System.Collections.Generic; 
using System.Text;

namespace QMSSystem.Model
{
    class Models
    {
    }


    public class PrivilegeMode
    { 
        //DASHBOARD 
        public bool D_Home {get;set;}

        //DASHBOARD 
        public bool D_Dashboard { get; set; }

        //Off Control & spec
        public bool  D_OffControl  {get;set;}
        public bool D_OffSpec {get;set;}

        //Trend Data
        public bool D_TrendHighlight {get;set;}
        public bool D_TrendGAS {get;set;}
        public bool D_TrendUtility {get;set;}
        public bool D_TrendProduct {get;set;}

        public bool D_TrendEmission { get; set; }
        public bool D_ProductCSC { get; set; }
        public bool D_TrendWasteWater { get; set; }
        public bool D_TrendHotOilFlashPoint { get; set; }
        public bool D_TrendHotOilPhysical { get; set; }
        public bool D_TrendSulfurInGasWeekly { get; set; }
        public bool D_TrendSulfurInGasMonthly { get; set; }
        public bool D_TrendHgInPLMonthly { get; set; }
        public bool D_TrendHgInPL { get; set; }
        public bool D_TrendHgOutletMRU { get; set; }
        public bool D_TrendTwoHundredThousandPond { get; set; }
        public bool D_TrendHgStab { get; set; }
        public bool D_TrendAcidOffGas { get; set; }
        public bool D_TrenddObservePondsAndOilyWaters { get; set; }
        public bool D_TrenddObservePonds { get; set; }
        public bool D_TrenddOilyWaters { get; set; }
        public bool D_TrendLabControlChart { get; set; }

        //Master Data
        public bool M_PlantAndProduct {get;set;}
        public bool M_ReduceFeed {get;set;}
        public bool M_DownTime {get;set;}
        public bool M_CorrectData{get;set;}
        public bool  M_RootCause {get;set;}
        public bool M_KPI {get;set;}
        public bool M_IntervalSampling { get;set; }
        public bool M_Shift {get;set;}
        public bool M_ControlGas {get;set;}
        public bool M_SpecGas {get;set;}
        public bool M_ControlUtility {get;set;}
        public bool M_ControlProdcut {get;set;}
        public bool M_ControlClarifiedSystem { get; set; }
        public bool M_ControlWatseWater { get; set; }
        public bool M_ControlObservePonds { get; set; }
        public bool M_ControlOilyWaters { get; set; }
        public bool M_ControlHotOilFlashPoint { get; set; }
        public bool M_ControlHotOilPhysical { get; set; }
        public bool M_ControlSulfurInGasWeekly { get; set; }
        public bool M_ControlSulfurInGasMonthly { get; set; }
        public bool M_ControlHgInPLMonthly { get; set; }
        public bool M_ControlHgInPL { get; set; }
        public bool M_ControlHgOutletMRU { get; set; }
        public bool M_ControlTwoHundredThousandPond { get; set; }
        public bool M_ControlHgStab { get; set; }
        public bool M_ControlAcidOffGas { get; set; }
        public bool M_TemplateGas {get;set;}
        public bool M_TemplateUtility {get;set;}
        public bool M_Email {get;set;}
        public bool M_Unit {get;set;}
        public bool M_DataLeak { get; set; }//เพิ่มตอน pop ต่อ DataLeak
        public bool M_ControlEmission { get; set; }
        public bool M_TemplateEmission { get; set; }
        public bool M_TemplateClarifiedSystem { get; set; }
        public bool M_TemplateHotOilFlashPoint { get; set; }
        public bool M_TemplateHotOilPhysical { get; set; }
        public bool M_TemplateSulfurInGasWeekly { get; set; }
        public bool M_TemplateSulfurInGasMonthly { get; set; }
        public bool M_TemplateHgInPLMonthly { get; set; }
        public bool M_TemplateHgInPL { get; set; }
        public bool M_TemplateHgOutletMRU { get; set; }
        public bool M_TemplateWatseWater { get; set; }
        public bool M_TemplateObservePonds { get; set; }
        public bool M_TemplateOilyWaters { get; set; }
        public bool M_TemplateTwoHundredThousandPond { get; set; }
        public bool M_TemplateHgStab { get; set; }
        public bool M_TemplateAcidOffGas { get; set; }
        public bool M_ProductMerge { get; set; }
        public bool M_CustomerMerge { get; set; }
        public bool M_SigmaLevel { get; set; }

        //Mansge Data
        public bool N_RawDataExport {get;set;}
        public bool N_OffControlCal {get;set;}
        public bool N_OffSpecCal {get;set;}
        public bool N_TurnAround {get;set;}
        public bool N_DownTime {get;set;}
        public bool  N_ChangeGrade {get;set;}
        public bool N_CrossVolume {get;set;}
        public bool N_Abnormal {get;set;}
        public bool N_ReduceFeed {get;set;}
        public bool N_Exception {get;set;}
        public bool N_ImpExpTemplate { get; set; }
        public bool N_TemplateGas {get;set;}
        public bool N_TemplateUtility {get;set;}
        
        public bool N_TemplateEmission { get; set; }
        public bool N_TemplateCOA {get;set;}
        public bool N_TemplateClarifiedSystem { get; set; }
        public bool N_TemplateWasteWater { get; set; }
        public bool N_TemplateObservePonds { get; set; }
        public bool N_TemplateOilyWaters { get; set; }
        public bool N_TemplateHotOilFlashPoint { get; set; }
        public bool N_TemplateHotOilPhysical { get; set; }
        public bool N_TemplateSulfurInGasWeekly { get; set; }
        public bool N_TemplateSulfurInGasMonthly { get; set; }
        public bool N_TemplateHgInPLMonthly { get; set; }
        public bool N_TemplateHgInPL { get; set; }
        public bool N_TemplateHgOutletMRU { get; set; }
        public bool N_TemplateTwoHundredThousandPond { get; set; }
        public bool N_TemplateHgStab { get; set; }
        public bool N_TemplateAcidOffGas { get; set; }
        public bool N_LabControlChart { get; set; }
        public bool N_TemplateCSCProduct { get; set; }
        public bool N_TemplateCOAToDataLake { get; set; }
        public bool N_DataLake { get; set; }
        //public bool N_LoadDataLake { get; set; }
        //public bool N_LoadEventRawDataToDB { get; set; }
        //public bool N_LoadQmsRpOffControlToDataLake { get; set; }
        //public bool N_LoadQmsRpOffControlSummaryToDataLake { get; set; }
        //public bool N_LoadQmsRpOffControlStampToDataLake { get; set; }
        //public bool N_LoadAbnormalToDB { get; set; }
        //public bool N_LoadEventCalToDB { get; set; }
        //public bool N_CheckTotalEXQProduct { get; set; }

        //Report
        public bool R_OffControlSum {get;set;}
        public bool R_OffSpecSum {get;set;}
        public bool R_CorrectData {get;set;}
        public bool R_Shift {get;set;}
        public bool R_TrendGas {get;set;}
        public bool  R_TrendUtitliy {get;set;}
        public bool R_TrendProduct {get;set;}


        //Setting 
        public bool S_Home {get;set;}
        public bool S_Abnormal {get;set;}
        public bool S_ExportData {get;set;}
        public bool S_ReportOff {get;set;}
        public bool S_ExtraSchedule {get;set;}
        public bool S_TrendHightlight {get;set;}
        public bool S_Connect {get;set;}
        public bool S_Authen { get; set; }
        public bool S_Log { get; set; }
        public bool S_ProductCSCPath { get; set; }
        public bool S_SortOption { get; set; }

    }

    public class AuthenAccess
    {
        public string name { get; set; }
        public int code { get; set; }
        public bool access { get; set; }
    }

    public class Search
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
    }

    public class SpecControlValue
    {
        public bool bMaxValue { get; set; }
        public decimal MaxValue { get; set; }
        public bool bMinValue { get; set; }
        public decimal MinValue { get; set; }
        public string SpecView { get; set; }
        public DateTime Trend_DATE { get; set; }
    }

    public class CreateRPOffControl
    {
        public long ID {get;set;}
        public string NAME {get;set;}
        public byte OFF_CONTROL_TYPE {get;set;}
        public DateTime START_DATE {get;set;}
        public DateTime END_DATE {get;set;}

        public string TITLE_DESC {get;set;}
        public string DETAIL_DESC {get;set;}

    }

    public class ViewRPOffControl
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public string TITLE_DESC { get; set; }
        public string DETAIL_DESC { get; set; }

    }



    #region InternalQualityControl

   
    public class IQCRuleSearchModel
    {
        public string RULE_ID { get; set; }
        public string RULE_NAME { get; set; }
        public string RULE_DESC { get; set; }
    }

    public class IQCRuleSearch : Search
    {
        public IQCRuleSearchModel mSearch { get; set; }
    }

    public class IQCMethodSearchModel
    {
        public long METHOD_ID { get; set; }
        public string METHOD_NAME { get; set; }
        public string METHOD_DESC { get; set; }
    }

    public class IQCMethodSearch : Search
    {
        public IQCMethodSearchModel mSearch { get; set; }
    }


    public class IQCPossibleCauseSearchModel
    {
        public long POSSICAUSE_ID { get; set; }
        public string POSSICAUSE_NAME { get; set; }
        public string POSSICAUSE_DESC { get; set; }
    }

    public class IQCPossibleCauseSearch : Search
    {
        public IQCPossibleCauseSearchModel mSearch { get; set; }
    }

    public class SetIQCPossibleCausetPosition
    {
        public long POSSICAUSE_ID { get; set; }
        public int DIRECTION { get; set; }
    }


    public class IQCFormulaSearchModel
    {
        public long METHOD_ID { get; set; }
        public string FORMULA_REV { get; set; }
        public long FORMULA_PROD { get; set; }
        public decimal? FORMULA_SAMPLE { get; set; }
        public decimal? FORMULA_STD { get; set; }
        public decimal? FORMULA_UP { get; set; }
        public decimal? FORMULA_UCL { get; set; }
        public string UCL_PATTERN { get; set; }
        public decimal? FORMULA_UWL { get; set; }
        public string UWL_PATTERN { get; set; }
        public decimal? FORMULA_AVG { get; set; }
        public string AVG_PATTERN { get; set; }
        public decimal? FORMULA_LWL { get; set; }
        public string LWL_PATTERN { get; set; }
        public decimal? FORMULA_LCL { get; set; }
        public string LCL_PATTERN { get; set; }
        public decimal? FORMULA_LOW { get; set; }
        public string FORMULA_DESC { get; set; }
    }

    public class IQCFormulaSearch : Search
    {
        public IQCFormulaSearchModel mSearch { get; set; }
    }


    public class IQCMailAlertSearchModel
    {
        public long EMAIL_ID { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string ALERT_DESC { get; set; }
    }

    public class IQCMailAlertSearch : Search
    {
        public IQCMailAlertSearchModel mSearch { get; set; }
    }

    public class IQCItemSearchModel
    {
        public long ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public string ITEM_DESC { get; set; }
    }

    public class IQCItemSearch : Search
    {
        public IQCItemSearchModel mSearch { get; set; }
    }

    public class IQCEquipSearchModel
    {
        public long EQUIP_ID { get; set; }
        public string EQUIP_NAME { get; set; }
        public string EQUIP_DESC { get; set; }
    }

    public class IQCEquipSearch : Search
    {
        public IQCEquipSearchModel mSearch { get; set; }
    }

    public class IQCItemEquipSearchModel
    {
        public long ITEM_ID { get; set; }
        public long EQUIP_ID { get; set; }
    }

    public class IQCItemEquipSearch : Search
    {
        public IQCItemEquipSearchModel mSearch { get; set; }
    }


    public class IQCTemplateSearchModel
    {
        public byte TEMPLATE_TYPE { get; set; }
        public string FORMULA_REV { get; set; }
        public string NAME { get; set; }
        public string FILE_NAME { get; set; }
    }

    public class IQCTemplateSearch : Search
    {
        public IQCTemplateSearchModel mSearch { get; set; }
    }

    public class IQCControlDataSearchModel
    {
        public byte TEMPLATE_ID { get; set; }
        public long ITEM_ID { get; set; }
        public long METHOD_ID { get; set; }
        public DateTime? STARTEDGE { get; set; }
        public DateTime? ENDEDGE { get; set; }
        public DateTime? KEEPTIME { get; set; }
        public Decimal? KEEPVALUE { get; set; }
    }

    public class IQCControlDataSearch : Search
    {
        public IQCControlDataSearchModel mSearch { get; set; }
    }

    public class IQCControlHistorySearchModel 
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public DateTime? STARTEDGE { get; set; }
        public DateTime? ENDEDGE { get; set; } 
        public string TITLE { get; set; }
        public DateTime? SENDEMAIL { get; set; } 
    }
    public class IQCControlHistorySearch : Search
    {
        public IQCControlHistorySearchModel mSearch { get; set; }
    }

    public class IQCControlRuleSearchModel 
    {
        public long ID { get; set; }
        public long HISTORY_ID { get; set; }
	    public long RULE_ID { get; set; }
        public DateTime? POINT_START { get; set; }
        public DateTime POINT_END { get; set; }
    }
    public class IQCControlRuleSearch : Search
    {
        public IQCControlRuleSearchModel mSearch { get; set; }

    }

    public class IQCControlReasonSearchModel
    {
        public long ID { get; set; }
        public long CONTROLRULE_ID { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string REASON { get; set; }
    }
    public class IQCControlReasonSearch : Search
    {
        public IQCControlReasonSearchModel mSearch { get; set; }

    }

    public class DraftControlRule
    {
        public long ID { get; set; }
        public string RULE_ID { get; set; }
        public string POINT_START { get; set; }
        public string POINT_END { get; set; }
    }
    #endregion


    #region Master

    public class SessionModel
    {
        public bool check_login { get; set; }
        public string user_name { get; set; } 
        public int user_level { get; set; }
    }

    public class CreateAbnormalSetting
    {
        public short ABNORMAL_MAX_REPEAT { get; set; }
        public short ABNORMAL_OVERSHOOT { get; set; }
        public string ABNORMAL_ALPHABET1 { get; set; }
        public string ABNORMAL_ALPHABET2 { get; set; }
    }

    public class CreatePRODUCT_CSC_PATH
    {
        public string PRODUCT_CSC_PATH { get; set; }
        public string PRODUCT_CSC_NAME { get; set; }
    }


    public class CreateExportDataBoundary
    { 
        public short UPPER_BOUNDARY { get; set; }
        public short LOWER_BOUNDARY { get; set; }
         
    }

    public class ExportPrefixSearchModel
    { 
        public long PLANT_ID { get; set; }
    }

    public class ExportPrefixSearch : Search
    {
        public ExportPrefixSearchModel mSearch { get; set; }
    }

    public class CorrectDataSearchModel
    {
        public string NAME { get; set; }
        public long? DELETE_OUTPUT_QTY { get; set; } //ตัวตั้ง
        public long? DELETE_INPUT_QTY { get; set; } //ตัวหาร
        public long? CAL_OFF_CONTROL { get; set; }
    }

    public class CorrectDataSearch : Search
    {
        public CorrectDataSearchModel mSearch { get; set; }
    }

    public class TemplateSearchModel
    {
        public byte TEMPLATE_TYPE { get; set; }
        //public long? PLANT_ID { get; set; }
        public string TEMPLATE_AREA { get; set; }
        public string NAME { get; set; }
        public string FILE_NAME { get; set; }
    }

    public class TemplateSearch : Search
    {
        public TemplateSearchModel mSearch { get; set; }
    }

    public class TrendManageSearchModel
    { 
        public long TEMPLATE_ID { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    } 

    public class TrendManageSearch : Search
    {
        public TrendManageSearchModel mSearch { get; set; }
    }
     
    public class TrendGasGraphSearchModel
    {
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        //public long TEMPLATE_ID1 { get; set; }
        //public long PLANT_ID1 { get; set; }
        public string TEMPLATE_AREA1 { get; set; }
        public string  SAMPLE_ID1 { get; set; } //ใช้ string เพราะ id ไม่ unique
        public string ITEM_ID1 { get; set; } //ใช้ string เพราะ id ไม่ unique
        public long UNIT_ID1 { get; set; }
        public string UNIT_NAME1 { get; set; }
        public byte TEMPLATE_TYPE1 { get; set; }
        public bool TEMPLATE_ALL_FLAG1 { get; set; }
        //public long TEMPLATE_ID2 { get; set; }
        //public long PLANT_ID2 { get; set; }
        public string TEMPLATE_AREA2 { get; set; }
        public string SAMPLE_ID2 { get; set; }
        public string ITEM_ID2 { get; set; }
        public long UNIT_ID2 { get; set; }
        public string UNIT_NAME2 { get; set; }
        public byte TEMPLATE_TYPE2 { get; set; }
        public bool TEMPLATE_ALL_FLAG2 { get; set; }

        public string TEMPLATE_AREA3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string SAMPLE_ID3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string ITEM_ID3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public long UNIT_ID3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string UNIT_NAME3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public byte TEMPLATE_TYPE3 { get; set; }
        public bool TEMPLATE_ALL_FLAG3 { get; set; }

        public string TEMPLATE_AREA4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string SAMPLE_ID4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string ITEM_ID4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public long UNIT_ID4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string UNIT_NAME4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public byte TEMPLATE_TYPE4 { get; set; }
        public bool TEMPLATE_ALL_FLAG4 { get; set; }

        public string TEMPLATE_AREA5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string SAMPLE_ID5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string ITEM_ID5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public long UNIT_ID5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string UNIT_NAME5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public byte TEMPLATE_TYPE5 { get; set; }
        public bool TEMPLATE_ALL_FLAG5 { get; set; }

        public string TEMPLATE_AREA6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string SAMPLE_ID6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string ITEM_ID6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public long UNIT_ID6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public string UNIT_NAME6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public byte TEMPLATE_TYPE6 { get; set; }
        public bool TEMPLATE_ALL_FLAG6 { get; set; }
        public byte TEMPLATE_TYPE { get; set; }
    }

    public class TrendCOAGraphSearchModel
    {
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        //public string COA_IDs1 { get; set; }
        //public long COA_ID1 { get; set; }

        public string CUSTOMER1 { get; set; }
        public string PRODUCT1 { get; set; }
        public string ITEM1 { get; set; }
        public string UNIT_NAME1 { get; set; }
        public string TEST_METHOD1 { get; set; }
        public string SPECIFICATION1 { get; set; }
        public string Tank1 { get; set; } 
        public long? PRODUCT_MERGE_ID1 { get; set; }
        public string PRODUCT_MERGE_NAME1 { get; set; }
        //public string COA_IDs2 { get; set; }
        //public long COA_ID2 { get; set; }

        public string CUSTOMER2 { get; set; }
        public string PRODUCT2 { get; set; }
        public string ITEM2 { get; set; }
        public string UNIT_NAME2 { get; set; }
        public string TEST_METHOD2 { get; set; }
        public string SPECIFICATION2 { get; set; }
        public string Tank2 { get; set; }
        public long? PRODUCT_MERGE_ID2 { get; set; }
        public string PRODUCT_MERGE_NAME2 { get; set; }

        public string CUSTOMER3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string PRODUCT3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string ITEM3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string UNIT_NAME3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string TEST_METHOD3 { get; set; }
        public string SPECIFICATION3 { get; set; }
        public string Tank3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis 
        public long? PRODUCT_MERGE_ID3 { get; set; }
        public string PRODUCT_MERGE_NAME3 { get; set; }

        public string CUSTOMER4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string PRODUCT4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string ITEM4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string UNIT_NAME4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string TEST_METHOD4 { get; set; }
        public string SPECIFICATION4 { get; set; }
        public string Tank4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public long? PRODUCT_MERGE_ID4 { get; set; }
        public string PRODUCT_MERGE_NAME4 { get; set; }

        public string CUSTOMER5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string PRODUCT5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string ITEM5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string UNIT_NAME5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string TEST_METHOD5 { get; set; }
        public string SPECIFICATION5 { get; set; }
        public string Tank5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public long? PRODUCT_MERGE_ID5 { get; set; }
        public string PRODUCT_MERGE_NAME5 { get; set; }

        public string CUSTOMER6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string PRODUCT6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string ITEM6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string UNIT_NAME6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public string TEST_METHOD6 { get; set; }
        public string SPECIFICATION6 { get; set; }
        public string Tank6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple y axis
        public long? PRODUCT_MERGE_ID6 { get; set; }
        public string PRODUCT_MERGE_NAME6 { get; set; }
    }


    public class ManageLIMSSearchModel
    {
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public bool bLoadCOA { get; set; }
        public bool bLoadCSC { get; set; }
        public bool bDeleteOldFile { get; set; }
    }

    public class ProductQualityToCSCSearchModel
    {
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public List<PRODUCT_COA> LIST_PRODUCT { get; set; }
    }

    public class ProductExaDataSearchModel
    {
        public long ID { get; set; }
        public byte CROSS_VOLUME_FLAG { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }

        public byte EXA_DATA_TYPE { get; set; }
    }
      
    public class TrendGraphSum
    {
        public byte TEMPLATE_TYPE { get; set; }
        public string AreaName { get; set; }
        public string SampleName { get; set; }
        public string ItemsName { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
        public decimal AvgValue { get; set; }
        public decimal SDValue { get; set; }
        public string UnitName { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public string TankName { get; set; }

        public decimal SCALEMAX { get; set; }
        public decimal SCALEMIN { get; set; }
        public decimal STEPSIZE { get; set; }
    }
    
    public class ControlValueSearchModel
    {
        public byte GROUP_TYPE { get; set; }
        public string NAME { get; set; }
        public string CONTROL_DESC { get; set; }
    }

    public class ControlValueSearch : Search
    {
        public ControlValueSearchModel mSearch { get; set; }
    }

    public class ControlValueRowSearchModel
    {
        public long CONTROL_ID { get; set; }
    }

    public class ControlValueRowSearch : Search
    {
        public ControlValueRowSearchModel mSearch { get; set; }
    } 

    public class ControlValueColumnSearchModel
    {
        public long CONTROL_ID { get; set; }
    }

    public class ControlValueColumnSearch : Search
    {
        public ControlValueColumnSearchModel mSearch { get; set; }
    }

    public class ControlValueDataSearchModel
    {
        public byte CONTROL_ROW_ID { get; set; }
        public byte CONTROL_COLUMN_ID { get; set; }
    }

    public class ControlValueDataSearch : Search
    {
        public ControlValueDataSearchModel mSearch { get; set; }
    }

    public class TemplateRowSearchModel
    {
        public byte TEMPLATE_ID { get; set; }
    }

    public class TemplateRowSearch : Search
    {
        public TemplateRowSearchModel mSearch { get; set; }
    }

    public class TemplateColumnSearchModel
    {
        public byte TEMPLATE_ID { get; set; }
    }

    public class TemplateColumnSearch : Search
    {
        public TemplateColumnSearchModel mSearch { get; set; }
    }

    public class TemplateControlDataSearchModel
    {
        public byte TEMPLATE_ID { get; set; }
    }

    public class TemplateControlDataSearch : Search
    {
        public TemplateControlDataSearchModel mSearch { get; set; }
    } 

    public class UnitSearchModel
    {
        public string Name { get; set; }
    }

    public class UnitSearch : Search
    {
        public UnitSearchModel mSearch { get; set; }
    }

    public class PlantProductSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public long GRADE_ID { get; set; }
    }

    public class PlantProductSearch : Search
    {
        public PlantProductSearchModel mSearch { get; set; }
    }

    public class SetPlantPosition 
    {
        public long PLANT_ID { get; set; }
        public int DIRECTION { get; set; }
    }

    public class SetPosicausePosition
    {
        public long POSSICAUSE_ID { get; set; }
        public int DIRECTION { get; set; }
    }

    public class SetMethodPosition
    {
        public long METHOD_ID { get; set; }
        public int DIRECTION { get; set; }
    }

    public class SetSamplePosition
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public int DIRECTION { get; set; }
    }

    public class SetItemPosition
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public int DIRECTION { get; set; }
    }

    public class SetProductPosition
    {
        public long PRODUCT_ID { get; set; }
        public int DIRECTION { get; set; }
    }

    public class SetOffControlGraphPosition
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public int DIRECTION { get; set; }
    }

    public class SetExaTagPosition
    {
        public long ID { get; set; }
        public long PRODUCT_MAPPING_ID { get; set; }
        public int TAG_TYPE { get; set; }
        public int DIRECTION { get; set; }
    }

    public class RootCauseTypeSearchModel
    {
        public string NAME { get; set; }
        //public long? ROOT_CAUSE_TYPE_ID { get; set; }
    }

    public class RootCauseTypeSearch : Search
    {
        public RootCauseTypeSearchModel mSearch { get; set; }
    }


    public class RootCauseSearchModel
    {
        public string NAME { get; set; }
        public long ROOT_CAUSE_TYPE_ID { get; set; } //ตัวตั้ง
        public string ROOT_CAUSE_DESC { get; set; }
    }

    public class RootCauseSearch : Search
    {
        public RootCauseSearchModel mSearch { get; set; }
    }

    public class KPISearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public string KPI_DESC { get; set; }
    }

    public class KPISearch : Search
    {
        public KPISearchModel mSearch { get; set; }
    }

    public class PageIndexList
    {
        public int PageIndex { get; set; } 
    }


    public class ReduceFeedSearchModel
    {
        public long PLANT_ID { get; set; }
        public decimal? LIMIT_VALUE { get; set; }
        public string EXCEL_NAME { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public long UNIT_ID { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public string REDUCE_FEED_DESC { get; set; }
    }

    public class ReduceFeedSearch : Search
    {
        public ReduceFeedSearchModel mSearch { get; set; }
    }

    public class ReduceFeedDetailSearchModel
    {
        public long REDUCE_FEED_ID { get; set; }
        public decimal? CONVERT_VALUE { get; set; }
        public string EXA_TAG_NAME { get; set; }
    }

    public class ReduceFeedDetailSearch : Search
    {
        public ReduceFeedDetailSearchModel mSearch { get; set; }
    }


    public class DowntimeSearchModel
    {
        public long PLANT_ID { get; set; }
        public decimal? LIMIT_VALUE { get; set; }
        public string EXCEL_NAME { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public long UNIT_ID { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public string DOWNTIME_DESC { get; set; }
    }

    public class DowntimeSearch : Search
    {
        public DowntimeSearchModel mSearch { get; set; }
    }

    public class DowntimeDetailSearchModel
    {
        public long DOWNTIME_ID { get; set; }
        public decimal? CONVERT_VALUE { get; set; }
        public string EXA_TAG_NAME { get; set; }
    }

    public class DowntimeDetailSearch : Search
    {
        public DowntimeDetailSearchModel mSearch { get; set; }
    }


    #endregion


    #region Transaction
    public class AbnormalSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public decimal? VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
    }

    public class AbnormalSearch : Search
    {
        public AbnormalSearchModel mSearch { get; set; }
    }


    public class ChangeGradeSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public long? GRADE_FROM { get; set; }
        public long? GRADE_TO { get; set; }
        public decimal? VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
    }

    public class ChangeGradeSearch : Search
    {
        public ChangeGradeSearchModel mSearch { get; set; }
    }

    public class GetChangeGradeList
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
    }

    public class COADataSearchModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string CUSTOMER { get; set; }
        public string PRODUCT { get; set; }
        public string ITEM { get; set; }
        public string UNIT { get; set; }
        public string SPECIFICATION { get; set; }
        public string TEST_METHOD { get; set; }
        public string TANK { get; set; }
        //public string SPECTIFICATION { get; set; }

        public long? PRODUCT_MERGE_ID { get; set; }
    }

    public class COADataSearch : Search
    {
        public COADataSearchModel mSearch { get; set; }
    }


    public class CrossVolumeSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public long PRODUCT_FROM { get; set; }
        public long PRODUCT_TO { get; set; }
        public decimal? VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
    }

    public class CrossVolumeSearch : Search
    {
        public CrossVolumeSearchModel mSearch { get; set; }
    }


    public class TRDowntimeSearchModel
    {
        public long PLANT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
    }

    public class TRDowntimeSearch : Search
    {
        public TRDowntimeSearchModel mSearch { get; set; }
    }


    public class ExceptionSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public decimal? VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
    }

    public class ExceptionSearch : Search
    {
        public ExceptionSearchModel mSearch { get; set; }
    }


    public class FileTrendSearchModel
    {
        public long TEMPLATE_ID { get; set; }
        public string NAME { get; set; } 
    }

    public class FileTrendSearch : Search
    {
        public FileTrendSearchModel mSearch { get; set; }
    }


    public class OffControlCalSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public decimal? VOLUME { get; set; }
        public string CONTROL_VALUE { get; set; }
        public long ROOT_CAUSE_ID { get; set; }
        public string OFF_CONTROL_DESC { get; set; } 
    }

    public class OffControlCalSearch : Search
    {
        public OffControlCalSearchModel mSearch { get; set; }
    }


    public class ProductDataSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public long EXA_TAG_ID { get; set; }
        public DateTime? TAG_DATE { get; set; }
        public string TAG_DATA { get; set; }
        public decimal? TAG_VALUE { get; set; }
        public byte? OFF_CONTROL_FLAG_0 { get; set; }
        public byte? OFF_SPEC_FLAG_0 { get; set; }
        public byte? TURN_AROUND_FLAG_0 { get; set; }
        public byte? DOWNTIME_FLAG_0 { get; set; }
        public byte? CHANGE_GRADE_FLAG_0 { get; set; }
        public byte? CROSS_VOLUME_FLAG_0 { get; set; }
        public byte? GC_ERROR_FLAG_0 { get; set; }
        public byte? REDUCE_FEED_FLAG_0 { get; set; }
        public byte? OFF_CONTROL_FLAG { get; set; }
        public byte? OFF_SPEC_FLAG { get; set; }
        public byte? TURN_AROUND_FLAG { get; set; }
        public byte? DOWNTIME_FLAG { get; set; }
        public byte? CHANGE_GRADE_FLAG { get; set; }
        public byte? CROSS_VOLUME_FLAG { get; set; }
        public byte? GC_ERROR_FLAG { get; set; }
        public byte? REDUCE_FEED_FLAG { get; set; }
        public byte? EXCEPTION_FLAG { get; set; }
        public byte? EXCEPTION_VALUE { get; set; }
    }

    public class ProductDataSearch : Search
    {
        public ProductDataSearchModel mSearch { get; set; }
    }


    public class ProductDowntimeDataSearchModel
    {
        public long DOWNTIME_DETAIL_ID { get; set; }
        public DateTime? TAG_DATE { get; set; }
        public decimal? TAG_VALUE { get; set; }
        public byte? DOWNTIME_FLAG_0 { get; set; }
        public byte? DOWNTIME_FLAG { get; set; }
    }

    public class ProductDowntimeDataSearch : Search
    {
        public ProductDowntimeDataSearchModel mSearch { get; set; }
    }


    public class RawDataSearchModel
    {
        public long PLANT_ID { get; set; }
        public DateTime? DATE_START { get; set; }
        public DateTime? DATE_END { get; set; }
        public string FILE_PATH { get; set; }
        public string RAW_DATA_DESC { get; set; }
    }

    public class RawDataSearch : Search
    {
        public RawDataSearchModel mSearch { get; set; }
    }


    public class TRReduceFeedSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
    }

    public class TRReduceFeedSearch : Search
    {
        public TRReduceFeedSearchModel mSearch { get; set; }
    }


    public class TRReduceFeedDataSearchModel
    {
        public long REDUCE_FEED_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public decimal? TAG_VALUE { get; set; }
        public byte? REDUCE_FEED_FLAG_0 { get; set; }
        public byte? REDUCE_FEED_FLAG { get; set; }
    }

    public class TRReduceFeedDataSearch : Search
    {
        public TRReduceFeedDataSearchModel mSearch { get; set; }
    }


    public class TemplateCOAModel
    {
        public long[] ids { get; set; }
        public List<TemplateCOASearchModel> listData { get; set; }
    }
    public class TemplateCOASearchModel
    {
        public long ID { get; set; }
        public long COA_DOC_STATUS { get; set; }
        public string PRODUCT { get; set; }
        public string CUSTOMER { get; set; }
        public long? CUSTOMER_MERGE_ID { get; set; }
        public byte TREND_GRAPH_FLAG { get; set; }
    }

    public class TemplateCOASearch : Search
    {
        public TemplateCOASearchModel mSearch { get; set; }
    }


    public class TemplateCOADetailModel
    {
        public long userId { get; set; }
        public long[] ids { get; set; }
        public List<TemplateCOADetailSearchModel2> listData { get; set; }
    }

    public class TemplateCOADetailSearchModel2
    {
        public long ID { get; set; }
        public long? PRODUCT_MERGE_ID { get; set; }
        public byte TREND_GRAPH_FLAG { get; set; }
    }
    public class TemplateCOADetailSearchModel
    {
        public long ID { get; set; }
        public string COA_ID { get; set; }
        public long ITEM_ID { get; set; }
        public long UNIT_ID { get; set; }
        public string TEST_METHOD { get; set; }
        public string SPECIFICATION { get; set; }
        public long CONTROL_ID { get; set; }
        public long? PRODUCT_MERGE_ID { get; set; }
        public byte TREND_GRAPH_FLAG { get; set; }
    }

    public class TemplateCOADetailSearch : Search
    {
        public TemplateCOADetailSearchModel mSearch { get; set; }
    }


    public class TrendDataSearchModel
    { 

        public long TEMPLATE_ID { get; set; }
        public long SAMPLE_ID { get; set; }
        public long ITEM_ID { get; set; }
        public DateTime? RECORD_DATE { get; set; }
        public byte? CONC_LOADING_FLAG { get; set; }
        public string DATA_VALUE_TEXT { get; set; }
        public string DATA_SIGN_1 { get; set; }
        public decimal? DATA_VALUE_1 { get; set; }
        public string DATA_SIGN_2 { get; set; }
        public decimal? DATA_VALUE_2 { get; set; }
    }

    public class TrendDataSearch : Search
    {
        public TrendDataSearchModel mSearch { get; set; }
    }
      
    public class TurnAroundSearchModel
    {
        public long PLANT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
    }

    public class TurnAroundSearch : Search
    {
        public TurnAroundSearchModel mSearch { get; set; }
    }

    //public static class ReturnStatus
    //{
    //   public static string Success = "Success";
    //   public static string Error = "Error";
    //}

    public class IQCControlSearchModel
    {
        public DateTime SENDMAIL_DATE { get; set; }
        public long CONTROL_ID { get; set; }
        public long RULE_ID { get; set; }
        public DateTime START_POINT { get; set; }
        public DateTime END_POINT { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string CONTROL_REASON { get; set; }
        public string CONTROL_DESC { get; set; }
    }

    public class IQCControlSearch : Search
    {
        public IQCControlSearchModel mSearch { get; set; }
    }

    public class IQCConfigSearchModel
    {
        public long CONTROL_ID { get; set; }
        public decimal CONFIG_SAMPLE { get; set; }
        public decimal CONFIG_STD { get; set; }
        public decimal CONFIG_AVG { get; set; }
        public decimal CONFIG_TOLERANCE { get; set; }
    }

    public class IQCConfigSearch : Search
    {
        public IQCConfigSearchModel mSearch { get; set; }
    }



    #endregion


    #region Report

    public class ReportTrendSearchModel
    {
        public string NAME { get; set; }
        public byte TREND_TYPE { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
    }

    public class ReportTrendSearch : Search
    {
        public ReportTrendSearchModel mSearch { get; set; }
    }

    public class ReportOffControlSearchModel
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }

        public byte DOC_STATUS { get; set; }

        public string TITLE_DESC { get; set; }
        public string DETAIL_DESC { get; set; }

        public string TITLE_GRAPH { get; set; }
        public string TITLE_GRAPH_X { get; set; }
        public string TITLE_GRAPH_Y { get; set; }
        public byte AUTOGEN_FLAG { get; set; }
        public byte SHOW_DASHBOARD { get; set; }
        public byte STATUS_RAW_DATA { get; set; }
    }

    public class ReportOffControlSearch : Search
    {
        public ReportOffControlSearchModel mSearch { get; set; }
    }

    public class ExaScheduleSearchModel
    {
        public long PLANT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; } 
    }

    public class ExaScheduleSearch : Search
    {
        public ExaScheduleSearchModel mSearch { get; set; }
    }

    public class ExaDowntimeSearchModel
    {
        public long PLANT_ID { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }

    public class ExaDowntimeSearch : Search
    {
        public ExaDowntimeSearchModel mSearch { get; set; }
    }

    public class ExaReduceFeedSearchModel
    {
        public long PLANT_ID { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }

    public class ExaReduceFeedSearch : Search
    {
        public ExaReduceFeedSearchModel mSearch { get; set; }
    }

    public class ExaProductSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public bool bCrossVolume { get; set; }
        public long QMS_TR_CROSS_VOLUME_ID { get; set; }
        public bool bExceptAbnormal { get; set; } //ยกเว้น abnormal
        public long PRODUCT_ID_TO { get; set; }
    }

    public class ExaProductSearch : Search
    {
        public ExaProductSearchModel mSearch { get; set; }
    }

    public class ExaProductExSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public List<long> TAG_EXA_ID { get; set; }
    }

    public class ExaProductSearchEx : Search
    {
        public ExaProductExSearchModel mSearch { get; set; }
    }

    public class ReportOffControlDetailSearchModel
    {
        public long RP_OFF_CONTROL_ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public decimal? VOLUME { get; set; }
        public string CONTROL_VALUE { get; set; }
        public long ROOT_CAUSE_ID { get; set; }

        public bool CHK_AUTO_CAL { get; set; }
        public DateTime? AUTO_START_DATE { get; set; }
        public DateTime? AUTO_END_DATE { get; set; }
        public byte OFF_TYPE { get; set;  }
        public byte AUTO_OFF_TYPE { get; set; }
    }

    public class ReportOffControlDetailSearch : Search
    {
        public ReportOffControlDetailSearchModel mSearch { get; set; }
    }

    public class ReportOffControlGraphSearchModel
    {
        public long RP_OFF_CONTROL_ID { get; set; }
        public string PREVIOUS { get; set; }
        public decimal? OFF_CONTROL { get; set; }
        public byte? POSITION { get; set; }
        public string GRAPH_DESC { get; set; }
    }

    public class ReportOffControlGraphSearch : Search
    {
        public ReportOffControlGraphSearchModel mSearch { get; set; }
    }

    public class ReportOffControlAttachSearchModel
    {
        public long RP_OFF_CONTROL_ID { get; set; }
        public string NAME { get; set; }
        public string PATH { get; set; }
        public byte? POSITION { get; set; }
    }

    public class ReportOffControlAttachSearch : Search
    {
        public ReportOffControlAttachSearchModel mSearch { get; set; }
    }


    public class ReportOffControlSummarySearchModel
    {
        public long RP_OFF_CONTROL_ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public decimal? VALUE { get; set; }
        public byte DATA_TYPE { get; set; }
    }

    public class ReportOffControlSummarySearch : Search
    {
        public ReportOffControlSummarySearchModel mSearch { get; set; }
    }

   
    public class RPOffControlSummaryRowSearchModel
    {
        public byte RP_OFF_CONTROL_ID { get; set; }
    }

    public class RPOffControlSummaryRowSearch : Search
    {
        public RPOffControlSummaryRowSearchModel mSearch { get; set; }
    }

    public class RPOffControlSummaryColumnSearchModel
    {
        public byte RP_OFF_CONTROL_ID { get; set; }
    }

    public class RPOffControlSummaryColumnSearch : Search
    {
        public RPOffControlSummaryColumnSearchModel mSearch { get; set; }
    }

    public class RPOffControlSummarySearchModel
    {
        public byte PLANT_ID { get; set; }
        public byte PRODUCT_ID { get; set; }
    }

    public class RPOffControlSummarySearch : Search
    {
        public RPOffControlSummarySearchModel mSearch { get; set; }
    }

    #endregion


    #region Setting

    public class SettingOffControlSearchModel
    {
        public long RP_OFF_CONTROL_ID { get; set; }        
        public byte OFF_CONTROL_TYPE { get; set; }
        public string OFF_CONTROL_DESC { get; set; }
    }

    public class SettingOffControlSearch : Search
    {
        public SettingOffControlSearchModel mSearch { get; set; }
    }

    public class ConfigSettingSearchModel
    {
        public long ID { get; set; }
        public string HOME_BACKGROUND { get; set; }
        public byte ABNORMAL_MAX_REPEAT { get; set; }
        public byte ABNORMAL_OVERSHOOT { get; set; }
        public string ABNORMAL_ALPHABET { get; set; }
        public Int16 UPPER_BOUNDARY { get; set; }
        public Int16 LOWER_BOUNDARY { get; set; }

        public long REPORT_OFF_CONTROL { get; set; }
        public long REPORT_OFF_SPEC { get; set; }
        public long HIGHLIGHT_1 { get; set; }
        public long HIGHLIGHT_2 { get; set; }
        public long HIGHLIGHT_3 { get; set; }
        public long HIGHLIGHT_4 { get; set; }

        public DateTime EXA_SCHEDULE { get; set; }
        public string LIMS_SERVER { get; set; }
        public DateTime LIMS_SCHEDULE { get; set; }
        public string LIMS_USER { get; set; }
        public string LIMS_PASSWORD { get; set; }
        public Int16 LIMS_PORT { get; set; }
        public string LIMS_SID { get; set; }
        public DateTime TEMPLATE_SCHEDULE { get; set; }
    }

    public class ConfigSettingSearch : Search
    {
        public ConfigSettingSearchModel mSearch { get; set; }
    }

    public class FileAttachSearchModel
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public string PATH { get; set; }
        public byte BOARD_TYPE { get; set; }        
    }

    public class FileAttachSearch : Search
    {
        public FileAttachSearchModel mSearch { get; set; }
    }

    public class SystemLogSearchModel
    {
        public long ID { get; set; }
        //public DateTime? LOG_DATE { get; set; }
        public byte ERROR_LEVEL { get; set; }
        public string PROCESS_NAME { get; set; }
        public string PROCESS_DESC { get; set; }
    }

    public class SystemLogSearch : Search
    {
        public SystemLogSearchModel mSearch { get; set; }
    }
    #endregion

    public class FolderDataDetail
    {
        public long TEMPLATE_ID { get; set; }
        public string m_dtFile { get; set; }
        public string m_dtFolder { get; set; }
        public string m_dtDirectory { get; set; }
        public DateTime m_FolderModify { get; set; }
        public DateTime m_FileModify { get; set; }
        public bool ITEM_SELECTED { get; set; }
    }

    public class TrendSample
    {
        public long PLANT_ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public byte TEMPLATE_TYPE { get; set; }
        public long SAMPLE_ID { get; set; } 
        public string SAMPLE_NAME { get; set; }
        public string SAMPLE_POINT { get; set; }
    }

    public class TrendArea
    {
        public long PLANT_ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public long AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
    }

    public class TrendItem
    {
        public long PLANT_ID { get; set; }
        public long SAMPLE_ID { get; set; }
        public long ITEM_ID { get; set; } 
        public string ITEM_NAME { get; set; }
        public long UNIT_ID { get; set; }
        public string UNIT_NAME { get; set; }
    }

    public class CheckOffControlSpecModel
    {
        public byte OFF_STATUS { get; set; }
        public string CONTROL_NAME { get; set; }
        public decimal CONTROL_VALUE { get; set; }
    }
     

    public class AccumDetailConvertList
    {
        public long Position { get; set; } 
    }

    public class AccumConvertList
    {
        public long AccumPosition { get; set; }
        public List<AccumDetailConvertList> DetailPostion { get; set; }
    }

    public class PersonalInfo
    {
        public string PERSON_CODE { get; set; }
        public string INAME { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }

        public string FULLNAMETH { get; set; }

        public string POSCODE { get; set; }
        public string POSNAME { get; set; }

        public string UNITCODE { get; set; }
        public string UNITNAME { get; set; }

        public string EMAIL { get; set; }

        public bool ITEM_SELECTED { get; set; }
    }

    public class PersonalUnit
    {
        public string UNITCODE { get; set; }
        public string UNITABBR { get; set; }
        public string DUMMY_RELATIONSHIP { get; set; }
    }

    public class PersonlPosCode
    {
        public string POSCODE { get; set; }
        public string POSNAME { get; set; }
        public string POSABBR { get; set; }
    }

    public class PersonalInfoSearchModel
    {
        public string UNIT_NAME { get; set; }
        public string NAME_SURNAME { get; set; }
        public string EMPLOY_ID { get; set; }
        public string POSTITION { get; set; }
    }

    public class PersonalInfoSearch : Search
    {
        public PersonalInfoSearchModel mSearch { get; set; }
    }

    public class  QMS_MA_EMAILSearchModel
    {
        public byte GROUP_TYPE { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public string POSITION { get; set; }
        public string UNIT_NAME { get; set; }
    }

    public class QMS_MA_EMAILSearch : Search
    {
        public QMS_MA_EMAILSearchModel mSearch { get; set; }
    }

    public class CorrectDataReportSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }

    public class CorrectDataReportSearch : Search
    {
        public CorrectDataReportSearchModel mSearch { get; set; }
    }

    public class ShiftAnalysisReportSearchModel
    {
        public long PLANT_ID { get; set; }
        public byte OFF_TYPE { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }

    public class ShiftAnalysisReportSearch : Search
    {
        public ShiftAnalysisReportSearchModel mSearch { get; set; }
    } 

    public class VIEW_CORRECT_REPORT
    {
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public byte CORRECT_TYPE { get; set; }
        public string CORRECT_NAME { get; set; }

        public string SHIFT { get; set; }
        public string DESC { get; set; }
        public decimal VOLUME { get; set; } 
    }

    public class VIEW_CORRECT_SUM_REPORT
    {
        public string header1 { get; set; }
        public string header2 { get; set; }

        public List<VIEW_CORRECT_REPORT> listTurnAroundResult { get; set; }
        public List<VIEW_CORRECT_REPORT> listDownTimeResult { get; set; }
        public List<VIEW_CORRECT_REPORT> listChangeGradeResult { get; set; }
        public List<VIEW_CORRECT_REPORT> listCrossVolumeResult { get; set; }
        public List<VIEW_CORRECT_REPORT> listAbnormalResult { get; set; }
        public List<VIEW_CORRECT_REPORT> listReduceFeedResult { get; set; }
        public List<VIEW_CORRECT_REPORT> listExceptionResult { get; set; }

        public decimal totalVolTurnAround { get; set; }
        public decimal totalVolDownTime { get; set; }
        public decimal totalVolChangeGrade { get; set; }
        public decimal totalVolCross { get; set; }
        public decimal totalVolAbnormal { get; set; }
        public decimal totalVolReduceFeed { get; set; }
        public decimal totalVolException { get; set; }

    }

    public class VIEW_SHIFT_REPORT
    {
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public string SHIFT_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public decimal VOLUME { get; set; }
    }

    public class VIEW_SHIFT_SUM_REPORT
    {
        public string SHIFT_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public decimal VOLUME { get; set; }
    }

    public class VIEW_OPERATION_SHIFT_REPORT
    {
        public string header1 { get; set; }
        public string header2 { get; set; }

        public List<VIEW_SHIFT_REPORT> listShiftDetail { get; set; }
        public List<VIEW_SHIFT_SUM_REPORT> listSummary { get; set; } 
    }

    public class VoulmeEXQSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }

    public class OffControlCalAutoSearchModel  
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public long RPOffControlId { get; set; }
        public long ROOT_CAUSE_ID { get; set; }
        public byte OFF_TYPE { get; set; }
        public byte AUTO_OFF_TYPE { get; set; }
        public bool CHK_CORRECTDATA { get; set; }
        public bool CHK_RECALRAW { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public byte AUTOGEN_FLAG { get; set; }


        /****** for recal ******/
        public bool CHK_ALL_PLANT { get; set; }
        public bool CHK_ALL_PRODUCT { get; set; }
        public List<long> LIST_PLANT_ID { get; set; }
        public List<long> LIST_PRODUCT_ID { get; set; }

    }



    public class DashOffControlCalAutoSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte OFF_TYPE { get; set; }
        public byte AUTO_OFF_TYPE { get; set; }
        public bool CHK_CORRECTDATA { get; set; }
        public bool CHK_RECALRAW { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public byte AUTOGEN_FLAG { get; set; }
        public byte SHOW_DASHBOARD { get; set; }


        /****** for recal ******/
        public bool CHK_ALL_PLANT { get; set; }
        public bool CHK_ALL_PRODUCT { get; set; }
        public List<long> LIST_PLANT_ID { get; set; }
        public List<long> LIST_PRODUCT_ID { get; set; }

    }

    public class OffControlCalAutoReportSearchModel
    {
        public long RPOffControlId { get; set; }
        public byte OFF_TYPE { get; set; }
        public byte AUTO_OFF_TYPE { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }
    public class productDistributionChartSearchModel
    {
        public long? GSP { get; set; }
        public long? Product { get; set; }
        public string Composition { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
    }

    public class ConvertEventRawDataToCPKModel
    { 
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
    }

    public class VoulmeEXQSearch : Search
    {
        public VoulmeEXQSearchModel mSearch { get; set; }
    }

    public class OffControlAutoCalInterception
    { 
        public DateTime ACTIVE_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public int CORRECT_TYPE { get; set; }
    }

    public class OffControlAutoCalResult
    {
        public decimal VOLUME_INPUT { get; set; }
        public decimal VOULUME_OFFCONTROL { get; set; } 
    }

    public class V_TEMPLATE_MASTER
    {
        public string ID { get; set; }
        public string CUSTOMERNAME { get; set; }
        public string PRODUCT { get; set; }
        public DateTime? CREATEDT { get; set; }
    }

    public class V_TEMPLATE_DETAIL
    {
        public string ID { get; set; }
        public DateTime SAMPLINGDATE { get; set; }
        public string SAMPLINGNAME { get; set; }
        public string SAMPLINGPOINT { get; set; }
        
        public string CUSTOMERNAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string TANK_NAME { get; set; }
        public string ITEM { get; set; }
        public string UNIT { get; set; }

        public int USER_SEQUENCE { get; set; }
        public string TEST_METHOD { get; set; }   
        public string SPECIFICATION { get; set; }
        public string DISPLAYVALUE { get; set; }
        public decimal ENTERVALUE { get; set; }
        //public DateTime CREATEDT { get; set; } 
    }

    public class TEMPLATE_COA_CHECK
    {
        //public string ID { get; set; }//COA_ID  ยกเลิก ใช้ ลูกค้า กับ ผลิตภัณฑ์ แทน
        public string PRODUCT_NAME { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public long Qty { get; set; }
    }

    public class CUSTOMER_COA
    {
        public string COA_ID { get; set; } //ยกเลิกใช้เป็น customer name แทน
        public string CUSTOMER { get; set; }
        public string SHOW_CUSTOMER { get; set; }
        public long? CUS_MERGE { get; set; }
    }

    public class PRODUCT_COA
    {
        public string COA_ID { get; set; } //ยกเลิกใช้เป็น product name แทน
        public string PRODUCT { get; set; }
        public int POSITION { get; set; }
    }

    public class TemplateName_COMBO
    {
        public string TEMPLATE_ID { get; set; }
        public string TEMPLATE_NAME { get; set; }
        public int TEMPLATE_TYPE { get; set; }
        public string CREATE_USER { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
    }

    public class CUSTOMER_PRODUCT_COA
    {
        public string COA_ID { get; set; }
        public string CUSTOMER { get; set; }
        public string PRODUCT { get; set; }
    }

    public class ITEM_COA
    {
        public long ID { get; set; }
        public string COA_ID { get; set; }
        public string ITEM { get; set; }
        public string ITEM_SHOW { get; set; }
        public string UNIT_NAME { get; set; }
        public string SPECIFICATION { get; set; }
        public string TEST_METHOD { get; set; }
        public int USER_SEQUENCE { get; set; } 
        public long? PRODUCT_MERGE_ID { get; set; } 
        public string DISPLAY_NAME { get; set; }
    }

    public class TANK_COA
    {
        public string TANK_ID { get; set; }
        public string TANK_NAME { get; set; } 
    }

    //public class GRAPH_TREDN_GAS
    //{
    //    /*for COA */
    //    public List<GRAPH_TREND_GAS_DATA> listCOATrend1 { get; set; }
    //    public List<GRAPH_TREND_GAS_DATA> listCOATrend2 { get; set; }

    //    public TrendGraphSum listSummary1 { get; set; }
    //    public TrendGraphSum listSummary2 { get; set; }

    //    public SpecControlValue spec1 { get; set; }
    //    public SpecControlValue spec2 { get; set; }
    //    /*for COA */
    //}

    public class CscData
    {
        public string ITEM_LABEL { get; set; }
        public decimal? ENTER_VALUE { get; set; }
        public string DISPLAY_VALUE { get; set; }
        public string TANK_NAME { get; set; }
        public string UNIT_NAME { get; set; }
        public string SPECIFICATION { get; set; }
        public string TEST_METHOD { get; set; }
    }

    public class ProductCscData
    {
        public DateTime SAMPLING_DATE { get; set; }
        public List<CscData> ITEMS_DATA { get; set; }
    }

    public class ItemCscDataExcel
    {
        public string ITEM { get; set; }
        public string UNIT{ get; set; }
        public string SPEC { get; set; }
        public int iColumn { get; set; }
        public int USER_SEQUENCE { get; set; }
        public string TEST_METHOD { get; set; }
    }

    public class GRAPH_TREDN_GAS
    { 
        public List<GRAPH_TREND_GAS_DATA> listTrend1 { get; set; }
        public List<GRAPH_TREND_GAS_DATA> listTrend2 { get; set; }
        public List<GRAPH_TREND_GAS_DATA> listTrend3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public List<GRAPH_TREND_GAS_DATA> listTrend4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public List<GRAPH_TREND_GAS_DATA> listTrend5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public List<GRAPH_TREND_GAS_DATA> listTrend6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item

        public TrendGraphSum listSummary1 { get; set; }
        public TrendGraphSum listSummary2 { get; set; }
        public TrendGraphSum listSummary3 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public TrendGraphSum listSummary4 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public TrendGraphSum listSummary5 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item
        public TrendGraphSum listSummary6 { get; set; }//เพิ่มตอน pop ทำ graph mutiple product & item

        public SpecControlValue control1 { get; set; } //เพิ่มมาตอน Trend data Product
        public SpecControlValue control2 { get; set; } //เพิ่มมาตอน Trend data Product
        public SpecControlValue control3 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public SpecControlValue control4 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public SpecControlValue control5 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public SpecControlValue control6 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item

        public SpecControlValue spec1 { get; set; }
        public SpecControlValue spec2 { get; set; }
        public SpecControlValue spec3 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public SpecControlValue spec4 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public SpecControlValue spec5 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public SpecControlValue spec6 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public List<SpecControlValue> controlConf1 { get; set; }
        public List<SpecControlValue> controlConf2 { get; set; }
        public List<SpecControlValue> controlConf3 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public List<SpecControlValue> controlConf4 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public List<SpecControlValue> controlConf5 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public List<SpecControlValue> controlConf6 { get; set; }
        public int? qtydatacontrol1 { get; set; }
        public int? qtydatacontrol2 { get; set; }
        public int? qtydatacontrol3 { get; set; }
        public int? qtydatacontrol4 { get; set; }
        public int? qtydatacontrol5 { get; set; }
        public int? qtydatacontrol6 { get; set; }

        public List<List<SpecControlValue>> listControlConf1 { get; set; }
        public List<List<SpecControlValue>> listControlConf2 { get; set; }
        public List<List<SpecControlValue>> listControlConf3 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public List<List<SpecControlValue>> listControlConf4 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public List<List<SpecControlValue>> listControlConf5 { get; set; } //เพิ่มตอน pop ทำ graph mutiple product & item
        public List<List<SpecControlValue>> listControlConf6 { get; set; }
    }

    //public class GRAPH_TREDN_PRODUCT//เพิ่มตอน pop ทำ graph mutiple y axis
    //{
    //    public List<GRAPH_TREND_PRODUCT_DATA> listTrend1 { get; set; }
    //    public List<GRAPH_TREND_PRODUCT_DATA> listTrend2 { get; set; }
    //    public List<GRAPH_TREND_PRODUCT_DATA> listTrend3 { get; set; }
    //    public List<GRAPH_TREND_PRODUCT_DATA> listTrend4 { get; set; }
    //    public List<GRAPH_TREND_PRODUCT_DATA> listTrend5 { get; set; }
    //    public List<GRAPH_TREND_PRODUCT_DATA> listTrend6 { get; set; }
    //    public List<GRAPH_TREND_PRODUCT_DATA> listTrend7 { get; set; }

    //    public TrendGraphSum listSummary1 { get; set; }
    //    public TrendGraphSum listSummary2 { get; set; }
    //    public TrendGraphSum listSummary3 { get; set; }
    //    public TrendGraphSum listSummary4 { get; set; }
    //    public TrendGraphSum listSummary5 { get; set; }
    //    public TrendGraphSum listSummary6 { get; set; }
    //    public TrendGraphSum listSummary7 { get; set; }

    //    public SpecControlValue control1 { get; set; }
    //    public SpecControlValue control2 { get; set; }
    //    public SpecControlValue control3 { get; set; }
    //    public SpecControlValue control4 { get; set; }
    //    public SpecControlValue control5 { get; set; }
    //    public SpecControlValue control6 { get; set; }
    //    public SpecControlValue control7 { get; set; }

    //    public SpecControlValue spec1 { get; set; }
    //    public SpecControlValue spec2 { get; set; }
    //    public SpecControlValue spec3 { get; set; }
    //    public SpecControlValue spec4 { get; set; }
    //    public SpecControlValue spec5 { get; set; }
    //    public SpecControlValue spec6 { get; set; }
    //    public SpecControlValue spec7 { get; set; }
    //}

    public class GRAPH_TREDN_GAS_DATA
    {
        public List<GRAPH_TREND_GAS_DATA> listTrend { get; set; }
        public TrendGraphSum listSummary { get; set; }
        public SpecControlValue spec { get; set; }
        public List<SpecControlValue> specConf { get; set; }
        public List<List<SpecControlValue>> listSpecConf { get; set; }
        public int? qtydatacontrol { get; set; }
    }

    public class TEMPLATE_IMP_EXP_DATA
    {
        public long TAG_ID { get; set; }
        public string EXCEL_NAME { get; set; }
    }

    public class VALIDATE_EXCEPTION_OVERLLAP
    {
        public bool IS_OVERLAP { get; set; }
        public string MSG_ERROR { get; set; }
        //public DateTime statDate { get; set; }
        //public DateTime endDate { get; set; }
    }


    public class IntervalSamplingSearch : Search
    {
        public IntervalSamplingSearchModel mSearch { get; set; }
    }
    public class IntervalSamplingSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string COMPOSATION { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public string KPI_DESC { get; set; }
    }


    public class ProductMergeRowSearchModel
    {
        public long ID { get; set; }
    }

    public class ProductMergeRowSearch : Search
    {
        public ProductMergeRowSearchModel mSearch { get; set; }
    }

    public class ProductMergeSearch : Search
    {
        public ProductMergeSearchModel mSearch { get; set; }
    }
    public class ProductMergeSearchModel
    {
        public string ITEM { get; set; }
        public string UNIT { get; set; }
        public string CONTROL_VALUE { get; set; }
        public string TEST_METHOD { get; set; }
        public string SPEC_VALUE { get; set; }
        public string DISPLAY_NAME { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }
    }

    public class CustomerMergeRowSearchModel
    {
        public long ID { get; set; }
    }

    public class CustomerMergeRowSearch : Search
    {
        public CustomerMergeRowSearchModel mSearch { get; set; }
    }

    public class CustomerMergeSearch : Search
    {
        public CustomerMergeSearchModel mSearch { get; set; }
    }
    public class CustomerMergeSearchModel
    {
        public string CUSTOMER_NAME { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATE { get; set; }
    }

    public class SigmaLevelSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public string SIGMA_LEVEL_DESC { get; set; }
    }

    public class SigmaLevelSearch : Search
    {
        public SigmaLevelSearchModel mSearch { get; set; }
    }


    public class DashboardSearchModel
    {
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public string KPI_DESC { get; set; }
    }

    public class DashboardSearch : Search
    {
        public DashboardSearchModel mSearch { get; set; }
    }





}




