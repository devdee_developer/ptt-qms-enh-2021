//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QMSSystem.CoreDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS_TR_IQC_CONTROL
    {
        public long ID { get; set; }
        public System.DateTime SENDMAIL_DATE { get; set; }
        public long CONTROL_ID { get; set; }
        public long RULE_ID { get; set; }
        public System.DateTime START_POINT { get; set; }
        public System.DateTime END_POINT { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string CONTROL_REASON { get; set; }
        public string CONTROL_DESC { get; set; }
        public short POSITION { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public System.DateTime UPDATE_DATE { get; set; }
    }
}
