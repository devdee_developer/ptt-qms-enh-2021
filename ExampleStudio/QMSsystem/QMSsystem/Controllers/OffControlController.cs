﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using System.IO;
using QMSSystem.CoreDB.Helper;
using QMSSystem.Model;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_OffControlCal, PrivilegeModeEnum.N_OffSpecCal, PrivilegeModeEnum.D_OffControl, PrivilegeModeEnum.D_OffSpec }, DataAccess = DataAccessEnum.Authen)]
    public class OffControlController : BaseController
    {
        //
        // GET: /OffControl/
        public ActionResult DashboardControl(long id = 0)
        {
            ConfigServices ConfigServices = new ConfigServices(_username);
            ReportServices ReportServices = new ReportServices(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_ST_SYSTEM_CONFIG createQMS_ST_SYSTEM_CONFIG = new CreateQMS_ST_SYSTEM_CONFIG();

            ReportOffControlSearchModel searchOffControl = new ReportOffControlSearchModel();           
            ReportOffControlDetailSearchModel searchOffControlDetail = new ReportOffControlDetailSearchModel();
            ReportOffControlAttachSearchModel searchOffControlAttach = new ReportOffControlAttachSearchModel();
            ReportOffControlGraphSearchModel searchOffControlGraph = new ReportOffControlGraphSearchModel();
            ReportOffControlSummarySearchModel searchOffControlSummary = new ReportOffControlSummarySearchModel();
   
            searchOffControlDetail.RP_OFF_CONTROL_ID = id;
            searchOffControlGraph.RP_OFF_CONTROL_ID = id;
            searchOffControlAttach.RP_OFF_CONTROL_ID = id;
            searchOffControlSummary.RP_OFF_CONTROL_ID = id;
          
            List<ViewQMS_ST_SYSTEM_CONFIG> listData = new List<ViewQMS_ST_SYSTEM_CONFIG>();

            listData = ConfigServices.getAllQMS_ST_SYSTEM_CONFIG(_db);

            ViewQMS_ST_SYSTEM_CONFIG dataItem = listData.FirstOrDefault();

            if (null != dataItem)
            {
                createQMS_ST_SYSTEM_CONFIG = ConfigServices.getQMS_ST_SYSTEM_CONFIGById(_db, dataItem.ID);
                
                id = dataItem.REPORT_OFF_CONTROL;
                searchOffControl.ID = id;
            }
            searchOffControl.OFF_CONTROL_TYPE = (byte)OFF_TYPE.CONTROL;

            ViewBag.createQMS_ST_SYSTEM_CONFIG = createQMS_ST_SYSTEM_CONFIG;

            ViewBag.RP_OFF_CONROL_ID = id;

            ViewBag.listOffControl = ReportServices.getQMS_RP_OFF_CONTROLList(_db, searchOffControl.OFF_CONTROL_TYPE);

            ViewBag.searchOffControl = searchOffControl;
            ViewBag.searchOffControlDetail = searchOffControlDetail;
            ViewBag.searchOffControlAttach = searchOffControlAttach;
            ViewBag.searchOffControlGraph = searchOffControlGraph;
            ViewBag.searchOffControlSummary = searchOffControlSummary;
   
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.DateNow = getDummyDateTime();

            return View();

        }

        public JsonResult GetDashboardOffControlSummary_T0(long RPOffControlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getDashboardOffControlSummary_T0(_db, RPOffControlId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDashboardOffControlSummary_T1(long RPOffControlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getDashboardOffControlSummary_T1(_db, RPOffControlId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDashboardOffControlSummary_T2(long RPOffControlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getDashboardOffControlSummary_T2(_db, RPOffControlId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void OffControlDataExcel(long rp_off_control_id)
        {
            ReportServices ReportServices = new ReportServices(_username);

            ExcelPackage excel = ReportServices.OffControlDataExcel(_db, rp_off_control_id);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.ReportOffControl + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }

        public void OffControlGraphDataExcel(long rp_off_control_id)
        {
            ReportServices ReportServices = new ReportServices(_username);

            ExcelPackage excel = ReportServices.OffControlGraphDataExcel(_db, rp_off_control_id);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.OffControlSummaryReportGraph + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }
         
        public ActionResult ListManageOffControl()//control ListRawDataExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            OffControlCalSearchModel searchOffControlCal = new OffControlCalSearchModel();
            CreateQMS_TR_OFF_CONTROL_CAL createQMS_TR_OFF_CONTROL_CAL = new CreateQMS_TR_OFF_CONTROL_CAL();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();
            createQMS_TR_OFF_CONTROL_CAL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.CONTROL;
            createQMS_TR_OFF_CONTROL_CAL.START_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.END_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.ROOT_CAUSE_ID = 1;//default;

            searchOffControlCal.OFF_CONTROL_TYPE = (byte)OFF_TYPE.CONTROL;

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusListWithInitail();
            ViewBag.listDocStatusEx = this.getDocStatusList();

            ViewBag.searchOffControlCal = searchOffControlCal;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_TR_OFF_CONTROL_CAL = createQMS_TR_OFF_CONTROL_CAL;
            ViewBag.volumeSearch = volumeSearch;
            return View();
        }

        public ActionResult CreateManageOffControl(long id = 0)//control CresteRawDataExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_OFF_CONTROL_CAL createQMS_TR_OFF_CONTROL_CAL = new CreateQMS_TR_OFF_CONTROL_CAL();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_OFF_CONTROL_CAL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.CONTROL;
            createQMS_TR_OFF_CONTROL_CAL.START_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.END_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.ROOT_CAUSE_ID = 1;//default;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_OFF_CONTROL_CAL = transactionServices.getQMS_TR_OFF_CONTROL_CALById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();

            ViewBag.createQMS_TR_OFF_CONTROL_CAL = createQMS_TR_OFF_CONTROL_CAL;
            ViewBag.volumeSearch = volumeSearch;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_OFF_CONTROL_CAL(CreateQMS_TR_OFF_CONTROL_CAL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);

                    VALIDATE_EXCEPTION_OVERLLAP checkOverlap = transactionServices.IsOverlapMasterCorrectDataEx(_db, model.ID, model.PLANT_ID, model.PRODUCT_ID, 0, model.START_DATE, model.END_DATE, (byte)CORRECT_DATA_TYPE.EXCEPTION);
                    if (checkOverlap.IS_OVERLAP)
                    {
                        result = new { result = ReturnStatus.ERROR, message = checkOverlap.MSG_ERROR };
                    }
                    else
                    {
                        long nResult = transactionServices.SaveQMS_TR_OFF_CONTROL_CAL(_db, model);

                        if (nResult > 0)
                        {
                            model = transactionServices.getQMS_TR_OFF_CONTROL_CALById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_OFF_CONTROL_CALBySearch(OffControlCalSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_OFF_CONTROL_CAL> listData = new List<ViewQMS_TR_OFF_CONTROL_CAL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                     
                    listData = transactionServices.searchQMS_TR_OFF_CONTROL_CAL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        } 

        public JsonResult DeleteQMS_TR_OFF_CONTROL_CAL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_OFF_CONTROL_CALByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_OFF_CONTROL_CAL(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_OFF_CONTROL_CALByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public void OffControlCalDataExcel(ProductExaDataSearchModel searchModel)
        {
            TransactionService transDataServices = new TransactionService(_username);
            //ProductExaDataSearchModel searchModel = new ProductExaDataSearchModel();

            //searchModel.PRODUCT_ID = PLANT_ID;
            //searchModel.PLANT_ID = PRODUCT_ID;
            //searchModel.START_DATE = START_DATE;
            //searchModel.END_DATE = END_DATE;

            ExcelPackage excel = transDataServices.ProductExaDataExcel(_db, searchModel);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.OffControl + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }
        public ActionResult ManageLoadDataLake()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadDataFromDataLakeTODB(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    MasterDataService masterDataServices = new MasterDataService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                    
                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        DateTime? dStratDate = null;
                        DateTime? dEndDate = null;
                        dStratDate = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                        dEndDate = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                        var nResult = masterDataServices.LoadDataLakeTODB(_db, dStratDate, dEndDate);
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageLoadEventRawDataToDB()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadEventRawDataFromDataLakeTODB(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    TransactionService transDataServices = new TransactionService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);

                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        DateTime? dStratDate = null;
                        DateTime? dEndDate = null;
                        dStratDate = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                        dEndDate = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                        var nResult = transDataServices.LoadEventRawDataFromDataLakeTODB(_db, dStratDate, dEndDate);
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageLoadQmsRpOffControlToDataLake()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadRP_OFF_CONTROLFromDBTODataLake(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    TransactionService transDataServices = new TransactionService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);

                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        DateTime? dStratDate = null;
                        DateTime? dEndDate = null;
                        dStratDate = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                        dEndDate = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                        var nResult = transDataServices.LoadRP_OFF_CONTROLFromDBTODataLake(_db, dStratDate, dEndDate);
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageLoadQmsRpOffControlSummaryToDataLake()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadRP_OFF_CONTROL_SUMMARYFromDBTODataLake(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    TransactionService transDataServices = new TransactionService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);

                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        DateTime? dStratDate = null;
                        DateTime? dEndDate = null;
                        dStratDate = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                        dEndDate = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                        var nResult = transDataServices.LoadRP_OFF_CONTROL_SUMMARYFromDBTODataLake(_db, dStratDate, dEndDate);
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageLoadQmsRpOffControlStampToDataLake()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadOffControlFromDBTODataLake(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    TransactionService transDataServices = new TransactionService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);

                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        DateTime? dStratDate = null;
                        DateTime? dEndDate = null;
                        dStratDate = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                        dEndDate = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                        var nResult = transDataServices.LoadOffControlFromDBTODataLake(_db, dStratDate, dEndDate);
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageLoadAbnormalToDB()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadAbnormalFromDataLakeTODB(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    TransactionService transDataServices = new TransactionService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);

                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        DateTime? dStratDate = null;
                        DateTime? dEndDate = null;
                        dStratDate = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                        dEndDate = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                        var nResult = transDataServices.LoadAbnormalFromDataLakeTODB(_db, dStratDate, dEndDate);
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageLoadEventCalToDB()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadDataLake_EVENT_DATA_TODB(ManageLIMSSearchModel searchModel)
        {
            Object result;
            try
            {
                TransactionService transDataServices = new TransactionService(_username);
                var nResult = transDataServices.LoadDataLake_EVENT_DATA_TODB(_db);
                result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
            }
            catch (Exception ex)
            {
                result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageCheckTotalEXQProduct()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult getTotalDataForCheckDataLakeInputing(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    TransactionService transDataServices = new TransactionService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    //DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);

                    int nResult = transDataServices.getTotalDataForCheckDataLakeInputing(_db, startTime);
                    result = new { result = ReturnStatus.SUCCESS, message = nResult };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
