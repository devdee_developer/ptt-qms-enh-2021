﻿using System.Collections.Generic;

using System.Linq;
using QMSSystem.Model;

namespace QMSSystem.CoreDB
{

    public partial class QMS_RP_OFF_CONTROL 
    {
        public static List<QMS_RP_OFF_CONTROL> GetAllType(QMSDBEntities db)
        {
            return db.QMS_RP_OFF_CONTROL.Where(m => m.DELETE_FLAG == 0 ).ToList(); //linq 1 = Off control 2 = Off Spec
        }

        public static List<QMS_RP_OFF_CONTROL> GetAll(QMSDBEntities db, byte controltype)
        {
            return db.QMS_RP_OFF_CONTROL.Where(m => m.DELETE_FLAG == 0 && m.OFF_CONTROL_TYPE == controltype).ToList(); //linq 1 = Off control 2 = Off Spec
        }

        public static List<QMS_RP_OFF_CONTROL> GetActive(QMSDBEntities db, byte controltype)
        {
            return db.QMS_RP_OFF_CONTROL.Where(m => m.DELETE_FLAG == 0 && m.OFF_CONTROL_TYPE == controltype && m.DOC_STATUS == 2).ToList(); //linq 1 = Off control 2 = Off Spec
        }

        public static QMS_RP_OFF_CONTROL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_RP_OFF_CONTROL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_RP_OFF_CONTROL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_OFF_CONTROL.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL> GetAllBySearch(QMSDBEntities db, ReportOffControlSearchModel searchModel, byte controltype)
        {
            List<QMS_RP_OFF_CONTROL> lstResult = new List<QMS_RP_OFF_CONTROL>();

            IQueryable<QMS_RP_OFF_CONTROL> query = db.QMS_RP_OFF_CONTROL.Where(m => m.DELETE_FLAG == 0);

            try
            {
                             
                if (null != searchModel.OFF_CONTROL_TYPE && searchModel.OFF_CONTROL_TYPE > 0)
                {
                    query = query.Where(m => m.OFF_CONTROL_TYPE == searchModel.OFF_CONTROL_TYPE && m.OFF_CONTROL_TYPE == controltype);
                }

                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    query = query.Where(m => m.NAME.Contains(searchModel.NAME) && m.OFF_CONTROL_TYPE == controltype);
                }

                if (null != searchModel.DOC_STATUS && searchModel.DOC_STATUS > 0)
                {
                    query = query.Where(m => m.DOC_STATUS == searchModel.DOC_STATUS && m.OFF_CONTROL_TYPE == controltype);
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                if (null != searchModel.TITLE_DESC && "" != searchModel.TITLE_DESC)
                {
                    query = query.Where(m => m.TITLE_DESC.Contains(searchModel.TITLE_DESC) && m.OFF_CONTROL_TYPE == controltype);
                }

                if (null != searchModel.DETAIL_DESC && "" != searchModel.DETAIL_DESC)
                {
                    query = query.Where(m => m.DETAIL_DESC.Contains(searchModel.DETAIL_DESC) && m.OFF_CONTROL_TYPE == controltype);
                }

                if (null != searchModel.AUTOGEN_FLAG && searchModel.AUTOGEN_FLAG > 0)
                {
                    query = query.Where(m => m.AUTOGEN_FLAG == searchModel.AUTOGEN_FLAG && m.AUTOGEN_FLAG == searchModel.AUTOGEN_FLAG);
                }

                if (null != searchModel.SHOW_DASHBOARD && searchModel.SHOW_DASHBOARD > 0)
                {
                    query = query.Where(m => m.SHOW_DASHBOARD == searchModel.SHOW_DASHBOARD && m.SHOW_DASHBOARD == searchModel.SHOW_DASHBOARD);
                }

                if (null != searchModel.STATUS_RAW_DATA && searchModel.STATUS_RAW_DATA > 0)
                {
                    query = query.Where(m => m.STATUS_RAW_DATA == searchModel.STATUS_RAW_DATA && m.STATUS_RAW_DATA == searchModel.STATUS_RAW_DATA);
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_RP_OFF_CONTROL> GetAllDashBySearch(QMSDBEntities db, DashOffControlCalAutoSearchModel searchModel)
        {
            List<QMS_RP_OFF_CONTROL> lstResult = new List<QMS_RP_OFF_CONTROL>();

            IQueryable<QMS_RP_OFF_CONTROL> query = db.QMS_RP_OFF_CONTROL.Where(m => m.DELETE_FLAG == 0);

            try
            {
                if (searchModel.AUTOGEN_FLAG != 2)
                {
                    if (null != searchModel.AUTOGEN_FLAG)
                    {
                        query = query.Where(m => m.AUTOGEN_FLAG == searchModel.AUTOGEN_FLAG);
                    }

                }

                if (searchModel.SHOW_DASHBOARD != 2)
                {

                    if (null != searchModel.SHOW_DASHBOARD && searchModel.SHOW_DASHBOARD > 0)
                    {
                        query = query.Where(m => m.SHOW_DASHBOARD == searchModel.SHOW_DASHBOARD);
                    }

                }

                if (null != searchModel.AUTO_OFF_TYPE && searchModel.AUTO_OFF_TYPE > 0)
                {
                    query = query.Where(m => m.STATUS_RAW_DATA == searchModel.AUTO_OFF_TYPE);
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_RP_OFF_CONTROL> GetAllDashByFlag(QMSDBEntities db, DashOffControlCalAutoSearchModel searchModel)
        {
            List<QMS_RP_OFF_CONTROL> lstResult = new List<QMS_RP_OFF_CONTROL>();

            IQueryable<QMS_RP_OFF_CONTROL> query = db.QMS_RP_OFF_CONTROL.Where(m => m.DELETE_FLAG == 0);

            try
            {
                if (searchModel.AUTOGEN_FLAG != 2)
                {
                    if (null != searchModel.AUTOGEN_FLAG)
                    {
                        query = query.Where(m => m.AUTOGEN_FLAG == searchModel.AUTOGEN_FLAG);
                    }

                }

                if (null != searchModel.SHOW_DASHBOARD && searchModel.SHOW_DASHBOARD > 0)
                {
                    query = query.Where(m => m.SHOW_DASHBOARD == searchModel.SHOW_DASHBOARD);
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }


    }

    public partial class QMS_RP_OFF_CONTROL_DETAIL 
    {
        public static List<QMS_RP_OFF_CONTROL_DETAIL> GetAll(QMSDBEntities db)
        {

            return db.QMS_RP_OFF_CONTROL_DETAIL.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_DETAIL> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_RP_OFF_CONTROL_DETAIL.Where(m => m.DELETE_FLAG == 0).OrderBy(m => m.PLANT_ID).ToList(); //linq
        }

        public static QMS_RP_OFF_CONTROL_DETAIL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_RP_OFF_CONTROL_DETAIL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_RP_OFF_CONTROL_DETAIL> GetActiveAllByRP_OFF_CONTROL_ID(QMSDBEntities db, long RPOffControlId)
        {
            return db.QMS_RP_OFF_CONTROL_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControlId).ToList(); //linq
        }

        //public static List<QMS_RP_OFF_CONTROL_DETAIL> GetActiveAllByRP_OFF_CONTROL_IDForDash(QMSDBEntities db, long RPOffControlId)
        //{
        //    List<QMS_RP_OFF_CONTROL_DETAIL> listMappingOrder = new List<QMS_RP_OFF_CONTROL_DETAIL>();
        //    listMappingOrder = db.QMS_RP_OFF_CONTROL_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControlId).ToList();

        //    listMappingOrder.ForEach(a =>
        //    {
        //        string[] tokens = a.EXCEL_NAME.Split('_');
        //        a.EXCEL_NAME = tokens[2];
        //    });
        //    return //linq
        //}
        public static List<QMS_RP_OFF_CONTROL_DETAIL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_OFF_CONTROL_DETAIL.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_DETAIL> GetAllBySearch(QMSDBEntities db, ReportOffControlDetailSearchModel searchModel)
        {
            List<QMS_RP_OFF_CONTROL_DETAIL> lstResult = new List<QMS_RP_OFF_CONTROL_DETAIL>();

            IQueryable<QMS_RP_OFF_CONTROL_DETAIL> query = db.QMS_RP_OFF_CONTROL_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == searchModel.RP_OFF_CONTROL_ID);

            try
            {

                if (null != searchModel.PLANT_ID && searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (null != searchModel.PRODUCT_ID && searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                }

                if (null != searchModel.VOLUME && searchModel.VOLUME >= 0)
                {
                    query = query.Where(m => m.VOLUME == searchModel.VOLUME);
                }

                if (null != searchModel.CONTROL_VALUE && "" != searchModel.CONTROL_VALUE)
                {
                    query = query.Where(m => m.CONTROL_VALUE.Contains(searchModel.CONTROL_VALUE));
                }

                if (null != searchModel.ROOT_CAUSE_ID && searchModel.ROOT_CAUSE_ID > 0)
                {
                    query = query.Where(m => m.ROOT_CAUSE_ID == searchModel.ROOT_CAUSE_ID);
                }


                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }


        public static List<QMS_RP_OFF_CONTROL_DETAIL> GetAllBySearchForDash(QMSDBEntities db, long RPOffControlId, DashOffControlCalAutoSearchModel searchModel)
        {
            List<QMS_RP_OFF_CONTROL_DETAIL> lstResult = new List<QMS_RP_OFF_CONTROL_DETAIL>();

            IQueryable<QMS_RP_OFF_CONTROL_DETAIL> query = db.QMS_RP_OFF_CONTROL_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControlId);

            try
            {

                if (null != searchModel.PLANT_ID && searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (null != searchModel.PRODUCT_ID && searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                //if (null != searchModel.START_DATE && null != searchModel.END_DATE)
                //{
                //    query = query.Where(m => m.START_DATE >= searchModel.START_DATE && m.END_DATE <= searchModel.END_DATE);
                //}


                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

    }

    public partial class QMS_RP_OFF_CONTROL_GRAPH 
    {
        public static List<QMS_RP_OFF_CONTROL_GRAPH> GetAll(QMSDBEntities db)
        {

            return db.QMS_RP_OFF_CONTROL_GRAPH.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_GRAPH> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_RP_OFF_CONTROL_GRAPH.Where(m => m.DELETE_FLAG == 0).OrderBy(m => m.PREVIOUS).ToList(); //linq
        }

        public static QMS_RP_OFF_CONTROL_GRAPH GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_RP_OFF_CONTROL_GRAPH.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_RP_OFF_CONTROL_GRAPH> GetActiveAllByRP_OFF_CONTROL_ID(QMSDBEntities db, long RPOffControlId)
        {
            return db.QMS_RP_OFF_CONTROL_GRAPH.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControlId).ToList(); //linq
        }

        public static List<QMS_RP_OFF_CONTROL_GRAPH> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_OFF_CONTROL_GRAPH.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_GRAPH> GetAllBySearch(QMSDBEntities db, ReportOffControlGraphSearchModel searchModel)
        {
            List<QMS_RP_OFF_CONTROL_GRAPH> lstResult = new List<QMS_RP_OFF_CONTROL_GRAPH>();

            IQueryable<QMS_RP_OFF_CONTROL_GRAPH> query = db.QMS_RP_OFF_CONTROL_GRAPH.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == searchModel.RP_OFF_CONTROL_ID);

            try
            {

                if (null != searchModel.PREVIOUS && "" != searchModel.PREVIOUS)
                {
                    query = query.Where(m => m.PREVIOUS.Contains(searchModel.PREVIOUS));
                }

                if (null != searchModel.OFF_CONTROL && searchModel.OFF_CONTROL > 0)
                {
                    query = query.Where(m => m.OFF_CONTROL == searchModel.OFF_CONTROL);
                }

                if (null != searchModel.POSITION && searchModel.POSITION > 0)
                {
                    query = query.Where(m => m.POSITION == searchModel.POSITION);
                }

                if (null != searchModel.GRAPH_DESC && "" != searchModel.GRAPH_DESC)
                {
                    query = query.Where(m => m.GRAPH_DESC.Contains(searchModel.GRAPH_DESC));
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

    }

    public partial class QMS_RP_OFF_CONTROL_ATTACH 
    {
        public static List<QMS_RP_OFF_CONTROL_ATTACH> GetAll(QMSDBEntities db)
        {

            return db.QMS_RP_OFF_CONTROL_ATTACH.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_ATTACH> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_RP_OFF_CONTROL_ATTACH.Where(m => m.DELETE_FLAG == 0).OrderBy(m => m.NAME).ToList(); //linq
        }

        public static QMS_RP_OFF_CONTROL_ATTACH GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_RP_OFF_CONTROL_ATTACH.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_RP_OFF_CONTROL_ATTACH> GetActiveAllByRP_OFF_CONTROL_ID(QMSDBEntities db, long RPOffControlId)
        {
            return db.QMS_RP_OFF_CONTROL_ATTACH.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControlId).ToList(); //linq
        }

        public static List<QMS_RP_OFF_CONTROL_ATTACH> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_OFF_CONTROL_ATTACH.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_ATTACH> GetAllBySearch(QMSDBEntities db, ReportOffControlAttachSearchModel searchModel)
        {
            List<QMS_RP_OFF_CONTROL_ATTACH> lstResult = new List<QMS_RP_OFF_CONTROL_ATTACH>();

            IQueryable<QMS_RP_OFF_CONTROL_ATTACH> query = db.QMS_RP_OFF_CONTROL_ATTACH.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == searchModel.RP_OFF_CONTROL_ID);

            try
            {

                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    query = query.Where(m => m.NAME.Contains(searchModel.NAME));
                }


                if (null != searchModel.PATH && "" != searchModel.PATH)
                {
                    query = query.Where(m => m.PATH.Contains(searchModel.PATH));
                }

            lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }

    }

    public partial class QMS_RP_OFF_CONTROL_SUMMARY 
    {
        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetAll(QMSDBEntities db)
        {
            return db.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetByListRPOffControlId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => ids.Contains(m.RP_OFF_CONTROL_ID)).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetAllistByRPOffControlProductId(QMSDBEntities db, long RPOffControlId, long productId)
        {
            return db.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControlId && m.PRODUCT_ID == productId).ToList(); //linq
        }

        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetAllByRPOffControlId(QMSDBEntities db, long RPOffControl_id)
        {
            return db.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControl_id).ToList(); //linq
        }

        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetAllOffControlByRPOffControlId(QMSDBEntities db, long RPOffControl_id)
        {
            return db.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControl_id && (m.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL_NO_DATA || m.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL )).ToList(); //linq
        }

        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetAllOffControlByDateAutoGen(QMSDBEntities db, long RPOffControl_id)
        {
            return db.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControl_id && (m.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL_NO_DATA || m.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.OFF_CONTROL)).ToList(); //linq
        }
        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetAllInputVolumeByRPOffControlId(QMSDBEntities db, long RPOffControl_id)
        {
            return db.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.DELETE_FLAG == 0 && m.RP_OFF_CONTROL_ID == RPOffControl_id && (m.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.INPUT_VOLUME_NO_DATA || m.DATA_TYPE == (byte)REPORT_SUMMARY_DATA_TYPE.INPUT_VOLUME)).ToList(); //linq
        }  

        public static QMS_RP_OFF_CONTROL_SUMMARY GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_RP_OFF_CONTROL_SUMMARY> GetAllBySearch(QMSDBEntities db, ReportOffControlSummarySearchModel searchModel)
        {
            List<QMS_RP_OFF_CONTROL_SUMMARY> lstResult = new List<QMS_RP_OFF_CONTROL_SUMMARY>();

            IQueryable<QMS_RP_OFF_CONTROL_SUMMARY> query = db.QMS_RP_OFF_CONTROL_SUMMARY.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (searchModel.PRODUCT_ID > 0)
                {
                    query = query.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID);
                }

                if (searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }
                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        } 
        

    }

    public partial class QMS_RP_TREND_DATA 
    {
        public static List<QMS_RP_TREND_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_RP_TREND_DATA.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_RP_TREND_DATA> GetAllBySearch(QMSDBEntities db, ReportTrendSearchModel searchModel)
        {
            List<QMS_RP_TREND_DATA> lstResult = new List<QMS_RP_TREND_DATA>();

            IQueryable<QMS_RP_TREND_DATA> query = db.QMS_RP_TREND_DATA.Where(m => m.DELETE_FLAG == 0 && m.TREND_TYPE == searchModel.TREND_TYPE);

            try
            {

                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    query = query.Where(m => m.NAME.Contains(searchModel.NAME));
                }

                if (null != searchModel.START_DATE)
                {
                    query = query.Where(m => m.START_DATE >= searchModel.START_DATE);
                }

                if (null != searchModel.END_DATE)
                {
                    query = query.Where( m => m.END_DATE <= searchModel.END_DATE);
                }

                lstResult = query.ToList();
            }
            catch
            {

            }

            return lstResult;
        }

        public static QMS_RP_TREND_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_RP_TREND_DATA.Where(m => m.ID == id && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static List<QMS_RP_TREND_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_TREND_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }
    }

    public partial class QMS_RP_TREND_DATA_DETAIL 
    {
        public static List<QMS_RP_TREND_DATA_DETAIL> GetAll(QMSDBEntities db)
        {
            return db.QMS_RP_TREND_DATA_DETAIL.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_RP_TREND_DATA_DETAIL> GetAllByReportId(QMSDBEntities db, long id)
        {
            return db.QMS_RP_TREND_DATA_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.TREND_DATA_ID == id).ToList();
        }

        public static QMS_RP_TREND_DATA_DETAIL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_RP_TREND_DATA_DETAIL.Where(m => m.ID == id && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static List<QMS_RP_TREND_DATA_DETAIL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_RP_TREND_DATA_DETAIL.Where(m => ids.Contains(m.ID)).ToList();
        }
    }

}
