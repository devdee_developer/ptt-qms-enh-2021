USE [PTT-GSPQualityManagementSystem_test]
GO
/****** Object:  Table [dbo].[QMS_TR_IQC_CONTROLRULE]    Script Date: 16-Oct-20 10:21:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QMS_TR_IQC_CONTROLRULE](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[METHOD_NAME] [nvarchar](150) NOT NULL,
	[METHOD_DESC] [nvarchar](250) NULL,
	[POSITION] [smallint] NOT NULL,
	[DELETE_FLAG] [tinyint] NOT NULL,
	[CREATE_USER] [nvarchar](50) NOT NULL,
	[CREATE_DATE] [datetime] NOT NULL,
	[UPDATE_USER] [nvarchar](50) NOT NULL,
	[UPDATE_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_QMS_TR_IQC_CONTROLRULE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

