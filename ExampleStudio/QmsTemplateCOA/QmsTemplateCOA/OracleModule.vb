﻿Imports Oracle.DataAccess.Client

Module OracleModule
    
    Public Function GetOrcacleDBConnection() As OracleConnection
        Dim host As String = "172.20.133.27"
        Dim port As Integer = 1521
        Dim sid As String = "gsplims"
        Dim user As String = "gspresult"
        Dim password As String = "gspresult1"
        Dim Conn As OracleConnection = New OracleConnection()

        Conn = DBOracleUtils.GetDBConnection(host, port, sid, user, password)
        Return Conn
    End Function

    Public Function getViewDataSQL() As String
        Return "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT" 
    End Function

    Public Function getViewDataSQLByDate(stdate As String, endate As String) As String
        Return "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT  WHERE   SAMPLINGDATE >= to_date('" + stdate + "','DD-MM-YYYY') and SAMPLINGDATE <= to_date('" + endate + "','DD-MM-YYYY')"
    End Function

    Public Function getViewDataSQLMaster(stdate As String, endate As String) As String
        Return "SELECT * FROM   SAPPHIRE.V_TEMPLATE_MASTER  WHERE   CREATEDT >= to_date('" + stdate + "','DD-MM-YYYY') and CREATEDT <= to_date('" + endate + "','DD-MM-YYYY')"
    End Function

    Public Function getViewDataSQLDetail(stdate As String, endate As String) As String
        Return "SELECT * FROM   SAPPHIRE.V_TEMPLATE_DETAIL  WHERE   CREATEDT >= to_date('" + stdate + "','DD-MM-YYYY') and CREATEDT <= to_date('" + endate + "','DD-MM-YYYY')"
    End Function

    Public Function getViewCustomer() As String
        Return "SELECT * FROM SAPPHIRE.V_COA_PRODUCT_CUSTOMER "
    End Function

End Module
