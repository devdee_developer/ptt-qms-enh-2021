﻿Imports System.Configuration
Imports System.Data.Entity.Core.EntityClient
Imports System.Data.SqlClient
Imports System.IO
Imports System.Threading
Imports QMSSystem.CoreDB.Services
Imports QMSSystem.Model

Public Class QmsTemplateCSCServices
    Private Schedular As Timer
    Private m_nCount As Integer = 3
    Private m_bError As Boolean = False
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Me.WriteToFile("Service started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.ScheduleService()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Me.WriteToFile("Service stopped at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.Schedular.Dispose()
    End Sub
    Private Sub SchedularCallback(e As Object)
        Me.WriteToFile("Start : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Try
            Dim activeDate As DateTime = Date.Now
            Dim startDate As DateTime
            Dim endDate As DateTime 'start date
            startDate = New DateTime(activeDate.Year, activeDate.Month, activeDate.Day, 0, 0, 0).AddDays(-1)
            endDate = startDate.AddDays(1).AddMilliseconds(-1)

            Try
                loadProductQualityToCSC(startDate, endDate)
            Catch ex As Exception
                Me.WriteToFile(ex.Message)
                If ex.InnerException IsNot Nothing Then
                    Me.WriteToFile(ex.InnerException.Message)
                End If

            End Try

            Threading.Thread.Sleep(5000)

            Threading.Thread.Sleep(5000)

        Catch ex As Exception
            Me.WriteToFile(ex.Message)
        End Try
        Me.WriteToFile("End : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.ScheduleService()
    End Sub
    Public Sub loadProductQualityToCSC(startDate As DateTime, endDate As DateTime)

        Try
            Me.WriteToFile("Start function .. loadProductQualityToCSC")
            Dim transServices As New TransactionService("Service")
            'Dim connectionString As String
            'connectionString = GetConnectionString()
            Dim db As New QMSSystem.CoreDB.QMSDBEntities() 'QMSSystem.CoreDB.QMSDBEntities(connectionString)
            Dim coaService = New COAServices(db, "Service")

            Dim startTime As DateTime = startDate
            Dim endTime As DateTime = startDate.AddDays(1)
            Me.WriteToFile("startTime " + Convert.ToString(startTime))
            Do While (startTime <= endDate)
                endTime = startTime.AddDays(1)
                coaService.DailyCOAWriteProductQualityToCSC(startTime, endTime, False)
                startTime = startTime.AddDays(1)
            Loop
            Me.WriteToFile("endTime " + Convert.ToString(endTime))
        Catch ex As Exception
            Me.WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)
        End Try
    End Sub
    Private Sub WriteToFile(text As String)
        Dim dirName As String
        Dim path As String
        Try
            dirName = AppDomain.CurrentDomain.BaseDirectory & "\\servicelogs"

            If (Not System.IO.Directory.Exists(dirName)) Then
                System.IO.Directory.CreateDirectory(dirName)
            End If

            path = dirName & "\\" & DateTime.Now.ToString("yyyyMMdd") & ".log"

            Using writer As New StreamWriter(path, True)
                writer.WriteLine(text)
                writer.Close()
            End Using
        Catch
        End Try

    End Sub
    Public Sub ScheduleService()
        Try
            'System.Diagnostics.Debugger.Launch()
            Schedular = New Timer(New TimerCallback(AddressOf SchedularCallback))
            Dim mode As String = ConfigurationManager.AppSettings("Mode").ToUpper()
            Me.WriteToFile((Convert.ToString("Service Mode: ") & mode) + " {0}")
            m_nCount = 3
            'Set the Default Time.
            Dim scheduledTime As DateTime = DateTime.MinValue
            If mode = "DAILY" Then
                'Get the Scheduled Time from AppSettings.
                'System.Diagnostics.Debugger.Launch()
                scheduledTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings("ScheduledTime"))
                Try
                    Dim connectionString As String
                    Me.WriteToFile("Before get conn")
                    Dim cogService = New ConfigServices("Service")
                    Dim createTemplateConnect = New CreateQMS_ST_TEMPLATE_CONNECT()
                    Dim db As New QMSSystem.CoreDB.QMSDBEntities()
                    createTemplateConnect = cogService.getTEMP_SCHEDULE(db)
                    Me.WriteToFile("After get conn")
                    If createTemplateConnect.TEMPLATE_SCHEDULE.HasValue Then
                        Me.WriteToFile(createTemplateConnect.TEMPLATE_SCHEDULE.Value.ToString())
                        scheduledTime = New DateTime(scheduledTime.Year, scheduledTime.Month, scheduledTime.Day, createTemplateConnect.TEMPLATE_SCHEDULE.Value.Hour, createTemplateConnect.TEMPLATE_SCHEDULE.Value.Minute, 0)
                        scheduledTime = scheduledTime.AddHours(1)
                    End If
                Catch ex As Exception
                    WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)
                End Try
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next day.
                    scheduledTime = scheduledTime.AddDays(1)
                End If
            End If
            If mode.ToUpper() = "INTERVAL" Then
                'Get the Interval in Minutes from AppSettings.
                Dim intervalMinutes As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))
                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(intervalMinutes)
                End If
            End If
            'scheduledTime = New DateTime(2022, 2, 11, 21, 52, 0)
            Dim ScheduledDelayTime As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("ScheduledDelayTime"))
            If (ScheduledDelayTime > 0) Then
                scheduledTime = scheduledTime.AddHours(ScheduledDelayTime)
            End If
            Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
            Dim schedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s) ScheduledDelayTime : {4}", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, ScheduledDelayTime)
            Me.WriteToFile((Convert.ToString("1Service scheduled to run after: ") & schedule) + " {0}")
            'Get the difference in Minutes between the Scheduled and Current Time.
            Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)
            'Change the Timer's Due Time.
            Schedular.Change(dueTime, Timeout.Infinite)
        Catch ex As Exception
            WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)
            'Stop the Windows Service.
            Using serviceController As New System.ServiceProcess.ServiceController("SimpleService")
                serviceController.[Stop]()
            End Using
        End Try
    End Sub
End Class
