﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace QMSSystem.Model
{
    class TransactionModels
    {
    }

    //-----QMS_TR_ABNORMAL
    public class CreateQMS_TR_ABNORMAL
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; } 
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewQMS_TR_ABNORMAL
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CORRECT_DATA_TYPE_NAME { get; set; }
        public string CAUSE_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
    
    //-----QMS_TR_CHANGE_GRADE
    public class CreateQMS_TR_CHANGE_GRADE
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long GRADE_FROM { get; set; }
        public long GRADE_TO { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public bool GRADE_CHECK { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewQMS_TR_CHANGE_GRADE
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long GRADE_FROM { get; set; }
        public string GRADE_FROM_NAME { get; set; }
        public long GRADE_TO { get; set; }
        public string GRADE_TO_NAME { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CORRECT_DATA_TYPE_NAME { get; set; }
        public string CAUSE_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
    
    //-----QMS_TR_COA_DATA
    public class CreateQMS_TR_COA_DATA
    {
        public long ID { get; set; }

        public DateTime SAMPLING_DATE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string SAMPLING_NAME { get; set; }
        public string SAMPLING_POINT { get; set; }
        public string PRODUCT_NAME { get; set; }
         

        public string TANK_NAME { get; set; }
        public string ITEM_NAME { get; set; }
        public string UNIT_NAME { get; set; }
        public string TEST_METHOD { get; set; }
        public string SPECIFICATION { get; set; }
        public string DISPLAY_VALUE { get; set; }
        public decimal ENTERED_VALUE { get; set; } 
      
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_COA_DATA
    {
        public long ID { get; set; }
        public DateTime CREATEDT { get; set; }
        public DateTime SAMPLING_DATE { get; set; }
        public string COA_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string ITEM { get; set; }
        public string DISPLAY_VALUE { get; set; }
        public decimal ENTERED_VALUE { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    //-----QMS_TR_CROSS_VOLUME
    public class CreateQMS_TR_CROSS_VOLUME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
       // public long PRODUCT_FROM { get; set; }
        public long PRODUCT_TO { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public bool CROSS_VOLUME_CHECK { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; } 
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewQMS_TR_CROSS_VOLUME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long PRODUCT_FROM { get; set; }
        public string PRODUCT_FROM_NAME { get; set; }
        public long PRODUCT_TO { get; set; }
        public string PRODUCT_TO_NAME { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CORRECT_DATA_TYPE_NAME { get; set; }
        public string CAUSE_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
    
    //-----QMS_TR_DOWNTIME
    public class CreateQMS_TR_DOWNTIME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public long? BLUEPRINT_ID { get; set; }
        public long? DATALAKE_ID { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; } 
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewQMS_TR_DOWNTIME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CORRECT_DATA_TYPE_NAME { get; set; }
        public string CAUSE_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public long? BLUEPRINT_ID { get; set; }
        public long? DATALAKE_ID { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
    
    //-----QMS_TR_EXCEPTION
    public class CreateQMS_TR_EXCEPTION
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewQMS_TR_EXCEPTION
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CORRECT_DATA_TYPE_NAME { get; set; }
        public string CAUSE_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    //-----QMS_TR_FILE_TREND_DATA
    public class CreateQMS_TR_FILE_TREND_DATA
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string NAME { get; set; }
        public DateTime UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_FILE_TREND_DATA
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string TEMPLATE_NAME { get; set; }
        public string NAME { get; set; }
        public bool ITEM_SELECTED { get; set; }
    }

    //-----QMS_TR_OFF_CONTROL_CAL
    public class CreateQMS_TR_OFF_CONTROL_CAL
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public string CONTROL_VALUE { get; set; }
        public long ROOT_CAUSE_ID { get; set; }
        public string OFF_CONTROL_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }
        public long CROSS_VOLUME_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_OFF_CONTROL_CAL
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public decimal VOLUME { get; set; }
        public string CONTROL_VALUE { get; set; }
        public long ROOT_CAUSE_ID { get; set; }
        public string ROOT_CAUSE_NAME { get; set; }
        public string OFF_CONTROL_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public long CROSS_VOLUME_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    //-----QMS_TR_PRODUCT_DATA
    public class CreateQMS_TR_PRODUCT_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public long EXA_TAG_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_DATA { get; set; }
        public decimal TAG_VALUE { get; set; }
        public decimal TAG_CONVERT { get; set; }
        public byte OFF_CONTROL_FLAG_0 { get; set; }
        public byte OFF_SPEC_FLAG_0 { get; set; }
        public byte TURN_AROUND_FLAG_0 { get; set; }
        public byte DOWNTIME_FLAG_0 { get; set; }
        public byte CHANGE_GRADE_FLAG_0 { get; set; }
        public byte CROSS_VOLUME_FLAG_0 { get; set; }
        public byte GC_ERROR_FLAG_0 { get; set; }
        public byte REDUCE_FEED_FLAG_0 { get; set; }
        public byte OFF_CONTROL_FLAG { get; set; }
        public byte OFF_SPEC_FLAG { get; set; }
        public byte TURN_AROUND_FLAG { get; set; }
        public byte DOWNTIME_FLAG { get; set; }
        public byte CHANGE_GRADE_FLAG { get; set; }
        public byte CROSS_VOLUME_FLAG { get; set; }
        public byte GC_ERROR_FLAG { get; set; }
        public byte REDUCE_FEED_FLAG { get; set; }
        public byte EXCEPTION_FLAG { get; set; }
        public byte EXCEPTION_VALUE { get; set; }
        
    }

    public class ViewQMS_TR_PRODUCT_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public long EXA_TAG_ID { get; set; }
        public string EXA_TAG_NAME { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_DATA { get; set; }
        public decimal TAG_VALUE { get; set; }
        public decimal TAG_CONVERT { get; set; }
        public byte OFF_CONTROL_FLAG_0 { get; set; }
        public byte OFF_SPEC_FLAG_0 { get; set; }
        public byte TURN_AROUND_FLAG_0 { get; set; }
        public byte DOWNTIME_FLAG_0 { get; set; }
        public byte CHANGE_GRADE_FLAG_0 { get; set; }
        public byte CROSS_VOLUME_FLAG_0 { get; set; }
        public byte GC_ERROR_FLAG_0 { get; set; }
        public byte REDUCE_FEED_FLAG_0 { get; set; }
        public byte OFF_CONTROL_FLAG { get; set; }
        public byte OFF_SPEC_FLAG { get; set; }
        public byte TURN_AROUND_FLAG { get; set; }
        public byte DOWNTIME_FLAG { get; set; }
        public byte CHANGE_GRADE_FLAG { get; set; }
        public byte CROSS_VOLUME_FLAG { get; set; }
        public byte GC_ERROR_FLAG { get; set; }
        public byte REDUCE_FEED_FLAG { get; set; }
        public byte EXCEPTION_FLAG { get; set; }
        public byte EXCEPTION_VALUE { get; set; }
        public bool ITEM_SELECTED { get; set; }
    }
   
    //-----QMS_TR_PRODUCT_DOWNTIME_DATA
    public class CreateQMS_TR_PRODUCT_DOWNTIME_DATA
    {
        public long ID { get; set; }
        public long DOWNTIME_DETAIL_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public decimal TAG_VALUE { get; set; }
        public decimal TAG_CONVERT { get; set; }
        public byte DOWNTIME_FLAG_0 { get; set; }
        public byte DOWNTIME_FLAG { get; set; }
    }

    public class ViewQMS_TR_PRODUCT_DOWNTIME_DATA
    {
        public long ID { get; set; }
        public long DOWNTIME_DETAIL_ID { get; set; }
        public string DOWNTIME_NAME { get; set; }
        public DateTime TAG_DATE { get; set; }
        public decimal TAG_VALUE { get; set; }
        public byte DOWNTIME_FLAG_0 { get; set; }
        public byte DOWNTIME_FLAG { get; set; }
        public bool ITEM_SELECTED { get; set; }
    }
   
    //-----QMS_TR_RAW_DATA
    public class CreateQMS_TR_RAW_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime DATE_START { get; set; }
        public DateTime DATE_END { get; set; }
        public string FILE_PATH { get; set; }
        public string RAW_DATA_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

          
        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_RAW_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public DateTime DATE_START { get; set; }
        public DateTime DATE_END { get; set; }
        public string FILE_PATH { get; set; }
        public string RAW_DATA_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
   
    //-----QMS_TR_REDUCE_FEED
    public class CreateQMS_TR_REDUCE_FEED
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public long? BLUEPRINT_ID { get; set; }
        public long? DATALAKE_ID { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; } 
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewQMS_TR_REDUCE_FEED
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CORRECT_DATA_TYPE_NAME { get; set; }
        public string CAUSE_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public long? BLUEPRINT_ID { get; set; }
        public long? DATALAKE_ID { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
    
    //-----QMS_TR_REDUCE_FEED_DATA
    public class CreateQMS_TR_REDUCE_FEED_DATA
    {
        public long ID { get; set; }
        public long REDUCE_FEED_DETAIL_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public decimal TAG_VALUE { get; set; }
        public decimal TAG_CONVERT { get; set; }
        public byte REDUCE_FEED_FLAG_0 { get; set; }
        public byte REDUCE_FEED_FLAG { get; set; }
    }

    public class ViewQMS_TR_REDUCE_FEED_DATA
    {
        public long ID { get; set; }
        public long REDUCE_FEED_DETAIL_ID { get; set; }
        public string REDUCE_FEED_NAME { get; set; }
        public DateTime TAG_DATE { get; set; }
        public decimal TAG_VALUE { get; set; }
        public byte REDUCE_FEED_FLAG_0 { get; set; }
        public byte REDUCE_FEED_FLAG { get; set; }
        public bool ITEM_SELECTED { get; set; }
    }
    
    //-----QMS_TR_TEMPLATE_COA
    public class CreateQMS_TR_TEMPLATE_COA
    {
        public long ID { get; set; }
        public string COA_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }

        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_VIEW { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

        public byte TREND_GRAPH_FLAG { get; set; }
        public string TREND_GRAPH_FLAG_VIEW { get; set; }
        public long? CUSTOMER_MERGE_ID { get; set; }
    }


    public class ViewQMS_TR_TEMPLATE_COA
    {
        public long ID { get; set; }

        public string CUSTOMER_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string COA_ID { get; set; }

        //public string PRODUCT_ID { get; set; }
        //public string CUSTOMER_ID { get; set; }       
        //public int COA_DT_COUNT { get; set; }
        //public string TANK_NAME { get; set; }
         
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public byte TREND_GRAPH_FLAG { get; set; }
        public string TREND_GRAPH_FLAG_VIEW { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        public long? CUSTOMER_MERGE_ID { get; set; }
    }
    
    //-----QMS_TR_TEMPLATE_COA_DETAIL
    public class CreateQMS_TR_TEMPLATE_COA_DETAIL
    {
        public long ID { get; set; }
        public string COA_ID { get; set; }

        public string CUSTOMER_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string TANK_NAME { get; set; }
        public string ITEM_NAME { get; set; }
        public string UNIT_NAME { get; set; }

        public int USER_SEQUENCE { get; set; }
        public string TEST_METHOD { get; set; }
        public string SPECIFICATION { get; set; } 
        public long CONTROL_ID { get; set; }

        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

        public byte TREND_GRAPH_FLAG { get; set; }
        public string TREND_GRAPH_FLAG_VIEW { get; set; }
        public long? PRODUCT_MERGE_ID { get; set; }

    }

    public class ViewQMS_TR_TEMPLATE_COA_DETAIL
    {
        public bool ITEM_SELECTED { get; set; }

        public long ID { get; set; }
        public string COA_ID { get; set; }

        public string CUSTOMER_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string TANK_NAME { get; set; }
        public string ITEM_NAME { get; set; }
        public string UNIT_NAME { get; set; }

        public int USER_SEQUENCE { get; set; }
        public string TEST_METHOD { get; set; }
        public string SPECIFICATION { get; set; }
        public long CONTROL_ID { get; set; }

        public long CONTROL_GROUP_ID { get; set; }
        public long CONTROL_GROUP_SMAPLE_ID { get; set; }
        public long CONTROL_GROUP_ITEM_ID { get; set; }

        public int test { get; set; }

        public byte TREND_GRAPH_FLAG { get; set; }
        public string TREND_GRAPH_FLAG_VIEW { get; set; }
        public long? PRODUCT_MERGE_ID { get; set; }
        public string DISPLAY_NAME { get; set; }
        public string CONTROL_VALUE { get; set; }
        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
    
    //-----QMS_TR_TREND_DATA
    public class CreateQMS_TR_TREND_DATA
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public long SAMPLE_ID { get; set; }
        public long ITEM_ID { get; set; }
        public DateTime RECORD_DATE { get; set; }
        public byte CONC_LOADING_FLAG { get; set; }
        public string DATA_VALUE_TEXT { get; set; }
        public string DATA_SIGN_1 { get; set; }
        public decimal DATA_VALUE_1 { get; set; }
        public string DATA_SIGN_2 { get; set; }
        public decimal DATA_VALUE_2 { get; set; }
        public string DESCRIPTION { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    //public class GRAPH_TREDN_GAS_DATA
    //{
    //    public DateTime Trend_DATE { get; set; }
    //    public string SHOW_DATE { get; set; } 

    //    public string CUSTOMER_NAME { get; set; }
    //    public string PRODUCT_NAME { get; set; }
    //    public string ITEM { get; set; }

    //    public string DATA_SIGN { get; set; }
    //    public decimal? DISPLAY_VALUE { get; set; }
    //    public decimal DATA_VALUE { get; set; }
    //}

    public class GRAPH_TREND_GAS_DATA
    {
        public DateTime Trend_DATE { get; set; }
        public string SHOW_DATE { get; set; }

        public string CUSTOMER_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
       // public string ITEM { get; set; } 

        public string TEMPLATE_NAME { get; set; }
        public string SAMPLE_NAME { get; set; }
        public string ITEM_NAME { get; set; }

        public string DATA_SIGN { get; set; }
        public string DISPLAY_VALUE { get; set; }
        public string DESCRIPTION { get; set; }
        public decimal? DATA_VALUE { get; set; }
    }

    public class ViewQMS_TR_TREND_DATA
    {
        public long ID { get; set; }
        public long TEMPLATE_ID { get; set; }
        public string TEMPLATE_NAME { get; set; }
        public long SAMPLE_ID { get; set; }
        public string SAMPLE_NAME { get; set; }
        public long ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public DateTime RECORD_DATE { get; set; }
        public byte CONC_LOADING_FLAG { get; set; }
        public string DATA_VALUE_TEXT { get; set; } 

        public string DATA_SIGN_1 { get; set; }
        public decimal DATA_VALUE_1 { get; set; }
        public string DATA_SIGN_2 { get; set; }
        public decimal DATA_VALUE_2 { get; set; }

        public string SHOW_DATE { get; set; } 

        public long CONTROL_ID { get; set; }
        public decimal CONTROL_VALUE { get; set; }
        public long UNIT_ID { get; set; }
        public string UNIT_NAME { get; set; }

        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }
    
    //-----QMS_TR_TURN_AROUND
    public class CreateQMS_TR_TURN_AROUND
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CAUSE_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_TURN_AROUND
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long CORRECT_DATA_TYPE { get; set; }
        public string CORRECT_DATA_TYPE_NAME { get; set; }
        public string CAUSE_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    //-----QMS_TR_EXQ_DOWNTIME_DATA
    public class CreateQMS_TR_EXQ_DOWNTIME_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; } 
        public DateTime TAG_DATE { get; set; }
        public string TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    public class ViewQMS_TR_EXQ_DOWNTIME_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }


    //-----QMS_TR_EXQ_REDUCE_FEED_DATA
    public class CreateQMS_TR_EXQ_REDUCE_FEED_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    public class ViewQMS_TR_EXQ_REDUCE_FEED_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    //-----QMS_TR_EXQ_PRODUCT_DATA
    public class CreateQMS_TR_EXQ_PRODUCT_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    public class ViewQMS_TR_EXQ_PRODUCT_DATA
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    //-----QMS_TR_EXQ_DOWNTIME
    public class CreateQMS_TR_EXQ_DOWNTIME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public long TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    public class ViewQMS_TR_EXQ_DOWNTIME
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public long TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }


    //-----QMS_TR_EXQ_REDUCE_FEED
    public class CreateQMS_TR_EXQ_REDUCE_FEED
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public long TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    public class ViewQMS_TR_EXQ_REDUCE_FEED
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public long TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }
    }

    //-----QMS_TR_EXQ_PRODUCT
    public class CreateQMS_TR_EXQ_PRODUCT
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public long TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }


        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_EXQ_PRODUCT
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public long TAG_EXA_ID { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_CONVERT { get; set; }
        public string TAG_GCERROR { get; set; }
        public byte STATUS_1 { get; set; }
        public byte STATUS_2 { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class CAL_OFF_CONTROL
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public DateTime TAG_DATE { get; set; } 
        public string TAG_VOLUME { get; set; }
        public string TAG_GCERROR { get; set; }
        public string OFF_CONTROL_NAME { get; set; }
        public string OFF_SPEC_NAME { get; set; } 
    }

    public class CAL_CHANGE_GRADE_OFF_CONTROL
    {
        public List<CAL_OFF_CONTROL> normalCal { get; set; }
        public List<CAL_OFF_CONTROL> secondCal { get; set; }
    }

    public class CAL_DOWNTIME_DATA
    {
        public long PLANT_ID { get; set; }
        //public long DOWNTIME_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_GCERROR { get; set; } 
    }

    public class CAL_REDUCE_FEED_DATA
    {
        public long PLANT_ID { get; set; }
        //public long DOWNTIME_ID { get; set; }
        public DateTime TAG_DATE { get; set; }
        public string TAG_VALUE { get; set; }
        public string TAG_GCERROR { get; set; }
    }

    //-----QMS_TR_BLUEPRINT_ABNORMAL
    public class CreateQMS_TR_BLUEPRINT_ABNORMAL
    {
        public long BlueprintId { get; set; }
        public string Plant { get; set; }
        public string AbnormalType { get; set; }
        public int? EventId { get; set; }
        public string InputSource { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public float? TotalTimeHr { get; set; }
        public string Cause { get; set; }
        public string ShortCause { get; set; }
        public bool? Trip { get; set; }
        public bool? EquipmentFailure { get; set; }
        public string SD_Category { get; set; }
        public string Define { get; set; }
        public string SD_Reason { get; set; }
        public string EquipmentCode { get; set; }
        public string Source { get; set; }
        public string PlanType { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }



    //-----QMS_TR_IQC_CONTROL
    public class CreateQMS_TR_IQC_CONTROL
    {
        public long ID { get; set; }
        public DateTime SENDMAIL_DATE { get; set; }
        public long CONTROL_ID { get; set; }
        public long RULE_ID { get; set; }
        public DateTime START_POINT { get; set; }
        public DateTime END_POINT { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string CONTROL_REASON { get; set; }
        public string CONTROL_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }


        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_IQC_CONTROL
    {
        public long ID { get; set; }
        public DateTime SENDMAIL_DATE { get; set; }
        public long CONTROL_ID { get; set; }
        public long RULE_ID { get; set; }
        public string RULE_NAME { get; set; }
        public DateTime START_POINT { get; set; }
        public DateTime END_POINT { get; set; }
        public long POSSICAUSE_ID { get; set; }
        public string POSSICAUSE_NAME { get; set; }
        public string CONTROL_REASON { get; set; }
        public string CONTROL_DESC { get; set; }
        public byte DELETE_FLAG { get; set; }

    }

    //-----QMS_TR_IQC_CONTROL
    public class CreateQMS_TR_IQC_CONFIG
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public decimal CONFIG_SAMPLE { get; set; }
        public decimal CONFIG_STD { get; set; }
        public decimal CONFIG_AVG { get; set; }
        public decimal CONFIG_TOLERANCE { get; set; }
        public byte DELETE_FLAG { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class ViewQMS_TR_IQC_CONFIG
    {
        public long ID { get; set; }
        public long CONTROL_ID { get; set; }
        public decimal CONFIG_SAMPLE { get; set; }
        public decimal CONFIG_STD { get; set; }
        public decimal CONFIG_AVG { get; set; }
        public decimal CONFIG_TOLERANCE { get; set; }
        public byte DELETE_FLAG { get; set; }

    }
}
