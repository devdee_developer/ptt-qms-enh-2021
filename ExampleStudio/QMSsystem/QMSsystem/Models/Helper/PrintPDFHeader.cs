﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace QMSsystem.Models
{
    public class PrintPDFHeader : PdfPageEventHelper
    {

        /*
        * We use a __single__ Image instance that's assigned __once__;
        * the image bytes added **ONCE** to the PDF file. If you create 
        * separate Image instances in OnEndPage()/OnEndPage(), for example,
        * you'll end up with a much bigger file size.
        */


        /**** image ****/
        public Image ImageHeader { get; set; }
        public float ScalePercent { get; set; }
        public float xImage { get; set; }
        public float yImage { get; set; }

        public BaseFont bsFont { get; set; }
        public Font fontHeader { get; set; }
        public Paragraph infoHeader { get; set; }
        public PdfPTable head { get; set; }

        protected PdfTemplate total;
        private float ajFooterPage;


        public PrintPDFHeader()
        {
            ImageHeader = null;
            fontHeader = new Font(Font.FontFamily.COURIER, 14);
            head = null;
            infoHeader = new Paragraph("");
            ajFooterPage = 40f;
        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            total = writer.DirectContent.CreateTemplate(200, 100);
            total.BoundingBox = new Rectangle(-20, -20, 200, 100);
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            total.BeginText();
            total.SetFontAndSize(bsFont, 12);
            total.SetTextMatrix(0, 0);
            int pageNumber = writer.PageNumber;
            total.ShowText(Convert.ToString(pageNumber));
            total.EndText();
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {

            /************ Set Header Page **************/

            // cell height 
            float cellHeight = document.TopMargin;
            // PDF document size      
            Rectangle page = document.PageSize;

            //if (ImageHeader != null)
            //{
            //    ImageHeader.SetAbsolutePosition(xImage, yImage);
            //    ImageHeader.ScalePercent(ScalePercent);
            //    document.Add(ImageHeader);
            //} 

            if (head != null)
            {
                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                    // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  writer.DirectContent
                );
            }
            /************ Set Header Page **************/



            /************ Set Footer Page **************/

            float textBase = document.Bottom - 10;// -20;

            PdfContentByte cb = writer.DirectContent;
            string text = "Page " + writer.PageNumber + " of ";
            cb.SaveState();
            cb.BeginText();
            cb.SetFontAndSize(bsFont, 12);

            float adjust = bsFont.GetWidthPoint("0", 12);
            cb.SetTextMatrix(document.Right - ajFooterPage - (adjust - 3), textBase);
            cb.ShowText(text);
            cb.EndText();
            cb.AddTemplate(total, document.Right - adjust, textBase);

            cb.RestoreState();

            /************ Set Footer Page **************/
        }

    } 
}