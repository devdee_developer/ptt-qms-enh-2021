﻿Imports System.ServiceProcess
Imports System.Reflection

Module Service
    'Service Control Manager object specific access types
    Private Const STANDARD_RIGHTS_REQUIRED As Integer = &HF0000
    Private Const SC_MANAGER_CONNECT As Integer = &H1
    Private Const SC_MANAGER_CREATE_SERVICE As Integer = &H2
    Private Const SC_MANAGER_ENUMERATE_SERVICE As Integer = &H4
    Private Const SC_MANAGER_LOCK As Integer = &H8
    Private Const SC_MANAGER_QUERY_LOCK_STATUS As Integer = &H10
    Private Const SC_MANAGER_MODIFY_BOOT_CONFIG As Integer = &H20
    Private Const SC_MANAGER_ALL_ACCESS As Integer =
    (STANDARD_RIGHTS_REQUIRED Or SC_MANAGER_CONNECT Or
    SC_MANAGER_CREATE_SERVICE Or SC_MANAGER_ENUMERATE_SERVICE Or
    SC_MANAGER_LOCK Or SC_MANAGER_QUERY_LOCK_STATUS Or
    SC_MANAGER_MODIFY_BOOT_CONFIG)

    'Service Access types
    Private Const SERVICE_QUERY_CONFIG As Integer = &H1
    Private Const SERVICE_CHANGE_CONFIG As Integer = &H2
    Private Const SERVICE_QUERY_STATUS As Integer = &H4
    Private Const SERVICE_ENUMERATE_DEPENDENTS As Integer = &H8
    Private Const SERVICE_START As Integer = &H10
    Private Const SERVICE_STOP As Integer = &H20
    Private Const SERVICE_PAUSE_CONTINUE As Integer = &H40
    Private Const SERVICE_INTERROGATE As Integer = &H80
    Private Const SERVICE_USER_DEFINED_CONTROL As Integer = &H100
    Private Const SERVICE_ALL_ACCESS As Integer = (STANDARD_RIGHTS_REQUIRED Or SERVICE_QUERY_CONFIG Or SERVICE_CHANGE_CONFIG Or SERVICE_QUERY_STATUS Or _
SERVICE_ENUMERATE_DEPENDENTS Or SERVICE_START Or SERVICE_STOP Or
SERVICE_PAUSE_CONTINUE Or SERVICE_INTERROGATE Or
SERVICE_USER_DEFINED_CONTROL)

    Private Const SERVICE_WIN32_OWN_PROCESS As Integer = &H10
    Private Const SERVICE_AUTO_START As Integer = &H2
    Private Const SERVICE_ERROR_NORMAL As Integer = &H1

    Declare Auto Function OpenSCManager Lib "advapi32.dll" (ByVal sMachName As String, ByVal sDbName As String, ByVal iAccess As Integer) As Integer
    Declare Auto Function CloseServiceHandle Lib "advapi32.dll" (ByVal sHandle As Integer) As Integer
    Declare Auto Function CreateService Lib "advapi32.dll" (ByVal hSCM As Integer, ByVal sName As String, ByVal sDisplay As String, ByVal iAccess As _
    Integer, ByVal iSvcType As Integer, ByVal iStartType As Integer, ByVal _
    iError As Integer, ByVal sPath As String, ByVal sGroup As String, ByVal _
    iTag As Integer, ByVal sDepends As String, ByVal sUser As String, ByVal _
    sPass As String) As Integer
    Declare Auto Function OpenService Lib "advapi32.dll" (ByVal hSCManager As Integer, ByVal lpServiceName As String, ByVal dwDesiredAccess As _
Integer) As Integer
    Declare Auto Function DeleteService Lib "advapi32.dll" (ByVal hSvc As Integer) As Boolean
    Declare Auto Function GetLastError Lib "KERNEL32" () As Integer

    Private Const ServiceName As String = "DevDeeQMSLoadOffControlServices"
    Private Const DisplayName As String = "DevDeeQMS Load OffControl Services"

    Public Sub InstallService()

        If ServiceInstalled() Then
            Exit Sub
        End If

        Dim hSCM As Integer = OpenSCManager(Nothing, Nothing,
        SC_MANAGER_ALL_ACCESS)
        If hSCM = 0 Then
            MsgBox("InstallService: Unable to open Service Control Manager.Error: " & GetLastError().ToString)
            Exit Sub
        End If

        Dim sExeName As String = [Assembly].GetExecutingAssembly.Location

        Dim hSvc As Integer = CreateService(hSCM, ServiceName, DisplayName,
        SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START,
        SERVICE_ERROR_NORMAL, sExeName, Nothing, Nothing, Nothing, Nothing,
        Nothing)
        If hSvc = 0 Then
            CloseServiceHandle(hSvc)
            MsgBox("Unable to install service, could not create service handle. Error: " & GetLastError().ToString)
            Exit Sub
        End If

        CloseServiceHandle(hSvc)
        CloseServiceHandle(hSCM)

        MsgBox("Install Success")

    End Sub

    Public Sub UninstallService()

        If Not ServiceInstalled() Then
            Exit Sub
        End If

        If Not StopService() Then
            MsgBox("UninstallService: Could not stop service. Error: " &
            GetLastError().ToString)
            Exit Sub
        End If

        Dim hSCM As Integer = OpenSCManager(Nothing, Nothing,
        SC_MANAGER_ALL_ACCESS)
        If hSCM = 0 Then
            MsgBox("UninstallService: Unable to open Service Control Manager. Error: " & GetLastError().ToString)
            Exit Sub
        End If

        Dim hSvc As Integer = OpenService(hSCM, ServiceName,
        SERVICE_ALL_ACCESS)
        If hSvc = 0 Then
            CloseServiceHandle(hSCM)
            MsgBox("UninstallService: Unable to open service: " &
            GetLastError().ToString)
            Exit Sub
        End If

        If Not DeleteService(hSvc) Then
            CloseServiceHandle(hSvc)
            CloseServiceHandle(hSCM)
            MsgBox("UninstallService: Unable to delete service: " &
            GetLastError().ToString)
        End If

        CloseServiceHandle(hSvc)
        CloseServiceHandle(hSCM)

    End Sub

    Private Function ServiceInstalled() As Boolean

        Dim services() As ServiceController
        services = ServiceController.GetServices()

        For Each sc As ServiceController In services
            If sc.ServiceName = ServiceName Then
                Console.WriteLine("Service was installed")
                Return True
            End If
        Next

        Return False

    End Function

    Private Function StopService() As Boolean

        Dim iTimeout As Integer = 0
        Dim bTimeout As Boolean = False

        Dim svc As New ServiceController(ServiceName)
        If svc.Status = ServiceControllerStatus.Stopped Then
            Return True
        End If

        svc.Stop()
        iTimeout = 0
        While (svc.Status <> ServiceControllerStatus.Stopped)
            System.Threading.Thread.Sleep(1000)
            iTimeout += 1
            If iTimeout > 120 Then
                bTimeout = True
                Exit While
            End If
            svc.Refresh()
        End While

        Return Not bTimeout

    End Function



End Module