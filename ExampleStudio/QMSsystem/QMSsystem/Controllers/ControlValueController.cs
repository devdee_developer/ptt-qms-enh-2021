﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using System.IO;
using QMSsystem.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QMSsystem.Models.Privilege;
using QMSSystem.CoreDB;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_ControlGas, PrivilegeModeEnum.M_ControlProdcut,
        PrivilegeModeEnum.M_ControlProdcut, PrivilegeModeEnum.M_ControlProdcut, PrivilegeModeEnum.M_ControlClarifiedSystem,
        PrivilegeModeEnum.M_ControlWatseWater, PrivilegeModeEnum.M_ControlHotOilFlashPoint, PrivilegeModeEnum.M_ControlHotOilPhysical, 
        PrivilegeModeEnum.M_ControlSulfurInGasMonthly, PrivilegeModeEnum.M_ControlHgInPLMonthly, PrivilegeModeEnum.M_ControlHgInPL,
        PrivilegeModeEnum.M_ControlHgOutletMRU, PrivilegeModeEnum.M_ControlSulfurInGasWeekly, PrivilegeModeEnum.M_ControlTwoHundredThousandPond,
        PrivilegeModeEnum.M_ControlHgStab, PrivilegeModeEnum.M_ControlAcidOffGas, PrivilegeModeEnum.M_ProductMerge, PrivilegeModeEnum.M_CustomerMerge
    }, DataAccess = DataAccessEnum.Authen)]
    public class ControlValueController : BaseController
    {
        //
        // GET: /ControlValue/

        public ActionResult ListMasterControlGas()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.GAS;
            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.GAS;

            ViewBag.searchControlValue = searchControlValue;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValueData = searchControlValueData;

            ViewBag.listUnit = listUnit;

            return View();
        }


        public ActionResult CreateMasterControlGas(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValue = new ControlValueDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            string actionName = Resources.Strings.AddNew;

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.GAS;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_CONTROL = masterDataServices.getQMS_MA_CONTROLById(_db, id);
                createQMS_MA_CONTROL_ROW.CONTROL_ID = id;
                createQMS_MA_CONTROL_COLUMN.CONTROL_ID = id;
            }

            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            ViewBag.ActionName = actionName;
            return View();
        }

        public ActionResult ListMasterControlUtility()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();
            
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.Utility;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.Utility;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
             
            return View();
        }

        public ActionResult CreateMasterControlUtility(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValue = new ControlValueDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            string actionName = Resources.Strings.AddNew;

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.Utility;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_CONTROL = masterDataServices.getQMS_MA_CONTROLById(_db, id);
                createQMS_MA_CONTROL_ROW.CONTROL_ID = id;
                createQMS_MA_CONTROL_COLUMN.CONTROL_ID = id;
            }

            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            ViewBag.ActionName = actionName;
            return View();
        }

        public ActionResult ListMasterControlEmission()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.EMISSION;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.EMISSION;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlClarifiedSystem()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.ClarifiedSystem;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.ClarifiedSystem;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlWatseWater()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.WatseWater;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.WatseWater;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }

        public ActionResult ListMasterControlObservePonds()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.ObservePonds;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.ObservePonds;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }

        public ActionResult ListMasterControlOilyWaters()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.OilyWaters;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.OilyWaters;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }

        public ActionResult ListMasterControlHotOilFlashPoint()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HotOilFlashPoint;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HotOilFlashPoint;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlHotOilPhysical()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HotOilPhysical;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HotOilPhysical;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlHgInPLMonthly()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgInPLMonthly;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgInPLMonthly;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlHgInPL()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgInPL;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgInPL;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlHgOutletMRU()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgOutletMRU;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgOutletMRU;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlSulfurInGasWeekly()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.SulfurInGasWeekly;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.SulfurInGasWeekly;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlSulfurInGasMonthly()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.SulfurInGasMonthly;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.SulfurInGasMonthly;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlTwoHundredThousandPond()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.TwoHundredThousandPond;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.TwoHundredThousandPond;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlHgStab()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgStab;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.HgStab;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public ActionResult ListMasterControlAcidOffGas()
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.AcidOffGas;
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.AcidOffGas;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }
        public JsonResult CreateQMS_MA_CONTROL(CreateQMS_MA_CONTROL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_CONTROL(_db, model);

                    if (nResult > 0)
                    {
                        model = masterDataServices.getQMS_MA_CONTROLById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_CONTROL_ROW(CreateQMS_MA_CONTROL_ROW model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_CONTROL_ROW(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_CONTROL_COLUMN(CreateQMS_MA_CONTROL_COLUMN model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_CONTROL_COLUMN(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_CONTROL_DATA(CreateQMS_MA_CONTROL_DATA model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_CONTROL_DATA(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CreateQMS_MA_CONTROL_DATACONF(CreateQMS_MA_CONTROL_DATACONF model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_CONTROL_DATACONF(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ListMasterControlProduct()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel();
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValueData = new ControlValueDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.PRODUCT; 
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.PRODUCT;

            ViewBag.searchControlValueData = searchControlValueData;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            return View();
        }

        public ActionResult CreateMasterControlProduct(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValue = new ControlValueDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            string actionName = Resources.Strings.AddNew;

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.PRODUCT;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_CONTROL = masterDataServices.getQMS_MA_CONTROLById(_db, id);
                createQMS_MA_CONTROL_ROW.CONTROL_ID = id;
                createQMS_MA_CONTROL_COLUMN.CONTROL_ID = id;
            }

            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult DeleteQMS_MA_CONTROL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_CONTROLByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_CONTROL_ROW(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_CONTROL_ROWByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_CONTROL_COLUMN(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_CONTROL_COLUMNByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_CONTROL_DATA(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_CONTROL_DATAByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteQMS_MA_CONTROL_DATACONF(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_CONTROL_DATACONFByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_CONTROLBySearch(ControlValueSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CONTROL> listData = new List<ViewQMS_MA_CONTROL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_CONTROL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetQMS_MA_CONTROL_ROWBySearch(ControlValueRowSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CONTROL_ROW> listData = new List<ViewQMS_MA_CONTROL_ROW>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_CONTROL_ROW(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };

                    QMS_MA_CONTROL Result = new QMS_MA_CONTROL();
                    Result = QMS_MA_CONTROL.GetById(_db, model.mSearch.CONTROL_ID);

                    if(Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.WatseWater
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilFlashPoint
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilPhysical
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPLMonthly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPL
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgOutletMRU
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasMonthly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.ObservePonds
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.OilyWaters
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasWeekly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.TwoHundredThousandPond
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgStab
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.AcidOffGas
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.EMISSION)
                    {
                        List<List<ViewQMS_MA_CONTROL_DATACONF>> listControlData = new List<List<ViewQMS_MA_CONTROL_DATACONF>>();
                        listControlData = masterDataServices.getTemplateControlConfigValue(_db, model.mSearch.CONTROL_ID);
                        result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex, listControlData = listControlData };
                    }
                    else
                    {
                        List<List<ViewQMS_MA_CONTROL_DATA>> listControlData = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                        listControlData = masterDataServices.getTemplateControlValue(_db, model.mSearch.CONTROL_ID);
                        result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex, listControlData = listControlData };
                    }
                    
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetSamplePosition(SetSamplePosition model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CONTROL_ROW> listData = new List<ViewQMS_MA_CONTROL_ROW>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.setQMS_MA_CONTROL_ROWPostion(_db, model, out count, out totalPage, out listPageIndex);

                    List<List<ViewQMS_MA_CONTROL_DATA>> listControlData = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                    listControlData = masterDataServices.getTemplateControlValue(_db, model.CONTROL_ID);


                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex, listControlData = listControlData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetItemPosition(SetItemPosition model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                { 
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CONTROL_COLUMN> listData = new List<ViewQMS_MA_CONTROL_COLUMN>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.setQMS_MA_CONTROL_COLUMNTPostion(_db, model, out count, out totalPage, out listPageIndex);

                    List<List<ViewQMS_MA_CONTROL_DATA>> listControlData = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                    listControlData = masterDataServices.getTemplateControlValue(_db, model.CONTROL_ID);


                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex, listControlData = listControlData };
                     
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_CONTROL_COLUMNBySearch(ControlValueColumnSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CONTROL_COLUMN> listData = new List<ViewQMS_MA_CONTROL_COLUMN>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_CONTROL_COLUMN(_db, model, out count, out totalPage, out listPageIndex);

                    //List<List<ViewQMS_MA_CONTROL_DATA>> listControlData = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                    //listControlData = masterDataServices.getTemplateControlValue(_db, model.mSearch.CONTROL_ID);


                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex};
                    QMS_MA_CONTROL Result = new QMS_MA_CONTROL();
                    Result = QMS_MA_CONTROL.GetById(_db, model.mSearch.CONTROL_ID);

                    if (Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.WatseWater
                        || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilFlashPoint
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilPhysical
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPLMonthly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPL
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgOutletMRU
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasMonthly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.ObservePonds
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.OilyWaters
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasWeekly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.TwoHundredThousandPond
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgStab
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.AcidOffGas
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.EMISSION)
                    {
                        List<List<ViewQMS_MA_CONTROL_DATACONF>> listControlData = new List<List<ViewQMS_MA_CONTROL_DATACONF>>();
                        listControlData = masterDataServices.getTemplateControlConfigValue(_db, model.mSearch.CONTROL_ID);
                        result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex, listControlData = listControlData };
                    }
                    else
                    {
                        List<List<ViewQMS_MA_CONTROL_DATA>> listControlData = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                        listControlData = masterDataServices.getTemplateControlValue(_db, model.mSearch.CONTROL_ID);
                        result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex, listControlData = listControlData };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_CONTROL_DATABySearch(ControlValueDataSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CONTROL_DATA> listData = new List<ViewQMS_MA_CONTROL_DATA>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_CONTROL_DATA(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTemplateControlValue(long controlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    result = new { result = ReturnStatus.SUCCESS};

                    QMS_MA_CONTROL Result = new QMS_MA_CONTROL();
                    Result = QMS_MA_CONTROL.GetById(_db, controlId);

                    if (Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.WatseWater
                        || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilFlashPoint
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilPhysical
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPLMonthly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPL
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgOutletMRU
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasMonthly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.ObservePonds
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.OilyWaters
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasWeekly
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.TwoHundredThousandPond
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgStab
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.AcidOffGas
                       || Result.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.EMISSION)
                    {
                        List<List<ViewQMS_MA_CONTROL_DATACONF>> listData = new List<List<ViewQMS_MA_CONTROL_DATACONF>>();
                        listData = masterDataServices.getTemplateControlConfigValue(_db, controlId);
                        result = new { result = ReturnStatus.SUCCESS, message = listData };
                    }
                    else
                    {
                        List<List<ViewQMS_MA_CONTROL_DATA>> listData = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                        listData = masterDataServices.getTemplateControlValue(_db, controlId);
                        result = new { result = ReturnStatus.SUCCESS, message = listData };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getQMS_MA_CONTROL_DATACONFByRowColControlId(CreateQMS_MA_CONTROL_DATACONF model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_CONTROL_DATACONF> listData = new List<ViewQMS_MA_CONTROL_DATACONF>();
                    listData = masterDataServices.getQMS_MA_CONTROL_DATACONFByRowColControlId(_db, model);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSampleListByControlId(long controlId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_CONTROL_ROW> listData = new List<ViewQMS_MA_CONTROL_ROW>();
                    listData = masterDataServices.getQMS_MA_CONTROL_ROWListByControlId(_db, controlId); 

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemControlValueListBySampleId(long controlId, long sampleId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_CONTROL_DATA> listData = new List<ViewQMS_MA_CONTROL_DATA>();
                    listData = masterDataServices.getQMS_MA_CONTROL_DATAListByControlSampleId(_db, controlId, sampleId); 

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemControlConfigValueListBySampleId(long controlId, long sampleId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_CONTROL_DATACONF> listData = new List<ViewQMS_MA_CONTROL_DATACONF>();
                    listData = masterDataServices.getQMS_MA_CONTROL_DATACONFListByControlSampleId(_db, controlId, sampleId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemControlValueListBySampleIdEx( long sampleId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_CONTROL_DATA> listData = new List<ViewQMS_MA_CONTROL_DATA>();
                    listData = masterDataServices.getQMS_MA_CONTROL_DATAListByControlSampleId(_db, 0, sampleId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public void GetTemplateControlValuePDF(long controlId, int bDownload = 0)
        {
            MemoryStream ms = new MemoryStream();
            PrintPDFHeader ePdf = new PrintPDFHeader();
            Phrase phrase = new Phrase();
            Document pdfDoc = new Document();//(new Rectangle(288f, 144f), 22, 22, 75, 70); 
            pdfDoc.SetMargins(22, 22, 75, 40); 
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A3.Rotate());


            #region getData 

            #endregion

            
            try
            {
                initialFont();

                #region getData

                MasterDataService masterDataServices = new MasterDataService(_username);
                QMS_MA_CONTROL nResult = new QMS_MA_CONTROL();
                nResult = QMS_MA_CONTROL.GetById(_db, controlId);
                List<List<ViewQMS_MA_CONTROL_DATA>> listResult = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                if (nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.WatseWater
                    || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilFlashPoint
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HotOilPhysical
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPLMonthly
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgInPL
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgOutletMRU
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasMonthly
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.ObservePonds
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.OilyWaters
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.SulfurInGasWeekly
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.TwoHundredThousandPond
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.HgStab
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.AcidOffGas
                   || nResult.GROUP_TYPE == (byte)CONTROL_GROUP_TYPE.EMISSION)
                {
                    List<List<ViewQMS_MA_CONTROL_DATA>> matrixResult = new List<List<ViewQMS_MA_CONTROL_DATA>>();
                    List<ViewQMS_MA_CONTROL_DATA> templistResult = new List<ViewQMS_MA_CONTROL_DATA>();
                    ViewQMS_MA_CONTROL_DATA tempData = new ViewQMS_MA_CONTROL_DATA();
                    List<List<ViewQMS_MA_CONTROL_DATACONF>> resConf = masterDataServices.getTemplateControlConfigValue(_db, controlId);
                    foreach (List<ViewQMS_MA_CONTROL_DATACONF> dt in resConf)
                    {
                        templistResult = new List<ViewQMS_MA_CONTROL_DATA>();
                        foreach (ViewQMS_MA_CONTROL_DATACONF md in dt)
                        {
                            tempData = new ViewQMS_MA_CONTROL_DATA();
                            tempData.ID = md.ID;
                            tempData.CONTROL_ID = md.CONTROL_ID;
                            tempData.CONTROL_ROW_ID = md.CONTROL_ROW_ID;
                            tempData.CONTROL_COLUMN_ID = md.CONTROL_COLUMN_ID;
                            tempData.MAX_NO_CAL = null != md.MAX_NO_CAL? md.MAX_NO_CAL.Value:false;
                            tempData.MAX_FLAG = null != md.MAX_FLAG ? md.MAX_FLAG.Value : false;
                            tempData.MAX_VALUE = null != md.MAX_VALUE ? md.MAX_VALUE.Value : (decimal)0.000000;
                            tempData.MIN_NO_CAL = null != md.MIN_NO_CAL ? md.MIN_NO_CAL.Value : false;
                            tempData.MIN_FLAG = null != md.MIN_FLAG ? md.MIN_FLAG.Value : false;
                            tempData.MIN_VALUE = null != md.MIN_VALUE ? md.MIN_VALUE.Value : (decimal)0.000000;
                            tempData.CONC_NO_CAL = null != md.CONC_NO_CAL ? md.CONC_NO_CAL.Value : false;
                            tempData.CONC_FLAG = null != md.CONC_FLAG ? md.CONC_FLAG.Value : false;
                            tempData.CONC_VALUE = null != md.CONC_VALUE ? md.CONC_VALUE.Value : (decimal)0.000000;
                            tempData.LOADING_NO_CAL = null != md.LOADING_NO_CAL ? md.LOADING_NO_CAL.Value : false;
                            tempData.LOADING_FLAG = null != md.LOADING_FLAG ? md.LOADING_FLAG.Value : false;
                            tempData.LOADING_VALUE = null != md.LOADING_VALUE ? md.LOADING_VALUE.Value : (decimal)0.000000;

                            tempData.CONC_LOADING_FLAG = null != md.CONC_LOADING_FLAG ? md.CONC_LOADING_FLAG.Value : false;
                            tempData.CONTROL_ROW_NAME = md.CONTROL_ROW_NAME;
                            tempData.CONTROL_COLUMN_NAME = md.CONTROL_COLUMN_NAME;
                            tempData.CONTROL_DATA_NAME = md.CONTROL_DATA_NAME;
                            tempData.UNIT_NAME = md.UNIT_NAME;
                            tempData.ITEM_TYPE = md.ITEM_TYPE;
                            tempData.ITEM_SELECTED = md.ITEM_SELECTED;


                            tempData.CREATE_USER = md.CREATE_USER;
                            tempData.SHOW_CREATE_DATE = md.SHOW_CREATE_DATE;
                            tempData.UPDATE_USER = md.UPDATE_USER;
                            tempData.SHOW_UPDATE_DATE = md.SHOW_UPDATE_DATE;
                            templistResult.Add(tempData);
                        }
                        listResult.Add(templistResult);
                    }
                }
                else
                {
                    listResult = masterDataServices.getTemplateControlValue(_db, controlId);
                }
                //List<List<ViewQMS_MA_CONTROL_DATA>> listResult =  masterDataServices.getTemplateControlValue(_db, controlId);
                CreateQMS_MA_CONTROL createQMS_MA_CONTROL = masterDataServices.getQMS_MA_CONTROLById(_db, controlId);
 
                #endregion


                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);
               

                #region setHeader
                PdfPTable head = new PdfPTable(9);
                float[] widths = new float[] { 22f, 30f, 75f, 50f, 70f, 70f, 70f, 70f, 116f };
                head.SetWidths(widths);
                head.WidthPercentage = 100;
                 

                Rectangle page = pdfDoc.PageSize;
                head.TotalWidth = page.Width - 22f; //magin right 
                head.HorizontalAlignment = Element.ALIGN_CENTER;
                 
                PdfPCell dataCol1 = new PdfPCell(); 

                String szHead = @Resources.Strings.TemplateGAS + "\n";
                szHead += createQMS_MA_CONTROL.NAME + "\n\n";

                Paragraph p = new Paragraph(szHead, fnt14);
                dataCol1 = new PdfPCell(p);
                dataCol1.Colspan = 9;
                dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                dataCol1.Border = PdfPCell.NO_BORDER;
                head.HorizontalAlignment = Element.ALIGN_CENTER;
                head.AddCell(dataCol1);
                head.CompleteRow();
                 
                writer.PageEvent = ePdf;

                float nTop = pdfDoc.GetTop(0) + 40f;
                //ePdf.ImageHeader = image;
                ePdf.ScalePercent = 50f;
                ePdf.xImage = 20f;
                ePdf.yImage = nTop;

                ePdf.fontHeader = fntHeader;
                ePdf.head = head;
                ePdf.bsFont = bf;

                #endregion
                pdfDoc.Open();

                try
                { 

                    if (listResult.Count() > 0)
                    { 
                        List<ViewQMS_MA_CONTROL_DATA> temp = listResult[0];

                        int maxWidth = (temp.Count() > 0) ? temp.Count() : 1;
                        int count  ;
                        PdfPTable body = new PdfPTable(maxWidth); //820/42
                        List<float> listWidth = new List<float>();
                        for (int col = 0; col < maxWidth; col++)
                        {
                            listWidth.Add((float)(1640 / maxWidth));
                        }
                        float[] widths2 = listWidth.ToArray();
                        body.SetWidths(widths2);
                        body.WidthPercentage = 100;
                        string showData = ""; 


                        foreach (List<ViewQMS_MA_CONTROL_DATA> listData in listResult)
                        {
                            
                            count = 0;
                            foreach (ViewQMS_MA_CONTROL_DATA dt in listData)
                            {

                                showData = dt.CONTROL_ROW_NAME + dt.CONTROL_COLUMN_NAME + dt.CONTROL_DATA_NAME;

                                if (dt.ITEM_TYPE == 1  ) 
                                {
                                    dataCol1 = new PdfPCell(new Phrase(showData, fnt12White));
                                    dataCol1.BackgroundColor = bHeaderExcel; 
                                }else if( dt.ITEM_TYPE == 2 ){
                                    dataCol1 = new PdfPCell(new Phrase(showData, fnt12White));
                                    dataCol1.BackgroundColor = bHeaderExcel2; 
                                }
                                else
                                {
                                    if ( showData != null && showData != "")
                                    {
                                        dataCol1 = new PdfPCell(new Phrase(showData, fnt12));
                                        dataCol1.BackgroundColor = bGreen;
                                    }else{
                                        dataCol1 = new PdfPCell(new Phrase(showData, fnt12));
                                    }
                                     
                                }

                                dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                                
                                body.AddCell(dataCol1);
                                count++;
                            } 
                            body.CompleteRow();  
                        }
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body);

                    }
                    else
                    {
                        phrase.Add(new Chunk("No data :" + listResult.Count()));
                        pdfDoc.Add(phrase);
                    }
                }
                catch (Exception ex)
                {
                    phrase.Add(new Chunk(ex.Message));
                    pdfDoc.Add(phrase);
                } 
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();

            }
            catch (Exception ex)
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);
                pdfDoc.Open();
                phrase.Add(new Chunk(ex.Message));
                pdfDoc.Add(phrase);
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();
            }



            // กำหนด ContentType เป็น application/pdf เพื่อ Response เป็น Pdf
            Response.ContentType = "application/pdf";

            // กำหนดชื่อไฟล์ที่ต้องการ Export
            if (bDownload > 0)
                Response.AddHeader("content-disposition", "attachment; filename=" + "UserReport_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf");
            // Export Pdf
            Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            Response.End();
        }


        public ActionResult ListMasterProductMerge()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ProductMergeSearchModel searchProductMerge = new ProductMergeSearchModel();
            CreateQMS_MA_PRODUCT_MERGE createQMS_MA_PRODUCT_MERGE = new CreateQMS_MA_PRODUCT_MERGE();


            ViewBag.searchProductMerge = searchProductMerge;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_PRODUCT_MERGE = createQMS_MA_PRODUCT_MERGE;

            return View();
        }

        public JsonResult GetQMS_MA_PRODUCT_MERGEBySearch(ProductMergeSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_PRODUCT_MERGE> listData = new List<ViewQMS_MA_PRODUCT_MERGE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    //model.mSearch.ACTIVE_DATE = new DateTime();
                    //model.mSearch.EXPIRE_DATE = new DateTime();
                    listData = masterDataServices.searchQMS_MA_PRODUCT_MERGE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_PRODUCT_MERGE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_PRODUCT_MERGEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateQMS_MA_PRODUCT_MERGE(CreateQMS_MA_PRODUCT_MERGE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_PRODUCT_MERGE(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListMasterCustomerMerge()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CustomerMergeSearchModel searchCustomerMerge = new CustomerMergeSearchModel();
            CreateQMS_MA_CUSTOMER_MERGE createQMS_MA_CUSTOMER_MERGE = new CreateQMS_MA_CUSTOMER_MERGE();


            ViewBag.searchCustomerMerge = searchCustomerMerge;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_CUSTOMER_MERGE = createQMS_MA_CUSTOMER_MERGE;


            return View();
        }


        public JsonResult GetQMS_MA_CUSTOMER_MERGEBySearch(CustomerMergeSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_CUSTOMER_MERGE> listData = new List<ViewQMS_MA_CUSTOMER_MERGE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_CUSTOMER_MERGE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_CUSTOMER_MERGE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_CUSTOMER_MERGEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateQMS_MA_CUSTOMER_MERGE(CreateQMS_MA_CUSTOMER_MERGE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_CUSTOMER_MERGE(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }




    }
}
