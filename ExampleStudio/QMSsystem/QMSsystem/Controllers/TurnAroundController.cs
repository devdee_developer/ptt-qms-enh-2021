﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_TurnAround  }, DataAccess = DataAccessEnum.Authen)]
    public class TurnAroundController : BaseController
    {
        //
        // GET: /TurnAround/

        public ActionResult ListManageTurnAround()//control ListTurnAroundExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TurnAroundSearchModel searchTurnAround = new TurnAroundSearchModel();
            CreateQMS_TR_TURN_AROUND createQMS_TR_TURN_AROUND = new CreateQMS_TR_TURN_AROUND(); 

            createQMS_TR_TURN_AROUND.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.TRUN_AROUND;
            createQMS_TR_TURN_AROUND.START_DATE = getDummyDateTime();
            createQMS_TR_TURN_AROUND.END_DATE = getDummyDateTime();

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();

            ViewBag.searchTurnAround = searchTurnAround;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_TR_TURN_AROUND = createQMS_TR_TURN_AROUND;
            ViewBag.listDocStatusEx = this.getDocStatusList();
            return View();
        }

        public ActionResult CreateManageTurnAround(long id = 0)//control CresteTurnAroundExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_TURN_AROUND createQMS_TR_TURN_AROUND = new CreateQMS_TR_TURN_AROUND();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_TURN_AROUND.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.TRUN_AROUND;
            createQMS_TR_TURN_AROUND.START_DATE = getDummyDateTime();
            createQMS_TR_TURN_AROUND.END_DATE = getDummyDateTime();

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_TURN_AROUND = transactionServices.getQMS_TR_TURN_AROUNDById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();

            ViewBag.createQMS_TR_TURN_AROUND = createQMS_TR_TURN_AROUND;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_TURN_AROUND(CreateQMS_TR_TURN_AROUND model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);

                    //check overlap --> model.DOC_STATUS == 0 หมายถึงไม่เอา initial
                    VALIDATE_EXCEPTION_OVERLLAP checkOverlap = transactionServices.IsOverlapMasterCorrectDataEx(_db, model.ID, model.PLANT_ID, 0, 0, model.START_DATE, model.END_DATE, (byte)CORRECT_DATA_TYPE.TRUN_AROUND);
                    if (checkOverlap.IS_OVERLAP)
                    {
                        result = new { result = ReturnStatus.ERROR, message = checkOverlap.MSG_ERROR};
                    }
                    else { 
                        long nResult = transactionServices.SaveQMS_TR_TURN_AROUND(_db, model);
                        if (nResult > 0)
                        {
                            transactionServices.SaveQMS_TR_OFF_CONTROL_CAL_With_TRUN_AROUD(_db, model); //Adjust Turn Atround
                            model = transactionServices.getQMS_TR_TURN_AROUNDById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_TURN_AROUNDBySearch(TurnAroundSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_TURN_AROUND> listData = new List<ViewQMS_TR_TURN_AROUND>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_TURN_AROUND(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_TURN_AROUND(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);
                ReportServices reportServices = new ReportServices(_username);

                try
                { 
                    //bool nResult = transactionServices.DeleteQMS_TR_TURN_AROUNDByListId(_db, ids);
                    bool nResult = transactionServices.DeleteQMS_TR_TURN_AROUNDByListIdEx(_db, ids);
                    
                    if (true == nResult)
                    { 
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_TURN_AROUND(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_TURN_AROUNDByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
