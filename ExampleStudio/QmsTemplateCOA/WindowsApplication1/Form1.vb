﻿Imports System.ComponentModel
Imports QMSSystem.CoreDB.Services
Imports System.Data.SqlClient
Imports System.Data.EntityClient
Imports System.Configuration

Public Class Form1

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Button1.Enabled = False
        lblStatus.Text = "0%"
        If BackgroundWorker1.IsBusy <> True Then
            ' Start the asynchronous operation.
            BackgroundWorker1.RunWorkerAsync()
        End If

    End Sub
    Public Function GetConnectionString() As String

        Try
            Return ConfigurationManager.ConnectionStrings("QMSDBEntities").ToString() 'QMSSystemExaDB
        Catch ex As Exception
            ' Specify the provider name, server and database. ptt-db-p03.ptt.corp; Initial Catalog=PTT-GSPQualityManagementSystem; User ID=pttgspqprdusr; Password=cpttgspqprdusr"/>
            Dim providerName As String = "System.Data.SqlClient"
            Dim serverName As String = "ptt-db-p03.ptt.corp" 'ptt-db-t03.ptt.corp"
            Dim databaseName As String = "PTT-GSPQualityManagementSystem" 'PTT-GSPQualityManagementSystem_test"

            ' Initialize the connection string builder for the
            ' underlying provider.
            Dim sqlBuilder As New SqlConnectionStringBuilder

            ' Set the properties for the data source.
            sqlBuilder.DataSource = serverName
            sqlBuilder.InitialCatalog = databaseName
            sqlBuilder.PersistSecurityInfo = True
            sqlBuilder.UserID = "pttgspqprdusr" 'pttgspqtstusr"
            sqlBuilder.Password = "cpttgspqprdusr" 'cpttgspqtstusr"


            ' Build the SqlConnection connection string.
            Dim providerString As String = sqlBuilder.ToString

            ' Initialize the EntityConnectionStringBuilder.
            Dim entityBuilder As New EntityConnectionStringBuilder

            'Set the provider name.
            entityBuilder.Provider = providerName
            ' Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = providerString
            ' Set the Metadata location to the current directory.
            entityBuilder.Metadata = "res://*//QMSSystem.csdl|" & _
                                        "res://*//QMSSystem.ssdl|" & _
                                        "res://*//QMSSystem.msl"

            Return entityBuilder.ToString()
        End Try
         
    End Function
    Private Sub BackgroundWorker1_Disposed(sender As Object, e As System.EventArgs) Handles BackgroundWorker1.Disposed

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim startDate As DateTime
        Dim endDate As DateTime
        Dim actionDate As DateTime
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
        Dim programStartTime As DateTime = DateTime.Now
        Dim programEndTime As DateTime

        startDate = dpkStart.Value
        endDate = dpkEnd.Value
        Dim stepValue As Double = 1
        Dim Total As Double = 0

        Dim connectionString As String
        'connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities() 'QMSSystem.CoreDB.QMSDBEntities(connectionString)
        Dim coaService = New COAServices(db, "Service")

        Try
            worker.ReportProgress(1) 'pgbStatus.Value = 1
            Dim interval As Integer = (endDate - startDate).TotalDays + 1

            If interval > 0 Then
                stepValue = 100 / (interval)
            Else
                stepValue = 100 / 1
            End If

            startDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0)
            endDate = New DateTime(dpkEnd.Value.Year, dpkEnd.Value.Month, dpkEnd.Value.Day, 23, 59, 59)

            Do Until (startDate >= endDate)

                actionDate = startDate.AddMinutes(-2) 'tempfor add data

                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                End If

                Try
                    coaService.DailyCOAMasterDataCheck(startDate, startDate.AddDays(1))
                    System.Threading.Thread.Sleep(1000)

                Catch ex As Exception

                End Try

                Total += stepValue
                If Total > 100 Then
                    Total = 100
                End If
                worker.ReportProgress(Convert.ToInt32(Total))

                startDate = startDate.AddDays(1)
                Threading.Thread.Sleep(500)
            Loop

        Catch ex As Exception

        End Try
        ' pgbStatus.Value = 100
        worker.ReportProgress(100)
        programEndTime = DateTime.Now
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        lblStatus.Text = (e.ProgressPercentage.ToString() + "%")
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        If e.Cancelled = True Then
            lblStatus.Text = "Canceled!"
            Button1.Enabled = True
        ElseIf e.Error IsNot Nothing Then
            lblStatus.Text = "Error: " & e.Error.Message
            Button1.Enabled = True
        Else
            lblStatus.Text = "Done!"
            Button1.Enabled = True
        End If
    End Sub

    Private Sub Label3_Click(sender As System.Object, e As System.EventArgs) Handles Label3.Click

    End Sub
End Class
