﻿CREATE TABLE [dbo].[QMS_MA_DOWNTIME](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PLANT_ID] [bigint] NOT NULL,
	[EXCEL_NAME] [nvarchar](20) NOT NULL,
	[LIMIT_VALUE] [decimal](10, 6) NOT NULL,
	[UNIT_ID] [bigint] NOT NULL,
	[ACTIVE_DATE] [datetime] NOT NULL,
	[DOWNTIME_DESC] [nvarchar](250) NULL,
	[DELETE_FLAG] [tinyint] NOT NULL,
	[CREATE_USER] [nvarchar](50) NOT NULL,
	[CREATE_DATE] [datetime] NOT NULL,
	[UPDATE_USER] [nvarchar](50) NOT NULL,
	[UPDATE_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_QMS_MA_DOWNTIME] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


