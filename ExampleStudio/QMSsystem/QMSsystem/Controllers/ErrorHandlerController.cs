﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QMSsystem.Controllers
{
    public class ErrorHandlerController : Controller
    {
        //
        // GET: /ErrorHandler/

        public ActionResult NotFound()
        {
            return View("~/Views/Shared/Error404.cshtml");
        }


        public ActionResult Index()
        {
            return View("~/Views/Shared/Error500.cshtml");
        }
    }
}
