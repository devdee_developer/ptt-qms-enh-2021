﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_DownTime, PrivilegeModeEnum.N_DownTime}, DataAccess = DataAccessEnum.Authen)]
    public class DowntimeController : BaseController
    {
        //
        // GET: /Downtime/

        public ActionResult ListMasterDowntime()//control ListMasterDowntime.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            DowntimeSearchModel searchDowntime = new DowntimeSearchModel();
            CreateQMS_MA_DOWNTIME createQMS_MA_DOWNTIME = new CreateQMS_MA_DOWNTIME();
            CreateQMS_MA_DOWNTIME_DETAIL createQMS_MA_DOWNTIME_DETAIL = new CreateQMS_MA_DOWNTIME_DETAIL();
            DowntimeDetailSearchModel searchDowntimeDetail = new DowntimeDetailSearchModel();
            createQMS_MA_DOWNTIME.ACTIVE_DATE = getDummyDateTime();

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            ViewBag.searchDowntime = searchDowntime;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_DOWNTIME = createQMS_MA_DOWNTIME;
            ViewBag.createQMS_MA_DOWNTIME_DETAIL = createQMS_MA_DOWNTIME_DETAIL;

            ViewBag.searchDowntimeDetail = searchDowntimeDetail;


            return View();
        }

        public ActionResult CreateMasterDowntime(long id = 0)//control CresteMasterDowntime.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_DOWNTIME createQMS_MA_DOWNTIME = new CreateQMS_MA_DOWNTIME();
            CreateQMS_MA_DOWNTIME_DETAIL createQMS_MA_DOWNTIME_DETAIL = new CreateQMS_MA_DOWNTIME_DETAIL();
            DowntimeDetailSearchModel searchDowntimeDetail = new DowntimeDetailSearchModel();

            string actionName = Resources.Strings.AddNew;

            createQMS_MA_DOWNTIME.ACTIVE_DATE = getDummyDateTime();
            searchDowntimeDetail.DOWNTIME_ID = id;
            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_DOWNTIME = masterDataServices.getQMS_MA_DOWNTIMEById(_db, id);
            }


            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listUnit = masterDataServices.getQMS_MA_UNITList(_db);//.getQMS_MA_UNITActiveList(_db);
            ViewBag.listDowntimeDetail = new List<ViewQMS_MA_DOWNTIME_DETAIL>();// masterDataServices.searchQMS_MA_DOWNTIME_DETAIL(_db, id);

            ViewBag.createQMS_MA_DOWNTIME = createQMS_MA_DOWNTIME;
            ViewBag.createQMS_MA_DOWNTIME_DETAIL = createQMS_MA_DOWNTIME_DETAIL;

            ViewBag.searchDowntimeDetail = searchDowntimeDetail;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult GetCreateQMS_MA_DOWNTIMEById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_DOWNTIME createQMS_MA_DOWNTIME = masterDataServices.getQMS_MA_DOWNTIMEById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_DOWNTIME };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_DOWNTIME(CreateQMS_MA_DOWNTIME model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_DOWNTIME(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_MA_DOWNTIME_DETAIL modelDetail = new QMSSystem.Model.CreateQMS_MA_DOWNTIME_DETAIL();

                        modelDetail.ID = model.DOWNTIME_DETAIL_ID;
                        modelDetail.DOWNTIME_ID = nResult;
                        modelDetail.EXA_TAG_NAME = model.EXA_TAG_NAME;
                        modelDetail.CONVERT_VALUE = model.CONVERT_VALUE;

                        long nResultDetail = masterDataServices.SaveQMS_MA_DOWNTIME_DETAIL(_db, modelDetail);

                        model = masterDataServices.getQMS_MA_DOWNTIMEById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, detailId = nResultDetail, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_DOWNTIMEBySearch(DowntimeSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_DOWNTIME> listData = new List<ViewQMS_MA_DOWNTIME>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_DOWNTIME(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_DOWNTIME(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_DOWNTIMEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateQMS_MA_DOWNTIME_DETAIL(CreateQMS_MA_DOWNTIME_DETAIL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_DOWNTIME_DETAIL(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_DOWNTIME_DETAILBySearch(DowntimeDetailSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_DOWNTIME_DETAIL> listData = new List<ViewQMS_MA_DOWNTIME_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_DOWNTIME_DETAIL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_DOWNTIME_DETAIL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_DOWNTIME_DETAILByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ListManageDowntime()//control ListDowntimeExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TRDowntimeSearchModel searchDowntime = new TRDowntimeSearchModel();

            CreateQMS_TR_DOWNTIME createQMS_TR_DOWNTIME = new CreateQMS_TR_DOWNTIME();

            createQMS_TR_DOWNTIME.START_DATE = getDummyDateTime();
            createQMS_TR_DOWNTIME.END_DATE = getDummyDateTime();

            ViewBag.createQMS_TR_DOWNTIME = createQMS_TR_DOWNTIME;
            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusListWithInitail();
            ViewBag.listDocStatusEx = this.getDocStatusList();
            ViewBag.searchDowntime = searchDowntime;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public ActionResult CreateManageDowntime(long id = 0)//control CresteDowntimeExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_DOWNTIME createQMS_TR_DOWNTIME = new CreateQMS_TR_DOWNTIME();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_DOWNTIME.START_DATE = getDummyDateTime();
            createQMS_TR_DOWNTIME.END_DATE = getDummyDateTime();
            createQMS_TR_DOWNTIME.CORRECT_DATA_TYPE  = (byte)CORRECT_DATA_TYPE.DOWN_TIME;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_DOWNTIME = transactionServices.getQMS_TR_DOWNTIMEById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();

            ViewBag.createQMS_TR_DOWNTIME = createQMS_TR_DOWNTIME;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_DOWNTIME(CreateQMS_TR_DOWNTIME model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);
                    
                    VALIDATE_EXCEPTION_OVERLLAP checkOverlap = transactionServices.IsOverlapMasterCorrectDataEx(_db, model.ID, model.PLANT_ID, 0, 0, model.START_DATE, model.END_DATE, (byte)CORRECT_DATA_TYPE.DOWN_TIME);
                    if (checkOverlap.IS_OVERLAP)
                    {
                        result = new { result = ReturnStatus.ERROR, message = checkOverlap.MSG_ERROR };
                    }
                    else
                    {
                        long nResult = transactionServices.SaveQMS_TR_DOWNTIME(_db, model);
                        if (nResult > 0)
                        {
                            transactionServices.SaveQMS_TR_OFF_CONTROL_CAL_With_DOWNTIME(_db, model); //Adjust Turn Atround
                            model = transactionServices.getQMS_TR_DOWNTIMEById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_DOWNTIMEBySearch(TRDowntimeSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_DOWNTIME> listData = new List<ViewQMS_TR_DOWNTIME>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_DOWNTIME(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_DOWNTIME(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_DOWNTIMEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_DOWNTIME(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_DOWNTIMEByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public void DowntimeDataExcel(ProductExaDataSearchModel searchModel)
        {
            TransactionService transDataServices = new TransactionService(_username);
            //ProductExaDataSearchModel searchModel = new ProductExaDataSearchModel();

            //searchModel.PRODUCT_ID = PLANT_ID;
            //searchModel.PLANT_ID = PRODUCT_ID;
            //searchModel.START_DATE = START_DATE;
            //searchModel.END_DATE = END_DATE;

            ExcelPackage excel = transDataServices.DowntimeExaDataExcelByPlantId(_db, searchModel);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.Downtime + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }


    }
}
