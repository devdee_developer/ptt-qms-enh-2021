﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
//using System.Data.OracleClient;
//using Oracle.DataAccess.Client;
using QMSSystem.CoreDB.Helper;

namespace QMSSystem.CoreDB.Services
{
    public class DBOracleUtils
    {

        public static OracleConnection
                       GetDBConnection(string host, int port, String sid, String user, String password)
        {

            Console.WriteLine("Getting Connection ..."); 
            OracleConnection conn = new OracleConnection();
            try
            {
                // Connection string to connect directly to Oracle.
                string connString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = "
                     + host + ")(PORT = " + port + "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = "
                     + sid + ")));Password=" + password + ";User ID=" + user;

                conn.ConnectionString = connString;
            }
            catch(Exception ex)
            {
                if (ex.InnerException != null)
                {
                    LogHelper.WriteErrorLog("inner message 32:" + ex.InnerException.Message);
                }
                LogHelper.WriteErrorLog("35 " + ex.Message);
            }
            

            return conn;
        }

    }


    public class DBUtils
    {
        public static OracleConnection GetDBConnection()
        {
            string host = "172.20.133.27";
            int port = 1521;
            string sid = "gsplims";
            string user = "gspresult";
            string password = "gspresult1";

            return DBOracleUtils.GetDBConnection(host, port, sid, user, password);
        }

        public static OracleConnection GetDBConnectionEx(string host = "172.20.133.27", int port = 1521, string sid = "gsplims", string user = "gspresult", string password = "gspresult1")
        { 
            return DBOracleUtils.GetDBConnection(host, port, sid, user, password);
        }

        public static string getViewDataSQL()
        {
            string SQLresult = "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT"; // "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT;";
            return SQLresult;
        }

        public static string getViewDataSQL2()
        {
            string SQLresult = "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT  WHERE SAMPLINGDATE >= to_date('17-09-2016','DD-MM-YYYY') and SAMPLINGDATE <= to_date('18-09-2016','DD-MM-YYYY')"; // "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT;";
            return SQLresult;

        }

        public static string getViewDataSQLMaster(string stdate, string endate)
        {
            string SQLresult = "SELECT * FROM   SAPPHIRE.V_TEMPLATE_MASTER  WHERE   CREATEDT >= to_date('" + stdate + "','DD-MM-YYYY') and CREATEDT <= to_date('" + endate + "','DD-MM-YYYY') ORDER BY CREATEDT ASC "; // "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT;";
            return SQLresult;

        }

        public static string getViewDataSQLDetail(string stdate, string endate)
        {
           //string SQLresult = "SELECT * FROM   SAPPHIRE.V_TEMPLATE_DETAIL  WHERE   CREATEDT >= to_date('" + stdate + "','DD-MM-YYYY') and CREATEDT <= to_date('" + endate + "','DD-MM-YYYY')"; // "SELECT * FROM SAPPHIRE.V_INTERFACE_COA_RESULT;"; 

           string   SQLresult = " SELECT DISTINCT a1.SAMPLINGNAME,  a1.CUSTOMERNAME,  a1.SAMPLINGPOINT,  a1.SAMPLINGDATE, a1.ITEM, ";
                    SQLresult += " a1.UNIT,  a1. SPECIFICATION, a1.DISPLAYVALUE, a1.ENTEREDVALUE, a1.PRODUCTNAME, ";
                    SQLresult += " a1.TANKNAME,  a2.USERSEQUENCE,  a2.TESTMETHOD, a2.ID ";
                    SQLresult += " FROM SAPPHIRE.V_INTERFACE_COA_RESULT a1 ";
                    SQLresult += " INNER JOIN ( ";
                    SQLresult += " SELECT DISTINCT * FROM SAPPHIRE.V_TEMPLATE_DETAIL WHERE SAPPHIRE.V_TEMPLATE_DETAIL.SAMPLINGDATE > TO_DATE ('" + stdate + "', 'DD-MM-YYYY')  AND SAPPHIRE.V_TEMPLATE_DETAIL.SAMPLINGDATE < TO_DATE ('" + endate + "', 'DD-MM-YYYY') ";
                    SQLresult += " ) a2 ON a1.SAMPLINGNAME = a2.SAMPLINGNAME ";
                    SQLresult += " AND a1.CUSTOMERNAME = a2.CUSTOMERNAME AND a1.SAMPLINGPOINT = a2.SAMPLINGPOINT  AND a1.SAMPLINGDATE = a2.SAMPLINGDATE AND a1.PRODUCTNAME = a2.PRODUCTNAME ";
                    //SQLresult += " AND a1.ITEM = a2.ITEM AND a1.UNIT = a2.UNIT AND a1. SPECIFICATION = a2.SPECIFICATIONDISPLAY AND a1.TANKNAME = a2.TANKNAME ";
                    SQLresult += " AND a1.ITEM = a2.ITEM AND a1.UNIT = a2.UNIT AND a1. SPECIFICATION = a2.SPECIFICATIONDISPLAY  "; // เอา ถังออก
                    SQLresult += " WHERE  a1.SAMPLINGDATE > TO_DATE ('" + stdate + "', 'DD-MM-YYYY')   AND a1.SAMPLINGDATE < TO_DATE ('" + endate + "', 'DD-MM-YYYY') AND a1. SPECIFICATION IS NOT NULL";


            return SQLresult;
        }
    }
}
