<!DOCTYPE html>
<html lang="en">

  <head>

    <?php include("./head_tag.php"); ?>



  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"<span>PTT QMS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include("./user_profile.php"); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php include("./sidemenu_qms.php"); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include("./menu_footer.php"); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php include("./top_nav.php"); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Plant and Product Mapping ( Add New )</h3>
                <div class="clearfix"></div>


              </div>
            </div>

            <div class="clearfix"></div>
            <hr>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="row"  style="text-align:right;">
                    <!-- <ul class="nav navbar-left panel_toolbox" style="text-align:right;">
                    <il><span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                      </il>
                      <il><span class="btn btn-warning" style="color:white;"><i class="fa fa-save"></i> Save</span>
                      </il>
                    </ul> -->
                    <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                    <span class="btn btn-success" style="color:white;"><i class="fa fa-save"></i> Save</span>
                  </div>
                  <div class="x_title">
                    <h2>Product</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">

                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:center;">
                          Plant
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <select class="form-control">
                            <option>Choose Plant</option>
                            <option>GSP#1</option>
                            <option>GSP#2</option>
                            <option>GSP#3</option>
                            <option>GSP#5</option>
                            <option>GSP#6</option>
                            <option>ESP</option>
                          </select>

                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:left;">
                            <!-- <span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i></span> -->
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".plant-modal-lg"><i class="fa fa-plus"></i> </button>
                        </div>
                        <!-- plant modal -->
                        <div class="modal fade plant-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Plant Management</h4>
                              </div>
                              <div class="modal-body">
                                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->



                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                  <br><br>
                                  <table class="table table-bordered" style="width:100%;">
                                    <thead style="width:100%;">
                                      <tr>
                                        <th><input type="checkbox"></th>
                                        <th>Plant Name</th>
                                        <th>Abbreviation</th>
                                        <th>Tools</th>
                                      <tr>
                                    </thead>
                                    <tbody style="width:100%;">
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td><input type="text" class="form-control" placeholder="Plant Name"></td>
                                        <td><input type="text" class="form-control" placeholder="Abbreviation"></td>
                                        <td><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>GSP#1</td>
                                        <td>G1</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>GSP#2</td>
                                        <td>G2</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>GSP#3</td>
                                        <td>G3</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>GSP#5</td>
                                        <td>G5</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>GSP#6</td>
                                        <td>G6</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>ESP</td>
                                        <td>ESP</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                              </div>
                              <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Run</button>
                              </div> -->

                            </div>
                          </div>
                        </div>
                        <!-- end plant modal -->


                        <!-- product modal -->
                        <div class="modal fade product-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Product Management</h4>
                              </div>
                              <div class="modal-body">
                                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->



                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                  <br><br>
                                  <table class="table table-bordered" style="width:100%;">
                                    <thead style="width:100%;">
                                      <tr>
                                        <th><input type="checkbox"></th>
                                        <th>Product Name</th>

                                        <th>Tools</th>
                                      <tr>
                                    </thead>
                                    <tbody style="width:100%;">
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td><input type="text" class="form-control" placeholder="Product Name"></td>

                                        <td><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Ethane</td>

                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Propane</td>

                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Pentane</td>

                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>LPG</td>

                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>


                                    </tbody>
                                  </table>
                                </div>
                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                              </div>
                              <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Run</button>
                              </div> -->

                            </div>
                          </div>
                        </div>
                        <!-- end product modal -->

                        <!-- Grade modal -->
                        <div class="modal fade grade-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Grade Management</h4>
                              </div>
                              <div class="modal-body">
                                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->



                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                  <br><br>
                                  <table class="table table-bordered" style="width:100%;">
                                    <thead style="width:100%;">
                                      <tr>
                                        <th><input type="checkbox"></th>
                                        <th>Grade</th>
                                        <th>Excel value</th>
                                        <th>Tools</th>
                                      <tr>
                                    </thead>
                                    <tbody style="width:100%;">
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td><input type="text" class="form-control" placeholder="Grade"></td>
                                        <td><input type="text" class="form-control" placeholder="Excel value"></td>
                                        <td><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Petro</td>
                                        <td>0</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a>
                                          <!-- <a href="#"><i class="fa fa-arrow-down"></i></a> -->
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Domestic</td>
                                        <td>1</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a>
                                          <!-- <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a> -->
                                        </td>
                                      </tr>

                                    </tbody>
                                  </table>
                                </div>
                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                              </div>
                              <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Run</button>
                              </div> -->

                            </div>
                          </div>
                        </div>
                        <!-- end Grade modal -->

                        <!-- Quality modal -->
                        <div class="modal fade quality-tag-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Quality Tag</h4>
                              </div>
                              <div class="modal-body">
                                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Excel Name
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Excel Name">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Unit
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Unit">
                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Exa Tag
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Exa Tag">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">

                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      <input type="checkbox">
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                                  Traget  Range
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">

                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      Traget value
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                                      <input type="text" class="form-control" placeholder="Traget value">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">

                                  </div>
                                </div>

                                <br>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
                              </div>

                            </div>
                          </div>
                        </div>
                        <!-- end Quality modal -->

                        <!-- Quality Accum modal -->
                        <div class="modal fade quality-accum-tag-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Quality Tag (Accum)</h4>
                              </div>
                              <div class="modal-body">
                                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Excel Name
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Excel Name">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Unit
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Unit">
                                  </div>
                                </div>
                                <br>

                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      <input type="checkbox">
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                                  Traget  Range
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">

                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      Traget value
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                                      <input type="text" class="form-control" placeholder="Traget value">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">

                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      <input type="checkbox">
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                                  Correct value
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">

                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      Min
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                                    <input type="text" class="form-control" placeholder="Min Value">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      Max
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Max Value">
                                  </div>
                                </div>

                                <br>
                                <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:right;">
                                    <div class="row" style="text-align:left;">
                                         <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                    </div>

                                    <br>
                                    <table class="table table-bordered" style="width:100%;">
                                      <thead style="width:100%;">
                                        <tr>
                                          <th><input type="checkbox"></th>

                                          <th>Exa Tag</th>
                                          <th>Convert Value</th>

                                          <th>Tools</th>
                                        <tr>
                                      </thead>
                                      <tbody style="width:100%;" style="text-align:left;">
                                        <tr>
                                          <td><input type="checkbox"></td>

                                          <td><select class="form-control">
                                            <option></option>
                                            <option></option>

                                          </select></td>
                                          <td><input type="text" class="form-control" placeholder="Convert Value"></td>
                                          <td><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span></td>
                                        </tr>
                                        <tr style="text-align:left;">
                                          <td><input type="checkbox"></td>

                                          <td>PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH4.PV</td>
                                          <td>0.82</td>

                                          <td><a href="#"><i class="fa fa-edit"></i></a>

                                          </td>
                                        </tr>
                                        <tr style="text-align:left;">
                                          <td><input type="checkbox"></td>

                                          <td>PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH6.PVPTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH6.P</td>
                                          <td>0.5</td>

                                          <td><a href="#"><i class="fa fa-edit"></i></a>

                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>




                                <br>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
                              </div>

                            </div>
                          </div>
                        </div>
                        <!-- end Quantity Accum modal -->

                        <!-- Quantity modal -->
                        <div class="modal fade quantity-tag-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Quantity Tag</h4>
                              </div>
                              <div class="modal-body">
                                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Excel Name
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Excel Name">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Unit
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Unit">
                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Exa Tag
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Exa Tag">
                                  </div>
                                  <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:right;">
                                    <input type="checkbox">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:left;">
                                    Mass PV
                                  </div>
                                  <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:right;">
                                    <input type="checkbox">
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:left;">
                                    Mass Sum
                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Proof
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Proof">
                                    <br>* แสดงเมื่อเป็น Mass PV
                                  </div>
                                  <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                    Convert Value
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                                    <input type="text" class="form-control" placeholder="Convert Value">
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
                              </div>

                            </div>
                          </div>
                        </div>
                        <!-- end Quantity modal -->

                        <!-- Control Value modal -->
                        <div class="modal fade control-value-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Control Value</h4>
                              </div>
                              <div class="modal-body">
                                <!-- <h4>คำแนะนำ</h4>
                                <p>คำอธิบายการใช้งานเมนู</p> -->
                                <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:right;">
                                    <div class="row" style="text-align:left;">
                                         <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                    </div>

                                    <br>
                                    <table class="table table-bordered" style="width:100%;">
                                      <thead style="width:100%;">
                                        <tr>
                                          <th style="text-align:center;"><input type="checkbox"></th>

                                          <th>Gas - Plant (Grade)</th>
                                          <th>Item - Unit</th>
                                          <th>Value</th>
                                          <th>Tools</th>
                                        <tr>
                                      </thead>
                                      <tbody style="width:100%;">
                                        <tr>
                                          <td style="text-align:center;"><input type="checkbox"></td>

                                          <td><select class="form-control">
                                            <option></option>
                                            <option></option>

                                          </select>
                                          </td>
                                          <td><select class="form-control">
                                                <option></option>
                                                <option></option>
                                              </select>
                                          </td>
                                          <td><input type="text" class="form-control" placeholder="Value"></td>
                                          <td style="text-align:center;"><i class="fa fa-edit"></i></td>
                                        </tr>
                                        <tr style="text-align:center;">
                                          <td style="text-align:center;"><input type="checkbox"></td>

                                          <td>C2 Process GSP#1
                                          </td>
                                          <td>C1 - %mol
                                          </td>
                                          <td style="width:15%;">> 1.9 </td>
                                          <td><i class="fa fa-edit"></i></td>
                                        </tr>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                        <!-- end Control Value modal -->

                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Product
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <select class="form-control">
                            <option>Choose Product</option>
                            <option>Product#1</option>
                            <option>Product#2</option>
                            <option>Product#3</option>
                            <option>Product#4</option>
                            <option>Product#5</option>
                          </select>

                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:left;">
                            <!-- <span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i></span> -->
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".product-modal-lg"><i class="fa fa-plus"></i> </button>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center;">

                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Default Grade
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <select class="form-control">
                            <option>Choose Default Grade</option>
                            <option>Petro</option>
                            <option>Demestic</option>
                          </select>

                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:left;">
                            <!-- <span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i></span> -->
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".grade-modal-lg"><i class="fa fa-plus"></i> </button>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center;">

                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Description
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <textarea id="message" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">

                          </textarea>
                        </div>

                    </div>
                  </div>

                </div>

              </div>


            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Quality Tag</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <ul class="nav navbar-left panel_toolbox">
                      <!-- <il>ตรวจสอบรายละเอียด
                      </il> -->

                      <il>
                          <!-- <span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> New Tag</span> -->
                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".quality-tag-modal-lg"><i class="fa fa-plus"></i> New Tag</button>
                      </il>
                      <il><span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                      </il>
                      <il>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".quality-accum-tag-modal-lg"><i class="fa fa-plus"></i> Accum Tag</button>
                        <!-- <span class="btn btn-primary" style="color:white;"> Accum Tag</span> -->
                      </il>
                    </ul>
                      <table class="table table-bordered" style="width:100%;">
                        <thead style="width:100%;">
                          <tr>
                            <th><input type="checkbox"></th>
                            <th>Excel Name</th>
                            <th style="width:30%;">Exa Tag</th>
                            <th>Traget Range</th>
                            <th>Unit</th>
                            <th>Control Value</th>
                            <th>Spec Value</th>
                            <th>Accum Tag</th>
                            <th>Tools</th>
                          <tr>
                        </thead>
                        <tbody style="width:100%;">
                          <tr>
                            <td><input type="checkbox"></td>
                            <td>G1C2_GCO_C1</td>
                            <td>PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH4.PV</td>
                            <td>1.9-2.0</td>
                            <td>%mol</td>
                            <td data-toggle="modal" data-target=".control-value-modal-lg">>1.9</td>
                            <td>>1.9</td>
                            <td>NO</td>
                            <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>

                            </td>
                          <tr>
                          <tr>
                            <td><input type="checkbox"></td>
                            <td>G1C2_GCO_C2</td>
                            <td>PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH6.PVPTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH6.P</td>
                            <td>-</td>
                            <td>%mol</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a> <a href="#"><i class="fa fa-arrow-up"></i></a>

                            </td>
                          <tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>G1C2_GCO_TC4</td>
                              <td>IC4 + NC4</td>
                              <td>-</td>
                              <td>%mol</td>
                              <td data-toggle="modal" data-target=".control-value-modal-lg">X1,X2</td>
                              <td>X1,X2</td>
                              <td>YES</td>
                              <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a> <a href="#"><i class="fa fa-arrow-up"></i></a>

                              </td>
                            <tr>

                        </tbody>
                      </table>


                  </div>

                </div>

              </div>


            </div>



            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Quantity Tag</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <ul class="nav navbar-left panel_toolbox">
                      <!-- <il>ตรวจสอบรายละเอียด
                      </il> -->

                      <!-- <il><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> New Tag</span> -->
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".quantity-tag-modal-lg"><i class="fa fa-plus"></i> New Tag</button>
                      </il>
                      <il><span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                      </il>
                      <!-- <il><span class="btn btn-primary" style="color:white;"> Accum Tag</span>
                      </il> -->
                    </ul>
                      <table class="table table-bordered" style="width:100%;">
                        <thead style="width:100%;">
                          <tr>
                            <th><input type="checkbox"></th>
                            <th>Excel Name</th>
                            <th style="width:30%;">Exa Tag</th>

                            <th>Unit</th>
                            <th>Type</th>
                            <th>Proof</th>
                            <th>Convert Value</th>
                            <th>Tools</th>
                          </tr>
                        </thead>
                        <tbody style="width:100%;">
                          <tr>
                            <td><input type="checkbox"></td>
                            <td>G1C2_GCO_C1</td>
                            <td>PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH4.PV</td>

                            <td>%mol</td>
                            <td>Mass PV</td>
                            <td>0.6</td>
                            <td>0.0169</td>
                            <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>

                            </td>
                          </tr>
                          <tr>
                            <td><input type="checkbox"></td>
                            <td>G1C2_GCO_C2</td>
                            <td>PTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH6.PVPTTGSP.GSP1.GC ONLINE.700-QR-005 ETHANE.NORMALIZE.700QR-5CH6.P</td>

                            <td>%mol</td>
                            <td>Mass Sum</td>
                            <td>-</td>
                            <td>-</td>
                            <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a> <a href="#"><i class="fa fa-arrow-up"></i></a>

                            </td>
                          </tr>


                        </tbody>
                      </table>


                  </div>

                </div>

              </div>


            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php include("./footer.php"); ?>
        <!-- /footer content -->
      </div>
    </div>


    <?php include("./footer_script.php"); ?>



    <!-- Flot -->
    <script>
      $(document).ready(function() {
        //random data
        var d1 = [
          [0, 1],
          [1, 9],
          [2, 6],
          [3, 10],
          [4, 5],
          [5, 17],
          [6, 6],
          [7, 10],
          [8, 7],
          [9, 11],
          [10, 35],
          [11, 9],
          [12, 12],
          [13, 5],
          [14, 3],
          [15, 4],
          [16, 9]
        ];

        //flot options
        var options = {
          series: {
            curvedLines: {
              apply: true,
              active: true,
              monotonicFit: true
            }
          },
          colors: ["#26B99A"],
          grid: {
            borderWidth: {
              top: 0,
              right: 0,
              bottom: 1,
              left: 1
            },
            borderColor: {
              bottom: "#7F8790",
              left: "#7F8790"
            }
          }
        };
        var plot = $.plot($("#placeholder3xx3"), [{
          label: "Registrations",
          data: d1,
          lines: {
            fillColor: "rgba(150, 202, 89, 0.12)"
          }, //#96CA59 rgba(150, 202, 89, 0.42)
          points: {
            fillColor: "#fff"
          }
        }], options);
      });
    </script>
    <!-- /Flot -->

    <!-- jQuery Sparklines -->
    <script>
      $(document).ready(function() {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'bar',
          height: '40',
          barWidth: 9,
          colorMap: {
            '7': '#a1a1a1'
          },
          barSpacing: 2,
          barColor: '#26B99A'
        });

        $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'line',
          width: '200',
          height: '40',
          lineColor: '#26B99A',
          fillColor: 'rgba(223, 223, 223, 0.57)',
          lineWidth: 2,
          spotColor: '#26B99A',
          minSpotColor: '#26B99A'
        });
      });
    </script>
    <!-- /jQuery Sparklines -->

    <!-- Doughnut Chart -->
    <script>
      $(document).ready(function() {
        var options = {
          legend: false,
          responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
              "Symbian",
              "Blackberry",
              "Other",
              "Android",
              "IOS"
            ],
            datasets: [{
              data: [15, 20, 30, 10, 30],
              backgroundColor: [
                "#BDC3C7",
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB"
              ],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });
      });
    </script>
    <!-- /Doughnut Chart -->

    <!-- bootstrap-daterangepicker -->
    <script type="text/javascript">
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- morris.js -->
    <script>
      $(document).ready(function() {
        Morris.Bar({
          element: 'graph_bar',
          data: [
            { "period": "Jan", "Hours worked": 80 },
            { "period": "Feb", "Hours worked": 125 },
            { "period": "Mar", "Hours worked": 176 },
            { "period": "Apr", "Hours worked": 224 },
            { "period": "May", "Hours worked": 265 },
            { "period": "Jun", "Hours worked": 314 },
            { "period": "Jul", "Hours worked": 347 },
            { "period": "Aug", "Hours worked": 287 },
            { "period": "Sep", "Hours worked": 240 },
            { "period": "Oct", "Hours worked": 211 }
          ],
          xkey: 'period',
          hideHover: 'auto',
          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          ykeys: ['Hours worked', 'sorned'],
          labels: ['Hours worked', 'SORN'],
          xLabelAngle: 60,
          resize: true
        });

        $MENU_TOGGLE.on('click', function() {
          $(window).resize();
        });
      });
    </script>
    <!-- /morris.js -->

    <!-- Skycons -->
    <script>
      var icons = new Skycons({
          "color": "#73879C"
        }),
        list = [
          "clear-day", "clear-night", "partly-cloudy-day",
          "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
          "fog"
        ],
        i;

      for (i = list.length; i--;)
        icons.set(list[i], list[i]);

      icons.play();
    </script>
    <!-- /Skycons -->

    <!-- gauge.js -->
    <script>
      var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
          length: 0.75,
          strokeWidth: 0.042,
          color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 100;
      gauge.animationSpeed = 32;
      gauge.set(80);
      gauge.setTextField(document.getElementById("gauge-text"));

      var target = document.getElementById('foo2'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 5000;
      gauge.animationSpeed = 32;
      gauge.set(4200);
      gauge.setTextField(document.getElementById("gauge-text2"));
    </script>
    <!-- /gauge.js -->
  </body>
</html>
