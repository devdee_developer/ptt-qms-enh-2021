<!-- jQuery -->
<script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="./assets/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="./assets/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="./assets/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- jQuery Sparklines -->
<script src="./assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- morris.js -->
<script src="./assets/vendors/raphael/raphael.min.js"></script>
<script src="./assets/vendors/morris.js/morris.min.js"></script>
<!-- gauge.js -->
<script src="./assets/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="./assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- Skycons -->
<script src="./assets/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="./assets/vendors/Flot/jquery.flot.js"></script>
<script src="./assets/vendors/Flot/jquery.flot.pie.js"></script>
<script src="./assets/vendors/Flot/jquery.flot.time.js"></script>
<script src="./assets/vendors/Flot/jquery.flot.stack.js"></script>
<script src="./assets/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="./assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="./assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="./assets/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="./assets/vendors/DateJS/build/date.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="./assets/vendors/moment/min/moment.min.js"></script>
<script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="./assets/build/js/custom.min.js"></script>
<script src="http://localhost:35729/livereload.js"></script>
