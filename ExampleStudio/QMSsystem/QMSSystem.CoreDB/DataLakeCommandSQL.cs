﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using QMSSystem.Model;
using QMSSystem.CoreDB.Services;

namespace QMSSystem.CoreDB
{
    class DataLakeCommandSQL
    {
        //product server
        public DataLakeCommandSQL() { _mConnectionString = "Data Source=ptt-gspcidb-p01.ptt.corp; Initial Catalog=GSP_QMS; User ID=gsp-qms-client; Password=cmL3h7V!"; }

        public DataLakeCommandSQL(string ConnectionString) { _mConnectionString = ConnectionString; }

        private string _mConnectionString { get; set; }

        public SqlConnection getConnection()
        {
            //local
            string connectionString = _mConnectionString; // "Data Source=.\\SQLEXPRESS; Initial Catalog=QMSSystem; Integrated Security=SSPI";

            SqlConnection connection = new SqlConnection(connectionString);

            return connection;
        }

        #region QMS_RAW_DATA

        public List<ViewQMS_RAW_DATA> getAllFromGSP_QMS(DateTime? dStratDate, DateTime? dEndDate)
        {
            List<ViewQMS_RAW_DATA> listResult = new List<ViewQMS_RAW_DATA>();
            try
            {
                string TextStratDate = "";
                string TextEndDate = "";
                //dStratDate = new DateTime(2020, 05, 22);
                //dEndDate = new DateTime(2020, 05, 23);
                if (dStratDate.HasValue && dEndDate.HasValue) 
                {
                    TextStratDate = dStratDate.Value.Year.ToString() + "-" + dStratDate.Value.Month.ToString() + "-" + dStratDate.Value.Day.ToString();
                    TextEndDate = dEndDate.Value.Year.ToString() + "-" + dEndDate.Value.Month.ToString() + "-" + dEndDate.Value.Day.ToString();
                }
                if ("" != TextStratDate && "" != TextEndDate)
                {
                    string szTable = "QMS_RAW_DATA"; // เปลี่ยนมาจาก GSP_QMS_TEST
                    string queryString = "SELECT * FROM " + szTable + " WHERE sampling_time >= '" + TextStratDate.ToString() +
                    "' and sampling_time < '" + TextEndDate.ToString() + "' ";
                    SqlConnection connection = getConnection();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            ViewQMS_RAW_DATA temp = new ViewQMS_RAW_DATA();
                            temp.measured = convertDatetimeData(reader[0].ToString());
                            temp.sampling_time = convertDatetimeData(reader[1].ToString());
                            temp.tag_name = reader[2].ToString();
                            temp.value = reader[3].ToString();
                            temp.status = reader[4].ToString();
                            listResult.Add(temp);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        reader.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public List<ViewQMS_RAW_DATA_ENH> getAllFromGSP_QMS_ENH(DateTime? dStratDate, DateTime? dEndDate)
        {
            List<ViewQMS_RAW_DATA_ENH> listResult = new List<ViewQMS_RAW_DATA_ENH>();
            try
            {
                string TextStratDate = "";
                string TextEndDate = "";
                //dStratDate = new DateTime(2020, 05, 22);
                //dEndDate = new DateTime(2020, 05, 23);
                if (dStratDate.HasValue && dEndDate.HasValue)
                {
                    TextStratDate = dStratDate.Value.Year.ToString() + "-" + dStratDate.Value.Month.ToString() + "-" + dStratDate.Value.Day.ToString();
                    TextEndDate = dEndDate.Value.Year.ToString() + "-" + dEndDate.Value.Month.ToString() + "-" + dEndDate.Value.Day.ToString();
                }
                if ("" != TextStratDate && "" != TextEndDate)
                {
                    string szTable = "QMS_RAW_DATA"; // เปลี่ยนมาจาก GSP_QMS_TEST
                    string queryString = "SELECT * FROM " + szTable + " WHERE sampling_time >= '" + TextStratDate.ToString() +
                    "' and sampling_time < '" + TextEndDate.ToString() + "' ";
                    SqlConnection connection = getConnection();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                ViewQMS_RAW_DATA_ENH temp = new ViewQMS_RAW_DATA_ENH();
                                temp.sampling_time = convertDatetimeData(reader[0].ToString());
                                temp.recorded_time = convertDatetimeData(reader[1].ToString());
                                temp.measured_time = convertDatetimeData(reader[2].ToString());
                                temp.tag_name = reader[3].ToString();
                                temp.value = (reader[4] != null) ? reader[4].ToString() : "";
                                temp.status = (reader[5] != null) ? reader[5].ToString() : "";
                                listResult.Add(temp);
                            }catch(Exception ex) { 
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        reader.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public List<ViewQMS_RAW_DATA_ENH> getAllNewRealTimeFromGSP_QMS_ENH(DateTime? dStratDate, DateTime? dEndDate, string condition)
        {
            List<ViewQMS_RAW_DATA_ENH> listResult = new List<ViewQMS_RAW_DATA_ENH>();
            try
            {
                string TextStratDate = "";
                string TextEndDate = "";
                if (dStratDate.HasValue && dEndDate.HasValue)
                {
                    TextStratDate = dStratDate.Value.Year.ToString() + "-" + dStratDate.Value.Month.ToString() + "-" + dStratDate.Value.Day.ToString();
                    TextEndDate = dEndDate.Value.Year.ToString() + "-" + dEndDate.Value.Month.ToString() + "-" + dEndDate.Value.Day.ToString();
                }
                if ("" != TextStratDate && "" != TextEndDate)
                {
                    string szTable = "dbo_gsp.vw_gsp_qms_realtime_data";
                    string queryString = "SELECT * FROM " + szTable + " WHERE sampling_time >= '" + TextStratDate.ToString() +
                    "' and sampling_time < '" + TextEndDate.ToString() + "' and tag_name in (" + condition + ")";
                    SqlConnection connection = getConnection();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                ViewQMS_RAW_DATA_ENH temp = new ViewQMS_RAW_DATA_ENH();
                                temp.sampling_time = convertDatetimeData(reader[0].ToString());
                                //temp.recorded_time = convertDatetimeData(reader[1].ToString());
                                temp.measured_time = convertDatetimeData(reader[1].ToString());
                                temp.tag_name = reader[2].ToString();
                                temp.value = (reader[3] != null) ? reader[3].ToString() : "";
                                temp.status = (reader[4] != null) ? reader[4].ToString() : "";
                                listResult.Add(temp);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        reader.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public DateTime convertDatetimeData(string dateData)
        {
            DateTime dTime = DateTime.Now;
            try
            {
                dTime = Convert.ToDateTime(dateData);
            }
            catch
            {

            }

            return dTime;
        }

        public DateTime? convertDatetimeDataEx(string dateData)
        {
            DateTime? dTime = null;
            try
            {
                dTime = Convert.ToDateTime(dateData);
            }
            catch
            {

            }

            return dTime;
        }

        public decimal? convertDecimalData(string dateData)
        {
            decimal? dTime = null;
            try
            {
                dTime = Convert.ToDecimal(dateData);
            }
            catch
            {

            }

            return dTime;
        }


        #endregion

        #region QMS_TR_BLUEPRINT_ABNORMAL
        //public List<CreateQMS_TR_BLUEPRINT_ABNORMAL> getAllFromQMS_TR_BLUEPRINT_ABNORMALAll_ENH(DateTime? dStratDate, DateTime? dEndDate)
        //{
        //    List<CreateQMS_TR_BLUEPRINT_ABNORMAL> listResult = new List<CreateQMS_TR_BLUEPRINT_ABNORMAL>();
        //    try
        //    {
        //        string queryString = "SELECT * FROM stg_blueprint.Abnormal";
        //        SqlConnection connection = getConnection();
        //        SqlCommand command = new SqlCommand(queryString, connection);
        //        connection.Open();
        //        SqlDataReader reader = command.ExecuteReader();
        //        try
        //        {
        //            while (reader.Read())
        //            {
        //                CreateQMS_TR_BLUEPRINT_ABNORMAL temp = new CreateQMS_TR_BLUEPRINT_ABNORMAL();
        //                temp.BlueprintId = Convert.ToInt32(reader[0]);
        //                temp.Plant = reader[1].ToString();
        //                temp.AbnormalType = reader[2].ToString();
        //                temp.EventId = convertIntData(reader[3].ToString());
        //                temp.InputSource = reader[4].ToString();
        //                temp.StartDateTime = convertDatetimeData(reader[5].ToString());
        //                temp.EndDateTime = convertDatetimeDataEx(reader[6].ToString());
        //                temp.TotalTimeHr = convertFloatData(reader[7].ToString());
        //                temp.Cause = convertStringData(reader[8].ToString());
        //                temp.ShortCause = convertStringData(reader[9].ToString());
        //                temp.Trip = convertBoolData(reader[10].ToString());
        //                temp.EquipmentFailure = convertBoolData(reader[11].ToString());
        //                temp.SD_Category = convertStringData(reader[12].ToString());
        //                temp.Define = convertStringData(reader[13].ToString());
        //                temp.SD_Reason = convertStringData(reader[14].ToString());
        //                temp.EquipmentCode = convertStringData(reader[15].ToString());
        //                temp.Source = convertStringData(reader[16].ToString());
        //                temp.PlanType = convertStringData(reader[17].ToString());
        //                temp.IsDeleted = convertBoolData(reader[18].ToString());
        //                temp.UpdateDateTime = convertDatetimeData(reader[19].ToString());
        //                listResult.Add(temp);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //        finally
        //        {
        //            reader.Close();
        //        }
        //        connection.Close();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return listResult;
        //}
        public List<CreateQMS_TR_BLUEPRINT_ABNORMAL> getAllFromQMS_TR_BLUEPRINT_ABNORMALAll(DateTime? dStratDate, DateTime? dEndDate)
        {
            List<CreateQMS_TR_BLUEPRINT_ABNORMAL> listResult = new List<CreateQMS_TR_BLUEPRINT_ABNORMAL>();
            try
            {
                    string queryString = "SELECT * FROM Abnormal";
                    SqlConnection connection = getConnection();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            CreateQMS_TR_BLUEPRINT_ABNORMAL temp = new CreateQMS_TR_BLUEPRINT_ABNORMAL();
                            temp.BlueprintId = Convert.ToInt32(reader[0]);
                            temp.Plant = reader[1].ToString();
                            temp.AbnormalType = reader[2].ToString();
                            temp.EventId = convertIntData(reader[3].ToString());
                            temp.InputSource = reader[4].ToString();
                            temp.StartDateTime = convertDatetimeData(reader[5].ToString());
                            temp.EndDateTime = convertDatetimeDataEx(reader[6].ToString());
                            temp.TotalTimeHr = convertFloatData(reader[7].ToString());
                            temp.Cause = convertStringData(reader[8].ToString());
                            temp.ShortCause = convertStringData(reader[9].ToString());
                            temp.Trip = convertBoolData(reader[10].ToString());
                            temp.EquipmentFailure = convertBoolData(reader[11].ToString());
                            temp.SD_Category = convertStringData(reader[12].ToString());
                            temp.Define = convertStringData(reader[13].ToString());
                            temp.SD_Reason = convertStringData(reader[14].ToString());
                            temp.EquipmentCode = convertStringData(reader[15].ToString());
                            temp.Source = convertStringData(reader[16].ToString());
                            temp.PlanType = convertStringData(reader[17].ToString());
                            temp.IsDeleted = convertBoolData(reader[18].ToString());
                            temp.UpdateDateTime = convertDatetimeData(reader[19].ToString());
                            listResult.Add(temp);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        reader.Close();
                    }
                    connection.Close();
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }
        public string convertStringData(string String)
        {
            string dstring = null;
            try
            {
                if (String == "") dstring = null;
            }
            catch
            {

            }

            return dstring;
        }
        public int? convertIntData(string Int)
        {
            int? dInt = null;
            try
            {
                dInt = Convert.ToInt32(Int);
            }
            catch
            {

            }

            return dInt;
        }

        public long convertLongData(string Int)
        {
            long dInt = 0;
            try
            {
                dInt = Convert.ToInt64(Int);
            }
            catch
            {

            }

            return dInt;
        }

        public float? convertFloatData(string Float)
        {
            float? dFloat = null;
            try
            {
                dFloat = float.Parse(Float);
            }
            catch
            {

            }

            return dFloat;
        }
        public bool? convertBoolData(string Bool)
        {
            bool? dBool = null;
            try
            {
                dBool = Convert.ToBoolean(Bool);
            }
            catch
            {

            }

            return dBool;
        }
        #endregion



        #region QMS_TR_EVENT_CAL

        public List<ViewQMS_TR_EVENT_CAL> getAllFromQMS_TR_EVENT_CAL()
        {
            List<ViewQMS_TR_EVENT_CAL> listResult = new List<ViewQMS_TR_EVENT_CAL>();
            try
            {

                string szTable = "EVENT_CAL"; // เปลี่ยนมาจาก GSP_QMS_TEST
                string queryString = "SELECT * FROM " + szTable; // +" WHERE sampling_time >= '" + TextStratDate.ToString() +  "' and sampling_time < '" + TextEndDate.ToString() + "' ";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_TR_EVENT_CAL temp = new ViewQMS_TR_EVENT_CAL();
                        temp.event_id = convertLongData(reader[0].ToString());
                    
                        temp.event_type = reader[1].ToString();
                        temp.plant = reader[2].ToString();
                        temp.start_date = convertDatetimeDataEx( reader[3].ToString());
                        temp.end_date = convertDatetimeDataEx(reader[4].ToString());
                        temp.total_event_time = convertDecimalData(reader[5].ToString());
                        listResult.Add(temp);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        #endregion

        #region EVENT_RAW_DATA

        public List<ViewQMS_TR_EVENT_RAW_DATA> getAllFromEVENT_RAW_DATA(DateTime? dStratDate, DateTime? dEndDate)
        {
            List<ViewQMS_TR_EVENT_RAW_DATA> listResult = new List<ViewQMS_TR_EVENT_RAW_DATA>();
            try
            {
                string TextStratDate = "";
                string TextEndDate = "";
                //dStratDate = new DateTime(2020, 05, 16);
                //dEndDate = new DateTime(2020, 05, 17);
                if (dStratDate.HasValue && dEndDate.HasValue)
                {
                    TextStratDate = dStratDate.Value.Year.ToString() + "-" + dStratDate.Value.Month.ToString() + "-" + dStratDate.Value.Day.ToString();
                    TextEndDate = dEndDate.Value.Year.ToString() + "-" + dEndDate.Value.Month.ToString() + "-" + dEndDate.Value.Day.ToString();
                }
                if ("" != TextStratDate && "" != TextEndDate)
                {
                    string szTable = "EVENT_RAW_DATA"; // เปลี่ยนมาจาก GSP_QMS_TEST
                    string queryString = "SELECT * FROM " + szTable + " WHERE recorded_time >= '" + TextStratDate.ToString() +
                    "' and recorded_time < '" + TextEndDate.ToString() + "' ";
                    SqlConnection connection = getConnection();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            ViewQMS_TR_EVENT_RAW_DATA temp = new ViewQMS_TR_EVENT_RAW_DATA();
                            temp.event_id = Convert.ToInt32(reader[0]);
                            temp.recorded_time = convertDatetimeData(reader[1].ToString());
                            temp.measured_time = convertDatetimeData(reader[2].ToString());
                            temp.value = reader[3].ToString();
                            temp.tag_name = reader[4].ToString();
                            listResult.Add(temp);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        reader.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }
        
        public ViewRealTimeFromEVENT_RAW_DATA_NEW getAllRealTimeFromEVENT_RAW_DATA_NEW(DateTime? dStratDate, DateTime? dEndDate, string condition)
        {
            ViewRealTimeFromEVENT_RAW_DATA_NEW Result = new ViewRealTimeFromEVENT_RAW_DATA_NEW();
            List<ViewQMS_TR_EVENT_RAW_DATA> listResult = new List<ViewQMS_TR_EVENT_RAW_DATA>();
            try
            {
                string TextStratDate = "";
                string TextEndDate = "";
                if (dStratDate.HasValue && dEndDate.HasValue)
                {
                    TextStratDate = dStratDate.Value.Year.ToString() + "-" + dStratDate.Value.Month.ToString() + "-" + dStratDate.Value.Day.ToString();
                    TextEndDate = dEndDate.Value.Year.ToString() + "-" + dEndDate.Value.Month.ToString() + "-" + dEndDate.Value.Day.ToString();
                }
                if ("" != TextStratDate && "" != TextEndDate)
                {
                    string szTable = "dbo_gsp.vw_gsp_qms_realtime_data";
                    string queryString = "SELECT * FROM " + szTable + " WHERE sampling_time >= '" + TextStratDate.ToString() +
                    "' and sampling_time < '" + TextEndDate.ToString() + "' and tag_name in (" + condition +")";
                    SqlConnection connection = getConnection();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            ViewQMS_TR_EVENT_RAW_DATA temp = new ViewQMS_TR_EVENT_RAW_DATA();
                            //temp.event_id = Convert.ToInt32(reader[0]);
                            temp.recorded_time = convertDatetimeData(reader[0].ToString());
                            int Minute = temp.recorded_time.Minute;
                            //if (Minute % 5 == 0)
                            //{
                            //    break;
                            //}
                            temp.measured_time = convertDatetimeData(reader[1].ToString());
                            temp.tag_name = reader[2].ToString();
                            temp.value = reader[3].ToString();
                            listResult.Add(temp);
                        }
                        Result.viewQMS_TR_EVENT_RAW_DATAs = listResult;
                        Result.error = listResult.Count().ToString();
                    }
                    catch (Exception ex)
                    {
                        Result.error = ex.Message;
                        if (ex.InnerException != null)
                        {
                            Result.error = Result.error + ", InnerException : " + ex.InnerException.Message;
                        }
                        Result.error = Result.error + ", StackTrace : " + ex.StackTrace;
                    }
                    finally
                    {
                        reader.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Result.error = ex.Message;
                if (ex.InnerException != null)
                {
                    Result.error = Result.error + ", InnerException : " + ex.InnerException.Message;
                }
                Result.error = Result.error + ", StackTrace : " + ex.StackTrace;
            }
            return Result;
        }
        
        #endregion

        #region QMS_RP_OFF_CONTROL

        public List<ViewQMS_RP_OFF_CONTROL> getFromQMS_RP_OFF_CONTROL()
        {
            List<ViewQMS_RP_OFF_CONTROL> listResult = new List<ViewQMS_RP_OFF_CONTROL>();
            try
            {
                    string queryString = "SELECT * FROM QMS_RP_OFF_CONTROL";
                    SqlConnection connection = getConnection();
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            ViewQMS_RP_OFF_CONTROL temp = new ViewQMS_RP_OFF_CONTROL();
                            temp.ID = Convert.ToInt32(reader[0]);
                            temp.OFF_CONTROL_TYPE = Convert.ToByte(reader[1]);
                            temp.NAME = reader[2].ToString();
                            temp.START_DATE = convertDatetimeData(reader[3].ToString());
                            temp.END_DATE = convertDatetimeData(reader[4].ToString());
                            temp.DOC_STATUS = Convert.ToByte(reader[5]);
                            temp.TITLE_DESC = reader[6].ToString();
                            temp.DETAIL_DESC = reader[7].ToString();
                            temp.TITLE_GRAPH = reader[8].ToString();
                            temp.TITLE_GRAPH_X = reader[9].ToString();
                            temp.TITLE_GRAPH_Y = reader[10].ToString();
                            temp.DELETE_FLAG = Convert.ToByte(reader[11]);
                            temp.CREATE_USER = reader[12].ToString();
                            temp.CREATE_DATE = convertDatetimeData(reader[13].ToString());
                            temp.UPDATE_USER = reader[14].ToString();
                            temp.UPDATE_DATE = convertDatetimeData(reader[15].ToString());
                            listResult.Add(temp);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        reader.Close();
                    }
                    connection.Close();
                }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public long SaveSQLListQMS_RP_OFF_CONTROL(QMS_RP_OFF_CONTROL Data)
        {
            long result = 0;

            try
            {
                if (null != Data)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("INSERT INTO QMS_RP_OFF_CONTROL   "
                                            + " ( ID, OFF_CONTROL_TYPE, NAME ,  "
                                            + " START_DATE, END_DATE, DOC_STATUS, TITLE_DESC, DETAIL_DESC , "
                                            + " TITLE_GRAPH, TITLE_GRAPH_X, TITLE_GRAPH_Y, DELETE_FLAG, CREATE_USER , "
                                            + " CREATE_DATE, UPDATE_USER, UPDATE_DATE ) "
                                            + " VALUES(@ID,  @OFF_CONTROL_TYPE, @NAME, "
                                            + " @START_DATE,  @END_DATE, @DOC_STATUS, @TITLE_DESC, @DETAIL_DESC, "
                                            + " @TITLE_GRAPH,  @TITLE_GRAPH_X, @TITLE_GRAPH_Y, @DELETE_FLAG, @CREATE_USER, "
                                            + " @CREATE_DATE,  @UPDATE_USER, @UPDATE_DATE )  ", conn);

                    conn.Open();
                    QMS_RP_OFF_CONTROL temp = new QMS_RP_OFF_CONTROL();

                    SQLcommand.Parameters.AddWithValue("@ID", temp.ID);
                    SQLcommand.Parameters.AddWithValue("@OFF_CONTROL_TYPE", temp.OFF_CONTROL_TYPE);
                    SQLcommand.Parameters.AddWithValue("@NAME", temp.NAME);
                    SQLcommand.Parameters.AddWithValue("@START_DATE", temp.START_DATE);
                    SQLcommand.Parameters.AddWithValue("@END_DATE", temp.END_DATE);
                    SQLcommand.Parameters.AddWithValue("@DOC_STATUS", temp.DOC_STATUS);
                    SQLcommand.Parameters.AddWithValue("@TITLE_DESC", temp.TITLE_DESC);
                    SQLcommand.Parameters.AddWithValue("@DETAIL_DESC", temp.DETAIL_DESC);
                    SQLcommand.Parameters.AddWithValue("@TITLE_GRAPH", temp.TITLE_GRAPH);
                    SQLcommand.Parameters.AddWithValue("@TITLE_GRAPH_X", temp.TITLE_GRAPH_X);
                    SQLcommand.Parameters.AddWithValue("@TITLE_GRAPH_Y", temp.TITLE_GRAPH_Y);
                    SQLcommand.Parameters.AddWithValue("@DELETE_FLAG", temp.DELETE_FLAG);
                    SQLcommand.Parameters.AddWithValue("@CREATE_USER", temp.CREATE_USER);
                    SQLcommand.Parameters.AddWithValue("@CREATE_DATE", temp.CREATE_DATE);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_USER", temp.UPDATE_USER);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", temp.UPDATE_DATE);

                    
                    SQLcommand.Parameters["@ID"].Value = Data.ID;
                    SQLcommand.Parameters["@OFF_CONTROL_TYPE"].Value = Data.OFF_CONTROL_TYPE;
                    SQLcommand.Parameters["@NAME"].Value = Data.NAME;
                    SQLcommand.Parameters["@START_DATE"].Value = Data.START_DATE;
                    SQLcommand.Parameters["@END_DATE"].Value = Data.END_DATE;
                    SQLcommand.Parameters["@DOC_STATUS"].Value = Data.DOC_STATUS;
                    SQLcommand.Parameters["@TITLE_DESC"].Value = Data.TITLE_DESC;
                    SQLcommand.Parameters["@DETAIL_DESC"].Value = Data.DETAIL_DESC;
                    SQLcommand.Parameters["@TITLE_GRAPH"].Value = Data.TITLE_GRAPH;
                    SQLcommand.Parameters["@TITLE_GRAPH_X"].Value = Data.TITLE_GRAPH_X;
                    SQLcommand.Parameters["@TITLE_GRAPH_Y"].Value = Data.TITLE_GRAPH_Y;
                    SQLcommand.Parameters["@DELETE_FLAG"].Value = Data.DELETE_FLAG;
                    SQLcommand.Parameters["@CREATE_USER"].Value = Data.CREATE_USER;
                    SQLcommand.Parameters["@CREATE_DATE"].Value = Data.CREATE_DATE;
                    SQLcommand.Parameters["@UPDATE_USER"].Value = Data.UPDATE_USER;
                    SQLcommand.Parameters["@UPDATE_DATE"].Value = Data.UPDATE_DATE;

                    SQLcommand.ExecuteNonQuery();

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public long UpdateSQLListQMS_RP_OFF_CONTROL(QMS_RP_OFF_CONTROL Data)
        {
            long result = 0;

            try
            {
                if (null != Data)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("UPDATE QMS_RP_OFF_CONTROL SET "
                                            + " OFF_CONTROL_TYPE = @OFF_CONTROL_TYPE,"
                                            + " NAME = @NAME, "
                                            + " START_DATE = @START_DATE,"
                                            + " END_DATE = @END_DATE,"
                                            + " DOC_STATUS = @DOC_STATUS,"
                                            + " TITLE_DESC = @TITLE_DESC,"
                                            + " DETAIL_DESC = @DETAIL_DESC,"
                                            + " TITLE_GRAPH = @TITLE_GRAPH,"
                                            + " TITLE_GRAPH_X = @TITLE_GRAPH_X,"
                                            + " TITLE_GRAPH_Y = @TITLE_GRAPH_Y,"
                                            + " DELETE_FLAG = @DELETE_FLAG,"
                                            + " CREATE_USER = @CREATE_USER,"
                                            + " CREATE_DATE = @CREATE_DATE,"
                                            + " UPDATE_USER = @UPDATE_USER,"
                                            + " UPDATE_DATE = @UPDATE_DATE "
                                            + " WHERE ID = @ID", conn);

                    conn.Open();
                    QMS_RP_OFF_CONTROL temp = new QMS_RP_OFF_CONTROL();

                    SQLcommand.Parameters.AddWithValue("@ID", temp.ID);
                    SQLcommand.Parameters.AddWithValue("@OFF_CONTROL_TYPE", temp.OFF_CONTROL_TYPE);
                    SQLcommand.Parameters.AddWithValue("@NAME", temp.NAME);
                    SQLcommand.Parameters.AddWithValue("@START_DATE", temp.START_DATE);
                    SQLcommand.Parameters.AddWithValue("@END_DATE", temp.END_DATE);
                    SQLcommand.Parameters.AddWithValue("@DOC_STATUS", temp.DOC_STATUS);
                    SQLcommand.Parameters.AddWithValue("@TITLE_DESC", temp.TITLE_DESC);
                    SQLcommand.Parameters.AddWithValue("@DETAIL_DESC", temp.DETAIL_DESC);
                    SQLcommand.Parameters.AddWithValue("@TITLE_GRAPH", temp.TITLE_GRAPH);
                    SQLcommand.Parameters.AddWithValue("@TITLE_GRAPH_X", temp.TITLE_GRAPH_X);
                    SQLcommand.Parameters.AddWithValue("@TITLE_GRAPH_Y", temp.TITLE_GRAPH_Y);
                    SQLcommand.Parameters.AddWithValue("@DELETE_FLAG", temp.DELETE_FLAG);
                    SQLcommand.Parameters.AddWithValue("@CREATE_USER", temp.CREATE_USER);
                    SQLcommand.Parameters.AddWithValue("@CREATE_DATE", temp.CREATE_DATE);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_USER", temp.UPDATE_USER);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", temp.UPDATE_DATE);


                    SQLcommand.Parameters["@ID"].Value = Data.ID;
                    SQLcommand.Parameters["@OFF_CONTROL_TYPE"].Value = Data.OFF_CONTROL_TYPE;
                    SQLcommand.Parameters["@NAME"].Value = Data.NAME;
                    SQLcommand.Parameters["@START_DATE"].Value = Data.START_DATE;
                    SQLcommand.Parameters["@END_DATE"].Value = Data.END_DATE;
                    SQLcommand.Parameters["@DOC_STATUS"].Value = Data.DOC_STATUS;
                    SQLcommand.Parameters["@TITLE_DESC"].Value = Data.TITLE_DESC;
                    SQLcommand.Parameters["@DETAIL_DESC"].Value = Data.DETAIL_DESC;
                    SQLcommand.Parameters["@TITLE_GRAPH"].Value = Data.TITLE_GRAPH;
                    SQLcommand.Parameters["@TITLE_GRAPH_X"].Value = Data.TITLE_GRAPH_X;
                    SQLcommand.Parameters["@TITLE_GRAPH_Y"].Value = Data.TITLE_GRAPH_Y;
                    SQLcommand.Parameters["@DELETE_FLAG"].Value = Data.DELETE_FLAG;
                    SQLcommand.Parameters["@CREATE_USER"].Value = Data.CREATE_USER;
                    SQLcommand.Parameters["@CREATE_DATE"].Value = Data.CREATE_DATE;
                    SQLcommand.Parameters["@UPDATE_USER"].Value = Data.UPDATE_USER;
                    SQLcommand.Parameters["@UPDATE_DATE"].Value = Data.UPDATE_DATE;

                    SQLcommand.ExecuteNonQuery();

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }
        #endregion

        #region QMS_RP_OFF_CONTROL_SUMMARY

        public List<ViewQMS_RP_OFF_CONTROL_SUMMARY> getFromQMS_RP_OFF_CONTROL_SUMMARY()
        {
            List<ViewQMS_RP_OFF_CONTROL_SUMMARY> listResult = new List<ViewQMS_RP_OFF_CONTROL_SUMMARY>();
            try
            {
                string queryString = "SELECT * FROM QMS_RP_OFF_CONTROL_SUMMARY";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_RP_OFF_CONTROL_SUMMARY temp = new ViewQMS_RP_OFF_CONTROL_SUMMARY();
                        temp.ID = Convert.ToInt32(reader[0]);
                        temp.RP_OFF_CONTROL_ID = Convert.ToInt32(reader[1]);
                        temp.PLANT_ID = Convert.ToInt32(reader[2]);
                        temp.PRODUCT_ID = Convert.ToInt32(reader[3]);
                        temp.VALUE = convertDecimalData(reader[4].ToString()).Value;
                        temp.DATA_TYPE = Convert.ToByte(reader[5]);
                        temp.DELETE_FLAG = Convert.ToByte(reader[6]);
                        temp.CREATE_USER = reader[7].ToString();
                        temp.CREATE_DATE = convertDatetimeData(reader[8].ToString());
                        temp.UPDATE_USER = reader[9].ToString();
                        temp.UPDATE_DATE = convertDatetimeData(reader[10].ToString());
                        listResult.Add(temp);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public long SaveSQLListQMS_RP_OFF_CONTROL_SUMMARY(QMS_RP_OFF_CONTROL_SUMMARY Data)
        {
            long result = 0;

            try
            {
                if (null != Data)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("INSERT INTO QMS_RP_OFF_CONTROL_SUMMARY   "
                                            + " ( ID, RP_OFF_CONTROL_ID, PLANT_ID ,  "
                                            + " PRODUCT_ID, VALUE, DATA_TYPE, DELETE_FLAG, CREATE_USER , "
                                            + " CREATE_DATE, UPDATE_USER, UPDATE_DATE ) "
                                            + " VALUES(@ID,  @RP_OFF_CONTROL_ID, @PLANT_ID, "
                                            + " @PRODUCT_ID,  @VALUE, @DATA_TYPE, @DELETE_FLAG, @CREATE_USER, "
                                            + " @CREATE_DATE,  @UPDATE_USER, @UPDATE_DATE )  ", conn);

                    conn.Open();
                    QMS_RP_OFF_CONTROL_SUMMARY temp = new QMS_RP_OFF_CONTROL_SUMMARY();

                    SQLcommand.Parameters.AddWithValue("@ID", temp.ID);
                    SQLcommand.Parameters.AddWithValue("@RP_OFF_CONTROL_ID", temp.RP_OFF_CONTROL_ID);
                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", temp.PLANT_ID);
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", temp.PRODUCT_ID);
                    SQLcommand.Parameters.AddWithValue("@VALUE", temp.VALUE);
                    SQLcommand.Parameters.AddWithValue("@DATA_TYPE", temp.DATA_TYPE);
                    SQLcommand.Parameters.AddWithValue("@DELETE_FLAG", temp.DELETE_FLAG);
                    SQLcommand.Parameters.AddWithValue("@CREATE_USER", temp.CREATE_USER);
                    SQLcommand.Parameters.AddWithValue("@CREATE_DATE", temp.CREATE_DATE);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_USER", temp.UPDATE_USER);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", temp.UPDATE_DATE);


                    SQLcommand.Parameters["@ID"].Value = Data.ID;
                    SQLcommand.Parameters["@RP_OFF_CONTROL_ID"].Value = Data.RP_OFF_CONTROL_ID;
                    SQLcommand.Parameters["@PLANT_ID"].Value = Data.PLANT_ID;
                    SQLcommand.Parameters["@PRODUCT_ID"].Value = Data.PRODUCT_ID;
                    SQLcommand.Parameters["@VALUE"].Value = Data.VALUE;
                    SQLcommand.Parameters["@DATA_TYPE"].Value = Data.DATA_TYPE;
                    SQLcommand.Parameters["@DELETE_FLAG"].Value = Data.DELETE_FLAG;
                    SQLcommand.Parameters["@CREATE_USER"].Value = Data.CREATE_USER;
                    SQLcommand.Parameters["@CREATE_DATE"].Value = Data.CREATE_DATE;
                    SQLcommand.Parameters["@UPDATE_USER"].Value = Data.UPDATE_USER;
                    SQLcommand.Parameters["@UPDATE_DATE"].Value = Data.UPDATE_DATE;

                    SQLcommand.ExecuteNonQuery();

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public long UpdateSQLListQMS_RP_OFF_CONTROL_SUMMARY(QMS_RP_OFF_CONTROL_SUMMARY Data)
        {
            long result = 0;

            try
            {
                if (null != Data)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("UPDATE QMS_RP_OFF_CONTROL_SUMMARY SET "
                                            + " RP_OFF_CONTROL_ID = @RP_OFF_CONTROL_ID,"
                                            + " PLANT_ID = @PLANT_ID, "
                                            + " PRODUCT_ID = @PRODUCT_ID,"
                                            + " VALUE = @VALUE,"
                                            + " DATA_TYPE = @DATA_TYPE,"
                                            + " DELETE_FLAG = @DELETE_FLAG,"
                                            + " CREATE_USER = @CREATE_USER,"
                                            + " CREATE_DATE = @CREATE_DATE,"
                                            + " UPDATE_USER = @UPDATE_USER,"
                                            + " UPDATE_DATE = @UPDATE_DATE "
                                            + " WHERE ID = @ID", conn);

                    conn.Open();
                    QMS_RP_OFF_CONTROL_SUMMARY temp = new QMS_RP_OFF_CONTROL_SUMMARY();

                    SQLcommand.Parameters.AddWithValue("@ID", temp.ID);
                    SQLcommand.Parameters.AddWithValue("@RP_OFF_CONTROL_ID", temp.RP_OFF_CONTROL_ID);
                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", temp.PLANT_ID);
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", temp.PRODUCT_ID);
                    SQLcommand.Parameters.AddWithValue("@VALUE", temp.VALUE);
                    SQLcommand.Parameters.AddWithValue("@DATA_TYPE", temp.DATA_TYPE);
                    SQLcommand.Parameters.AddWithValue("@DELETE_FLAG", temp.DELETE_FLAG);
                    SQLcommand.Parameters.AddWithValue("@CREATE_USER", temp.CREATE_USER);
                    SQLcommand.Parameters.AddWithValue("@CREATE_DATE", temp.CREATE_DATE);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_USER", temp.UPDATE_USER);
                    SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", temp.UPDATE_DATE);


                    SQLcommand.Parameters["@ID"].Value = Data.ID;
                    SQLcommand.Parameters["@RP_OFF_CONTROL_ID"].Value = Data.RP_OFF_CONTROL_ID;
                    SQLcommand.Parameters["@PLANT_ID"].Value = Data.PLANT_ID;
                    SQLcommand.Parameters["@PRODUCT_ID"].Value = Data.PRODUCT_ID;
                    SQLcommand.Parameters["@VALUE"].Value = Data.VALUE;
                    SQLcommand.Parameters["@DATA_TYPE"].Value = Data.DATA_TYPE;
                    SQLcommand.Parameters["@DELETE_FLAG"].Value = Data.DELETE_FLAG;
                    SQLcommand.Parameters["@CREATE_USER"].Value = Data.CREATE_USER;
                    SQLcommand.Parameters["@CREATE_DATE"].Value = Data.CREATE_DATE;
                    SQLcommand.Parameters["@UPDATE_USER"].Value = Data.UPDATE_USER;
                    SQLcommand.Parameters["@UPDATE_DATE"].Value = Data.UPDATE_DATE;

                    SQLcommand.ExecuteNonQuery();

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }
        
        #endregion

        #region QMS_RP_OFF_CONTROL_STAMP
        public List<ViewRP_OFF_CONTROL_SUMMARY> getFromQMS_RP_OFF_CONTROL_STAMP()
        {
            List<ViewRP_OFF_CONTROL_SUMMARY> listResult = new List<ViewRP_OFF_CONTROL_SUMMARY>();
            try
            {
                string queryString = "SELECT * FROM QMS_RP_OFF_CONTROL_STAMP";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        ViewRP_OFF_CONTROL_SUMMARY temp = new ViewRP_OFF_CONTROL_SUMMARY();
                        temp.ID = Convert.ToInt32(reader[0]);
                        temp.RP_OFF_CONTROL = Convert.ToInt32(reader[1]);
                        temp.PLANT = reader[2].ToString();
                        temp.PRODUCT = reader[3].ToString();
                        temp.OFF_VALUE = convertDecimalData(reader[4].ToString()).Value;
                        temp.INPUT_VOLUME = convertDecimalData(reader[5].ToString()).Value;
                        temp.PERCENT_VOLUME = convertDecimalData(reader[6].ToString()).Value;
                        listResult.Add(temp);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }
            return listResult;
        }

        public long SaveSQLListQMS_RP_OFF_CONTROL_STAMP(List<ViewRP_OFF_CONTROL_SUMMARY> listData)
        {
            long result = 0;

            try
            {
                if (null != listData)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("INSERT INTO QMS_RP_OFF_CONTROL_STAMP   "
                                            + " ( ID, RP_OFF_CONTROL, PLANT ,  "
                                            + " PRODUCT, OFF_VALUE, INPUT_VOLUME, PERCENT_VOLUME) "
                                            + " VALUES(@ID,  @RP_OFF_CONTROL, @PLANT, "
                                            + " @PRODUCT,  @OFF_VALUE, @INPUT_VOLUME, @PERCENT_VOLUME)  ", conn);

                    conn.Open();
                    ViewRP_OFF_CONTROL_SUMMARY temp = new ViewRP_OFF_CONTROL_SUMMARY();

                    SQLcommand.Parameters.AddWithValue("@ID", temp.ID);
                    SQLcommand.Parameters.AddWithValue("@RP_OFF_CONTROL", temp.RP_OFF_CONTROL);
                    SQLcommand.Parameters.AddWithValue("@PLANT", temp.PLANT);
                    SQLcommand.Parameters.AddWithValue("@PRODUCT", temp.PRODUCT);
                    SQLcommand.Parameters.AddWithValue("@OFF_VALUE", temp.OFF_VALUE);
                    SQLcommand.Parameters.AddWithValue("@INPUT_VOLUME", temp.INPUT_VOLUME);
                    SQLcommand.Parameters.AddWithValue("@PERCENT_VOLUME", temp.PERCENT_VOLUME);

                    foreach (ViewRP_OFF_CONTROL_SUMMARY dtEXQProduct in listData)
                    {
                        SQLcommand.Parameters["@ID"].Value = dtEXQProduct.ID;
                        SQLcommand.Parameters["@RP_OFF_CONTROL"].Value = dtEXQProduct.RP_OFF_CONTROL;
                        SQLcommand.Parameters["@PLANT"].Value = dtEXQProduct.PLANT;
                        SQLcommand.Parameters["@PRODUCT"].Value = dtEXQProduct.PRODUCT;
                        SQLcommand.Parameters["@OFF_VALUE"].Value = dtEXQProduct.OFF_VALUE;
                        SQLcommand.Parameters["@INPUT_VOLUME"].Value = dtEXQProduct.INPUT_VOLUME;
                        SQLcommand.Parameters["@PERCENT_VOLUME"].Value = dtEXQProduct.PERCENT_VOLUME;
                        SQLcommand.ExecuteNonQuery();
                    }
                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public long UpdateSQLListQMS_RP_OFF_CONTROL_STAMP(List<ViewRP_OFF_CONTROL_SUMMARY> listData)
        {
            long result = 0;

            try
            {
                if (null != listData)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("UPDATE QMS_RP_OFF_CONTROL_STAMP SET  "
                                            + " PLANT = @PLANT, "
                                            + " PRODUCT = @PRODUCT, "
                                            + " OFF_VALUE = @OFF_VALUE, "
                                            + " INPUT_VOLUME = @INPUT_VOLUME, "
                                            + " PERCENT_VOLUME = @PERCENT_VOLUME "
                                            + " WHERE RP_OFF_CONTROL = @RP_OFF_CONTROL", conn);

                    conn.Open();
                    ViewRP_OFF_CONTROL_SUMMARY temp = new ViewRP_OFF_CONTROL_SUMMARY();

                    SQLcommand.Parameters.AddWithValue("@ID", temp.ID);
                    SQLcommand.Parameters.AddWithValue("@RP_OFF_CONTROL", temp.RP_OFF_CONTROL);
                    SQLcommand.Parameters.AddWithValue("@PLANT", temp.PLANT);
                    SQLcommand.Parameters.AddWithValue("@PRODUCT", temp.PRODUCT);
                    SQLcommand.Parameters.AddWithValue("@OFF_VALUE", temp.OFF_VALUE);
                    SQLcommand.Parameters.AddWithValue("@INPUT_VOLUME", temp.INPUT_VOLUME);
                    SQLcommand.Parameters.AddWithValue("@PERCENT_VOLUME", temp.PERCENT_VOLUME);

                    foreach (ViewRP_OFF_CONTROL_SUMMARY dtEXQProduct in listData)
                    {
                        SQLcommand.Parameters["@ID"].Value = dtEXQProduct.ID;
                        SQLcommand.Parameters["@RP_OFF_CONTROL"].Value = dtEXQProduct.RP_OFF_CONTROL;
                        SQLcommand.Parameters["@PLANT"].Value = dtEXQProduct.PLANT;
                        SQLcommand.Parameters["@PRODUCT"].Value = dtEXQProduct.PRODUCT;
                        SQLcommand.Parameters["@OFF_VALUE"].Value = dtEXQProduct.OFF_VALUE;
                        SQLcommand.Parameters["@INPUT_VOLUME"].Value = dtEXQProduct.INPUT_VOLUME;
                        SQLcommand.Parameters["@PERCENT_VOLUME"].Value = dtEXQProduct.PERCENT_VOLUME;
                        SQLcommand.ExecuteNonQuery();
                    }
                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public long DeleteSQLListQMS_RP_OFF_CONTROL_STAMPByID(long RP_OFF_CONTROL)
        {
            long result = 0;

            try
            {
                if (null != RP_OFF_CONTROL)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("DELETE QMS_RP_OFF_CONTROL_STAMP "
                                            + " WHERE RP_OFF_CONTROL = @RP_OFF_CONTROL", conn);

                    conn.Open();
                    ViewRP_OFF_CONTROL_SUMMARY temp = new ViewRP_OFF_CONTROL_SUMMARY();

                    SQLcommand.Parameters.AddWithValue("@RP_OFF_CONTROL", temp.RP_OFF_CONTROL);

                    SQLcommand.Parameters["@RP_OFF_CONTROL"].Value = RP_OFF_CONTROL;
                    SQLcommand.ExecuteNonQuery();

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }
        #endregion

        #region QMS_TR_COA_DATA_DATA_LAKE
        public long SaveSQLListQMS_TR_COA_DATA_DATA_LAKE(QMS_TR_COA_DATA_DATA_LAKE Data)
        {
            long result = 0;

            try
            {
                if (null != Data)
                {
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("INSERT INTO QMS_TR_COA_DATA   "
                                            + " ( QMS_ID,  SAMPLING_DATE, PRODUCT_NAME ,  "
                                            + " TANK_NAME, DISPLAY_VALUE, UNIT_NAME , ITEM_NAME ) "
                                            + " VALUES(  @QMS_ID, @SAMPLING_DATE, @PRODUCT_NAME, "
                                            + " @TANK_NAME,  @DISPLAY_VALUE,  @UNIT_NAME, @ITEM_NAME )  ", conn);

                    conn.Open();
                    QMS_TR_COA_DATA_DATA_LAKE temp = new QMS_TR_COA_DATA_DATA_LAKE();

                    SQLcommand.Parameters.AddWithValue("@QMS_ID", temp.ID);
                    SQLcommand.Parameters.AddWithValue("@SAMPLING_DATE", temp.SAMPLING_DATE);
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_NAME", temp.PRODUCT_NAME);
                    SQLcommand.Parameters.AddWithValue("@TANK_NAME", temp.TANK_NAME);
                    SQLcommand.Parameters.AddWithValue("@DISPLAY_VALUE", temp.DISPLAY_VALUE);
                    SQLcommand.Parameters.AddWithValue("@UNIT_NAME", temp.UNIT_NAME);
                    SQLcommand.Parameters.AddWithValue("@ITEM_NAME", temp.ITEM_NAME);

                    SQLcommand.Parameters["@QMS_ID"].Value = Data.ID;
                    SQLcommand.Parameters["@SAMPLING_DATE"].Value = Data.SAMPLING_DATE;
                    SQLcommand.Parameters["@PRODUCT_NAME"].Value = Data.PRODUCT_NAME;
                    SQLcommand.Parameters["@TANK_NAME"].Value = Data.TANK_NAME;
                    SQLcommand.Parameters["@DISPLAY_VALUE"].Value = Data.DISPLAY_VALUE;
                    SQLcommand.Parameters["@UNIT_NAME"].Value = Data.UNIT_NAME;
                    SQLcommand.Parameters["@ITEM_NAME"].Value = Data.ITEM_NAME;

                    SQLcommand.ExecuteNonQuery();

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }


        public long DeleteCOAByDate(DateTime stratDate, DateTime endDate)
        {
            long result = 0;

            try
            {
                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE QMS_TR_COA_DATA "
                                        + " WHERE [SAMPLING_DATE] >= @START_DATE and [SAMPLING_DATE] <= @END_DATE", conn);

                conn.Open();
                QMS_TR_COA_DATA_DATA_LAKE_DELETE temp = new QMS_TR_COA_DATA_DATA_LAKE_DELETE();

                SQLcommand.Parameters.AddWithValue("@START_DATE", temp.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", temp.END_DATE);

                SQLcommand.Parameters["@START_DATE"].Value = stratDate;
                SQLcommand.Parameters["@END_DATE"].Value = endDate;
                SQLcommand.ExecuteNonQuery();

                conn.Close();

            }
            catch (Exception ex)
            {
            }
            return result;
        }
        #endregion

    }
}
