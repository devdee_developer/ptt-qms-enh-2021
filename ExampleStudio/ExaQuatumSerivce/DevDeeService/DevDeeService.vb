﻿Imports System.IO
Imports System.Threading
Imports System.Configuration
Imports System.Reflection
Imports QMSSystem.Model
Imports QMSSystem.CoreDB.Services
Imports System.Data.SqlClient
Imports System.Data.EntityClient

Public Class DevDeeService
    Dim objSession As QUANTUMAUTOMATIONLib.Session2
    Dim WithEvents objDataAccess As QUANTUMAUTOMATIONLib.QDataAccess
    '  Attribute objDataAccess.VB_VarHelpID = -1
    Dim objBrowser As QUANTUMAUTOMATIONLib.Browse2
    Dim objQualityHelper As QUANTUMAUTOMATIONLib.QQualityHelper

    Dim rsBrowseResults As ADODB.Recordset

    Private m_nCount As Integer = 3
    Private m_bError As Boolean = False

    Protected Overrides Sub OnStart(ByVal args() As String)
        'System.Diagnostics.Debugger.Launch() 
        Me.WriteToFile("Service started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.ScheduleService()
    End Sub

    Protected Overrides Sub OnStop()
        Me.WriteToFile("Service stopped at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Me.Schedular.Dispose()
    End Sub

    Private Schedular As Timer

    Public Sub ScheduleService()
        Try
            Schedular = New Timer(New TimerCallback(AddressOf SchedularCallback))
            Dim mode As String = ConfigurationManager.AppSettings("Mode").ToUpper()
            Me.WriteToFile((Convert.ToString("Service Mode: ") & mode) + " {0}")

            m_nCount = 3
            'Set the Default Time.
            Dim scheduledTime As DateTime = DateTime.MinValue

            If mode = "DAILY" Then
                'Get the Scheduled Time from AppSettings.
                'System.Diagnostics.Debugger.Launch()
                scheduledTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings("ScheduledTime"))
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next day.
                    scheduledTime = scheduledTime.AddDays(1)
                End If
            End If

            If mode.ToUpper() = "INTERVAL" Then
                'Get the Interval in Minutes from AppSettings.
                Dim intervalMinutes As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))

                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(intervalMinutes)
                End If
            End If

            Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
            Dim schedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds)

            Me.WriteToFile((Convert.ToString("Service scheduled to run after: ") & schedule) + " {0}")

            'Get the difference in Minutes between the Scheduled and Current Time.
            Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)

            'Change the Timer's Due Time.
            Schedular.Change(dueTime, Timeout.Infinite)
        Catch ex As Exception
            WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace)

            'Stop the Windows Service.
            Using serviceController As New System.ServiceProcess.ServiceController("SimpleService")
                serviceController.[Stop]()
            End Using
        End Try
    End Sub

    Private Sub SchedularCallback(e As Object)

        Dim i As Integer
        Me.WriteToFile("Service Log: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))


        'For i = 0 To m_nCount
        '    getGSP5Data()
        '    If (False = m_bError) Then
        '        Exit For
        '    End If
        '    Thread.Sleep(1000)
        'Next 

        Dim activeDate As DateTime = Date.Now
        Dim yesterdayDate As DateTime 'start date
        Dim connectionString As String
        connectionString = GetConnectionString()
        Dim db As New QMSSystem.CoreDB.QMSDBEntities(connectionString)
        Me.WriteToFile("Start : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Try
            yesterdayDate = New DateTime(activeDate.Year, activeDate.Month, activeDate.Day, 0, 0, 0).AddDays(-1).AddMinutes(-2) 'yesterday minus 2 minute 

            'loadExaProductTagToDB(db, yesterdayDate) 'load Product
            'Threading.Thread.Sleep(500)
            'loadExaReduceFeedTagToDB(db, yesterdayDate) 'load Reduce Feed
            'Threading.Thread.Sleep(500)
            'loadExaDowntimeTagToDB(db, yesterdayDate) 'load Downtime
            'Threading.Thread.Sleep(500)
            'loadExaDataByScheduleToDB(db) 'load by schedule
            'Threading.Thread.Sleep(500)

            'loadAllExaPlantProductToDB(db, yesterdayDate)
            'Threading.Thread.Sleep(500) 'load Product
            'loadAllExaDowntimeTagToDB(db, yesterdayDate)
            'Threading.Thread.Sleep(500) 'load Product
            'loadAllExaReduceTagToDB(db, yesterdayDate)
            'Threading.Thread.Sleep(500) 'load Downtime
            'loadAllExaDataByScheduleToDB(db)

            loadAllExaPlantProduct(db, yesterdayDate)
            Threading.Thread.Sleep(500) 'load Product
            loadAllExaDowntimeTag(db, yesterdayDate)
            Threading.Thread.Sleep(500) 'load Product
            loadAllExaReduceTag(db, yesterdayDate)
            Threading.Thread.Sleep(500) 'load Downtime
            loadAllExaDataBySchedule(db)

        Catch ex As Exception
            Me.WriteToFile(ex.Message)
        End Try


        Me.WriteToFile("End : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        'Reset scheeduleService
        Me.ScheduleService()
    End Sub

    Private Sub WriteToFile(text As String)
        Dim dirName As String
        Dim path As String
        Try
            dirName = AppDomain.CurrentDomain.BaseDirectory ' [Assembly].GetExecutingAssembly.Location 'Directory.GetCurrentDirectory()

            path = dirName & "\\" & DateTime.Now.ToString("yyyyMMdd") & ".log"

            Using writer As New StreamWriter(path, True)
                writer.WriteLine(text)
                writer.Close()
            End Using
        Catch
        End Try

    End Sub

    Private Sub getGSP5Data()
        Dim objMDA As QUANTUMAUTOMATIONLib.QDataAccessMulti
        Dim dQueryTime As Date
        Dim dQueryTime1 As Date
        Dim dToday As Date

        ' Dimension the filter parameters to return interpolated data
        Dim vFilterParameters As Object
        Dim vFilterParms(1) As Object
        Dim vFilterParmsTest(2) As Object
        Dim lFilterId As Long
        Dim lNumPeriods As Long

        ' Dimension variable to receive the data
        Dim vData As Object
        Dim vValue As Object
        Dim vQuality As Object
        Dim vValue1 As Object
        Dim vQuality1 As Object
        Dim vTimestamp As Object

        m_bError = False

        Try
            Me.WriteToFile("Start ExaQuatum at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))

            ' Items are requested by using Role-Based Namespace Access strings
            'sItem =  "PTTGSP.GSP1.GC ONLINE.SALES GAS.700QR-2N2.PV.Value" '"MyNamespace.Tag1.Value"

            ' Create new Exaquantum QDataAccessMulti object
            objMDA = New QUANTUMAUTOMATIONLib.QDataAccessMulti
            'objMDA.Add(sItem)
            'Test Value
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3504-QT-001 RVP.3504QT001GCIU.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.3504.FLOW.3504FI005.PV.Value")
            objMDA.Add("PTTGSP.GSP1.GC ONLINE.SALES GAS.700QR-2N2.PV.Value")

            'Ethane Section 
            'objMDA.Add("PTTGSP.GSP5.3519.FLOW.3519FI002.PV.Value xxx")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR2 C2 TO ETU.3519QT002CH4B.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR2 C2 TO ETU.3519QT002C2H6B.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR2 C2 TO ETU.3519QT002C3H8B.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR2 C2 TO ETU.3519QT0022IC4B.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR2 C2 TO ETU.3519QT002NC4B.PV.Value")

            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR2 C2 TO ETU.3519QT002CO2B.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR1 C2 TO TOC.3519QT002CH4A.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR1 C2 TO TOC.3519QT002C2H6A.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR1 C2 TO TOC.3519QT002C3H8A.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR1 C2 TO TOC.3519QT002IC4A.PV.Value")

            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR1 C2 TO TOC.3519QT002NC4A.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR1 C2 TO TOC.3519QT002CO2A.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.GC ONLINE.3519-QT-002 STR1 C2 TO TOC.3519QT002H2SA.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.3519.FLOW.3519FI002.PV.Value")
            'objMDA.Add("PTTGSP.GSP5.3519.FLOW.3519FI002.PV.Value")

            dToday = DateTime.Today

            dQueryTime = dToday.AddDays(-1) '1440 = 1days
            dQueryTime1 = dToday
            objMDA.SetQueryTimes(StartTime:=dQueryTime, EndTime:=dQueryTime1)
            ' Set filter parameters to return interpolated data
            lFilterId = 1
            lNumPeriods = 200

            vFilterParmsTest(0) = 2
            vFilterParmsTest(1) = 120
            vFilterParmsTest(2) = True

            vFilterParameters = vFilterParmsTest

            objMDA.SetFilterParameters(vFilterParameters)
            ' Read filtered data from yesterday to now


            'Dim data As Object
            vData = objMDA.ReadValue()
            'Iterate through the returned data retrieving the history values
            Dim lNumberOfPoints As Long
            Dim lmaxColume As Long
            Dim lPoint As Long
            Dim ColumePoint As Long
            Dim writeData As String

            lNumberOfPoints = UBound(vData(1, 0), 2)
            lmaxColume = UBound(vData, 2)


            'Write header'
            writeData = "Order, Date"
            For ColumePoint = 0 To lmaxColume
                writeData = writeData & "," & vData(0, ColumePoint)
            Next
            WriteToFile(writeData)

            'Write Data 
            For lPoint = 0 To lNumberOfPoints
                Try
                    'vValue = vData(1, 0)(0, lPoint)
                    'vQuality = vData(1, 0)(1, lPoint)
                    'vTimestamp = vData(1, 0)(2, lPoint)

                    'vValue1 = vData(1, 1)(0, lPoint)
                    'vQuality1 = vData(1, 1)(1, lPoint) 
                    writeData = "#" & (lPoint + 1).ToString() & "," & vData(1, 0)(2, lPoint)

                    For ColumePoint = 0 To lmaxColume
                        writeData = writeData & "," & vData(1, ColumePoint)(0, lPoint)
                    Next

                    WriteToFile(writeData)

                    'WriteToFile("#" & (lPoint + 1).ToString() & "::" & (vTimestamp).ToString() & "::" & vValue & "::" & vValue1)

                Catch ex As Exception
                    WriteToFile(ex.Message)
                End Try

            Next
        Catch ex As Exception

            WriteToFile(ex.Message)
            m_nCount = m_nCount - 1
            m_bError = True
        End Try

        Me.WriteToFile("Stop ExaQuatum at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        objMDA = Nothing

    End Sub


    'Exa quatum with QMS module
    Public Sub loadExaDataByScheduleToDB(db As QMSSystem.CoreDB.QMSDBEntities)

        Dim configDataServices As New ConfigServices("ExaQuatum")
        Dim listSchedule As List(Of ViewQMS_ST_EXAQUATUM_SCHEDULE)
        Dim actionDate As DateTime

        Try
            'Step 1 load all schedule 
            listSchedule = configDataServices.getQMS_ST_EXAQUATUM_SCHEDULEListByStatus(db, EXA_SCHEDULE_STATUS.PENDING)

            If listSchedule.Count() > 0 Then

                For Each schedule As ViewQMS_ST_EXAQUATUM_SCHEDULE In listSchedule
                    Try
                        actionDate = New DateTime(schedule.START_DATE.Year, schedule.START_DATE.Month, schedule.START_DATE.Day)
                        'Step 2 loop day until finished 
                        Do Until (actionDate > schedule.END_DATE)
                            loadExaProductTagByPlantIDToDB(db, actionDate, schedule.PLANT_ID)
                            Threading.Thread.Sleep(500)

                            loadExaReduceFeedTagToDBByPlantAndActiveDate(db, schedule.PLANT_ID, actionDate)
                            Threading.Thread.Sleep(500)

                            loadExaDowntimeTagToDBByPlantAndActiveDate(db, schedule.PLANT_ID, actionDate)
                            Threading.Thread.Sleep(500)

                            actionDate = actionDate.AddDays(1)
                        Loop

                        schedule.DOC_STATUS = EXA_SCHEDULE_STATUS.CONFIRM
                    Catch ex As Exception
                        schedule.DOC_STATUS = EXA_SCHEDULE_STATUS.FAIL
                    End Try

                    'Step 3 update status
                    Dim createModel As New CreateQMS_ST_EXAQUATUM_SCHEDULE()
                    createModel.ID = schedule.ID
                    createModel.PLANT_ID = schedule.PLANT_ID
                    createModel.START_DATE = schedule.START_DATE
                    createModel.END_DATE = schedule.END_DATE
                    createModel.DOC_STATUS = schedule.DOC_STATUS

                    configDataServices.SaveQMS_ST_EXAQUATUM_SCHEDULE(db, createModel)

                Next

            End If

        Catch ex As Exception

        End Try
    End Sub
    Public Sub loadExaReduceFeedTagToDBByPlantAndActiveDate(db As QMSSystem.CoreDB.QMSDBEntities, plantID As Long, actionDate As DateTime)

        Dim listExaTag As List(Of ViewQMS_MA_REDUCE_FEED_DETAIL) 
        Dim vData As Object
        Dim lNumberOfPoints As Long
        Dim lmaxColume As Long
        Dim startDate As DateTime = actionDate 'actionDate.AddDays(-1) '1440 = 1days
        Dim endDate As DateTime = actionDate.AddDays(1) 'actionDate

        Dim masterDataServices As New MasterDataService("ExaQuatum")
        Dim transDataServices As New TransactionService("ExaQuatum")
        Dim configDataServices As New ConfigServices("ExaQuatum")

        Try
            listExaTag = masterDataServices.getQMS_MA_REDUCE_FEED_DETAILActiveListByPlantAndActiveDate(db, plantID, actionDate)
            vData = getReduceExaTagData(listExaTag, startDate, endDate)
            lNumberOfPoints = UBound(vData(1, 0), 2)
            lmaxColume = UBound(vData, 2)


            For ColumePoint = 0 To lmaxColume

                Dim listProductData As New List(Of CreateQMS_TR_REDUCE_FEED_DATA)() 'new list data.
                Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.

                Dim columnCount As Long = 0
                Dim ExatagID As Long = 0
                Dim CurrentExaTag As New ViewQMS_MA_REDUCE_FEED_DETAIL()
                Dim REDUCE_FEED_STATUS As Integer

                For Each qtyExaTag As ViewQMS_MA_REDUCE_FEED_DETAIL In listExaTag
                    If ColumePoint = columnCount Then
                        ExatagID = qtyExaTag.REDUCE_FEED_ID
                        CurrentExaTag = qtyExaTag
                        Exit For
                    End If
                    columnCount += 1 'Increase for column checking...
                Next

                For lPoint = 0 To lNumberOfPoints

                    Try
                        Dim createProductData As New CreateQMS_TR_REDUCE_FEED_DATA()
                        Dim tagValue As Decimal = 0
                        Dim convertValue As Decimal = 0
                        Dim OFF_CONTROL_FLAG_0 As Byte = 0
                        Dim OFF_SPEC_FLAG_0 As Byte = 0
                        Dim REDUCE_FEED_FLAG As Byte = 0

                        If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                            tagValue = vData(1, ColumePoint)(0, lPoint)
                        Else
                            tagValue = 0
                            REDUCE_FEED_STATUS = 1 ' Value cannot read 
                        End If
                        convertValue = getConvertValue(tagValue, CurrentExaTag)

                        'Check Off Control and Spec
                        createProductData.ID = 0 'add New 
                        createProductData.REDUCE_FEED_FLAG = REDUCE_FEED_STATUS
                        createProductData.REDUCE_FEED_FLAG_0 = REDUCE_FEED_STATUS
                        createProductData.TAG_DATE = vData(1, 0)(2, lPoint)
                        createProductData.TAG_VALUE = tagValue
                        createProductData.TAG_CONVERT = convertValue
                        createProductData.REDUCE_FEED_DETAIL_ID = CurrentExaTag.ID

                        listProductData.Add(createProductData)
                    Catch ex As Exception
                        Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                        createError.ERROR_LEVEL = 1
                        createError.LOG_DATE = Date.Now
                        createError.PROCESS_DESC = ex.Message
                        createError.PROCESS_NAME = "EXAQUATUM"

                        listError.Add(createError)
                    End Try
                Next

                'Save data
                Try
                    transDataServices.SaveListQMS_TR_REDUCE_FEED_DATA(db, listProductData)
                    '.SaveListQMS_TR_PRODUCT_DATA(db, listProductData)
                    configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
                Catch ex As Exception
                    Me.WriteToFile(ex.Message) 'MessageBox.Show(ex.Message)
                End Try

                ' Exit For 'Temp exit add for test
            Next
        Catch

        End Try

    End Sub
    Public Sub loadExaReduceFeedTagToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            'Step 1 load all plant
            Dim masterDataServices As New MasterDataService("ExaQuatum")
            'Dim transDataServices As New TransactionService("ExaQuatum")
            'Dim configDataServices As New ConfigServices("ExaQuatum")

            Dim listPlant As List(Of ViewQMS_MA_PLANT) = masterDataServices.getQMS_MA_PLANTActiveList(db)
            'Dim listExaTag As List(Of ViewQMS_MA_REDUCE_FEED_DETAIL)

            'Dim vData As Object
            'Dim lNumberOfPoints As Long
            'Dim lmaxColume As Long
            'Dim startDate As DateTime = actionDate 'actionDate.AddDays(-1) '1440 = 1days
            'Dim endDate As DateTime = actionDate.AddDays(1) 'actionDate


            If listPlant.Count() > 0 Then
                For Each plant As ViewQMS_MA_PLANT In listPlant
                    'Step 2 list Active ReduceFeed Tag
                    loadExaReduceFeedTagToDBByPlantAndActiveDate(db, plant.ID, actionDate)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub loadExaDowntimeTagToDBByPlantAndActiveDate(db As QMSSystem.CoreDB.QMSDBEntities, plantID As Long, actionDate As DateTime)

        Dim masterDataServices As New MasterDataService("ExaQuatum")
        Dim transDataServices As New TransactionService("ExaQuatum")
        Dim configDataServices As New ConfigServices("ExaQuatum")

        Dim listExaTag As List(Of ViewQMS_MA_DOWNTIME_DETAIL)

        Dim vData As Object
        Dim lNumberOfPoints As Long
        Dim lmaxColume As Long
        Dim startDate As DateTime = actionDate 'actionDate.AddDays(-1) '1440 = 1days
        Dim endDate As DateTime = actionDate.AddDays(1) 'actionDate

        Try
            listExaTag = masterDataServices.getQMS_MA_DOWNTIME_DETAILActiveListByPlantAndActiveDate(db, plantID, actionDate)
            vData = getDownloadTagData(listExaTag, startDate, endDate)
            lNumberOfPoints = UBound(vData(1, 0), 2)
            lmaxColume = UBound(vData, 2)

            For ColumePoint = 0 To lmaxColume

                Dim listProductData As New List(Of CreateQMS_TR_PRODUCT_DOWNTIME_DATA)() 'new list data.
                Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.

                Dim columnCount As Long = 0
                Dim ExatagID As Long = 0
                Dim CurrentExaTag As New ViewQMS_MA_DOWNTIME_DETAIL()

                For Each qtyExaTag As ViewQMS_MA_DOWNTIME_DETAIL In listExaTag
                    If ColumePoint = columnCount Then
                        ExatagID = qtyExaTag.DOWNTIME_ID
                        CurrentExaTag = qtyExaTag
                        Exit For
                    End If
                    columnCount += 1 'Increase for column checking...
                Next

                For lPoint = 0 To lNumberOfPoints

                    Try
                        Dim createProductData As New CreateQMS_TR_PRODUCT_DOWNTIME_DATA()
                        Dim tagValue As Decimal = 0
                        Dim convertValue As Decimal = 0
                        Dim OFF_CONTROL_FLAG_0 As Byte = 0
                        Dim OFF_SPEC_FLAG_0 As Byte = 0
                        Dim DOWNTIME_FLAG As Byte = 0

                        If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                            tagValue = vData(1, ColumePoint)(0, lPoint)
                        Else
                            tagValue = 0
                            DOWNTIME_FLAG = 1 ' Value cannot read 
                        End If
                        convertValue = getConvertValue(tagValue, CurrentExaTag) 'tagValue * CurrentExaTag.CONVERT_VALUE

                        'Check Off Control and Spec
                        createProductData.ID = 0 'add New 
                        createProductData.DOWNTIME_FLAG = DOWNTIME_FLAG
                        createProductData.DOWNTIME_FLAG_0 = DOWNTIME_FLAG
                        createProductData.TAG_DATE = vData(1, 0)(2, lPoint)
                        createProductData.TAG_VALUE = tagValue
                        createProductData.TAG_CONVERT = convertValue
                        createProductData.DOWNTIME_DETAIL_ID = CurrentExaTag.ID

                        listProductData.Add(createProductData)
                    Catch ex As Exception
                        Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                        createError.ERROR_LEVEL = 1
                        createError.LOG_DATE = Date.Now
                        createError.PROCESS_DESC = ex.Message
                        createError.PROCESS_NAME = "EXAQUATUM"

                        listError.Add(createError)
                    End Try
                Next

                'Save data
                Try
                    transDataServices.SaveListQMS_TR_PRODUCT_DOWNTIME_DATA(db, listProductData)
                    '.SaveListQMS_TR_PRODUCT_DATA(db, listProductData)
                    configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
                Catch ex As Exception
                    'MessageBox.Show(ex.Message)
                    Me.WriteToFile(ex.Message)
                End Try

                ' Exit For 'Temp exit add for test
            Next
        Catch

        End Try

    End Sub
    Public Sub loadExaDowntimeTagToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            'Step 1 load all plant
            Dim masterDataServices As New MasterDataService("ExaQuatum") 

            Dim listPlant As List(Of ViewQMS_MA_PLANT) = masterDataServices.getQMS_MA_PLANTActiveList(db)
             

            If listPlant.Count() > 0 Then
                For Each plant As ViewQMS_MA_PLANT In listPlant
                    'Step 2 list Active ReduceFeed Tag
                    loadExaDowntimeTagToDBByPlantAndActiveDate(db, plant.ID, actionDate)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub loadExaProductTagToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)

        Try
            Dim startDate As Date
            Dim endDate As Date

            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim listPlant As New List(Of ViewQMS_MA_PLANT)()
            Dim listProduct As New List(Of ViewQMS_MA_PRODUCT)()
            Dim listTag As New List(Of ViewQMS_MA_EXQ_TAG)()
            Dim listPlantProduct As New List(Of ViewQMS_MA_PRODUCT_MAPPING)()
            'Dim GAS As Byte = 2 'Check Enum data
            'Dim Spec As Byte = 1 'Check Enum data
            'Dim listControlGroup As List(Of ViewQMS_MA_CONTROL) = masterDataServices.getQMS_MA_CONTROLListByGroupType(db, GAS) '(byte) CONTROL_GROUP_TYPE.GAS);
            'Dim listSpecGroup As List(Of ViewQMS_MA_CONTROL) = masterDataServices.getQMS_MA_CONTROLListByGroupType(db, Spec) '(byte) CONTROL_GROUP_TYPE.SPEC);
            'Dim listControlData As New List(Of ViewQMS_MA_CONTROL_DATA)
            'Dim listSpecData As New List(Of ViewQMS_MA_CONTROL_DATA)

            Dim tempControlSpec As New ViewQMS_MA_CONTROL_DATA

            'Pull all control data
            'If listControlGroup.Count() > 0 Then
            '    Dim listGroupId() As Long
            '    listGroupId = (From dt In listControlGroup Select dt.ID).ToArray()
            '    listControlData = masterDataServices.getQMS_MA_CONTROL_DATAByListControlId(db, listGroupId)
            'End If
            'If (listSpecGroup.Count() > 0) Then 
            '    Dim listSpecId() = (From dt In listSpecGroup Select dt.ID).ToArray()
            '    listSpecData = masterDataServices.getQMS_MA_CONTROL_DATAByListControlId(db, listSpecId)
            'End If


            'Dim listControl As New List(Of View
            'Dim writeData As String
            'Dim nLong As Long

            'listPlant = maSerive.getQMS_MA_PLANTActiveList(db)

            listPlantProduct = masterDataServices.getQMS_MA_PRODUCT_MAPPINGList(db)

            'Step 1. get PlantAndProductMapping
            If listPlantProduct.Count() > 0 Then
                startDate = actionDate 'actionDate.AddDays(-1) '1440 = 1days
                endDate = actionDate.AddDays(1) 'actionDate

                For Each plantProduct As ViewQMS_MA_PRODUCT_MAPPING In listPlantProduct

                    'Step 2 get Tag
                    Dim listQualityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listQuantityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listAccumTag As New List(Of ViewQMS_MA_EXQ_TAG)()

                    listQualityTag = masterDataServices.getQualityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listQuantityTag = masterDataServices.getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listAccumTag = masterDataServices.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)

                    'Step 3 save product exatra to database
                    If listQualityTag IsNot Nothing And listQualityTag.Count() > 0 Then
                        loadExaDataToDatabase(db, listQualityTag, plantProduct, startDate, endDate)
                    End If

                    If listQuantityTag IsNot Nothing And listQuantityTag.Count() > 0 Then
                        loadExaDataToDatabase(db, listQuantityTag, plantProduct, startDate, endDate)
                    End If

                    If listAccumTag IsNot Nothing And listAccumTag.Count() > 0 Then
                        loadExaAccumDataToDatabase(db, listAccumTag, plantProduct, startDate, endDate)
                    End If

                    'Exit For 'Temp exit add for test
                Next
            End If

        Catch ex As Exception
            ' MessageBox.Show(ex.Message)
        End Try

    End Sub
    Public Sub loadExaProductTagByPlantIDToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime, plantId As Long)

        Try
            Dim startDate As Date
            Dim endDate As Date

            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim listPlant As New List(Of ViewQMS_MA_PLANT)()
            Dim listProduct As New List(Of ViewQMS_MA_PRODUCT)()
            Dim listTag As New List(Of ViewQMS_MA_EXQ_TAG)()
            Dim listPlantProduct As New List(Of ViewQMS_MA_PRODUCT_MAPPING)()
            Dim tempControlSpec As New ViewQMS_MA_CONTROL_DATA

            listPlantProduct = masterDataServices.getQMS_MA_PRODUCT_MAPPINGListByPlantId(db, plantId)

            'Step 1. get PlantAndProductMapping
            If listPlantProduct.Count() > 0 Then
                startDate = actionDate 'actionDate.AddDays(-1) '1440 = 1days
                endDate = actionDate.AddDays(1) 'actionDate

                For Each plantProduct As ViewQMS_MA_PRODUCT_MAPPING In listPlantProduct

                    'Step 2 get Tag
                    Dim listQualityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listQuantityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listAccumTag As New List(Of ViewQMS_MA_EXQ_TAG)()

                    listQualityTag = masterDataServices.getQualityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listQuantityTag = masterDataServices.getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listAccumTag = masterDataServices.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)

                    'Step 3 save product exatra to database
                    If listQualityTag IsNot Nothing And listQualityTag.Count() > 0 Then
                        loadExaDataToDatabase(db, listQualityTag, plantProduct, startDate, endDate)
                    End If

                    Threading.Thread.Sleep(500)

                    If listQuantityTag IsNot Nothing And listQuantityTag.Count() > 0 Then
                        loadExaDataToDatabase(db, listQuantityTag, plantProduct, startDate, endDate)
                    End If

                    Threading.Thread.Sleep(500)

                    If listAccumTag IsNot Nothing And listAccumTag.Count() > 0 Then
                        loadExaAccumDataToDatabase(db, listAccumTag, plantProduct, startDate, endDate)
                    End If

                    Threading.Thread.Sleep(500)

                    'Exit For 'Temp exit add for test
                Next
            End If

        Catch ex As Exception
            ' MessageBox.Show(ex.Message)
        End Try

    End Sub
    Public Function loadExaAccumDataToDatabase(db As QMSSystem.CoreDB.QMSDBEntities, listQualityTag As List(Of ViewQMS_MA_EXQ_TAG), plantProduct As ViewQMS_MA_PRODUCT_MAPPING, startDate As DateTime, endDate As DateTime)

        Dim transDataServices As New TransactionService("ExaQuatum")
        Dim configDataServices As New ConfigServices("ExaQuatum")


        Try
            transDataServices.loadExaAccumDataToDatabase(db, listQualityTag, plantProduct, startDate, endDate)
        Catch ex As Exception
            Dim createError As New CreateQMS_ST_SYSTEM_LOG()

            createError.ERROR_LEVEL = 1
            createError.LOG_DATE = Date.Now
            createError.PROCESS_DESC = ex.Message
            createError.PROCESS_NAME = "EXAQUATUM"
            configDataServices.SaveQMS_ST_SYSTEM_LOG(db, createError)
        End Try

    End Function
    Public Function getConvertValue(tagValue As Decimal, CurrentExaTag As ViewQMS_MA_EXQ_TAG) As Decimal
        Dim convertValue As Decimal = 0

        Try

            If CurrentExaTag.TAG_TYPE = 2 Then 'it is PV tag == 2 SUM tag = 1
                convertValue = tagValue * CurrentExaTag.TAG_FLOW_CONVERT_VALUE
            Else
                convertValue = tagValue
            End If
        Catch ex As Exception
            convertValue = tagValue
        End Try

        Return convertValue
    End Function
    Public Function getConvertValue(tagValue As Decimal, CurrentExaTag As ViewQMS_MA_DOWNTIME_DETAIL) As Decimal
        Dim convertValue As Decimal = 0

        Try
            convertValue = tagValue * CurrentExaTag.CONVERT_VALUE

        Catch ex As Exception
            convertValue = tagValue
        End Try

        Return convertValue
    End Function
    Public Function getConvertValue(tagValue As Decimal, CurrentExaTag As ViewQMS_MA_REDUCE_FEED_DETAIL) As Decimal
        Dim convertValue As Decimal = 0

        Try
            convertValue = tagValue * CurrentExaTag.CONVERT_VALUE

        Catch ex As Exception
            convertValue = tagValue
        End Try

        Return convertValue
    End Function
    Public Function getTagData(tagValue As Object) As String
        Dim TagData As String = ""

        Try
            TagData = tagValue
        Catch ex As Exception
            TagData = "NULL"
        End Try

        Return TagData
    End Function
    Public Function SaveCannotGetDataError(db As QMSSystem.CoreDB.QMSDBEntities)
        Try
            Dim configDataServices As New ConfigServices("ExaQuatum")
            Dim createError As New CreateQMS_ST_SYSTEM_LOG()
            Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.

            createError.ERROR_LEVEL = 1
            createError.LOG_DATE = Date.Now
            createError.PROCESS_DESC = "CAN NOT GET EXA QUATUMN DATA"
            createError.PROCESS_NAME = "EXAQUATUM"

            listError.Add(createError)

            configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
        Catch ex As Exception

        End Try

    End Function
    Public Function loadExaDataToDatabase(db As QMSSystem.CoreDB.QMSDBEntities, listQualityTag As List(Of ViewQMS_MA_EXQ_TAG), plantProduct As ViewQMS_MA_PRODUCT_MAPPING, startDate As DateTime, endDate As DateTime)
        Dim vData As Object
        Dim lNumberOfPoints As Long
        Dim lmaxColume As Long

        Dim transDataServices As New TransactionService("ExaQuatum")
        Dim configDataServices As New ConfigServices("ExaQuatum")
        Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.

        vData = getExaTagData(listQualityTag, startDate, endDate)
        lNumberOfPoints = UBound(vData(1, 0), 2)
        lmaxColume = UBound(vData, 2)



        For ColumePoint = 0 To lmaxColume

            Dim listProductData As New List(Of CreateQMS_TR_PRODUCT_DATA)() 'new list data.
            listError = New List(Of CreateQMS_ST_SYSTEM_LOG)()

            Dim columnCount As Long = 0
            Dim ExatagID As Long = 0
            Dim CurrentExaTag As New ViewQMS_MA_EXQ_TAG()

            For Each qtyExaTag As ViewQMS_MA_EXQ_TAG In listQualityTag
                If ColumePoint = columnCount Then
                    ExatagID = qtyExaTag.ID
                    CurrentExaTag = qtyExaTag
                    Exit For
                End If
                columnCount += 1 'Increase for column checking...
            Next

            For lPoint = 0 To lNumberOfPoints

                Try
                    Dim createProductData As New CreateQMS_TR_PRODUCT_DATA()
                    Dim tagValue As Decimal = 0
                    Dim convertValue As Decimal = 0
                    Dim OFF_CONTROL_FLAG_0 As Byte = 0
                    Dim OFF_SPEC_FLAG_0 As Byte = 0
                    Dim GC_ERROR_FLAG_0 As Byte = 0

                    If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                        tagValue = vData(1, ColumePoint)(0, lPoint)
                    Else
                        tagValue = 0
                        GC_ERROR_FLAG_0 = 1 ' Value cannot read 
                    End If

                    convertValue = getConvertValue(tagValue, CurrentExaTag)
                    If CurrentExaTag.TAG_TYPE = 2 Then 'it is PV tag
                        If (convertValue < CurrentExaTag.TAG_FLOW_CHECK_VALUE) Then
                            GC_ERROR_FLAG_0 = 1 ' Value cannot read  GC_ERROR
                        End If
                    End If

                    'Check Off Control and Spec
                    createProductData.ID = 0 'add New 
                    createProductData.PLANT_ID = plantProduct.PLANT_ID
                    createProductData.PRODUCT_ID = plantProduct.PRODUCT_ID
                    createProductData.EXA_TAG_ID = ExatagID 'plantProduct.EXA_TAG_ID
                    createProductData.TAG_DATE = vData(1, 0)(2, lPoint)
                    createProductData.TAG_DATA = getTagData(vData(1, ColumePoint)(0, lPoint))
                    createProductData.TAG_VALUE = tagValue
                    createProductData.TAG_CONVERT = convertValue
                    createProductData.OFF_CONTROL_FLAG_0 = OFF_CONTROL_FLAG_0
                    createProductData.OFF_SPEC_FLAG_0 = OFF_SPEC_FLAG_0
                    createProductData.TURN_AROUND_FLAG_0 = 0
                    createProductData.DOWNTIME_FLAG_0 = 0
                    createProductData.CHANGE_GRADE_FLAG_0 = 0
                    createProductData.CROSS_VOLUME_FLAG_0 = 0
                    createProductData.GC_ERROR_FLAG_0 = GC_ERROR_FLAG_0
                    createProductData.REDUCE_FEED_FLAG_0 = 0
                    createProductData.OFF_CONTROL_FLAG = OFF_CONTROL_FLAG_0
                    createProductData.OFF_SPEC_FLAG = OFF_SPEC_FLAG_0
                    createProductData.TURN_AROUND_FLAG = 0
                    createProductData.DOWNTIME_FLAG = 0
                    createProductData.CHANGE_GRADE_FLAG = 0
                    createProductData.CROSS_VOLUME_FLAG = 0
                    createProductData.GC_ERROR_FLAG = GC_ERROR_FLAG_0
                    createProductData.REDUCE_FEED_FLAG = 0
                    createProductData.EXCEPTION_FLAG = 0
                    createProductData.EXCEPTION_VALUE = 0

                    'nLong = transDataServices.SaveQMS_TR_PRODUCT_DATA(db, createProductData)
                    listProductData.Add(createProductData)
                Catch ex As Exception
                    Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                    createError.ERROR_LEVEL = 1
                    createError.LOG_DATE = Date.Now
                    createError.PROCESS_DESC = ex.Message
                    createError.PROCESS_NAME = "EXAQUATUM"

                    listError.Add(createError)
                End Try
            Next

            'Save data
            Try
                transDataServices.SaveQualityQMS_TR_PRODUCT_DATA(db, listProductData, CurrentExaTag)
                '.SaveListQMS_TR_PRODUCT_DATA(db, listProductData)
                configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
            Catch ex As Exception
                Me.WriteToFile(ex.Message) 'MessageBox.Show(ex.Message)
            End Try

            ' Exit For 'Temp exit add for test
        Next

        'If lmaxColume <> 720 Then '1440 = 24 hrs 
        '    SaveCannotGetDataError(db)
        'End If
    End Function
    Public Function CheckOffControlSpec(valueTag As Decimal, controlData As ViewQMS_MA_CONTROL_DATA) As Byte
        Dim nResult As Byte = 0

        If controlData IsNot Nothing Then
            If controlData.MAX_FLAG = True And valueTag > controlData.MAX_VALUE Then 'check max off control
                nResult = 1 'Over Max
                Return nResult
            End If

            If controlData.MIN_FLAG = True And valueTag < controlData.MIN_VALUE Then 'check min off control
                nResult = 2 'Over Min
                Return nResult
            End If
        End If

        Return nResult
    End Function
    Public Function CheckCorrectTarget(objData As Object, exaTag As ViewQMS_MA_EXQ_TAG) As Byte
        Dim nResult As Byte = 0

        If objData IsNot Nothing Then

            If exaTag.TAG_CORRECT_MAX Then

            End If
        End If

        Return nResult
    End Function

    Public Function GetConnectionString() As String

        ' Specify the provider name, server and database.
        Dim providerName As String = "System.Data.SqlClient"
        Dim serverName As String = "ptt-db-t03.ptt.corp"
        Dim databaseName As String = "PTT-GSPQualityManagementSystem_test"

        ' Initialize the connection string builder for the
        ' underlying provider.
        Dim sqlBuilder As New SqlConnectionStringBuilder

        ' Set the properties for the data source.
        sqlBuilder.DataSource = serverName
        sqlBuilder.InitialCatalog = databaseName
        sqlBuilder.PersistSecurityInfo = True
        sqlBuilder.UserID = "pttgspqtstusr"
        sqlBuilder.Password = "cpttgspqtstusr"


        ' Build the SqlConnection connection string.
        Dim providerString As String = sqlBuilder.ToString

        ' Initialize the EntityConnectionStringBuilder.
        Dim entityBuilder As New EntityConnectionStringBuilder

        'Set the provider name.
        entityBuilder.Provider = providerName
        ' Set the provider-specific connection string.
        entityBuilder.ProviderConnectionString = providerString
        ' Set the Metadata location to the current directory.
        entityBuilder.Metadata = "res://*//QMSSystem.csdl|" & _
                                    "res://*//QMSSystem.ssdl|" & _
                                    "res://*//QMSSystem.msl"

        Return entityBuilder.ToString()
    End Function
    Public Function getDownloadTagData(listExaTag As List(Of ViewQMS_MA_DOWNTIME_DETAIL), startDate As Date, endDate As Date) As Object
        Dim objMDA As QUANTUMAUTOMATIONLib.QDataAccessMulti


        ' Dimension the filter parameters to return interpolated data
        Dim vFilterParameters As Object
        Dim vFilterParms(1) As Object
        Dim vFilterParmsTest(2) As Object
        Dim lFilterId As Long
        Dim lNumPeriods As Long

        ' Dimension variable to receive the data
        Dim vData As New Object

        Try
            ' Create new Exaquantum QDataAccessMulti object
            objMDA = New QUANTUMAUTOMATIONLib.QDataAccessMulti

            'table1.Columns.Add("Date")
            For Each exaTag As ViewQMS_MA_DOWNTIME_DETAIL In listExaTag
                objMDA.Add(exaTag.EXA_TAG_NAME)
                'table1.Columns.Add(exaTag.EXA_TAG_NAME)
            Next

            objMDA.SetQueryTimes(StartTime:=startDate, EndTime:=endDate)

            ' Set filter parameters to return interpolated data
            lFilterId = 1
            lNumPeriods = 200

            vFilterParmsTest(0) = 2
            vFilterParmsTest(1) = 120
            vFilterParmsTest(2) = True

            vFilterParameters = vFilterParmsTest

            objMDA.SetFilterParameters(vFilterParameters)
            ' Read filtered data from yesterday to now 
            vData = objMDA.ReadValue()
        Catch ex As Exception

        End Try

        objMDA = Nothing

        Return vData
    End Function
    Public Function getReduceExaTagData(listExaTag As List(Of ViewQMS_MA_REDUCE_FEED_DETAIL), startDate As Date, endDate As Date) As Object
        Dim objMDA As QUANTUMAUTOMATIONLib.QDataAccessMulti


        ' Dimension the filter parameters to return interpolated data
        Dim vFilterParameters As Object
        Dim vFilterParms(1) As Object
        Dim vFilterParmsTest(2) As Object
        Dim lFilterId As Long
        Dim lNumPeriods As Long

        ' Dimension variable to receive the data
        Dim vData As New Object

        Try
            ' Create new Exaquantum QDataAccessMulti object
            objMDA = New QUANTUMAUTOMATIONLib.QDataAccessMulti

            'table1.Columns.Add("Date")
            For Each exaTag As ViewQMS_MA_REDUCE_FEED_DETAIL In listExaTag
                objMDA.Add(exaTag.EXA_TAG_NAME)
                'table1.Columns.Add(exaTag.EXA_TAG_NAME)
            Next

            objMDA.SetQueryTimes(StartTime:=startDate, EndTime:=endDate)

            ' Set filter parameters to return interpolated data
            lFilterId = 1
            lNumPeriods = 200

            vFilterParmsTest(0) = 2
            vFilterParmsTest(1) = 120
            vFilterParmsTest(2) = True

            vFilterParameters = vFilterParmsTest

            objMDA.SetFilterParameters(vFilterParameters)
            ' Read filtered data from yesterday to now 
            vData = objMDA.ReadValue()
        Catch ex As Exception

        End Try

        objMDA = Nothing

        Return vData
    End Function
    Public Function getExaTagData(listExaTag As List(Of ViewQMS_MA_EXQ_TAG), startDate As Date, endDate As Date) As Object
        Dim objMDA As QUANTUMAUTOMATIONLib.QDataAccessMulti


        ' Dimension the filter parameters to return interpolated data
        Dim vFilterParameters As Object
        Dim vFilterParms(1) As Object
        Dim vFilterParmsTest(2) As Object
        Dim lFilterId As Long
        Dim lNumPeriods As Long

        ' Dimension variable to receive the data
        Dim vData As New Object
        Dim vValue As Object
        Dim vQuality As Object
        Dim vValue1 As Object
        Dim vQuality1 As Object
        Dim vTimestamp As Object
        Dim table1 As DataTable = New DataTable("result")

        Try
            ' Create new Exaquantum QDataAccessMulti object
            objMDA = New QUANTUMAUTOMATIONLib.QDataAccessMulti

            'table1.Columns.Add("Date")
            For Each exaTag As ViewQMS_MA_EXQ_TAG In listExaTag
                objMDA.Add(exaTag.EXA_TAG_NAME)
                'table1.Columns.Add(exaTag.EXA_TAG_NAME)
            Next

            objMDA.SetQueryTimes(StartTime:=startDate, EndTime:=endDate)

            ' Set filter parameters to return interpolated data
            lFilterId = 1
            lNumPeriods = 200

            vFilterParmsTest(0) = 2
            vFilterParmsTest(1) = 120
            vFilterParmsTest(2) = True

            vFilterParameters = vFilterParmsTest

            objMDA.SetFilterParameters(vFilterParameters)
            ' Read filtered data from yesterday to now

            'Dim data As Object
            vData = objMDA.ReadValue()
            'Iterate through the returned data retrieving the history values
            'Dim lNumberOfPoints As Long
            'Dim lmaxColume As Long
            'Dim lPoint As Long
            'Dim ColumePoint As Long
            'Dim writeData As String

            'lNumberOfPoints = UBound(vData(1, 0), 2)
            'lmaxColume = UBound(vData, 2)

            ''Write header'
            'writeData = "Order, Date"
            'For ColumePoint = 0 To lmaxColume
            '    writeData = writeData & "," & vData(0, ColumePoint)
            'Next

            ''Write Data 

            'For lPoint = 0 To lNumberOfPoints
            '    Try
            '        writeData = "#" & (lPoint + 1).ToString() & "," & vData(1, 0)(2, lPoint)

            '        Dim dr As DataRow = dt.NewRow

            '        For ColumePoint = 0 To lmaxColume
            '            writeData = writeData & "," & vData(1, ColumePoint)(0, lPoint)
            '        Next

            '        'WriteToFile(writeData)

            '    Catch ex As Exception
            '        'WriteToFile(ex.Message)
            '    End Try

            'Next
        Catch ex As Exception

            'WriteToFile(ex.Message)
            'm_nCount = m_nCount - 1
            'm_bError = True
        End Try

        objMDA = Nothing

        Return vData
    End Function

    Public Sub loadAllExaDataByScheduleToDB(db As QMSSystem.CoreDB.QMSDBEntities)

        Dim configDataServices As New ConfigServices("ExaQuatum")
        Dim listSchedule As List(Of ViewQMS_ST_EXAQUATUM_SCHEDULE)
        Dim actionDate As DateTime

        Try
            'Step 1 load all schedule 
            listSchedule = configDataServices.getQMS_ST_EXAQUATUM_SCHEDULEListByStatus(db, EXA_SCHEDULE_STATUS.PENDING)

            If listSchedule.Count() > 0 Then

                For Each schedule As ViewQMS_ST_EXAQUATUM_SCHEDULE In listSchedule
                    Try
                        actionDate = New DateTime(schedule.START_DATE.Year, schedule.START_DATE.Month, schedule.START_DATE.Day, 0, 0, 0).AddMinutes(-2)

                        'Step 2 loop day until finished 
                        Do Until (actionDate > schedule.END_DATE)
                            loadAllExaProductTagByPlantIDToDB(db, actionDate, schedule.PLANT_ID) 
                            loadAllExaDowntimeTagToDB(db, actionDate)
                            loadAllExaReduceTagToDB(db, actionDate)
                            actionDate = actionDate.AddDays(1)
                        Loop

                        schedule.DOC_STATUS = EXA_SCHEDULE_STATUS.CONFIRM
                    Catch ex As Exception
                        schedule.DOC_STATUS = EXA_SCHEDULE_STATUS.FAIL
                    End Try

                    'Step 3 update status
                    Dim createModel As New CreateQMS_ST_EXAQUATUM_SCHEDULE()
                    createModel.ID = schedule.ID
                    createModel.PLANT_ID = schedule.PLANT_ID
                    createModel.START_DATE = schedule.START_DATE
                    createModel.END_DATE = schedule.END_DATE
                    createModel.DOC_STATUS = schedule.DOC_STATUS

                    configDataServices.SaveQMS_ST_EXAQUATUM_SCHEDULE(db, createModel)

                Next

            End If

        Catch ex As Exception

        End Try
    End Sub
    Public Sub loadAllExaProductTagByPlantIDToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime, plantId As Long)

        Try
            Dim startDate As Date
            Dim endDate As Date

            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim listPlant As New List(Of ViewQMS_MA_PLANT)()
            Dim listProduct As New List(Of ViewQMS_MA_PRODUCT)()
            Dim listTag As New List(Of ViewQMS_MA_EXQ_TAG)()
            Dim listPlantProduct As New List(Of ViewQMS_MA_PRODUCT_MAPPING)()
            Dim tempControlSpec As New ViewQMS_MA_CONTROL_DATA

            listPlantProduct = masterDataServices.getQMS_MA_PRODUCT_MAPPINGListByPlantId(db, plantId)

            'Step 1. get PlantAndProductMapping
            If listPlantProduct.Count() > 0 Then
                startDate = actionDate 'actionDate.AddDays(-1) '1440 = 1days
                endDate = actionDate.AddDays(1) 'actionDate

                For Each plantProduct As ViewQMS_MA_PRODUCT_MAPPING In listPlantProduct

                    'Step 2 get Tag
                    Dim listQualityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listQuantityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listAccumTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listResultTag As New List(Of ViewQMS_MA_EXQ_TAG)()

                    listQualityTag = masterDataServices.getQualityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listQuantityTag = masterDataServices.getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listAccumTag = masterDataServices.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)

                    'Step 3 save product exatra to database
                    If listQualityTag IsNot Nothing And listQualityTag.Count() > 0 Then
                        listResultTag.AddRange(listQualityTag)
                        'loadExaDataToDatabase(db, listQualityTag, plantProduct, startDate, endDate)
                    End If

                    If listQuantityTag IsNot Nothing And listQuantityTag.Count() > 0 Then
                        listResultTag.AddRange(listQuantityTag)
                        'loadExaDataToDatabase(db, listQuantityTag, plantProduct, startDate, endDate)
                    End If

                    If listAccumTag IsNot Nothing And listAccumTag.Count() > 0 Then
                        listResultTag.AddRange(listAccumTag)
                        'loadExaAccumDataToDatabase(db, listAccumTag, plantProduct, startDate, endDate)
                    End If

                    If listResultTag IsNot Nothing And listResultTag.Count() > 0 Then
                        loadExaDataToQMS_TR_EXQ_PRODUCT_DATA(db, listResultTag, plantProduct, startDate, endDate)
                    End If

                    Threading.Thread.Sleep(500)

                    'Exit For 'Temp exit add for test
                Next
            End If

        Catch ex As Exception
            ' MessageBox.Show(ex.Message)
        End Try

    End Sub
    Public Sub loadAllExaDowntimeTagToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            'Step 1 load all plant
            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim transDataServices As New TransactionService("ExaQuatum")
            Dim configDataServices As New ConfigServices("ExaQuatum")

            Dim searchExaModel As New ExaDowntimeSearchModel

            Dim listPlant As List(Of ViewQMS_MA_PLANT) = masterDataServices.getQMS_MA_PLANTActiveList(db)
            Dim listExaTag As List(Of ViewQMS_MA_DOWNTIME_DETAIL)
            Dim listExaDowntimeData As New List(Of CreateQMS_TR_EXQ_DOWNTIME_DATA)()
            Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.
            Dim vData As Object
            Dim lNumberOfPoints As Long
            Dim lmaxColume As Long
            Dim startDate As DateTime = actionDate 'actionDate.AddDays(-1) '1440 = 1days
            Dim endDate As DateTime = actionDate.AddDays(1) 'actionDate

            Dim tempModel As CreateQMS_TR_EXQ_DOWNTIME_DATA


            If listPlant.Count() > 0 Then
                For Each plant As ViewQMS_MA_PLANT In listPlant
                    'Step 2 list Active ReduceFeed Tag
                    listExaTag = masterDataServices.getQMS_MA_DOWNTIME_DETAILActiveListByPlantAndActiveDate(db, plant.ID, actionDate)
                    vData = getDownloadTagData(listExaTag, startDate, endDate)
                    lNumberOfPoints = UBound(vData(1, 0), 2)
                    lmaxColume = UBound(vData, 2)

                    listExaDowntimeData = New List(Of CreateQMS_TR_EXQ_DOWNTIME_DATA) 'reset value

                    Dim listExaId As String
                    Dim tagValue As Decimal = 0
                    Dim GC_ERROR_FLAG_0 As Byte = 0

                    searchExaModel.START_DATE = startDate.AddMinutes(2) 'Add for protect delete
                    searchExaModel.END_DATE = endDate
                    searchExaModel.PLANT_ID = plant.ID

                    listExaId = ""
                    For Each exaTag As ViewQMS_MA_DOWNTIME_DETAIL In listExaTag
                        If (listExaId = "") Then
                            listExaId = exaTag.ID.ToString()
                        Else
                            listExaId = listExaId & "," & exaTag.ID.ToString()
                        End If
                    Next


                    For lPoint = 0 To lNumberOfPoints

                        tempModel = New CreateQMS_TR_EXQ_DOWNTIME_DATA
                        tempModel.ID = 0
                        tempModel.PLANT_ID = plant.ID
                        tempModel.TAG_DATE = vData(1, 0)(2, lPoint)
                        tempModel.TAG_EXA_ID = listExaId
                        tempModel.TAG_VALUE = ""
                        tempModel.TAG_CONVERT = ""
                        tempModel.TAG_GCERROR = ""
                        tempModel.STATUS_1 = CAL_OFF_STATUS.INITIAL
                        tempModel.STATUS_2 = CAL_OFF_STATUS.INITIAL

                        tagValue = 0
                        GC_ERROR_FLAG_0 = 0
                        Try
                            For ColumePoint = 0 To lmaxColume
                                If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                                    tagValue = vData(1, ColumePoint)(0, lPoint)
                                    GC_ERROR_FLAG_0 = 0
                                Else
                                    tagValue = 0
                                    GC_ERROR_FLAG_0 = 1 ' Value cannot read 
                                End If

                                If (tempModel.TAG_VALUE = "") Then
                                    tempModel.TAG_VALUE = getTagData(vData(1, ColumePoint)(0, lPoint))
                                Else
                                    tempModel.TAG_VALUE = tempModel.TAG_VALUE & "," & getTagData(vData(1, ColumePoint)(0, lPoint))
                                End If

                                If (tempModel.TAG_CONVERT = "") Then
                                    tempModel.TAG_CONVERT = getConvertValue(tagValue, listExaTag(ColumePoint))
                                Else
                                    tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & getConvertValue(tagValue, listExaTag(ColumePoint))
                                End If

                                If (tempModel.TAG_GCERROR = "") Then
                                    tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString()
                                Else
                                    tempModel.TAG_GCERROR = tempModel.TAG_GCERROR & "," & GC_ERROR_FLAG_0.ToString()
                                End If
                            Next
                            listExaDowntimeData.Add(tempModel)
                        Catch ex As Exception
                            Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                            createError.ERROR_LEVEL = 1
                            createError.LOG_DATE = Date.Now
                            createError.PROCESS_DESC = ex.Message
                            createError.PROCESS_NAME = "EXAQUATUM"

                            listError.Add(createError)
                        End Try

                    Next
                    'Save data
                    Try
                        transDataServices.SaveListQMS_TR_EXQ_DOWNTIME_DATA(db, listExaDowntimeData, listExaTag, searchExaModel)
                        configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
                    Catch ex As Exception
                        Me.WriteToFile(ex.Message) 'MessageBox.Show(ex.Message)
                    End Try

                    Threading.Thread.Sleep(500)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub loadAllExaReduceTagToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            'Step 1 load all plant
            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim transDataServices As New TransactionService("ExaQuatum")
            Dim configDataServices As New ConfigServices("ExaQuatum")

            Dim searchExaModel As New ExaReduceFeedSearchModel

            Dim listPlant As List(Of ViewQMS_MA_PLANT) = masterDataServices.getQMS_MA_PLANTActiveList(db)
            Dim listExaTag As List(Of ViewQMS_MA_REDUCE_FEED_DETAIL)
            Dim listExaReduceData As New List(Of CreateQMS_TR_EXQ_REDUCE_FEED_DATA)()
            Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.
            Dim vData As Object
            Dim lNumberOfPoints As Long
            Dim lmaxColume As Long
            Dim startDate As DateTime = actionDate 'actionDate.AddDays(-1) '1440 = 1days
            Dim endDate As DateTime = actionDate.AddDays(1) 'actionDate

            Dim tempModel As CreateQMS_TR_EXQ_REDUCE_FEED_DATA

            If listPlant.Count() > 0 Then
                For Each plant As ViewQMS_MA_PLANT In listPlant
                    'Step 2 list Active ReduceFeed Tag
                    listExaTag = masterDataServices.getQMS_MA_REDUCE_FEED_DETAILActiveListByPlantAndActiveDate(db, plant.ID, actionDate)
                    vData = getReduceExaTagData(listExaTag, startDate, endDate)
                    lNumberOfPoints = UBound(vData(1, 0), 2)
                    lmaxColume = UBound(vData, 2)

                    listExaReduceData = New List(Of CreateQMS_TR_EXQ_REDUCE_FEED_DATA) 'reset value

                    Dim listExaId As String
                    Dim tagValue As Decimal = 0
                    Dim GC_ERROR_FLAG_0 As Byte = 0

                    searchExaModel.START_DATE = startDate.AddMinutes(2) 'Add for protect delete
                    searchExaModel.END_DATE = endDate
                    searchExaModel.PLANT_ID = plant.ID

                    listExaId = ""
                    For Each exaTag As ViewQMS_MA_REDUCE_FEED_DETAIL In listExaTag
                        If (listExaId = "") Then
                            listExaId = exaTag.ID.ToString()
                        Else
                            listExaId = listExaId & "," & exaTag.ID.ToString()
                        End If
                    Next


                    For lPoint = 0 To lNumberOfPoints

                        tempModel = New CreateQMS_TR_EXQ_REDUCE_FEED_DATA
                        tempModel.ID = 0
                        tempModel.PLANT_ID = plant.ID
                        tempModel.TAG_DATE = vData(1, 0)(2, lPoint)
                        tempModel.TAG_EXA_ID = listExaId
                        tempModel.TAG_VALUE = ""
                        tempModel.TAG_CONVERT = ""
                        tempModel.TAG_GCERROR = ""
                        tempModel.STATUS_1 = CAL_OFF_STATUS.INITIAL
                        tempModel.STATUS_2 = CAL_OFF_STATUS.INITIAL

                        tagValue = 0
                        GC_ERROR_FLAG_0 = 0
                        Try
                            For ColumePoint = 0 To lmaxColume
                                If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                                    tagValue = vData(1, ColumePoint)(0, lPoint)
                                    GC_ERROR_FLAG_0 = 0
                                Else
                                    tagValue = 0
                                    GC_ERROR_FLAG_0 = 1 ' Value cannot read 
                                End If

                                If (tempModel.TAG_VALUE = "") Then
                                    tempModel.TAG_VALUE = getTagData(vData(1, ColumePoint)(0, lPoint))
                                Else
                                    tempModel.TAG_VALUE = tempModel.TAG_VALUE & "," & getTagData(vData(1, ColumePoint)(0, lPoint))
                                End If

                                If (tempModel.TAG_CONVERT = "") Then
                                    tempModel.TAG_CONVERT = getConvertValue(tagValue, listExaTag(ColumePoint))
                                Else
                                    tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & getConvertValue(tagValue, listExaTag(ColumePoint))
                                End If

                                If (tempModel.TAG_GCERROR = "") Then
                                    tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString()
                                Else
                                    tempModel.TAG_GCERROR = tempModel.TAG_GCERROR & "," & GC_ERROR_FLAG_0.ToString()
                                End If
                            Next
                            listExaReduceData.Add(tempModel)
                        Catch ex As Exception
                            Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                            createError.ERROR_LEVEL = 1
                            createError.LOG_DATE = Date.Now
                            createError.PROCESS_DESC = ex.Message
                            createError.PROCESS_NAME = "EXAQUATUM"

                            listError.Add(createError)
                        End Try

                    Next
                    'Save data
                    Try
                        transDataServices.SaveListQMS_TR_EXQ_REDUCE_FEED_DATA(db, listExaReduceData, listExaTag, searchExaModel)
                        configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
                    Catch ex As Exception
                        Me.WriteToFile(ex.Message) ' MessageBox.Show(ex.Message)
                    End Try

                    Threading.Thread.Sleep(500)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub loadAllExaPlantProductToDB(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            Dim startDate As Date
            Dim endDate As Date

            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim listPlant As New List(Of ViewQMS_MA_PLANT)()
            Dim listProduct As New List(Of ViewQMS_MA_PRODUCT)()
            Dim listTag As New List(Of ViewQMS_MA_EXQ_TAG)()
            Dim listPlantProduct As New List(Of ViewQMS_MA_PRODUCT_MAPPING)()
            Dim tempControlSpec As New ViewQMS_MA_CONTROL_DATA

            listPlantProduct = masterDataServices.getQMS_MA_PRODUCT_MAPPINGList(db)

            'Step 1. get PlantAndProductMapping
            If listPlantProduct.Count() > 0 Then
                startDate = actionDate 'actionDate.AddDays(-1) '1440 = 1days
                endDate = actionDate.AddDays(1) 'actionDate

                For Each plantProduct As ViewQMS_MA_PRODUCT_MAPPING In listPlantProduct

                    'Step 2 get Tag
                    Dim listQualityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listQuantityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listAccumTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listResultTag As New List(Of ViewQMS_MA_EXQ_TAG)()

                    listQualityTag = masterDataServices.getQualityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listQuantityTag = masterDataServices.getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listAccumTag = masterDataServices.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)

                    'Step 3 save product exatra to database
                    If listQualityTag IsNot Nothing And listQualityTag.Count() > 0 Then
                        listResultTag.AddRange(listQualityTag)
                        'loadExaDataToDatabase(db, listQualityTag, plantProduct, startDate, endDate)
                    End If

                    If listQuantityTag IsNot Nothing And listQuantityTag.Count() > 0 Then
                        listResultTag.AddRange(listQuantityTag)
                        'loadExaDataToDatabase(db, listQuantityTag, plantProduct, startDate, endDate)
                    End If

                    If listAccumTag IsNot Nothing And listAccumTag.Count() > 0 Then
                        listResultTag.AddRange(listAccumTag)
                        'loadExaAccumDataToDatabase(db, listAccumTag, plantProduct, startDate, endDate)
                    End If

                    If listResultTag IsNot Nothing And listResultTag.Count() > 0 Then
                        loadExaDataToQMS_TR_EXQ_PRODUCT_DATA(db, listResultTag, plantProduct, startDate, endDate)
                    End If

                    Threading.Thread.Sleep(500)
                    'Exit For 'Temp exit add for test
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function loadExaDataToQMS_TR_EXQ_PRODUCT_DATA(db As QMSSystem.CoreDB.QMSDBEntities, listExaTag As List(Of ViewQMS_MA_EXQ_TAG), plantProduct As ViewQMS_MA_PRODUCT_MAPPING, startDate As DateTime, endDate As DateTime)
        Dim vData As Object
        Dim lNumberOfPoints As Long
        Dim lmaxColume As Long

        Dim transDataServices As New TransactionService("ExaQuatum")
        Dim configDataServices As New ConfigServices("ExaQuatum")

        Dim tempModel As CreateQMS_TR_EXQ_PRODUCT_DATA
        Dim searchExaProduct As New ExaProductSearchModel
        Dim listExaProductData As New List(Of CreateQMS_TR_EXQ_PRODUCT_DATA)()
        Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.
        Dim listExaId As String

        vData = getExaTagData(listExaTag, startDate, endDate)
        lNumberOfPoints = UBound(vData(1, 0), 2)
        lmaxColume = UBound(vData, 2)
        Dim tagValue As Decimal = 0
        Dim GC_ERROR_FLAG_0 As Byte = 0
        Dim tempFlowCheck As Decimal

        searchExaProduct.START_DATE = startDate.AddMinutes(2) 'Add for protect delete
        searchExaProduct.END_DATE = endDate
        searchExaProduct.PLANT_ID = plantProduct.PLANT_ID
        searchExaProduct.PRODUCT_ID = plantProduct.PRODUCT_ID

        listExaId = ""
        For Each exaTag As ViewQMS_MA_EXQ_TAG In listExaTag
            If (listExaId = "") Then
                listExaId = exaTag.ID.ToString()
            Else
                listExaId = listExaId & "," & exaTag.ID.ToString()
            End If
        Next

        'switch rows to column
        For lPoint = 0 To lNumberOfPoints
            tempModel = New CreateQMS_TR_EXQ_PRODUCT_DATA
            tempModel.ID = 0
            tempModel.PLANT_ID = plantProduct.PLANT_ID
            tempModel.PRODUCT_ID = plantProduct.PRODUCT_ID
            tempModel.TAG_DATE = vData(1, 0)(2, lPoint)
            tempModel.TAG_EXA_ID = listExaId
            tempModel.TAG_VALUE = ""
            tempModel.TAG_CONVERT = ""
            tempModel.TAG_GCERROR = ""
            tempModel.STATUS_1 = CAL_OFF_STATUS.INITIAL
            tempModel.STATUS_2 = CAL_OFF_STATUS.INITIAL

            tagValue = 0
            GC_ERROR_FLAG_0 = 0
            Try
                For ColumePoint = 0 To lmaxColume
                    If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                        tagValue = vData(1, ColumePoint)(0, lPoint)
                        GC_ERROR_FLAG_0 = 0
                    Else
                        tagValue = 0
                        If (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.ACCUM) Then
                            GC_ERROR_FLAG_0 = 0 ' Value cannot read 
                        Else
                            GC_ERROR_FLAG_0 = 1 ' Value cannot read 
                        End If
                    End If

                    If (tempModel.TAG_VALUE = "") Then
                        tempModel.TAG_VALUE = getTagData(vData(1, ColumePoint)(0, lPoint))
                    Else
                        tempModel.TAG_VALUE = tempModel.TAG_VALUE & "," & getTagData(vData(1, ColumePoint)(0, lPoint))
                    End If

                    If (tempModel.TAG_CONVERT = "") Then
                        If (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUALITY_TAG) Then
                            tempModel.TAG_CONVERT = tagValue.ToString()
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUANTITY_PV) Then
                            tempFlowCheck = getConvertValue(tagValue, listExaTag(ColumePoint))
                            tempModel.TAG_CONVERT = tempFlowCheck.ToString()
                            'check flow 
                            If (tempFlowCheck < listExaTag(ColumePoint).TAG_FLOW_CHECK_VALUE) Then
                                GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE
                            End If
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.ACCUM) Then
                            tempModel.TAG_CONVERT = tagValue.ToString()
                        Else
                            tempModel.TAG_CONVERT = tagValue.ToString()
                        End If
                    Else
                        If (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUALITY_TAG) Then
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & tagValue.ToString()
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUANTITY_PV) Then
                            tempFlowCheck = getConvertValue(tagValue, listExaTag(ColumePoint))
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & getConvertValue(tagValue, listExaTag(ColumePoint))
                            'check flow 
                            If (tempFlowCheck < listExaTag(ColumePoint).TAG_FLOW_CHECK_VALUE) Then
                                GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE
                            End If
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.ACCUM) Then
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & tagValue.ToString()
                        Else
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & tagValue.ToString()
                        End If
                    End If

                    If (tempModel.TAG_GCERROR = "") Then
                        tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString()
                    Else
                        tempModel.TAG_GCERROR = tempModel.TAG_GCERROR & "," & GC_ERROR_FLAG_0.ToString()
                    End If
                Next
                listExaProductData.Add(tempModel)
            Catch ex As Exception
                Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                createError.ERROR_LEVEL = 1
                createError.LOG_DATE = Date.Now
                createError.PROCESS_DESC = ex.Message
                createError.PROCESS_NAME = "EXAQUATUM"

                listError.Add(createError)
            End Try

        Next

        'Save data
        Try
            transDataServices.SaveListQMS_TR_EXQ_PRODUCT_DATA(db, listExaProductData, listExaTag, searchExaProduct)
            configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
        Catch ex As Exception
            Me.WriteToFile(ex.Message) 'MessageBox.Show(ex.Message)
        End Try
    End Function



    'New data base method
    Public Sub loadAllExaPlantProduct(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            Dim startDate As Date
            Dim endDate As Date

            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim listPlant As New List(Of ViewQMS_MA_PLANT)()
            Dim listProduct As New List(Of ViewQMS_MA_PRODUCT)()
            Dim listTag As New List(Of ViewQMS_MA_EXQ_TAG)()
            Dim listPlantProduct As New List(Of ViewQMS_MA_PRODUCT_MAPPING)()
            Dim tempControlSpec As New ViewQMS_MA_CONTROL_DATA

            listPlantProduct = masterDataServices.getQMS_MA_PRODUCT_MAPPINGList(db)

            'Step 1. get PlantAndProductMapping
            If listPlantProduct.Count() > 0 Then
                startDate = actionDate 'actionDate.AddDays(-1) '1440 = 1days
                endDate = actionDate.AddDays(1) 'actionDate

                For Each plantProduct As ViewQMS_MA_PRODUCT_MAPPING In listPlantProduct

                    'Step 2 get Tag
                    Dim listQualityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listQuantityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listAccumTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listResultTag As New List(Of ViewQMS_MA_EXQ_TAG)()

                    listQualityTag = masterDataServices.getQualityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listQuantityTag = masterDataServices.getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listAccumTag = masterDataServices.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)

                    'Step 3 save product exatra to database
                    If listQualityTag IsNot Nothing And listQualityTag.Count() > 0 Then
                        listResultTag.AddRange(listQualityTag)
                        'loadExaDataToDatabase(db, listQualityTag, plantProduct, startDate, endDate)
                    End If

                    If listQuantityTag IsNot Nothing And listQuantityTag.Count() > 0 Then
                        listResultTag.AddRange(listQuantityTag)
                        'loadExaDataToDatabase(db, listQuantityTag, plantProduct, startDate, endDate)
                    End If

                    If listAccumTag IsNot Nothing And listAccumTag.Count() > 0 Then
                        listResultTag.AddRange(listAccumTag)
                        'loadExaAccumDataToDatabase(db, listAccumTag, plantProduct, startDate, endDate)
                    End If

                    If listResultTag IsNot Nothing And listResultTag.Count() > 0 Then
                        loadExaDataToQMS_TR_EXQ_PRODUCT(db, listResultTag, plantProduct, startDate, endDate)
                    End If

                    Threading.Thread.Sleep(500)
                    'Exit For 'Temp exit add for test
                Next
            End If
        Catch ex As Exception
            Me.WriteToFile(ex.Message)
        End Try
    End Sub
    Public Function loadExaDataToQMS_TR_EXQ_PRODUCT(db As QMSSystem.CoreDB.QMSDBEntities, listExaTag As List(Of ViewQMS_MA_EXQ_TAG), plantProduct As ViewQMS_MA_PRODUCT_MAPPING, startDate As DateTime, endDate As DateTime)
        Dim vData As Object
        Dim lNumberOfPoints As Long
        Dim lmaxColume As Long

        Dim transDataServices As New TransactionService("ExaQuatum")
        Dim configDataServices As New ConfigServices("ExaQuatum")

        Dim tempModel As CreateQMS_TR_EXQ_PRODUCT
        Dim searchExaProduct As New ExaProductSearchModel
        Dim listExaProductData As New List(Of CreateQMS_TR_EXQ_PRODUCT)()
        Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.
        Dim listExaId As String

        vData = getExaTagData(listExaTag, startDate, endDate)
        lNumberOfPoints = UBound(vData(1, 0), 2)
        lmaxColume = UBound(vData, 2)
        Dim tagValue As Decimal = 0
        Dim GC_ERROR_FLAG_0 As Byte = 0
        Dim tempFlowCheck As Decimal

        searchExaProduct.START_DATE = startDate.AddMinutes(2) 'Add for protect delete
        searchExaProduct.END_DATE = endDate
        searchExaProduct.PLANT_ID = plantProduct.PLANT_ID
        searchExaProduct.PRODUCT_ID = plantProduct.PRODUCT_ID

        listExaId = ""
        For Each exaTag As ViewQMS_MA_EXQ_TAG In listExaTag
            If (listExaId = "") Then
                listExaId = exaTag.ID.ToString()
            Else
                listExaId = listExaId & "," & exaTag.ID.ToString()
            End If
        Next

        For ColumePoint = 0 To lmaxColume

            tempModel = New CreateQMS_TR_EXQ_PRODUCT
            tempModel.ID = 0
            tempModel.PLANT_ID = plantProduct.PLANT_ID
            tempModel.PRODUCT_ID = plantProduct.PRODUCT_ID
            tempModel.TAG_DATE = searchExaProduct.START_DATE
            tempModel.TAG_EXA_ID = listExaTag(ColumePoint).ID
            tempModel.TAG_VALUE = ""
            tempModel.TAG_CONVERT = ""
            tempModel.TAG_GCERROR = ""
            tempModel.STATUS_1 = CAL_OFF_STATUS.INITIAL
            tempModel.STATUS_2 = CAL_OFF_STATUS.INITIAL
            Try
                For lPoint = 0 To lNumberOfPoints

                    If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                        tagValue = vData(1, ColumePoint)(0, lPoint)
                        GC_ERROR_FLAG_0 = 0
                    Else
                        tagValue = 0
                        If (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.ACCUM) Then
                            GC_ERROR_FLAG_0 = 0 ' Value cannot read 
                        Else
                            GC_ERROR_FLAG_0 = 1 ' Value cannot read 
                        End If
                    End If

                    If (tempModel.TAG_VALUE = "") Then
                        tempModel.TAG_VALUE = getTagData(vData(1, ColumePoint)(0, lPoint))
                    Else
                        tempModel.TAG_VALUE = tempModel.TAG_VALUE & "," & getTagData(vData(1, ColumePoint)(0, lPoint))
                    End If

                    If (tempModel.TAG_CONVERT = "") Then
                        If (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUALITY_TAG) Then
                            tempModel.TAG_CONVERT = tagValue.ToString()
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUANTITY_PV) Then
                            tempFlowCheck = getConvertValue(tagValue, listExaTag(ColumePoint))
                            tempModel.TAG_CONVERT = tempFlowCheck.ToString()
                            'check flow 
                            If (tempFlowCheck < listExaTag(ColumePoint).TAG_FLOW_CHECK_VALUE) Then
                                GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE
                            End If
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.ACCUM) Then
                            tempModel.TAG_CONVERT = tagValue.ToString()
                        Else
                            tempModel.TAG_CONVERT = tagValue.ToString()
                        End If
                    Else
                        If (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUALITY_TAG) Then
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & tagValue.ToString()
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.QUANTITY_PV) Then
                            tempFlowCheck = getConvertValue(tagValue, listExaTag(ColumePoint))
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & getConvertValue(tagValue, listExaTag(ColumePoint))
                            'check flow 
                            If (tempFlowCheck < listExaTag(ColumePoint).TAG_FLOW_CHECK_VALUE) Then
                                GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE
                            End If
                        ElseIf (listExaTag(ColumePoint).TAG_TYPE = EXA_TAG_TYPE.ACCUM) Then
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & tagValue.ToString()
                        Else
                            tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & tagValue.ToString()
                        End If
                    End If

                    If (tempModel.TAG_GCERROR = "") Then
                        tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString()
                    Else
                        tempModel.TAG_GCERROR = tempModel.TAG_GCERROR & "," & GC_ERROR_FLAG_0.ToString()
                    End If

                Next
                listExaProductData.Add(tempModel)

                If (lNumberOfPoints <> 720) And (listExaTag(ColumePoint).TAG_TYPE <> EXA_TAG_TYPE.ACCUM) Then
                    Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                    createError.ERROR_LEVEL = 1
                    createError.LOG_DATE = Date.Now
                    createError.PROCESS_DESC = "Tag " & listExaTag(ColumePoint).EXA_TAG_NAME & " can not get total day (720 datas)"
                    createError.PROCESS_NAME = "EXAQUATUM"

                    listError.Add(createError)
                End If

            Catch ex As Exception
                Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                createError.ERROR_LEVEL = 1
                createError.LOG_DATE = Date.Now
                createError.PROCESS_DESC = ex.Message
                createError.PROCESS_NAME = "EXAQUATUM"

                listError.Add(createError)
            End Try
        Next

        'Save data
        Try
            transDataServices.SaveListQMS_TR_EXQ_PRODUCT(db, listExaProductData, listExaTag, searchExaProduct)
            configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
        Catch ex As Exception
            Me.WriteToFile(ex.Message) ' MessageBox.Show(ex.Message)
        End Try
    End Function
    Public Sub loadAllExaDowntimeTag(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            'Step 1 load all plant
            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim transDataServices As New TransactionService("ExaQuatum")
            Dim configDataServices As New ConfigServices("ExaQuatum")

            Dim searchExaModel As New ExaDowntimeSearchModel

            Dim listPlant As List(Of ViewQMS_MA_PLANT) = masterDataServices.getQMS_MA_PLANTActiveList(db)
            Dim listExaTag As List(Of ViewQMS_MA_DOWNTIME_DETAIL)
            Dim listExaDowntimeData As New List(Of CreateQMS_TR_EXQ_DOWNTIME)()
            Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.
            Dim vData As Object
            Dim lNumberOfPoints As Long
            Dim lmaxColume As Long
            Dim startDate As DateTime = actionDate 'actionDate.AddDays(-1) '1440 = 1days
            Dim endDate As DateTime = actionDate.AddDays(1) 'actionDate

            Dim tempModel As CreateQMS_TR_EXQ_DOWNTIME


            If listPlant.Count() > 0 Then
                For Each plant As ViewQMS_MA_PLANT In listPlant
                    'Step 2 list Active ReduceFeed Tag
                    listExaTag = masterDataServices.getQMS_MA_DOWNTIME_DETAILActiveListByPlantAndActiveDate(db, plant.ID, actionDate)
                    vData = getDownloadTagData(listExaTag, startDate, endDate)
                    lNumberOfPoints = UBound(vData(1, 0), 2)
                    lmaxColume = UBound(vData, 2)

                    listExaDowntimeData = New List(Of CreateQMS_TR_EXQ_DOWNTIME) 'reset value

                    Dim listExaId As String
                    Dim tagValue As Decimal = 0
                    Dim GC_ERROR_FLAG_0 As Byte = 0

                    searchExaModel.START_DATE = startDate.AddMinutes(2) 'Add for protect delete
                    searchExaModel.END_DATE = endDate
                    searchExaModel.PLANT_ID = plant.ID

                    listExaId = ""
                    For Each exaTag As ViewQMS_MA_DOWNTIME_DETAIL In listExaTag
                        If (listExaId = "") Then
                            listExaId = exaTag.ID.ToString()
                        Else
                            listExaId = listExaId & "," & exaTag.ID.ToString()
                        End If
                    Next

                    For ColumePoint = 0 To lmaxColume

                        tempModel = New CreateQMS_TR_EXQ_DOWNTIME
                        tempModel.ID = 0
                        tempModel.PLANT_ID = plant.ID
                        tempModel.TAG_DATE = searchExaModel.START_DATE
                        tempModel.TAG_EXA_ID = listExaTag(ColumePoint).ID
                        tempModel.TAG_VALUE = ""
                        tempModel.TAG_CONVERT = ""
                        tempModel.TAG_GCERROR = ""
                        tempModel.STATUS_1 = CAL_OFF_STATUS.INITIAL
                        tempModel.STATUS_2 = CAL_OFF_STATUS.INITIAL

                        tagValue = 0
                        GC_ERROR_FLAG_0 = 0
                        Try
                            For lPoint = 0 To lNumberOfPoints
                                If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                                    tagValue = vData(1, ColumePoint)(0, lPoint)
                                    GC_ERROR_FLAG_0 = 0
                                Else
                                    tagValue = 0
                                    GC_ERROR_FLAG_0 = 1 ' Value cannot read 
                                End If

                                If (tempModel.TAG_VALUE = "") Then
                                    tempModel.TAG_VALUE = getTagData(vData(1, ColumePoint)(0, lPoint))
                                Else
                                    tempModel.TAG_VALUE = tempModel.TAG_VALUE & "," & getTagData(vData(1, ColumePoint)(0, lPoint))
                                End If

                                If (tempModel.TAG_CONVERT = "") Then
                                    tempModel.TAG_CONVERT = getConvertValue(tagValue, listExaTag(ColumePoint))
                                Else
                                    tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & getConvertValue(tagValue, listExaTag(ColumePoint))
                                End If

                                If (tempModel.TAG_GCERROR = "") Then
                                    tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString()
                                Else
                                    tempModel.TAG_GCERROR = tempModel.TAG_GCERROR & "," & GC_ERROR_FLAG_0.ToString()
                                End If
                            Next

                            listExaDowntimeData.Add(tempModel)

                            If (lNumberOfPoints <> 720) Then
                                Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                                createError.ERROR_LEVEL = 1
                                createError.LOG_DATE = Date.Now
                                createError.PROCESS_DESC = "Tag " & listExaTag(ColumePoint).EXA_TAG_NAME & " can not get total day (720 datas)"
                                createError.PROCESS_NAME = "EXAQUATUM"

                                listError.Add(createError)
                            End If

                        Catch ex As Exception
                            Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                            createError.ERROR_LEVEL = 1
                            createError.LOG_DATE = Date.Now
                            createError.PROCESS_DESC = ex.Message
                            createError.PROCESS_NAME = "EXAQUATUM"

                            listError.Add(createError)
                        End Try



                    Next

                    'Save data
                    Try
                        transDataServices.SaveListQMS_TR_EXQ_DOWNTIME(db, listExaDowntimeData, listExaTag, searchExaModel)
                        configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
                    Catch ex As Exception
                        Me.WriteToFile(ex.Message) '   MessageBox.Show(ex.Message)
                    End Try

                    Threading.Thread.Sleep(500)

                Next
            End If
        Catch ex As Exception
            Me.WriteToFile(ex.Message)
        End Try
    End Sub

    Public Sub loadAllExaReduceTag(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime)
        Try
            'Step 1 load all plant
            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim transDataServices As New TransactionService("ExaQuatum")
            Dim configDataServices As New ConfigServices("ExaQuatum")

            Dim searchExaModel As New ExaReduceFeedSearchModel

            Dim listPlant As List(Of ViewQMS_MA_PLANT) = masterDataServices.getQMS_MA_PLANTActiveList(db)
            Dim listExaTag As List(Of ViewQMS_MA_REDUCE_FEED_DETAIL)
            Dim listExaReduceData As New List(Of CreateQMS_TR_EXQ_REDUCE_FEED)()
            Dim listError As New List(Of CreateQMS_ST_SYSTEM_LOG)() 'new list error.
            Dim vData As Object
            Dim lNumberOfPoints As Long
            Dim lmaxColume As Long
            Dim startDate As DateTime = actionDate 'actionDate.AddDays(-1) '1440 = 1days
            Dim endDate As DateTime = actionDate.AddDays(1) 'actionDate

            Dim tempModel As CreateQMS_TR_EXQ_REDUCE_FEED

            If listPlant.Count() > 0 Then
                For Each plant As ViewQMS_MA_PLANT In listPlant
                    'Step 2 list Active ReduceFeed Tag
                    listExaTag = masterDataServices.getQMS_MA_REDUCE_FEED_DETAILActiveListByPlantAndActiveDate(db, plant.ID, actionDate)
                    vData = getReduceExaTagData(listExaTag, startDate, endDate)
                    lNumberOfPoints = UBound(vData(1, 0), 2)
                    lmaxColume = UBound(vData, 2)

                    listExaReduceData = New List(Of CreateQMS_TR_EXQ_REDUCE_FEED) 'reset value

                    Dim listExaId As String
                    Dim tagValue As Decimal = 0
                    Dim GC_ERROR_FLAG_0 As Byte = 0

                    searchExaModel.START_DATE = startDate.AddMinutes(2) 'Add for protect delete
                    searchExaModel.END_DATE = endDate
                    searchExaModel.PLANT_ID = plant.ID

                    listExaId = ""
                    For Each exaTag As ViewQMS_MA_REDUCE_FEED_DETAIL In listExaTag
                        If (listExaId = "") Then
                            listExaId = exaTag.ID.ToString()
                        Else
                            listExaId = listExaId & "," & exaTag.ID.ToString()
                        End If
                    Next


                    For ColumePoint = 0 To lmaxColume

                        tempModel = New CreateQMS_TR_EXQ_REDUCE_FEED
                        tempModel.ID = 0
                        tempModel.PLANT_ID = plant.ID
                        tempModel.TAG_DATE = searchExaModel.START_DATE
                        tempModel.TAG_EXA_ID = listExaTag(ColumePoint).ID
                        tempModel.TAG_VALUE = ""
                        tempModel.TAG_CONVERT = ""
                        tempModel.TAG_GCERROR = ""
                        tempModel.STATUS_1 = CAL_OFF_STATUS.INITIAL
                        tempModel.STATUS_2 = CAL_OFF_STATUS.INITIAL

                        tagValue = 0
                        GC_ERROR_FLAG_0 = 0
                        Try
                            For lPoint = 0 To lNumberOfPoints
                                If IsNumeric(vData(1, ColumePoint)(0, lPoint)) Then 'if number can be checking. 
                                    tagValue = vData(1, ColumePoint)(0, lPoint)
                                    GC_ERROR_FLAG_0 = 0
                                Else
                                    tagValue = 0
                                    GC_ERROR_FLAG_0 = 1 ' Value cannot read 
                                End If

                                If (tempModel.TAG_VALUE = "") Then
                                    tempModel.TAG_VALUE = getTagData(vData(1, ColumePoint)(0, lPoint))
                                Else
                                    tempModel.TAG_VALUE = tempModel.TAG_VALUE & "," & getTagData(vData(1, ColumePoint)(0, lPoint))
                                End If

                                If (tempModel.TAG_CONVERT = "") Then
                                    tempModel.TAG_CONVERT = getConvertValue(tagValue, listExaTag(ColumePoint))
                                Else
                                    tempModel.TAG_CONVERT = tempModel.TAG_CONVERT & "," & getConvertValue(tagValue, listExaTag(ColumePoint))
                                End If

                                If (tempModel.TAG_GCERROR = "") Then
                                    tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString()
                                Else
                                    tempModel.TAG_GCERROR = tempModel.TAG_GCERROR & "," & GC_ERROR_FLAG_0.ToString()
                                End If
                            Next
                            listExaReduceData.Add(tempModel)

                            If (lNumberOfPoints <> 720) Then
                                Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                                createError.ERROR_LEVEL = 1
                                createError.LOG_DATE = Date.Now
                                createError.PROCESS_DESC = "Tag " & listExaTag(ColumePoint).EXA_TAG_NAME & " can not get total day (720 datas)"
                                createError.PROCESS_NAME = "EXAQUATUM"

                                listError.Add(createError)
                            End If

                        Catch ex As Exception
                            Dim createError As New CreateQMS_ST_SYSTEM_LOG()

                            createError.ERROR_LEVEL = 1
                            createError.LOG_DATE = Date.Now
                            createError.PROCESS_DESC = ex.Message
                            createError.PROCESS_NAME = "EXAQUATUM"

                            listError.Add(createError)
                        End Try

                    Next
                    'Save data
                    Try
                        transDataServices.SaveListQMS_TR_EXQ_REDUCE_FEED(db, listExaReduceData, listExaTag, searchExaModel)
                        configDataServices.SaveListQMS_ST_SYSTEM_LOG(db, listError)
                    Catch ex As Exception
                        Me.WriteToFile(ex.Message) 'MessageBox.Show(ex.Message)
                    End Try

                    Threading.Thread.Sleep(500)
                Next
            End If
        Catch ex As Exception
            Me.WriteToFile(ex.Message)
        End Try
    End Sub

    Public Sub loadAllExaDataBySchedule(db As QMSSystem.CoreDB.QMSDBEntities)

        Dim configDataServices As New ConfigServices("ExaQuatum")
        Dim listSchedule As List(Of ViewQMS_ST_EXAQUATUM_SCHEDULE)
        Dim actionDate As DateTime

        Try
            'Step 1 load all schedule 
            listSchedule = configDataServices.getQMS_ST_EXAQUATUM_SCHEDULEListByStatus(db, EXA_SCHEDULE_STATUS.PENDING)

            If listSchedule.Count() > 0 Then

                For Each schedule As ViewQMS_ST_EXAQUATUM_SCHEDULE In listSchedule
                    Try
                        actionDate = New DateTime(schedule.START_DATE.Year, schedule.START_DATE.Month, schedule.START_DATE.Day)
                        'Step 2 loop day until finished 
                        Do Until (actionDate > schedule.END_DATE)
                            loadAllExaProductTagByPlantID(db, actionDate, schedule.PLANT_ID)
                            loadAllExaDowntimeTag(db, actionDate)
                            loadAllExaReduceTag(db, actionDate)
                            actionDate = actionDate.AddDays(1)
                        Loop

                        schedule.DOC_STATUS = EXA_SCHEDULE_STATUS.CONFIRM
                    Catch ex As Exception
                        schedule.DOC_STATUS = EXA_SCHEDULE_STATUS.FAIL
                    End Try

                    'Step 3 update status
                    Dim createModel As New CreateQMS_ST_EXAQUATUM_SCHEDULE()
                    createModel.ID = schedule.ID
                    createModel.PLANT_ID = schedule.PLANT_ID
                    createModel.START_DATE = schedule.START_DATE
                    createModel.END_DATE = schedule.END_DATE
                    createModel.DOC_STATUS = schedule.DOC_STATUS

                    configDataServices.SaveQMS_ST_EXAQUATUM_SCHEDULE(db, createModel)

                Next

            End If

        Catch ex As Exception

        End Try
    End Sub
    Public Sub loadAllExaProductTagByPlantID(db As QMSSystem.CoreDB.QMSDBEntities, actionDate As DateTime, plantId As Long)

        Try
            Dim startDate As Date
            Dim endDate As Date

            Dim masterDataServices As New MasterDataService("ExaQuatum")
            Dim listPlant As New List(Of ViewQMS_MA_PLANT)()
            Dim listProduct As New List(Of ViewQMS_MA_PRODUCT)()
            Dim listTag As New List(Of ViewQMS_MA_EXQ_TAG)()
            Dim listPlantProduct As New List(Of ViewQMS_MA_PRODUCT_MAPPING)()
            Dim tempControlSpec As New ViewQMS_MA_CONTROL_DATA

            listPlantProduct = masterDataServices.getQMS_MA_PRODUCT_MAPPINGListByPlantId(db, plantId)

            'Step 1. get PlantAndProductMapping
            If listPlantProduct.Count() > 0 Then
                startDate = actionDate 'actionDate.AddDays(-1) '1440 = 1days
                endDate = actionDate.AddDays(1) 'actionDate

                For Each plantProduct As ViewQMS_MA_PRODUCT_MAPPING In listPlantProduct

                    'Step 2 get Tag
                    Dim listQualityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listQuantityTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listAccumTag As New List(Of ViewQMS_MA_EXQ_TAG)()
                    Dim listResultTag As New List(Of ViewQMS_MA_EXQ_TAG)()

                    listQualityTag = masterDataServices.getQualityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listQuantityTag = masterDataServices.getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)
                    listAccumTag = masterDataServices.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, plantProduct.ID)

                    'Step 3 save product exatra to database
                    If listQualityTag IsNot Nothing And listQualityTag.Count() > 0 Then
                        listResultTag.AddRange(listQualityTag)
                        'loadExaDataToDatabase(db, listQualityTag, plantProduct, startDate, endDate)
                    End If

                    If listQuantityTag IsNot Nothing And listQuantityTag.Count() > 0 Then
                        listResultTag.AddRange(listQuantityTag)
                        'loadExaDataToDatabase(db, listQuantityTag, plantProduct, startDate, endDate)
                    End If

                    If listAccumTag IsNot Nothing And listAccumTag.Count() > 0 Then
                        listResultTag.AddRange(listAccumTag)
                        'loadExaAccumDataToDatabase(db, listAccumTag, plantProduct, startDate, endDate)
                    End If

                    If listResultTag IsNot Nothing And listResultTag.Count() > 0 Then
                        loadExaDataToQMS_TR_EXQ_PRODUCT(db, listResultTag, plantProduct, startDate, endDate)
                    End If

                    Threading.Thread.Sleep(500)

                    'Exit For 'Temp exit add for test
                Next
            End If

        Catch ex As Exception
            ' MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class
