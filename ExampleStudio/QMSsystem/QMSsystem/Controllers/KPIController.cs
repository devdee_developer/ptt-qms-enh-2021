﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_KPI }, DataAccess = DataAccessEnum.Authen)]
    public class KPIController : BaseController
    {
        //
        // GET: /KPI/
        public ActionResult ListMasterKPI()//control ListMasterKPI.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            KPISearchModel searchKPI = new KPISearchModel();
            CreateQMS_MA_KPI createQMS_MA_KPI = new CreateQMS_MA_KPI(); 
            createQMS_MA_KPI.ACTIVE_DATE = getDummyDateTime();

            
            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

            ViewBag.searchKPI = searchKPI;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_KPI = createQMS_MA_KPI;

            return View();
        } 

        public ActionResult CreateMasterKPI(long id = 0)//control CresteMasterKPI.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_KPI createQMS_MA_KPI = new CreateQMS_MA_KPI();
            string actionName = Resources.Strings.AddNew;

            createQMS_MA_KPI.ACTIVE_DATE = getDummyDateTime();

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_KPI = masterDataServices.getQMS_MA_KPIById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

            ViewBag.createQMS_MA_KPI = createQMS_MA_KPI;

            ViewBag.ActionName = actionName;
            return View();
        } 

        public JsonResult CreateQMS_MA_KPI(CreateQMS_MA_KPI model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_KPI(_db, model);

                    if (nResult > 0)
                    {
                        model = masterDataServices.getQMS_MA_KPIById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_KPI , PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home }, DataAccess = DataAccessEnum.Authen)]
        public JsonResult GetQMS_MA_KPIBySearch(KPISearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_KPI> listData = new List<ViewQMS_MA_KPI>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_KPI(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_KPI(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_KPIByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
