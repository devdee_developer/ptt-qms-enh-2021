﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.Model;
using QMSSystem.CoreDB.Services;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_Abnormal }, DataAccess = DataAccessEnum.Authen)]
    public class AbnormalController : BaseController
    {
        //
        // GET: /Abnormal/

        public ActionResult ListManageAbnormal()//control ListAbnormalExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            AbnormalSearchModel searchAbnormal = new AbnormalSearchModel();
            CreateQMS_TR_ABNORMAL createQMS_TR_ABNORMAL = new CreateQMS_TR_ABNORMAL();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel(); 
            createQMS_TR_ABNORMAL.START_DATE = getDummyDateTime();
            createQMS_TR_ABNORMAL.END_DATE = getDummyDateTime();

            createQMS_TR_ABNORMAL.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.ABNORMAL;

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusListWithInitail();
            ViewBag.listDocStatusEx = this.getDocStatusList();

            ViewBag.searchAbnormal = searchAbnormal;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_TR_ABNORMAL = createQMS_TR_ABNORMAL;
            ViewBag.volumeSearch = volumeSearch;

            return View();
        }

        public ActionResult CreateManageAbnormal(long id = 0)//control CresteAbnormalExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_ABNORMAL createQMS_TR_ABNORMAL = new CreateQMS_TR_ABNORMAL();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_ABNORMAL.START_DATE = getDummyDateTime();
            createQMS_TR_ABNORMAL.END_DATE = getDummyDateTime();

            createQMS_TR_ABNORMAL.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.ABNORMAL;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_ABNORMAL = transactionServices.getQMS_TR_ABNORMALById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();

            ViewBag.createQMS_TR_ABNORMAL = createQMS_TR_ABNORMAL;
            ViewBag.volumeSearch = volumeSearch;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_ABNORMAL(CreateQMS_TR_ABNORMAL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);

                    VALIDATE_EXCEPTION_OVERLLAP checkOverlap = transactionServices.IsOverlapMasterCorrectDataEx(_db, model.ID, model.PLANT_ID, model.PRODUCT_ID, 0, model.START_DATE, model.END_DATE, (byte)CORRECT_DATA_TYPE.ABNORMAL);
                     if (checkOverlap.IS_OVERLAP)
                     {
                         result = new { result = ReturnStatus.ERROR, message = checkOverlap.MSG_ERROR };
                     }
                     else
                     {
                         long nResult = transactionServices.SaveQMS_TR_ABNORMAL(_db, model);

                         if (nResult > 0)
                         {
                             transactionServices.SaveQMS_TR_OFF_CONTROL_CAL_With_ABNORMAL(_db, model); //Adjust
                             model = transactionServices.getQMS_TR_ABNORMALById(_db, nResult);
                             result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                         }
                         else
                         {
                             result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                         }
                     }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_ABNORMALBySearch(AbnormalSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_ABNORMAL> listData = new List<ViewQMS_TR_ABNORMAL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_ABNORMAL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_ABNORMAL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_ABNORMALByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_ABNORMAL(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_ABNORMALByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public void AbnormalDataExcel(DateTime START_DATE, DateTime END_DATE, long PLANT_ID, long PRODUCT_ID)//(ProductExaDataSearchModel searchModel)
        //{
        //    TransactionService transDataServices = new TransactionService(_username);
        //    ProductExaDataSearchModel searchModel = new ProductExaDataSearchModel();

        //    searchModel.PRODUCT_ID = PLANT_ID;
        //    searchModel.PLANT_ID = PRODUCT_ID;
        //    searchModel.START_DATE = START_DATE;
        //    searchModel.END_DATE = END_DATE;

        //    ExcelPackage excel = transDataServices.ProductExaDataExcel(_db, searchModel);

        //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.TrendData + ".xlsx");
        //    Response.BinaryWrite(excel.GetAsByteArray());

        //}


        public void AbnormalDataExcel(ProductExaDataSearchModel searchModel)
        {
            TransactionService transDataServices = new TransactionService(_username);
            //ProductExaDataSearchModel searchModel = new ProductExaDataSearchModel();

            //searchModel.PRODUCT_ID = PLANT_ID;
            //searchModel.PLANT_ID = PRODUCT_ID;
            //searchModel.START_DATE = START_DATE;
            //searchModel.END_DATE = END_DATE;

            ExcelPackage excel = transDataServices.ProductExaDataExcel(_db, searchModel);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.Abnormal + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }

    }
}
