﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.Model;
using QMSSystem.CoreDB.Services;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_Unit }, DataAccess = DataAccessEnum.Authen)]
    public class UnitController : BaseController
    {
        //
        // GET: /Unit/

        public ActionResult ListMasterUnit()
        {
            UnitSearchModel searchUnit = new UnitSearchModel();
            CreateQMS_MA_UNIT createQMS_MA_UNIT = new CreateQMS_MA_UNIT();
            searchUnit.Name = "";

            ViewBag.searchUnit = searchUnit;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_MA_UNIT = createQMS_MA_UNIT;
            return View();
        }

        public ActionResult CreateMasterUnit(long id=0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_UNIT createQMS_MA_UNIT = new CreateQMS_MA_UNIT();
            string actionName = Resources.Strings.AddNew;
             

            if (id > 0)
            {
                actionName = Resources.Strings.EditData; 
                createQMS_MA_UNIT = masterDataServices.getQMS_MA_UNITById(_db, id);
            }
             
            ViewBag.createQMS_MA_UNIT = createQMS_MA_UNIT;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_MA_UNIT(CreateQMS_MA_UNIT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool checkReuslt = masterDataServices.checkDuplicateName(_db, model); //


                    if (true == checkReuslt)
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.UnitDuplicate };
                    }
                    else
                    {
                        long nResult = masterDataServices.SaveQMS_MA_UNIT(_db, model);

                        if (nResult > 0)
                        {
                            model = masterDataServices.getQMS_MA_UNITById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    } 
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActiveUnitList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_UNIT> listData = new List<ViewQMS_MA_UNIT>();
                    listData = masterDataServices.getQMS_MA_UNITList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_UNITBySearch(UnitSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username); 

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_UNIT> listData = new List<ViewQMS_MA_UNIT>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_UNIT(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        } 

        public JsonResult DeleteQMS_MA_UNIT(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_UNITByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
