﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QMSSystem.CoreDB
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class QMSDBEntities : DbContext
    {
        public QMSDBEntities()
            : base("name=QMSDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<QMS_MA_CONTROL> QMS_MA_CONTROL { get; set; }
        public virtual DbSet<QMS_MA_CONTROL_COLUMN> QMS_MA_CONTROL_COLUMN { get; set; }
        public virtual DbSet<QMS_MA_CONTROL_DATA> QMS_MA_CONTROL_DATA { get; set; }
        public virtual DbSet<QMS_MA_CONTROL_DATACONF> QMS_MA_CONTROL_DATACONF { get; set; }
        public virtual DbSet<QMS_MA_CONTROL_ROW> QMS_MA_CONTROL_ROW { get; set; }
        public virtual DbSet<QMS_MA_CORRECT_DATA> QMS_MA_CORRECT_DATA { get; set; }
        public virtual DbSet<QMS_MA_CPK_INTERVAL_SAMPLING> QMS_MA_CPK_INTERVAL_SAMPLING { get; set; }
        public virtual DbSet<QMS_MA_CUSTOMER_MERGE> QMS_MA_CUSTOMER_MERGE { get; set; }
        public virtual DbSet<QMS_MA_DOWNTIME> QMS_MA_DOWNTIME { get; set; }
        public virtual DbSet<QMS_MA_DOWNTIME_DETAIL> QMS_MA_DOWNTIME_DETAIL { get; set; }
        public virtual DbSet<QMS_MA_EMAIL> QMS_MA_EMAIL { get; set; }
        public virtual DbSet<QMS_MA_EXQ_ACCUM_TAG> QMS_MA_EXQ_ACCUM_TAG { get; set; }
        public virtual DbSet<QMS_MA_EXQ_TAG> QMS_MA_EXQ_TAG { get; set; }
        public virtual DbSet<QMS_MA_GRADE> QMS_MA_GRADE { get; set; }
        public virtual DbSet<QMS_MA_IQC_CONTROLDATA> QMS_MA_IQC_CONTROLDATA { get; set; }
        public virtual DbSet<QMS_MA_IQC_CONTROLHISTORY> QMS_MA_IQC_CONTROLHISTORY { get; set; }
        public virtual DbSet<QMS_MA_IQC_CONTROLREASON> QMS_MA_IQC_CONTROLREASON { get; set; }
        public virtual DbSet<QMS_MA_IQC_CONTROLRULE> QMS_MA_IQC_CONTROLRULE { get; set; }
        public virtual DbSet<QMS_MA_IQC_EQUIP> QMS_MA_IQC_EQUIP { get; set; }
        public virtual DbSet<QMS_MA_IQC_FORMULA> QMS_MA_IQC_FORMULA { get; set; }
        public virtual DbSet<QMS_MA_IQC_ITEM> QMS_MA_IQC_ITEM { get; set; }
        public virtual DbSet<QMS_MA_IQC_ITEMEQUIP> QMS_MA_IQC_ITEMEQUIP { get; set; }
        public virtual DbSet<QMS_MA_IQC_MAILALERT> QMS_MA_IQC_MAILALERT { get; set; }
        public virtual DbSet<QMS_MA_IQC_METHOD> QMS_MA_IQC_METHOD { get; set; }
        public virtual DbSet<QMS_MA_IQC_POSSICAUSE> QMS_MA_IQC_POSSICAUSE { get; set; }
        public virtual DbSet<QMS_MA_IQC_RULE> QMS_MA_IQC_RULE { get; set; }
        public virtual DbSet<QMS_MA_IQC_TEMPLATE> QMS_MA_IQC_TEMPLATE { get; set; }
        public virtual DbSet<QMS_MA_KPI> QMS_MA_KPI { get; set; }
        public virtual DbSet<QMS_MA_OPERATION_SHIFT> QMS_MA_OPERATION_SHIFT { get; set; }
        public virtual DbSet<QMS_MA_PLANT> QMS_MA_PLANT { get; set; }
        public virtual DbSet<QMS_MA_PRODUCT> QMS_MA_PRODUCT { get; set; }
        public virtual DbSet<QMS_MA_PRODUCT_MAPPING> QMS_MA_PRODUCT_MAPPING { get; set; }
        public virtual DbSet<QMS_MA_REDUCE_FEED> QMS_MA_REDUCE_FEED { get; set; }
        public virtual DbSet<QMS_MA_REDUCE_FEED_DETAIL> QMS_MA_REDUCE_FEED_DETAIL { get; set; }
        public virtual DbSet<QMS_MA_ROOT_CAUSE> QMS_MA_ROOT_CAUSE { get; set; }
        public virtual DbSet<QMS_MA_ROOT_CAUSE_TYPE> QMS_MA_ROOT_CAUSE_TYPE { get; set; }
        public virtual DbSet<QMS_MA_SIGMA_LEVEL> QMS_MA_SIGMA_LEVEL { get; set; }
        public virtual DbSet<QMS_MA_SORTING_AREA> QMS_MA_SORTING_AREA { get; set; }
        public virtual DbSet<QMS_MA_SORTING_ITEM> QMS_MA_SORTING_ITEM { get; set; }
        public virtual DbSet<QMS_MA_SORTING_SAMPLE> QMS_MA_SORTING_SAMPLE { get; set; }
        public virtual DbSet<QMS_MA_TEMPLATE> QMS_MA_TEMPLATE { get; set; }
        public virtual DbSet<QMS_MA_TEMPLATE_COLUMN> QMS_MA_TEMPLATE_COLUMN { get; set; }
        public virtual DbSet<QMS_MA_TEMPLATE_CONTROL> QMS_MA_TEMPLATE_CONTROL { get; set; }
        public virtual DbSet<QMS_MA_TEMPLATE_ROW> QMS_MA_TEMPLATE_ROW { get; set; }
        public virtual DbSet<QMS_MA_UNIT> QMS_MA_UNIT { get; set; }
        public virtual DbSet<QMS_RP_OFF_CONTROL> QMS_RP_OFF_CONTROL { get; set; }
        public virtual DbSet<QMS_RP_OFF_CONTROL_ATTACH> QMS_RP_OFF_CONTROL_ATTACH { get; set; }
        public virtual DbSet<QMS_RP_OFF_CONTROL_DETAIL> QMS_RP_OFF_CONTROL_DETAIL { get; set; }
        public virtual DbSet<QMS_RP_OFF_CONTROL_GRAPH> QMS_RP_OFF_CONTROL_GRAPH { get; set; }
        public virtual DbSet<QMS_RP_OFF_CONTROL_SUMMARY> QMS_RP_OFF_CONTROL_SUMMARY { get; set; }
        public virtual DbSet<QMS_RP_TREND_DATA> QMS_RP_TREND_DATA { get; set; }
        public virtual DbSet<QMS_RP_TREND_DATA_DETAIL> QMS_RP_TREND_DATA_DETAIL { get; set; }
        public virtual DbSet<QMS_ST_AUTHORIZATION> QMS_ST_AUTHORIZATION { get; set; }
        public virtual DbSet<QMS_ST_EXAQUATUM_SCHEDULE> QMS_ST_EXAQUATUM_SCHEDULE { get; set; }
        public virtual DbSet<QMS_ST_EXPORT_PREFIX> QMS_ST_EXPORT_PREFIX { get; set; }
        public virtual DbSet<QMS_ST_FILE_ATTACH> QMS_ST_FILE_ATTACH { get; set; }
        public virtual DbSet<QMS_ST_SYSTEM_CONFIG> QMS_ST_SYSTEM_CONFIG { get; set; }
        public virtual DbSet<QMS_ST_SYSTEM_LOG> QMS_ST_SYSTEM_LOG { get; set; }
        public virtual DbSet<QMS_ST_SYSTEM_USER_STAMP> QMS_ST_SYSTEM_USER_STAMP { get; set; }
        public virtual DbSet<QMS_TR_ABNORMAL> QMS_TR_ABNORMAL { get; set; }
        public virtual DbSet<QMS_TR_BLUEPRINT_ABNORMAL> QMS_TR_BLUEPRINT_ABNORMAL { get; set; }
        public virtual DbSet<QMS_TR_CHANGE_GRADE> QMS_TR_CHANGE_GRADE { get; set; }
        public virtual DbSet<QMS_TR_COA_DATA> QMS_TR_COA_DATA { get; set; }
        public virtual DbSet<QMS_TR_CPK_TEMP_DATA> QMS_TR_CPK_TEMP_DATA { get; set; }
        public virtual DbSet<QMS_TR_CROSS_VOLUME> QMS_TR_CROSS_VOLUME { get; set; }
        public virtual DbSet<QMS_TR_DOWNTIME> QMS_TR_DOWNTIME { get; set; }
        public virtual DbSet<QMS_TR_EVENT_CAL> QMS_TR_EVENT_CAL { get; set; }
        public virtual DbSet<QMS_TR_EVENT_RAW_DATA> QMS_TR_EVENT_RAW_DATA { get; set; }
        public virtual DbSet<QMS_TR_EXCEPTION> QMS_TR_EXCEPTION { get; set; }
        public virtual DbSet<QMS_TR_EXQ_DOWNTIME> QMS_TR_EXQ_DOWNTIME { get; set; }
        public virtual DbSet<QMS_TR_EXQ_PRODUCT> QMS_TR_EXQ_PRODUCT { get; set; }
        public virtual DbSet<QMS_TR_EXQ_REDUCE_FEED> QMS_TR_EXQ_REDUCE_FEED { get; set; }
        public virtual DbSet<QMS_TR_FILE_TREND_DATA> QMS_TR_FILE_TREND_DATA { get; set; }
        public virtual DbSet<QMS_TR_IQC_CONFIG> QMS_TR_IQC_CONFIG { get; set; }
        public virtual DbSet<QMS_TR_IQC_CONTROL> QMS_TR_IQC_CONTROL { get; set; }
        public virtual DbSet<QMS_TR_OFF_CONTROL_CAL> QMS_TR_OFF_CONTROL_CAL { get; set; }
        public virtual DbSet<QMS_TR_PRODUCT_REMARK> QMS_TR_PRODUCT_REMARK { get; set; }
        public virtual DbSet<QMS_TR_RAW_DATA> QMS_TR_RAW_DATA { get; set; }
        public virtual DbSet<QMS_TR_REDUCE_FEED> QMS_TR_REDUCE_FEED { get; set; }
        public virtual DbSet<QMS_TR_TEMPLATE_COA> QMS_TR_TEMPLATE_COA { get; set; }
        public virtual DbSet<QMS_TR_TEMPLATE_COA_DETAIL> QMS_TR_TEMPLATE_COA_DETAIL { get; set; }
        public virtual DbSet<QMS_TR_TREND_DATA> QMS_TR_TREND_DATA { get; set; }
        public virtual DbSet<QMS_TR_TURN_AROUND> QMS_TR_TURN_AROUND { get; set; }
        public virtual DbSet<QMS_MA_IQC_DRAFTCONTROLRULE> QMS_MA_IQC_DRAFTCONTROLRULE { get; set; }
        public virtual DbSet<QMS_MA_PRODUCT_MERGE> QMS_MA_PRODUCT_MERGE { get; set; }
    }
}
