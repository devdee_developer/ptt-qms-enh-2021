//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QMSSystem.CoreDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS_MA_IQC_CONTROLRULE
    {
        public long ID { get; set; }
        public long HISTORY_ID { get; set; }
        public long RULE_ID { get; set; }
        public System.DateTime POINT_START { get; set; }
        public Nullable<System.DateTime> POINT_END { get; set; }
        public byte DELETE_FLAG { get; set; }
        public string CREATE_USER { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public System.DateTime UPDATE_DATE { get; set; }
    }
}
