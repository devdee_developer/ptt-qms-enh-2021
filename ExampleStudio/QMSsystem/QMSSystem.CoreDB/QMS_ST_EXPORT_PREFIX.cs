//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QMSSystem.CoreDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS_ST_EXPORT_PREFIX
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string DOWNTIME { get; set; }
        public string DOWNTIME_REMARK { get; set; }
        public string REDUCE_FEED { get; set; }
        public string REDUCE_FEED_REMARK { get; set; }
        public string LPG_GRADE { get; set; }
        public string GC_ERROR_1 { get; set; }
        public string GC_ERROR_2 { get; set; }
        public string GC_ERROR_3 { get; set; }
        public string GC_ERROR_4 { get; set; }
        public string GC_ERROR_5 { get; set; }
        public string GC_ERROR_REMARK { get; set; }
        public string GC_CYCCOUNT_1 { get; set; }
        public string GC_CYCCOUNT_2 { get; set; }
        public string GC_CYCCOUNT_3 { get; set; }
        public string GC_CYCCOUNT_4 { get; set; }
        public string GC_CYCCOUNT_5 { get; set; }
        public string GC_CYCCOUNT_6 { get; set; }
        public string GC_CYCCOUNT_7 { get; set; }
        public string GC_CYCCOUNT_8 { get; set; }
        public string GC_CYCCOUNT_9 { get; set; }
        public string GC_CYCCOUNT_10 { get; set; }
        public string CREATE_USER { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public System.DateTime UPDATE_DATE { get; set; }
    }
}
