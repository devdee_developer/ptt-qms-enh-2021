﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.Model;
using QMSSystem.CoreDB.Services;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_SpecGas }, DataAccess = DataAccessEnum.Authen)]
    public class SpecValueController : BaseController
    {
        //
        // GET: /SpecValue/
         

        public ActionResult ListMasterSpecGas()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchControlValue = new ControlValueSearchModel(); 

            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel  searchControlValueData = new ControlValueDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            searchControlValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.SPEC;
            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.SPEC;

            ViewBag.searchControlValue = searchControlValue;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            return View();
        }


        public ActionResult CreateMasterSpecGas(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            CreateQMS_MA_CONTROL_ROW createQMS_MA_CONTROL_ROW = new CreateQMS_MA_CONTROL_ROW();
            CreateQMS_MA_CONTROL_COLUMN createQMS_MA_CONTROL_COLUMN = new CreateQMS_MA_CONTROL_COLUMN();
            CreateQMS_MA_CONTROL_DATA createQMS_MA_CONTROL_DATA = new CreateQMS_MA_CONTROL_DATA();

            ControlValueRowSearchModel searchSample = new ControlValueRowSearchModel();
            ControlValueColumnSearchModel searchItem = new ControlValueColumnSearchModel();
            ControlValueDataSearchModel searchControlValue = new ControlValueDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);

            string actionName = Resources.Strings.AddNew;

            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.SPEC;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_CONTROL = masterDataServices.getQMS_MA_CONTROLById(_db, id);
                createQMS_MA_CONTROL_ROW.CONTROL_ID = id;
                createQMS_MA_CONTROL_COLUMN.CONTROL_ID = id;
            }

            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.createQMS_MA_CONTROL_ROW = createQMS_MA_CONTROL_ROW;
            ViewBag.createQMS_MA_CONTROL_COLUMN = createQMS_MA_CONTROL_COLUMN;
            ViewBag.createQMS_MA_CONTROL_DATA = createQMS_MA_CONTROL_DATA;

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;

            ViewBag.ActionName = actionName;
            return View();
        }
    }
}
