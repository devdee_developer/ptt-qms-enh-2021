﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using System.Web.Script.Serialization;
using QMSsystem.Models.Privilege;
using QMSsystem.Models.Helper;
using QMSSystem.CoreDB;
using System.Configuration;
using System.Data;
using System.Drawing;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;

namespace QMSsystem.Controllers
{
    public class LoginController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Index()
        { 
            ViewBag.Message = "";
            UserService usrService = new UserService("system");
            
            try
            {
                //Get Data From Cookie.... 
                if ((Session["SiteSession"] != null) || (Request.Cookies["QmsCookie"] != null)) //
                { 
                    if (Session["SiteSession"] != null)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    
                    SessionModel chkModel = new SessionModel();
                    String cookieDate_string = "";
                    string userName = "";
                    string szPassword = "";

                    userName = (Request.Cookies["QmsCookie"].Values["QmsData"] != null) ? Request.Cookies["QmsCookie"].Values["QmsData"].ToString() : "";
                    szPassword = (Request.Cookies["QmsCookie"].Values["QmsValue"] != null) ? Request.Cookies["QmsCookie"].Values["QmsValue"].ToString() : "";
                    cookieDate_string = (Request.Cookies["QmsCookie"].Values["Expiers"] != null) ? Request.Cookies["QmsCookie"].Values["Expiers"].ToString() : "";

                    if ("" != cookieDate_string && DateTime.Now < Convert.ToDateTime(cookieDate_string))
                    {
                        if (userName != "")
                        {
                            userName = Encryption.Decrypt(userName);
                        }

                        if (szPassword != "")
                        {
                            szPassword = Encryption.Decrypt(szPassword);
                        } 
                    }

                    return CheckUserLogin(userName, szPassword, true);

                }
            }
            catch (Exception ex)
            {
                //do something
            }


            return View();
        }

        protected ActionResult CheckUserLogin(string userName, string password, bool RememberMe)
        {
            UserService usrService = new UserService();
            ConfigServices configServices = new ConfigServices("");
            bool bResult = usrService.IsCheckUserExistEx(userName, password); //ตรวจสอบ user domain or back door

            if (true == bResult)//login Domain ok
            {
                SiteSession siteSession = new SiteSession();
                string szImagePath = "";
                string szUserShowName = "";
                string szDeptName = "";

                int bIsAdminAccess = usrService.isAdminAuthenAccess(userName, ConfigurationManager.ConnectionStrings["PISSYTEM"].ToString(), out szImagePath, out szUserShowName, out szDeptName); //ตรวจสอบ PIS or back door สิทธิ์

                if (bIsAdminAccess == (int)AuthenResult.Deny)
                {
                    ViewBag.Message = @Resources.Strings.AuthorizationFailure;
                   
                }
                else if (bIsAdminAccess == (int)AuthenResult.Admin || bIsAdminAccess == (int)AuthenResult.User )
                {
                    bool bIsAdmin = (bIsAdminAccess == (int)AuthenResult.Admin) ? true : false;

                    CreateQMS_ST_AUTHORIZATION createUser = new CreateQMS_ST_AUTHORIZATION();

                    if (bIsAdmin)
                    {
                        createUser = configServices.getCreateQMS_ST_AUTHORIZATIONByGROUP_AUTH(_db, (int)GROUP_AUTHORIZATION.ADMIN);
                    }
                    else
                    {
                        createUser = configServices.getCreateQMS_ST_AUTHORIZATIONByGROUP_AUTH(_db, (int)GROUP_AUTHORIZATION.USER);
                    }

                    if (createUser.LIST_AUTH != null && createUser.LIST_AUTH != "")
                    {
                        createUser.listAuth = JsonConvert.DeserializeObject<List<AuthenAccess>>(createUser.LIST_AUTH) as List<AuthenAccess>;
                        createUser.listAuth = configServices.convertToCurrentListAuthenAccess(createUser.listAuth);
                    }
                    else
                    {
                        createUser.listAuth = usrService.getDefaultListAuthenAccess(false);  
                    }

                    siteSession.m_Authen = createUser.listAuth;
                    siteSession.Username = userName;
                    siteSession.UserShowName = szUserShowName;
                    siteSession.ImagePath = szImagePath;
                    siteSession.DeptName = szDeptName;

                    Session["SiteSession"] = siteSession;
                    return this.ReturnHomeView(userName, password, bIsAdmin, RememberMe); 
                }

                return View(); 
            }
            else
            {
                return View(); 
            }
        } 

        protected ActionResult ReturnHomeView(string userName, string password, bool isAdmin, bool forever)
        {
            if (true == forever)
            {
                Response.Cookies["QmsCookie"].Secure = true;
                Response.Cookies["QmsCookie"].SameSite = SameSiteMode.Strict;
                Response.Cookies["QmsCookie"].HttpOnly = true;
                Response.Cookies["QmsCookie"].Expires = DateTime.Now.AddDays(365);
                Response.Cookies["QmsCookie"].Values["Expiers"] = DateTime.Now.AddDays(365).ToString();
                Response.Cookies["QmsCookie"].Values["QmsData"] = Encryption.Encrypt(userName);
                Response.Cookies["QmsCookie"].Values["QmsValue"] = Encryption.Encrypt(password);
            }
            else
            {
                Response.Cookies["QmsCookie"].Secure = true;
                Response.Cookies["QmsCookie"].SameSite = SameSiteMode.Strict;
                Response.Cookies["QmsCookie"].HttpOnly = true;
                Response.Cookies["QmsCookie"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["QmsCookie"].Values["Expiers"] = DateTime.Now.AddDays(-1).ToString();
                Response.Cookies["QmsCookie"].Values["QmsData"] = "";
                Response.Cookies["QmsCookie"].Values["QmsValue"] = "";
            }
 
            return RedirectToAction("Index", "Home");
        } 

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(string userName, string password, bool RememberMe = false)
        { 
            UserService usrService = new UserService(); 
            ViewBag.Message = @Resources.Strings.LoginErrorMsg; 
            return CheckUserLogin(userName, password, RememberMe);
        }

        [AllowAnonymous]
        public ActionResult Clear()
        {
            //clear 
            SiteSession siteSession = (Session["SiteSession"] == null ? null : (SiteSession)Session["SiteSession"]);

            if (siteSession != null)
            {
                Session["SiteSession"] = null;
            }

            if (Request.Cookies.Get("QmsCookie") != null)
            {
                Response.Cookies["QmsCookie"].Secure = true;
                Response.Cookies["QmsCookie"].SameSite = SameSiteMode.Strict;
                Response.Cookies["QmsCookie"].HttpOnly = true;
                Response.Cookies["QmsCookie"].Expires = DateTime.Now.AddDays(-1); 
                Response.Cookies["QmsCookie"].Values["Expiers"] = DateTime.Now.AddDays(-1).ToString();
                Response.Cookies["QmsCookie"].Values["QmsData"] = "";
                Response.Cookies["QmsCookie"].Values["QmsValue"] = "";

                
                //HttpCookie myCookie = new HttpCookie("QmsCookie");
                //myCookie.Expires = DateTime.Now.AddDays(-1d);
                //myCookie.Values["pw"] = "";
                //Response.Cookies.Add(myCookie);
            }


            return RedirectToAction("Index", "Login");
        }


        [AllowAnonymous]
        public ActionResult Error500()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Error404()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult LoginByToken(AccessToken acctoken)
        {

            try
            {
                SearchPersonnelInfoMod searchPerson = new SearchPersonnelInfoMod();
                if (Session["SiteSession"] != null)
                {
                    return RedirectToAction("Index", "Home");
                }

                var access_token = acctoken.access_token;
                searchPerson.Search_EmployeeCode = acctoken.empcode;
                TokenMod responseResult = new TokenMod();

                int bIsAdminAccess = 0;
                if (access_token == "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJzdWIiOiJoeXJDOHNqWkA5c2RwcCQiLCJuYW1lIjoicW1zYXV0aCIsImlhdCI6MjAyMzA5MjR9eDThsbIwMqQQRjQ1UsZEbX1GbPA2YN5iEr4CL3EWkU")
                {
                    var listUnitcode = getAllUnitCode();
                    var personInfo = getPersonInfo(searchPerson);
                    var person = personInfo.personList[0];

                    if (listUnitcode.resultCode == 200)
                    {
                        if (personInfo.resultCode == 200)
                        {
                            var unit = listUnitcode.unitList;
                            foreach (UnitMod item in unit)
                            {

                                if (person.UNITCODE == item.unitcode)
                                {
                                    if (person.UNITCODE == "80000607")
                                    {
                                        bIsAdminAccess = 99;
                                    }
                                    else
                                    {
                                        bIsAdminAccess = 1;
                                    }
                                }
                            }
                            if (bIsAdminAccess == 0)
                            {
                                return View("~/Views/Error/Error403.cshtml");
                            }

                            return CheckLoginMock(person, bIsAdminAccess, acctoken.empcode);
                        }
                        else
                        {
                            return View("~/Views/Error/Error402.cshtml");

                        }

                    }
                    else
                    {
                        return View("~/Views/Error/Error401.cshtml");

                    }
                }
                else
                {
                    //Response.Write("401 Unauthorized");
                    return View("~/Views/Error/Error401.cshtml");
                    //return RedirectToAction("Index", "Login");
                }

            }
            catch (Exception ex)
            {
                //do something
            }

            return View();
        }

        public ActionResult CheckLoginMock(PersonnelInfoMod person, int bIsAdminAccess, string empcode )
        {

            UserService usrService = new UserService();
            ConfigServices configServices = new ConfigServices("");
            SiteSession siteSession = new SiteSession();
            bool bIsAdmin = (bIsAdminAccess == 99) ? true : false;

            var unitCode = "";
            var fullname = "";
            var userName = person.FNAME;
            var szUserShowName = "";
            var szImagePath = "https://hq-web-s13.pttplc.com/directory/photo/"+ empcode + ".jpg";
            var szDeptName = "";

            CreateQMS_ST_AUTHORIZATION createUser = new CreateQMS_ST_AUTHORIZATION();

            if (bIsAdmin)
            {
                createUser = configServices.getCreateQMS_ST_AUTHORIZATIONByGROUP_AUTH(_db, (int)GROUP_AUTHORIZATION.ADMIN);
            }
            else
            {
                createUser = configServices.getCreateQMS_ST_AUTHORIZATIONByGROUP_AUTH(_db, (int)GROUP_AUTHORIZATION.USER);
            }

            if (createUser.LIST_AUTH != null && createUser.LIST_AUTH != "")
            {
                createUser.listAuth = JsonConvert.DeserializeObject<List<AuthenAccess>>(createUser.LIST_AUTH) as List<AuthenAccess>;
                createUser.listAuth = configServices.convertToCurrentListAuthenAccess(createUser.listAuth);
            }
            else
            {
                createUser.listAuth = usrService.getDefaultListAuthenAccess(false);
            }
            siteSession.m_Authen = createUser.listAuth;
            siteSession.Username = userName;
            siteSession.UserShowName = person.FULLNAMETH;
            siteSession.ImagePath = szImagePath;
            siteSession.DeptName = szDeptName;
            var password = "";

            Session["SiteSession"] = siteSession;
            return this.ReturnHomeView(userName, password, bIsAdmin, true);

        }


        [HttpPost]
        public ResponseUnitListMod getAllUnitCode()
        {  //HttpServletRequest request, HttpServletResponse res, @RequestBody(required = false) ReqAlarmBean bean) {

            UserService usrService = new UserService();
            ResponseUnitListMod response = new ResponseUnitListMod();
            try
            {
                PISServices personService = new PISServices();
                string apiTokenUrl = System.Configuration.ConfigurationManager.AppSettings["API_PIS_TOKEN"];
                string basicAuthen = System.Configuration.ConfigurationManager.AppSettings["API_PIS_BASIC_AUTHEN"];
                string personInfoUrl = System.Configuration.ConfigurationManager.AppSettings["API_PIS_PERSONINFO"];
                string unitUrl = System.Configuration.ConfigurationManager.AppSettings["API_PIS_UNIT"];

                string szUnitAbb = "ผยก.";// ConfigurationManager.AppSettings["GAS_UNITCODE"];
                string unitCode = "80000563";

                SearchUnitMod searchUnit = new SearchUnitMod();
                //searchUnit.Search_UnitAbb = szUnitAbb;
                searchUnit.Search_UnitCode = unitCode;
                SearchUnitMod searchUnit2 = new SearchUnitMod();
                List<UnitMod> listUnitCode = personService.getUnitInfo(searchUnit, apiTokenUrl, basicAuthen, unitUrl); // getUnitInfo(searchUnit); //getDummyRelateionshipByUniCode
                //UnitMod objUnit = listUnitCode[0];
                //searchUnit2.Search_DummyRelationship = "1-1-6-4-9";
                foreach (var obj in listUnitCode)
                {
                    if (obj.unitabbr == "ผยก.")
                    {
                        searchUnit2.Search_DummyRelationship = obj.DUMMY_RELATIONSHIP;
                    }
                }

                if (searchUnit2.Search_DummyRelationship == null)
                {
                    response.resultMess = "False";
                    response.resultCode = 400;
                }
                else
                {
                    searchUnit2.NumberOfRecordPerPage = 500;
                    searchUnit2.Search_UnitLevelID = "060";
                    List<UnitMod> list60 = personService.getUnitInfo(searchUnit2, apiTokenUrl, basicAuthen, unitUrl);
                    searchUnit2.Search_UnitLevelID = "050";
                    List<UnitMod> list50 = personService.getUnitInfo(searchUnit2, apiTokenUrl, basicAuthen, unitUrl);

                    //SearchUnitMod searchModel = new SearchUnitMod();
                    //searchModel.Search_UnitLevelID = "060";
                    //List<UnitMod> list60  = personService.getUnitInfo(searchModel, apiTokenUrl, basicAuthen, unitUrl);
                    //searchModel.Search_UnitLevelID = "050";
                    //List<UnitMod> list50 = personService.getUnitInfo(searchModel, apiTokenUrl, basicAuthen, unitUrl); 
                    list60.AddRange(list50);

                    ResponseUnitListMod resUnitList = new ResponseUnitListMod();
                    resUnitList.unitList = list60;
                    response = resUnitList;
                    //response = this.iconnectService.searchIconnectCatCode(bean.szCategoryCode, bean.szCategoryName, bean.lTemplateEnum, bean.szEnableFlag, szCatId);
                    response.resultMess = "Success";
                    response.resultCode = 200;
                }
              
            }
            catch (Exception e)
            {
                response.resultMess = e.Message;
            }
            return response;
        }

        [HttpPost]
        public ResponsePersonInfoListMod getPersonInfo(SearchPersonnelInfoMod searchPerson)
        {  //HttpServletRequest request, HttpServletResponse res, @RequestBody(required = false) ReqAlarmBean bean) {
            ResponsePersonInfoListMod response = new ResponsePersonInfoListMod();
            UserService usrService = new UserService();
            try
            {
                PISServices personService = new PISServices();
                string apiTokenUrl = System.Configuration.ConfigurationManager.AppSettings["API_PIS_TOKEN"];
                string basicAuthen = System.Configuration.ConfigurationManager.AppSettings["API_PIS_BASIC_AUTHEN"];
                string personInfoUrl = System.Configuration.ConfigurationManager.AppSettings["API_PIS_PERSONINFO"];
                string unitUrl = System.Configuration.ConfigurationManager.AppSettings["API_PIS_UNIT"];

                SearchUnitMod searchUnit = new SearchUnitMod();
                //searchUnit.Search_UnitCode = searchPerson.Search_UnitCode;
                searchUnit.Search_EmployeeCode = searchPerson.Search_EmployeeCode;
                //SearchUnitMod searchUnit2 = new SearchUnitMod();
                var listPersonTemp = personService.getPersonnelInfo(searchPerson, apiTokenUrl, basicAuthen, personInfoUrl);
                //List<UnitMod> listUnitCode = personService.getUnitInfo(searchUnit, apiTokenUrl, basicAuthen, unitUrl); // getUnitInfo(searchUnit); //getDummyRelateionshipByUniCode
                //UnitMod objUnit = listUnitCode[0];
                //searchUnit2.Search_DummyRelationship = objUnit.DUMMY_RELATIONSHIP;
                //searchUnit2.NumberOfRecordPerPage = 500;
                //List<UnitMod> listUnitCode2 = personService.getUnitInfo(searchUnit2, apiTokenUrl, basicAuthen, unitUrl);

                //List<PersonnelInfoMod> listPerson = new List<PersonnelInfoMod>();
                //foreach (UnitMod unitMod in listUnitCode2)
                //{
                //    searchPerson.Search_UnitCode = unitMod.unitcode;

                //    if (listPersonTemp != null && listPersonTemp.Count > 0)
                //    {
                //        listPersonTemp = listPersonTemp.Where(m => m.WSTCODE == "A" || m.WSTCODE == "B").ToList();
                //    }

                //    if (listPersonTemp != null && listPersonTemp.Count > 0)
                //    {
                //        listPerson.AddRange(listPersonTemp);
                //    }
                //}

                //ResponsePersonInfoListMod resPersonInfo = new ResponsePersonInfoListMod();
                //resPersonInfo.personList = listPersonTemp;
                response.personList = listPersonTemp;
                response.resultMess = "Success";
                response.resultCode = 200;
            }
            catch (Exception e)
            {
                response.resultMess = e.Message;
            }
            return response;
        }







    }
}
