<!DOCTYPE html>
<html lang="en">

  <head>

    <?php include("./head_tag.php"); ?>



  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"<span>PTT QMS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include("./user_profile.php"); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php include("./sidemenu_qms.php"); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include("./menu_footer.php"); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php include("./top_nav.php"); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>System Connect</h3>
                <div class="clearfix"></div>


              </div>
            </div>

            <div class="clearfix"></div>
            <hr>
            <!-- <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="row"  style="text-align:right;">


                    <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                    <span class="btn btn-success" style="color:white;"><i class="fa fa-save"></i> Save</span>
                  </div>
                  <div class="x_title">
                    <h2></h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">

                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Name
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Group of control value">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Status
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control">
                          <option>Choose Status</option>
                          <option></option>
                        </select>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Start
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                          <input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="Start date" aria-describedby="inputSuccess2Status">
                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          End
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                          <input type="text" class="form-control has-feedback-left" id="single_cal2" placeholder="Start date" aria-describedby="inputSuccess2Status">
                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
                      <br>
                    </div>
                  </div>
                </div>
              </div> -->

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Exaquantum / LIMS / Template </h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="exaquantum-tab" role="tab" data-toggle="tab" aria-expanded="true">Exaquantum System</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="lims-tab" data-toggle="tab" aria-expanded="false">LIMS System</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="template-tab" data-toggle="tab" aria-expanded="false">Template System</a>
                        </li>

                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="exaquantum-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:right;">
                                <br>
                                  <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
                                   <hr><br>
                                   <div class="row">
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                       Schedule Time
                                     </div>
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                       <select class="form-control">
                                         <option>Hour</option>
                                         <option>00</option>
                                         <option>01</option>
                                         <option>02</option>
                                         <option>03</option>
                                         <option>04</option>
                                         <option>05</option>
                                         <option>06</option>
                                         <option>07</option>
                                         <option>08</option>
                                         <option>09</option>
                                         <option>10</option>
                                         <option>11</option>
                                         <option>12</option>
                                       </select>
                                     </div>
                                     <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:center;">
                                       :
                                    </div>
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:left;">
                                       <select class="form-control">
                                         <option>Minute</option>
                                         <option>00</option>
                                         <option>01</option>
                                         <option>02</option>
                                         <option>03</option>
                                         <option>04</option>
                                         <option>05</option>
                                         <option>06</option>
                                         <option>07</option>
                                         <option>08</option>
                                         <option>09</option>
                                         <option>10</option>
                                         <option>11</option>
                                         <option>12</option>
                                         <option>...</option>
                                       </select>
                                     </div>
                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:left;">
                                       * ทุกวันจะทาการดึงข้อมูลตามช่วงเวลาที่ตั้งไว้ตรงนี้
                                     </div>

                                   </div>

                            </div>
                          </div>


                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="oc-lims-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:right;">
                                <br>
                                  <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
                                  <hr> <br>
                                   <div class="row">
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                        Server Name
                                     </div>
                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                       <input type="text" class="form-control" placeholder="">
                                     </div>

                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                     </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                       Schedule Time
                                     </div>
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                       <select class="form-control">
                                         <option>Hour</option>
                                         <option>00</option>
                                         <option>01</option>
                                         <option>02</option>
                                         <option>03</option>
                                         <option>04</option>
                                         <option>05</option>
                                         <option>06</option>
                                         <option>07</option>
                                         <option>08</option>
                                         <option>09</option>
                                         <option>10</option>
                                         <option>11</option>
                                         <option>12</option>
                                       </select>
                                     </div>
                                     <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:center;">
                                       :
                                    </div>
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:left;">
                                       <select class="form-control">
                                         <option>Minute</option>
                                         <option>00</option>
                                         <option>01</option>
                                         <option>02</option>
                                         <option>03</option>
                                         <option>04</option>
                                         <option>05</option>
                                         <option>06</option>
                                         <option>07</option>
                                         <option>08</option>
                                         <option>09</option>
                                         <option>10</option>
                                         <option>11</option>
                                         <option>12</option>
                                         <option>...</option>
                                       </select>
                                     </div>
                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:left;">
                                       * ทุกวันจะทาการดึงข้อมูลตามช่วงเวลาที่ตั้งไว้ตรงนี้
                                     </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                        Database User
                                     </div>
                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                       <input type="text" class="form-control" placeholder="">
                                     </div>

                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                     </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                        Password
                                     </div>
                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                       <input type="password" class="form-control" placeholder="" value="**********">
                                     </div>

                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                     </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                        SID
                                     </div>
                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                       <input type="text" class="form-control" placeholder="">
                                     </div>

                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;">
                                     </div>
                                   </div>
                            </div>
                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="template-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:right;">
                                <br>
                                  <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
                                  <hr>
                                   <br>

                                   <div class="row">
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                       Schedule Time
                                     </div>
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                       <select class="form-control">
                                         <option>Hour</option>
                                         <option>00</option>
                                         <option>01</option>
                                         <option>02</option>
                                         <option>03</option>
                                         <option>04</option>
                                         <option>05</option>
                                         <option>06</option>
                                         <option>07</option>
                                         <option>08</option>
                                         <option>09</option>
                                         <option>10</option>
                                         <option>11</option>
                                         <option>12</option>
                                       </select>
                                     </div>
                                     <div class="col-md-1 col-sm-1 col-xs-12" style="text-align:center;">
                                       :
                                    </div>
                                     <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:left;">
                                       <select class="form-control">
                                         <option>Minute</option>
                                         <option>00</option>
                                         <option>01</option>
                                         <option>02</option>
                                         <option>03</option>
                                         <option>04</option>
                                         <option>05</option>
                                         <option>06</option>
                                         <option>07</option>
                                         <option>08</option>
                                         <option>09</option>
                                         <option>10</option>
                                         <option>11</option>
                                         <option>12</option>
                                         <option>...</option>
                                       </select>
                                     </div>
                                     <div class="col-md-5 col-sm-5 col-xs-12" style="text-align:left;">
                                       * ทุกวันจะทาการดึงข้อมูลตามช่วงเวลาที่ตั้งไว้ตรงนี้
                                     </div>

                                   </div>

                            </div>
                          </div>

                        </div>


                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- Contol modal -->
        <div class="modal fade control-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Control Value</h4>
              </div>
              <div class="modal-body">
                <!-- <h4>เลือกรายงาน Off Spec Report ที่ต้องการ</h4> -->
                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      <input type="checkbox">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12" style="text-align:left;">
                      Show on Trend Data
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      <input type="checkbox">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12" style="text-align:left;">
                      CONC / LOADING
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      CONC   <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      LOADING  <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      Min   <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      Max  <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>
                  </div>
                </div>
                <br><br><br><br><br><br><br><br><br><br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-trash"></i> Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
              </div>

            </div>
          </div>
        </div>
        <!-- end Control modal -->
        <!-- footer content -->
          <?php include("./footer.php"); ?>
        <!-- /footer content -->
      </div>
    </div>


    <?php include("./footer_script.php"); ?>


    <!-- Flot -->

    <!-- /Flot -->

    <!-- jQuery Sparklines -->
    <script>
      $(document).ready(function() {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'bar',
          height: '40',
          barWidth: 9,
          colorMap: {
            '7': '#a1a1a1'
          },
          barSpacing: 2,
          barColor: '#26B99A'
        });

        $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'line',
          width: '200',
          height: '40',
          lineColor: '#26B99A',
          fillColor: 'rgba(223, 223, 223, 0.57)',
          lineWidth: 2,
          spotColor: '#26B99A',
          minSpotColor: '#26B99A'
        });
      });
    </script>
    <!-- /jQuery Sparklines -->

    <!-- Doughnut Chart -->

    <!-- /Doughnut Chart -->

    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'right',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };

        $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange_right').daterangepicker(optionSet1, cb);

        $('#reportrange_right').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange_right').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });

        $('#options1').click(function() {
          $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
        });

        $('#options2').click(function() {
          $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
        });

        $('#destroy').click(function() {
          $('#reportrange_right').data('daterangepicker').remove();
        });

      });
    </script>

    <script>
      $(document).ready(function() {
        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>

    <script>
      $(document).ready(function() {
        $('#single_cal1').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_1"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal2').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_2"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal3').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal4').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>

    <script>
      $(document).ready(function() {
        $('#reservation').daterangepicker(null, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- morris.js -->
    <script>
      $(document).ready(function() {
        Morris.Bar({
          element: 'graph_bar',
          data: [
            { "period": "Jan", "Hours worked": 80 },
            { "period": "Feb", "Hours worked": 125 },
            { "period": "Mar", "Hours worked": 176 },
            { "period": "Apr", "Hours worked": 224 },
            { "period": "May", "Hours worked": 265 },
            { "period": "Jun", "Hours worked": 314 },
            { "period": "Jul", "Hours worked": 347 },
            { "period": "Aug", "Hours worked": 287 },
            { "period": "Sep", "Hours worked": 240 },
            { "period": "Oct", "Hours worked": 211 }
          ],
          xkey: 'period',
          hideHover: 'auto',
          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          ykeys: ['Hours worked', 'sorned'],
          labels: ['Hours worked', 'SORN'],
          xLabelAngle: 60,
          resize: true
        });

        $MENU_TOGGLE.on('click', function() {
          $(window).resize();
        });
      });
    </script>
    <!-- /morris.js -->

    <!-- Skycons -->
    <script>
      var icons = new Skycons({
          "color": "#73879C"
        }),
        list = [
          "clear-day", "clear-night", "partly-cloudy-day",
          "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
          "fog"
        ],
        i;

      for (i = list.length; i--;)
        icons.set(list[i], list[i]);

      icons.play();
    </script>
    <!-- /Skycons -->

    <!-- gauge.js -->
    <script>
      var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
          length: 0.75,
          strokeWidth: 0.042,
          color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 100;
      gauge.animationSpeed = 32;
      gauge.set(80);
      gauge.setTextField(document.getElementById("gauge-text"));

      var target = document.getElementById('foo2'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 5000;
      gauge.animationSpeed = 32;
      gauge.set(4200);
      gauge.setTextField(document.getElementById("gauge-text2"));
    </script>
    <!-- /gauge.js -->
  </body>
</html>
