﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_Exception }, DataAccess = DataAccessEnum.Authen)]
    public class ExceptionController : BaseController
    {
        //
        // GET: /Exception/

        public ActionResult ListManageException()//control ListExceptionExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ExceptionSearchModel searchException = new ExceptionSearchModel();
            CreateQMS_TR_EXCEPTION createQMS_TR_EXCEPTION = new CreateQMS_TR_EXCEPTION();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();

            createQMS_TR_EXCEPTION.START_DATE = getDummyDateTime();
            createQMS_TR_EXCEPTION.END_DATE = getDummyDateTime();

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();
            ViewBag.listCorrectDataType = masterDataServices.getQMS_MA_CORRECT_DATAAllExceptionWithoutMaster(_db);

            ViewBag.searchException = searchException;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.listDocStatusEx = this.getDocStatusList();

            ViewBag.volumeSearch = volumeSearch;
            ViewBag.createQMS_TR_EXCEPTION = createQMS_TR_EXCEPTION;
            return View();
        }

        public ActionResult CreateManageException(long id = 0)//control CresteExceptionExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_EXCEPTION createQMS_TR_EXCEPTION = new CreateQMS_TR_EXCEPTION();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_EXCEPTION.START_DATE = getDummyDateTime();
            createQMS_TR_EXCEPTION.END_DATE = getDummyDateTime();

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_EXCEPTION = transactionServices.getQMS_TR_EXCEPTIONById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();
            ViewBag.listCorrectDataType = masterDataServices.getQMS_MA_CORRECT_DATAAllExceptionWithoutMaster(_db);
            ViewBag.volumeSearch = volumeSearch;
            ViewBag.createQMS_TR_EXCEPTION = createQMS_TR_EXCEPTION;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_EXCEPTION(CreateQMS_TR_EXCEPTION model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);

                    //Step 1. check validate correct data ...
                    VALIDATE_EXCEPTION_OVERLLAP modelResult = transactionServices.IsOverlapMasterCorrectData(_db, model);

                    if (true == modelResult.IS_OVERLAP)
                    {
                        result = new { result = ReturnStatus.ERROR, message = modelResult.MSG_ERROR };
                    }
                    else
                    {
                        //Step 2. save
                        long nResult = transactionServices.SaveQMS_TR_EXCEPTION(_db, model);

                        if (nResult > 0)
                        {
                            transactionServices.SaveQMS_TR_OFF_CONTROL_CAL_With_EXCEPTION(_db, model); //Adjust
                            model = transactionServices.getQMS_TR_EXCEPTIONById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_EXCEPTIONBySearch(ExceptionSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_EXCEPTION> listData = new List<ViewQMS_TR_EXCEPTION>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_EXCEPTION(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_EXCEPTION(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_EXCEPTIONByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_EXCEPTION(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_EXCEPTIONByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
