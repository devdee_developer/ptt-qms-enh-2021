﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSSystem.ExaDB;
using System.Configuration;
using QMSsystem.Models.Privilege; 

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_ChangeGrade }, DataAccess = DataAccessEnum.Authen)]
    public class ChangeGradeController : BaseController
    {
        //
        // GET: /ChangeGrade/



        public ActionResult ListManageChangeGrade()//control ListChangeGradeExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ChangeGradeSearchModel searchChangeGrade = new ChangeGradeSearchModel();
            CreateQMS_TR_CHANGE_GRADE createQMS_TR_CHANGE_GRADE = new CreateQMS_TR_CHANGE_GRADE();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel(); 

            createQMS_TR_CHANGE_GRADE.START_DATE = getDummyDateTime();
            createQMS_TR_CHANGE_GRADE.END_DATE = getDummyDateTime();
            createQMS_TR_CHANGE_GRADE.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.CHANGE_GRADE;

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusListWithInitail();
            ViewBag.listDocStatusEx = this.getDocStatusList();
            ViewBag.listFromGrade = masterDataServices.getQMS_MA_GRADEActiveList(_db);
            ViewBag.listToGrade = masterDataServices.getQMS_MA_GRADEActiveList(_db); 

            ViewBag.searchChangeGrade = searchChangeGrade;
            ViewBag.listPageSize = this.getPageSizeList();
             
            ViewBag.createQMS_TR_CHANGE_GRADE = createQMS_TR_CHANGE_GRADE;
            ViewBag.volumeSearch = volumeSearch;

            return View();
        }

        public ActionResult CreateManageChangeGrade(long id = 0)//control CresteChangeGradeExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_CHANGE_GRADE createQMS_TR_CHANGE_GRADE = new CreateQMS_TR_CHANGE_GRADE();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_CHANGE_GRADE.START_DATE = getDummyDateTime();
            createQMS_TR_CHANGE_GRADE.END_DATE = getDummyDateTime();
            createQMS_TR_CHANGE_GRADE.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.CHANGE_GRADE;
            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_CHANGE_GRADE = transactionServices.getQMS_TR_CHANGE_GRADEById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();
            ViewBag.listFromGrade = masterDataServices.getQMS_MA_GRADEActiveList(_db);
            ViewBag.listToGrade = masterDataServices.getQMS_MA_GRADEActiveList(_db);
            //ViewBag.listProductTo = masterDataServices.GetProductListByNotinId(_db, createQMS_TR_CROSS_VOLUME.PRODUCT_ID);

            ViewBag.createQMS_TR_CHANGE_GRADE = createQMS_TR_CHANGE_GRADE;
            ViewBag.volumeSearch = volumeSearch;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_CHANGE_GRADE(CreateQMS_TR_CHANGE_GRADE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);
                ExaCommandSQL exCmdSQL = new ExaCommandSQL(ConfigurationManager.ConnectionStrings["QMSSystemExaDB"].ToString());

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);
                    

                    VALIDATE_EXCEPTION_OVERLLAP checkOverlap = transactionServices.IsOverlapMasterCorrectDataEx(_db, model.ID, model.PLANT_ID, model.PRODUCT_ID, 0, model.START_DATE, model.END_DATE, (byte)CORRECT_DATA_TYPE.CHANGE_GRADE);
                    if (checkOverlap.IS_OVERLAP)
                    {
                        
                        result = new { result = ReturnStatus.ERROR, message = checkOverlap.MSG_ERROR };
                    }
                    else
                    {
                        long nResult = transactionServices.SaveQMS_TR_CHANGE_GRADE(_db, model);
                        //recal 
                        if (nResult > 0)
                        {
                            model.GRADE_CHECK = true; //alway true เพราะว่า เป็น คำสั่ง สร้าง delete = false;
                            transactionServices.SaveQMS_TR_OFF_CONTROL_CAL_With_CHANGE_GRADE(_db, model); //Adjust
                            model = transactionServices.getQMS_TR_CHANGE_GRADEById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_CHANGE_GRADEBySearch(ChangeGradeSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_CHANGE_GRADE> listData = new List<ViewQMS_TR_CHANGE_GRADE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_CHANGE_GRADE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_CHANGE_GRADE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_CHANGE_GRADEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_CHANGE_GRADE(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_CHANGE_GRADEByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetChangeGradeListByNotinId(long FromGradeId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_GRADE> listData = new List<ViewQMS_MA_GRADE>();
                    listData = masterDataServices.GetChangeGradeListByNotinId(_db, FromGradeId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetChangeGradeList(GetChangeGradeList model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    //get default gate ...
                    ViewQMS_MA_PRODUCT_MAPPING plantData = masterDataServices.getViewPlantIdProductId(_db, model.PLANT_ID, model.PRODUCT_ID);

                    List<ViewQMS_MA_GRADE> listData = new List<ViewQMS_MA_GRADE>();
                    listData = masterDataServices.GetChangeGradeListByNotinId(_db, plantData.GRADE_ID);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, default_grade = plantData  };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
