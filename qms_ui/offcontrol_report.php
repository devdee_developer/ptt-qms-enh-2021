<!DOCTYPE html>
<html lang="en">

  <head>

    <?php include("./head_tag.php"); ?>



  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"<span>PTT QMS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include("./user_profile.php"); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php include("./sidemenu_qms.php"); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include("./menu_footer.php"); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php include("./top_nav.php"); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Off Control Report (ชื่อรายงาน ...)</h3>
                <div class="clearfix"></div>
                <hr>
                <!-- <span class="btn btn-primary"><i class="fa fa-search"></i> Search Off Control Report</span> -->
                <!-- <br> -->

                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-search"></i> Search Off Control Report</button>
                  <br>
              </div>
              <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">

                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                      </button>
                      <h4 class="modal-title" id="myModalLabel">Dashboard</h4>
                    </div>
                    <div class="modal-body">
                      <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4>
                      <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
                      <br><br>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Off Control Report</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control">
                          <option>เลือกรายงาน Off Control Report</option>
                          <option>รายงานประจำวันที่ 2016-10-15</option>
                          <option>รายงานประจำวันที่ 2016-10-01</option>
                          <option>รายงานประจำวันที่ 2016-09-15</option>
                          <option>รายงานประจำวันที่ 2016-09-01</option>
                        </select>
                      </div>
                      <br><br>
                    </div>
                    <div class="modal-footer">
                      <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Run</button>
                    </div>

                  </div>
                </div>
              </div>

            </div>


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>% Product off control (compare to total production)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <il>ตรวจสอบรายละเอียด
                      </il>
                      <il><i class="fa fa-download"></i>
                      </il>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <!-- <img src="./images/qms/offcontrol_report1.png" alt=""/> -->
                      <table class="table table-bordered">
                        <thead>
                          <tr style="background-color:#e7cbcb;color:black;">
                            <th>Unit (%)</th>
                            <th>GSP 1</th>
                            <th>GSP 2</th>
                            <th>GSP 3</th>
                            <th>GSP 5</th>
                            <th>GSP 6</th>
                            <th>ESP</th>
                            <th >Net Product</th>
                          </tr>
                        </thead>
                        <tbody style="text-align:center;">
                          <tr>
                            <th scope="row">Ethane</th>
                            <td>1.85</td>
                            <td>0.54</td>
                            <td>0.21</td>
                            <td>0.11</td>
                            <td>0.10</td>
                            <td>0.04</td>
                            <td style="background-color:#f9e8a0;color:black;">0.41</td>
                          </tr>
                          <tr>
                            <th scope="row">Propane</th>
                            <td>0.09</td>
                            <td>0.33</td>
                            <td>0.72</td>
                            <td>0.09</td>
                            <td>0.05</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">0.41</td>
                          </tr>
                          <tr>
                            <th scope="row">Butane</th>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>4.96</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">4.96</td>
                          </tr>
                          <tr>
                            <th scope="row">LPG</th>
                            <td>3.68</td>
                            <td>3.77</td>
                            <td>5.42</td>
                            <td>3.22</td>
                            <td>0.17</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">3.96</td>
                          </tr>
                          <tr>
                            <th scope="row">NGL (Hi)</th>
                            <td>3.89</td>
                            <td>2.80</td>
                            <td>8.61</td>
                            <td>3.41</td>
                            <td>5.34</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">4.97</td>
                          </tr>
                          <tr>
                            <th scope="row">Net GSP</th>
                            <td style="background-color:#f9e8a0;color:black;">2.66</td>
                            <td style="background-color:#f9e8a0;color:black;">3.14</td>
                            <td style="background-color:#f9e8a0;color:black;">4.71</td>
                            <td style="background-color:#f9e8a0;color:black;">1.90</td>
                            <td style="background-color:#f9e8a0;color:black;">0.44</td>
                            <td style="background-color:#f9e8a0;color:black;">0.44</td>
                            <td style="background-color:red;color:white;"><b>1.90</b></td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Off Control Volume</h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <il>ตรวจสอบรายละเอียด
                      </il>
                      <il><i class="fa fa-download"></i>
                      </il>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <!-- <img src="./images/qms/offcontrol_report2.png" alt=""/> -->
                      <table class="table table-bordered">
                        <thead>
                          <tr style="background-color:#e7cbcb;color:black;">
                            <th>Off Control Volume (Tons)</th>
                            <th>GSP 1</th>
                            <th>GSP 2</th>
                            <th>GSP 3</th>
                            <th>GSP 5</th>
                            <th>GSP 6</th>
                            <th>ESP</th>
                            <th >Net Product</th>
                          </tr>
                        </thead>
                        <tbody style="text-align:center;">
                          <tr>
                            <th scope="row">Ethane</th>
                            <td>1.85</td>
                            <td>0.54</td>
                            <td>0.21</td>
                            <td>0.11</td>
                            <td>0.10</td>
                            <td>0.04</td>
                            <td style="background-color:#f9e8a0;color:black;">0.41</td>
                          </tr>
                          <tr>
                            <th scope="row">Propane</th>
                            <td>0.09</td>
                            <td>0.33</td>
                            <td>0.72</td>
                            <td>0.09</td>
                            <td>0.05</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">0.41</td>
                          </tr>
                          <tr>
                            <th scope="row">Butane</th>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>4.96</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">4.96</td>
                          </tr>
                          <tr>
                            <th scope="row">LPG</th>
                            <td>3.68</td>
                            <td>3.77</td>
                            <td>5.42</td>
                            <td>3.22</td>
                            <td>0.17</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">3.96</td>
                          </tr>
                          <tr>
                            <th scope="row">NGL (Hi)</th>
                            <td>3.89</td>
                            <td>2.80</td>
                            <td>8.61</td>
                            <td>3.41</td>
                            <td>5.34</td>
                            <td>-</td>
                            <td style="background-color:#f9e8a0;color:black;">4.97</td>
                          </tr>
                          <tr>
                            <th scope="row">Net GSP</th>
                            <td style="background-color:#f9e8a0;color:black;">2.66</td>
                            <td style="background-color:#f9e8a0;color:black;">3.14</td>
                            <td style="background-color:#f9e8a0;color:black;">4.71</td>
                            <td style="background-color:#f9e8a0;color:black;">1.90</td>
                            <td style="background-color:#f9e8a0;color:black;">0.44</td>
                            <td style="background-color:#f9e8a0;color:black;">0.44</td>
                            <td style="background-color:red;color:white;"><b>1.90</b></td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Total Production</h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <il>ตรวจสอบรายละเอียด
                      </il>
                      <il><i class="fa fa-download"></i>
                      </il>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <!-- <img src="./images/qms/offcontrol_report3.png" alt=""/> -->
                      <table class="table table-bordered">
                        <thead>
                          <tr style="background-color:#e7cbcb;color:black;">
                            <th>Total Production (Tons)</th>
                            <th>GSP 1</th>
                            <th>GSP 2</th>
                            <th>GSP 3</th>
                            <th>GSP 5</th>
                            <th>GSP 6</th>
                            <th>ESP</th>
                            <th >Net Product</th>
                          </tr>
                        </thead>
                        <tbody style="text-align:center;">
                          <tr>
                            <th scope="row">Ethane</th>
                            <td>89,284.67</td>
                            <td>14,785.93</td>
                            <td>19,017.79</td>
                            <td>111,045.89</td>
                            <td>135,360.29</td>
                            <td>131,584.51</td>
                            <td style="background-color:#f9e8a0;color:black;">501,079.08</td>
                          </tr>
                          <tr>
                            <th scope="row">Propane</th>
                            <td>25,749.91</td>
                            <td>9,015.64</td>
                            <td>24,813.25</td>
                            <td>28,954.74</td>
                            <td>141,665.26</td>
                            <td>0.00</td>
                            <td style="background-color:#f9e8a0;color:black;">230,198.79</td>
                          </tr>
                          <tr>
                            <th scope="row">Butane</th>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>97,330.37</td>
                            <td>0.00</td>
                            <td style="background-color:#f9e8a0;color:black;">97,330.37</td>
                          </tr>
                          <tr>
                            <th scope="row">LPG</th>
                            <td>101,896.63</td>
                            <td>103,703.54</td>
                            <td>127,034.92</td>
                            <td>161,240.20</td>
                            <td>8,731.59</td>
                            <td>0.00</td>
                            <td style="background-color:#f9e8a0;color:black;">502,606.88</td>
                          </tr>
                          <tr>
                            <th scope="row">NGL (Hi)</th>
                            <td>20,966.85</td>
                            <td>16,391.63</td>
                            <td>20,939.25</td>
                            <td>23,525.79</td>
                            <td>30,212.68</td>
                            <td>0.00</td>
                            <td style="background-color:#f9e8a0;color:black;">112,036.20</td>
                          </tr>
                          <tr>
                            <th scope="row">Net GSP</th>
                            <td style="background-color:#f9e8a0;color:black;">237898.06</td>
                            <td style="background-color:#f9e8a0;color:black;">143,896.74</td>
                            <td style="background-color:#f9e8a0;color:black;">191,805.21</td>
                            <td style="background-color:#f9e8a0;color:black;">324,766.62</td>
                            <td style="background-color:#f9e8a0;color:black;">413,300.19</td>
                            <td style="background-color:#f9e8a0;color:black;">131,212.68</td>
                            <td style="background-color:red;color:white;"><b>1,443,251.32</b></td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <!-- <h2>Total Production</h2> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <!-- <il>ตรวจสอบรายละเอียด
                      </il> -->
                      <il><i class="fa fa-download"></i>
                      </il>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <img src="./images/qms/offcontrol_report4.png" alt="" style="width:100%;"/>
                  </div>
                </div>
              </div>
              

              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel tile fixed_height_320 overflow_hidden">
                  <div class="x_title">
                    <h2>Reason</h2>
                    <ul class="nav navbar-right panel_toolbox">


                      <li><i class="fa fa-download"></i>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="" style="width:100%">
                      <tr>
                        <th style="width:37%;">
                          <p></p>
                        </th>
                        <th>
                          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                            <p class="">Reason</p>
                          </div>
                          <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            <p class="">(%)</p>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <td>
                          <!-- <canvas id="canvas1" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas> -->
                          <img src="./images/qms/pie_chart1.png" alt="" />
                        </td>
                        <td>
                          <table class="tile_info">
                            <tr>
                              <td>
                                <p><i class="fa fa-square blue"></i>Reason1 </p>
                              </td>
                              <td>30%</td>
                            </tr>
                            <tr>
                              <td>
                                <p><i class="fa fa-square green"></i>Reason2 </p>
                              </td>
                              <td>10%</td>
                            </tr>
                            <tr>
                              <td>
                                <p><i class="fa fa-square purple"></i>Reason3 </p>
                              </td>
                              <td>20%</td>
                            </tr>
                            <tr>
                              <td>
                                <p><i class="fa fa-square aero"></i>Reason4 </p>
                              </td>
                              <td>15%</td>
                            </tr>
                            <tr>
                              <td>
                                <p><i class="fa fa-square red"></i>Reason5 </p>
                              </td>
                              <td>30%</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>หมายเหตุ (Text Input)</h2>
                      <!-- <ul class="nav navbar-right panel_toolbox">
                        <il>ตรวจสอบรายละเอียด
                        </il>
                        <il><i class="fa fa-download"></i>
                        </il>
                      </ul> -->
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <textarea id="message" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">

                      </textarea>
                      <br>
                      <span class="btn btn-primary">บันทึกข้อความ</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>เอกสารแนบ เพิ่มเติม</h2>
                      <!-- <ul class="nav navbar-right panel_toolbox">
                        <il>ตรวจสอบรายละเอียด
                        </il>
                        <il><i class="fa fa-download"></i>
                        </il>
                      </ul> -->
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content" >
                          แนบ file เอกสารเพิ่มเติม คลิก...

                          <span class="btn btn-primary">Browse</span>

                        <hr>
                        <div class="row">
                          <i class="fa fa-list"></i> list รายการเอกสารแนบ
                          <br><br>
                          <a href="#"><i class="fa fa-download"></i> รายงาน off control วันที่ 2016-10-15</a>
                          <br>
                          <a href="#"><i class="fa fa-download"></i> รายงาน off control วันที่ 2016-10-1</a>
                        </div>

                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php include("./footer.php"); ?>
        <!-- /footer content -->
      </div>
    </div>


    <?php include("./footer_script.php"); ?>



    <!-- Flot -->
    <script>
      $(document).ready(function() {
        //random data
        var d1 = [
          [0, 1],
          [1, 9],
          [2, 6],
          [3, 10],
          [4, 5],
          [5, 17],
          [6, 6],
          [7, 10],
          [8, 7],
          [9, 11],
          [10, 35],
          [11, 9],
          [12, 12],
          [13, 5],
          [14, 3],
          [15, 4],
          [16, 9]
        ];

        //flot options
        var options = {
          series: {
            curvedLines: {
              apply: true,
              active: true,
              monotonicFit: true
            }
          },
          colors: ["#26B99A"],
          grid: {
            borderWidth: {
              top: 0,
              right: 0,
              bottom: 1,
              left: 1
            },
            borderColor: {
              bottom: "#7F8790",
              left: "#7F8790"
            }
          }
        };
        var plot = $.plot($("#placeholder3xx3"), [{
          label: "Registrations",
          data: d1,
          lines: {
            fillColor: "rgba(150, 202, 89, 0.12)"
          }, //#96CA59 rgba(150, 202, 89, 0.42)
          points: {
            fillColor: "#fff"
          }
        }], options);
      });
    </script>
    <!-- /Flot -->

    <!-- jQuery Sparklines -->
    <script>
      $(document).ready(function() {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'bar',
          height: '40',
          barWidth: 9,
          colorMap: {
            '7': '#a1a1a1'
          },
          barSpacing: 2,
          barColor: '#26B99A'
        });

        $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'line',
          width: '200',
          height: '40',
          lineColor: '#26B99A',
          fillColor: 'rgba(223, 223, 223, 0.57)',
          lineWidth: 2,
          spotColor: '#26B99A',
          minSpotColor: '#26B99A'
        });
      });
    </script>
    <!-- /jQuery Sparklines -->

    <!-- Doughnut Chart -->
    <script>
      $(document).ready(function() {
        var options = {
          legend: false,
          responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
              "Symbian",
              "Blackberry",
              "Other",
              "Android",
              "IOS"
            ],
            datasets: [{
              data: [15, 20, 30, 10, 30],
              backgroundColor: [
                "#BDC3C7",
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB"
              ],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });
      });
    </script>
    <!-- /Doughnut Chart -->

    <!-- bootstrap-daterangepicker -->
    <script type="text/javascript">
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- morris.js -->
    <script>
      $(document).ready(function() {
        Morris.Bar({
          element: 'graph_bar',
          data: [
            { "period": "Jan", "Hours worked": 80 },
            { "period": "Feb", "Hours worked": 125 },
            { "period": "Mar", "Hours worked": 176 },
            { "period": "Apr", "Hours worked": 224 },
            { "period": "May", "Hours worked": 265 },
            { "period": "Jun", "Hours worked": 314 },
            { "period": "Jul", "Hours worked": 347 },
            { "period": "Aug", "Hours worked": 287 },
            { "period": "Sep", "Hours worked": 240 },
            { "period": "Oct", "Hours worked": 211 }
          ],
          xkey: 'period',
          hideHover: 'auto',
          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          ykeys: ['Hours worked', 'sorned'],
          labels: ['Hours worked', 'SORN'],
          xLabelAngle: 60,
          resize: true
        });

        $MENU_TOGGLE.on('click', function() {
          $(window).resize();
        });
      });
    </script>
    <!-- /morris.js -->

    <!-- Skycons -->
    <script>
      var icons = new Skycons({
          "color": "#73879C"
        }),
        list = [
          "clear-day", "clear-night", "partly-cloudy-day",
          "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
          "fog"
        ],
        i;

      for (i = list.length; i--;)
        icons.set(list[i], list[i]);

      icons.play();
    </script>
    <!-- /Skycons -->

    <!-- gauge.js -->
    <script>
      var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
          length: 0.75,
          strokeWidth: 0.042,
          color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 100;
      gauge.animationSpeed = 32;
      gauge.set(80);
      gauge.setTextField(document.getElementById("gauge-text"));

      var target = document.getElementById('foo2'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 5000;
      gauge.animationSpeed = 32;
      gauge.set(4200);
      gauge.setTextField(document.getElementById("gauge-text2"));
    </script>
    <!-- /gauge.js -->
  </body>
</html>
