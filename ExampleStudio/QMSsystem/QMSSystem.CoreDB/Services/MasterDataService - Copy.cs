﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QMSSystem.Model;
using QMSSystem.CoreDB.Helper.Grid;
using System.IO;
using Excel;
using System.Data;
using System.Globalization;
using OfficeOpenXml;
 

namespace QMSSystem.CoreDB.Services
{
    public class MasterDataService : BaseService 
    {
        public MasterDataService(string userName) : base(userName) { }

        #region QMS_MA_COA_CUSTOMER
        //public List<ViewQMS_MA_COA_CUSTOMER> getQMS_MA_COA_CUSTOMERList(QMSDBEntities db)
        //{
        //    List<ViewQMS_MA_COA_CUSTOMER> listResult = new List<ViewQMS_MA_COA_CUSTOMER>();

        //    try
        //    {
        //        List<QMS_MA_COA_CUSTOMER> list = QMS_MA_COA_CUSTOMER.GetAll(db);

        //        if (null != list && list.Count() > 0)
        //        {
        //            listResult = (from dt in list
        //                          select new ViewQMS_MA_COA_CUSTOMER
        //                          {
        //                              ID = dt.ID,
        //                              NAME = dt.NAME,
        //                              DELETE_FLAG = dt.DELETE_FLAG
        //                          }).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.writeErrorLog(ex.Message);
        //    }

        //    return listResult;
        //}

        //private QMS_MA_COA_CUSTOMER convertModelToDB(CreateQMS_MA_COA_CUSTOMER model)
        //{
        //    QMS_MA_COA_CUSTOMER result = new QMS_MA_COA_CUSTOMER();

        //    result.ID = model.ID;
        //    result.NAME = (null == model.NAME) ? "" : model.NAME;
        //    result.CREATE_DATE = DateTime.Now;
        //    result.CREATE_USER = _currentUserName;
        //    result.UPDATE_DATE = DateTime.Now;
        //    result.UPDATE_USER = _currentUserName;

        //    return result;
        //}

        //public bool DeleteQMS_MA_COA_CUSTOMERById(QMSDBEntities db, long id)
        //{
        //    bool bResult = false;
        //    QMS_MA_COA_CUSTOMER result = new QMS_MA_COA_CUSTOMER();
        //    result = QMS_MA_COA_CUSTOMER.GetById(db, id);
        //    try
        //    {
        //        if (null != result)
        //        {
        //            result.DELETE_FLAG = 1;
        //            result.UPDATE_DATE = DateTime.Now;
        //            result.UPDATE_USER = _currentUserName;
        //            db.SaveChanges();
        //            bResult = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }

        //    return bResult;
        //}


        //public long SaveQMS_MA_COA_CUSTOMER(QMSDBEntities db, CreateQMS_MA_COA_CUSTOMER model)
        //{
        //    long result = 0;

        //    if (model.ID > 0)
        //    {
        //        result = UpdateQMS_MA_COA_CUSTOMER(db, model);
        //    }
        //    else
        //    {
        //        result = AddQMS_MA_COA_CUSTOMER(db, convertModelToDB(model));
        //    }

        //    return result;
        //}

        //public long UpdateQMS_MA_COA_CUSTOMER(QMSDBEntities db, CreateQMS_MA_COA_CUSTOMER model)
        //{
        //    long result = 0;

        //    try
        //    {
        //        QMS_MA_COA_CUSTOMER dt = new QMS_MA_COA_CUSTOMER();
        //        dt = QMS_MA_COA_CUSTOMER.GetById(db, model.ID);

        //        dt.NAME = (null == model.NAME) ? "" : model.NAME;
    

        //        //dt.CREATE_DATE = DateTime.Now;
        //        //dt.CREATE_USER = _currentUserName;
        //        dt.UPDATE_DATE = DateTime.Now;
        //        dt.UPDATE_USER = _currentUserName;

        //        db.SaveChanges();
        //        result = model.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }
        //    return result;
        //}

        //public long AddQMS_MA_COA_CUSTOMER(QMSDBEntities _db, QMS_MA_COA_CUSTOMER model)
        //{
        //    long result = 0;
        //    //try
        //    //{
        //    //    QMS_MA_COA_CUSTOMER dt = new QMS_MA_COA_CUSTOMER();
        //    //    dt = QMS_MA_COA_CUSTOMER.GetById(_db, model.ID);

        //    //    if (null == dt)
        //    //    {
        //    //        //model.bank_code = this.getBankCode(_db, model);
        //    //        _db.QMS_MA_COA_CUSTOMER.AddObject(model);
        //    //        _db.SaveChanges();
        //    //        result = model.ID;
        //    //    }
        //    //    else
        //    //    {
        //    //        _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
        //    //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    _errMsg = this.getEexceptionError(ex);
        //    //}
        //    return result;
        //}
        #endregion

        #region QMS_MA_COA_ITEM
        //public List<ViewQMS_MA_COA_ITEM> getQMS_MA_COA_ITEMList(QMSDBEntities db)
        //{
        //    List<ViewQMS_MA_COA_ITEM> listResult = new List<ViewQMS_MA_COA_ITEM>();

        //    try
        //    {
        //        List<QMS_MA_COA_ITEM> list = QMS_MA_COA_ITEM.GetAll(db);

        //        if (null != list && list.Count() > 0)
        //        {
        //            listResult = (from dt in list
        //                          select new ViewQMS_MA_COA_ITEM
        //                          {
        //                              ID = dt.ID,
        //                              NAME = dt.NAME,
        //                              DELETE_FLAG = dt.DELETE_FLAG 
        //                          }).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.writeErrorLog(ex.Message);
        //    }

        //    return listResult;
        //}

        //private QMS_MA_COA_ITEM convertModelToDB(CreateQMS_MA_COA_ITEM model)
        //{
        //    QMS_MA_COA_ITEM result = new QMS_MA_COA_ITEM();

        //    result.ID = model.ID;
        //    result.NAME = (null == model.NAME) ? "" : model.NAME;
            
        //    result.CREATE_DATE = DateTime.Now;
        //    result.CREATE_USER = _currentUserName;
        //    result.UPDATE_DATE = DateTime.Now;
        //    result.UPDATE_USER = _currentUserName;

        //    return result;
        //}

        //public bool DeleteQMS_MA_COA_ITEMById(QMSDBEntities db, long id)
        //{
        //    bool bResult = false;
        //    QMS_MA_COA_ITEM result = new QMS_MA_COA_ITEM();
        //    result = QMS_MA_COA_ITEM.GetById(db, id);
        //    try
        //    {
        //        if (null != result)
        //        {
        //            result.DELETE_FLAG = 1;
        //            result.UPDATE_DATE = DateTime.Now;
        //            result.UPDATE_USER = _currentUserName;
        //            db.SaveChanges();
        //            bResult = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }

        //    return bResult;
        //}


        //public long SaveQMS_MA_COA_ITEM(QMSDBEntities db, CreateQMS_MA_COA_ITEM model)
        //{
        //    long result = 0;

        //    if (model.ID > 0)
        //    {
        //        result = UpdateQMS_MA_COA_ITEM(db, model);
        //    }
        //    else
        //    {
        //        result = AddQMS_MA_COA_ITEM(db, convertModelToDB(model));
        //    }

        //    return result;
        //}

        //public long UpdateQMS_MA_COA_ITEM(QMSDBEntities db, CreateQMS_MA_COA_ITEM model)
        //{
        //    long result = 0;

        //    try
        //    {
        //        QMS_MA_COA_ITEM dt = new QMS_MA_COA_ITEM();
        //        dt = QMS_MA_COA_ITEM.GetById(db, model.ID);

        //        dt.NAME = (null == model.NAME) ? "" : model.NAME;
 
        //        //dt.CREATE_DATE = DateTime.Now;
        //        //dt.CREATE_USER = _currentUserName;
        //        dt.UPDATE_DATE = DateTime.Now;
        //        dt.UPDATE_USER = _currentUserName;

        //        db.SaveChanges();
        //        result = model.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }
        //    return result;
        //}

        //public long AddQMS_MA_COA_ITEM(QMSDBEntities _db, QMS_MA_COA_ITEM model)
        //{
        //    long result = 0;
        //    try
        //    {
        //        QMS_MA_COA_ITEM dt = new QMS_MA_COA_ITEM();
        //        dt = QMS_MA_COA_ITEM.GetById(_db, model.ID);

        //        if (null == dt)
        //        {
        //            //model.bank_code = this.getBankCode(_db, model);
        //            _db.QMS_MA_COA_ITEM.AddObject(model);
        //            _db.SaveChanges();
        //            result = model.ID;
        //        }
        //        else
        //        {
        //            _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }
        //    return result;
        //}
        #endregion
        
        #region QMS_MA_COA_PRODUCT
        //public List<ViewQMS_MA_COA_PRODUCT> getQMS_MA_COA_PRODUCTList(QMSDBEntities db)
        //{
        //    List<ViewQMS_MA_COA_PRODUCT> listResult = new List<ViewQMS_MA_COA_PRODUCT>();

        //    try
        //    {
        //        List<QMS_MA_COA_PRODUCT> list = QMS_MA_COA_PRODUCT.GetAll(db);

        //        if (null != list && list.Count() > 0)
        //        {
        //            listResult = (from dt in list
        //                          select new ViewQMS_MA_COA_PRODUCT
        //                          {
        //                              ID = dt.ID,
        //                              NAME = dt.NAME,
        //                              DELETE_FLAG = dt.DELETE_FLAG
        //                          }).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.writeErrorLog(ex.Message);
        //    }

        //    return listResult;
        //}

        //private QMS_MA_COA_PRODUCT convertModelToDB(CreateQMS_MA_COA_PRODUCT model)
        //{
        //    QMS_MA_COA_PRODUCT result = new QMS_MA_COA_PRODUCT();

        //    result.ID = model.ID;
        //    result.NAME = (null == model.NAME) ? "" : model.NAME;

        //    result.CREATE_DATE = DateTime.Now;
        //    result.CREATE_USER = _currentUserName;
        //    result.UPDATE_DATE = DateTime.Now;
        //    result.UPDATE_USER = _currentUserName;

        //    return result;
        //}

        //public bool DeleteQMS_MA_COA_PRODUCTById(QMSDBEntities db, long id)
        //{
        //    bool bResult = false;
        //    QMS_MA_COA_PRODUCT result = new QMS_MA_COA_PRODUCT();
        //    result = QMS_MA_COA_PRODUCT.GetById(db, id);
        //    try
        //    {
        //        if (null != result)
        //        {
        //            result.DELETE_FLAG = 1;
        //            result.UPDATE_DATE = DateTime.Now;
        //            result.UPDATE_USER = _currentUserName;
        //            db.SaveChanges();
        //            bResult = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }

        //    return bResult;
        //}


        //public long SaveQMS_MA_COA_PRODUCT(QMSDBEntities db, CreateQMS_MA_COA_PRODUCT model)
        //{
        //    long result = 0;

        //    if (model.ID > 0)
        //    {
        //        result = UpdateQMS_MA_COA_PRODUCT(db, model);
        //    }
        //    else
        //    {
        //        result = AddQMS_MA_COA_PRODUCT(db, convertModelToDB(model));
        //    }

        //    return result;
        //}

        //public long UpdateQMS_MA_COA_PRODUCT(QMSDBEntities db, CreateQMS_MA_COA_PRODUCT model)
        //{
        //    long result = 0;

        //    try
        //    {
        //        QMS_MA_COA_PRODUCT dt = new QMS_MA_COA_PRODUCT();
        //        dt = QMS_MA_COA_PRODUCT.GetById(db, model.ID);

        //        dt.NAME = (null == model.NAME) ? "" : model.NAME;

        //        //dt.CREATE_DATE = DateTime.Now;
        //        //dt.CREATE_USER = _currentUserName;
        //        dt.UPDATE_DATE = DateTime.Now;
        //        dt.UPDATE_USER = _currentUserName;

        //        db.SaveChanges();
        //        result = model.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }
        //    return result;
        //}

        //public long AddQMS_MA_COA_PRODUCT(QMSDBEntities _db, QMS_MA_COA_PRODUCT model)
        //{
        //    long result = 0;
        //    //try
        //    //{
        //    //    QMS_MA_COA_PRODUCT dt = new QMS_MA_COA_PRODUCT();
        //    //    dt = QMS_MA_COA_PRODUCT.GetById(_db, model.ID);

        //    //    if (null == dt)
        //    //    {
        //    //        //model.bank_code = this.getBankCode(_db, model);
        //    //        _db.QMS_MA_COA_PRODUCT.AddObject(model);
        //    //        _db.SaveChanges();
        //    //        result = model.ID;
        //    //    }
        //    //    else
        //    //    {
        //    //        _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
        //    //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    _errMsg = this.getEexceptionError(ex);
        //    //}
        //    return result;
        //}
        #endregion

        #region QMS_MA_CONTROL

        public CreateQMS_MA_CONTROL getQMS_MA_CONTROLById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_CONTROL objResult = new CreateQMS_MA_CONTROL();

            try
            {
                QMS_MA_CONTROL dt = new QMS_MA_CONTROL();
                dt = QMS_MA_CONTROL.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_CONTROL> getQMS_MA_CONTROLList(QMSDBEntities db)
        {
            List<ViewQMS_MA_CONTROL> listResult = new List<ViewQMS_MA_CONTROL>();

            try
            {
                List<QMS_MA_CONTROL> list = QMS_MA_CONTROL.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CONTROL
                                  {
                                      ID = dt.ID,
                                      GROUP_TYPE = dt.GROUP_TYPE,
                                      NAME = dt.NAME,
                                      CONTROL_DESC = dt.CONTROL_DESC,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_CONTROL> getQMS_MA_CONTROLListByGroupType(QMSDBEntities db, byte GROUP_TYPE)
        {
            List<ViewQMS_MA_CONTROL> listResult = new List<ViewQMS_MA_CONTROL>();

            try
            {
                List<QMS_MA_CONTROL> list = QMS_MA_CONTROL.GetAllByGroupType(db, GROUP_TYPE);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CONTROL
                                  {
                                      ID = dt.ID,
                                      GROUP_TYPE = dt.GROUP_TYPE,
                                      NAME = dt.NAME,
                                      CONTROL_DESC = dt.CONTROL_DESC,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_CONTROL> searchQMS_MA_CONTROL(QMSDBEntities db, ControlValueSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_CONTROL> objResult = new List<ViewQMS_MA_CONTROL>();
            
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "NAME";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_CONTROL> listData = new List<QMS_MA_CONTROL>();
                listData = QMS_MA_CONTROL.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_CONTROL>(listData.AsQueryable(), out count, out totalPage);
                    objResult = (from dt in query
                                 select new ViewQMS_MA_CONTROL
                                 {
                                     ID = dt.ID,
                                     NAME = dt.NAME,
                                     CONTROL_DESC = dt.CONTROL_DESC,
                                     GROUP_TYPE = dt.GROUP_TYPE , 

                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER,
                                     SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                     SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),

                                 }).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private CreateQMS_MA_CONTROL convertDBToModel(QMSDBEntities db, QMS_MA_CONTROL model)
        {
            CreateQMS_MA_CONTROL result = new CreateQMS_MA_CONTROL();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.GROUP_TYPE = model.GROUP_TYPE;
            result.CONTROL_DESC = (null == model.CONTROL_DESC) ? "" : model.CONTROL_DESC;
            
            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));

            return result;
        }

        private QMS_MA_CONTROL convertModelToDB(CreateQMS_MA_CONTROL model)
        {
            QMS_MA_CONTROL result = new QMS_MA_CONTROL();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.GROUP_TYPE = model.GROUP_TYPE;
            result.CONTROL_DESC = (null == model.CONTROL_DESC) ? "" : model.CONTROL_DESC;
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_CONTROLById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_CONTROL result = new QMS_MA_CONTROL();
            result = QMS_MA_CONTROL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_CONTROLByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_CONTROL> result = new List<QMS_MA_CONTROL>();
            result = QMS_MA_CONTROL.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        } 

        public long SaveQMS_MA_CONTROL(QMSDBEntities db, CreateQMS_MA_CONTROL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_CONTROL(db, model);
            }
            else
            {
                result = AddQMS_MA_CONTROL(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_CONTROL(QMSDBEntities db, CreateQMS_MA_CONTROL model)
        {
            long result = 0;

            try
            {
                QMS_MA_CONTROL dt = new QMS_MA_CONTROL();
                dt = QMS_MA_CONTROL.GetById(db, model.ID);
                dt.GROUP_TYPE = model.GROUP_TYPE;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.CONTROL_DESC = (null == model.CONTROL_DESC) ? "" : model.CONTROL_DESC;
                //dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_CONTROL(QMSDBEntities _db, QMS_MA_CONTROL model)
        {
            long result = 0;
            try
            {
                QMS_MA_CONTROL dt = new QMS_MA_CONTROL();
                dt = QMS_MA_CONTROL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_CONTROL.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion
        
        #region  QMS_MA_CONTROL_COLUMN
        public List<ViewQMS_MA_CONTROL_COLUMN> setArrowUPAndDown(List<ViewQMS_MA_CONTROL_COLUMN> listResult)
        {

            if (listResult.Count() > 1)
            {
                long count = 1;
                long maxCount = listResult.Count();

                foreach (ViewQMS_MA_CONTROL_COLUMN dt in listResult)
                {
                    if (count == 1)
                    {
                        dt.ARROW_UP = false;
                    }
                    else if (count == maxCount)
                    {
                        dt.ARROW_DOWN = false;
                    }
                    count++;
                }
            }
            else
            {
                foreach (ViewQMS_MA_CONTROL_COLUMN dt in listResult)
                {
                    dt.ARROW_UP = false;
                    dt.ARROW_DOWN = false;
                }
            }

            return listResult;
        }
         
        public List<ViewQMS_MA_CONTROL_COLUMN> searchQMS_MA_CONTROL_COLUMN(QMSDBEntities db, ControlValueColumnSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
		{
             List<ViewQMS_MA_CONTROL_COLUMN> objResult = new List<ViewQMS_MA_CONTROL_COLUMN>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                List<QMS_MA_CONTROL_COLUMN> list = QMS_MA_CONTROL_COLUMN.GetAll(db);
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_CONTROL_COLUMN> listData = new List<QMS_MA_CONTROL_COLUMN>();
                List<QMS_MA_UNIT> listUnit = QMS_MA_UNIT.GetAll(db);
                listData = QMS_MA_CONTROL_COLUMN.GetAllBySearch(db, searchModel.mSearch);

                if (listData.Count() > 0)
                { 
                    objResult = (from data in listData 
                                 select new ViewQMS_MA_CONTROL_COLUMN
                                 {
                                     ID = data.ID,
                                     CONTROL_ID = data.CONTROL_ID,
                                     ITEM = data.ITEM,
                                     UNIT_ID = data.UNIT_ID,
                                     UNIT_NAME = getUnitName(listUnit, data.UNIT_ID),
                                     ARROW_DOWN = true,
                                     ARROW_UP = true,
                                     POSITION = data.POSITION
                                 }).ToList();

                    objResult = objResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                    objResult = setArrowUPAndDown(objResult);
                    if (objResult != null)
                    {
                        objResult = grid.LoadGridData<ViewQMS_MA_CONTROL_COLUMN>(objResult.AsQueryable(), out count, out totalPage).ToList();
                        listPageIndex = getPageIndexList(totalPage);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_CONTROL_COLUMN> getQMS_MA_CONTROL_COLUMNList(QMSDBEntities db)
        {
            List<ViewQMS_MA_CONTROL_COLUMN> listResult = new List<ViewQMS_MA_CONTROL_COLUMN>();

            try
            {
                List<QMS_MA_CONTROL_COLUMN> list = QMS_MA_CONTROL_COLUMN.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list 
                                  select new ViewQMS_MA_CONTROL_COLUMN
                                  {
                                      ID = dt.ID,
                                      CONTROL_ID = dt.CONTROL_ID,
                                      ITEM = dt.ITEM,
                                      //DELETE_FLAG = dt.DELETE_FLAG 
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

       public List<ViewQMS_MA_CONTROL_COLUMN> getQMS_MA_CONTROL_COLUMNListByControlId(QMSDBEntities db, long id)
		{
            QMS_MA_CONTROL_COLUMN result = new QMS_MA_CONTROL_COLUMN();
			List<ViewQMS_MA_CONTROL_COLUMN> listResult = new List<ViewQMS_MA_CONTROL_COLUMN>();
            try
            {
                List<QMS_MA_CONTROL_COLUMN> list = QMS_MA_CONTROL_COLUMN.GetAllByControlId(db, id);
                List<QMS_MA_UNIT> listUnit = QMS_MA_UNIT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CONTROL_COLUMN
                                  {
                                      ID = dt.ID,
                                      CONTROL_ID = dt.CONTROL_ID,
                                      ITEM = dt.ITEM,
                                      UNIT_ID = dt.UNIT_ID ,
                                      POSITION = dt.POSITION,
                                      UNIT_NAME = getUnitName(listUnit, dt.UNIT_ID)
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_CONTROL_COLUMN convertModelToDB(CreateQMS_MA_CONTROL_COLUMN model)
        {
            QMS_MA_CONTROL_COLUMN result = new QMS_MA_CONTROL_COLUMN();

            result.ID = model.ID;
            result.CONTROL_ID = model.CONTROL_ID;
            result.ITEM = (null == model.ITEM) ? "" : model.ITEM;
            result.UNIT_ID = model.UNIT_ID;
            result.POSITION = model.POSITION;
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_CONTROL_COLUMNByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false; 
            List<QMS_MA_CONTROL_COLUMN> result = new List<QMS_MA_CONTROL_COLUMN>();
            result = QMS_MA_CONTROL_COLUMN.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        } 

        public bool DeleteQMS_MA_CONTROL_COLUMNById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_CONTROL_COLUMN result = new QMS_MA_CONTROL_COLUMN();
            result = QMS_MA_CONTROL_COLUMN.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_CONTROL_COLUMN(QMSDBEntities db, CreateQMS_MA_CONTROL_COLUMN model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_CONTROL_COLUMN(db, model);
            }
            else
            {
                result = AddQMS_MA_CONTROL_COLUMN(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_CONTROL_COLUMN(QMSDBEntities db, CreateQMS_MA_CONTROL_COLUMN model)
        {
            long result = 0;

            try
            { 
                QMS_MA_CONTROL_COLUMN dt = new QMS_MA_CONTROL_COLUMN();
                dt = QMS_MA_CONTROL_COLUMN.GetById(db, model.ID);

                dt.CONTROL_ID = model.CONTROL_ID;
                dt.ITEM = (null == model.ITEM) ? "" : model.ITEM;
                dt.UNIT_ID = model.UNIT_ID;
                dt.POSITION = model.POSITION;
               // dt.DELETE_FLAG = model.DELETE_FLAG;
               
                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        private short getMaxControlColumnPostion(QMSDBEntities db, long id)
        {
            short bResult = 1;

            try
            {
                var list = QMS_MA_CONTROL_COLUMN.GetAllByControlId(db, id);  //QMS_MA_PLANT.GetAll(db);

                if (list.Count() > 0)
                {
                    short max = list.Max(m => m.POSITION);

                    if (max < 32767)
                        bResult += max;
                    else
                        bResult = 1;
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }


            return bResult;
        }

        public long AddQMS_MA_CONTROL_COLUMN(QMSDBEntities _db, QMS_MA_CONTROL_COLUMN model)
        {
            long result = 0;
            try
            {
               
                QMS_MA_CONTROL_COLUMN dt = new QMS_MA_CONTROL_COLUMN();
                dt = QMS_MA_CONTROL_COLUMN.GetById(_db, model.ID);

                if (null == dt)
                {
                    model.POSITION = this.getMaxControlColumnPostion(_db, model.CONTROL_ID); //model.POSITION; //หาค่าสูงสุดสร้างใหม่จะไปต่อท้าย
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_CONTROL_COLUMN.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg =  QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_CONTROL_COLUMN> setQMS_MA_CONTROL_COLUMNTPostion(QMSDBEntities db, SetItemPosition model,  out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_CONTROL_COLUMN> listResult = new List<ViewQMS_MA_CONTROL_COLUMN>();

            try
            {
                List<QMS_MA_CONTROL_COLUMN> list = QMS_MA_CONTROL_COLUMN.GetAllByControlId(db, model.CONTROL_ID);

                QMS_MA_CONTROL_COLUMN LastId = new QMS_MA_CONTROL_COLUMN();
                QMS_MA_CONTROL_COLUMN CurrentId = new QMS_MA_CONTROL_COLUMN();
                QMS_MA_CONTROL_COLUMN NextId = new QMS_MA_CONTROL_COLUMN();

                CreateQMS_MA_CONTROL_COLUMN currentData = new CreateQMS_MA_CONTROL_COLUMN();
                CreateQMS_MA_CONTROL_COLUMN swapData = new CreateQMS_MA_CONTROL_COLUMN();

                bool ExitLoop = false;

                list = list.OrderBy(m => m.POSITION).ToList();

                foreach (QMS_MA_CONTROL_COLUMN dt in list)
                {
                    NextId = dt; // get ค่า ตัวต่อไป

                    if (true == ExitLoop)
                    {
                        break;
                    }

                    if (model.ID == dt.ID) // get ค่า ปัจจุบัน
                    {
                        CurrentId = dt;
                        ExitLoop = true;
                    }
                    else
                    {
                        LastId = dt; // get ค่า สุดท้าย
                    }
                }

                if (model.DIRECTION == (int)ARROW_STATUS.ARROW_UP)
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.ITEM = CurrentId.ITEM;
                    currentData.CONTROL_ID = CurrentId.CONTROL_ID;
                    currentData.UNIT_ID = CurrentId.UNIT_ID;
                    currentData.POSITION = LastId.POSITION; //สลับตำแหน่งกับค่า บน


                    swapData.ID = LastId.ID;
                    swapData.ITEM = LastId.ITEM;
                    swapData.CONTROL_ID = LastId.CONTROL_ID;
                    swapData.UNIT_ID = LastId.UNIT_ID;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                }
                else
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.ITEM = CurrentId.ITEM;
                    currentData.CONTROL_ID = CurrentId.CONTROL_ID;
                    currentData.UNIT_ID = CurrentId.UNIT_ID;
                    currentData.POSITION = NextId.POSITION; //สลับตำแหน่งกับค่า ล่าง 

                    swapData.ID = NextId.ID;
                    swapData.ITEM = NextId.ITEM;
                    swapData.CONTROL_ID = NextId.CONTROL_ID;
                    swapData.UNIT_ID = NextId.UNIT_ID;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 
                }

                UpdateQMS_MA_CONTROL_COLUMN(db, currentData);
                UpdateQMS_MA_CONTROL_COLUMN(db, swapData);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            ControlValueColumnSearch searchModel = new ControlValueColumnSearch();
            searchModel.mSearch = new ControlValueColumnSearchModel();
            searchModel.PageIndex = 1;
            searchModel.PageSize = -1;
            searchModel.SortColumn = "POSITION";
            searchModel.SortOrder = "ASC";
            searchModel.mSearch.CONTROL_ID = model.CONTROL_ID;

            return searchQMS_MA_CONTROL_COLUMN(db, searchModel, out count, out totalPage, out listPageIndex);
        }

        #endregion

        #region QMS_MA_CONTROL_DATA

        public List<List<ViewQMS_MA_CONTROL_DATA>> getTemplateControlValue(QMSDBEntities db, long control_id)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();
            ViewQMS_MA_CONTROL_DATA tempData = new ViewQMS_MA_CONTROL_DATA();
            List<List<ViewQMS_MA_CONTROL_DATA>> matrixResult = new List<List<ViewQMS_MA_CONTROL_DATA>>();

            try
            {
                List<ViewQMS_MA_CONTROL_ROW> listRow = getQMS_MA_CONTROL_ROWListByControlId(db, control_id);
                List<ViewQMS_MA_CONTROL_COLUMN> listColumn = getQMS_MA_CONTROL_COLUMNListByControlId(db, control_id);
                List<ViewQMS_MA_CONTROL_DATA> listData = getQMS_MA_CONTROL_DATAListByControlId(db, control_id);

                List<QMS_MA_UNIT> listUnitName = new List<QMS_MA_UNIT>();

                if (listRow.Count() > 0 && listColumn.Count() > 0)
                {
                    listResult = new List<ViewQMS_MA_CONTROL_DATA>();

                    //Header column .... สร้าง header column
                    tempData = new ViewQMS_MA_CONTROL_DATA();

                    tempData.CONTROL_ROW_ID = 0;
                    tempData.CONTROL_ROW_NAME = QMSSystem.CoreDB.Resource.ResourceString.SampleItem;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    listResult.Add(tempData);

                    foreach (ViewQMS_MA_CONTROL_COLUMN dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_MA_CONTROL_DATA();

                        tempData.CONTROL_ROW_ID = 0;
                        tempData.CONTROL_ROW_NAME = "";
                        tempData.CONTROL_ID = dtColumn.CONTROL_ID;
                        tempData.CONTROL_COLUMN_ID = dtColumn.ID;
                        tempData.CONTROL_COLUMN_NAME = dtColumn.ITEM + " (" + dtColumn.UNIT_NAME + ")"; 
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;

                        QMS_MA_UNIT unitname = new QMS_MA_UNIT();
                        unitname.ID = dtColumn.ID; //ใส่ column id ไว้ 
                        unitname.NAME = dtColumn.UNIT_NAME;
                        listUnitName.Add(unitname); //เพิ่มรายชื่อ unit name ของตัว control
                        listResult.Add(tempData);
                    }
                    matrixResult.Add(listResult);

                    //Data Row ........ สร้าง row
                    foreach( ViewQMS_MA_CONTROL_ROW dtRow in listRow){

                        // theader row
                        listResult = new List<ViewQMS_MA_CONTROL_DATA>();
                        tempData = new ViewQMS_MA_CONTROL_DATA();
                        tempData.CONTROL_ROW_ID = dtRow.ID;
                        tempData.CONTROL_ROW_NAME = dtRow.SAMPLE;
                        tempData.CONTROL_COLUMN_ID = 0;
                        tempData.CONTROL_COLUMN_NAME = "";
                        tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        listResult.Add(tempData);

                        foreach (ViewQMS_MA_CONTROL_COLUMN dtColumn in listColumn)
                        {
                            tempData = new ViewQMS_MA_CONTROL_DATA();

                            tempData.CONTROL_ROW_ID = dtRow.ID;
                            tempData.CONTROL_ID = dtRow.CONTROL_ID;
                            tempData.CONTROL_COLUMN_ID = dtColumn.ID;

                            if (listData.Count() > 0) //ตรวจสอบว่ามีข้อมูลในระบบหรือเปล่า
                            {
                                ViewQMS_MA_CONTROL_DATA dataItem = listData.Where(m => m.CONTROL_ROW_ID == dtRow.ID && m.CONTROL_COLUMN_ID == dtColumn.ID).FirstOrDefault();
                                 
                                if (null != dataItem)
                                {
                                    tempData = dataItem;

                                    if (tempData.MIN_FLAG == false && tempData.MAX_FLAG == false) //เป็นงาน conc/loadding
                                    {
                                        tempData.CONC_LOADING_FLAG = true;
                                    } 

                                    tempData.CONTROL_DATA_NAME = getControlValueText(dataItem);
                                    tempData.UNIT_NAME = getUnitName(listUnitName, tempData.CONTROL_COLUMN_ID);
                                }
                            }
                           
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                            listResult.Add(tempData);
                        }

                        matrixResult.Add(listResult);
                    }
                }
                
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public List<ViewQMS_MA_CONTROL_DATA> searchQMS_MA_CONTROL_DATA(QMSDBEntities db, ControlValueDataSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_CONTROL_DATA> objResult = new List<ViewQMS_MA_CONTROL_DATA>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_CONTROL_DATA> listData = new List<QMS_MA_CONTROL_DATA>();
                listData = QMS_MA_CONTROL_DATA.GetAllBySearch(db, searchModel.mSearch);
                 
                if (listData.Count() > 0)
                {
                    objResult = convertListData(db, listData);

                    if (objResult != null)
                    {
                        objResult = grid.LoadGridData<ViewQMS_MA_CONTROL_DATA>(objResult.AsQueryable(), out count, out totalPage).ToList();
                        listPageIndex = getPageIndexList(totalPage);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_CONTROL_DATA> getQMS_MA_CONTROL_DATAListByControlSampleId(QMSDBEntities db, long controlId, long sampleId)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();

            try
            {
                List<QMS_MA_CONTROL_DATA> list = QMS_MA_CONTROL_DATA.GetAllistByControlSampleId(db, controlId, sampleId);
                List<QMS_MA_CONTROL_COLUMN> listColumn = QMS_MA_CONTROL_COLUMN.GetAllByControlId(db, controlId);
                List<QMS_MA_UNIT> listUnit = QMS_MA_UNIT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);

                    foreach (ViewQMS_MA_CONTROL_DATA dt in listResult) //เปลี่ยนการแสดงผล เป็นสัญญาลักษณ์
                    {
                        dt.CONTROL_DATA_NAME = getControlValueText(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private List<ViewQMS_MA_CONTROL_DATA> convertListData(QMSDBEntities db,  List<QMS_MA_CONTROL_DATA> list)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();
            List<QMS_MA_CONTROL_COLUMN> listColumn = QMS_MA_CONTROL_COLUMN.GetAll(db);
            List<QMS_MA_UNIT> listUnit = QMS_MA_UNIT.GetAll(db);
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CONTROL_DATA
                                  {
                                       ID = dt.ID,
                                      //SHOW_ON_TREND_FLAG = (dt.SHOW_ON_TREND_FLAG == 1) ? true : false,
                                      CONTROL_ROW_ID = dt.CONTROL_ROW_ID,
                                      CONTROL_COLUMN_ID = dt.CONTROL_COLUMN_ID,
                                      CONTROL_COLUMN_NAME =  getControlColumnName(listColumn, dt.CONTROL_COLUMN_ID),
                                      CONTROL_ID = dt.CONTROL_ID,
                                       MAX_NO_CAL = (dt.MAX_NO_CAL == 1) ? true : false,
                                      MAX_FLAG = (dt.MAX_FLAG == 1) ? true : false,
                                      MAX_VALUE = dt.MAX_VALUE,
                                       MIN_NO_CAL = (dt.MIN_NO_CAL == 1) ? true : false,
                                      MIN_FLAG = (dt.MIN_FLAG == 1) ? true : false,
                                      MIN_VALUE = dt.MIN_VALUE,
                                       CONC_NO_CAL = (dt.CONC_NO_CAL == 1) ? true : false,
                                      CONC_FLAG = (dt.CONC_FLAG == 1) ? true : false,
                                      CONC_VALUE = dt.CONC_VALUE,
                                       LOADING_NO_CAL = (dt.LOADING_NO_CAL == 1) ? true : false,
                                      LOADING_FLAG = (dt.LOADING_FLAG == 1) ? true : false,
                                      LOADING_VALUE = dt.LOADING_VALUE,
                                      UNIT_NAME = getUnitName(listColumn, listUnit, dt.CONTROL_COLUMN_ID),

                                      

                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss"),
                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss")
                                      // DELETE_FLAG = dt.DELETE_FLAG
                                       
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<ViewQMS_MA_CONTROL_DATA> getQMS_MA_CONTROL_DATAList(QMSDBEntities db)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();

            try
            {
                List<QMS_MA_CONTROL_DATA> list = QMS_MA_CONTROL_DATA.GetAll(db);
                listResult = convertListData(db, list);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_CONTROL_DATA> getQMS_MA_CONTROL_DATAListByListIds(QMSDBEntities db, long[] ids)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();

            try
            {
                List<QMS_MA_CONTROL_DATA> list = QMS_MA_CONTROL_DATA.GetByListId(db, ids);
                listResult = convertListData(db, list);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_CONTROL_DATA> getQMS_MA_CONTROL_DATAByListControlId(QMSDBEntities db, long[] ids)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();

            try
            {
                List<QMS_MA_CONTROL_DATA> list = QMS_MA_CONTROL_DATA.GetByListControlId(db, ids);

                listResult = convertListData(db, list);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public ViewQMS_MA_CONTROL_DATA getQMS_MA_CONTROL_DATAListById(QMSDBEntities db, long id)
        {
            ViewQMS_MA_CONTROL_DATA dtResult = new ViewQMS_MA_CONTROL_DATA();

            try
            {
                QMS_MA_CONTROL_DATA dt = QMS_MA_CONTROL_DATA.GetById(db, id);

                dtResult.ID = dt.ID;
                dtResult.CONTROL_ROW_ID = dt.CONTROL_ROW_ID;
                dtResult.CONTROL_COLUMN_ID = dt.CONTROL_COLUMN_ID;
                dtResult.CONTROL_ID = dt.CONTROL_ID;
                dtResult.MAX_FLAG = (dt.MAX_FLAG == 1) ? true : false;
                dtResult.MAX_VALUE = dt.MAX_VALUE;
                dtResult.MIN_FLAG = (dt.MIN_FLAG == 1) ? true : false;
                dtResult.MIN_VALUE = dt.MIN_VALUE;
                dtResult.CONC_FLAG = (dt.CONC_FLAG == 1) ? true : false;
                dtResult.CONC_VALUE = dt.CONC_VALUE;
                dtResult.LOADING_FLAG = (dt.LOADING_FLAG == 1) ? true : false;
                dtResult.LOADING_VALUE = dt.LOADING_VALUE;
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return dtResult;
        }

        public List<ViewQMS_MA_CONTROL_DATA> getQMS_MA_CONTROL_DATAListByControlId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();

            try
            {
                List<QMS_MA_CONTROL_DATA> list = QMS_MA_CONTROL_DATA.GetAllByControlId(db, id);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_CONTROL_DATA convertModelToDB(CreateQMS_MA_CONTROL_DATA model)
        {
            QMS_MA_CONTROL_DATA result = new QMS_MA_CONTROL_DATA();

            result.ID = model.ID;
            
            //result.SHOW_ON_TREND_FLAG = (model.SHOW_ON_TREND_FLAG == true) ? (byte)1: (byte)0;
            result.CONTROL_ROW_ID = model.CONTROL_ROW_ID;
            result.CONTROL_COLUMN_ID = model.CONTROL_COLUMN_ID;
            result.CONTROL_ID = model.CONTROL_ID;
            result.MAX_FLAG = (model.MAX_FLAG == true) ? (byte)1 : (byte)0; 
            result.MAX_VALUE = model.MAX_VALUE;
            result.MIN_FLAG = (model.MIN_FLAG == true) ? (byte)1 : (byte)0; 
            result.MIN_VALUE = model.MIN_VALUE;
            result.CONC_FLAG = (model.CONC_FLAG == true) ? (byte)1 : (byte)0; 
            result.CONC_VALUE = model.CONC_VALUE;
            result.LOADING_FLAG = (model.LOADING_FLAG == true) ? (byte)1 : (byte)0; 
            result.LOADING_VALUE = model.LOADING_VALUE; 
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.MAX_NO_CAL = (model.MAX_NO_CAL == true) ? (byte)1 : (byte)0;
            result.MIN_NO_CAL = (model.MIN_NO_CAL == true) ? (byte)1 : (byte)0;
            result.CONC_NO_CAL = (model.CONC_NO_CAL == true) ? (byte)1 : (byte)0;
            result.LOADING_NO_CAL = (model.LOADING_NO_CAL == true) ? (byte)1 : (byte)0;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_CONTROL_DATAByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_CONTROL_DATA> result = new List<QMS_MA_CONTROL_DATA>();
            result = QMS_MA_CONTROL_DATA.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        } 

        public bool DeleteQMS_MA_CONTROL_DATAById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_CONTROL_DATA result = new QMS_MA_CONTROL_DATA();
            result = QMS_MA_CONTROL_DATA.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }


        public long SaveQMS_MA_CONTROL_DATA(QMSDBEntities db, CreateQMS_MA_CONTROL_DATA model)
        {
            long result = 0;

            if (true == model.CONC_FLAG || true == model.LOADING_FLAG)
            {
                model.MIN_FLAG = false;
                model.MAX_FLAG = false;
            }

            if (false == model.MIN_FLAG) model.MIN_VALUE = 0;
            if (false == model.MAX_FLAG) model.MAX_VALUE = 0;
            if (false == model.CONC_FLAG) model.CONC_VALUE = 0;
            if (false == model.LOADING_FLAG) model.LOADING_VALUE = 0;
 
            if (model.ID > 0)
            {
                result = UpdateQMS_MA_CONTROL_DATA(db, model);
            }
            else
            {
                result = AddQMS_MA_CONTROL_DATA(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_CONTROL_DATA(QMSDBEntities db, CreateQMS_MA_CONTROL_DATA model)
        {
            long result = 0;

            try
            {
                QMS_MA_CONTROL_DATA dt = new QMS_MA_CONTROL_DATA();
                dt = QMS_MA_CONTROL_DATA.GetById(db, model.ID);

                //dt.SHOW_ON_TREND_FLAG = (model.SHOW_ON_TREND_FLAG == true) ? (byte)1 : (byte)0;
                dt.CONTROL_ROW_ID = model.CONTROL_ROW_ID;
                dt.CONTROL_COLUMN_ID = model.CONTROL_COLUMN_ID;
                dt.CONTROL_ID = model.CONTROL_ID;
                dt.MAX_FLAG = (model.MAX_FLAG == true) ? (byte)1 : (byte)0;
                dt.MAX_VALUE = model.MAX_VALUE;
                dt.MIN_FLAG = (model.MIN_FLAG == true) ? (byte)1 : (byte)0;
                dt.MIN_VALUE = model.MIN_VALUE;
                dt.CONC_FLAG = (model.CONC_FLAG == true) ? (byte)1 : (byte)0;
                dt.CONC_VALUE = model.CONC_VALUE;
                dt.LOADING_FLAG = (model.LOADING_FLAG == true) ? (byte)1 : (byte)0;
                dt.LOADING_VALUE = model.LOADING_VALUE;
                //dt.DELETE_FLAG = model.DELETE_FLAG;


                dt.MAX_NO_CAL = (model.MAX_NO_CAL == true) ? (byte)1 : (byte)0;
                dt.MIN_NO_CAL = (model.MIN_NO_CAL == true) ? (byte)1 : (byte)0;
                dt.CONC_NO_CAL = (model.CONC_NO_CAL == true) ? (byte)1 : (byte)0;
                dt.LOADING_NO_CAL = (model.LOADING_NO_CAL == true) ? (byte)1 : (byte)0;
  
                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_CONTROL_DATA(QMSDBEntities _db, QMS_MA_CONTROL_DATA model)
        {
            long result = 0;
            try
            {
                QMS_MA_CONTROL_DATA dt = new QMS_MA_CONTROL_DATA();
                dt = QMS_MA_CONTROL_DATA.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_CONTROL_DATA.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_CONTROL_ROW

        public List<ViewQMS_MA_CONTROL_ROW> setArrowUPAndDown(List<ViewQMS_MA_CONTROL_ROW> listResult)
        {

            if (listResult.Count() > 1)
            {
                long count = 1;
                long maxCount = listResult.Count();

                foreach (ViewQMS_MA_CONTROL_ROW dt in listResult)
                {
                    if (count == 1)
                    {
                        dt.ARROW_UP = false;
                    }
                    else if (count == maxCount)
                    {
                        dt.ARROW_DOWN = false;
                    }
                    count++;
                }
            }
            else
            {
                foreach (ViewQMS_MA_CONTROL_ROW dt in listResult)
                {
                    dt.ARROW_UP = false;
                    dt.ARROW_DOWN = false;
                }
            }

            return listResult;
        }

        public List<ViewQMS_MA_CONTROL_ROW> searchQMS_MA_CONTROL_ROW(QMSDBEntities db, ControlValueRowSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_CONTROL_ROW> objResult = new List<ViewQMS_MA_CONTROL_ROW>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_CONTROL_ROW> listData = new List<QMS_MA_CONTROL_ROW>();
                listData = QMS_MA_CONTROL_ROW.GetAllBySearch(db, searchModel.mSearch);

                if (listData.Count() > 0)
                {

                    objResult = (from data in listData 
                                 select new ViewQMS_MA_CONTROL_ROW
                                 {
                                     ID = data.ID,
                                     CONTROL_ID = data.CONTROL_ID,
                                     GRADE_ID = data.GRADE_ID,
                                     PLANT_ID = data.PLANT_ID,
                                     SAMPLE = data.SAMPLE,
                                     ARROW_DOWN = true,
                                     ARROW_UP = true,
                                     POSITION = data.POSITION
                                 }).ToList();

                    objResult = objResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                    objResult = setArrowUPAndDown(objResult);

                    if (objResult != null)
                    {
                        objResult = grid.LoadGridData<ViewQMS_MA_CONTROL_ROW>(objResult.AsQueryable(), out count, out totalPage).ToList();
                        listPageIndex = getPageIndexList(totalPage);
                    }
                }
                 
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_CONTROL_ROW> getQMS_MA_CONTROL_ROWListByControlId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_CONTROL_ROW> listResult = new List<ViewQMS_MA_CONTROL_ROW>();

            try
            {
                List<QMS_MA_CONTROL_ROW> list = QMS_MA_CONTROL_ROW.GetAllByControlId(db, id);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CONTROL_ROW
                                  {
                                      ID = dt.ID,
                                      CONTROL_ID = dt.CONTROL_ID,
                                      SAMPLE = dt.SAMPLE,
                                      PLANT_ID = dt.PLANT_ID,
                                      GRADE_ID = dt.GRADE_ID,
                                      POSITION = dt.POSITION,
                                      // DELETE_FLAG = dt.DELETE_FLAG                                      
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_CONTROL_ROW> getQMS_MA_CONTROL_ROWList(QMSDBEntities db)
        {
            List<ViewQMS_MA_CONTROL_ROW> listResult = new List<ViewQMS_MA_CONTROL_ROW>();

            try
            {
                List<QMS_MA_CONTROL_ROW> list = QMS_MA_CONTROL_ROW.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CONTROL_ROW
                                  {
                                      ID = dt.ID,
                                      CONTROL_ID = dt.CONTROL_ID,
                                      SAMPLE = dt.SAMPLE,
                                      PLANT_ID = dt.PLANT_ID,
                                      GRADE_ID = dt.GRADE_ID,
                                     // DELETE_FLAG = dt.DELETE_FLAG                                      
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_CONTROL_ROW convertModelToDB(CreateQMS_MA_CONTROL_ROW model)
        {
            QMS_MA_CONTROL_ROW result = new QMS_MA_CONTROL_ROW();

            result.ID = model.ID;
            result.CONTROL_ID = model.CONTROL_ID;
            result.SAMPLE = model.SAMPLE;
            result.PLANT_ID = model.PLANT_ID;
            result.GRADE_ID = model.GRADE_ID;
            result.POSITION = model.POSITION;
            //result.DELETE_FLAG = model.DELETE_FLAG; 

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_CONTROL_ROWByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_CONTROL_ROW> result = new List<QMS_MA_CONTROL_ROW>();
            result = QMS_MA_CONTROL_ROW.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        } 

        public bool DeleteQMS_MA_CONTROL_ROWById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_CONTROL_ROW result = new QMS_MA_CONTROL_ROW();
            result = QMS_MA_CONTROL_ROW.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }


        public long SaveQMS_MA_CONTROL_ROW(QMSDBEntities db, CreateQMS_MA_CONTROL_ROW model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_CONTROL_ROW(db, model);
            }
            else
            {
                result = AddQMS_MA_CONTROL_ROW(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_CONTROL_ROW(QMSDBEntities db, CreateQMS_MA_CONTROL_ROW model)
        {
            long result = 0;

            try
            {
                QMS_MA_CONTROL_ROW dt = new QMS_MA_CONTROL_ROW();
                dt = QMS_MA_CONTROL_ROW.GetById(db, model.ID);

                dt.CONTROL_ID = model.CONTROL_ID;
                dt.SAMPLE = model.SAMPLE;
                dt.PLANT_ID = model.PLANT_ID;
                dt.GRADE_ID = model.GRADE_ID;
                dt.POSITION = model.POSITION;
              //  dt.DELETE_FLAG = model.DELETE_FLAG;
                
                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        private short getMaxControlRowPostion(QMSDBEntities db, long id)
        {
            short bResult = 1;

            try
            {
                var list = QMS_MA_CONTROL_ROW.GetAllByControlId(db, id);  //QMS_MA_PLANT.GetAll(db);

                if (list.Count() > 0)
                {
                    short max = list.Max(m => m.POSITION);

                    if (max < 32767)
                        bResult += max;
                    else
                        bResult = 1;
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }


            return bResult;
        }

        public long AddQMS_MA_CONTROL_ROW(QMSDBEntities _db, QMS_MA_CONTROL_ROW model)
        {
            long result = 0;
            try
            {
                QMS_MA_CONTROL_ROW dt = new QMS_MA_CONTROL_ROW();
                dt = QMS_MA_CONTROL_ROW.GetById(_db, model.ID);

                if (null == dt)
                {
                    model.POSITION = this.getMaxControlRowPostion(_db, model.CONTROL_ID); //model.POSITION; //หาค่าสูงสุดสร้างใหม่จะไปต่อท้าย
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_CONTROL_ROW.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_CONTROL_ROW> setQMS_MA_CONTROL_ROWPostion(QMSDBEntities db, SetSamplePosition model, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_CONTROL_ROW> listResult = new List<ViewQMS_MA_CONTROL_ROW>();

            try
            {
                List<QMS_MA_CONTROL_ROW> list = QMS_MA_CONTROL_ROW.GetAllByControlId(db, model.CONTROL_ID);

                QMS_MA_CONTROL_ROW LastId = new QMS_MA_CONTROL_ROW();
                QMS_MA_CONTROL_ROW CurrentId = new QMS_MA_CONTROL_ROW();
                QMS_MA_CONTROL_ROW NextId = new QMS_MA_CONTROL_ROW();

                CreateQMS_MA_CONTROL_ROW currentData = new CreateQMS_MA_CONTROL_ROW();
                CreateQMS_MA_CONTROL_ROW swapData = new CreateQMS_MA_CONTROL_ROW();

                bool ExitLoop = false;

                list = list.OrderBy(m => m.POSITION).ToList();

                foreach (QMS_MA_CONTROL_ROW dt in list)
                {
                    NextId = dt; // get ค่า ตัวต่อไป

                    if (true == ExitLoop)
                    {
                        break;
                    }

                    if (model.ID == dt.ID) // get ค่า ปัจจุบัน
                    {
                        CurrentId = dt;
                        ExitLoop = true;
                    }
                    else
                    {
                        LastId = dt; // get ค่า สุดท้าย
                    }
                }

                if (model.DIRECTION == (int)ARROW_STATUS.ARROW_UP)
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.PLANT_ID = CurrentId.PLANT_ID;
                    currentData.CONTROL_ID = CurrentId.CONTROL_ID;
                    currentData.SAMPLE = CurrentId.SAMPLE;
                    currentData.POSITION = LastId.POSITION; //สลับตำแหน่งกับค่า บน

                    swapData.ID = LastId.ID;
                    swapData.PLANT_ID = LastId.PLANT_ID;
                    swapData.CONTROL_ID = LastId.CONTROL_ID;
                    swapData.SAMPLE = LastId.SAMPLE;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                }
                else
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.PLANT_ID = CurrentId.PLANT_ID;
                    currentData.CONTROL_ID = CurrentId.CONTROL_ID;
                    currentData.SAMPLE = CurrentId.SAMPLE;
                    currentData.POSITION = NextId.POSITION; //สลับตำแหน่งกับค่า ล่าง 

                    swapData.ID = NextId.ID;
                    swapData.PLANT_ID = NextId.PLANT_ID;
                    swapData.CONTROL_ID = NextId.CONTROL_ID;
                    swapData.SAMPLE = NextId.SAMPLE;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 
                }

                UpdateQMS_MA_CONTROL_ROW(db, currentData);
                UpdateQMS_MA_CONTROL_ROW(db, swapData);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            ControlValueRowSearch searchModel = new ControlValueRowSearch();
            searchModel.mSearch = new ControlValueRowSearchModel();
            searchModel.PageIndex = 1;
            searchModel.PageSize = -1;
            searchModel.SortColumn = "POSITION";
            searchModel.SortOrder = "ASC";
            searchModel.mSearch.CONTROL_ID = model.CONTROL_ID;

            return searchQMS_MA_CONTROL_ROW(db, searchModel, out count, out totalPage, out listPageIndex);
        }
        #endregion

        #region QMS_MA_CORRECT_DATA

        private List<ViewQMS_MA_CORRECT_DATA> convertListData(QMSDBEntities db, List<QMS_MA_CORRECT_DATA> list)
        {
            List<ViewQMS_MA_CORRECT_DATA> listResult = new List<ViewQMS_MA_CORRECT_DATA>(); 
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CORRECT_DATA
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<ViewQMS_MA_CORRECT_DATA> getQMS_MA_CORRECT_DATAActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_CORRECT_DATA> listResult = new List<ViewQMS_MA_CORRECT_DATA>();

            try
            {
                List<QMS_MA_CORRECT_DATA> list = QMS_MA_CORRECT_DATA.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_CORRECT_DATA> getQMS_MA_CORRECT_DATAAllExceptionWithoutMaster(QMSDBEntities db)
        {
            List<ViewQMS_MA_CORRECT_DATA> listResult = new List<ViewQMS_MA_CORRECT_DATA>();

            try
            {
                List<QMS_MA_CORRECT_DATA> list = QMS_MA_CORRECT_DATA.GetAllExceptionWithoutMaster(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_CORRECT_DATA> searchQMS_MA_CORRECT_DATA(QMSDBEntities db, CorrectDataSearch searchModel, out long count, out long totalPage, out List<PageIndexList>  listPageIndex)
        {
            List<ViewQMS_MA_CORRECT_DATA> objResult = new List<ViewQMS_MA_CORRECT_DATA>();
            
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PRIORITY";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_CORRECT_DATA> listData = new List<QMS_MA_CORRECT_DATA>();
                listData = QMS_MA_CORRECT_DATA.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_CORRECT_DATA>(listData.AsQueryable(), out count, out totalPage);
                    objResult = (from dt in query
                                 select new ViewQMS_MA_CORRECT_DATA
                                 {
                                     ID = dt.ID,
                                     NAME = dt.NAME,

                                     CROSS_VOLUME_FLAG = dt.CROSS_VOLUME_FLAG,
                                     DELETE_INPUT_QTY = dt.DELETE_INPUT_QTY,
                                     DELETE_OUTPUT_QTY = dt.DELETE_OUTPUT_QTY,
                                     CAL_OFF_CONTROL = dt.CAL_OFF_CONTROL,
                                     PRIORITY = dt.PRIORITY,

                                     DELETE_OUTPUT_QTY_NAME = reovleMultiplicand(dt.DELETE_OUTPUT_QTY),
                                     DELETE_INPUT_QTY_NAME = reovleDenominator(dt.DELETE_INPUT_QTY),
                                     CAL_OFF_CONTROL_NAME = reovleOffControlMethod(dt.CAL_OFF_CONTROL),

                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER,
                                     SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                     SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),

                                 }).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_CORRECT_DATA getQMS_MA_CORRECT_DATAById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_CORRECT_DATA objResult = new CreateQMS_MA_CORRECT_DATA();

            try
            {
                QMS_MA_CORRECT_DATA dt = new QMS_MA_CORRECT_DATA();
                dt = QMS_MA_CORRECT_DATA.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_CORRECT_DATA> getQMS_MA_CORRECT_DATAList(QMSDBEntities db)
        {
            List<ViewQMS_MA_CORRECT_DATA> listResult = new List<ViewQMS_MA_CORRECT_DATA>();

            try
            {
                List<QMS_MA_CORRECT_DATA> list = QMS_MA_CORRECT_DATA.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_CORRECT_DATA
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      CROSS_VOLUME_FLAG = dt.CROSS_VOLUME_FLAG,
                                      DELETE_INPUT_QTY = dt.DELETE_INPUT_QTY,
                                      DELETE_OUTPUT_QTY = dt.DELETE_OUTPUT_QTY ,
                                      CAL_OFF_CONTROL = dt.CAL_OFF_CONTROL,
                                      SYSTEM_CONFIG_FLAG = dt.SYSTEM_CONFIG_FLAG,
                                      PRIORITY = dt.PRIORITY,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_CORRECT_DATA convertModelToDB(CreateQMS_MA_CORRECT_DATA model)
        {
            QMS_MA_CORRECT_DATA result = new QMS_MA_CORRECT_DATA();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.CROSS_VOLUME_FLAG = model.CROSS_VOLUME_FLAG;
            result.DELETE_INPUT_QTY = model.DELETE_INPUT_QTY;
            result.DELETE_OUTPUT_QTY = model.DELETE_OUTPUT_QTY;
            result.CAL_OFF_CONTROL = model.CAL_OFF_CONTROL;
            result.SYSTEM_CONFIG_FLAG = model.SYSTEM_CONFIG_FLAG;
            result.PRIORITY = model.PRIORITY;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_CORRECT_DATA convertDBToModel(QMSDBEntities db, QMS_MA_CORRECT_DATA model)
        {
            CreateQMS_MA_CORRECT_DATA result = new CreateQMS_MA_CORRECT_DATA();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.CROSS_VOLUME_FLAG = model.CROSS_VOLUME_FLAG;
            result.DELETE_INPUT_QTY = model.DELETE_INPUT_QTY;
            result.DELETE_OUTPUT_QTY = model.DELETE_OUTPUT_QTY;
            result.CAL_OFF_CONTROL = model.CAL_OFF_CONTROL;
            result.SYSTEM_CONFIG_FLAG = model.SYSTEM_CONFIG_FLAG;
            result.PRIORITY = model.PRIORITY;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));

            return result;
        }

        public bool DeleteQMS_MA_CORRECT_DATAById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_CORRECT_DATA result = new QMS_MA_CORRECT_DATA();
            result = QMS_MA_CORRECT_DATA.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool IsMasterCorrectData(long[] ids)
        {
            bool bResult = false;

            try
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    if (ids[i] <= 6)
                    {
                        bResult = true;
                        _errMsg = @Resource.ResourceString.CannotDeleteMaster;
                    }
                }
            }
            catch
            {
            }

            return bResult;
        }

        public bool DeleteQMS_MA_CORRECT_DATAByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_CORRECT_DATA> result = new List<QMS_MA_CORRECT_DATA>();
            result = QMS_MA_CORRECT_DATA.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_CORRECT_DATA(QMSDBEntities db, CreateQMS_MA_CORRECT_DATA model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_CORRECT_DATA(db, model);
            }
            else
            {
                result = AddQMS_MA_CORRECT_DATA(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_CORRECT_DATA(QMSDBEntities db, CreateQMS_MA_CORRECT_DATA model)
        {
            long result = 0;

            try
            {
                QMS_MA_CORRECT_DATA dt = new QMS_MA_CORRECT_DATA();
                dt = QMS_MA_CORRECT_DATA.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.CROSS_VOLUME_FLAG = model.CROSS_VOLUME_FLAG;
                dt.DELETE_INPUT_QTY = model.DELETE_INPUT_QTY;
                dt.DELETE_OUTPUT_QTY = model.DELETE_OUTPUT_QTY;
                dt.CAL_OFF_CONTROL = model.CAL_OFF_CONTROL;
                dt.SYSTEM_CONFIG_FLAG = model.SYSTEM_CONFIG_FLAG;
                dt.PRIORITY = model.PRIORITY;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_CORRECT_DATA(QMSDBEntities _db, QMS_MA_CORRECT_DATA model)
        {
            long result = 0;
            try
            {
                QMS_MA_CORRECT_DATA dt = new QMS_MA_CORRECT_DATA();
                dt = QMS_MA_CORRECT_DATA.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_CORRECT_DATA.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_DOWNTIME

        private List<ViewQMS_MA_DOWNTIME_DETAIL> convertListData(List<QMS_MA_DOWNTIME_DETAIL> list, decimal limitValue)
        {
            List<ViewQMS_MA_DOWNTIME_DETAIL> listResult = new List<ViewQMS_MA_DOWNTIME_DETAIL>();
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_DOWNTIME_DETAIL
                                  {
                                      ID = dt.ID,
                                      DOWNTIME_ID = dt.DOWNTIME_ID,
                                      EXA_TAG_NAME = dt.EXA_TAG_NAME,
                                      CONVERT_VALUE = dt.CONVERT_VALUE,
                                      DELETE_FLAG = dt.DELETE_FLAG,
                                      MIN_LIMIT = limitValue 
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public  decimal getQMS_MA_DOWNTIME_LimitByPlantAndActiveDate(QMSDBEntities db, long plantId, DateTime activeDate)
        {
            decimal nResult = 0;
            List<QMS_MA_DOWNTIME_DETAIL> listData = new List<QMS_MA_DOWNTIME_DETAIL>();
            QMS_MA_DOWNTIME activeData = new QMS_MA_DOWNTIME();
            try
            {
                //Get All Reduce feed by plantId
                List<QMS_MA_DOWNTIME> listReduce = QMS_MA_DOWNTIME.GetByListPlantId(db, plantId);

                if (listReduce.Count() > 0) //ถ้ามี list reduce feed แสดงว่า plant นี้มีการตรวจสอบ
                {
                    if (listReduce.Count() == 1)//มีข้อมูล เพียงชุดเดียว
                    {
                        activeData = listReduce.FirstOrDefault();
                    }
                    else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                    {
                        int flagData = 0;
                        listReduce = listReduce.OrderBy(m => m.ACTIVE_DATE).ToList(); //เรียงจาก น้อยไปมาก
                        foreach (QMS_MA_DOWNTIME dt in listReduce)
                        {

                            if (activeDate > dt.ACTIVE_DATE)
                            {
                                if (flagData == 0) { activeData = dt; } else { flagData = 1; } //set อย่างน้อย 1 ตัว
                                break;
                            }
                            activeData = dt;
                        }
                    }
                    nResult = activeData.LIMIT_VALUE; 
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return nResult;
        }

        public List<ViewQMS_MA_DOWNTIME_DETAIL> getQMS_MA_DOWNTIME_DETAILActiveListByPlantAndActiveDate(QMSDBEntities db, long plantId, DateTime activeDate)
        {
            List<ViewQMS_MA_DOWNTIME_DETAIL> listResult = new List<ViewQMS_MA_DOWNTIME_DETAIL>();
            List<QMS_MA_DOWNTIME_DETAIL> listData = new List<QMS_MA_DOWNTIME_DETAIL>();
            QMS_MA_DOWNTIME activeData = new QMS_MA_DOWNTIME();
            try
            {
                //Get All Reduce feed by plantId
                List<QMS_MA_DOWNTIME> listReduce = QMS_MA_DOWNTIME.GetByListPlantId(db, plantId);

                if (listReduce.Count() > 0) //ถ้ามี list reduce feed แสดงว่า plant นี้มีการตรวจสอบ
                {
                    if (listReduce.Count() == 1)//มีข้อมูล เพียงชุดเดียว
                    {
                        activeData = listReduce.FirstOrDefault();
                    }
                    else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                    {
                        int flagData = 0;
                        listReduce = listReduce.OrderBy(m => m.ACTIVE_DATE).ToList(); //เลียงจาก น้อยไปมาก
                        foreach (QMS_MA_DOWNTIME dt in listReduce)
                        {

                            if (activeDate > dt.ACTIVE_DATE)
                            {
                                if (flagData == 0) { activeData = dt; } else { flagData = 1; } //set อย่างน้อย 1 ตัว
                                break;
                            }
                            activeData = dt;
                        }
                    }
                    decimal limit = activeData.LIMIT_VALUE;
                    listData = QMS_MA_DOWNTIME_DETAIL.GetActiveAllByDOWNTIME_ID(db, activeData.ID);
                    listResult = convertListData(listData , activeData.LIMIT_VALUE);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_DOWNTIME> searchQMS_MA_DOWNTIME(QMSDBEntities db, DowntimeSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_DOWNTIME> objResult = new List<ViewQMS_MA_DOWNTIME>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_DOWNTIME> listData = new List<QMS_MA_DOWNTIME>();
                listData = QMS_MA_DOWNTIME.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);

                List<QMS_MA_DOWNTIME_DETAIL> listDataDetail = QMS_MA_DOWNTIME_DETAIL.GetActiveAll(db);
                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_DOWNTIME
                                 {

                //if (listData != null)
                //{
                //    objResult = (from dt in listData
                //                 join rd in listDataDetail on dt.ID equals rd.DOWNTIME_ID into group1
                //                 from g1 in group1.DefaultIfEmpty()
                //                 select new ViewQMS_MA_DOWNTIME
                //                 {

                                     ID = dt.ID,
                                     PLANT_ID = dt.PLANT_ID,
                                     PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                     EXCEL_NAME = dt.EXCEL_NAME,
                                     LIMIT_VALUE = dt.LIMIT_VALUE,
                                     UNIT_ID = dt.UNIT_ID,
                                     //EXA_TAG_NAME = (null == g1) ? "" : g1.EXA_TAG_NAME,// joint with QMS_MA_DOWNTIME_DETAIL
                                     EXA_TAG_NAME = getDOWNTIME_NAME(listDataDetail, dt.ID),
                                     ACTIVE_DATE = dt.ACTIVE_DATE,
                                     DOWNTIME_DESC = dt.DOWNTIME_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    if (null != searchModel.mSearch.EXA_TAG_NAME && "" != searchModel.mSearch.EXA_TAG_NAME)
                    {
                        objResult = objResult.Where(m => m.EXA_TAG_NAME.Contains(searchModel.mSearch.EXA_TAG_NAME)).ToList();
                    }
                    objResult = grid.LoadGridData<ViewQMS_MA_DOWNTIME>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_DOWNTIME getQMS_MA_DOWNTIMEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_DOWNTIME objResult = new CreateQMS_MA_DOWNTIME();

            try
            {
                QMS_MA_DOWNTIME dt = new QMS_MA_DOWNTIME();
                dt = QMS_MA_DOWNTIME.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_DOWNTIME> getQMS_MA_DOWNTIMEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_DOWNTIME> listResult = new List<ViewQMS_MA_DOWNTIME>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);


            try
            {
                List<QMS_MA_DOWNTIME> list = QMS_MA_DOWNTIME.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_DOWNTIME
                                  {
                                      ID = dt.ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      EXCEL_NAME = dt.EXCEL_NAME,
                                      LIMIT_VALUE = dt.LIMIT_VALUE,
                                      UNIT_ID = dt.UNIT_ID,
                                      //EXA_TAG_NAME = dt.EXA_TAG_NAME,
                                      ACTIVE_DATE = dt.ACTIVE_DATE,
                                      DOWNTIME_DESC = dt.DOWNTIME_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_DOWNTIME convertModelToDB(CreateQMS_MA_DOWNTIME model)
        {
            QMS_MA_DOWNTIME result = new QMS_MA_DOWNTIME();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
            result.LIMIT_VALUE = model.LIMIT_VALUE;
            result.UNIT_ID = model.UNIT_ID;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.DOWNTIME_DESC = (null == model.DOWNTIME_DESC) ? "" : model.DOWNTIME_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_DOWNTIME convertDBToModel(QMSDBEntities db, QMS_MA_DOWNTIME model)
        {
            CreateQMS_MA_DOWNTIME result = new CreateQMS_MA_DOWNTIME();

            List<QMS_MA_DOWNTIME_DETAIL> listDetail = QMS_MA_DOWNTIME_DETAIL.GetActiveAllByDOWNTIME_ID(db, model.ID);

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
            result.LIMIT_VALUE = model.LIMIT_VALUE;
            result.UNIT_ID = model.UNIT_ID;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.DOWNTIME_DESC = (null == model.DOWNTIME_DESC) ? "" : model.DOWNTIME_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));

            if (null != listDetail && listDetail.Count() > 0)
            {
                result.DOWNTIME_DETAIL_ID = listDetail[0].ID;
                result.CONVERT_VALUE = listDetail[0].CONVERT_VALUE;
                result.EXA_TAG_NAME = listDetail[0].EXA_TAG_NAME;
            }
            else
            {
                result.DOWNTIME_DETAIL_ID = 0;
            }

            return result;
        }

        public bool DeleteQMS_MA_DOWNTIMEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_DOWNTIME result = new QMS_MA_DOWNTIME();
            result = QMS_MA_DOWNTIME.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_DOWNTIMEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_DOWNTIME> result = new List<QMS_MA_DOWNTIME>();
            result = QMS_MA_DOWNTIME.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_DOWNTIME(QMSDBEntities db, CreateQMS_MA_DOWNTIME model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_DOWNTIME(db, model);
            }
            else
            {
                result = AddQMS_MA_DOWNTIME(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_DOWNTIME(QMSDBEntities db, CreateQMS_MA_DOWNTIME model)
        {
            long result = 0;

            try
            {
                QMS_MA_DOWNTIME dt = new QMS_MA_DOWNTIME();
                dt = QMS_MA_DOWNTIME.GetById(db, model.ID);

                dt.PLANT_ID = model.PLANT_ID;
                dt.PLANT_ID = model.PLANT_ID;
                dt.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
                dt.LIMIT_VALUE = model.LIMIT_VALUE;
                dt.UNIT_ID = model.UNIT_ID;
                dt.ACTIVE_DATE = model.ACTIVE_DATE;
                dt.DOWNTIME_DESC = (null == model.DOWNTIME_DESC) ? "" : model.DOWNTIME_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_DOWNTIME(QMSDBEntities _db, QMS_MA_DOWNTIME model)
        {
            long result = 0;
            try
            {
                QMS_MA_DOWNTIME dt = new QMS_MA_DOWNTIME();
                dt = QMS_MA_DOWNTIME.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_DOWNTIME.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion

        #region QMS_MA_DOWNTIME_DETAIL
        //public List<ViewQMS_MA_DOWNTIME_DETAIL> getQMS_MA_DOWNTIME_DETAILActiveList(QMSDBEntities db)
        //{
        //    List<ViewQMS_MA_DOWNTIME_DETAIL> listResult = new List<ViewQMS_MA_DOWNTIME_DETAIL>();

        //    try
        //    {
        //        List<QMS_MA_DOWNTIME_DETAIL> list = QMS_MA_DOWNTIME_DETAIL.GetActiveAll(db);
        //        listResult = convertListData(list);
        //    }
        //    catch (Exception ex)
        //    {
        //        this.writeErrorLog(ex.Message);
        //    }

        //    return listResult;
        //}

        public List<ViewQMS_MA_DOWNTIME_DETAIL> searchQMS_MA_DOWNTIME_DETAIL(QMSDBEntities db, DowntimeDetailSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_DOWNTIME_DETAIL> objResult = new List<ViewQMS_MA_DOWNTIME_DETAIL>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "DOWNTIME_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_DOWNTIME_DETAIL> listData = new List<QMS_MA_DOWNTIME_DETAIL>();
                listData = QMS_MA_DOWNTIME_DETAIL.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = convertListData(listData, -1); 
                    objResult = grid.LoadGridData<ViewQMS_MA_DOWNTIME_DETAIL>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }


            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_DOWNTIME_DETAIL getQMS_MA_DOWNTIME_DETAILById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_DOWNTIME_DETAIL objResult = new CreateQMS_MA_DOWNTIME_DETAIL();

            try
            {
                QMS_MA_DOWNTIME_DETAIL dt = new QMS_MA_DOWNTIME_DETAIL();
                dt = QMS_MA_DOWNTIME_DETAIL.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        //public List<ViewQMS_MA_DOWNTIME_DETAIL> getQMS_MA_DOWNTIME_DETAILList(QMSDBEntities db)
        //{
        //    List<ViewQMS_MA_DOWNTIME_DETAIL> listResult = new List<ViewQMS_MA_DOWNTIME_DETAIL>();

        //    try
        //    {
        //        List<QMS_MA_DOWNTIME_DETAIL> list = QMS_MA_DOWNTIME_DETAIL.GetAll(db);

        //        if (null != list && list.Count() > 0)
        //        {
        //            listResult = convertListData(list);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.writeErrorLog(ex.Message);
        //    }

        //    return listResult;
        //}

        private QMS_MA_DOWNTIME_DETAIL convertModelToDB(CreateQMS_MA_DOWNTIME_DETAIL model)
        {
            QMS_MA_DOWNTIME_DETAIL result = new QMS_MA_DOWNTIME_DETAIL();

            result.ID = model.ID;
            result.DOWNTIME_ID = model.DOWNTIME_ID;
            result.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
            result.CONVERT_VALUE = model.CONVERT_VALUE;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_DOWNTIME_DETAIL convertDBToModel(QMS_MA_DOWNTIME_DETAIL model)
        {
            CreateQMS_MA_DOWNTIME_DETAIL result = new CreateQMS_MA_DOWNTIME_DETAIL();

            result.ID = model.ID;
            result.DOWNTIME_ID = model.DOWNTIME_ID;
            result.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
            result.CONVERT_VALUE = model.CONVERT_VALUE;
            result.DELETE_FLAG = model.DELETE_FLAG;


            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_DOWNTIME_DETAILById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_DOWNTIME_DETAIL result = new QMS_MA_DOWNTIME_DETAIL();
            result = QMS_MA_DOWNTIME_DETAIL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_DOWNTIME_DETAILByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_DOWNTIME_DETAIL> result = new List<QMS_MA_DOWNTIME_DETAIL>();
            result = QMS_MA_DOWNTIME_DETAIL.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_DOWNTIME_DETAIL(QMSDBEntities db, CreateQMS_MA_DOWNTIME_DETAIL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_DOWNTIME_DETAIL(db, model);
            }
            else
            {
                result = AddQMS_MA_DOWNTIME_DETAIL(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_DOWNTIME_DETAIL(QMSDBEntities db, CreateQMS_MA_DOWNTIME_DETAIL model)
        {
            long result = 0;

            try
            {
                QMS_MA_DOWNTIME_DETAIL dt = new QMS_MA_DOWNTIME_DETAIL();
                dt = QMS_MA_DOWNTIME_DETAIL.GetById(db, model.ID);


                dt.DOWNTIME_ID = model.DOWNTIME_ID;
                dt.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
                dt.CONVERT_VALUE = model.CONVERT_VALUE;
                dt.DELETE_FLAG = model.DELETE_FLAG;


                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_DOWNTIME_DETAIL(QMSDBEntities _db, QMS_MA_DOWNTIME_DETAIL model)
        {
            long result = 0;
            try
            {
                QMS_MA_DOWNTIME_DETAIL dt = new QMS_MA_DOWNTIME_DETAIL();
                dt = QMS_MA_DOWNTIME_DETAIL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_DOWNTIME_DETAIL.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion

        #region QMS_MA_EMAIL

        public List<ViewQMS_MA_EMAIL> convertListData(QMSDBEntities db, List<QMS_MA_EMAIL> list)
        {
            List<ViewQMS_MA_EMAIL> listResult = new List<ViewQMS_MA_EMAIL>();

            try
            {
                if (null != list && list.Count() > 0)
                {

                    listResult = (from dt in list
                                  select new ViewQMS_MA_EMAIL
                                  {
                                      ID = dt.ID,
                                      GROUP_TYPE = dt.GROUP_TYPE,
                                      EMPLOYEE_ID = dt.EMPLOYEE_ID,
                                      NAME = dt.NAME,
                                      POSITION = dt.POSITION,
                                      EMAIL = dt.EMAIL,
                                      UNIT_NAME = dt.UNIT_NAME,
                                      DEPARTMENT = dt.DEPARTMENT,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_EMAIL> getQMS_MA_EMAILList(QMSDBEntities db)
        {
            List<ViewQMS_MA_EMAIL> listResult = new List<ViewQMS_MA_EMAIL>();

            try
            {
                List<QMS_MA_EMAIL> list = QMS_MA_EMAIL.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_EMAIL convertModelToDB(CreateQMS_MA_EMAIL model)
        {
            QMS_MA_EMAIL result = new QMS_MA_EMAIL();

            result.ID = model.ID;
            result.GROUP_TYPE = model.GROUP_TYPE;
            result.EMPLOYEE_ID = (null == model.EMPLOYEE_ID) ? "" : model.EMPLOYEE_ID; 
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.POSITION = (null == model.POSITION) ? "" : model.POSITION;
            result.EMAIL = (null == model.EMAIL) ? "" : model.EMAIL;
            result.UNIT_NAME = (null == model.UNIT_NAME) ? "" : model.UNIT_NAME;
            result.DEPARTMENT = (null == model.DEPARTMENT) ? "" : model.DEPARTMENT;
            //result.DELETE_FLAG = model.DELETE_FLAG;

           

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_EMAILById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_EMAIL result = new QMS_MA_EMAIL();
            result = QMS_MA_EMAIL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_EMAILByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_EMAIL> result = new List<QMS_MA_EMAIL>();
            result = QMS_MA_EMAIL.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(db.DeleteObject); //db.DeleteObject(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveListQMS_MA_EMAIL(QMSDBEntities db, List<CreateQMS_MA_EMAIL> listEmail)
        {
            long result = 0;

            try
            {
                foreach( CreateQMS_MA_EMAIL dtEmail in listEmail){
                    QMS_MA_EMAIL tempData = QMS_MA_EMAIL.GetByTypeAndEmpCode(db, dtEmail.GROUP_TYPE, dtEmail.EMPLOYEE_ID);

                    if (null != tempData) //Update
                    {
                        dtEmail.ID = tempData.ID;
                        result = UpdateQMS_MA_EMAIL(db, dtEmail);
                    }
                    else //Add New
                    {
                        result = AddQMS_MA_EMAIL(db, convertModelToDB(dtEmail));
                    }
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveQMS_MA_EMAIL(QMSDBEntities db, CreateQMS_MA_EMAIL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_EMAIL(db, model);
            }
            else
            {
                result = AddQMS_MA_EMAIL(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_EMAIL(QMSDBEntities db, CreateQMS_MA_EMAIL model)
        {
            long result = 0;

            try
            {
                QMS_MA_EMAIL dt = new QMS_MA_EMAIL();
                dt = QMS_MA_EMAIL.GetById(db, model.ID);

                dt.GROUP_TYPE = model.GROUP_TYPE;
                dt.EMPLOYEE_ID = (null == model.EMPLOYEE_ID) ? "" : model.EMPLOYEE_ID;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.POSITION = (null == model.POSITION) ? "" : model.POSITION;
                dt.EMAIL = (null == model.EMAIL) ? "" : model.EMAIL;
                dt.UNIT_NAME = (null == model.UNIT_NAME) ? "" : model.UNIT_NAME;
                dt.DEPARTMENT = (null == model.DEPARTMENT) ? "" : model.DEPARTMENT;
               // dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_EMAIL(QMSDBEntities _db, QMS_MA_EMAIL model)
        {
            long result = 0;
            try
            {
                QMS_MA_EMAIL dt = new QMS_MA_EMAIL();
                dt = QMS_MA_EMAIL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_EMAIL.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_EMAIL> searchQMS_MA_EMAIL(QMSDBEntities db, QMS_MA_EMAILSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_EMAIL> objResult = new List<ViewQMS_MA_EMAIL>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "NAME";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_EMAIL> listData = new List<QMS_MA_EMAIL>();
                listData = QMS_MA_EMAIL.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_EMAIL>(listData.AsQueryable(), out count, out totalPage);
                    objResult = convertListData(db, listData);
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        #endregion

        #region QMS_MA_EXQ_ACCUM_TAG
        public List<ViewQMS_MA_EXQ_ACCUM_TAG> getQMS_MA_EXQ_ACCUM_TAGList(QMSDBEntities db)
        {
            List<ViewQMS_MA_EXQ_ACCUM_TAG> listResult = new List<ViewQMS_MA_EXQ_ACCUM_TAG>();

            try
            {
                List<QMS_MA_EXQ_ACCUM_TAG> list = QMS_MA_EXQ_ACCUM_TAG.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_EXQ_ACCUM_TAG
                                  {
                                      ID = dt.ID,
                                      EXA_TAG_ID = dt.EXA_TAG_ID,
                                      EXA_TAG_VALUE = dt.EXA_TAG_VALUE,
                                      CONVERT_VALUE = dt.CONVERT_VALUE,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public string getExaTagNameById(List<QMS_MA_EXQ_TAG> listData, long id)
        {
            string szExaTagName = "";

            try{
                if(listData.Count() > 0)
                    szExaTagName = listData.Where(m => m.ID == id).Select(m => m.EXA_TAG_NAME).FirstOrDefault();
            }catch(Exception ex){

            }

            return szExaTagName;
        }

        public string getExCellNameById(List<QMS_MA_EXQ_TAG> listData, long id)
        {
            string szExaCellName = "";

            try
            {
                if (listData.Count() > 0)
                    szExaCellName = listData.Where(m => m.ID == id).Select(m => m.EXCEL_NAME).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }

            return szExaCellName;
        }

        public List<ViewQMS_MA_EXQ_ACCUM_TAG> getQMS_MA_EXQ_ACCUM_TAGListByExaTagId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_EXQ_ACCUM_TAG> listResult = new List<ViewQMS_MA_EXQ_ACCUM_TAG>();

            try
            {
                List<QMS_MA_EXQ_ACCUM_TAG> list = QMS_MA_EXQ_ACCUM_TAG.GetByExaId(db, id);

                QMS_MA_EXQ_TAG masterData = QMS_MA_EXQ_TAG.GetById(db, id);
                List<QMS_MA_EXQ_TAG> listQualityTag =  QMS_MA_EXQ_TAG.GetQualityTagAllByProductMapId(db, masterData.PRODUCT_MAPPING_ID);

               

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_EXQ_ACCUM_TAG
                                  {
                                      ID = dt.ID,
                                      EXA_TAG_ID = dt.EXA_TAG_ID,
                                      EXA_TAG_VALUE = dt.EXA_TAG_VALUE,
                                      CONVERT_VALUE = dt.CONVERT_VALUE,
                                      EXA_TAG_NAME = getExaTagNameById (listQualityTag , dt.EXA_TAG_VALUE),
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_EXQ_ACCUM_TAG convertModelToDB(CreateQMS_MA_EXQ_ACCUM_TAG model)
        {
            QMS_MA_EXQ_ACCUM_TAG result = new QMS_MA_EXQ_ACCUM_TAG();

            result.ID = model.ID;
            result.EXA_TAG_ID = model.EXA_TAG_ID;
            result.EXA_TAG_VALUE = model.EXA_TAG_VALUE;
            result.CONVERT_VALUE = model.CONVERT_VALUE;
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_EXQ_ACCUM_TAGById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_EXQ_ACCUM_TAG result = new QMS_MA_EXQ_ACCUM_TAG();
            result = QMS_MA_EXQ_ACCUM_TAG.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_EXQ_ACCUM_TAGByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_EXQ_ACCUM_TAG> result = new List<QMS_MA_EXQ_ACCUM_TAG>();
            result = QMS_MA_EXQ_ACCUM_TAG.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_EXQ_ACCUM_TAG(QMSDBEntities db, CreateQMS_MA_EXQ_ACCUM_TAG model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_EXQ_ACCUM_TAG(db, model);
            }
            else
            {
                result = AddQMS_MA_EXQ_ACCUM_TAG(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_EXQ_ACCUM_TAG(QMSDBEntities db, CreateQMS_MA_EXQ_ACCUM_TAG model)
        {
            long result = 0;

            try
            {
                QMS_MA_EXQ_ACCUM_TAG dt = new QMS_MA_EXQ_ACCUM_TAG();
                dt = QMS_MA_EXQ_ACCUM_TAG.GetById(db, model.ID);

                dt.EXA_TAG_ID = model.EXA_TAG_ID;
                dt.EXA_TAG_VALUE = model.EXA_TAG_VALUE;
                dt.CONVERT_VALUE = model.CONVERT_VALUE;
                //dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_EXQ_ACCUM_TAG(QMSDBEntities _db, QMS_MA_EXQ_ACCUM_TAG model)
        {
            long result = 0;
            try
            {
                QMS_MA_EXQ_ACCUM_TAG dt = new QMS_MA_EXQ_ACCUM_TAG();
                dt = QMS_MA_EXQ_ACCUM_TAG.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_EXQ_ACCUM_TAG.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_EXQ_TAG

        private List<ViewQMS_MA_EXQ_TAG> convertListData(QMSDBEntities db, List<QMS_MA_EXQ_TAG> list)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            List<ViewQMS_MA_UNIT> listUnit = getQMS_MA_UNITList(db);
            List<ViewQMS_MA_CONTROL> listControlGroup = getQMS_MA_CONTROLListByGroupType(db, (byte)CONTROL_GROUP_TYPE.GAS);
            List<ViewQMS_MA_CONTROL> listSpecGroup = getQMS_MA_CONTROLListByGroupType(db, (byte)CONTROL_GROUP_TYPE.SPEC);


            List<ViewQMS_MA_CONTROL_DATA> listControlData = new List<ViewQMS_MA_CONTROL_DATA>();// getQMS_MA_CONTROL_DATAList(db);
            List<ViewQMS_MA_CONTROL_DATA> listSpecData = new List<ViewQMS_MA_CONTROL_DATA>();

            if (listControlGroup.Count() > 0)
            {
                long[] listGroupId = listControlGroup.Select(m => m.ID).ToArray();
                listControlData = getQMS_MA_CONTROL_DATAByListControlId(db, listGroupId);
            }

            if (listSpecGroup.Count() > 0)
            {
                long[] listSpecId = listSpecGroup.Select(m => m.ID).ToArray();
                listSpecData = getQMS_MA_CONTROL_DATAByListControlId(db, listSpecId);
            }
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_EXQ_TAG
                                  {
                                      ID = dt.ID,
                                      PRODUCT_MAPPING_ID = dt.PRODUCT_MAPPING_ID,
                                      UNIT_ID = dt.UNIT_ID,
                                      UNIT_NAME = getUnitName(listUnit, dt.UNIT_ID),
                                      CONTROL_ID = dt.CONTROL_ID,
                                      SPEC_ID = dt.SPEC_ID,
                                      EXCEL_NAME = dt.EXCEL_NAME,
                                      EXA_TAG_NAME = dt.EXA_TAG_NAME,
                                      LAKE_TAG_NAME = dt.LAKE_TAG_NAME,//เพิ่มตอน pop ต่อ DataLeak
                                      TAG_TYPE = dt.TAG_TYPE,
                                      SHOW_TAG_TYPE = getShowTargetValue(dt.TAG_TYPE),
                                      TAG_FLOW_CONVERT_VALUE = dt.TAG_FLOW_CONVERT_VALUE,
                                      TAG_FLOW_CHECK = dt.TAG_FLOW_CHECK,
                                      TAG_FLOW_CHECK_VALUE = dt.TAG_FLOW_CHECK_VALUE,
                                      TAG_TARGET_CHECK = dt.TAG_TARGET_CHECK,
                                      TAG_TARGET_MIN = dt.TAG_TARGET_MIN,
                                      TAG_TARGET_MAX = dt.TAG_TARGET_MAX,
                                      SHOW_TAG_TARGET_VALUE = getShowTargetValue(dt.TAG_TARGET_CHECK, dt.TAG_TARGET_MIN),
                                      TAG_CORRECT = dt.TAG_CORRECT,
                                      TAG_CORRECT_MIN = dt.TAG_CORRECT_MIN,
                                      TAG_CORRECT_MAX = dt.TAG_CORRECT_MAX,

                                      CONTROL_GROUP_SMAPLE_ID = getControlGroupRowIdByControlDataId(listControlData, dt.CONTROL_ID),
                                      CONTROL_GROUP_ITEM_ID = getControlGroupColumnIdByControlDataId(listControlData, dt.CONTROL_ID),
                                      CONTROL_GROUP_ID = getControlGroupIdByControlDataId(listControlData, dt.CONTROL_ID),
                                      CONTROL_VALUE = getControlValueTextByControlDataIdEx(listControlData, dt.CONTROL_ID, dt.CHANGE_CONTROL_ID),

                                      SPEC_GROUP_SMAPLE_ID = getControlGroupRowIdByControlDataId(listSpecData, dt.SPEC_ID),
                                      SPEC_GROUP_ITEM_ID = getControlGroupColumnIdByControlDataId(listSpecData, dt.SPEC_ID),
                                      SPEC_GROUP_ID = getControlGroupIdByControlDataId(listSpecData, dt.SPEC_ID),
                                      SPEC_VALUE = getControlValueTextByControlDataIdEx(listSpecData, dt.SPEC_ID, dt.SPEC_ID),

                                      SHOW_CONVERT_VAlUE = getShowConvertValue(dt.TAG_TYPE, dt.TAG_FLOW_CONVERT_VALUE),
                                      SHOW_PROOF_VALUE = getShowProofValue(dt.TAG_TYPE, dt.TAG_FLOW_CHECK_VALUE),

                                      POSITION = dt.POSITION,

                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss"),
                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss"),

                                      CHANGE_CONTROL_CHECK  = dt.CHANGE_CONTROL_CHECK,
                                      CHANGE_CONTROL_ID = dt.CHANGE_CONTROL_ID,
                                      CHANGE_SPEC_CHECK = dt.CHANGE_SPEC_CHECK,
                                      CHANGE_SPEC_ID = dt.CHANGE_SPEC_ID,

                                      CONTROL_GROUP_SMAPLE_ID2 = getControlGroupRowIdByControlDataId(listControlData, dt.CHANGE_CONTROL_ID),
                                      CONTROL_GROUP_ITEM_ID2 = getControlGroupColumnIdByControlDataId(listControlData, dt.CHANGE_CONTROL_ID),
                                      CONTROL_GROUP_ID2 = getControlGroupIdByControlDataId(listControlData, dt.CHANGE_CONTROL_ID),

                                      SPEC_GROUP_SMAPLE_ID2 = getControlGroupRowIdByControlDataId(listSpecData, dt.CHANGE_SPEC_ID),
                                      SPEC_GROUP_ITEM_ID2 = getControlGroupColumnIdByControlDataId(listSpecData, dt.CHANGE_SPEC_ID),
                                      SPEC_GROUP_ID2 = getControlGroupIdByControlDataId(listSpecData, dt.CHANGE_SPEC_ID),

                                      ARROW_DOWN = true,
                                      ARROW_UP = true
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        private short getMaxEXQ_TAGPostion(QMSDBEntities db, long PRODUCT_MAPPING_ID, byte type )
        {
            short bResult = 1;

            try
            {
                List<QMS_MA_EXQ_TAG> list = new List<QMS_MA_EXQ_TAG>();
                if (type == (byte)EXA_TAG_TYPE.ACCUM)
                {
                    list = QMS_MA_EXQ_TAG.GetAccumTagAllByProductMapId(db, PRODUCT_MAPPING_ID); 
                }
                else if (type == (byte)EXA_TAG_TYPE.QUALITY_TAG)
                {
                    list = QMS_MA_EXQ_TAG.GetQualityTagAllByProductMapId(db, PRODUCT_MAPPING_ID);
                }
                else
                {
                    list = QMS_MA_EXQ_TAG.GetQuantityTagAllByProductMapId(db, PRODUCT_MAPPING_ID);
                }
               

                if (list.Count() > 0)
                {
                    short max = list.Max(m => m.POSITION);

                    if (max < 32767)
                        bResult += max;
                    else
                        bResult = 1;
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }


            return bResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getQMS_MA_EXQ_TAGList(QMSDBEntities db)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                List<QMS_MA_EXQ_TAG> list = QMS_MA_EXQ_TAG.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getListEXQTagByListId(QMSDBEntities db, long[] ids)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                List<QMS_MA_EXQ_TAG> list = QMS_MA_EXQ_TAG.GetByListId(db, ids);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getQualityQMS_MA_EXQ_TAGList(QMSDBEntities db)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                List<QMS_MA_EXQ_TAG> list = QMS_MA_EXQ_TAG.GetQualityTagAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private string getShowTargetValue(byte targetCheck , decimal targetValue)
        {
            string szResult = "-";
            try
            {
                szResult = (targetCheck == 1) ? Convert.ToDouble(targetValue).ToString() : "-";
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return szResult;
        }

        private string getShowTargetValue(byte targetType)
        {
            string szResult = "-";
            try
            {
                if (targetType == (int)EXA_TAG_TYPE.QUALITY_TAG)
                {
                    szResult = Resource.ResourceString.QUALITY;
                }
                else if (targetType == (int)EXA_TAG_TYPE.QUANTITY_PV)
                {
                    szResult = Resource.ResourceString.QUANTITY_PV;
                }
                else if (targetType == (int)EXA_TAG_TYPE.QUANTITY_SUM)
                {
                    szResult = Resource.ResourceString.QUANTITY_SUM;
                }
                else
                {
                    szResult = Resource.ResourceString.QUALITY_ACCUM;
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return szResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getListQMS_MA_EXQ_TAG(QMSDBEntities db, long id, byte type)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();
            List<QMS_MA_EXQ_TAG> list = new List<QMS_MA_EXQ_TAG>();

            if (type == (byte)EXA_TAG_TYPE.QUALITY_TAG) //เป็น tag คุณภาพ
            {
                list = QMS_MA_EXQ_TAG.GetQualityTagAllByProductMapId(db, id);
            }
            else if (type == (byte)EXA_TAG_TYPE.ACCUM) //Accum
            {
                list = QMS_MA_EXQ_TAG.GetAccumTagAllByProductMapId(db, id);
            }
            else //เป็น tag ปริมาณ
            { 
                list = QMS_MA_EXQ_TAG.GetQuantityTagAllByProductMapId(db, id);
            }
            
 
            if (null != list && list.Count() > 0)
            { 
                listResult = convertListData(db, list);
                listResult = listResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                listResult = setArrowUPAndDown(listResult);
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getQualityQMS_MA_EXQ_TAGListByProductMapId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                listResult = getListQMS_MA_EXQ_TAG(db, id, (byte)EXA_TAG_TYPE.QUALITY_TAG);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getQuantityQMS_MA_EXQ_TAGListByProductMapId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                listResult = getListQMS_MA_EXQ_TAG(db, id, (byte)EXA_TAG_TYPE.QUANTITY_PV); 
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getAccumTagQMS_MA_EXQ_TAGListByProductMapId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                listResult = getListQMS_MA_EXQ_TAG(db, id, (byte)EXA_TAG_TYPE.ACCUM); 
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_EXQ_TAG convertModelToDB(CreateQMS_MA_EXQ_TAG model)
        {
            QMS_MA_EXQ_TAG result = new QMS_MA_EXQ_TAG();

            result.ID = model.ID;
            result.PRODUCT_MAPPING_ID = model.PRODUCT_MAPPING_ID;
            result.UNIT_ID = model.UNIT_ID;
            result.CONTROL_ID = model.CONTROL_ID;
            result.SPEC_ID = model.SPEC_ID;
            result.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
            result.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
            result.LAKE_TAG_NAME = (null == model.LAKE_TAG_NAME) ? "" : model.LAKE_TAG_NAME;
            result.TAG_TYPE = model.TAG_TYPE;
            result.TAG_FLOW_CONVERT_VALUE = model.TAG_FLOW_CONVERT_VALUE;
            result.TAG_FLOW_CHECK = model.TAG_FLOW_CHECK;// (model.TAG_FLOW_CHECK == true) ? (byte)1 : (byte)0;
            result.TAG_FLOW_CHECK_VALUE = model.TAG_FLOW_CHECK_VALUE;
            result.TAG_TARGET_CHECK = model.TAG_TARGET_CHECK;//model.TAG_TARGET_CHECK == true) ? (byte)1 : (byte)0;
            result.TAG_TARGET_MIN = model.TAG_TARGET_MIN;
            result.TAG_TARGET_MAX = model.TAG_TARGET_MAX;
            result.TAG_CORRECT = model.TAG_CORRECT;
            result.TAG_CORRECT_MIN = model.TAG_CORRECT_MIN;
            result.TAG_CORRECT_MAX = model.TAG_CORRECT_MAX;
           // result.DELETE_FLAG = model.DELETE_FLAG;

            result.CHANGE_CONTROL_CHECK = model.CHANGE_CONTROL_CHECK;
            result.CHANGE_CONTROL_ID = model.CHANGE_CONTROL_ID;
            result.CHANGE_SPEC_CHECK = model.CHANGE_SPEC_CHECK;
            result.CHANGE_SPEC_CHECK = model.CHANGE_SPEC_CHECK;
            
           
            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_EXQ_TAG convertDBToModel(QMS_MA_EXQ_TAG model)
        {
            CreateQMS_MA_EXQ_TAG result = new CreateQMS_MA_EXQ_TAG();

            result.ID = model.ID;
            result.PRODUCT_MAPPING_ID = model.PRODUCT_MAPPING_ID;
            result.UNIT_ID = model.UNIT_ID;
            result.CONTROL_ID = model.CONTROL_ID;
            result.SPEC_ID = model.SPEC_ID;
            result.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
            result.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
            result.LAKE_TAG_NAME = (null == model.LAKE_TAG_NAME) ? "" : model.LAKE_TAG_NAME;
            result.TAG_TYPE = model.TAG_TYPE;
            result.TAG_FLOW_CONVERT_VALUE = model.TAG_FLOW_CONVERT_VALUE;
            result.TAG_FLOW_CHECK = model.TAG_FLOW_CHECK;// (model.TAG_FLOW_CHECK == true) ? (byte)1 : (byte)0;
            result.TAG_FLOW_CHECK_VALUE = model.TAG_FLOW_CHECK_VALUE;
            result.TAG_TARGET_CHECK = model.TAG_TARGET_CHECK;//model.TAG_TARGET_CHECK == true) ? (byte)1 : (byte)0;
            result.TAG_TARGET_MIN = model.TAG_TARGET_MIN;
            result.TAG_TARGET_MAX = model.TAG_TARGET_MAX;
            result.TAG_CORRECT = model.TAG_CORRECT;
            result.TAG_CORRECT_MIN = model.TAG_CORRECT_MIN;
            result.TAG_CORRECT_MAX = model.TAG_CORRECT_MAX;

            result.CHANGE_CONTROL_CHECK = model.CHANGE_CONTROL_CHECK;
            result.CHANGE_CONTROL_ID = model.CHANGE_CONTROL_ID;
            result.CHANGE_SPEC_CHECK = model.CHANGE_SPEC_CHECK;
            result.CHANGE_SPEC_CHECK = model.CHANGE_SPEC_CHECK;
            // result.DELETE_FLAG = model.DELETE_FLAG;


            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_EXQ_TAGById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_EXQ_TAG result = new QMS_MA_EXQ_TAG();
            result = QMS_MA_EXQ_TAG.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_EXQ_TAGByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_EXQ_TAG> result = new List<QMS_MA_EXQ_TAG>();
            result = QMS_MA_EXQ_TAG.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> setQMS_MA_EXQ_TAGPostion(QMSDBEntities db, SetExaTagPosition model)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                List<QMS_MA_EXQ_TAG> list = new List<QMS_MA_EXQ_TAG>();

                if (model.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG) //tag คุณภาพ
                {
                    list = QMS_MA_EXQ_TAG.GetQualityTagAllByProductMapId(db, model.PRODUCT_MAPPING_ID);// .GetActiveAll(db);
                }
                else if (model.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM) // tag รวม
                {
                    list = QMS_MA_EXQ_TAG.GetAccumTagAllByProductMapId(db, model.PRODUCT_MAPPING_ID);// .GetActiveAll(db);
                }
                else //ปริมาณ 
                {
                    list = QMS_MA_EXQ_TAG.GetQuantityTagAllByProductMapId(db, model.PRODUCT_MAPPING_ID);// .GetActiveAll(db);
                }
                

                QMS_MA_EXQ_TAG LastId = new QMS_MA_EXQ_TAG();
                QMS_MA_EXQ_TAG CurrentId = new QMS_MA_EXQ_TAG();
                QMS_MA_EXQ_TAG NextId = new QMS_MA_EXQ_TAG();

                CreateQMS_MA_EXQ_TAG currentData = new CreateQMS_MA_EXQ_TAG();
                CreateQMS_MA_EXQ_TAG swapData = new CreateQMS_MA_EXQ_TAG();

                bool ExitLoop = false;

                list = list.OrderBy(m => m.POSITION).ToList();

                foreach (QMS_MA_EXQ_TAG dt in list)
                {
                    NextId = dt; // get ค่า ตัวต่อไป

                    if (true == ExitLoop)
                    {
                        break;
                    }

                    if (model.ID == dt.ID) // get ค่า ปัจจุบัน
                    {
                        CurrentId = dt;
                        ExitLoop = true;
                    }
                    else
                    {
                        LastId = dt; // get ค่า สุดท้าย
                    }
                }

                if (ExitLoop) {  //ไม่เจอ ตัวจริง

                    if (model.DIRECTION == (int)ARROW_STATUS.ARROW_UP)
                    {
                        short priority = CurrentId.POSITION; 
                        currentData = convertDBToModel(CurrentId) ;
                        currentData.POSITION = LastId.POSITION;

                        swapData = convertDBToModel(LastId); 
                        swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                    }
                    else
                    {
                        short priority = CurrentId.POSITION;

                        currentData = convertDBToModel(CurrentId); 
                        currentData.POSITION = NextId.POSITION; //สลับตำแหน่งกับค่า ล่าง

                        swapData = convertDBToModel(NextId); 
                        swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                    }

                    UpdateQMS_MA_EXQ_TAG(db, currentData);
                    UpdateQMS_MA_EXQ_TAG(db, swapData);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            if (model.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG)
            {
                return getQualityQMS_MA_EXQ_TAGListByProductMapId(db, model.PRODUCT_MAPPING_ID);

            }
            else if (model.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
            {
                return getAccumTagQMS_MA_EXQ_TAGListByProductMapId(db, model.PRODUCT_MAPPING_ID);

            }
            else
            {
                return getQuantityQMS_MA_EXQ_TAGListByProductMapId(db, model.PRODUCT_MAPPING_ID);
            }

            
        }


        public long SaveQMS_MA_EXQ_TAG(QMSDBEntities db, CreateQMS_MA_EXQ_TAG model)
        {
            long result = 0;

            List<QMS_MA_EXQ_TAG> listQualityExaTag = QMS_MA_EXQ_TAG.GetQualityTagAllByProductMapId(db, model.PRODUCT_MAPPING_ID);
             
            
            if (model.TAG_TYPE != (byte)EXA_TAG_TYPE.QUANTITY_PV)
            {
                model.TAG_FLOW_CHECK_VALUE = 0;
                model.TAG_FLOW_CONVERT_VALUE = 0;
            }
            
            if (model.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
            {
                model.EXA_TAG_NAME = "";
                if (model.lstAccumTag.Count() > 0 && model.EXA_TAG_NAME == "")
                {
                    foreach (CreateQMS_MA_EXQ_ACCUM_TAG createData in model.lstAccumTag)
                    {
                        QMS_MA_EXQ_TAG dataValue = listQualityExaTag.Where(m => m.ID == createData.EXA_TAG_VALUE).FirstOrDefault();
                        if ("" == model.EXA_TAG_NAME)
                        {
                            model.EXA_TAG_NAME = dataValue.EXA_TAG_NAME;
                        }
                        else
                        {
                            model.EXA_TAG_NAME += " + " + dataValue.EXA_TAG_NAME;
                        }
                    }
                }
            }



            if (model.ID > 0)
            {
                result = UpdateQMS_MA_EXQ_TAG(db, model);
            }
            else
            {
                result = AddQMS_MA_EXQ_TAG(db, convertModelToDB(model));
            }

            //Accum Tag...
            if (model.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
            {
                //Save 
                if (null != model.lstAccumTag && model.lstAccumTag.Count() > 0)
                {
                    foreach (CreateQMS_MA_EXQ_ACCUM_TAG createData in model.lstAccumTag)
                    {
                        createData.EXA_TAG_ID = result; //id ของ master 
                        SaveQMS_MA_EXQ_ACCUM_TAG(db, createData);
                    }
                }

                //Delete
                if (null != model.lstDelAccumTag && model.lstDelAccumTag.Count() > 0)
                {
                    DeleteQMS_MA_EXQ_ACCUM_TAGByListId(db , model.lstDelAccumTag.ToArray());
                } 
            }


            return result;
        }

        public long UpdateQMS_MA_EXQ_TAG(QMSDBEntities db, CreateQMS_MA_EXQ_TAG model)
        {
            long result = 0;

            try
            {
                QMS_MA_EXQ_TAG dt = new QMS_MA_EXQ_TAG();
                dt = QMS_MA_EXQ_TAG.GetById(db, model.ID);

                dt.PRODUCT_MAPPING_ID = model.PRODUCT_MAPPING_ID;
                dt.UNIT_ID = model.UNIT_ID;
                dt.CONTROL_ID = model.CONTROL_ID;
                dt.SPEC_ID = model.SPEC_ID;
                dt.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
                dt.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
                dt.LAKE_TAG_NAME = (null == model.LAKE_TAG_NAME) ? "" : model.LAKE_TAG_NAME;
                dt.TAG_TYPE = model.TAG_TYPE;
                dt.TAG_FLOW_CONVERT_VALUE = model.TAG_FLOW_CONVERT_VALUE;
                dt.TAG_FLOW_CHECK_VALUE = model.TAG_FLOW_CHECK_VALUE;
                dt.TAG_FLOW_CHECK = model.TAG_FLOW_CHECK;// (model.TAG_FLOW_CHECK == true) ? (byte)1 : (byte)0;
                dt.TAG_TARGET_CHECK = model.TAG_TARGET_CHECK;//(model.TAG_TARGET_CHECK == true) ? (byte)1 : (byte)0;
                dt.TAG_TARGET_MIN = model.TAG_TARGET_MIN;
                dt.TAG_TARGET_MAX = model.TAG_TARGET_MAX;
                dt.TAG_CORRECT = model.TAG_CORRECT;
                dt.TAG_CORRECT_MIN = model.TAG_CORRECT_MIN;
                dt.TAG_CORRECT_MAX = model.TAG_CORRECT_MAX;

                dt.CHANGE_CONTROL_CHECK = model.CHANGE_CONTROL_CHECK;
                dt.CHANGE_CONTROL_ID = model.CHANGE_CONTROL_ID;
                dt.CHANGE_SPEC_CHECK = model.CHANGE_SPEC_CHECK;
                dt.CHANGE_SPEC_ID = model.SPEC_ID;

                dt.POSITION = model.POSITION;
              //  dt.DELETE_FLAG = model.DELETE_FLAG;
                    


                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long UpdateSpecAndControlQMS_MA_EXQ_TAG(QMSDBEntities db, CreateQMS_MA_EXQ_TAG model)
        {
            long result = 0;

            try
            {
                QMS_MA_EXQ_TAG dt = new QMS_MA_EXQ_TAG();
                dt = QMS_MA_EXQ_TAG.GetById(db, model.ID);
                 
                dt.CONTROL_ID = model.CONTROL_ID;
                dt.SPEC_ID = model.SPEC_ID;
                dt.CHANGE_CONTROL_CHECK = model.CHANGE_CONTROL_CHECK;
                dt.CHANGE_CONTROL_ID = model.CHANGE_CONTROL_ID;
                dt.CHANGE_SPEC_CHECK = model.CHANGE_SPEC_CHECK;
                dt.CHANGE_SPEC_ID = model.SPEC_ID;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_EXQ_TAG(QMSDBEntities _db, QMS_MA_EXQ_TAG model)
        {
            long result = 0;
            try
            {
                QMS_MA_EXQ_TAG dt = new QMS_MA_EXQ_TAG();
                dt = QMS_MA_EXQ_TAG.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    model.POSITION = this.getMaxEXQ_TAGPostion(_db, model.PRODUCT_MAPPING_ID, model.TAG_TYPE);
                    _db.QMS_MA_EXQ_TAG.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public bool LoadQMS_MA_EXQ_TAG_From_Excel(QMSDBEntities _db,string filename)
        {
            bool bResult = false;
            try
            {
                List<QMS_MA_EXQ_TAG> listEXQ_TAG = new List<QMS_MA_EXQ_TAG>();
                listEXQ_TAG = QMS_MA_EXQ_TAG.GetAll(_db);
                FileInfo existingFile = new FileInfo(filename);
                if (existingFile.Exists && listEXQ_TAG.Count() > 0)
                {
                    var startrow = 2;
                    using (ExcelPackage xlPackage = new ExcelPackage(existingFile))
                    {
                        var sheet = xlPackage.Workbook.Worksheets[2];
                        if (sheet.Cells["D1"].Value.ToString() == "EXA_TAG_NAME" && sheet.Cells["E1"].Value.ToString() == "ROOT_TAG_MAPPING")
                        {
                            foreach (QMS_MA_EXQ_TAG item in listEXQ_TAG)
                            {
                                for (var rownum = startrow; rownum <= sheet.Dimension.End.Row; rownum++)
                                {
                                    if (item.EXA_TAG_NAME == sheet.Cells['D' + rownum.ToString()].Value.ToString())
                                    {
                                        item.LAKE_TAG_NAME = sheet.Cells['E' + rownum.ToString()].Value.ToString();
                                        CreateQMS_MA_EXQ_TAG model = convertDBToModel(item);
                                        UpdateQMS_MA_EXQ_TAG(_db, model);
                                    }
                                }
                            }
                            bResult = true;
                        }
                        else
                        {
                            bResult = false;
                        }
                    }
                    
                }
                else
                {
                    bResult = false;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public string LoadDataLakeTODB(QMSDBEntities _db, DateTime? dStratDate, DateTime? dEndDate)
        {
            bool bResult = false;
            string Counttagname = "0";
            try
            {
                ConfigServices cogService = new ConfigServices(_currentUserName);
                CreateQMS_ST_EXA_CONNECT createExaConnect = new CreateQMS_ST_EXA_CONNECT();
                createExaConnect = cogService.getEXA_SCHEDULE(_db);
                if (createExaConnect.LAKE_SERVER != null && createExaConnect.LAKE_DATABASE != null && createExaConnect.LAKE_USER != null && createExaConnect.LAKE_PASSWORD != null)
                {
                    string szConnectionString = String.Format("Data Source={0}; Initial Catalog={1}; User ID={2}; Password={3}", createExaConnect.LAKE_SERVER.ToString(), createExaConnect.LAKE_DATABASE.ToString(), createExaConnect.LAKE_USER.ToString(), createExaConnect.LAKE_PASSWORD.ToString());
                    //ListGSP_QMS
                    DataLakeCommandSQL DataLakeCommandSQL = new DataLakeCommandSQL(szConnectionString);
                    List<ViewQMS_RAW_DATA> ListGSP_QMSAll = new List<ViewQMS_RAW_DATA>();
                    if (dStratDate.HasValue && dEndDate.HasValue) 
                    {
                        ListGSP_QMSAll = DataLakeCommandSQL.getAllFromGSP_QMS(dStratDate, dEndDate);

                        //listPlantProduct
                        List<ViewQMS_MA_PRODUCT_MAPPING> listPlantProduct = new List<ViewQMS_MA_PRODUCT_MAPPING>();
                        listPlantProduct = getQMS_MA_PRODUCT_MAPPINGList(_db);

                        if (listPlantProduct.Count() > 0 && ListGSP_QMSAll.Count() > 0)
                        {
                            List<string> Listtagname = ListGSP_QMSAll.Select(x => x.tag_name).Distinct().ToList();
                            Counttagname = Listtagname.Count().ToString();
                            var CleanData = CleanDataBeforeInsertDataLake(_db, new DateTime(ListGSP_QMSAll[0].sampling_time.Year, ListGSP_QMSAll[0].sampling_time.Month, ListGSP_QMSAll[0].sampling_time.Day));
                            if (CleanData)
                            {
                                foreach (var plantProduct in listPlantProduct)
                                {
                                    //listResultTag
                                    List<ViewQMS_MA_EXQ_TAG> listResultTag = new List<ViewQMS_MA_EXQ_TAG>();
                                    //listQualityTag
                                    List<ViewQMS_MA_EXQ_TAG> listQualityTag = new List<ViewQMS_MA_EXQ_TAG>();
                                    listQualityTag = getQualityQMS_MA_EXQ_TAGListByProductMapId(_db, plantProduct.ID);
                                    if (listQualityTag.Count() > 0)
                                    {
                                        listResultTag.AddRange(listQualityTag);  
                                        //MapLakeTagName(_db, listQualityTag, ListGSP_QMSAll, plantProduct);
                                    }

                                    //listQuantityTag
                                    List<ViewQMS_MA_EXQ_TAG> listQuantityTag = new List<ViewQMS_MA_EXQ_TAG>();
                                    listQuantityTag = getQuantityQMS_MA_EXQ_TAGListByProductMapId(_db, plantProduct.ID);
                                    if (listQuantityTag.Count() > 0)
                                    {
                                        listResultTag.AddRange(listQuantityTag);
                                        //MapLakeTagName(_db, listQuantityTag, ListGSP_QMSAll, plantProduct);
                                    }

                                    //listAccumTag
                                    List<ViewQMS_MA_EXQ_TAG> listAccumTag = new List<ViewQMS_MA_EXQ_TAG>();
                                    listAccumTag = getAccumTagQMS_MA_EXQ_TAGListByProductMapId(_db, plantProduct.ID);
                                    if (listAccumTag.Count() > 0)
                                    {
                                        listResultTag.AddRange(listAccumTag);
                                        //MapLakeTagName(_db, listAccumTag, ListGSP_QMSAll, plantProduct);
                                    }

                                    if (listResultTag.Count() > 0) 
                                    {
                                        MapLakeTagName(_db, listResultTag,ListGSP_QMSAll, plantProduct, dStratDate, dEndDate);
                                    }
                                }
                            }
                        }
                        bResult = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return Counttagname;
        }

        public bool MapLakeTagName(QMSDBEntities _db, List<ViewQMS_MA_EXQ_TAG> listTag, List<ViewQMS_RAW_DATA> ListGSP_QMSAll, ViewQMS_MA_PRODUCT_MAPPING plantProduct, DateTime? dStratDate, DateTime? dEndDate)
        {
            bool bResult = false;
            try 
            {
                ExaProductSearchModel searchExaProduct = new ExaProductSearchModel();
                searchExaProduct.START_DATE = dStratDate.Value.AddMinutes(2);
                searchExaProduct.END_DATE = dEndDate.Value;
                searchExaProduct.PLANT_ID = plantProduct.PLANT_ID;
                searchExaProduct.PRODUCT_ID = plantProduct.PRODUCT_ID;

                List<CreateQMS_TR_EXQ_PRODUCT> listExaProductData = new List<CreateQMS_TR_EXQ_PRODUCT>();
                foreach (var laketag in listTag)
                {
                    List<ViewQMS_RAW_DATA> ListGSP_QMS = new List<ViewQMS_RAW_DATA>();
                    if (laketag.LAKE_TAG_NAME != "")
                    {
                        ListGSP_QMS = ListGSP_QMSAll.Where(x => x.tag_name == laketag.LAKE_TAG_NAME).ToList();
                        if (ListGSP_QMS.Count() > 0)
                        {
                            CreateQMS_TR_EXQ_PRODUCT tempModel = new CreateQMS_TR_EXQ_PRODUCT();
                            tempModel.ID = 0;
                            tempModel.PLANT_ID = plantProduct.PLANT_ID;
                            tempModel.PRODUCT_ID = plantProduct.PRODUCT_ID;
                            tempModel.TAG_DATE = new DateTime(ListGSP_QMS[0].sampling_time.Year, ListGSP_QMS[0].sampling_time.Month, ListGSP_QMS[0].sampling_time.Day);
                            tempModel.TAG_EXA_ID = laketag.ID;
                            tempModel.TAG_VALUE = "";
                            tempModel.TAG_CONVERT = "";
                            tempModel.TAG_GCERROR = "";
                            tempModel.STATUS_1 = (byte)CAL_OFF_STATUS.INITIAL;
                            tempModel.STATUS_2 = (byte)CAL_OFF_STATUS.INITIAL;

                            string GC_ERROR_FLAG_0 = "0";
                            string tempFlowCheck = "0";

                            foreach (var GSP in ListGSP_QMS)
                            {
                                if (tempModel.TAG_VALUE == "")
                                {
                                    tempModel.TAG_VALUE = GSP.value.ToString();
                                }
                                else
                                {
                                    tempModel.TAG_VALUE = tempModel.TAG_VALUE + "," + GSP.value.ToString();
                                }
                                if (tempModel.TAG_CONVERT == "")
                                {
                                    if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG)
                                    {
                                        tempModel.TAG_CONVERT = GSP.value.ToString();
                                    }
                                    else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV)
                                    {
                                        tempFlowCheck = getConvertValueTagConvert(GSP.value.ToString(), laketag);
                                        tempModel.TAG_CONVERT = tempFlowCheck.ToString();
                                        if (Convert.ToDecimal(tempFlowCheck) < laketag.TAG_FLOW_CONVERT_VALUE)
                                        {
                                            GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE.ToString();
                                        }
                                    }
                                    else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
                                    {
                                        tempModel.TAG_CONVERT = GSP.value.ToString();
                                    }
                                    else
                                    {
                                        tempModel.TAG_CONVERT = GSP.value.ToString();
                                    }
                                }
                                else
                                {
                                    if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG)
                                    {
                                        tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + GSP.value.ToString();
                                    }
                                    else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV)
                                    {
                                        tempFlowCheck = getConvertValueTagConvert(GSP.value.ToString(), laketag);
                                        tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + tempFlowCheck.ToString();
                                        if (Convert.ToDecimal(tempFlowCheck) < laketag.TAG_FLOW_CONVERT_VALUE)
                                        {
                                            GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE.ToString();
                                        }
                                    }
                                    else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
                                    {
                                        tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + GSP.value.ToString();
                                    }
                                    else
                                    {
                                        tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + GSP.value.ToString();
                                    }
                                }
                                if (tempModel.TAG_GCERROR == "")
                                {
                                    tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString();
                                }
                                else
                                {
                                    tempModel.TAG_GCERROR = tempModel.TAG_GCERROR + "," + GC_ERROR_FLAG_0.ToString();
                                }
                            }
                            listExaProductData.Add(tempModel);
                        }
                    }
                }
                if (listExaProductData.Count() > 0) 
                {
                    TransactionService TransactionService = new TransactionService(_currentUserName);
                    long nResult = TransactionService.SaveListQMS_TR_EXQ_PRODUCT(_db, listExaProductData, listTag, searchExaProduct);
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return bResult;
        }

        //public bool MapLakeTagNameold(QMSDBEntities _db, List<ViewQMS_MA_EXQ_TAG> listTag, List<ViewQMS_RAW_DATA> ListGSP_QMSAll, ViewQMS_MA_PRODUCT_MAPPING plantProduct)
        //{
        //    bool bResult = false;
        //    try
        //    {
        //        foreach (var laketag in listTag)
        //        {
        //            List<ViewQMS_RAW_DATA> ListGSP_QMS = new List<ViewQMS_RAW_DATA>();
        //            if (laketag.LAKE_TAG_NAME != "") 
        //            {
        //                ListGSP_QMS = ListGSP_QMSAll.Where(x => x.tag_name == laketag.LAKE_TAG_NAME).ToList();
        //                if (ListGSP_QMS.Count()>0)
        //                {
        //                    CreateQMS_TR_EXQ_PRODUCT tempModel = new CreateQMS_TR_EXQ_PRODUCT();
        //                    tempModel.ID = 0;
        //                    tempModel.PLANT_ID = plantProduct.PLANT_ID;
        //                    tempModel.PRODUCT_ID = plantProduct.PRODUCT_ID;
        //                    tempModel.TAG_DATE = new DateTime(ListGSP_QMS[0].sampling_time.Year, ListGSP_QMS[0].sampling_time.Month, ListGSP_QMS[0].sampling_time.Day);
        //                    tempModel.TAG_EXA_ID = laketag.ID;
        //                    tempModel.TAG_VALUE = "";
        //                    tempModel.TAG_CONVERT = "";
        //                    tempModel.TAG_GCERROR = "";
        //                    tempModel.STATUS_1 = (byte)CAL_OFF_STATUS.INITIAL;
        //                    tempModel.STATUS_2 = (byte)CAL_OFF_STATUS.INITIAL;

        //                    string GC_ERROR_FLAG_0 = "0";
        //                    string tempFlowCheck = "0";

        //                    foreach (var GSP in ListGSP_QMS)
        //                    {
        //                        if (tempModel.TAG_VALUE == "")
        //                        {
        //                            tempModel.TAG_VALUE = GSP.value.ToString();
        //                        }
        //                        else 
        //                        {
        //                            tempModel.TAG_VALUE = tempModel.TAG_VALUE + "," + GSP.value.ToString();
        //                        }
        //                        if (tempModel.TAG_CONVERT == "")
        //                        {
        //                            if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG)
        //                            {
        //                                tempModel.TAG_CONVERT = GSP.value.ToString();
        //                            }
        //                            else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV)
        //                            {
        //                                tempFlowCheck = getConvertValueTagConvert(GSP.value.ToString(), laketag);
        //                                tempModel.TAG_CONVERT = tempFlowCheck.ToString();
        //                                if (Convert.ToDecimal(tempFlowCheck) < laketag.TAG_FLOW_CONVERT_VALUE)
        //                                {
        //                                    GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE.ToString();
        //                                }
        //                            }
        //                            else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
        //                            {
        //                                tempModel.TAG_CONVERT = GSP.value.ToString();
        //                            }
        //                            else
        //                            {
        //                                tempModel.TAG_CONVERT = GSP.value.ToString();
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG)
        //                            {
        //                                tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + GSP.value.ToString();
        //                            }
        //                            else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV)
        //                            {
        //                                tempFlowCheck = getConvertValueTagConvert(GSP.value.ToString(), laketag);
        //                                tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + tempFlowCheck.ToString();
        //                                if (Convert.ToDecimal(tempFlowCheck) < laketag.TAG_FLOW_CONVERT_VALUE)
        //                                {
        //                                    GC_ERROR_FLAG_0 = CAL_OFF_STATUS.GC_ERROR_FLOWRATE.ToString();
        //                                }
        //                            }
        //                            else if (laketag.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
        //                            {
        //                                tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + GSP.value.ToString();
        //                            }
        //                            else
        //                            {
        //                                tempModel.TAG_CONVERT = tempModel.TAG_CONVERT + "," + GSP.value.ToString();
        //                            }
        //                        }
        //                        if (tempModel.TAG_GCERROR == "")
        //                        {
        //                            tempModel.TAG_GCERROR = GC_ERROR_FLAG_0.ToString();
        //                        }
        //                        else
        //                        {
        //                            tempModel.TAG_GCERROR = tempModel.TAG_GCERROR + "," + GC_ERROR_FLAG_0.ToString();
        //                        }
        //                    }
        //                    TransactionService TransactionService = new TransactionService(_currentUserName);
        //                    long nResult = TransactionService.SaveQMS_TR_PRODUCT(_db, tempModel);
        //                }
        //            }
        //        }
        //        bResult = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        _errMsg = this.getEexceptionError(ex);
        //    }

        //    return bResult;
        //}
        
        public string getConvertValueTagConvert(string tagValue, ViewQMS_MA_EXQ_TAG CurrentExaTag)
        {
            string convertValue = "0";
            try 
            {
                if (CurrentExaTag.TAG_TYPE == 2)
                {
                    convertValue = (Convert.ToDecimal(tagValue) * CurrentExaTag.TAG_FLOW_CONVERT_VALUE).ToString();
                }
                else
                {
                    convertValue = tagValue;
                }
            }
            catch (Exception ex)
            {
                convertValue = "0";
            }
            return convertValue;
        }
        public bool CleanDataBeforeInsertDataLake(QMSDBEntities _db, DateTime? dateTime)
        {
            bool bResult = false;
            try
            {
                //ListQMS_TR_EXQ_PRODUCT
                QMS_TR_EXQ_PRODUCT QMS_TR_EXQ_PRODUCT = new QMS_TR_EXQ_PRODUCT();
                List<QMS_TR_EXQ_PRODUCT> ListQMS_TR_EXQ_PRODUCT = new List<QMS_TR_EXQ_PRODUCT>();
                ListQMS_TR_EXQ_PRODUCT = _db.QMS_TR_EXQ_PRODUCT.Where(x => x.TAG_DATE == dateTime).ToList();
                if (ListQMS_TR_EXQ_PRODUCT.Count() > 0)
                {
                    ListQMS_TR_EXQ_PRODUCT.ForEach(_db.DeleteObject);
                    _db.SaveChanges();
                    bResult = true;
                }
                bResult = true;
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }
        #endregion

        #region QMS_MA_GRADE
         

        public List<ViewQMS_MA_GRADE> getQMS_MA_GRADEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_GRADE> listResult = new List<ViewQMS_MA_GRADE>();

            try
            {
                List<QMS_MA_GRADE> list = QMS_MA_GRADE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_GRADE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      EXCEL_FLAG = dt.EXCEL_FLAG,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_GRADE> getQMS_MA_GRADEActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_GRADE> listResult = new List<ViewQMS_MA_GRADE>();
            List<ViewQMS_MA_GRADE> listResult2 = new List<ViewQMS_MA_GRADE>();
            try
            {
                List<QMS_MA_GRADE> list = QMS_MA_GRADE.GetActiveAll(db);

                listResult.Add(new ViewQMS_MA_GRADE { ID = 2, NAME = "None" });

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_GRADE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      EXCEL_FLAG  = dt.EXCEL_FLAG,
                                      
                                  }).ToList();
                    //listResult.Concat(listResult2);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_GRADE convertModelToDB(CreateQMS_MA_GRADE model)
        {
            QMS_MA_GRADE result = new QMS_MA_GRADE();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.EXCEL_FLAG = model.EXCEL_FLAG;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_GRADEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_GRADE result = new QMS_MA_GRADE();
            result = QMS_MA_GRADE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_GRADEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_GRADE> result = new List<QMS_MA_GRADE>();
            result = QMS_MA_GRADE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long CheckDuplicateName(QMSDBEntities db, CreateQMS_MA_GRADE model)
        {
            long result = 0;

            QMS_MA_GRADE duplicate = new QMS_MA_GRADE();

            if (model.ID > 0)
            {
                duplicate = QMS_MA_GRADE.GetByGradeNameAndSkipGradeId(db, model.NAME, model.ID); 
            }
            else
            {
                duplicate = QMS_MA_GRADE.GetByGradeName(db, model.NAME);
            }


            if (null != duplicate && duplicate.ID > 0)
            {
                result = -1; // Error
            } 
            return result;
        }

        public long SaveQMS_MA_GRADE(QMSDBEntities db, CreateQMS_MA_GRADE model)
        {
            long result = 0;
            result = CheckDuplicateName(db, model);

            if (0 == result)
            {
                if (model.ID > 0)
                {
                    result = UpdateQMS_MA_GRADE(db, model);
                }
                else
                {
                    result = AddQMS_MA_GRADE(db, convertModelToDB(model));
                }
            }

            return result;
        }

        public long UpdateQMS_MA_GRADE(QMSDBEntities db, CreateQMS_MA_GRADE model)
        {
            long result = 0;

            try
            {
                QMS_MA_GRADE dt = new QMS_MA_GRADE();
                dt = QMS_MA_GRADE.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.EXCEL_FLAG = model.EXCEL_FLAG;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_GRADE(QMSDBEntities _db, QMS_MA_GRADE model)
        {
            long result = 0;
            try
            {
                QMS_MA_GRADE dt = new QMS_MA_GRADE();
                dt = QMS_MA_GRADE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_GRADE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_GRADE> GetChangeGradeListByNotinId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_GRADE> listResult = new List<ViewQMS_MA_GRADE>();

            try
            {
                List<QMS_MA_GRADE> list = QMS_MA_GRADE.GetAllByNotinGradeId(db, id);

                listResult.Add(new ViewQMS_MA_GRADE { ID = 2, NAME = "None" });

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_GRADE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        #endregion

        #region QMS_MA_KPI

        public List<ViewQMS_MA_KPI> searchQMS_MA_KPI(QMSDBEntities db, KPISearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_KPI> objResult = new List<ViewQMS_MA_KPI>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PRODUCT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_KPI> listData = new List<QMS_MA_KPI>();
                listData = QMS_MA_KPI.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_KPI>(listData.AsQueryable(), out count, out totalPage);
                    objResult = (from dt in query
                                 select new ViewQMS_MA_KPI
                                 {
                                     ID = dt.ID,
                                     PLANT_ID = dt.PLANT_ID,
                                     PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                     PRODUCT_ID = dt.PRODUCT_ID,
                                     PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                     START_VALUE_5 = dt.START_VALUE_5,
                                     END_VALUE_5 = dt.END_VALUE_5,
                                     START_VALUE_4 = dt.START_VALUE_4,
                                     END_VALUE_4 = dt.END_VALUE_4,
                                     START_VALUE_3 = dt.START_VALUE_3,
                                     END_VALUE_3 = dt.END_VALUE_3,
                                     START_VALUE_2 = dt.START_VALUE_2,
                                     END_VALUE_2 = dt.END_VALUE_2,
                                     START_VALUE_1 = dt.START_VALUE_1,
                                     END_VALUE_1 = dt.END_VALUE_1,
                                     ACTIVE_DATE = dt.ACTIVE_DATE,
                                     KPI_DESC = dt.KPI_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG,

                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER,
                                     SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                     SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),


                                 }).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_KPI getQMS_MA_KPIById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_KPI objResult = new CreateQMS_MA_KPI();

            try
            {
                QMS_MA_KPI dt = new QMS_MA_KPI();
                dt = QMS_MA_KPI.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_KPI> getQMS_MA_KPIList(QMSDBEntities db)
        {
            List<ViewQMS_MA_KPI> listResult = new List<ViewQMS_MA_KPI>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);

            try
            {
                List<QMS_MA_KPI> list = QMS_MA_KPI.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_KPI
                                  {
                                      ID = dt.ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                      START_VALUE_5 = dt.START_VALUE_5,
                                      END_VALUE_5 = dt.END_VALUE_5,
                                      START_VALUE_4 = dt.START_VALUE_4,
                                      END_VALUE_4 = dt.END_VALUE_4,
                                      START_VALUE_3 = dt.START_VALUE_3,
                                      END_VALUE_3 = dt.END_VALUE_3,
                                      START_VALUE_2 = dt.START_VALUE_2,
                                      END_VALUE_2 = dt.END_VALUE_2,
                                      START_VALUE_1 = dt.START_VALUE_1,
                                      END_VALUE_1 = dt.END_VALUE_1,
                                      ACTIVE_DATE = dt.ACTIVE_DATE,
                                      KPI_DESC = dt.KPI_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_KPI convertModelToDB(CreateQMS_MA_KPI model)
        {
            QMS_MA_KPI result = new QMS_MA_KPI();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.START_VALUE_5 = model.START_VALUE_5;
            result.END_VALUE_5 = model.END_VALUE_5;
            result.START_VALUE_4 = model.START_VALUE_4;
            result.END_VALUE_4 = model.END_VALUE_4;
            result.START_VALUE_3 = model.START_VALUE_3;
            result.END_VALUE_3 = model.END_VALUE_3;
            result.START_VALUE_2 = model.START_VALUE_2;
            result.END_VALUE_2 = model.END_VALUE_2;
            result.START_VALUE_1 = model.START_VALUE_1;
            result.END_VALUE_1 = model.END_VALUE_1;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.KPI_DESC = (null == model.KPI_DESC) ? "" : model.KPI_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_KPI convertDBToModel(QMS_MA_KPI model)
        {
            CreateQMS_MA_KPI result = new CreateQMS_MA_KPI();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.START_VALUE_5 = model.START_VALUE_5;
            result.END_VALUE_5 = model.END_VALUE_5;
            result.START_VALUE_4 = model.START_VALUE_4;
            result.END_VALUE_4 = model.END_VALUE_4;
            result.START_VALUE_3 = model.START_VALUE_3;
            result.END_VALUE_3 = model.END_VALUE_3;
            result.START_VALUE_2 = model.START_VALUE_2;
            result.END_VALUE_2 = model.END_VALUE_2;
            result.START_VALUE_1 = model.START_VALUE_1;
            result.END_VALUE_1 = model.END_VALUE_1;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.KPI_DESC = (null == model.KPI_DESC) ? "" : model.KPI_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_KPIById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_KPI result = new QMS_MA_KPI();
            result = QMS_MA_KPI.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_KPIByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_KPI> result = new List<QMS_MA_KPI>();
            result = QMS_MA_KPI.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_KPI(QMSDBEntities db, CreateQMS_MA_KPI model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_KPI(db, model);
            }
            else
            {
                result = AddQMS_MA_KPI(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_KPI(QMSDBEntities db, CreateQMS_MA_KPI model)
        {
            long result = 0;

            try
            {
                QMS_MA_KPI dt = new QMS_MA_KPI();
                dt = QMS_MA_KPI.GetById(db, model.ID);

                dt.PLANT_ID = model.PLANT_ID;
                dt.PRODUCT_ID = model.PRODUCT_ID;
                dt.START_VALUE_5 = model.START_VALUE_5;
                dt.END_VALUE_5 = model.END_VALUE_5;
                dt.START_VALUE_4 = model.START_VALUE_4;
                dt.END_VALUE_4 = model.END_VALUE_4;
                dt.START_VALUE_3 = model.START_VALUE_3;
                dt.END_VALUE_3 = model.END_VALUE_3;
                dt.START_VALUE_2 = model.START_VALUE_2;
                dt.END_VALUE_2 = model.END_VALUE_2;
                dt.START_VALUE_1 = model.START_VALUE_1;
                dt.END_VALUE_1 = model.END_VALUE_1;
                dt.ACTIVE_DATE = model.ACTIVE_DATE;
                dt.KPI_DESC = (null == model.KPI_DESC) ? "" : model.KPI_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_KPI(QMSDBEntities _db, QMS_MA_KPI model)
        {
            long result = 0;
            try
            {
                QMS_MA_KPI dt = new QMS_MA_KPI();
                dt = QMS_MA_KPI.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_KPI.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }



        #endregion

        #region QMS_MA_OPERATION_SHIFT

        public long saveQMS_MA_OPERATION_SHIFTByExcelFile(QMSDBEntities db, string excelFile, short year)
        {
            long nResult = -1;
            FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
            string ext = Path.GetExtension(fileStream.Name);
            //ExcelDataReader library .................
            //Choose one of either 1 or 2
            IExcelDataReader excelReader;

            if (ext == ".xls")
            {
                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
            }
            else // if( ext == ".xlsx")
            {
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
            }

            try
            {  
                
                int[] shiftRows1 = new int[12] { 3, 8, 13, 18, 23, 28, 33, 38, 43, 48, 53, 58 };
                int[] shiftRows2 = new int[12] { 4, 9, 14, 19, 24, 29, 34, 39, 44, 49, 54, 59 };
                List<string> listShift1 = new List<string>();
                List<string> listShift2 = new List<string>();
                string shiftName = "";
                string dayOfweek = "";
                object rawData;

                DataSet result = excelReader.AsDataSet();
                for (byte month = 0; month < 12; month++)
                {
                    CreateQMS_MA_OPERATION_SHIFT temp = new CreateQMS_MA_OPERATION_SHIFT();
                    listShift1 = new List<string>();
                    listShift2 = new List<string>();

                    temp.YEAR = year;
                    temp.MONTH = (byte)(month + 1);
                     
                    for (int i = 0; i < 31; i++)
                    {
                        dayOfweek = getWeekOfDay(year, (month + 1), (i + 1));

                        rawData = result.Tables[0].Rows[shiftRows1[month]][i + 2];
                        shiftName = (dayOfweek == "")? "" :rawData.ToString();
                        listShift1.Add(shiftName);
                         
                        rawData = result.Tables[0].Rows[shiftRows2[month]][i + 2];
                        shiftName = (dayOfweek == "") ? "" : rawData.ToString();
                        listShift2.Add(shiftName);
                    }                    
                    temp.SHIFT_1 = String.Join(",", listShift1.Select(x => x.ToString()).ToArray());
                    temp.SHIFT_2 = String.Join(",", listShift2.Select(x => x.ToString()).ToArray());
                    SaveQMS_MA_OPERATION_SHIFT(db, temp); //{3,2}
                }               
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            excelReader.Close();
            return nResult;
        }

        public List<QMS_MA_OPERATION_SHIFT> getAllByStartEndYear(QMSDBEntities db, int startYear, int endYear)
        {
            List<QMS_MA_OPERATION_SHIFT> listResult = new List<QMS_MA_OPERATION_SHIFT>();

            try
            {
                listResult = QMS_MA_OPERATION_SHIFT.GetAllByStartEndYear(db, startYear, endYear); ;
                 
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }
        
        public List<ShiftYear> getShiftYear(QMSDBEntities db)
        {
            List<ShiftYear> listResult = new List<ShiftYear>();

            try
            {
               List<short> listYear =  QMS_MA_OPERATION_SHIFT.GetAllGroupYear(db);

               foreach (short year in listYear)
               {
                   ShiftYear tempYear = new ShiftYear();
                   tempYear.YEAR = year;
                   tempYear.YEAR_NAME = year.ToString();

                   listResult.Add(tempYear);
               }
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        private CreateQMS_MA_OPERATION_SHIFT convertDBToModel(QMS_MA_OPERATION_SHIFT model)
        {
            CreateQMS_MA_OPERATION_SHIFT result = new CreateQMS_MA_OPERATION_SHIFT(); 

            result.ID = model.ID;
            result.YEAR = model.YEAR;
            result.MONTH = model.MONTH;
            result.SHIFT_1 = (null == model.SHIFT_1) ? "" : model.SHIFT_1;
            result.SHIFT_2 = (null == model.SHIFT_2) ? "" : model.SHIFT_2;

            return result;
        }

        public long SaveOperationShiftDetail(QMSDBEntities db, ColumnShiftData model)
        {
            long nResult = 0;

            try
            {
                QMS_MA_OPERATION_SHIFT dataShift =  QMS_MA_OPERATION_SHIFT.GetById(db, model.ID);
                String shiftOperation = "";
                List<String> listOperation = new List<String>();

                //ตรวจสอบ ว่า เป็น กะ อะไร
                if (model.SHIFT == (byte)OPERATION_SHIFT_TIME.TIME1)
                {
                    shiftOperation = dataShift.SHIFT_1;
                }
                else
                {
                    shiftOperation = dataShift.SHIFT_2;
                }

                listOperation = shiftOperation.Split(',').ToList<string>();

                //ตรวจสอบ edit วันที่ เท่าไหร่
                if (listOperation.Count() > 0)
                {
                    for (int i = 0; i < listOperation.Count(); i++)
                    {
                        if ((i+1) == model.DATE)
                        {
                            listOperation[i] = model.columnData; 
                            break;
                        }
                    } 
                }

                if (model.SHIFT == (byte)OPERATION_SHIFT_TIME.TIME1)
                {
                    dataShift.SHIFT_1 = String.Join(",", listOperation.Select(x => x.ToString()).ToArray());
                }
                else
                {
                    dataShift.SHIFT_2 = String.Join(",", listOperation.Select(x => x.ToString()).ToArray());
                }

                nResult = SaveQMS_MA_OPERATION_SHIFT(db, convertDBToModel(dataShift));

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return nResult;
        }

        private List<string> addShiftDatetime(List<string>listData, string shiftName)
        {
            bool bFound = false;
            try
            {
                if (listData.Count() > 0)
                {
                    foreach (string dtData in listData)
                    {
                        if (shiftName == dtData)
                        {
                            bFound = true; break;
                        }
                    }

                    if (false == bFound)
                    {
                        listData.Add(shiftName);
                    } 
                }
                else
                {
                    listData.Add(shiftName);
                }
            }
            catch
            {

            }

            return listData;
        }

        public string getShiftNameByDateTime(QMSDBEntities db, List<QMS_MA_OPERATION_SHIFT> listShiftData, DateTime START_DATE, DateTime END_DATE)
        {
            string shiftName = "";
            try
            {
                List<string> listShift = new List<string>();
                int startYear = START_DATE.Year; 
                int endYear = END_DATE.Year; 
                DateTime compareStartTime;
                DateTime compareEndTime;

                //DateTime firstDayInMonth;
                //DateTime lastDayInMonth;

                for (startYear = START_DATE.Year; startYear <= endYear; startYear++)
                {

                    if (null != listShiftData && listShiftData.Count() > 0)
                    {
                        foreach (QMS_MA_OPERATION_SHIFT dtData in listShiftData)
                        {

                            compareStartTime = new DateTime(dtData.YEAR, dtData.MONTH, 1, 0, 0, 0).AddHours(-4);
                            compareEndTime = new DateTime(dtData.YEAR, dtData.MONTH, 1, 0, 0, 0).AddMonths(1).AddMilliseconds(-1);

                            //firstDayInMonth = new DateTime(dtData.YEAR, dtData.MONTH, 1, 0, 0, 0);
                            //lastDayInMonth = firstDayInMonth.AddMonths(1).AddMilliseconds(-1);
                            //ตรวจสอบเพิ่มเติมต้องไม่ใช่ วัน สุดท้ายหรือวัน เริ่มต้น ของเดือนนั้นๆ

                            if (END_DATE < compareStartTime || START_DATE > compareEndTime )
                            {
                                //ไม่อยู่ในช่วงที่ สนใจ
                                continue;
                            }


                            List<string> listShift1 = dtData.SHIFT_1.Split(',').ToList<string>(); //08.00am-08.00pm 
                            List<string> listShift2 = dtData.SHIFT_2.Split(',').ToList<string>(); //08.00pm-08.00am 


                            for (int i = 0; i < listShift1.Count(); i++)
                            {
                                try
                                {
                                    compareStartTime = new DateTime(startYear, dtData.MONTH, i + 1, 0, 0, 0).AddHours(-4);
                                    compareEndTime = new DateTime(startYear, dtData.MONTH, i + 1, 0, 0, 0).AddHours(8);


                                    if ((START_DATE >= compareStartTime && START_DATE <= compareEndTime)  //case 1
                                         || (END_DATE >= compareStartTime && END_DATE <= compareEndTime)  //case 2
                                         || (START_DATE >= compareStartTime && END_DATE <= compareEndTime)  //case 3
                                        )
                                    {
                                        //in period
                                        listShift = addShiftDatetime(listShift, listShift1[i]);
                                    }

                                    compareStartTime = new DateTime(startYear, dtData.MONTH, i + 1, 0, 0, 1).AddHours(8);
                                    compareEndTime = new DateTime(startYear, dtData.MONTH, i + 1, 0, 0, 0).AddHours(20);

                                    if ((START_DATE >= compareStartTime && START_DATE <= compareEndTime)  //case 1
                                         || (END_DATE >= compareStartTime && END_DATE <= compareEndTime)  //case 2
                                         || (START_DATE >= compareStartTime && END_DATE <= compareEndTime)  //case 3
                                        )
                                    {
                                        //in period
                                        listShift = addShiftDatetime(listShift, listShift2[i]);
                                    }

                                }
                                catch
                                { // กรณี ที่ไม่สามารถ เปลี่ยนเป็น วันที่ ได้

                                }
                            }


                        }

                    } 
                    startYear += 1;
                }

                if (null != listShift && listShift.Count() > 0)
                {
                    foreach (string dtShiftName in listShift)
                    {
                        if (shiftName == "")
                        {
                            shiftName = dtShiftName;
                        }
                        else
                        {
                            shiftName += " , " + dtShiftName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return shiftName; 
        }

        public ShiftTableData getOperationShiftByYearId(QMSDBEntities db, int yearId)
        {
            ShiftTableData shiftTable = new ShiftTableData();
            shiftTable.tableData = new List<List<ColumnShiftData>>();
            List<List<ColumnShiftData>> matrixResult = new List<List<ColumnShiftData>>();
            List<ColumnShiftData> listResult = new List<ColumnShiftData>();
              
            try
            {
                List<QMS_MA_OPERATION_SHIFT> list = QMS_MA_OPERATION_SHIFT.GetAllByYear(db, yearId);
                string[] monthString = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November" , "December" };
                string weekOfDayString = ""; 

                if (null != list && list.Count() > 0)
                {
                    shiftTable.CREATE_USER = list[0].CREATE_USER;
                    shiftTable.UPDATE_USER = list[0].UPDATE_USER;
                    shiftTable.SHOW_UPDATE_DATE = list[0].UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
                    shiftTable.SHOW_CREATE_DATE = list[0].CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));


                    foreach (QMS_MA_OPERATION_SHIFT dtData in list)
                    {
                        List<ColumnShiftData> tempData1 = new List<ColumnShiftData>();
                        List<ColumnShiftData> tempData2 = new List<ColumnShiftData>();
                        List<ColumnShiftData> tempData3 = new List<ColumnShiftData>();
                        List<ColumnShiftData> tempData4 = new List<ColumnShiftData>();

                        List<string> listShift1 = dtData.SHIFT_1.Split(',').ToList<string>();
                        List<string> listShift2 = dtData.SHIFT_2.Split(',').ToList<string>();

                        //Add Row Name 
                        ColumnShiftData tempShiftData1 = new ColumnShiftData();
                        ColumnShiftData tempShiftData2 = new ColumnShiftData();
                        ColumnShiftData tempShiftData3 = new ColumnShiftData();
                        ColumnShiftData tempShiftData4 = new ColumnShiftData();

                        tempShiftData1.columnData = monthString[dtData.MONTH - 1];
                        tempShiftData2.columnData = Resource.ResourceString.DATE;
                        tempShiftData3.columnData = Resource.ResourceString.SHIFT1_TIME; 
                        tempShiftData4.columnData = Resource.ResourceString.SHIFT2_TIME;

                        tempShiftData1.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_NAME;
                        tempShiftData2.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_NAME;
                        tempShiftData3.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_NAME;
                        tempShiftData4.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_NAME;

                        tempData1.Add(tempShiftData1);
                        tempData2.Add(tempShiftData2);
                        tempData3.Add(tempShiftData3);
                        tempData4.Add(tempShiftData4);

                        for (int i = 0; i < 31; i++)
                        {
                            weekOfDayString = getWeekOfDay(yearId, dtData.MONTH, (i + 1));

                            tempShiftData1 = new ColumnShiftData();
                            tempShiftData2 = new ColumnShiftData();
                            tempShiftData3 = new ColumnShiftData();
                            tempShiftData4 = new ColumnShiftData();

                            tempShiftData1.columnData = (weekOfDayString == "")? "" : (i+1).ToString();
                            tempShiftData1.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_NAME;
                            tempShiftData1.DATE = (byte)(i + 1);
                            tempShiftData1.ID = dtData.ID;
                            tempShiftData1.MONTH = dtData.MONTH;
                            tempShiftData1.YEAR = yearId;

                            tempShiftData2.columnData = weekOfDayString;
                            tempShiftData2.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_NAME;
                            tempShiftData2.DATE = (byte)(i + 1);
                            tempShiftData2.ID = dtData.ID;
                            tempShiftData2.MONTH = dtData.MONTH;
                            tempShiftData2.YEAR = yearId;

                            tempShiftData3.columnData = (null == listShift1[i]) ? "" : listShift1[i];
                            tempShiftData3.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_DATA;
                            tempShiftData3.DATE = (byte)(i + 1);
                            tempShiftData3.ID = dtData.ID;
                            tempShiftData3.SHIFT = (byte) OPERATION_SHIFT_TIME.TIME1;
                            tempShiftData3.SHIFT_NAME = Resource.ResourceString.SHIFT1_TIME;
                            tempShiftData3.SHOW_DATE = getShowDate(yearId, dtData.MONTH, (i + 1));
                            tempShiftData3.MONTH = dtData.MONTH;
                            tempShiftData3.YEAR = yearId;

                            tempShiftData4.columnData = (null == listShift2[i]) ? "" : listShift2[i];
                            tempShiftData4.columnType = (byte)COLUMN_SHIFT_TYPE.COLUMN_DATA;
                            tempShiftData4.DATE = (byte)(i + 1);
                            tempShiftData4.ID = dtData.ID;
                            tempShiftData4.SHIFT = (byte)OPERATION_SHIFT_TIME.TIME2;
                            tempShiftData4.SHIFT_NAME = Resource.ResourceString.SHIFT1_TIME;
                            tempShiftData4.SHOW_DATE = getShowDate(yearId, dtData.MONTH, (i + 1));
                            tempShiftData4.MONTH = dtData.MONTH;
                            tempShiftData4.YEAR = yearId;


                            tempData1.Add(tempShiftData1);
                            tempData2.Add(tempShiftData2);
                            tempData3.Add(tempShiftData3);
                            tempData4.Add(tempShiftData4);
                        }


                        matrixResult.Add(tempData1);
                        matrixResult.Add(tempData2);
                        matrixResult.Add(tempData3);
                        matrixResult.Add(tempData4);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            shiftTable.tableData = matrixResult;
            return shiftTable;
        }

        public List<ViewQMS_MA_OPERATION_SHIFT> getQMS_MA_OPERATION_SHIFTList(QMSDBEntities db)
        {
            List<ViewQMS_MA_OPERATION_SHIFT> listResult = new List<ViewQMS_MA_OPERATION_SHIFT>();

            try
            {
                List<QMS_MA_OPERATION_SHIFT> list = QMS_MA_OPERATION_SHIFT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_OPERATION_SHIFT
                                  {
                                      ID = dt.ID,
                                      YEAR = dt.YEAR,
                                      MONTH = dt.MONTH,
                                      SHIFT_1 = dt.SHIFT_1,
                                      SHIFT_2 = dt.SHIFT_2,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_OPERATION_SHIFT convertModelToDB(CreateQMS_MA_OPERATION_SHIFT model)
        {
            QMS_MA_OPERATION_SHIFT result = new QMS_MA_OPERATION_SHIFT();

            result.ID = model.ID;
            result.YEAR = model.YEAR;
            result.MONTH = model.MONTH;
            result.SHIFT_1 = (null == model.SHIFT_1) ? "" : model.SHIFT_1;
            result.SHIFT_2 = (null == model.SHIFT_2) ? "" : model.SHIFT_2;
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;
    
            return result;
        }

        public bool DeleteQMS_MA_OPERATION_SHIFTById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_OPERATION_SHIFT result = new QMS_MA_OPERATION_SHIFT();
            result = QMS_MA_OPERATION_SHIFT.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_OPERATION_SHIFTByYear(QMSDBEntities db, int year)
        {
            bool bResult = false;
            List<QMS_MA_OPERATION_SHIFT> result = new List<QMS_MA_OPERATION_SHIFT>();
            result = QMS_MA_OPERATION_SHIFT.GetAllByYear(db, year);
            try
            {
                DateTime dtCurrent  = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }
         

        public long SaveQMS_MA_OPERATION_SHIFT(QMSDBEntities db, CreateQMS_MA_OPERATION_SHIFT model)
        {
            long result = 0;

            QMS_MA_OPERATION_SHIFT dt = QMS_MA_OPERATION_SHIFT.GetByMonthIdYearId(db, model.MONTH, model.YEAR);

            if(null != dt)  //(model.ID > 0)
            {
                model.ID = dt.ID;
                result = UpdateQMS_MA_OPERATION_SHIFT(db, model);
            }
            else
            {
                result = AddQMS_MA_OPERATION_SHIFT(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_OPERATION_SHIFT(QMSDBEntities db, CreateQMS_MA_OPERATION_SHIFT model)
        {
            long result = 0;

            try
            {
                QMS_MA_OPERATION_SHIFT dt = new QMS_MA_OPERATION_SHIFT();
                dt = QMS_MA_OPERATION_SHIFT.GetById(db, model.ID);

                dt.YEAR = model.YEAR;
                dt.MONTH = model.MONTH;
                dt.SHIFT_1 = (null == model.SHIFT_1) ? "" : model.SHIFT_1;
                dt.SHIFT_2 = (null == model.SHIFT_2) ? "" : model.SHIFT_2;
                dt.DELETE_FLAG = model.DELETE_FLAG;
    

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_OPERATION_SHIFT(QMSDBEntities _db, QMS_MA_OPERATION_SHIFT model)
        {
            long result = 0;
            try
            {
                QMS_MA_OPERATION_SHIFT dt = new QMS_MA_OPERATION_SHIFT();
                dt = QMS_MA_OPERATION_SHIFT.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_OPERATION_SHIFT.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }


        #endregion

        #region QMS_MA_PLANT
        public List<ViewQMS_MA_PLANT> getQMS_MA_PLANTList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PLANT> listResult = new List<ViewQMS_MA_PLANT>();

            try
            {
                List<QMS_MA_PLANT> list = QMS_MA_PLANT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PLANT
                                  {
                                      ID = dt.ID,
                                      NAME= dt.NAME,
                                      ABBREVIATION = dt.ABBREVIATION,
                                      POSITION = dt.POSITION,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_PLANT> setQMS_MA_PLANTPostion(QMSDBEntities db, SetPlantPosition model)
        {
            try
            {
                List<QMS_MA_PLANT> list = QMS_MA_PLANT.GetActiveAll(db);

                QMS_MA_PLANT LastId = new QMS_MA_PLANT();
                QMS_MA_PLANT CurrentId = new QMS_MA_PLANT();
                QMS_MA_PLANT NextId = new QMS_MA_PLANT();

                CreateQMS_MA_PLANT currentData = new CreateQMS_MA_PLANT();
                CreateQMS_MA_PLANT swapData = new CreateQMS_MA_PLANT();

                bool ExitLoop = false;

                list = list.OrderBy(m => m.POSITION).ToList();
                
                foreach (QMS_MA_PLANT dt in list)
                {
                    NextId = dt; // get ค่า ตัวต่อไป

                    if (true == ExitLoop)
                    {
                        break;
                    }

                    if (model.PLANT_ID == dt.ID) // get ค่า ปัจจุบัน
                    {
                        CurrentId = dt;
                        ExitLoop = true;
                    }
                    else
                    { 
                        LastId = dt; // get ค่า สุดท้าย
                    }
                }

                if (model.DIRECTION == (int)ARROW_STATUS.ARROW_UP)
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.NAME = CurrentId.NAME;
                    currentData.ABBREVIATION = CurrentId.ABBREVIATION;
                    currentData.POSITION = LastId.POSITION; //สลับตำแหน่งกับค่า บน


                    swapData.ID = LastId.ID;
                    swapData.NAME = LastId.NAME;
                    swapData.ABBREVIATION = LastId.ABBREVIATION;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 
                     
                }
                else
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.NAME = CurrentId.NAME;
                    currentData.ABBREVIATION = CurrentId.ABBREVIATION;
                    currentData.POSITION = NextId.POSITION; //สลับตำแหน่งกับค่า ล่าง


                    swapData.ID = NextId.ID;
                    swapData.NAME = NextId.NAME;
                    swapData.ABBREVIATION = NextId.ABBREVIATION;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 
                     
                }

                UpdateQMS_MA_PLANT(db, currentData);
                UpdateQMS_MA_PLANT(db, swapData); 

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return getQMS_MA_PLANTActiveList(db);
        }

        public List<ViewQMS_MA_PLANT> getQMS_MA_PLANTActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PLANT> listResult = new List<ViewQMS_MA_PLANT>();

            try
            {
                List<QMS_MA_PLANT> list = QMS_MA_PLANT.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PLANT
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      ABBREVIATION = dt.ABBREVIATION,
                                      POSITION = dt.POSITION,
                                      ARROW_UP = true,
                                      ARROW_DOWN = true
                                  }).ToList();

                    listResult = listResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                    listResult = setArrowUPAndDown(listResult);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_PLANT convertModelToDB(CreateQMS_MA_PLANT model)
        {
            QMS_MA_PLANT result = new QMS_MA_PLANT();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.POSITION = model.POSITION;
            result.ABBREVIATION = model.ABBREVIATION;
            //result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_PLANTById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_PLANT result = new QMS_MA_PLANT();
            result = QMS_MA_PLANT.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_PLANTByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_PLANT> result = new List<QMS_MA_PLANT>();
            result = QMS_MA_PLANT.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long CheckDuplicateName(QMSDBEntities db, CreateQMS_MA_PLANT model)
        {
            long result = 0;

            try
            {
                QMS_MA_PLANT duplicate = new QMS_MA_PLANT();

                if (model.ID > 0)
                {
                    duplicate = QMS_MA_PLANT.GetByPlantNameAndSkipPlantId(db, model.NAME, model.ID);
                }
                else
                {
                    duplicate = QMS_MA_PLANT.GetByPlantName(db, model.NAME);
                }


                if (null != duplicate && duplicate.ID > 0)
                {
                    result = -1; // Error
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveQMS_MA_PLANT(QMSDBEntities db, CreateQMS_MA_PLANT model)
        {
            long result = 0;

            result = CheckDuplicateName(db, model);
            if (0 == result)
            {

                if (model.ID > 0)
                {
                    result = UpdateQMS_MA_PLANT(db, model);
                }
                else
                {
                    result = AddQMS_MA_PLANT(db, convertModelToDB(model));
                }
            }

            return result;
        }

        private short getMaxPlantPostion(QMSDBEntities db)
        {
            short bResult = 1;

            try
            {
                var list = QMS_MA_PLANT.GetAll(db);

                if (list.Count() > 0)
                {
                    short max = list.Max(m => m.POSITION);

                    if (max < 32767)
                        bResult += max;
                    else
                        bResult = 1;
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }


            return bResult;
        }

        public long UpdateQMS_MA_PLANT(QMSDBEntities db, CreateQMS_MA_PLANT model)
        {
            long result = 0;

            try
            {
                QMS_MA_PLANT dt = new QMS_MA_PLANT();
                dt = QMS_MA_PLANT.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.POSITION = model.POSITION;
                dt.ABBREVIATION = model.ABBREVIATION;
                //dt.DELETE_FLAG = model.DELETE_FLAG;


                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_PLANT(QMSDBEntities _db, QMS_MA_PLANT model)
        {
            long result = 0;
            try
            {
                QMS_MA_PLANT dt = new QMS_MA_PLANT();
                dt = QMS_MA_PLANT.GetById(_db, model.ID);

                if (null == dt)
                {
                    model.POSITION = this.getMaxPlantPostion(_db); //model.POSITION; //หาค่าสูงสุดสร้างใหม่จะไปต่อท้าย
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_PLANT.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_PRODUCT
        public List<ViewQMS_MA_PRODUCT> getQMS_MA_PRODUCTList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PRODUCT> listResult = new List<ViewQMS_MA_PRODUCT>();

            try
            {
                List<QMS_MA_PRODUCT> list = QMS_MA_PRODUCT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PRODUCT
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      POSITION = dt.POSITION,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }
        
        public List<ViewQMS_MA_PRODUCT> setQMS_MA_PRODUCTPostion(QMSDBEntities db, SetProductPosition model)
        {
            List<ViewQMS_MA_PRODUCT> listResult = new List<ViewQMS_MA_PRODUCT>();

            try
            {
                List<QMS_MA_PRODUCT> list = QMS_MA_PRODUCT.GetActiveAll(db);

                QMS_MA_PRODUCT LastId = new QMS_MA_PRODUCT();
                QMS_MA_PRODUCT CurrentId = new QMS_MA_PRODUCT();
                QMS_MA_PRODUCT NextId = new QMS_MA_PRODUCT();

                CreateQMS_MA_PRODUCT currentData = new CreateQMS_MA_PRODUCT();
                CreateQMS_MA_PRODUCT swapData = new CreateQMS_MA_PRODUCT();

                bool ExitLoop = false;

                list = list.OrderBy(m => m.POSITION).ToList();

                foreach (QMS_MA_PRODUCT dt in list)
                {
                    NextId = dt; // get ค่า ตัวต่อไป

                    if (true == ExitLoop)
                    {
                        break;
                    }

                    if (model.PRODUCT_ID == dt.ID) // get ค่า ปัจจุบัน
                    {
                        CurrentId = dt;
                        ExitLoop = true;
                    }
                    else
                    {
                        LastId = dt; // get ค่า สุดท้าย
                    }
                }

                if (model.DIRECTION == (int)ARROW_STATUS.ARROW_UP)
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.NAME = CurrentId.NAME;
                    currentData.POSITION = LastId.POSITION; //สลับตำแหน่งกับค่า บน


                    swapData.ID = LastId.ID;
                    swapData.NAME = LastId.NAME;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                }
                else
                {
                    short priority = CurrentId.POSITION;

                    currentData.ID = CurrentId.ID;
                    currentData.NAME = CurrentId.NAME;
                    currentData.POSITION = NextId.POSITION; //สลับตำแหน่งกับค่า ล่าง


                    swapData.ID = NextId.ID;
                    swapData.NAME = NextId.NAME;
                    swapData.POSITION = priority; //สลับตำแหน่งกับค่าปัจจุบัน 

                }

                UpdateQMS_MA_PRODUCT(db, currentData);
                UpdateQMS_MA_PRODUCT(db, swapData); 
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return getQMS_MA_PRODUCTActiveList(db);
        }

        public List<ViewQMS_MA_PRODUCT> getQMS_MA_PRODUCTActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PRODUCT> listResult = new List<ViewQMS_MA_PRODUCT>();

            try
            {
                List<QMS_MA_PRODUCT> list = QMS_MA_PRODUCT.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PRODUCT
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME, 
                                      POSITION = dt.POSITION,
                                      ARROW_DOWN = true,
                                      ARROW_UP = true
                                      
                                  }).ToList();
                    listResult = listResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                    listResult = setArrowUPAndDown(listResult);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }
               

        private QMS_MA_PRODUCT convertModelToDB(CreateQMS_MA_PRODUCT model)
        {
            QMS_MA_PRODUCT result = new QMS_MA_PRODUCT();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.POSITION = model.POSITION;
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_PRODUCTById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_PRODUCT result = new QMS_MA_PRODUCT();
            result = QMS_MA_PRODUCT.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_PRODUCTByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_PRODUCT> result = new List<QMS_MA_PRODUCT>();
            result = QMS_MA_PRODUCT.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        private short getMaxProductPostion(QMSDBEntities db)
        {
            short bResult = 1;

            try
            {
                var list = QMS_MA_PRODUCT.GetAll(db);

                if (list.Count() > 0)
                {
                    short max = list.Max(m => m.POSITION);

                    if (max < 32767)
                        bResult += max;
                    else
                        bResult = 1;
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }


            return bResult;
        }

        public long CheckDuplicateName(QMSDBEntities db, CreateQMS_MA_PRODUCT model)
        {
            long result = 0;

            try { 
                QMS_MA_PRODUCT duplicate = new QMS_MA_PRODUCT();

                if (model.ID > 0)
                {
                    duplicate = QMS_MA_PRODUCT.GetByProductNameAndSkipProductId(db, model.NAME, model.ID);               
                }
                else
                {
                    duplicate = QMS_MA_PRODUCT.GetByProductName(db, model.NAME); 
                }


                if (null != duplicate && duplicate.ID > 0)
                {
                    result = -1; // Error
                }
            } 
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveQMS_MA_PRODUCT(QMSDBEntities db, CreateQMS_MA_PRODUCT model)
        {
            long result = 0;

            result = CheckDuplicateName(db, model);

            if (0 == result)
            {
                if (model.ID > 0)
                {
                    result = UpdateQMS_MA_PRODUCT(db, model);
                }
                else
                {
                    result = AddQMS_MA_PRODUCT(db, convertModelToDB(model));
                }
            }

            return result;
        }

        public long UpdateQMS_MA_PRODUCT(QMSDBEntities db, CreateQMS_MA_PRODUCT model)
        {
            long result = 0;

            try
            {
                QMS_MA_PRODUCT dt = new QMS_MA_PRODUCT();
                dt = QMS_MA_PRODUCT.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.POSITION = model.POSITION;
                //dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_PRODUCT(QMSDBEntities _db, QMS_MA_PRODUCT model)
        {
            long result = 0;
            try
            {
                QMS_MA_PRODUCT dt = new QMS_MA_PRODUCT();
                dt = QMS_MA_PRODUCT.GetById(_db, model.ID);

                if (null == dt)
                {
                     model.POSITION = this.getMaxProductPostion(_db); //model.POSITION; //หาค่าสูงสุดสร้างใหม่จะไปต่อท้าย
                    _db.QMS_MA_PRODUCT.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_PRODUCT> GetProductListByNotinId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_PRODUCT> listResult = new List<ViewQMS_MA_PRODUCT>();

            try
            {
                List<QMS_MA_PRODUCT> list = QMS_MA_PRODUCT.GetAllByNotinProductId(db, id);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_PRODUCT
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      POSITION = dt.POSITION,
                                      ARROW_DOWN = true,
                                      ARROW_UP = true

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        #endregion

        #region QMS_MA_PRODUCT_MAPPING

        public List<ViewQMS_MA_PRODUCT_MAPPING> convertListData(QMSDBEntities db, List<QMS_MA_PRODUCT_MAPPING> list)
        { 
            List<ViewQMS_MA_PRODUCT_MAPPING> listResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            List<QMS_MA_GRADE> listGrade = QMS_MA_GRADE.GetAll(db);

            try
            {
                if (null != list && list.Count() > 0)
                {

                    listResult = (from dt in list
                                  select new ViewQMS_MA_PRODUCT_MAPPING
                                  {
                                      ID = dt.ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PRODUCT_ID = dt.PRODUCT_ID,
                                      GRADE_ID = dt.GRADE_ID,
                                      PRODUCT_DESC = dt.PRODUCT_DESC,

                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      GRADE_NAME = getGradeName(listGrade, dt.GRADE_ID),
                                      PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID),
                                      HIGHLOW_CHECK = (1 == dt.HIGHLOW_CHECK) ? true : false,

                                      CREATE_DATE = dt.CREATE_DATE,
                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      UPDATE_DATE = dt.UPDATE_DATE,

                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        } 

        public CreateQMS_MA_PRODUCT_MAPPING getcreateQMS_MA_PRODUCT_MAPPINGById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_PRODUCT_MAPPING objResult = new CreateQMS_MA_PRODUCT_MAPPING();

            try
            {
                QMS_MA_PRODUCT_MAPPING dt = new QMS_MA_PRODUCT_MAPPING();
                dt = QMS_MA_PRODUCT_MAPPING.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_PRODUCT_MAPPING> getQMS_MA_PRODUCT_MAPPINGList(QMSDBEntities db)
        {
            List<ViewQMS_MA_PRODUCT_MAPPING> listResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();

            try
            {
                List<QMS_MA_PRODUCT_MAPPING> list = QMS_MA_PRODUCT_MAPPING.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_PRODUCT_MAPPING> getQMS_MA_PRODUCT_MAPPINGListByPlantId(QMSDBEntities db, long plantId)
        {
            List<ViewQMS_MA_PRODUCT_MAPPING> listResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();

            try
            {
                List<QMS_MA_PRODUCT_MAPPING> list = QMS_MA_PRODUCT_MAPPING.GetByListPlantId(db, plantId); 

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }
         
        public List<ViewQMS_MA_PRODUCT_MAPPING> searchCreateQMS_MA_PRODUCT_MAPPING(QMSDBEntities db, PlantProductSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_PRODUCT_MAPPING> objResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();
             
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();
            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
                List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
                List<QMS_MA_GRADE> listGrade = QMS_MA_GRADE.GetAll(db);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_PRODUCT_MAPPING> listData = new List<QMS_MA_PRODUCT_MAPPING>();
                listData = QMS_MA_PRODUCT_MAPPING.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    IQueryable<QMS_MA_PRODUCT_MAPPING> query = grid.LoadGridData<QMS_MA_PRODUCT_MAPPING>(listData.AsQueryable(), out count, out totalPage);
                    objResult = convertListData(db, query.ToList()); 
                   
                    listPageIndex = getPageIndexList(totalPage );
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private QMS_MA_PRODUCT_MAPPING convertModelToDB(CreateQMS_MA_PRODUCT_MAPPING model)
        {
            QMS_MA_PRODUCT_MAPPING result = new QMS_MA_PRODUCT_MAPPING();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.GRADE_ID = model.GRADE_ID;
            result.PRODUCT_DESC = (null == model.PRODUCT_DESC) ? "" : model.PRODUCT_DESC;
            result.HIGHLOW_CHECK = (true == model.HIGHLOW_CHECK) ? (byte) 1: (byte) 0;
            //result.DELETE_FLAG = model.DELETE_FLAG;
            
            
            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_PRODUCT_MAPPING convertDBToModel(QMS_MA_PRODUCT_MAPPING model)
        {
            CreateQMS_MA_PRODUCT_MAPPING result = new CreateQMS_MA_PRODUCT_MAPPING();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.GRADE_ID = model.GRADE_ID;
            result.PRODUCT_DESC = (null == model.PRODUCT_DESC) ? "" : model.PRODUCT_DESC;
            //result.DELETE_FLAG = model.DELETE_FLAG;
            result.HIGHLOW_CHECK = (1 == model.HIGHLOW_CHECK) ? true : false;

            result.CREATE_DATE = model.CREATE_DATE;
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_DATE = model.UPDATE_DATE;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.UPDATE_USER = model.UPDATE_USER;

            return result;
        }
         
        public bool DeleteQMS_MA_PRODUCT_MAPPINGById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_PRODUCT_MAPPING result = new QMS_MA_PRODUCT_MAPPING();
            result = QMS_MA_PRODUCT_MAPPING.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_PRODUCT_MAPPINGByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_PRODUCT_MAPPING> result = new List<QMS_MA_PRODUCT_MAPPING>();
            result = QMS_MA_PRODUCT_MAPPING.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public QMS_MA_PRODUCT_MAPPING getPlantIdProductId(QMSDBEntities db, long PlantId, long ProductId)
        {
            QMS_MA_PRODUCT_MAPPING dtData = new QMS_MA_PRODUCT_MAPPING();

            try
            {
                dtData = QMS_MA_PRODUCT_MAPPING.GetByPlantIdProductId(db, PlantId, ProductId); 
            }
            catch
            {

            }

            return dtData;
        }

        public ViewQMS_MA_PRODUCT_MAPPING getViewPlantIdProductId(QMSDBEntities db, long PlantId, long ProductId)
        {
            ViewQMS_MA_PRODUCT_MAPPING dtData = new ViewQMS_MA_PRODUCT_MAPPING(); 
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            List<QMS_MA_PRODUCT> listProduct = QMS_MA_PRODUCT.GetAll(db);
            List<QMS_MA_GRADE> listGrade = QMS_MA_GRADE.GetAll(db);
            try
            {
                QMS_MA_PRODUCT_MAPPING dt = QMS_MA_PRODUCT_MAPPING.GetByPlantIdProductId(db, PlantId, ProductId);

                dtData.ID = dt.ID;
                dtData.PLANT_ID = dt.PLANT_ID;
                dtData.PRODUCT_ID = dt.PRODUCT_ID;
                dtData.GRADE_ID = dt.GRADE_ID;
                dtData.PRODUCT_DESC = dt.PRODUCT_DESC;

                dtData.PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID);
                dtData.GRADE_NAME = getGradeName(listGrade, dt.GRADE_ID);
                dtData.PRODUCT_NAME = getProductName(listProduct, dt.PRODUCT_ID);
                dtData.HIGHLOW_CHECK = (1 == dt.HIGHLOW_CHECK) ? true : false;

                dtData.CREATE_DATE = dt.CREATE_DATE;
                dtData.CREATE_USER = dt.CREATE_USER;
                dtData.UPDATE_USER = dt.UPDATE_USER;
                dtData.UPDATE_DATE = dt.UPDATE_DATE;
            }
            catch
            {

            }

            return dtData;
        }

        public long CheckDuplicateName(QMSDBEntities db, CreateQMS_MA_PRODUCT_MAPPING model)
        {
            long result = 0;

            try
            {
                QMS_MA_PRODUCT_MAPPING duplicate = new QMS_MA_PRODUCT_MAPPING();

                if (model.ID > 0)
                {
                    duplicate = QMS_MA_PRODUCT_MAPPING.GetByPlantIdProductIdSkipId(db, model.PLANT_ID, model.PRODUCT_ID, model.ID);
                }
                else
                {
                    duplicate = QMS_MA_PRODUCT_MAPPING.GetByPlantIdProductId(db, model.PLANT_ID, model.PRODUCT_ID);
                }


                if (null != duplicate && duplicate.ID > 0)
                {
                    result = -1; // Error
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long SaveQMS_MA_PRODUCT_MAPPING(QMSDBEntities db, CreateQMS_MA_PRODUCT_MAPPING model)
        {
            long result = 0;

            result = CheckDuplicateName(db, model);

            if (0 == result)
            {
                if (model.ID > 0)
                {
                    result = UpdateQMS_MA_PRODUCT_MAPPING(db, model);
                }
                else
                {
                    result = AddQMS_MA_PRODUCT_MAPPING(db, convertModelToDB(model));
                }
            }

            return result;
        }

        public long UpdateQMS_MA_PRODUCT_MAPPING(QMSDBEntities db, CreateQMS_MA_PRODUCT_MAPPING model)
        {
            long result = 0;

            try
            {
                QMS_MA_PRODUCT_MAPPING dt = new QMS_MA_PRODUCT_MAPPING();
                dt = QMS_MA_PRODUCT_MAPPING.GetById(db, model.ID);

                dt.PLANT_ID = model.PLANT_ID;
                dt.PRODUCT_ID = model.PRODUCT_ID;
                dt.GRADE_ID = model.GRADE_ID;
                dt.PRODUCT_DESC = (null == model.PRODUCT_DESC) ? "" : model.PRODUCT_DESC;
                dt.HIGHLOW_CHECK = (true == model.HIGHLOW_CHECK) ? (byte)1 : (byte)0;
                //dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_PRODUCT_MAPPING(QMSDBEntities _db, QMS_MA_PRODUCT_MAPPING model)
        {
            long result = 0;
            try
            {
                QMS_MA_PRODUCT_MAPPING dt = new QMS_MA_PRODUCT_MAPPING();
                dt = QMS_MA_PRODUCT_MAPPING.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_PRODUCT_MAPPING.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_REDUCE_FEED
        public List<ViewQMS_MA_REDUCE_FEED> searchQMS_MA_REDUCE_FEED(QMSDBEntities db, ReduceFeedSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_REDUCE_FEED> objResult = new List<ViewQMS_MA_REDUCE_FEED>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_REDUCE_FEED> listData = new List<QMS_MA_REDUCE_FEED>();
                listData = QMS_MA_REDUCE_FEED.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);

                List<QMS_MA_REDUCE_FEED_DETAIL>  listDataDetail = QMS_MA_REDUCE_FEED_DETAIL.GetAll(db);
                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_REDUCE_FEED
                                 {

                //if (listData != null)
                //{
                //    objResult = (from dt in listData
                //                 join rd in listDataDetail on dt.ID equals rd.REDUCE_FEED_ID into group1
                //                 from g1 in group1.DefaultIfEmpty() 
                //                 select new ViewQMS_MA_REDUCE_FEED
                //                 {
             
                                     ID = dt.ID,
                                     PLANT_ID = dt.PLANT_ID,
                                     PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                     EXCEL_NAME = dt.EXCEL_NAME,
                                     LIMIT_VALUE = dt.LIMIT_VALUE,
                                     UNIT_ID = dt.UNIT_ID,
                                     //EXA_TAG_NAME = (null == g1)? "" : g1.EXA_TAG_NAME,// joint with QMS_MA_REDUCE_FEED_DETAIL
                                     EXA_TAG_NAME = getEXA_TAG_NAME(listDataDetail, dt.ID),
                                     ACTIVE_DATE = dt.ACTIVE_DATE, 
                                     REDUCE_FEED_DESC = dt.REDUCE_FEED_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    if (null != searchModel.mSearch.EXA_TAG_NAME && "" != searchModel.mSearch.EXA_TAG_NAME)
                    {
                        objResult = objResult.Where(m => m.EXA_TAG_NAME.Contains(searchModel.mSearch.EXA_TAG_NAME)).ToList();
                    }

                    objResult = grid.LoadGridData<ViewQMS_MA_REDUCE_FEED>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_REDUCE_FEED getQMS_MA_REDUCE_FEEDById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_REDUCE_FEED objResult = new CreateQMS_MA_REDUCE_FEED();

            try
            {
                QMS_MA_REDUCE_FEED dt = new QMS_MA_REDUCE_FEED();
                dt = QMS_MA_REDUCE_FEED.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private List<ViewQMS_MA_REDUCE_FEED> convertListData(QMSDBEntities db, List<QMS_MA_REDUCE_FEED> list)
        {
            List<ViewQMS_MA_REDUCE_FEED> listResult = new List<ViewQMS_MA_REDUCE_FEED>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_REDUCE_FEED
                                  {
                                      ID = dt.ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      EXCEL_NAME = dt.EXCEL_NAME,
                                      LIMIT_VALUE = dt.LIMIT_VALUE,
                                      UNIT_ID = dt.UNIT_ID,
                                      //EXA_TAG_NAME = dt.EXA_TAG_NAME,
                                      ACTIVE_DATE = dt.ACTIVE_DATE,
                                      REDUCE_FEED_DESC = dt.REDUCE_FEED_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<ViewQMS_MA_REDUCE_FEED> getQMS_MA_REDUCE_FEEDList(QMSDBEntities db)
        {
            List<ViewQMS_MA_REDUCE_FEED> listResult = new List<ViewQMS_MA_REDUCE_FEED>(); 
            try
            {
                List<QMS_MA_REDUCE_FEED> list = QMS_MA_REDUCE_FEED.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_REDUCE_FEED> getQMS_MA_REDUCE_FEEDListByPlantId(QMSDBEntities db, long plantId)
        {
            List<ViewQMS_MA_REDUCE_FEED> listResult = new List<ViewQMS_MA_REDUCE_FEED>();
            try
            {
                List<QMS_MA_REDUCE_FEED> list = QMS_MA_REDUCE_FEED.GetByListPlantId(db, plantId);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_REDUCE_FEED convertModelToDB(CreateQMS_MA_REDUCE_FEED model)
        {
            QMS_MA_REDUCE_FEED result = new QMS_MA_REDUCE_FEED();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
            result.LIMIT_VALUE = model.LIMIT_VALUE;                                 
            result.UNIT_ID = model.UNIT_ID;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.REDUCE_FEED_DESC = (null == model.REDUCE_FEED_DESC) ? "" : model.REDUCE_FEED_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_REDUCE_FEED convertDBToModel(QMSDBEntities db, QMS_MA_REDUCE_FEED model)
        {
            CreateQMS_MA_REDUCE_FEED result = new CreateQMS_MA_REDUCE_FEED();
            List<QMS_MA_REDUCE_FEED_DETAIL> listDetail = QMS_MA_REDUCE_FEED_DETAIL.GetActiveAllByREDUCE_FEED_ID(db, model.ID);
              
            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
            result.LIMIT_VALUE = model.LIMIT_VALUE;
            result.UNIT_ID = model.UNIT_ID;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.REDUCE_FEED_DESC = (null == model.REDUCE_FEED_DESC) ? "" : model.REDUCE_FEED_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));

            if (null != listDetail && listDetail.Count() > 0)
            {
                result.REDUCE_FEED_DETAIL_ID = listDetail[0].ID;
                result.CONVERT_VALUE = listDetail[0].CONVERT_VALUE;
                result.EXA_TAG_NAME = listDetail[0].EXA_TAG_NAME;
            }
            else
            {
                result.REDUCE_FEED_DETAIL_ID = 0;
            }

             

            return result;
        }

        public bool DeleteQMS_MA_REDUCE_FEEDById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_REDUCE_FEED result = new QMS_MA_REDUCE_FEED();
            result = QMS_MA_REDUCE_FEED.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_REDUCE_FEEDByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_REDUCE_FEED> result = new List<QMS_MA_REDUCE_FEED>();
            result = QMS_MA_REDUCE_FEED.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_REDUCE_FEED(QMSDBEntities db, CreateQMS_MA_REDUCE_FEED model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_REDUCE_FEED(db, model);
            }
            else
            {
                result = AddQMS_MA_REDUCE_FEED(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_REDUCE_FEED(QMSDBEntities db, CreateQMS_MA_REDUCE_FEED model)
        {
            long result = 0;

            try
            {
                QMS_MA_REDUCE_FEED dt = new QMS_MA_REDUCE_FEED();
                dt = QMS_MA_REDUCE_FEED.GetById(db, model.ID);

                dt.PLANT_ID = model.PLANT_ID;
                dt.EXCEL_NAME = (null == model.EXCEL_NAME) ? "" : model.EXCEL_NAME;
                dt.LIMIT_VALUE = model.LIMIT_VALUE;
                dt.UNIT_ID = model.UNIT_ID;
                dt.ACTIVE_DATE = model.ACTIVE_DATE;
                dt.REDUCE_FEED_DESC = (null == model.REDUCE_FEED_DESC) ? "" : model.REDUCE_FEED_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_REDUCE_FEED(QMSDBEntities _db, QMS_MA_REDUCE_FEED model)
        {
            long result = 0;
            try
            {
                QMS_MA_REDUCE_FEED dt = new QMS_MA_REDUCE_FEED();
                dt = QMS_MA_REDUCE_FEED.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_REDUCE_FEED.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

       

        #endregion

        #region QMS_MA_REDUCE_FEED_DETAIL

        private List<ViewQMS_MA_REDUCE_FEED_DETAIL> convertListData(List<QMS_MA_REDUCE_FEED_DETAIL> list)
        {
            List<ViewQMS_MA_REDUCE_FEED_DETAIL> listResult = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>(); 
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_REDUCE_FEED_DETAIL
                                  {
                                      ID = dt.ID, 
                                      REDUCE_FEED_ID = dt.REDUCE_FEED_ID,
                                      EXA_TAG_NAME = dt.EXA_TAG_NAME,
                                      CONVERT_VALUE = dt.CONVERT_VALUE,
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public decimal getQMS_MA_REDUCE_FEED_LimitByPlantAndActiveDate(QMSDBEntities db, long plantId, DateTime activeDate)
        {
            decimal nResult = 0;
            List<QMS_MA_REDUCE_FEED_DETAIL> listTemp = new List<QMS_MA_REDUCE_FEED_DETAIL>();
            QMS_MA_REDUCE_FEED activeData = new QMS_MA_REDUCE_FEED();
            try
            {
                //Get All Reduce feed by plantId
                List<QMS_MA_REDUCE_FEED> listReduce = QMS_MA_REDUCE_FEED.GetByListPlantId(db, plantId);

                if (listReduce.Count() > 0) //ถ้ามี list reduce feed แสดงว่า plant นี้มีการตรวจสอบ
                {
                    if (listReduce.Count() == 1)//มีข้อมูล เพียงชุดเดียว
                    {
                        activeData = listReduce.FirstOrDefault();
                    }
                    else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                    {
                        int flagData = 0;
                        listReduce = listReduce.OrderBy(m => m.ACTIVE_DATE).ToList(); //เลียงจาก น้อยไปมาก
                        foreach (QMS_MA_REDUCE_FEED dt in listReduce)
                        {

                            if (activeDate > dt.ACTIVE_DATE)
                            {
                                if (flagData == 0) { activeData = dt; } else { flagData = 1; } //set อย่างน้อย 1 ตัว
                                break;
                            }
                            activeData = dt;
                        }
                    }

                    nResult = activeData.LIMIT_VALUE;
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return nResult;
        }
         
        public List<ViewQMS_MA_REDUCE_FEED_DETAIL> getQMS_MA_REDUCE_FEED_DETAILActiveListByPlantAndActiveDate(QMSDBEntities db, long plantId, DateTime activeDate)
        {
            List<ViewQMS_MA_REDUCE_FEED_DETAIL> listResult = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();
            List<QMS_MA_REDUCE_FEED_DETAIL> listTemp = new List<QMS_MA_REDUCE_FEED_DETAIL>();
            QMS_MA_REDUCE_FEED activeData = new QMS_MA_REDUCE_FEED();
            try
            {
                //Get All Reduce feed by plantId
                List<QMS_MA_REDUCE_FEED> listReduce = QMS_MA_REDUCE_FEED.GetByListPlantId(db, plantId);

                if (listReduce.Count() > 0) //ถ้ามี list reduce feed แสดงว่า plant นี้มีการตรวจสอบ
                {
                    if (listReduce.Count() == 1)//มีข้อมูล เพียงชุดเดียว
                    {
                        activeData = listReduce.FirstOrDefault(); 
                    }
                    else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                    {
                        int flagData = 0;
                        listReduce = listReduce.OrderBy(m => m.ACTIVE_DATE).ToList(); //เลียงจาก น้อยไปมาก
                        foreach(QMS_MA_REDUCE_FEED dt in listReduce){
                           
                            if (activeDate > dt.ACTIVE_DATE)
                            {
                                if (flagData == 0) { activeData = dt; } else { flagData = 1; } //set อย่างน้อย 1 ตัว
                                break;
                            }
                            activeData = dt;
                        }
                    }

                    listTemp = QMS_MA_REDUCE_FEED_DETAIL.GetActiveAllByREDUCE_FEED_ID(db, activeData.ID);
                    listResult = convertListData(listTemp);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_REDUCE_FEED_DETAIL> getQMS_MA_REDUCE_FEED_DETAILActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_REDUCE_FEED_DETAIL> listResult = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();

            try
            {
                List<QMS_MA_REDUCE_FEED_DETAIL> list = QMS_MA_REDUCE_FEED_DETAIL.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_REDUCE_FEED_DETAIL> searchQMS_MA_REDUCE_FEED_DETAIL(QMSDBEntities db, ReduceFeedDetailSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_REDUCE_FEED_DETAIL> objResult = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "REDUCE_FEED_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_REDUCE_FEED_DETAIL> listData = new List<QMS_MA_REDUCE_FEED_DETAIL>();
                listData = QMS_MA_REDUCE_FEED_DETAIL.GetAllBySearch(db, searchModel.mSearch);
 
                if (listData != null)
                {
                    objResult = convertListData(listData);
                    objResult = grid.LoadGridData<ViewQMS_MA_REDUCE_FEED_DETAIL>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }


            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_REDUCE_FEED_DETAIL getQMS_MA_REDUCE_FEED_DETAILById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_REDUCE_FEED_DETAIL objResult = new CreateQMS_MA_REDUCE_FEED_DETAIL();

            try
            {
                QMS_MA_REDUCE_FEED_DETAIL dt = new QMS_MA_REDUCE_FEED_DETAIL();
                dt = QMS_MA_REDUCE_FEED_DETAIL.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_REDUCE_FEED_DETAIL> getQMS_MA_REDUCE_FEED_DETAILList(QMSDBEntities db)
        {
            List<ViewQMS_MA_REDUCE_FEED_DETAIL> listResult = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();
 
            try
            {
                List<QMS_MA_REDUCE_FEED_DETAIL> list = QMS_MA_REDUCE_FEED_DETAIL.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_REDUCE_FEED_DETAIL convertModelToDB(CreateQMS_MA_REDUCE_FEED_DETAIL model)
        {
            QMS_MA_REDUCE_FEED_DETAIL result = new QMS_MA_REDUCE_FEED_DETAIL();

            result.ID = model.ID;
            result.REDUCE_FEED_ID = model.REDUCE_FEED_ID;
            result.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
            result.CONVERT_VALUE = model.CONVERT_VALUE;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_REDUCE_FEED_DETAIL convertDBToModel(QMS_MA_REDUCE_FEED_DETAIL model)
        {
            CreateQMS_MA_REDUCE_FEED_DETAIL result = new CreateQMS_MA_REDUCE_FEED_DETAIL();

            result.ID = model.ID;
            result.REDUCE_FEED_ID = model.REDUCE_FEED_ID;
            result.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
            result.CONVERT_VALUE = model.CONVERT_VALUE;
            result.DELETE_FLAG = model.DELETE_FLAG;


            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_REDUCE_FEED_DETAILById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_REDUCE_FEED_DETAIL result = new QMS_MA_REDUCE_FEED_DETAIL();
            result = QMS_MA_REDUCE_FEED_DETAIL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_REDUCE_FEED_DETAILByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_REDUCE_FEED_DETAIL> result = new List<QMS_MA_REDUCE_FEED_DETAIL>();
            result = QMS_MA_REDUCE_FEED_DETAIL.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_REDUCE_FEED_DETAIL(QMSDBEntities db, CreateQMS_MA_REDUCE_FEED_DETAIL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_REDUCE_FEED_DETAIL(db, model);
            }
            else
            {
                result = AddQMS_MA_REDUCE_FEED_DETAIL(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_REDUCE_FEED_DETAIL(QMSDBEntities db, CreateQMS_MA_REDUCE_FEED_DETAIL model)
        {
            long result = 0;

            try
            {
                QMS_MA_REDUCE_FEED_DETAIL dt = new QMS_MA_REDUCE_FEED_DETAIL();
                dt = QMS_MA_REDUCE_FEED_DETAIL.GetById(db, model.ID);


                dt.REDUCE_FEED_ID = model.REDUCE_FEED_ID;
                dt.EXA_TAG_NAME = (null == model.EXA_TAG_NAME) ? "" : model.EXA_TAG_NAME;
                dt.CONVERT_VALUE = model.CONVERT_VALUE;
                dt.DELETE_FLAG = model.DELETE_FLAG;


                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_REDUCE_FEED_DETAIL(QMSDBEntities _db, QMS_MA_REDUCE_FEED_DETAIL model)
        {
            long result = 0;
            try
            {
                QMS_MA_REDUCE_FEED_DETAIL dt = new QMS_MA_REDUCE_FEED_DETAIL();
                dt = QMS_MA_REDUCE_FEED_DETAIL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_REDUCE_FEED_DETAIL.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_ROOT_CAUSE

        public List<ViewQMS_MA_ROOT_CAUSE> getQMS_MA_ROOT_CAUSEActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_ROOT_CAUSE> listResult = new List<ViewQMS_MA_ROOT_CAUSE>();

            try
            {
                List<QMS_MA_ROOT_CAUSE> list = QMS_MA_ROOT_CAUSE.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_ROOT_CAUSE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_ROOT_CAUSE> searchQMS_MA_ROOT_CAUSE(QMSDBEntities db, RootCauseSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_ROOT_CAUSE> objResult = new List<ViewQMS_MA_ROOT_CAUSE>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "NAME";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_ROOT_CAUSE> listData = new List<QMS_MA_ROOT_CAUSE>();
                listData = QMS_MA_ROOT_CAUSE.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_ROOT_CAUSE_TYPE> listRootCauseType = QMS_MA_ROOT_CAUSE_TYPE.GetActiveAll(db);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_ROOT_CAUSE>(listData.AsQueryable(), out count, out totalPage);
                    objResult = (from dt in query
                                 select new ViewQMS_MA_ROOT_CAUSE
                                 {
                                     ID = dt.ID,
                                     NAME = dt.NAME,
                                     ROOT_CAUSE_TYPE_ID = dt.ROOT_CAUSE_TYPE_ID,
                                     ROOT_CAUSE_TYPE_NAME = getRootCauseTypeName(listRootCauseType, dt.ROOT_CAUSE_TYPE_ID),                                  
                                     ROOT_CAUSE_DESC = dt.ROOT_CAUSE_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG

                                 }).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_ROOT_CAUSE getQMS_MA_ROOT_CAUSEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_ROOT_CAUSE objResult = new CreateQMS_MA_ROOT_CAUSE();

            try
            {
                QMS_MA_ROOT_CAUSE dt = new QMS_MA_ROOT_CAUSE();
                dt = QMS_MA_ROOT_CAUSE.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_ROOT_CAUSE> getQMS_MA_ROOT_CAUSEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_ROOT_CAUSE> listResult = new List<ViewQMS_MA_ROOT_CAUSE>();
            List<QMS_MA_ROOT_CAUSE_TYPE> listRootCauseType = QMS_MA_ROOT_CAUSE_TYPE.GetAll(db);


            try
            {
                List<QMS_MA_ROOT_CAUSE> list = QMS_MA_ROOT_CAUSE.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_ROOT_CAUSE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      ROOT_CAUSE_TYPE_ID = dt.ROOT_CAUSE_TYPE_ID,
                                      ROOT_CAUSE_TYPE_NAME = getRootCauseTypeName(listRootCauseType, dt.ROOT_CAUSE_TYPE_ID),                                  
                                      ROOT_CAUSE_DESC = dt.ROOT_CAUSE_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_ROOT_CAUSE convertModelToDB(CreateQMS_MA_ROOT_CAUSE model)
        {
            QMS_MA_ROOT_CAUSE result = new QMS_MA_ROOT_CAUSE();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.ROOT_CAUSE_TYPE_ID = model.ROOT_CAUSE_TYPE_ID;
            result.ROOT_CAUSE_DESC = (null == model.ROOT_CAUSE_DESC) ? "" : model.ROOT_CAUSE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_ROOT_CAUSE convertDBToModel(QMSDBEntities db, QMS_MA_ROOT_CAUSE model)
        {
            CreateQMS_MA_ROOT_CAUSE result = new CreateQMS_MA_ROOT_CAUSE();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.ROOT_CAUSE_TYPE_ID = model.ROOT_CAUSE_TYPE_ID;
            result.ROOT_CAUSE_DESC = (null == model.ROOT_CAUSE_DESC) ? "" : model.ROOT_CAUSE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_ROOT_CAUSEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_ROOT_CAUSE result = new QMS_MA_ROOT_CAUSE();
            result = QMS_MA_ROOT_CAUSE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_ROOT_CAUSEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_ROOT_CAUSE> result = new List<QMS_MA_ROOT_CAUSE>();
            result = QMS_MA_ROOT_CAUSE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_ROOT_CAUSE(QMSDBEntities db, CreateQMS_MA_ROOT_CAUSE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_ROOT_CAUSE(db, model);
            }
            else
            {
                result = AddQMS_MA_ROOT_CAUSE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_ROOT_CAUSE(QMSDBEntities db, CreateQMS_MA_ROOT_CAUSE model)
        {
            long result = 0;

            try
            {
                QMS_MA_ROOT_CAUSE dt = new QMS_MA_ROOT_CAUSE();
                dt = QMS_MA_ROOT_CAUSE.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.ROOT_CAUSE_TYPE_ID = model.ROOT_CAUSE_TYPE_ID;
                dt.ROOT_CAUSE_DESC = (null == model.ROOT_CAUSE_DESC) ? "" : model.ROOT_CAUSE_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;


                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_ROOT_CAUSE(QMSDBEntities _db, QMS_MA_ROOT_CAUSE model)
        {
            long result = 0;
            try
            {
                QMS_MA_ROOT_CAUSE dt = new QMS_MA_ROOT_CAUSE();
                dt = QMS_MA_ROOT_CAUSE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_ROOT_CAUSE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_ROOT_CAUSE_TYPE

        public List<ViewQMS_MA_ROOT_CAUSE_TYPE> getQMS_MA_ROOT_CAUSE_TYPEActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_ROOT_CAUSE_TYPE> listResult = new List<ViewQMS_MA_ROOT_CAUSE_TYPE>();

            try
            {
                List<QMS_MA_ROOT_CAUSE_TYPE> list = QMS_MA_ROOT_CAUSE_TYPE.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_ROOT_CAUSE_TYPE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_ROOT_CAUSE_TYPE> searchQMS_MA_ROOT_CAUSE_TYPE(QMSDBEntities db, RootCauseTypeSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_ROOT_CAUSE_TYPE> objResult = new List<ViewQMS_MA_ROOT_CAUSE_TYPE>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "NAME";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_ROOT_CAUSE_TYPE> listData = new List<QMS_MA_ROOT_CAUSE_TYPE>();
                listData = QMS_MA_ROOT_CAUSE_TYPE.GetAllBySearch(db, searchModel.mSearch);
         
                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_ROOT_CAUSE_TYPE>(listData.AsQueryable(), out count, out totalPage);
                    objResult = (from dt in query
                                 select new ViewQMS_MA_ROOT_CAUSE_TYPE
                                 {
                                     ID = dt.ID,
                                     NAME = dt.NAME,
                                     DELETE_FLAG = dt.DELETE_FLAG

                                 }).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }


        public List<ViewQMS_MA_ROOT_CAUSE_TYPE> getQMS_MA_ROOT_CAUSE_TYPEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_ROOT_CAUSE_TYPE> listResult = new List<ViewQMS_MA_ROOT_CAUSE_TYPE>();

            try
            {
                List<QMS_MA_ROOT_CAUSE_TYPE> list = QMS_MA_ROOT_CAUSE_TYPE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_ROOT_CAUSE_TYPE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_ROOT_CAUSE_TYPE convertModelToDB(CreateQMS_MA_ROOT_CAUSE_TYPE model)
        {
            QMS_MA_ROOT_CAUSE_TYPE result = new QMS_MA_ROOT_CAUSE_TYPE();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_ROOT_CAUSE_TYPE convertDBToModel(QMS_MA_ROOT_CAUSE_TYPE model)
        {
            CreateQMS_MA_ROOT_CAUSE_TYPE result = new CreateQMS_MA_ROOT_CAUSE_TYPE();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;


            return result;
        }


        public bool DeleteQMS_MA_ROOT_CAUSE_TYPEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_ROOT_CAUSE_TYPE result = new QMS_MA_ROOT_CAUSE_TYPE();
            result = QMS_MA_ROOT_CAUSE_TYPE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_ROOT_CAUSE_TYPEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_ROOT_CAUSE_TYPE> result = new List<QMS_MA_ROOT_CAUSE_TYPE>();
            result = QMS_MA_ROOT_CAUSE_TYPE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }


        public long SaveQMS_MA_ROOT_CAUSE_TYPE(QMSDBEntities db, CreateQMS_MA_ROOT_CAUSE_TYPE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_ROOT_CAUSE_TYPE(db, model);
            }
            else
            {
                result = AddQMS_MA_ROOT_CAUSE_TYPE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_ROOT_CAUSE_TYPE(QMSDBEntities db, CreateQMS_MA_ROOT_CAUSE_TYPE model)
        {
            long result = 0;

            try
            {
                QMS_MA_ROOT_CAUSE_TYPE dt = new QMS_MA_ROOT_CAUSE_TYPE();
                dt = QMS_MA_ROOT_CAUSE_TYPE.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_ROOT_CAUSE_TYPE(QMSDBEntities _db, QMS_MA_ROOT_CAUSE_TYPE model)
        {
            long result = 0;
            try
            {
                QMS_MA_ROOT_CAUSE_TYPE dt = new QMS_MA_ROOT_CAUSE_TYPE();
                dt = QMS_MA_ROOT_CAUSE_TYPE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_ROOT_CAUSE_TYPE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public CreateQMS_MA_ROOT_CAUSE_TYPE getCreateQMS_MA_ROOT_CAUSE_TYPEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_ROOT_CAUSE_TYPE objResult = new CreateQMS_MA_ROOT_CAUSE_TYPE();

            try
            {
                QMS_MA_ROOT_CAUSE_TYPE dt = new QMS_MA_ROOT_CAUSE_TYPE();
                dt = QMS_MA_ROOT_CAUSE_TYPE.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }
       
        
        #endregion

        #region QMS_MA_TEMPLATE

        private List<ViewQMS_MA_TEMPLATE> convertListData(QMSDBEntities db, List<QMS_MA_TEMPLATE> list)
        {
            List<ViewQMS_MA_TEMPLATE> listResult = new List<ViewQMS_MA_TEMPLATE>();
            List<QMS_MA_PLANT> listPlant = QMS_MA_PLANT.GetAll(db);
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_TEMPLATE
                                  {
                                     ID = dt.ID,
                                     NAME = dt.NAME,
                                     FILE_NAME = dt.FILE_NAME,
                                     PATH_NAME = dt.PATH_NAME,
                                     SHEET_NAME = dt.SHEET_NAME,
                                     ACTIVE_DATE = dt.ACTIVE_DATE,
                                     EXPIRE_DATE = dt.EXPIRE_DATE,
                                     TEMPLATE_DESC = dt.TEMPLATE_DESC,
                                     TEMPLATE_AREA = dt.TEMPLATE_AREA,
                                     //PLANT_ID = dt.PLANT_ID,
                                     //PLANT_NAME = getPlantName(listPlant , dt.PLANT_ID),
                                     TEMPLATE_TYPE = dt.TEMPLATE_TYPE,


                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER,
                                     SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US")),
                                     SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US")),

                                 }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<QMS_MA_TEMPLATE> GetAllByTemplateType(QMSDBEntities db, byte templateType)
        {
            List<QMS_MA_TEMPLATE> listData = new List<QMS_MA_TEMPLATE>();

            try
            {
                listData = QMS_MA_TEMPLATE.GetAllByTemplateType(db, templateType);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listData;
        }

        public List<QMS_MA_TEMPLATE> GetAllByTemplateTypeAndActiveDate(QMSDBEntities db, DateTime startDate, DateTime endDate, byte templateType)
        {
            List<QMS_MA_TEMPLATE> listData = new List<QMS_MA_TEMPLATE>();
 
            try
            { 
                listData = QMS_MA_TEMPLATE.GetAllByTemplateTypeAndActiveDate(db, startDate, endDate, templateType); 
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listData;
        }

        public List<ViewQMS_MA_TEMPLATE> searchQMS_MA_TEMPLATE(QMSDBEntities db, TemplateSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_TEMPLATE> objResult = new List<ViewQMS_MA_TEMPLATE>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_TEMPLATE> listData = new List<QMS_MA_TEMPLATE>();
                listData = QMS_MA_TEMPLATE.GetAllBySearch(db, searchModel.mSearch); 

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_TEMPLATE>(listData.AsQueryable(), out count, out totalPage).ToList();
                    objResult = convertListData(db, query);
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<TemplateName_COMBO> getMasterTemplateCombo(QMSDBEntities db, byte TemplateType)
        {
            List<TemplateName_COMBO> objResult = new List<TemplateName_COMBO>();
 
            try
            {
                List<QMS_MA_TEMPLATE> listData = QMS_MA_TEMPLATE.GetAllByTemplateType(db, TemplateType);

                if (null != listData && listData.Count() > 0)
                {
                    objResult = listData.GroupBy(m => m.TEMPLATE_AREA).Select(m => new TemplateName_COMBO
                    {
                        TEMPLATE_ID = m.Key,
                        TEMPLATE_NAME = m.Key
                    }).ToList();
                }
                 
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<List<ViewQMS_MA_TEMPALTE_EXCEL>> getTemplateExcelFile(QMSDBEntities db, long template_id)
        { 
            List<ViewQMS_MA_TEMPALTE_EXCEL> listResult = new List<ViewQMS_MA_TEMPALTE_EXCEL>();
            ViewQMS_MA_TEMPALTE_EXCEL tempData = new ViewQMS_MA_TEMPALTE_EXCEL();
            List<List<ViewQMS_MA_TEMPALTE_EXCEL>> matrixResult = new List<List<ViewQMS_MA_TEMPALTE_EXCEL>>();
            int maxRow = 50; 
            string EXCEL_FIELD = "";

            try
            {
                List<string> listColumn = new List<string>(new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO" });
                //List<string> listColumn = new List<string>(new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"  });

                List<ViewQMS_MA_TEMPLATE_ROW> listSample = getQMS_MA_TEMPLATE_ROWListById(db, template_id);
                List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = getQMS_MA_TEMPLATE_COLUMNListById(db, template_id);
                List<ViewQMS_MA_TEMPLATE_CONTROL> listControl = getQMS_MA_TEMPLATE_CONTROLListById(db, template_id);
                List<ViewQMS_MA_CONTROL_DATA> listControlData = getQMS_MA_CONTROL_DATAList(db);
                List<ViewQMS_MA_CONTROL_ROW> listControlRow = getQMS_MA_CONTROL_ROWList(db);
                List<ViewQMS_MA_CONTROL_COLUMN> listControlColumn = getQMS_MA_CONTROL_COLUMNList(db); 

                //Column
                listResult = new List<ViewQMS_MA_TEMPALTE_EXCEL>();
                foreach (string dtColumn in listColumn)
                {
                    tempData = new ViewQMS_MA_TEMPALTE_EXCEL();
                    tempData.TD_NAME =  dtColumn;
                    tempData.ITEM_TYPE = (byte)ITEM_TYPE.COL_HEADER;
                    listResult.Add(tempData);
                }
                matrixResult.Add(listResult); 
                //Column

                for (int i = 1; i < maxRow; i++)
                {
                    listResult = new List<ViewQMS_MA_TEMPALTE_EXCEL>();
                    foreach (string dtColumn in listColumn)
                    {
                        tempData = new ViewQMS_MA_TEMPALTE_EXCEL();
                        if (dtColumn == "")
                        {
                            tempData.TD_NAME = i.ToString();
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.ROW_HEADER;
                        }
                        else
                        {
                            tempData.ROW_DATA = new ViewQMS_MA_TEMPLATE_ROW();
                            tempData.COLUMN_DATA = new ViewQMS_MA_TEMPLATE_COLUMN();
                            tempData.CONTROL_DATA = new ViewQMS_MA_TEMPLATE_CONTROL();

                            tempData.DATA_TYPE = (byte)EXCEL_DATA_TYPE.SAMPLE;
                            EXCEL_FIELD = dtColumn + i.ToString();

                            //ExcelName
                            tempData.ROW_DATA.EXCEL_FIELD = EXCEL_FIELD;
                            tempData.COLUMN_DATA.EXCEL_FIELD = EXCEL_FIELD;
                            tempData.CONTROL_DATA.EXCEL_FIELD = EXCEL_FIELD;

                            ViewQMS_MA_TEMPLATE_ROW tempSample = listSample.Where(m => m.EXCEL_FIELD == EXCEL_FIELD).FirstOrDefault();
                            ViewQMS_MA_TEMPLATE_COLUMN tempItem = listItem.Where(m => m.EXCEL_FIELD == EXCEL_FIELD).FirstOrDefault();
                            ViewQMS_MA_TEMPLATE_CONTROL tempControl = listControl.Where(m => m.EXCEL_FIELD == EXCEL_FIELD).FirstOrDefault();

                            if (null != tempSample)
                            {
                                tempData.ROW_DATA = tempSample;
                                tempData.TD_NAME = tempSample.NAME;
                            }
                            else if (null != tempItem)
                            {
                                tempData.DATA_TYPE = (byte)EXCEL_DATA_TYPE.ITEM;
                                tempData.COLUMN_DATA = tempItem;
                                tempData.TD_NAME = tempItem.NAME + "(" + tempItem.UNIT_NAME + ")";
                            }
                            else if (null != tempControl)
                            {
                                tempData.DATA_TYPE = (byte)EXCEL_DATA_TYPE.CONTROL_VALUE;
                                tempData.CONTROL_DATA = tempControl;
                                tempData.TD_NAME = getControlValueTextByControlDataId(listControlData, tempControl.CONTROL_ID);

                                var data = listControlData.Where(m => m.ID == tempControl.CONTROL_ID).FirstOrDefault();

                               
                                if (null != data)
                                {
                                    tempData.CONTROL_DATA.CONTROL_GROUP_ID = data.CONTROL_ID;
                                    tempData.CONTROL_DATA.CONTROL_GROUP_SMAPLE_ID = data.CONTROL_ROW_ID;
                                    tempData.CONTROL_DATA.CONTROL_GROUP_ITEM_ID = data.CONTROL_COLUMN_ID; 
                                    tempData.CONTROL_DATA.CONTROL_GROUP_SMAPLE_NAME = getControlROWNameEx(listControlRow, data.CONTROL_ROW_ID); //
                                    tempData.CONTROL_DATA.CONTROL_GROUP_ITEM_NAME = getControlColumnNameEx(listControlColumn, data.CONTROL_COLUMN_ID);
                                     
                                }
                            }

                            var sampleId = listSample.Where(m => m.EXCEL_ROW == i).OrderBy(m => m.EXCEL_FIELD).FirstOrDefault();


                            if (null != sampleId && tempData.CONTROL_DATA.SAMPLE_ID == 0) //set Default sample
                            { 
                                 tempData.CONTROL_DATA.SAMPLE_ID = sampleId.ID; 
                            }

                            //List<ViewQMS_MA_TEMPLATE_COLUMN> ItemIdList = listItem.Where(m => dtColumn.Contains( m.EXCEL_FIELD )).OrderBy(m => m.EXCEL_FIELD).ToList();

                            List<ViewQMS_MA_TEMPLATE_COLUMN> ItemIdList = listItem.Where(m => m.EXCEL_FIELD.Contains(dtColumn)).OrderBy(m => m.EXCEL_FIELD).ToList(); //.FirstOrDefault();

                            if (null != ItemIdList && ItemIdList.Count() > 0 && tempData.CONTROL_DATA.ITEM_ID == 0) //set Default sample
                            {
                                //tempData.CONTROL_DATA.ITEM_ID = ItemId.ID;
                                 foreach (ViewQMS_MA_TEMPLATE_COLUMN dtItem in ItemIdList)
                                 {
                                     //tempData.CONTROL_DATA.ITEM_ID = ItemId.ID;
                                     if (i > dtItem.EXCEL_ROW)
                                     {
                                         tempData.CONTROL_DATA.ITEM_ID = dtItem.ID;
                                     }
                                 }
                            }

                            //if (null != ItemIdList && ItemIdList.Count() > 0 && tempData.CONTROL_DATA.ITEM_ID == 0) //set Default sample
                            //{
                            //    foreach (ViewQMS_MA_TEMPLATE_COLUMN dtItem in ItemIdList)
                            //    {
                            //        //tempData.CONTROL_DATA.ITEM_ID = ItemId.ID;
                            //        if (i > dtItem.EXCEL_ROW)
                            //        {
                            //            tempData.CONTROL_DATA.ITEM_ID = dtItem.ID;
                            //        }
                            //    }
                            //} 
                            tempData.ITEM_TYPE = (byte)ITEM_TYPE.DATA;
                        }
                        listResult.Add(tempData);
                    } 
                    matrixResult.Add(listResult); 
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return matrixResult;
        }

        public CreateQMS_MA_TEMPLATE getQMS_MA_TEMPLATEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_TEMPLATE objResult = new CreateQMS_MA_TEMPLATE();

            try
            {
                QMS_MA_TEMPLATE dt = new QMS_MA_TEMPLATE();
                dt = QMS_MA_TEMPLATE.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }
    

        public List<ViewQMS_MA_TEMPLATE> getQMS_MA_TEMPLATEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_TEMPLATE> listResult = new List<ViewQMS_MA_TEMPLATE>();

            try
            {
                List<QMS_MA_TEMPLATE> list = QMS_MA_TEMPLATE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public CreateQMS_MA_TEMPLATE convertDBToModel(QMSDBEntities db, QMS_MA_TEMPLATE model)
        {
            CreateQMS_MA_TEMPLATE result = new CreateQMS_MA_TEMPLATE();

            result.ID = model.ID;
            result.TEMPLATE_TYPE = model.TEMPLATE_TYPE;
            result.TEMPLATE_AREA = (null == model.TEMPLATE_AREA) ? "" : model.TEMPLATE_AREA; 
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
            result.PATH_NAME = (null == model.PATH_NAME) ? "" : model.PATH_NAME;
            result.SHEET_NAME = (null == model.SHEET_NAME) ? "" : model.SHEET_NAME;
            result.TEMPLATE_DESC = (null == model.TEMPLATE_DESC) ? "" : model.TEMPLATE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.EXPIRE_DATE = model.EXPIRE_DATE;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));

            return result;
        }

        private QMS_MA_TEMPLATE convertModelToDB(CreateQMS_MA_TEMPLATE model)
        {
            QMS_MA_TEMPLATE result = new QMS_MA_TEMPLATE();

            result.ID = model.ID;
            result.TEMPLATE_TYPE = model.TEMPLATE_TYPE;
            result.TEMPLATE_AREA = (null == model.TEMPLATE_AREA) ? "" : model.TEMPLATE_AREA;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
            result.PATH_NAME = (null == model.PATH_NAME) ? "" : model.PATH_NAME;
            result.SHEET_NAME = (null == model.SHEET_NAME) ? "" : model.SHEET_NAME;
            
            result.TEMPLATE_DESC = (null == model.TEMPLATE_DESC) ? "" : model.TEMPLATE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.EXPIRE_DATE = model.EXPIRE_DATE;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_TEMPLATEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_TEMPLATE result = new QMS_MA_TEMPLATE();
            result = QMS_MA_TEMPLATE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_TEMPLATE_DATAByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_TEMPLATE> result = new List<QMS_MA_TEMPLATE>();
            result = QMS_MA_TEMPLATE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_TEMPLATE_DATA(QMSDBEntities db, CreateQMS_MA_TEMPALTE_EXCEL model)
        {
            bool bResult = false;

            try
            {
                if (model.ROW_DATA.ID > 0)
                {
                    bResult =  DeleteQMS_MA_TEMPLATE_ROWById(db, model.ROW_DATA.ID);
                }

                if (model.COLUMN_DATA.ID > 0)
                {
                    bResult = DeleteQMS_MA_TEMPLATE_COLUMNById(db, model.COLUMN_DATA.ID);
                }

                if (model.CONTROL_DATA.ID > 0)
                {
                    bResult = DeleteQMS_MA_TEMPLATE_CONTROLById(db, model.CONTROL_DATA.ID);
                }

                if (model.ROW_DATA.ID == 0 && model.COLUMN_DATA.ID == 0 && model.CONTROL_DATA.ID == 0)// ไม่มีข้อมูลจะให้ ลบ
                {
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_TEMPLATE_DATA(QMSDBEntities db, CreateQMS_MA_TEMPALTE_EXCEL model)
        {
            long result = 0;
            bool bResult = false;

            try{
                if (model.DATA_TYPE == (byte)EXCEL_DATA_TYPE.SAMPLE) //ยังไม่ได้ ทำ delete ค่าอื่น กรณีเปลี่ยน type
                {
                    model.ROW_DATA.TEMPLATE_ID = model.TEMPLATE_ID;
                    result = SaveQMS_MA_TEMPLATE_ROW(db, model.ROW_DATA);

                    //ลบ ข้อมูลเก่า ถ้ามี 
                    if (model.COLUMN_DATA.ID > 0)
                    {
                        bResult = DeleteQMS_MA_TEMPLATE_COLUMNById(db, model.COLUMN_DATA.ID);
                    }

                    if (model.CONTROL_DATA.ID > 0)
                    {
                        bResult = DeleteQMS_MA_TEMPLATE_CONTROLById(db, model.CONTROL_DATA.ID);
                    }

                }
                else if (model.DATA_TYPE == (byte)EXCEL_DATA_TYPE.ITEM)
                {
                    model.COLUMN_DATA.TEMPLATE_ID = model.TEMPLATE_ID;
                    result = SaveQMS_MA_TEMPLATE_COLUMN(db, model.COLUMN_DATA);

                    //ลบ ข้อมูลเก่า ถ้ามี 
                    if (model.ROW_DATA.ID > 0)
                    {
                        bResult = DeleteQMS_MA_TEMPLATE_ROWById(db, model.ROW_DATA.ID);
                    }

                    if (model.CONTROL_DATA.ID > 0)
                    {
                        bResult = DeleteQMS_MA_TEMPLATE_CONTROLById(db, model.CONTROL_DATA.ID);
                    }

                }
                else if (model.DATA_TYPE == (byte)EXCEL_DATA_TYPE.CONTROL_VALUE)
                {
                    model.CONTROL_DATA.TEMPLATE_ID = model.TEMPLATE_ID;
                    result = SaveQMS_MA_TEMPLATE_CONTROL(db, model.CONTROL_DATA);

                    //ลบ ข้อมูลเก่า ถ้ามี 
                    if (model.ROW_DATA.ID > 0)
                    {
                        bResult = DeleteQMS_MA_TEMPLATE_ROWById(db, model.ROW_DATA.ID);
                    }

                    if (model.COLUMN_DATA.ID > 0)
                    {
                        bResult = DeleteQMS_MA_TEMPLATE_COLUMNById(db, model.COLUMN_DATA.ID);
                    } 
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
             
            return result;
        }

        public bool checkDuplicateTemplateName(QMSDBEntities db, CreateQMS_MA_TEMPLATE model)
        {
            bool result = true;

            try
            {
               List<QMS_MA_TEMPLATE> listData = QMS_MA_TEMPLATE.GetAllByTemplateTypeAndName(db, model.NAME, model.TEMPLATE_TYPE);

               if (null != listData && listData.Count() > 0  )
               {
                   if ( listData.Count() == 1 && listData[0].ID == model.ID) // spkip for edit
                   {
                       result = false;
                   }
               }
               else
               {
                   result = false;
               }
            }
            catch (Exception ex)
            {
                _errMsg = ex.Message;
            }

            return result;
        }

        public long SaveQMS_MA_TEMPLATE(QMSDBEntities db, CreateQMS_MA_TEMPLATE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_TEMPLATE(db, model);
            }
            else
            {
                result = AddQMS_MA_TEMPLATE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_TEMPLATE(QMSDBEntities db, CreateQMS_MA_TEMPLATE model)
        {
            long result = 0;

            try
            {
                QMS_MA_TEMPLATE dt = new QMS_MA_TEMPLATE();
                dt = QMS_MA_TEMPLATE.GetById(db, model.ID);

                dt.TEMPLATE_TYPE = model.TEMPLATE_TYPE;
                dt.TEMPLATE_AREA = (null == model.TEMPLATE_AREA) ? "" : model.TEMPLATE_AREA;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
                dt.PATH_NAME = (null == model.PATH_NAME) ? "" : model.PATH_NAME;
                dt.SHEET_NAME = (null == model.SHEET_NAME) ? "" : model.SHEET_NAME;
                
                dt.TEMPLATE_DESC = (null == model.TEMPLATE_DESC) ? "" : model.TEMPLATE_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                dt.ACTIVE_DATE = model.ACTIVE_DATE;
                dt.EXPIRE_DATE = model.EXPIRE_DATE;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_TEMPLATE(QMSDBEntities _db, QMS_MA_TEMPLATE model)
        {
            long result = 0;
            try
            {
                QMS_MA_TEMPLATE dt = new QMS_MA_TEMPLATE();
                dt = QMS_MA_TEMPLATE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_TEMPLATE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_TEMPLATE_COLUMN
        public List<ViewQMS_MA_TEMPLATE_COLUMN> getQMS_MA_TEMPLATE_COLUMNList(QMSDBEntities db)
        {
            List<ViewQMS_MA_TEMPLATE_COLUMN> listResult = new List<ViewQMS_MA_TEMPLATE_COLUMN>();

            try
            {
                List<QMS_MA_TEMPLATE_COLUMN> list = QMS_MA_TEMPLATE_COLUMN.GetAll(db);
                List<ViewQMS_MA_UNIT> listUnit = getQMS_MA_UNITList(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_TEMPLATE_COLUMN
                                  {
                                      ID = dt.ID,
                                      TEMPLATE_ID = dt.TEMPLATE_ID,
                                      NAME = dt.NAME,
                                      EXCEL_FIELD = dt.EXCEL_FIELD,
                                      UNIT_ID = dt.UNIT_ID,
                                      UNIT_NAME = getUnitName(listUnit, dt.UNIT_ID),
                                      
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_COLUMN> getQMS_MA_TEMPLATE_COLUMNListById(QMSDBEntities db, long template_id)
        {
            List<ViewQMS_MA_TEMPLATE_COLUMN> listResult = new List<ViewQMS_MA_TEMPLATE_COLUMN>();

            try
            {
                List<QMS_MA_TEMPLATE_COLUMN> list = QMS_MA_TEMPLATE_COLUMN.GetAllByTemplateId(db, template_id);
                List<ViewQMS_MA_UNIT> listUnit = getQMS_MA_UNITList(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_TEMPLATE_COLUMN
                                  {
                                      ID = dt.ID,
                                      TEMPLATE_ID = dt.TEMPLATE_ID,
                                      NAME = dt.NAME,
                                      EXCEL_FIELD = dt.EXCEL_FIELD,
                                      EXCEL_COL = getColumnNameFromFieldName(dt.EXCEL_FIELD),
                                      EXCEL_ROW = getRowsNumberFromFieldName(dt.EXCEL_FIELD),
                                      UNIT_ID = dt.UNIT_ID,
                                      UNIT_NAME = getUnitName( listUnit, dt.UNIT_ID ),
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_TEMPLATE_COLUMN convertModelToDB(CreateQMS_MA_TEMPLATE_COLUMN model)
        {
            QMS_MA_TEMPLATE_COLUMN result = new QMS_MA_TEMPLATE_COLUMN();

            result.ID = model.ID;

            result.TEMPLATE_ID = model.TEMPLATE_ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.EXCEL_FIELD = (null == model.EXCEL_FIELD) ? "" : model.EXCEL_FIELD;
            result.UNIT_ID = model.UNIT_ID;
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_TEMPLATE_COLUMNById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_TEMPLATE_COLUMN result = new QMS_MA_TEMPLATE_COLUMN();
            result = QMS_MA_TEMPLATE_COLUMN.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }


        public long SaveQMS_MA_TEMPLATE_COLUMN(QMSDBEntities db, CreateQMS_MA_TEMPLATE_COLUMN model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_TEMPLATE_COLUMN(db, model);
            }
            else
            {
                result = AddQMS_MA_TEMPLATE_COLUMN(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_TEMPLATE_COLUMN(QMSDBEntities db, CreateQMS_MA_TEMPLATE_COLUMN model)
        {
            long result = 0;

            try
            {
                QMS_MA_TEMPLATE_COLUMN dt = new QMS_MA_TEMPLATE_COLUMN();
                dt = QMS_MA_TEMPLATE_COLUMN.GetById(db, model.ID);

                dt.TEMPLATE_ID = model.TEMPLATE_ID;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.EXCEL_FIELD = (null == model.EXCEL_FIELD) ? "" : model.EXCEL_FIELD;
                dt.UNIT_ID = model.UNIT_ID;
                
               // dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_TEMPLATE_COLUMN(QMSDBEntities _db, QMS_MA_TEMPLATE_COLUMN model)
        {
            long result = 0;
            try
            {
                QMS_MA_TEMPLATE_COLUMN dt = new QMS_MA_TEMPLATE_COLUMN();
                dt = QMS_MA_TEMPLATE_COLUMN.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_TEMPLATE_COLUMN.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_TEMPLATE_CONTROL

        private List<ViewQMS_MA_TEMPLATE_CONTROL> convertListData(QMSDBEntities db, List<QMS_MA_TEMPLATE_CONTROL> list)
        {
            List<ViewQMS_MA_TEMPLATE_CONTROL> listResult = new List<ViewQMS_MA_TEMPLATE_CONTROL>(); 
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_TEMPLATE_CONTROL
                                  {
                                      ID = dt.ID,
                                      SAMPLE_ID = dt.SAMPLE_ID,
                                      ITEM_ID = dt.ITEM_ID,
                                      EXCEL_FIELD = dt.EXCEL_FIELD,
                                      CONTROL_ID = dt.CONTROL_ID,
                                      SHOW_ON_TREND_FLAG = (dt.SHOW_ON_TREND_FLAG == 1) ? true : false,
                                      CONTROL_FLAG = (dt.ID == 0) ? false : true,

                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss"),
                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss")

                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_CONTROL> getQMS_MA_TEMPLATE_CONTROLList(QMSDBEntities db)
        {
            List<ViewQMS_MA_TEMPLATE_CONTROL> listResult = new List<ViewQMS_MA_TEMPLATE_CONTROL>();

            try
            {
                List<QMS_MA_TEMPLATE_CONTROL> list = QMS_MA_TEMPLATE_CONTROL.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_CONTROL> getQMS_MA_TEMPLATE_CONTROLListById(QMSDBEntities db, long template_id)
        {
            List<ViewQMS_MA_TEMPLATE_CONTROL> listResult = new List<ViewQMS_MA_TEMPLATE_CONTROL>();

            try
            {
                List<QMS_MA_TEMPLATE_CONTROL> list = QMS_MA_TEMPLATE_CONTROL.GetAllByTemplateId(db, template_id);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_TEMPLATE_CONTROL convertModelToDB(CreateQMS_MA_TEMPLATE_CONTROL model)
        {
            QMS_MA_TEMPLATE_CONTROL result = new QMS_MA_TEMPLATE_CONTROL();

            result.ID = model.ID;
            result.SAMPLE_ID = model.SAMPLE_ID;
            result.ITEM_ID = model.ITEM_ID;
            result.EXCEL_FIELD = (null == model.EXCEL_FIELD) ? "" : model.EXCEL_FIELD;
            result.CONTROL_ID = model.CONTROL_ID;
            result.SHOW_ON_TREND_FLAG = (model.SHOW_ON_TREND_FLAG == true) ? (byte)1 : (byte)0;
            
            //result.DELETE_FLAG = model.DELETE_FLAG;
            result.TEMPLATE_ID = model.TEMPLATE_ID;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_TEMPLATE_CONTROLById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_TEMPLATE_CONTROL result = new QMS_MA_TEMPLATE_CONTROL();
            result = QMS_MA_TEMPLATE_CONTROL.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }


        public long SaveQMS_MA_TEMPLATE_CONTROL(QMSDBEntities db, CreateQMS_MA_TEMPLATE_CONTROL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_TEMPLATE_CONTROL(db, model);
            }
            else
            {
                result = AddQMS_MA_TEMPLATE_CONTROL(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_TEMPLATE_CONTROL(QMSDBEntities db, CreateQMS_MA_TEMPLATE_CONTROL model)
        {
            long result = 0;

            try
            {
                QMS_MA_TEMPLATE_CONTROL dt = new QMS_MA_TEMPLATE_CONTROL();
                dt = QMS_MA_TEMPLATE_CONTROL.GetById(db, model.ID);

                dt.SAMPLE_ID = model.SAMPLE_ID;
                dt.ITEM_ID = model.ITEM_ID;
                dt.EXCEL_FIELD = (null == model.EXCEL_FIELD) ? "" : model.EXCEL_FIELD;
                dt.CONTROL_ID = model.CONTROL_ID;
                dt.SHOW_ON_TREND_FLAG = (model.SHOW_ON_TREND_FLAG == true) ? (byte)1 : (byte)0;
                 
                //dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_TEMPLATE_CONTROL(QMSDBEntities _db, QMS_MA_TEMPLATE_CONTROL model)
        {
            long result = 0;
            try
            {
                QMS_MA_TEMPLATE_CONTROL dt = new QMS_MA_TEMPLATE_CONTROL();
                dt = QMS_MA_TEMPLATE_CONTROL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_TEMPLATE_CONTROL.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_TEMPLATE_ROW

        public List<TrendItem> getTrendItemListBySample(QMSDBEntities db, string SampleName, long PLANT_ID, byte TEMPLATE_TYPE)
        {
            List<TrendItem> listResult = new List<TrendItem>();

            try
            {
                List<ViewQMS_MA_TEMPLATE_ROW> listData = getQMS_MA_TEMPLATE_ROWListByPlantAndTypeId(db, PLANT_ID, TEMPLATE_TYPE);

                if (listData.Count() > 0)
                {

                    long[] ids = listData.Where(m => m.NAME == SampleName).Select(m => m.ID).ToArray();

                    List<QMS_MA_TEMPLATE_CONTROL> listTemplateControl = QMS_MA_TEMPLATE_CONTROL.GetByListSampleId(db, ids);

                    if (listTemplateControl.Count() > 0)
                    {
                        long[] Itemids = listTemplateControl.Where(m => m.SHOW_ON_TREND_FLAG == 1).Select(m => m.ITEM_ID).ToArray();//เลือก เฉพาะอันที่เปิดแสดงผล หน้าจอ
                        List<ViewQMS_MA_UNIT> listUnit = getQMS_MA_UNITList(db);
                        List<QMS_MA_TEMPLATE_COLUMN> listItem = QMS_MA_TEMPLATE_COLUMN.GetByListId(db, Itemids);

                        if (listItem.Count() > 0)
                        {
                            listItem = listItem.GroupBy(m => new { m.NAME, m.UNIT_ID }).Select(g => g.First()).ToList();

                            foreach (QMS_MA_TEMPLATE_COLUMN dt in listItem)
                            {
                                TrendItem tempItem = new TrendItem();

                                tempItem.ITEM_ID = dt.ID;
                                tempItem.ITEM_NAME = dt.NAME;
                                tempItem.UNIT_ID = dt.UNIT_ID;
                                tempItem.UNIT_NAME = getUnitName(listUnit, dt.UNIT_ID);
                                listResult.Add(tempItem);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        //public List<TrendItem> getTrendItemListBySampleEx(QMSDBEntities db, string SampleName, long TemplateId)
        public List<TrendItem> getTrendItemListBySampleEx(QMSDBEntities db, string SampleName, string TemplateId, byte TEMPLATE_TYPE)
        {
            List<TrendItem> listResult = new List<TrendItem>();

            try
            {

                List<QMS_MA_TEMPLATE> listTemplate = QMS_MA_TEMPLATE.GetAllByTemplateTypeAndTEMPLATE_AREA(db, TemplateId, TEMPLATE_TYPE);

                if (null != listTemplate && listTemplate.Count() > 0)
                {
                    long[] listIds = listTemplate.Select(m => m.ID).ToArray();

                    List<QMS_MA_TEMPLATE_ROW> listData = QMS_MA_TEMPLATE_ROW.GetAllByListTemplateId(db, listIds);
                     
                    if (listData.Count() > 0)
                    {  
                        //List<ViewQMS_MA_TEMPLATE_ROW> listData = getQMS_MA_TEMPLATE_ROWListByTemplateId(db, TemplateId);
                        long[] ids = listData.Where(m => m.NAME == SampleName).Select(m => m.ID).ToArray();

                        List<QMS_MA_TEMPLATE_CONTROL> listTemplateControl = QMS_MA_TEMPLATE_CONTROL.GetByListSampleId(db, ids);

                        if (listTemplateControl.Count() > 0)
                        {
                            long[] Itemids = listTemplateControl.Where(m => m.SHOW_ON_TREND_FLAG == 1).Select(m => m.ITEM_ID).ToArray();//เลือก เฉพาะอันที่เปิดแสดงผล หน้าจอ
                            List<ViewQMS_MA_UNIT> listUnit = getQMS_MA_UNITList(db);
                            List<QMS_MA_TEMPLATE_COLUMN> listItem = QMS_MA_TEMPLATE_COLUMN.GetByListId(db, Itemids);

                            if (listItem.Count() > 0)
                            {
                                listItem = listItem.GroupBy(m => new { m.NAME, m.UNIT_ID }).Select(g => g.First()).ToList();

                                foreach (QMS_MA_TEMPLATE_COLUMN dt in listItem)
                                {
                                    TrendItem tempItem = new TrendItem();

                                    tempItem.ITEM_ID = dt.ID;
                                    tempItem.ITEM_NAME = dt.NAME;
                                    tempItem.UNIT_ID = dt.UNIT_ID;
                                    tempItem.UNIT_NAME = getUnitName(listUnit, dt.UNIT_ID);
                                    listResult.Add(tempItem);
                                }

                            }
                        }
                    }
                }
                 
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<TrendSample> getTrendSampleListByPlantAndTypeId(QMSDBEntities db, long PLANT_ID, byte TEMPLATE_TYPE)
        {
            List<TrendSample> listResult = new List<TrendSample>();

            try
            {
                 List<ViewQMS_MA_TEMPLATE_ROW> listData = getQMS_MA_TEMPLATE_ROWListByPlantAndTypeId(db, PLANT_ID, TEMPLATE_TYPE);

                 if (listData.Count() > 0)
                 {

                     listData = listData.GroupBy(x => x.NAME).Select(g => g.First()).ToList();

                     //var listGroup = listData.GroupBy(x => new { x.NAME })
                     //                        .Select(x => new TrendSample
                     //                        {
                     //                            SAMPLE_NAME = x.Key.NAME,
                     //                            //SAMPLE_POINT =  ,
                     //                            PLANT_ID = PLANT_ID
                     //                            //SAMPLE_ID มีหลายค่า ไม่ uquip
                     //                        }).ToList();

                      foreach (ViewQMS_MA_TEMPLATE_ROW dt in listData)
                      {
                          TrendSample tempTrendSample = new TrendSample();

                          tempTrendSample.PLANT_ID = PLANT_ID;
                          tempTrendSample.SAMPLE_NAME = dt.NAME;
                          tempTrendSample.SAMPLE_POINT = dt.SAMPLING_POINT;

                          listResult.Add(tempTrendSample);
                      }

                     
                 }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<TrendSample> getTrendSampleListByStringTemplateId(QMSDBEntities db, string TemplateId, byte TEMPLATE_TYPE)
        {
            List<TrendSample> listResult = new List<TrendSample>();
            
            try
            {
                //Step1 get by template name ... 
                List<QMS_MA_TEMPLATE> listTemplate = QMS_MA_TEMPLATE.GetAllByTemplateTypeAndTEMPLATE_AREA(db, TemplateId, TEMPLATE_TYPE);

                if (null != listTemplate && listTemplate.Count() > 0)
                {
                    long[] listIds = listTemplate.Select(m => m.ID).ToArray();

                    List<QMS_MA_TEMPLATE_ROW> listData = QMS_MA_TEMPLATE_ROW.GetAllByListTemplateId(db, listIds);
                      
                    if (listData.Count() > 0)
                    {

                        listData = listData.GroupBy(x => x.NAME).Select(g => g.First()).ToList();

                        foreach (QMS_MA_TEMPLATE_ROW dt in listData)
                        {
                            TrendSample tempTrendSample = new TrendSample();

                            //tempTrendSample.TEMPLATE_ID = TemplateId;
                            tempTrendSample.SAMPLE_NAME = dt.NAME;
                            tempTrendSample.SAMPLE_POINT = dt.SAMPLING_POINT;

                            listResult.Add(tempTrendSample);
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<TrendSample> getTrendSampleListByTemplateId(QMSDBEntities db, long TemplateId)
        {
            List<TrendSample> listResult = new List<TrendSample>();

            try
            {
                List<ViewQMS_MA_TEMPLATE_ROW> listData = getQMS_MA_TEMPLATE_ROWListByTemplateId(db, TemplateId);

                if (listData.Count() > 0)
                {

                    listData = listData.GroupBy(x => x.NAME).Select(g => g.First()).ToList();

                    foreach (ViewQMS_MA_TEMPLATE_ROW dt in listData)
                    {
                        TrendSample tempTrendSample = new TrendSample();

                        tempTrendSample.TEMPLATE_ID = TemplateId;
                        tempTrendSample.SAMPLE_NAME = dt.NAME;
                        tempTrendSample.SAMPLE_POINT = dt.SAMPLING_POINT;

                        listResult.Add(tempTrendSample);
                    }


                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_ROW> getQMS_MA_TEMPLATE_ROWListByTemplaeAreaAndTypeId(QMSDBEntities db, string TEMPLATE_AREA, byte TEMPLATE_TYPE)
        {
            List<ViewQMS_MA_TEMPLATE_ROW> listResult = new List<ViewQMS_MA_TEMPLATE_ROW>();

            try
            {
                List<QMS_MA_TEMPLATE> listTemplate = QMS_MA_TEMPLATE.GetAllByTemplateTypeAndTEMPLATE_AREA(db, TEMPLATE_AREA, TEMPLATE_TYPE);


                if (listTemplate.Count() > 0) // แสดงว่ามี template type นี้อยุ่
                {
                    long[] listIds = listTemplate.Select(m => m.ID).ToArray();

                    List<QMS_MA_TEMPLATE_ROW> list = QMS_MA_TEMPLATE_ROW.GetAllByListTemplateId(db, listIds);

                    if (null != list && list.Count() > 0)
                    {
                        listResult = convertListData(db, list);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_ROW> getQMS_MA_TEMPLATE_ROWListByPlantAndTypeId(QMSDBEntities db, long PLANT_ID, byte TEMPLATE_TYPE)
        {
            List<ViewQMS_MA_TEMPLATE_ROW> listResult = new List<ViewQMS_MA_TEMPLATE_ROW>();

            try
            {
                List<QMS_MA_TEMPLATE> listTemplate = QMS_MA_TEMPLATE.GetAllByTemplateTypeAndPlantId(db, PLANT_ID, TEMPLATE_TYPE);


                if (listTemplate.Count() > 0) // แสดงว่ามี template type นี้อยุ่
                {
                    long[] listIds = listTemplate.Select(m => m.ID).ToArray();

                    List<QMS_MA_TEMPLATE_ROW> list = QMS_MA_TEMPLATE_ROW.GetAllByListTemplateId(db, listIds);

                    if (null != list && list.Count() > 0)
                    {
                        listResult = convertListData(db, list);
                    }
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_ROW> getQMS_MA_TEMPLATE_ROWListByTemplateId(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_TEMPLATE_ROW> listResult = new List<ViewQMS_MA_TEMPLATE_ROW>();
             
            try
            {
                long[] listIds = new long[] { id };

                List<QMS_MA_TEMPLATE_ROW> list = QMS_MA_TEMPLATE_ROW.GetAllByListTemplateId(db, listIds);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_ROW> convertListData(QMSDBEntities db, List<QMS_MA_TEMPLATE_ROW> list)
        {
            List<ViewQMS_MA_TEMPLATE_ROW> listResult = new List<ViewQMS_MA_TEMPLATE_ROW>(); 

            try
            {
                if (null != list && list.Count() > 0)
                {

                    listResult = (from dt in list
                                  select new ViewQMS_MA_TEMPLATE_ROW
                                  {
                                      ID = dt.ID,
                                      TEMPLATE_ID = dt.TEMPLATE_ID,
                                      NAME = dt.NAME,
                                      EXCEL_FIELD = dt.EXCEL_FIELD,
                                      EXCEL_COL = getColumnNameFromFieldName(dt.EXCEL_FIELD),
                                      EXCEL_ROW = getRowsNumberFromFieldName(dt.EXCEL_FIELD),
                                      SAMPLING_POINT = dt.SAMPLING_POINT,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }
         
        public List<ViewQMS_MA_TEMPLATE_ROW> getQMS_MA_TEMPLATE_ROWList(QMSDBEntities db)
        {
            List<ViewQMS_MA_TEMPLATE_ROW> listResult = new List<ViewQMS_MA_TEMPLATE_ROW>();

            try
            {
                List<QMS_MA_TEMPLATE_ROW> list = QMS_MA_TEMPLATE_ROW.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db,list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_TEMPLATE_ROW> getQMS_MA_TEMPLATE_ROWListById(QMSDBEntities db, long template_id)
        {
            List<ViewQMS_MA_TEMPLATE_ROW> listResult = new List<ViewQMS_MA_TEMPLATE_ROW>();

            try
            {
                List<QMS_MA_TEMPLATE_ROW> list = QMS_MA_TEMPLATE_ROW.GetAllByTemplateId(db, template_id);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db,list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_TEMPLATE_ROW convertModelToDB(CreateQMS_MA_TEMPLATE_ROW model)
        {
            QMS_MA_TEMPLATE_ROW result = new QMS_MA_TEMPLATE_ROW();

            result.ID = model.ID;

            result.TEMPLATE_ID = model.TEMPLATE_ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.EXCEL_FIELD = (null == model.EXCEL_FIELD) ? "" : model.EXCEL_FIELD;
            result.SAMPLING_POINT = (null == model.SAMPLING_POINT) ? "" : model.SAMPLING_POINT;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_TEMPLATE_ROWById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_TEMPLATE_ROW result = new QMS_MA_TEMPLATE_ROW();
            result = QMS_MA_TEMPLATE_ROW.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }


        public long SaveQMS_MA_TEMPLATE_ROW(QMSDBEntities db, CreateQMS_MA_TEMPLATE_ROW model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_TEMPLATE_ROW(db, model);
            }
            else
            {
                result = AddQMS_MA_TEMPLATE_ROW(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_TEMPLATE_ROW(QMSDBEntities db, CreateQMS_MA_TEMPLATE_ROW model)
        {
            long result = 0;

            try
            {
                QMS_MA_TEMPLATE_ROW dt = new QMS_MA_TEMPLATE_ROW();
                dt = QMS_MA_TEMPLATE_ROW.GetById(db, model.ID);

                dt.TEMPLATE_ID = model.TEMPLATE_ID;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.EXCEL_FIELD = (null == model.EXCEL_FIELD) ? "" : model.EXCEL_FIELD;
                dt.SAMPLING_POINT = (null == model.SAMPLING_POINT) ? "" : model.SAMPLING_POINT;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_TEMPLATE_ROW(QMSDBEntities _db, QMS_MA_TEMPLATE_ROW model)
        {
            long result = 0;
            try
            {
                QMS_MA_TEMPLATE_ROW dt = new QMS_MA_TEMPLATE_ROW();
                dt = QMS_MA_TEMPLATE_ROW.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_TEMPLATE_ROW.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_MA_UNIT

        //public List<ViewQMS_MA_UNIT> getQMS_MA_UNITActiveList(QMSDBEntities db)
        //{
        //    List<ViewQMS_MA_UNIT> listResult = new List<ViewQMS_MA_UNIT>();

        //    try
        //    {
        //        List<QMS_MA_UNIT> list = QMS_MA_UNIT.GetActiveAll(db);

        //        if (null != list && list.Count() > 0)
        //        {
        //            listResult = (from dt in list
        //                          select new ViewQMS_MA_UNIT
        //                          {
        //                              ID = dt.ID,
        //                              NAME = dt.NAME
        //                          }).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.writeErrorLog(ex.Message);
        //    }

        //    return listResult;
        //}
		public List<ViewQMS_MA_UNIT> searchQMS_MA_UNIT(QMSDBEntities db, UnitSearch searchModel, out long count, out long totalPage, out  List<PageIndexList> listPageIndex)
		{
            List<ViewQMS_MA_UNIT> objResult = new List<ViewQMS_MA_UNIT>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "NAME";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_UNIT> listData = new List<QMS_MA_UNIT>();
                listData = QMS_MA_UNIT.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_UNIT>(listData.AsQueryable(), out count, out totalPage);
                    objResult = (from dt in query
                                 select new ViewQMS_MA_UNIT
                                 {
                                     ID = dt.ID,
                                     NAME = dt.NAME,
                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER,
                                     SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                     SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"))
                                 }).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_UNIT> getQMS_MA_UNITList(QMSDBEntities db)
        {
            List<ViewQMS_MA_UNIT> listResult = new List<ViewQMS_MA_UNIT>();

            try
            {
                List<QMS_MA_UNIT> list = QMS_MA_UNIT.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_UNIT
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      //DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public CreateQMS_MA_UNIT getQMS_MA_UNITById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_UNIT objResult = new CreateQMS_MA_UNIT();

            try
            {
                QMS_MA_UNIT dt = new QMS_MA_UNIT();
                dt = QMS_MA_UNIT.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private QMS_MA_UNIT convertModelToDB(CreateQMS_MA_UNIT model)
        {
            QMS_MA_UNIT result = new QMS_MA_UNIT();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            //result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_UNIT convertDBToModel(QMS_MA_UNIT model)
        {
            CreateQMS_MA_UNIT result = new CreateQMS_MA_UNIT();

            result.ID = model.ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss",new CultureInfo("en-US"));

            return result;
        }

        public bool DeleteQMS_MA_UNITById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_UNIT result = new QMS_MA_UNIT();
            result = QMS_MA_UNIT.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_UNITByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_UNIT> result = new List<QMS_MA_UNIT>();
            result = QMS_MA_UNIT.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool checkDuplicateName(QMSDBEntities db, CreateQMS_MA_UNIT model)
        {
            bool bResult = false;
             
            try
            {
               var tempData =   QMS_MA_UNIT.CheckDuplicateUnitName(db, model.ID, model.NAME);
               if (null != tempData)
               {
                   bResult = true;
               }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_UNIT(QMSDBEntities db, CreateQMS_MA_UNIT model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_UNIT(db, model);
            }
            else
            {
                result = AddQMS_MA_UNIT(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_UNIT(QMSDBEntities db, CreateQMS_MA_UNIT model)
        {
            long result = 0;

            try
            {
                QMS_MA_UNIT dt = new QMS_MA_UNIT();
                dt = QMS_MA_UNIT.GetById(db, model.ID);

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
               // dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_UNIT(QMSDBEntities _db, QMS_MA_UNIT model)
        {
            long result = 0;
            try
            {
                QMS_MA_UNIT dt = new QMS_MA_UNIT();
                dt = QMS_MA_UNIT.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_UNIT.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion



        #region QMS_MA_IQC_RULE

        public List<ViewQMS_MA_IQC_RULE> searchQMS_MA_IQC_RULE(QMSDBEntities db, IQCRuleSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_RULE> objResult = new List<ViewQMS_MA_IQC_RULE>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "RULE_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_RULE> listData = new List<QMS_MA_IQC_RULE>();
                listData = QMS_MA_IQC_RULE.GetAllBySearch(db, searchModel.mSearch);
            
                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_RULE
                                 {

                                     ID = dt.ID,
                                     RULE_ID = dt.RULE_ID,
                                     RULE_NAME = dt.RULE_NAME,
                                     RULE_DESC = dt.RULE_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_RULE>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_RULE getQMS_MA_IQC_RULEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_RULE objResult = new CreateQMS_MA_IQC_RULE();

            try
            {
                QMS_MA_IQC_RULE dt = new QMS_MA_IQC_RULE();
                dt = QMS_MA_IQC_RULE.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_RULE> getQMS_MA_IQC_RULEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_RULE> listResult = new List<ViewQMS_MA_IQC_RULE>();
      
            try
            {
                List<QMS_MA_IQC_RULE> list = QMS_MA_IQC_RULE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_RULE
                                  {
                                      ID = dt.ID,
                                      RULE_ID = dt.RULE_ID,
                                      RULE_NAME = dt.RULE_NAME,
                                      RULE_DESC = dt.RULE_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_RULE convertModelToDB(CreateQMS_MA_IQC_RULE model)
        {
            QMS_MA_IQC_RULE result = new QMS_MA_IQC_RULE();

            result.ID = model.ID;
            result.RULE_ID = (null == model.RULE_ID) ? "" : model.RULE_ID;
            result.RULE_NAME = (null == model.RULE_NAME) ? "" : model.RULE_NAME;
            result.RULE_DESC = (null == model.RULE_DESC) ? "" : model.RULE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_RULE convertDBToModel(QMSDBEntities db, QMS_MA_IQC_RULE model)
        {
            CreateQMS_MA_IQC_RULE result = new CreateQMS_MA_IQC_RULE();

            result.ID = model.ID;
            result.RULE_ID = (null == model.RULE_ID) ? "" : model.RULE_ID;
            result.RULE_NAME = (null == model.RULE_NAME) ? "" : model.RULE_NAME;
            result.RULE_DESC = (null == model.RULE_DESC) ? "" : model.RULE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));

            
            return result;
        }

        public bool DeleteQMS_MA_IQC_RULEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_RULE result = new QMS_MA_IQC_RULE();
            result = QMS_MA_IQC_RULE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_RULEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_RULE> result = new List<QMS_MA_IQC_RULE>();
            result = QMS_MA_IQC_RULE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_RULE(QMSDBEntities db, CreateQMS_MA_IQC_RULE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_RULE(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_RULE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_RULE(QMSDBEntities db, CreateQMS_MA_IQC_RULE model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_RULE dt = new QMS_MA_IQC_RULE();
                dt = QMS_MA_IQC_RULE.GetById(db, model.ID);

                dt.RULE_ID = (null == model.RULE_ID) ? "" : model.RULE_ID;
                dt.RULE_NAME = (null == model.RULE_NAME) ? "" : model.RULE_NAME;
                dt.RULE_DESC = (null == model.RULE_DESC) ? "" : model.RULE_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_RULE(QMSDBEntities _db, QMS_MA_IQC_RULE model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_RULE dt = new QMS_MA_IQC_RULE();
                dt = QMS_MA_IQC_RULE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_RULE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion



        #region QMS_MA_IQC_POSSICAUSE

        public List<ViewQMS_MA_IQC_POSSICAUSE> searchQMS_MA_IQC_POSSICAUSE(QMSDBEntities db, IQCPossibleCauseSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_POSSICAUSE> objResult = new List<ViewQMS_MA_IQC_POSSICAUSE>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "POSSICAUSE_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_POSSICAUSE> listData = new List<QMS_MA_IQC_POSSICAUSE>();
                listData = QMS_MA_IQC_POSSICAUSE.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_POSSICAUSE
                                 {

                                     ID = dt.ID,
                                     POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                     POSSICAUSE_NAME = dt.POSSICAUSE_NAME,
                                     POSSICAUSE_DESC = dt.POSSICAUSE_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_POSSICAUSE>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_POSSICAUSE getQMS_MA_IQC_POSSICAUSEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_POSSICAUSE objResult = new CreateQMS_MA_IQC_POSSICAUSE();

            try
            {
                QMS_MA_IQC_POSSICAUSE dt = new QMS_MA_IQC_POSSICAUSE();
                dt = QMS_MA_IQC_POSSICAUSE.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_POSSICAUSE> getQMS_MA_IQC_POSSICAUSEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_POSSICAUSE> listResult = new List<ViewQMS_MA_IQC_POSSICAUSE>();

            try
            {
                List<QMS_MA_IQC_POSSICAUSE> list = QMS_MA_IQC_POSSICAUSE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_POSSICAUSE
                                  {
                                      ID = dt.ID,
                                      POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                      POSSICAUSE_NAME = dt.POSSICAUSE_NAME,
                                      POSSICAUSE_DESC = dt.POSSICAUSE_DESC,
                                      POSITION = dt.POSITION,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_POSSICAUSE convertModelToDB(CreateQMS_MA_IQC_POSSICAUSE model)
        {
            QMS_MA_IQC_POSSICAUSE result = new QMS_MA_IQC_POSSICAUSE();

            result.ID = model.ID;
            result.POSSICAUSE_ID =  model.POSSICAUSE_ID;
            result.POSSICAUSE_NAME = (null == model.POSSICAUSE_NAME) ? "" : model.POSSICAUSE_NAME;
            result.POSSICAUSE_DESC = (null == model.POSSICAUSE_DESC) ? "" : model.POSSICAUSE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_POSSICAUSE convertDBToModel(QMSDBEntities db, QMS_MA_IQC_POSSICAUSE model)
        {
            CreateQMS_MA_IQC_POSSICAUSE result = new CreateQMS_MA_IQC_POSSICAUSE();

            result.ID = model.ID;
            result.POSSICAUSE_ID =  model.POSSICAUSE_ID;
            result.POSSICAUSE_NAME = (null == model.POSSICAUSE_NAME) ? "" : model.POSSICAUSE_NAME;
            result.POSSICAUSE_DESC = (null == model.POSSICAUSE_DESC) ? "" : model.POSSICAUSE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_POSSICAUSEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_POSSICAUSE result = new QMS_MA_IQC_POSSICAUSE();
            result = QMS_MA_IQC_POSSICAUSE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_POSSICAUSEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_POSSICAUSE> result = new List<QMS_MA_IQC_POSSICAUSE>();
            result = QMS_MA_IQC_POSSICAUSE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_POSSICAUSE(QMSDBEntities db, CreateQMS_MA_IQC_POSSICAUSE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_POSSICAUSE(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_POSSICAUSE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_POSSICAUSE(QMSDBEntities db, CreateQMS_MA_IQC_POSSICAUSE model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_POSSICAUSE dt = new QMS_MA_IQC_POSSICAUSE();
                dt = QMS_MA_IQC_POSSICAUSE.GetById(db, model.ID);

                dt.POSSICAUSE_ID = model.POSSICAUSE_ID;
                dt.POSSICAUSE_NAME = (null == model.POSSICAUSE_NAME) ? "" : model.POSSICAUSE_NAME;
                dt.POSSICAUSE_DESC = (null == model.POSSICAUSE_DESC) ? "" : model.POSSICAUSE_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_POSSICAUSE(QMSDBEntities _db, QMS_MA_IQC_POSSICAUSE model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_POSSICAUSE dt = new QMS_MA_IQC_POSSICAUSE();
                dt = QMS_MA_IQC_POSSICAUSE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_POSSICAUSE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }



        public List<ViewQMS_MA_IQC_POSSICAUSE> getQMS_MA_IQC_POSSICAUSEActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_POSSICAUSE> listResult = new List<ViewQMS_MA_IQC_POSSICAUSE>();

            try
            {
                List<QMS_MA_IQC_POSSICAUSE> list = QMS_MA_IQC_POSSICAUSE.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_POSSICAUSE
                                  {
                                      ID = dt.ID,
                                      POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                      POSSICAUSE_NAME = dt.POSSICAUSE_NAME,
                                      POSSICAUSE_DESC = dt.POSSICAUSE_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG,

                                      POSITION = dt.POSITION,
                                      ARROW_UP = true,
                                      ARROW_DOWN = true
                                  }).ToList();

                    listResult = listResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                    listResult = setArrowUPAndDown(listResult);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        #endregion



        #region QMS_MA_IQC_METHOD

        public List<ViewQMS_MA_IQC_METHOD> searchQMS_MA_IQC_METHOD(QMSDBEntities db, IQCMethodSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_METHOD> objResult = new List<ViewQMS_MA_IQC_METHOD>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "METHOD_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_METHOD> listData = new List<QMS_MA_IQC_METHOD>();
                listData = QMS_MA_IQC_METHOD.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_METHOD
                                 {

                                     ID = dt.ID,
                                     METHOD_ID = dt.METHOD_ID,
                                     METHOD_NAME = dt.METHOD_NAME,
                                     METHOD_DESC = dt.METHOD_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_METHOD>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_METHOD getQMS_MA_IQC_METHODById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_METHOD objResult = new CreateQMS_MA_IQC_METHOD();

            try
            {
                QMS_MA_IQC_METHOD dt = new QMS_MA_IQC_METHOD();
                dt = QMS_MA_IQC_METHOD.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_METHOD> getQMS_MA_IQC_METHODList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_METHOD> listResult = new List<ViewQMS_MA_IQC_METHOD>();

            try
            {
                List<QMS_MA_IQC_METHOD> list = QMS_MA_IQC_METHOD.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_METHOD
                                  {
                                      ID = dt.ID,
                                      METHOD_ID = dt.METHOD_ID,
                                      METHOD_NAME = dt.METHOD_NAME,
                                      METHOD_DESC = dt.METHOD_DESC,
                                      POSITION = dt.POSITION,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_METHOD convertModelToDB(CreateQMS_MA_IQC_METHOD model)
        {
            QMS_MA_IQC_METHOD result = new QMS_MA_IQC_METHOD();

            result.ID = model.ID;
            result.METHOD_ID = model.METHOD_ID;
            result.METHOD_NAME = (null == model.METHOD_NAME) ? "" : model.METHOD_NAME;
            result.METHOD_DESC = (null == model.METHOD_DESC) ? "" : model.METHOD_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_METHOD convertDBToModel(QMSDBEntities db, QMS_MA_IQC_METHOD model)
        {
            CreateQMS_MA_IQC_METHOD result = new CreateQMS_MA_IQC_METHOD();

            result.ID = model.ID;
            result.METHOD_ID = model.METHOD_ID;
            result.METHOD_NAME = (null == model.METHOD_NAME) ? "" : model.METHOD_NAME;
            result.METHOD_DESC = (null == model.METHOD_DESC) ? "" : model.METHOD_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_METHODById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_METHOD result = new QMS_MA_IQC_METHOD();
            result = QMS_MA_IQC_METHOD.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_METHODByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_METHOD> result = new List<QMS_MA_IQC_METHOD>();
            result = QMS_MA_IQC_METHOD.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_METHOD(QMSDBEntities db, CreateQMS_MA_IQC_METHOD model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_METHOD(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_METHOD(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_METHOD(QMSDBEntities db, CreateQMS_MA_IQC_METHOD model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_METHOD dt = new QMS_MA_IQC_METHOD();
                dt = QMS_MA_IQC_METHOD.GetById(db, model.ID);

                dt.METHOD_ID = model.METHOD_ID;
                dt.METHOD_NAME = (null == model.METHOD_NAME) ? "" : model.METHOD_NAME;
                dt.METHOD_DESC = (null == model.METHOD_DESC) ? "" : model.METHOD_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_METHOD(QMSDBEntities _db, QMS_MA_IQC_METHOD model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_METHOD dt = new QMS_MA_IQC_METHOD();
                dt = QMS_MA_IQC_METHOD.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_METHOD.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }


        public List<ViewQMS_MA_IQC_METHOD> getQMS_MA_IQC_METHODActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_METHOD> listResult = new List<ViewQMS_MA_IQC_METHOD>();

            try
            {
                List<QMS_MA_IQC_METHOD> list = QMS_MA_IQC_METHOD.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_METHOD
                                  {
                                      ID = dt.ID,
                                      METHOD_ID = dt.METHOD_ID,
                                      METHOD_NAME = dt.METHOD_NAME,
                                      METHOD_DESC = dt.METHOD_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG,

                                      POSITION = dt.POSITION
                                  }).ToList();

                    listResult = listResult.OrderBy(m => m.POSITION).ToList(); //ต้อง list ตาม position ด้วย
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

   
        #endregion



        #region QMS_MA_IQC_FORMULA

        public List<ViewQMS_MA_IQC_FORMULA> searchQMS_MA_IQC_FORMULA(QMSDBEntities db, IQCFormulaSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_FORMULA> objResult = new List<ViewQMS_MA_IQC_FORMULA>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "METHOD_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_FORMULA> listData = new List<QMS_MA_IQC_FORMULA>();
                listData = QMS_MA_IQC_FORMULA.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_IQC_ITEM> listIQCTtem = QMS_MA_IQC_ITEM.GetAll(db);
                List<QMS_MA_IQC_METHOD> listIQCMethod = QMS_MA_IQC_METHOD.GetAll(db);
                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_FORMULA
                                 {

                                     ID = dt.ID,
                                     METHOD_ID = dt.METHOD_ID,
                                     FORMULA_REV = dt.FORMULA_REV,
                                     FILE_NAME = dt.FILE_NAME,
                                     METHOD_NAME = getIQCsigmaName(listIQCMethod,dt.METHOD_ID),
                                     FORMULA_PROD = dt.FORMULA_PROD,
                                     FORMULA_PRODNAME = getIQCItemName(listIQCTtem, dt.FORMULA_PROD),
                                     FORMULA_SAMPLE = dt.FORMULA_SAMPLE,
                                     FORMULA_STD = dt.FORMULA_STD,
                                     FORMULA_UP = dt.FORMULA_UP,
                                     FORMULA_UCL = dt.FORMULA_UCL,
                                     UCL_PATTERN = dt.UCL_PATTERN,
                                     FORMULA_UWL = dt.FORMULA_UWL,
                                     UWL_PATTERN = dt.UWL_PATTERN,
                                     FORMULA_AVG = dt.FORMULA_AVG,
                                     AVG_PATTERN = dt.AVG_PATTERN,
                                     FORMULA_LWL = dt.FORMULA_LWL,
                                     LWL_PATTERN = dt.LWL_PATTERN,
                                     FORMULA_LCL = dt.FORMULA_LCL,
                                     LCL_PATTERN = dt.LCL_PATTERN,
                                     FORMULA_LOW = dt.FORMULA_LOW,
                                     FORMULA_DESC = dt.FORMULA_DESC,
                                     ACTIVE_DATE = dt.ACTIVE_DATE,
                                     EXPIRE_DATE = dt.EXPIRE_DATE,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_FORMULA>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_FORMULA getQMS_MA_IQC_FORMULAById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_FORMULA objResult = new CreateQMS_MA_IQC_FORMULA();

            try
            {
                QMS_MA_IQC_FORMULA dt = new QMS_MA_IQC_FORMULA();
                dt = QMS_MA_IQC_FORMULA.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_FORMULA> getQMS_MA_IQC_FORMULAList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_FORMULA> listResult = new List<ViewQMS_MA_IQC_FORMULA>();

            try
            {
                List<QMS_MA_IQC_FORMULA> list = QMS_MA_IQC_FORMULA.GetAll(db);
                List<QMS_MA_IQC_ITEM> listIQCTtem = QMS_MA_IQC_ITEM.GetAll(db);
                List<QMS_MA_IQC_METHOD> listIQCMethod = QMS_MA_IQC_METHOD.GetAll(db);
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_FORMULA
                                  {
                                      ID = dt.ID,
                                      METHOD_ID = dt.METHOD_ID,
                                      FORMULA_REV = dt.FORMULA_REV,
                                      FILE_NAME = dt.FILE_NAME,
                                      METHOD_NAME = getIQCsigmaName(listIQCMethod, dt.METHOD_ID),
                                      FORMULA_NAMEREV = dt.FORMULA_REV , // +"  "+ getIQCsigmaName(List,dt.METHOD_ID),
                                      FORMULA_PROD = dt.FORMULA_PROD,
                                      FORMULA_PRODNAME = getIQCItemName(listIQCTtem, dt.FORMULA_PROD),
                                      FORMULA_SAMPLE = dt.FORMULA_SAMPLE,
                                      FORMULA_STD = dt.FORMULA_STD,
                                      FORMULA_UP = dt.FORMULA_UP,
                                      FORMULA_UCL = dt.FORMULA_UCL,
                                      UCL_PATTERN = dt.UCL_PATTERN,
                                      FORMULA_UWL = dt.FORMULA_UWL,
                                      UWL_PATTERN = dt.UWL_PATTERN,
                                      FORMULA_AVG = dt.FORMULA_AVG,
                                      AVG_PATTERN = dt.AVG_PATTERN,
                                      FORMULA_LWL = dt.FORMULA_LWL,
                                      LWL_PATTERN = dt.LWL_PATTERN,
                                      FORMULA_LCL = dt.FORMULA_LCL,
                                      LCL_PATTERN = dt.LCL_PATTERN,
                                      FORMULA_LOW = dt.FORMULA_LOW,
                                      FORMULA_DESC = dt.FORMULA_DESC,
                                      ACTIVE_DATE = dt.ACTIVE_DATE,
                                      EXPIRE_DATE = dt.EXPIRE_DATE,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_FORMULA convertModelToDB(CreateQMS_MA_IQC_FORMULA model)
        {
            QMS_MA_IQC_FORMULA result = new QMS_MA_IQC_FORMULA();

            result.ID = model.ID;
            result.METHOD_ID =  model.METHOD_ID;
            result.FORMULA_REV = (null == model.FORMULA_REV) ? "" : model.FORMULA_REV;
            result.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
            result.FORMULA_PROD =  model.FORMULA_PROD;
            result.FORMULA_SAMPLE = model.FORMULA_SAMPLE;
            result.FORMULA_STD = model.FORMULA_STD;
            result.FORMULA_UP = model.FORMULA_UP;
            result.FORMULA_UCL = model.FORMULA_UCL;
            result.UCL_PATTERN = (null == model.UCL_PATTERN) ? "" : model.UCL_PATTERN;
            result.FORMULA_UWL = model.FORMULA_UWL;
            result.UWL_PATTERN = (null == model.UWL_PATTERN) ? "" : model.UWL_PATTERN;
            result.FORMULA_AVG = model.FORMULA_AVG;
            result.AVG_PATTERN = (null == model.AVG_PATTERN) ? "" : model.AVG_PATTERN;
            result.FORMULA_LWL = model.FORMULA_LWL;
            result.LWL_PATTERN = (null == model.LWL_PATTERN) ? "" : model.LWL_PATTERN;
            result.FORMULA_LCL = model.FORMULA_LCL;
            result.LCL_PATTERN = (null == model.LCL_PATTERN) ? "" : model.LCL_PATTERN;
            result.FORMULA_LOW = model.FORMULA_LOW;
            result.FORMULA_DESC = (null == model.FORMULA_DESC) ? "" : model.FORMULA_DESC;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.EXPIRE_DATE = model.EXPIRE_DATE;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_FORMULA convertDBToModel(QMSDBEntities db, QMS_MA_IQC_FORMULA model)
        {
            CreateQMS_MA_IQC_FORMULA result = new CreateQMS_MA_IQC_FORMULA();

            result.ID = model.ID;
            result.METHOD_ID =  model.METHOD_ID;
            result.FORMULA_REV = (null == model.FORMULA_REV) ? "" : model.FORMULA_REV;
            result.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
            result.FORMULA_PROD =  model.FORMULA_PROD;
            result.FORMULA_SAMPLE = model.FORMULA_SAMPLE;
            result.FORMULA_STD = model.FORMULA_STD;
            result.FORMULA_UP = model.FORMULA_UP;
            result.FORMULA_UCL = model.FORMULA_UCL;
            result.UCL_PATTERN = (null == model.UCL_PATTERN) ? "" : model.UCL_PATTERN;
            result.FORMULA_UWL = model.FORMULA_UWL;
            result.UWL_PATTERN = (null == model.UWL_PATTERN) ? "" : model.UWL_PATTERN;
            result.FORMULA_AVG = model.FORMULA_AVG;
            result.AVG_PATTERN = (null == model.AVG_PATTERN) ? "" : model.AVG_PATTERN;
            result.FORMULA_LWL = model.FORMULA_LWL;
            result.LWL_PATTERN = (null == model.LWL_PATTERN) ? "" : model.LWL_PATTERN;
            result.FORMULA_LCL = model.FORMULA_LCL;
            result.LCL_PATTERN = (null == model.LCL_PATTERN) ? "" : model.LCL_PATTERN;
            result.FORMULA_LOW = model.FORMULA_LOW;
            result.FORMULA_DESC = (null == model.FORMULA_DESC) ? "" : model.FORMULA_DESC;
            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.EXPIRE_DATE = model.EXPIRE_DATE;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_FORMULAById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_FORMULA result = new QMS_MA_IQC_FORMULA();
            result = QMS_MA_IQC_FORMULA.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_FORMULAByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_FORMULA> result = new List<QMS_MA_IQC_FORMULA>();
            result = QMS_MA_IQC_FORMULA.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_FORMULA(QMSDBEntities db, CreateQMS_MA_IQC_FORMULA model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_FORMULA(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_FORMULA(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_FORMULA(QMSDBEntities db, CreateQMS_MA_IQC_FORMULA model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_FORMULA dt = new QMS_MA_IQC_FORMULA();
                dt = QMS_MA_IQC_FORMULA.GetById(db, model.ID);

                dt.METHOD_ID =  model.METHOD_ID;
                dt.FORMULA_REV = (null == model.FORMULA_REV) ? "" : model.FORMULA_REV;
                dt.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
                dt.FORMULA_PROD =  model.FORMULA_PROD;
                dt.FORMULA_SAMPLE = model.FORMULA_SAMPLE;
                dt.FORMULA_STD = model.FORMULA_STD;
                dt.FORMULA_UP = model.FORMULA_UP;
                dt.FORMULA_UCL = model.FORMULA_UCL;
                dt.UCL_PATTERN = (null == model.UCL_PATTERN) ? "" : model.UCL_PATTERN;
                dt.FORMULA_UWL = model.FORMULA_UWL;
                dt.UWL_PATTERN = (null == model.UWL_PATTERN) ? "" : model.UWL_PATTERN;
                dt.FORMULA_AVG = model.FORMULA_AVG;
                dt.AVG_PATTERN = (null == model.AVG_PATTERN) ? "" : model.AVG_PATTERN;
                dt.FORMULA_LWL = model.FORMULA_LWL;
                dt.LWL_PATTERN = (null == model.LWL_PATTERN) ? "" : model.LWL_PATTERN;
                dt.FORMULA_LCL = model.FORMULA_LCL;
                dt.LCL_PATTERN = (null == model.LCL_PATTERN) ? "" : model.LCL_PATTERN;
                dt.FORMULA_LOW = model.FORMULA_LOW;
                dt.FORMULA_DESC = (null == model.FORMULA_DESC) ? "" : model.FORMULA_DESC;
                dt.ACTIVE_DATE = model.ACTIVE_DATE;
                dt.EXPIRE_DATE = model.EXPIRE_DATE;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_FORMULA(QMSDBEntities _db, QMS_MA_IQC_FORMULA model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_FORMULA dt = new QMS_MA_IQC_FORMULA();
                dt = QMS_MA_IQC_FORMULA.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_FORMULA.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

    
        //public void DailyCOAWriteProductQualityToCSC(DateTime startTime, DateTime endTime, bool bForceDelete)
        //{
        //    try
        //    {
        //        TransactionService transServices = new TransactionService(_currentUserName);
        //        ConfigServices cogServices = new ConfigServices(_currentUserName);
        //        List<QMS_TR_COA_DATA> listCOAData = QMS_TR_COA_DATA.GetByListDateTime(_db, startTime, endTime);
        //        List<CUSTOMER_COA> listCustomerCOA = new List<CUSTOMER_COA>();
        //        ProductQualityToCSCSearchModel searchModel = new ProductQualityToCSCSearchModel();

        //        CreatePRODUCT_CSC_PATH modelCSC = cogServices.getProductCSCPath(_db);// @"\\ryg-app-s01\Lab_Report\Reports\Product Quality to CSC"; //@"F:\Product Quality to CSC"; // @"\\ryg-app-s01\Lab_Report\Reports\Product Quality to CSC";
        //        string szFileName = modelCSC.PRODUCT_CSC_NAME;// @Resource.ResourceString.ProductQualityToCSC;//+ " " + "Feed Quality from PTT_Pang +".xlsx";
        //        string szExtension = ".xlsx";
        //        string szFilePath = "";
        //        string szMonth = "";

        //        searchModel.START_DATE = startTime;
        //        searchModel.END_DATE = endTime;


        //        //.OrderBy(m => m.Key.CUSTOMER_NAME)
        //        if (listCOAData.Count() > 0)
        //        {
        //            listCustomerCOA = listCOAData.GroupBy(m => new { m.CUSTOMER_NAME })
        //                                                 .Select(m => new CUSTOMER_COA
        //                                                 {
        //                                                     CUSTOMER = m.Key.CUSTOMER_NAME,
        //                                                     COA_ID = m.Key.CUSTOMER_NAME
        //                                                 }).OrderBy(m => m.CUSTOMER).ToList();

        //            string curCustomer = null;

        //            foreach (CUSTOMER_COA coaData in listCustomerCOA)
        //            {
        //                ExcelPackage excel = null;

        //                if (null == curCustomer || (curCustomer != coaData.CUSTOMER))
        //                { //เจอ customer ใหม่

        //                    curCustomer = coaData.CUSTOMER;
        //                    searchModel.CUSTOMER_NAME = coaData.CUSTOMER;


        //                    //searchModel.LIST_PRODUCT = listCOAData.Where(m => m.CUSTOMER_NAME == curCustomer).GroupBy(m => m.PRODUCT_NAME)
        //                    //                                 .Select(m => new PRODUCT_COA
        //                    //                                 {
        //                    //                                     PRODUCT = m.Key,//ไม่ได้ใช้ในกรณีนี้
        //                    //                                     COA_ID = m.Key
        //                    //                                 }).ToList();

        //                    searchModel.LIST_PRODUCT = transServices.getProductCOAListByCustomer(_db, curCustomer);

        //                    if (null != searchModel.LIST_PRODUCT && searchModel.LIST_PRODUCT.Count() > 0)
        //                    {
        //                        //searchModel.LIST_PRODUCT = searchModel.LIST_PRODUCT.OrderByDescending(m => m.POSITION).ToList();
        //                        if (true == CreateDirectoryCSC(modelCSC.PRODUCT_CSC_PATH, startTime, curCustomer))
        //                        {
        //                            szMonth = (startTime.Month < 10) ? "0" + startTime.Month.ToString() : startTime.Month.ToString();
        //                            szFilePath = modelCSC.PRODUCT_CSC_PATH + "\\" + curCustomer + "\\" + startTime.Year + "\\" + szMonth + "\\" + szFileName + "_" + curCustomer + "_" + szMonth + "_" + startTime.Year + szExtension;
        //                            excel = GetExcelPackageFromPath(szFilePath, searchModel.LIST_PRODUCT[0].PRODUCT, bForceDelete);


        //                            //ได้ file excel แล้ว render data
        //                            excel = transServices.CSCDataExcelExecute(_db, searchModel, excel, true);
        //                        }
        //                    }
        //                }

        //                if (null != excel)
        //                {
        //                    excel.Save();
        //                    //excel.Dispose();
        //                }

        //            }

        //            //QMS_TR_TEMPLATE_COA template_id = QMS_TR_TEMPLATE_COA.GetByCOA_ID(db, product.COA_ID);
        //            //List<QMS_TR_TEMPLATE_COA_DETAIL> listDetail = QMS_TR_TEMPLATE_COA_DETAIL.GetByCOAId(db, template_id.ID);

        //            //if (!Directory.Exists(PATH_NAME))
        //            //{
        //            //     Try to create the directory.
        //            //    DirectoryInfo di = Directory.CreateDirectory(PATH_NAME);
        //            //}
        //        }
        //    }
        //    catch (IOException ioex)
        //    {
        //        Console.WriteLine(ioex.Message);
        //        this.writeErrorLog(ioex.Message);
        //        if (ioex.InnerException != null)
        //        {
        //            this.writeErrorLog("inner message :" + ioex.InnerException.Message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.writeErrorLog(ex.Message);

        //        if (ex.InnerException != null)
        //        {
        //            this.writeErrorLog("inner message :" + ex.InnerException.Message);
        //        }
        //    }
        //}
  
        #endregion



        #region QMS_MA_IQC_MAILALERT

        public List<ViewQMS_MA_IQC_MAILALERT> searchQMS_MA_IQC_MAILALERT(QMSDBEntities db, IQCMailAlertSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_MAILALERT> objResult = new List<ViewQMS_MA_IQC_MAILALERT>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_MAILALERT> listData = new List<QMS_MA_IQC_MAILALERT>();
                listData = QMS_MA_IQC_MAILALERT.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_IQC_POSSICAUSE> listPossibleCause = QMS_MA_IQC_POSSICAUSE.GetAll(db);
                List<QMS_MA_EMAIL> listEmail = QMS_MA_EMAIL.GetAll(db);


                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_MAILALERT
                                 {

                                     ID = dt.ID,
                                     EMAIL_ID = dt.EMAIL_ID,
                                     EMPLOYEE_ID = getEmployeeName(listEmail, dt.EMAIL_ID),
                                     NAME = getNameMail(listEmail, dt.EMAIL_ID),
                                     EMAIL = getEmail(listEmail, dt.EMAIL_ID),
                                     POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                     POSSICAUSE_NAME = getPossibleName(listPossibleCause, dt.POSSICAUSE_ID),
                                     ALERT_DESC = dt.ALERT_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_MAILALERT>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_MAILALERT getQMS_MA_IQC_MAILALERTById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_MAILALERT objResult = new CreateQMS_MA_IQC_MAILALERT();

            try
            {
                QMS_MA_IQC_MAILALERT dt = new QMS_MA_IQC_MAILALERT();
                dt = QMS_MA_IQC_MAILALERT.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_MAILALERT> getQMS_MA_IQC_MAILALERTList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_MAILALERT> listResult = new List<ViewQMS_MA_IQC_MAILALERT>();

            try
            {
                List<QMS_MA_IQC_MAILALERT> list = QMS_MA_IQC_MAILALERT.GetAll(db);
                List<QMS_MA_EMAIL> listEmail = QMS_MA_EMAIL.GetAll(db);
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_MAILALERT
                                  {
                                      ID = dt.ID,
                                      EMAIL_ID = dt.EMAIL_ID,
                                      POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                      ALERT_DESC = dt.ALERT_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_MAILALERT convertModelToDB(CreateQMS_MA_IQC_MAILALERT model)
        {
            QMS_MA_IQC_MAILALERT result = new QMS_MA_IQC_MAILALERT();

            result.ID = model.ID;
            result.EMAIL_ID =  model.EMAIL_ID;
            result.POSSICAUSE_ID = model.POSSICAUSE_ID;
            result.ALERT_DESC = (null == model.ALERT_DESC) ? "" : model.ALERT_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_MAILALERT convertDBToModel(QMSDBEntities db, QMS_MA_IQC_MAILALERT model)
        {
            CreateQMS_MA_IQC_MAILALERT result = new CreateQMS_MA_IQC_MAILALERT();

            result.ID = model.ID;

            result.EMAIL_ID = model.EMAIL_ID;
            result.POSSICAUSE_ID = model.POSSICAUSE_ID;
            result.ALERT_DESC = (null == model.ALERT_DESC) ? "" : model.ALERT_DESC;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_MAILALERTById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_MAILALERT result = new QMS_MA_IQC_MAILALERT();
            result = QMS_MA_IQC_MAILALERT.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_MAILALERTByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_MAILALERT> result = new List<QMS_MA_IQC_MAILALERT>();
            result = QMS_MA_IQC_MAILALERT.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_MAILALERT(QMSDBEntities db, CreateQMS_MA_IQC_MAILALERT model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_MAILALERT(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_MAILALERT(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_MAILALERT(QMSDBEntities db, CreateQMS_MA_IQC_MAILALERT model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_MAILALERT dt = new QMS_MA_IQC_MAILALERT();
                dt = QMS_MA_IQC_MAILALERT.GetById(db, model.ID);

                
                dt.EMAIL_ID = model.EMAIL_ID;
                dt.POSSICAUSE_ID = model.POSSICAUSE_ID;
                dt.ALERT_DESC = (null == model.ALERT_DESC) ? "" : model.ALERT_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_MAILALERT(QMSDBEntities _db, QMS_MA_IQC_MAILALERT model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_MAILALERT dt = new QMS_MA_IQC_MAILALERT();
                dt = QMS_MA_IQC_MAILALERT.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_MAILALERT.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }



        public List<ViewQMS_MA_EMAIL> getQMS_MA_EMAILActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_EMAIL> listResult = new List<ViewQMS_MA_EMAIL>();

            try
            {
                List<QMS_MA_EMAIL> list = QMS_MA_EMAIL.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_EMAIL
                                  {
                                      ID = dt.ID,    
                                      EMPLOYEE_ID = dt.EMPLOYEE_ID,
                                      NAME = dt.NAME,
                                      POSITION = dt.POSITION,
                                      EMAIL = dt.EMAIL
                                      
                                  }).ToList();

                    
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }



        private CreateQMS_MA_EMAIL convertDBToModel(QMSDBEntities db, QMS_MA_EMAIL model)
        {
            CreateQMS_MA_EMAIL result = new CreateQMS_MA_EMAIL();

            result.ID = model.ID;
            result.ID = model.ID;
            result.GROUP_TYPE = model.GROUP_TYPE;
            result.EMPLOYEE_ID = (null == model.EMPLOYEE_ID) ? "" : model.EMPLOYEE_ID;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.POSITION = (null == model.POSITION) ? "" : model.POSITION;
            result.EMAIL = (null == model.EMAIL) ? "" : model.EMAIL;
            result.UNIT_NAME = (null == model.UNIT_NAME) ? "" : model.UNIT_NAME;
            result.DEPARTMENT = (null == model.DEPARTMENT) ? "" : model.DEPARTMENT;
            //result.DELETE_FLAG = model.DELETE_FLAG;


            //result.CREATE_DATE = DateTime.Now;
            //result.CREATE_USER = _currentUserName;
            //result.UPDATE_DATE = DateTime.Now;
            //result.UPDATE_USER = _currentUserName;

            return result;
        }

        public long SaveQMS_MA_IQC_ADDEMAIL(QMSDBEntities db, CreateQMS_MA_EMAIL model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_ADDEMAIL(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_ADDEMAIL(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_ADDEMAIL(QMSDBEntities db, CreateQMS_MA_EMAIL model)
        {
            long result = 0;

            try
            {
                QMS_MA_EMAIL dt = new QMS_MA_EMAIL();
                dt = QMS_MA_EMAIL.GetById(db, model.ID);

                dt.GROUP_TYPE = model.GROUP_TYPE;
                dt.EMPLOYEE_ID = (null == model.EMPLOYEE_ID) ? "" : model.EMPLOYEE_ID;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.POSITION = (null == model.POSITION) ? "" : model.POSITION;
                dt.EMAIL = (null == model.EMAIL) ? "" : model.EMAIL;
                dt.UNIT_NAME = (null == model.UNIT_NAME) ? "" : model.UNIT_NAME;
                dt.DEPARTMENT = (null == model.DEPARTMENT) ? "" : model.DEPARTMENT;
                
                //dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_ADDEMAIL(QMSDBEntities _db, QMS_MA_EMAIL model)
        {
            long result = 0;
            try
            {
                QMS_MA_EMAIL dt = new QMS_MA_EMAIL();
                dt = QMS_MA_EMAIL.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_EMAIL.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }


        #endregion



        #region QMS_MA_IQC_ITEM

        public List<ViewQMS_MA_IQC_ITEM> searchQMS_MA_IQC_ITEM(QMSDBEntities db, IQCItemSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_ITEM> objResult = new List<ViewQMS_MA_IQC_ITEM>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ITEM_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_ITEM> listData = new List<QMS_MA_IQC_ITEM>();
                listData = QMS_MA_IQC_ITEM.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_ITEM
                                 {

                                     ID = dt.ID,
                                     ITEM_ID = dt.ITEM_ID,
                                     ITEM_NAME = dt.ITEM_NAME,
                                     ITEM_DESC = dt.ITEM_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_ITEM>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_ITEM getQMS_MA_IQC_ITEMById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_ITEM objResult = new CreateQMS_MA_IQC_ITEM();

            try
            {
                QMS_MA_IQC_ITEM dt = new QMS_MA_IQC_ITEM();
                dt = QMS_MA_IQC_ITEM.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_ITEM> getQMS_MA_IQC_ITEMList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_ITEM> listResult = new List<ViewQMS_MA_IQC_ITEM>();

            try
            {
                List<QMS_MA_IQC_ITEM> list = QMS_MA_IQC_ITEM.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_ITEM
                                  {
                                      ID = dt.ID,
                                      ITEM_ID = dt.ITEM_ID,
                                      ITEM_NAME = dt.ITEM_NAME,
                                      ITEM_DESC = dt.ITEM_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_ITEM convertModelToDB(CreateQMS_MA_IQC_ITEM model)
        {
            QMS_MA_IQC_ITEM result = new QMS_MA_IQC_ITEM();

            result.ID = model.ID;
            result.ITEM_ID = model.ITEM_ID;
            result.ITEM_NAME = (null == model.ITEM_NAME) ? "" : model.ITEM_NAME;
            result.ITEM_DESC = (null == model.ITEM_DESC) ? "" : model.ITEM_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_ITEM convertDBToModel(QMSDBEntities db, QMS_MA_IQC_ITEM model)
        {
            CreateQMS_MA_IQC_ITEM result = new CreateQMS_MA_IQC_ITEM();

            result.ID = model.ID;
            result.ITEM_ID = model.ITEM_ID;
            result.ITEM_NAME = (null == model.ITEM_NAME) ? "" : model.ITEM_NAME;
            result.ITEM_DESC = (null == model.ITEM_DESC) ? "" : model.ITEM_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_ITEMById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_ITEM result = new QMS_MA_IQC_ITEM();
            result = QMS_MA_IQC_ITEM.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_ITEMByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_ITEM> result = new List<QMS_MA_IQC_ITEM>();
            result = QMS_MA_IQC_ITEM.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_ITEM(QMSDBEntities db, CreateQMS_MA_IQC_ITEM model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_ITEM(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_ITEM(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_ITEM(QMSDBEntities db, CreateQMS_MA_IQC_ITEM model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_ITEM dt = new QMS_MA_IQC_ITEM();
                dt = QMS_MA_IQC_ITEM.GetById(db, model.ID);

                dt.ITEM_ID = model.ITEM_ID;
                dt.ITEM_NAME = (null == model.ITEM_NAME) ? "" : model.ITEM_NAME;
                dt.ITEM_DESC = (null == model.ITEM_DESC) ? "" : model.ITEM_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_ITEM(QMSDBEntities _db, QMS_MA_IQC_ITEM model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_ITEM dt = new QMS_MA_IQC_ITEM();
                dt = QMS_MA_IQC_ITEM.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_ITEM.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_IQC_ITEM> getQMS_MA_IQC_ITEMActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_ITEM> listResult = new List<ViewQMS_MA_IQC_ITEM>();

            try
            {
                List<QMS_MA_IQC_ITEM> list = QMS_MA_IQC_ITEM.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_ITEM
                                  {
                                      ID = dt.ID,
                                      ITEM_ID = dt.ITEM_ID,
                                      ITEM_NAME = dt.ITEM_NAME,
                                      ITEM_DESC = dt.ITEM_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG

                                  }).ToList();


                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_MA_IQC_EQUIP> getQMS_MA_IQC_EQUIPActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_EQUIP> listResult = new List<ViewQMS_MA_IQC_EQUIP>();

            try
            {
                List<QMS_MA_IQC_EQUIP> list = QMS_MA_IQC_EQUIP.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_EQUIP
                                  {
                                      ID = dt.ID,
                                      EQUIP_ID = dt.EQUIP_ID,
                                      EQUIP_NAME = dt.EQUIP_NAME,
                                      EQUIP_DESC = dt.EQUIP_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG

                                  }).ToList();


                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }


        #endregion



        #region QMS_MA_IQC_EQUIP

        public List<ViewQMS_MA_IQC_EQUIP> searchQMS_MA_IQC_EQUIP(QMSDBEntities db, IQCEquipSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_EQUIP> objResult = new List<ViewQMS_MA_IQC_EQUIP>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "EQUIP_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_EQUIP> listData = new List<QMS_MA_IQC_EQUIP>();
                listData = QMS_MA_IQC_EQUIP.GetAllBySearch(db, searchModel.mSearch);
               


                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_EQUIP
                                 {

                                     ID = dt.ID,                                     
                                     EQUIP_ID = dt.EQUIP_ID,
                                     EQUIP_NAME = dt.EQUIP_NAME,
                                     EQUIP_DESC = dt.EQUIP_DESC,
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_EQUIP>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_EQUIP getQMS_MA_IQC_EQUIPById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_EQUIP objResult = new CreateQMS_MA_IQC_EQUIP();

            try
            {
                QMS_MA_IQC_EQUIP dt = new QMS_MA_IQC_EQUIP();
                dt = QMS_MA_IQC_EQUIP.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_EQUIP> getQMS_MA_IQC_EQUIPList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_EQUIP> listResult = new List<ViewQMS_MA_IQC_EQUIP>();
           
            try
            {
                List<QMS_MA_IQC_EQUIP> list = QMS_MA_IQC_EQUIP.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_EQUIP
                                  {
                                      ID = dt.ID,                                      
                                      EQUIP_ID = dt.EQUIP_ID,
                                      EQUIP_NAME = dt.EQUIP_NAME,
                                      EQUIP_DESC = dt.EQUIP_DESC,
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_EQUIP convertModelToDB(CreateQMS_MA_IQC_EQUIP model)
        {
            QMS_MA_IQC_EQUIP result = new QMS_MA_IQC_EQUIP();

            result.ID = model.ID;
            result.EQUIP_ID = model.EQUIP_ID;
            result.EQUIP_NAME = (null == model.EQUIP_NAME) ? "" : model.EQUIP_NAME;
            result.EQUIP_DESC = (null == model.EQUIP_DESC) ? "" : model.EQUIP_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_EQUIP convertDBToModel(QMSDBEntities db, QMS_MA_IQC_EQUIP model)
        {
            CreateQMS_MA_IQC_EQUIP result = new CreateQMS_MA_IQC_EQUIP();

            result.ID = model.ID;
            result.EQUIP_ID = model.EQUIP_ID;
            result.EQUIP_NAME = (null == model.EQUIP_NAME) ? "" : model.EQUIP_NAME;
            result.EQUIP_DESC = (null == model.EQUIP_DESC) ? "" : model.EQUIP_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_EQUIPById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_EQUIP result = new QMS_MA_IQC_EQUIP();
            result = QMS_MA_IQC_EQUIP.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_EQUIPByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_EQUIP> result = new List<QMS_MA_IQC_EQUIP>();
            result = QMS_MA_IQC_EQUIP.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_EQUIP(QMSDBEntities db, CreateQMS_MA_IQC_EQUIP model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_EQUIP(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_EQUIP(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_EQUIP(QMSDBEntities db, CreateQMS_MA_IQC_EQUIP model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_EQUIP dt = new QMS_MA_IQC_EQUIP();
                dt = QMS_MA_IQC_EQUIP.GetById(db, model.ID);

                dt.EQUIP_ID = model.EQUIP_ID;
                dt.EQUIP_NAME = (null == model.EQUIP_NAME) ? "" : model.EQUIP_NAME;
                dt.EQUIP_DESC = (null == model.EQUIP_DESC) ? "" : model.EQUIP_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_EQUIP(QMSDBEntities _db, QMS_MA_IQC_EQUIP model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_EQUIP dt = new QMS_MA_IQC_EQUIP();
                dt = QMS_MA_IQC_EQUIP.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_EQUIP.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion



        #region QMS_MA_IQC_ITEMEQUIP

        public List<ViewQMS_MA_IQC_ITEMEQUIP> searchQMS_MA_IQC_ITEMEQUIP(QMSDBEntities db, IQCItemEquipSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_ITEMEQUIP> objResult = new List<ViewQMS_MA_IQC_ITEMEQUIP>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ITEM_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_ITEMEQUIP> listData = new List<QMS_MA_IQC_ITEMEQUIP>();
                listData = QMS_MA_IQC_ITEMEQUIP.GetAllBySearch(db, searchModel.mSearch);
                List<QMS_MA_IQC_ITEM> listItem = QMS_MA_IQC_ITEM.GetAll(db);
                List<QMS_MA_IQC_EQUIP> listEquip = QMS_MA_IQC_EQUIP.GetAll(db);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_ITEMEQUIP
                                 {

                                     ID = dt.ID,
                                     ITEM_NAME = getIQCItemName(listItem, dt.ITEM_ID),
                                     EQUIP_ID = dt.EQUIP_ID,
                                     EQUIP_NAME = getIQCEquipName(listEquip, dt.EQUIP_ID),
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_ITEMEQUIP>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

 
        public List<ViewQMS_MA_IQC_ITEMEQUIP> getListQMS_MA_IQC_ITEMEQUIP(QMSDBEntities db, long id)
        {
            List<ViewQMS_MA_IQC_ITEMEQUIP> listResult = new List<ViewQMS_MA_IQC_ITEMEQUIP>();
            List<QMS_MA_IQC_ITEMEQUIP> list = new List<QMS_MA_IQC_ITEMEQUIP>();

            list = QMS_MA_IQC_ITEMEQUIP.GetByListItemEquipId(db, id);
            

            if (null != list && list.Count() > 0)
            {
                listResult = convertListData(db, list);
                listResult = listResult.OrderBy(m => m.EQUIP_ID).ToList(); 
            }

            return listResult;
        }

        private List<ViewQMS_MA_IQC_ITEMEQUIP> convertListData(QMSDBEntities db, List<QMS_MA_IQC_ITEMEQUIP> list)
        {
            List<ViewQMS_MA_IQC_ITEMEQUIP> listResult = new List<ViewQMS_MA_IQC_ITEMEQUIP>();
            List<QMS_MA_IQC_ITEM> listItem = QMS_MA_IQC_ITEM.GetAll(db);
            List<QMS_MA_IQC_EQUIP> listEquip = QMS_MA_IQC_EQUIP.GetAll(db);

            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_ITEMEQUIP
                                  {
                                      ID = dt.ID,
                                      ITEM_ID = dt.ITEM_ID,
                                      ITEM_NAME = getIQCItemName(listItem, dt.ITEM_ID),
                                      EQUIP_ID = dt.EQUIP_ID,
                                      EQUIP_NAME = getIQCEquipName(listEquip, dt.EQUIP_ID),
                                      ITEMEQUIP_NAME = getIQCItemName(listItem, dt.ITEM_ID) + " (" + getIQCEquipName(listEquip, dt.EQUIP_ID) + ") ",

                                      DELETE_FLAG = dt.DELETE_FLAG

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }


        public CreateQMS_MA_IQC_ITEMEQUIP getQMS_MA_IQC_ITEMEQUIPById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_ITEMEQUIP objResult = new CreateQMS_MA_IQC_ITEMEQUIP();

            try
            {
                QMS_MA_IQC_ITEMEQUIP dt = new QMS_MA_IQC_ITEMEQUIP();
                dt = QMS_MA_IQC_ITEMEQUIP.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_ITEMEQUIP> getQMS_MA_IQC_ITEMEQUIPList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_ITEMEQUIP> listResult = new List<ViewQMS_MA_IQC_ITEMEQUIP>();

            try
            {
                List<QMS_MA_IQC_ITEMEQUIP> list = QMS_MA_IQC_ITEMEQUIP.GetAll(db);
                List<QMS_MA_IQC_ITEM> listItem = QMS_MA_IQC_ITEM.GetAll(db);
                List<QMS_MA_IQC_EQUIP> listEquip = QMS_MA_IQC_EQUIP.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_ITEMEQUIP
                                  {
                                      ID = dt.ID,
                                      ITEM_ID = dt.ITEM_ID,
                                      ITEM_NAME = getIQCItemName(listItem, dt.ITEM_ID),
                                      EQUIP_ID = dt.EQUIP_ID,
                                      EQUIP_NAME = getIQCEquipName(listEquip, dt.EQUIP_ID),
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_ITEMEQUIP convertModelToDB(CreateQMS_MA_IQC_ITEMEQUIP model)
        {
            QMS_MA_IQC_ITEMEQUIP result = new QMS_MA_IQC_ITEMEQUIP();

            result.ID = model.ID;
            result.ITEM_ID = model.ITEM_ID;
            result.EQUIP_ID = model.EQUIP_ID;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_ITEMEQUIP convertDBToModel(QMSDBEntities db, QMS_MA_IQC_ITEMEQUIP model)
        {
            CreateQMS_MA_IQC_ITEMEQUIP result = new CreateQMS_MA_IQC_ITEMEQUIP();

            result.ID = model.ID;
            result.ITEM_ID = model.ITEM_ID;
            result.EQUIP_ID = model.EQUIP_ID;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_ITEMEQUIPById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_ITEMEQUIP result = new QMS_MA_IQC_ITEMEQUIP();
            result = QMS_MA_IQC_ITEMEQUIP.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_ITEMEQUIPByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_ITEMEQUIP> result = new List<QMS_MA_IQC_ITEMEQUIP>();
            result = QMS_MA_IQC_ITEMEQUIP.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_ITEMEQUIP(QMSDBEntities db, CreateQMS_MA_IQC_ITEMEQUIP model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_ITEMEQUIP(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_ITEMEQUIP(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_ITEMEQUIP(QMSDBEntities db, CreateQMS_MA_IQC_ITEMEQUIP model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_ITEMEQUIP dt = new QMS_MA_IQC_ITEMEQUIP();
                dt = QMS_MA_IQC_ITEMEQUIP.GetById(db, model.ID);

                dt.ITEM_ID = model.ITEM_ID;
                dt.EQUIP_ID = model.EQUIP_ID;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_ITEMEQUIP(QMSDBEntities _db, QMS_MA_IQC_ITEMEQUIP model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_ITEMEQUIP dt = new QMS_MA_IQC_ITEMEQUIP();
                dt = QMS_MA_IQC_ITEMEQUIP.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_ITEMEQUIP.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public List<ViewQMS_MA_IQC_ITEMEQUIP> getQMS_MA_IQC_ITEMEQUIPActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_ITEMEQUIP> listResult = new List<ViewQMS_MA_IQC_ITEMEQUIP>();

            try
            {
                List<QMS_MA_IQC_ITEMEQUIP> list = QMS_MA_IQC_ITEMEQUIP.GetActiveAll(db);
                List<QMS_MA_IQC_ITEM> listItem = QMS_MA_IQC_ITEM.GetAll(db);
                List<QMS_MA_IQC_EQUIP> listEquip = QMS_MA_IQC_EQUIP.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_ITEMEQUIP
                                  {
                                      ID = dt.ID,
                                      ITEM_ID = dt.ITEM_ID,
                                      ITEM_NAME = getIQCItemName(listItem, dt.ITEM_ID),
                                      EQUIP_ID = dt.EQUIP_ID,
                                      EQUIP_NAME = getIQCEquipName(listEquip, dt.EQUIP_ID),
                                      ITEMEQUIP_NAME = getIQCItemName(listItem, dt.ITEM_ID) + " (" + getIQCEquipName(listEquip, dt.EQUIP_ID) + ") ",

                                      DELETE_FLAG = dt.DELETE_FLAG

                                  }).ToList();


                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }



        #endregion

        

        #region QMS_MA_IQC_TEMPLATE

   
        private List<ViewQMS_MA_IQC_TEMPLATE> convertListData(QMSDBEntities db, List<QMS_MA_IQC_TEMPLATE> list)
        {
            List<ViewQMS_MA_IQC_TEMPLATE> listResult = new List<ViewQMS_MA_IQC_TEMPLATE>();
            List<QMS_MA_IQC_ITEMEQUIP> listItemEquip = QMS_MA_IQC_ITEMEQUIP.GetAll(db);
            List<QMS_MA_IQC_METHOD> listIQCMethod = QMS_MA_IQC_METHOD.GetAll(db);
            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_TEMPLATE
                                  {
                                      ID = dt.ID,
                                      NAME = dt.NAME,
                                      FILE_NAME = dt.FILE_NAME,
                                      PATH_NAME = dt.PATH_NAME,
                                      SHEET_NAME = dt.SHEET_NAME,
                                      COLUMN_DATE = dt.COLUMN_DATE,
                                      COLUMN_DATA = dt.COLUMN_DATA,
                                      COLUMN_UNIT = dt.COLUMN_UNIT,
                                      ROW_START = dt.ROW_START,
                                      ACTIVE_DATE = dt.ACTIVE_DATE,
                                      EXPIRE_DATE = dt.EXPIRE_DATE,
                                      TEMPLATE_DESC = dt.TEMPLATE_DESC,
                                      TEMPLATE_TYPE = dt.TEMPLATE_TYPE,


                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<QMS_MA_IQC_TEMPLATE> GetAllByIQCTemplateType(QMSDBEntities db, byte templateType)
        {
            List<QMS_MA_IQC_TEMPLATE> listData = new List<QMS_MA_IQC_TEMPLATE>();

            try
            {
                listData = QMS_MA_IQC_TEMPLATE.GetAllByTemplateType(db, templateType);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listData;
        }

        public List<QMS_MA_IQC_TEMPLATE> GetAllByIQCTemplateTypeAndActiveDate(QMSDBEntities db, DateTime startDate, DateTime endDate, byte templateType)
        {
            List<QMS_MA_IQC_TEMPLATE> listData = new List<QMS_MA_IQC_TEMPLATE>();

            try
            {
                listData = QMS_MA_IQC_TEMPLATE.GetAllByIQCTemplateTypeAndActiveDate(db, startDate, endDate, templateType);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listData;
        }

        public List<ViewQMS_MA_IQC_TEMPLATE> searchQMS_MA_IQC_TEMPLATE(QMSDBEntities db, IQCTemplateSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_TEMPLATE> objResult = new List<ViewQMS_MA_IQC_TEMPLATE>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_TEMPLATE> listData = new List<QMS_MA_IQC_TEMPLATE>();
                listData = QMS_MA_IQC_TEMPLATE.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_MA_IQC_TEMPLATE>(listData.AsQueryable(), out count, out totalPage).ToList();
                    objResult = convertListData(db, query);
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_TEMPLATE getQMS_MA_IQC_TEMPLATEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_TEMPLATE objResult = new CreateQMS_MA_IQC_TEMPLATE();

            try
            {
                QMS_MA_IQC_TEMPLATE dt = new QMS_MA_IQC_TEMPLATE();
                dt = QMS_MA_IQC_TEMPLATE.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_TEMPLATE> getQMS_MA_IQC_TEMPLATEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_TEMPLATE> listResult = new List<ViewQMS_MA_IQC_TEMPLATE>();

            try
            {
                List<QMS_MA_IQC_TEMPLATE> list = QMS_MA_IQC_TEMPLATE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public CreateQMS_MA_IQC_TEMPLATE convertDBToModel(QMSDBEntities db, QMS_MA_IQC_TEMPLATE model)
        {
            CreateQMS_MA_IQC_TEMPLATE result = new CreateQMS_MA_IQC_TEMPLATE();
            List<QMS_MA_IQC_ITEMEQUIP> listItemEquip = QMS_MA_IQC_ITEMEQUIP.GetAll(db);

            result.ID = model.ID;
            result.TEMPLATE_TYPE = model.TEMPLATE_TYPE;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
            result.PATH_NAME = (null == model.PATH_NAME) ? "" : model.PATH_NAME;
            result.SHEET_NAME = (null == model.SHEET_NAME) ? "" : model.SHEET_NAME;
            result.COLUMN_DATE = (null == model.COLUMN_DATE) ? "" : model.COLUMN_DATE;
            result.COLUMN_DATA = (null == model.COLUMN_DATA) ? "" : model.COLUMN_DATA;
            result.COLUMN_UNIT = model.COLUMN_UNIT;
            result.ROW_START = model.ROW_START;
            result.TEMPLATE_DESC = (null == model.TEMPLATE_DESC) ? "" : model.TEMPLATE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.EXPIRE_DATE = model.EXPIRE_DATE;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));

            return result;
        }

        private QMS_MA_IQC_TEMPLATE convertModelToDB(CreateQMS_MA_IQC_TEMPLATE model)
        {
            QMS_MA_IQC_TEMPLATE result = new QMS_MA_IQC_TEMPLATE();

            result.ID = model.ID;
            result.TEMPLATE_TYPE = model.TEMPLATE_TYPE;
            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
            result.PATH_NAME = (null == model.PATH_NAME) ? "" : model.PATH_NAME;
            result.SHEET_NAME = (null == model.SHEET_NAME) ? "" : model.SHEET_NAME;
            result.COLUMN_DATE = (null == model.COLUMN_DATE) ? "" : model.COLUMN_DATE;
            result.COLUMN_DATA = (null == model.COLUMN_DATA) ? "" : model.COLUMN_DATA;
            result.COLUMN_UNIT = model.COLUMN_UNIT;
            result.ROW_START = model.ROW_START;
            result.TEMPLATE_DESC = (null == model.TEMPLATE_DESC) ? "" : model.TEMPLATE_DESC;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.ACTIVE_DATE = model.ACTIVE_DATE;
            result.EXPIRE_DATE = model.EXPIRE_DATE;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_IQC_TEMPLATEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_TEMPLATE result = new QMS_MA_IQC_TEMPLATE();
            result = QMS_MA_IQC_TEMPLATE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_TEMPLATE_DATAByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_TEMPLATE> result = new List<QMS_MA_IQC_TEMPLATE>();
            result = QMS_MA_IQC_TEMPLATE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_IQC_TEMPLATE(QMSDBEntities db, CreateQMS_MA_IQC_TEMPLATE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_TEMPLATE(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_TEMPLATE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_TEMPLATE(QMSDBEntities db, CreateQMS_MA_IQC_TEMPLATE model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_TEMPLATE dt = new QMS_MA_IQC_TEMPLATE();
                dt = QMS_MA_IQC_TEMPLATE.GetById(db, model.ID);

                dt.TEMPLATE_TYPE = model.TEMPLATE_TYPE;
                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.FILE_NAME = (null == model.FILE_NAME) ? "" : model.FILE_NAME;
                dt.PATH_NAME = (null == model.PATH_NAME) ? "" : model.PATH_NAME;
                dt.SHEET_NAME = (null == model.SHEET_NAME) ? "" : model.SHEET_NAME;
                dt.COLUMN_DATE = (null == model.COLUMN_DATE) ? "" : model.COLUMN_DATE;
                dt.COLUMN_DATA = (null == model.COLUMN_DATA) ? "" : model.COLUMN_DATA;
                dt.COLUMN_UNIT = model.COLUMN_UNIT;
                dt.ROW_START = model.ROW_START;
                dt.TEMPLATE_DESC = (null == model.TEMPLATE_DESC) ? "" : model.TEMPLATE_DESC;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                dt.ACTIVE_DATE = model.ACTIVE_DATE;
                dt.EXPIRE_DATE = model.EXPIRE_DATE;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_TEMPLATE(QMSDBEntities _db, QMS_MA_IQC_TEMPLATE model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_TEMPLATE dt = new QMS_MA_IQC_TEMPLATE();
                dt = QMS_MA_IQC_TEMPLATE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_TEMPLATE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion



        #region QMS_MA_IQC_CONTROLDATA


        private List<ViewQMS_MA_IQC_CONTROLDATA> convertListData(QMSDBEntities db, List<QMS_MA_IQC_CONTROLDATA> list ,IQCControlDataSearch searchModel)
        {
            List<ViewQMS_MA_IQC_CONTROLDATA> listResult = new List<ViewQMS_MA_IQC_CONTROLDATA>();

            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_CONTROLDATA
                                  {
                                      ID = dt.ID,
                                      TEMPLATE_ID = dt.TEMPLATE_ID,
                                      KEEPTIME = dt.KEEPTIME,
                                      KEEPVALUE = dt.KEEPVALUE,
                                      ITEM_ID = searchModel.mSearch.ITEM_ID,
                                      METHOD_ID = searchModel.mSearch.METHOD_ID,
                                      FORMULA_UCL = getIQCControlDataUCL(db, searchModel, dt.KEEPTIME),
                                      FORMULA_UWL = getIQCControlDataUWL(db, searchModel, dt.KEEPTIME),
                                      FORMULA_AVG = getIQCControlDataAVG(db, searchModel, dt.KEEPTIME),
                                      FORMULA_LWL = getIQCControlDataLWL(db, searchModel, dt.KEEPTIME),
                                      FORMULA_LCL = getIQCControlDataLCL(db, searchModel, dt.KEEPTIME),
                                      DELETE_FLAG = dt.DELETE_FLAG,

                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),
                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US")),

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return listResult;
        }

        public List<QMS_MA_IQC_CONTROLDATA> GetAllByIQCTemplateID(QMSDBEntities db, byte templateId)
        {
            List<QMS_MA_IQC_CONTROLDATA> listData = new List<QMS_MA_IQC_CONTROLDATA>();

            try
            {
                listData = QMS_MA_IQC_CONTROLDATA.GetAllByTemplateId(db, templateId);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listData;
        }

        public List<ViewQMS_MA_IQC_CONTROLDATA> getQMS_MA_IQC_CONTROLDATAList(QMSDBEntities db,IQCControlDataSearch searchModel)
        {
            List<ViewQMS_MA_IQC_CONTROLDATA> listResult = new List<ViewQMS_MA_IQC_CONTROLDATA>();

            try
            {
                List<QMS_MA_IQC_CONTROLDATA> list = QMS_MA_IQC_CONTROLDATA.GetAllBySearch(db,searchModel.mSearch);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_CONTROLDATA
                                  {
                                      ID = dt.ID,
                                      TEMPLATE_ID = dt.TEMPLATE_ID,
                                      KEEPTIME = dt.KEEPTIME,
                                      KEEPVALUE = dt.KEEPVALUE,
                                      ITEM_ID = searchModel.mSearch.ITEM_ID,
                                      METHOD_ID = searchModel.mSearch.METHOD_ID,
                                      FORMULA_UCL = getIQCControlDataUCL(db, searchModel, dt.KEEPTIME),
                                      FORMULA_UWL = getIQCControlDataUWL(db, searchModel, dt.KEEPTIME),
                                      FORMULA_AVG = getIQCControlDataAVG(db, searchModel, dt.KEEPTIME),
                                      FORMULA_LWL = getIQCControlDataLWL(db, searchModel, dt.KEEPTIME),
                                      FORMULA_LCL = getIQCControlDataLCL(db, searchModel, dt.KEEPTIME),
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }


        public List<ViewQMS_MA_IQC_CONTROLDATA> searchQMS_MA_IQC_CONTROLDATA(QMSDBEntities db, IQCControlDataSearch searchModel)
        {
            List<ViewQMS_MA_IQC_CONTROLDATA> objResult = new List<ViewQMS_MA_IQC_CONTROLDATA>();
            
            try
            {
                List<QMS_MA_IQC_CONTROLDATA> listData = new List<QMS_MA_IQC_CONTROLDATA>();
                listData = QMS_MA_IQC_CONTROLDATA.GetAllBySearch(db, searchModel.mSearch);

                objResult = convertListData(db, listData, searchModel);
               
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_CONTROLDATA getQMS_MA_IQC_CONTROLDATAById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_CONTROLDATA objResult = new CreateQMS_MA_IQC_CONTROLDATA();

            try
            {
                QMS_MA_IQC_CONTROLDATA dt = new QMS_MA_IQC_CONTROLDATA();
                dt = QMS_MA_IQC_CONTROLDATA.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_CONTROLDATA convertDBToModel(QMSDBEntities db, QMS_MA_IQC_CONTROLDATA model)
        {
            CreateQMS_MA_IQC_CONTROLDATA result = new CreateQMS_MA_IQC_CONTROLDATA();
  
            result.ID = model.ID;
            result.TEMPLATE_ID = model.TEMPLATE_ID;
            result.KEEPTIME = model.KEEPTIME;
            result.KEEPVALUE = model.KEEPVALUE;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));

            return result;
        }

        private QMS_MA_IQC_CONTROLDATA convertModelToDB(CreateQMS_MA_IQC_CONTROLDATA model)
        {
            QMS_MA_IQC_CONTROLDATA result = new QMS_MA_IQC_CONTROLDATA();

            result.ID = model.ID;
            result.TEMPLATE_ID = model.TEMPLATE_ID;
            result.KEEPTIME = model.KEEPTIME;
            result.KEEPVALUE = model.KEEPVALUE;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public bool DeleteQMS_MA_IQC_CONTROLDATAById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_CONTROLDATA result = new QMS_MA_IQC_CONTROLDATA();
            result = QMS_MA_IQC_CONTROLDATA.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_CONTROLDATA_DATAByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_CONTROLDATA> result = new List<QMS_MA_IQC_CONTROLDATA>();
            result = QMS_MA_IQC_CONTROLDATA.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_MA_IQC_CONTROLDATA(QMSDBEntities db, CreateQMS_MA_IQC_CONTROLDATA model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_CONTROLDATA(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_CONTROLDATA(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_CONTROLDATA(QMSDBEntities db, CreateQMS_MA_IQC_CONTROLDATA model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_CONTROLDATA dt = new QMS_MA_IQC_CONTROLDATA();
                dt = QMS_MA_IQC_CONTROLDATA.GetById(db, model.ID);

                dt.TEMPLATE_ID = model.TEMPLATE_ID;
                dt.KEEPTIME = model.KEEPTIME;
                dt.KEEPVALUE = model.KEEPVALUE;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_CONTROLDATA(QMSDBEntities _db, QMS_MA_IQC_CONTROLDATA model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_CONTROLDATA dt = new QMS_MA_IQC_CONTROLDATA();
                dt = QMS_MA_IQC_CONTROLDATA.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_CONTROLDATA.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion



        #region QMS_MA_IQC_CONTROLHISTORY

        public List<ViewQMS_MA_IQC_CONTROLHISTORY> searchQMS_MA_IQC_CONTROLHISTORY(QMSDBEntities db, IQCControlHistorySearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_CONTROLHISTORY> objResult = new List<ViewQMS_MA_IQC_CONTROLHISTORY>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "HISTORY_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_CONTROLHISTORY> listData = new List<QMS_MA_IQC_CONTROLHISTORY>();
                listData = QMS_MA_IQC_CONTROLHISTORY.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_CONTROLHISTORY
                                 {

                                     ID = dt.ID,
                                     TEMPLATE_ID = dt.TEMPLATE_ID,
                                     STARTEDGE = dt.STARTEDGE,
                                     ENDEDGE = dt.ENDEDGE,
                                     TITLE = dt.TITLE,
                                     SENDEMAIL = dt.SENDEMAIL, 
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_CONTROLHISTORY>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_CONTROLHISTORY getQMS_MA_IQC_CONTROLHISTORYById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_CONTROLHISTORY objResult = new CreateQMS_MA_IQC_CONTROLHISTORY();

            try
            {
                QMS_MA_IQC_CONTROLHISTORY dt = new QMS_MA_IQC_CONTROLHISTORY();
                dt = QMS_MA_IQC_CONTROLHISTORY.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_CONTROLHISTORY> getQMS_MA_IQC_CONTROLHISTORYList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_CONTROLHISTORY> listResult = new List<ViewQMS_MA_IQC_CONTROLHISTORY>();

            try
            {
                List<QMS_MA_IQC_CONTROLHISTORY> list = QMS_MA_IQC_CONTROLHISTORY.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_CONTROLHISTORY
                                  {
                                      ID = dt.ID,
                                      TEMPLATE_ID = dt.TEMPLATE_ID,
                                      STARTEDGE = dt.STARTEDGE,
                                      ENDEDGE = dt.ENDEDGE,
                                      TITLE = dt.TITLE,
                                      SENDEMAIL = dt.SENDEMAIL, 
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_CONTROLHISTORY convertModelToDB(CreateQMS_MA_IQC_CONTROLHISTORY model)
        {
            QMS_MA_IQC_CONTROLHISTORY result = new QMS_MA_IQC_CONTROLHISTORY();

            result.ID = model.ID;
            result.TEMPLATE_ID = model.TEMPLATE_ID;
            result.STARTEDGE = model.STARTEDGE;
            result.ENDEDGE = model.ENDEDGE;
            result.TITLE = (null == model.TITLE) ? "" : model.TITLE;
            result.SENDEMAIL = model.SENDEMAIL;
            
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_CONTROLHISTORY convertDBToModel(QMSDBEntities db, QMS_MA_IQC_CONTROLHISTORY model)
        {
            CreateQMS_MA_IQC_CONTROLHISTORY result = new CreateQMS_MA_IQC_CONTROLHISTORY();

            result.ID = model.ID;
            result.TEMPLATE_ID = model.TEMPLATE_ID;
            result.STARTEDGE = model.STARTEDGE;
            result.ENDEDGE = model.ENDEDGE;
            result.TITLE = (null == model.TITLE) ? "" : model.TITLE;
            result.SENDEMAIL = model.SENDEMAIL;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_CONTROLHISTORYById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_CONTROLHISTORY result = new QMS_MA_IQC_CONTROLHISTORY();
            result = QMS_MA_IQC_CONTROLHISTORY.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_CONTROLHISTORYByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_CONTROLHISTORY> result = new List<QMS_MA_IQC_CONTROLHISTORY>();
            result = QMS_MA_IQC_CONTROLHISTORY.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_CONTROLHISTORY(QMSDBEntities db, CreateQMS_MA_IQC_CONTROLHISTORY model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_CONTROLHISTORY(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_CONTROLHISTORY(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_CONTROLHISTORY(QMSDBEntities db, CreateQMS_MA_IQC_CONTROLHISTORY model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_CONTROLHISTORY dt = new QMS_MA_IQC_CONTROLHISTORY();
                dt = QMS_MA_IQC_CONTROLHISTORY.GetById(db, model.ID);

                dt.TEMPLATE_ID = model.TEMPLATE_ID;
                dt.STARTEDGE = model.STARTEDGE;
                dt.ENDEDGE = model.ENDEDGE;
                dt.TITLE = (null == model.TITLE) ? "" : model.TITLE;
                dt.SENDEMAIL = model.SENDEMAIL;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_CONTROLHISTORY(QMSDBEntities _db, QMS_MA_IQC_CONTROLHISTORY model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_CONTROLHISTORY dt = new QMS_MA_IQC_CONTROLHISTORY();
                dt = QMS_MA_IQC_CONTROLHISTORY.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_CONTROLHISTORY.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }


        public List<ViewQMS_MA_IQC_CONTROLHISTORY> getQMS_MA_IQC_CONTROLHISTORYActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_CONTROLHISTORY> listResult = new List<ViewQMS_MA_IQC_CONTROLHISTORY>();

            try
            {
                List<QMS_MA_IQC_CONTROLHISTORY> list = QMS_MA_IQC_CONTROLHISTORY.GetActiveAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_CONTROLHISTORY
                                  {
                                      ID = dt.ID,
                                      TEMPLATE_ID = dt.TEMPLATE_ID,
                                      STARTEDGE = dt.STARTEDGE,
                                      ENDEDGE = dt.ENDEDGE,
                                      TITLE = dt.TITLE,
                                      SENDEMAIL = dt.SENDEMAIL, 
                                      DELETE_FLAG = dt.DELETE_FLAG,

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        #endregion



        #region QMS_MA_IQC_CONTROLRULE

        public List<ViewQMS_MA_IQC_CONTROLRULE> searchQMS_MA_IQC_CONTROLRULE(QMSDBEntities db, IQCControlRuleSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_MA_IQC_CONTROLRULE> objResult = new List<ViewQMS_MA_IQC_CONTROLRULE>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "RULE_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_MA_IQC_CONTROLRULE> listData = new List<QMS_MA_IQC_CONTROLRULE>();
                List<QMS_MA_IQC_RULE> listRule = QMS_MA_IQC_RULE.GetAll(db);
                List<QMS_MA_IQC_POSSICAUSE> listPossibleCause = QMS_MA_IQC_POSSICAUSE.GetAll(db);

                listData = QMS_MA_IQC_CONTROLRULE.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_MA_IQC_CONTROLRULE
                                 {

                                     ID = dt.ID,
                                     HISTORY_ID = dt.HISTORY_ID,
                                     RULE_ID = dt.RULE_ID,
                                     RULE_NAME = getIQCRuleName(listRule, dt.RULE_ID),
                                     POINT_START = dt.POINT_START,
                                     POINT_END = dt.POINT_END,
                                     POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                     POSSICAUSE_NAME = getPossibleName(listPossibleCause, dt.POSSICAUSE_ID),
                                     REASON = dt.REASON,
                                     
                                     DELETE_FLAG = dt.DELETE_FLAG
                                 }).ToList();

                    objResult = grid.LoadGridData<ViewQMS_MA_IQC_CONTROLRULE>(objResult.AsQueryable(), out count, out totalPage).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_MA_IQC_CONTROLRULE getQMS_MA_IQC_CONTROLRULEById(QMSDBEntities db, long id)
        {
            CreateQMS_MA_IQC_CONTROLRULE objResult = new CreateQMS_MA_IQC_CONTROLRULE();

            try
            {
                QMS_MA_IQC_CONTROLRULE dt = new QMS_MA_IQC_CONTROLRULE();
                dt = QMS_MA_IQC_CONTROLRULE.GetById(db, id);

                objResult = convertDBToModel(db, dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_MA_IQC_CONTROLRULE> getQMS_MA_IQC_CONTROLRULEList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_CONTROLRULE> listResult = new List<ViewQMS_MA_IQC_CONTROLRULE>();

            try
            {
                List<QMS_MA_IQC_CONTROLRULE> list = QMS_MA_IQC_CONTROLRULE.GetAll(db);
                List<QMS_MA_IQC_RULE> listRule = QMS_MA_IQC_RULE.GetAll(db);
                List<QMS_MA_IQC_POSSICAUSE> listPossibleCause = QMS_MA_IQC_POSSICAUSE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_CONTROLRULE
                                  {
                                      ID = dt.ID,
                                      HISTORY_ID = dt.HISTORY_ID,
                                      RULE_ID = dt.RULE_ID,
                                      RULE_NAME = getIQCRuleName(listRule, dt.RULE_ID),
                                      POINT_START = dt.POINT_START,
                                      POINT_END = dt.POINT_END,
                                      POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                      POSSICAUSE_NAME = getPossibleName(listPossibleCause, dt.POSSICAUSE_ID),
                                      DELETE_FLAG = dt.DELETE_FLAG
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_MA_IQC_CONTROLRULE convertModelToDB(CreateQMS_MA_IQC_CONTROLRULE model)
        {
            QMS_MA_IQC_CONTROLRULE result = new QMS_MA_IQC_CONTROLRULE();

            result.ID = model.ID;
            result.HISTORY_ID = model.HISTORY_ID;
            result.RULE_ID = model.RULE_ID;
            result.POINT_START = model.POINT_START;
            result.POINT_END = model.POINT_END;
            result.POSSICAUSE_ID = model.POSSICAUSE_ID;
            result.DELETE_FLAG = model.DELETE_FLAG;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_MA_IQC_CONTROLRULE convertDBToModel(QMSDBEntities db, QMS_MA_IQC_CONTROLRULE model)
        {
            CreateQMS_MA_IQC_CONTROLRULE result = new CreateQMS_MA_IQC_CONTROLRULE();

            result.ID = model.ID;
            result.HISTORY_ID = model.HISTORY_ID;
            result.RULE_ID = model.RULE_ID;
            result.POINT_START = model.POINT_START;
            result.POINT_END = model.POINT_END;
            result.POSSICAUSE_ID = model.POSSICAUSE_ID;
            result.DELETE_FLAG = model.DELETE_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));


            return result;
        }

        public bool DeleteQMS_MA_IQC_CONTROLRULEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_MA_IQC_CONTROLRULE result = new QMS_MA_IQC_CONTROLRULE();
            result = QMS_MA_IQC_CONTROLRULE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_MA_IQC_CONTROLRULEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_MA_IQC_CONTROLRULE> result = new List<QMS_MA_IQC_CONTROLRULE>();
            result = QMS_MA_IQC_CONTROLRULE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }



        public long SaveQMS_MA_IQC_CONTROLRULE(QMSDBEntities db, CreateQMS_MA_IQC_CONTROLRULE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_MA_IQC_CONTROLRULE(db, model);
            }
            else
            {
                result = AddQMS_MA_IQC_CONTROLRULE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_MA_IQC_CONTROLRULE(QMSDBEntities db, CreateQMS_MA_IQC_CONTROLRULE model)
        {
            long result = 0;

            try
            {
                QMS_MA_IQC_CONTROLRULE dt = new QMS_MA_IQC_CONTROLRULE();
                dt = QMS_MA_IQC_CONTROLRULE.GetById(db, model.ID);

                dt.HISTORY_ID = model.HISTORY_ID;
                dt.RULE_ID = model.RULE_ID;
                dt.POINT_START = model.POINT_START;
                dt.POINT_END = model.POINT_END;
                dt.POSSICAUSE_ID = model.POSSICAUSE_ID;
                dt.DELETE_FLAG = model.DELETE_FLAG;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_MA_IQC_CONTROLRULE(QMSDBEntities _db, QMS_MA_IQC_CONTROLRULE model)
        {
            long result = 0;
            try
            {
                QMS_MA_IQC_CONTROLRULE dt = new QMS_MA_IQC_CONTROLRULE();
                dt = QMS_MA_IQC_CONTROLRULE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_MA_IQC_CONTROLRULE.AddObject(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }


        public List<ViewQMS_MA_IQC_CONTROLRULE> getQMS_MA_IQC_CONTROLRULEActiveList(QMSDBEntities db)
        {
            List<ViewQMS_MA_IQC_CONTROLRULE> listResult = new List<ViewQMS_MA_IQC_CONTROLRULE>();

            try
            {
                List<QMS_MA_IQC_CONTROLRULE> list = QMS_MA_IQC_CONTROLRULE.GetActiveAll(db);
                List<QMS_MA_IQC_RULE> listRule = QMS_MA_IQC_RULE.GetAll(db);
                List<QMS_MA_IQC_POSSICAUSE> listPossibleCause = QMS_MA_IQC_POSSICAUSE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_MA_IQC_CONTROLRULE
                                  {
                                      ID = dt.ID,
                                      HISTORY_ID = dt.HISTORY_ID,
                                      RULE_ID = dt.RULE_ID,
                                      RULE_NAME = getIQCRuleName(listRule, dt.RULE_ID),
                                      POINT_START = dt.POINT_START,
                                      POINT_END = dt.POINT_END,
                                      POSSICAUSE_ID = dt.POSSICAUSE_ID,
                                      POSSICAUSE_NAME = getPossibleName(listPossibleCause, dt.POSSICAUSE_ID),
                                      DELETE_FLAG = dt.DELETE_FLAG,

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        #endregion


        public int Excelcify(string sxCol)
        {
            int result = -1;
            sxCol = sxCol.ToUpper();

            if (string.IsNullOrEmpty(sxCol))
                return result;


            for (int i = sxCol.Length; i > 0; i--)
            {
                char _c = sxCol[i - 1];
                //
                // Function =>  (26 ^ reversed_char_index) * char_value
                //          A = 1 ------ Z = 26 ------ AA = 27 ------ AZ = 54
                // 64 because there 'A' starts at index 65 and we want to give 'A' the value 1.
                result += Convert.ToInt32(Math.Pow(26, sxCol.Length - i)) * (Convert.ToInt32(_c) - 64);
            }

            return result;
        }


        public bool executeFileTemplateToDB(QMSDBEntities db, FolderDataDetail model)
        {
            bool bResult = true;


            //ExcelPackage excel = null;
            try
            {
                MasterDataService masterService = new MasterDataService(this._currentUserName);
                TransactionService transService = new TransactionService(this._currentUserName);
                CreateQMS_MA_TEMPLATE templateData = masterService.getQMS_MA_TEMPLATEById(db, model.TEMPLATE_ID);
                List<ViewQMS_MA_TEMPLATE_ROW> listRows = masterService.getQMS_MA_TEMPLATE_ROWListById(db, model.TEMPLATE_ID);
                List<ViewQMS_MA_TEMPLATE_COLUMN> listColumns = masterService.getQMS_MA_TEMPLATE_COLUMNListById(db, model.TEMPLATE_ID);

                List<CreateQMS_TR_TREND_DATA> listTrendData = new List<CreateQMS_TR_TREND_DATA>();

                decimal? tempVal1 = null;
                decimal? tempVal2 = null;

                if (listRows.Count() > 0 && listColumns.Count() > 0)
                {
                    string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                    string workSheet = templateData.SHEET_NAME;
                    string filename = Path.GetFileName(excelFile);
                    //var m_FileModify = File.GetLastWriteTime(excelFile);
                    //Step 1. เปิดไฟล์ excel... string ext = Path.GetExtension(myFilePath);

                    //byte[] file = File.ReadAllBytes(excelFile);
                    //MemoryStream ms = new MemoryStream(file);

                    FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                    string ext = Path.GetExtension(fileStream.Name);

                    //ExcelDataReader library .................
                    //Choose one of either 1 or 2
                    IExcelDataReader excelReader;

                    if (ext == ".xls")
                    {
                        //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                        excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                    }
                    else // if( ext == ".xlsx")
                    {
                        //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                    }

                    ////Choose one of either 3, 4, or 5
                    ////3. DataSet - The result of each spreadsheet will be created in the result.Tables
                    DataSet result = excelReader.AsDataSet();

                    ////4. DataSet - Create column names from first row
                    //excelReader.IsFirstRowAsColumnNames = true;
                    //DataSet result = excelReader.AsDataSet();

                    //5. Data Reader methods
                    //while (excelReader.Read())
                    //{
                    //    //excelReader.GetInt32(0);
                    //}


                    //for (int i = 1; i < result.Tables[0].Rows.Count; i++)
                    //{
                    //    DataRow data = result.Tables[0].Rows[i];

                    //    int x = data.Table.Columns.Count;
                    //    System.Diagnostics.Debug.Write(data.Table.Rows[i]["Amortization"]);
                    //}


                    //6. Free resources (IExcelDataReader is IDisposable)



                    //var file = new FileInfo(excelFile);
                    //excel = new ExcelPackage(fileStream); 

                    //var worksheet = excel.Workbook.Worksheets[workSheet];

                    int RowsNumber = 0;
                    string ColName = "";
                    object rawData;

                    //Step 2. วนลูป Row, Column ของ template เผื่อ อ่าน ค่า ดาต้า

                    //2.1 get Datetime 
                    int rowNo = getRowsNumberFromFieldName(templateData.EXCEL_DATE) - 1;
                    ColName = getColumnNameFromFieldName(templateData.EXCEL_DATE);
                    int colNo = Excelcify(ColName);
                    DateTime? recordDate = getDateTimeFromFileName(templateData.FILE_NAME, model.m_dtFile);


                    var dataCollect = result.Tables[templateData.SHEET_NAME];

                    if (null == dataCollect && null != result.Tables[0]) //ให้ โอกาส อีกครั้ง
                    {
                        for (int i = 0; i < result.Tables.Count; i++)
                        {
                            if (templateData.SHEET_NAME.ToLower().Trim() == result.Tables[i].ToString().ToLower().Trim())
                            {
                                dataCollect = result.Tables[i];
                            }
                        }
                    }

                    if (null != dataCollect)
                    {
                        var dateRecord = dataCollect.Rows[rowNo][colNo];// Convert.ToInt64(result.Tables[0].Rows[rowNo][colNo]);

                        if (typeof(DBNull) != dateRecord.GetType())
                        {
                            if (typeof(DateTime) == dateRecord.GetType())
                            {
                                recordDate = (DateTime)dateRecord;
                            }
                            else
                            {
                                long oaDate = Convert.ToInt64(dataCollect.Rows[rowNo][colNo]);
                                recordDate = DateTime.FromOADate(oaDate);
                            }
                        }

                        if (null == recordDate)
                        {
                            bResult = false;
                            _errMsg = "This data is not date formating.";
                        }
                        else
                        {
                            foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                            {
                                RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD);

                                foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColumns)
                                {
                                    ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                    CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                    //2.2 ค่า id ต่างๆ 
                                    tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                    tempData.SAMPLE_ID = rowsData.ID;
                                    tempData.ITEM_ID = colData.ID;


                                    //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                    //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 

                                    rowNo = getRowsNumberFromFieldName(ColName) - 1;
                                    ColName = getColumnNameFromFieldName(ColName);
                                    colNo = Excelcify(ColName);

                                    rawData = dataCollect.Rows[rowNo][colNo];


                                    try
                                    {
                                        recordDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 9, 0, 0); //default 9 โมง
                                        //getTime
                                        try
                                        {
                                            int TimeRowNo = rowNo;
                                            int TimeColNo = Excelcify(templateData.EXCEL_TIME);

                                            DateTime tempRecordTime = DateTime.FromOADate(Double.Parse(dataCollect.Rows[TimeRowNo][TimeColNo].ToString()));
                                            recordDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, tempRecordTime.Hour, tempRecordTime.Minute, tempRecordTime.Second);
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                    tempData.RECORD_DATE = recordDate.Value;
                                    if (null != rawData && typeof(DBNull) != rawData.GetType())
                                    {
                                        tempVal1 = null;
                                        tempVal2 = null;

                                        try
                                        {
                                            tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                            if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                            {
                                                tempData.CONC_LOADING_FLAG = 1;
                                                string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                            }
                                            else
                                            {
                                                int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                tempData.CONC_LOADING_FLAG = 0;

                                                if (startIndex >= 0)
                                                {
                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                    tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                }
                                                else
                                                {
                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            this.writeErrorLog(ex.Message);
                                        }

                                        if (null != tempVal1)
                                        {
                                            tempData.DATA_VALUE_1 = tempVal1.Value;

                                            if (null != tempVal2)
                                                tempData.DATA_VALUE_2 = tempVal2.Value;

                                            listTrendData.Add(tempData);
                                        }
                                        //else
                                        //{
                                        //    DateTime startDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 0, 0, 0);
                                        //    DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);
                                        //    bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                        //}
                                    }
                                }
                            }

                            //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                            DateTime startDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 0, 0, 0);
                            DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                            bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);


                            if (listTrendData.Count() > 0)
                            {
                                //Step 2 Save value data to database .......
                                foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                {
                                    //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                    QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                    if (null != temp)
                                    {
                                        dtData.ID = temp.ID;
                                    }

                                    long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                }
                            }
                        }
                        //System.Threading.Thread.Sleep(500);
                    }
                    else
                    {
                        _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                        bResult = false;
                    }


                    excelReader.Close();
                }
            }
            catch (Exception ex)
            {
                _errMsg = ex.Message;
                bResult = false;
                //this.writeErrorLog(ex.Message);
            }



            return bResult;
        }
        public bool executeFileTemplateIQCToDB(QMSDBEntities db, FolderDataDetail model)
        {
            bool bResult = true;


            //ExcelPackage excel = null;
            try
            {
                MasterDataService masterService = new MasterDataService(this._currentUserName);

                CreateQMS_MA_IQC_TEMPLATE templateData = masterService.getQMS_MA_IQC_TEMPLATEById(db, model.TEMPLATE_ID);

                string excelFilePath = templateData.PATH_NAME + templateData.FILE_NAME;

                if (templateData.PATH_NAME != "" && templateData.FILE_NAME != "")
                {
                    string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                    string workSheet = templateData.SHEET_NAME;
                    string filename = Path.GetFileName(excelFile);
                    //var m_FileModify = File.GetLastWriteTime(excelFile);
                    //Step 1. เปิดไฟล์ excel... string ext = Path.GetExtension(myFilePath);

                    //byte[] file = File.ReadAllBytes(excelFile);
                    //MemoryStream ms = new MemoryStream(file);

                    FileStream fileStream = new FileStream(excelFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                    string ext = Path.GetExtension(fileStream.Name);

                    //ExcelDataReader library .................
                    //Choose one of either 1 or 2
                    IExcelDataReader excelReader;

                    if (ext == ".xls")
                    {
                        //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                        excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                    }
                    else // if( ext == ".xlsx")
                    {
                        //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                    }

                    FileInfo fileInfo = new FileInfo(excelFilePath);
                    ExcelPackage package = new ExcelPackage(fileInfo);
                    ExcelWorksheet sheet = package.Workbook.Worksheets[workSheet];
                    int rows = sheet.Dimension.Rows; // 20
                 

                    //DataSet result = excelReader.AsDataSet();

                    DateTime? recordDate = getDateTimeFromFileName(templateData.FILE_NAME, model.m_dtFile);
                    int ColDateIndex = ColumnNameToIndex(templateData.COLUMN_DATE.ToString());
                    int ColValueIndex = ColumnNameToIndex(templateData.COLUMN_DATA.ToString());
                    int rowNo = templateData.ROW_START;
                    if (sheet != null) {
                      
                            //var dateRecord = sheet.Cells[rowNo,ColDateIndex].Value.ToString();
                            double d = double.Parse(sheet.Cells[rowNo, ColDateIndex].Value.ToString());
                            DateTime dateRecord = DateTime.FromOADate(d);
                            List<QMS_MA_IQC_CONTROLDATA> dt = new List<QMS_MA_IQC_CONTROLDATA>();
                            if (dateRecord.Year<2000)
                            {

                                bResult = false;
                                _errMsg = "This data is not date formating.";

                            }
                            else
                            {
                                for (int i = rowNo; i <= rows; i++)
                                {
                                IQCControlDataSearchModel search = new IQCControlDataSearchModel();
                                search.TEMPLATE_ID = (byte)model.TEMPLATE_ID;
                                dt = QMS_MA_IQC_CONTROLDATA.GetAllBySearch(db, search);

                                CreateQMS_MA_IQC_CONTROLDATA tempData = new CreateQMS_MA_IQC_CONTROLDATA();
                                try
                                {
                                    if (sheet.Cells[i, ColValueIndex].Value != null && sheet.Cells[i, ColDateIndex].Value!= null)
                                    {
                                        double dKEEPTIME = double.Parse(sheet.Cells[i, ColDateIndex].Value.ToString());
                                        DateTime KEEPTIME = DateTime.FromOADate(dKEEPTIME);
                                        tempData.KEEPTIME = KEEPTIME;

                                        tempData.KEEPVALUE = Math.Round(Decimal.Parse(sheet.Cells[i, ColValueIndex].Value.ToString()),4);
                                        tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                        tempData.ID = 0;
                                        if (tempData.KEEPTIME != null && tempData.KEEPVALUE != null)
                                        {

                                            List<QMS_MA_IQC_CONTROLDATA> checkDuplicate = dt.FindAll(x => x.KEEPTIME == tempData.KEEPTIME && x.KEEPVALUE == tempData.KEEPVALUE).ToList();

                                            if (checkDuplicate.Count() == 0)
                                            {
                                                long nResult = masterService.SaveQMS_MA_IQC_CONTROLDATA(db, tempData);
                                            }

                                        }
                                    }

                                   

                                }
                                catch (Exception ex)
                                {


                                }





                            }

                        }
                    }
                    else
                    {
                        _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                        bResult = false;
                    }
                  
                    //var dataCollect = result.Tables[templateData.SHEET_NAME];


                    //if (null != dataCollect)
                    //{
                    //    var dateRecord = dataCollect.Rows[rowNo][ColDateIndex];// Convert.ToInt64(result.Tables[0].Rows[rowNo][colNo]);
                    //    List<QMS_MA_IQC_CONTROLDATA> dt = new List<QMS_MA_IQC_CONTROLDATA>();
                    //    if (typeof(DateTime) != dateRecord.GetType())
                    //    {

                    //        bResult = false;
                    //        _errMsg = "This data is not date formating.";

                    //    }
                    //    else
                    //    {
                    //        IQCControlDataSearchModel search = new IQCControlDataSearchModel();
                    //        dt = QMS_MA_IQC_CONTROLDATA.GetAllBySearch(db, search);
                    //        for (int i = rowNo; i < dataCollect.Rows.Count; i++)
                    //        {
                    //            CreateQMS_MA_IQC_CONTROLDATA tempData = new CreateQMS_MA_IQC_CONTROLDATA();
                    //            try {
                    //                tempData.KEEPTIME = DateTime.Parse(dataCollect.Rows[i][ColDateIndex].ToString());

                    //                tempData.KEEPVALUE = Decimal.Parse(dataCollect.Rows[i][ColValueIndex].ToString());
                    //                tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                    //                tempData.ID = 0;
                    //                if (tempData.KEEPTIME != null && tempData.KEEPVALUE != null)
                    //                {

                    //                    List<QMS_MA_IQC_CONTROLDATA> checkDuplicate = dt.FindAll(x => x.KEEPTIME == tempData.KEEPTIME && x.KEEPVALUE == tempData.KEEPVALUE).ToList();

                    //                    if (checkDuplicate.Count() == 0) {
                    //                        long nResult = masterService.SaveQMS_MA_IQC_CONTROLDATA(db, tempData);
                    //                    }
                                    
                    //                }

                    //            }
                    //            catch (Exception ex)
                    //            {
                                 
                                  
                    //            }

                               

                                
                    //        }



                    //    }
                    //}
                    //else
                    //{
                    //    _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                    //    bResult = false;
                    //}


                    excelReader.Close();
                }
            }
            catch (Exception ex)
            {
                _errMsg = ex.Message;
                bResult = false;
                //this.writeErrorLog(ex.Message);
            }



            return bResult;
        }
        public static int ColumnNameToIndex(string name)
        {

            var upperCaseName =  name.ToUpper();
            var number = 0;
            var pow = 1;
            for (var i = upperCaseName.Length - 1; i >= 0; i--)
            {
                number += (upperCaseName[i] - 'A' + 1) * pow;
                pow *= 26;
            }

            return number;
        }
      
    }
}
