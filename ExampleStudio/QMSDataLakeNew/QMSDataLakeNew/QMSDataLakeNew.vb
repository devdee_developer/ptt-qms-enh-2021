﻿Imports System.Configuration
Imports System.Data.EntityClient
Imports System.Data.SqlClient
Imports System.IO
Imports System.Threading
Imports QMSSystem.CoreDB.Services
Imports System.ServiceProcess
Imports QMSSystem.Model

Public Class QMSDataLakeNew

    Private Schedular As Timer
    Private m_nCount As Integer = 3
    Private m_bError As Boolean = False

    Private schedulerTimer As Timer
    Private scheduledTime As DateTime = DateTime.Today.AddHours(8) ' Example: Set the scheduled time to 8:00 AM
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Me.WriteToFile("Service started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        'System.Diagnostics.Debugger.Launch()
        'Me.ScheduleService()

        'System.Diagnostics.Debugger.Launch()
        scheduledTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings("ScheduledTime"))
        scheduledTime = scheduledTime.AddDays(1)

        'Get the Interval in Minutes from AppSettings.
        Dim intervalMinutes As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))

        Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
        Dim schedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds)
        Me.WriteToFile((Convert.ToString("Service scheduled to run after: ") & schedule) + " {0}")

        ' Calculate the initial delay until the scheduled time
        Dim delay As Integer = CInt((scheduledTime - DateTime.Now).TotalMilliseconds)

        ' Ensure the delay is positive (i.e., the scheduled time is in the future)
        If delay < 0 Then
            scheduledTime = scheduledTime.AddMinutes(intervalMinutes) ' Schedule for the next day if the time has already passed today
            delay = CInt((scheduledTime - DateTime.Now).TotalMilliseconds)
        End If
        ' Create a timer with the initial delay and no periodic interval
        schedulerTimer = New Timer(AddressOf ScheduleTask, Nothing, delay, Timeout.Infinite)
    End Sub
    Private Sub ScheduleTask(state As Object)
        ' This method is called by the timer at each interval
        ' Perform your scheduled task here
        ' For example, write to a log file or perform some processing

        'Get the Interval in Minutes from AppSettings.
        Dim intervalMinutes As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))

        Me.WriteToFile("Start : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))

        ' Schedule the next task for the next day at the same time
        scheduledTime = scheduledTime.AddMinutes(intervalMinutes)
        Dim delay As Integer = CInt((scheduledTime - DateTime.Now).TotalMilliseconds)
        schedulerTimer.Change(delay, Timeout.Infinite)

        Try
            Dim activeDate As DateTime = Date.Now
            Dim startDate As DateTime
            Dim endDate As DateTime 'start date

            startDate = activeDate
            endDate = startDate.AddDays(1)

            Dim connectionString As String
            Me.WriteToFile("Before get conn SchedularCallback")
            connectionString = GetConnectionString()
            Dim db As New QMSSystem.CoreDB.QMSDBEntities()
            Dim MasterDataService As New MasterDataService("Service")
            Dim TransactionService As New TransactionService("Service")
            Me.WriteToFile("After get conn SchedularCallback")
            Try
                Dim Totaltag As String
                Totaltag = TransactionService.LoadEventRawDataNewRealTimeFromDataLakeTODB(db, startDate, endDate)
                Me.WriteToFile("Total tag from EVENT_RAW_DATA : " + Totaltag)
            Catch ex As Exception
                Dim err As String = ""
                err = err + "error : " + ex.Message
                If ex.InnerException IsNot Nothing Then
                    err = err + ", InnerException : " + ex.InnerException.Message
                End If
                err = err + ", InnerException : " + ex.StackTrace

                Me.WriteToFile(err)
            End Try
            Thread.Sleep(5000)
        Catch ex As Exception
            Dim err As String = ""
            err = err + "error : " + ex.Message
            If ex.InnerException IsNot Nothing Then
                err = err + ", InnerException : " + ex.InnerException.Message
            End If
            err = err + ", InnerException : " + ex.StackTrace

            Me.WriteToFile(err)
        End Try

        Me.WriteToFile("End : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
        Dim schedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds)
        Me.WriteToFile((Convert.ToString("Service scheduled to run after: ") & schedule) + " {0}")
    End Sub
    Protected Overrides Sub OnStop()
        Try
            ' Add code here to perform any tear-down necessary to stop your service.
            Me.WriteToFile("Service stopped at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))

            ' Clean up resources
            schedulerTimer.Dispose()
        Catch ex As Exception
            Dim err As String = ""
            err = err + "error : " + ex.Message
            If ex.InnerException IsNot Nothing Then
                err = err + ", InnerException : " + ex.InnerException.Message
            End If
            err = err + ", InnerException : " + ex.StackTrace

            Me.WriteToFile(err)
        End Try
    End Sub
    Private Sub WriteToFile(text As String)
        Dim dirName As String
        Dim path As String
        Try
            dirName = AppDomain.CurrentDomain.BaseDirectory & "\\servicelogs" ' [Assembly].GetExecutingAssembly.Location 'Directory.GetCurrentDirectory()

            If (Not System.IO.Directory.Exists(dirName)) Then
                System.IO.Directory.CreateDirectory(dirName)
            End If

            path = dirName & "\\" & DateTime.Now.ToString("yyyyMMdd") & ".log"

            Using writer As New StreamWriter(path, True)
                writer.WriteLine(text)
                writer.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Public Function GetConnectionString() As String

        Dim DBConnection As String
        DBConnection = System.Configuration.ConfigurationManager.ConnectionStrings("QMSDBEntities").ToString()

        Return DBConnection

        Dim providerName As String = "System.Data.SqlClient"
        Dim serverName As String = "ptt-db-t03.ptt.corp"
        Dim databaseName As String = "PTT-GSPQualityManagementSystem_test"
        Dim UserID As String = "pttgspqprdusr"
        Dim Password As String = "cpttgspqprdusr"

        Dim sqlBuilder As New SqlConnectionStringBuilder

        sqlBuilder.DataSource = serverName
        sqlBuilder.InitialCatalog = databaseName
        sqlBuilder.PersistSecurityInfo = True
        sqlBuilder.UserID = UserID
        sqlBuilder.Password = Password

        Dim providerString As String = sqlBuilder.ToString

        Dim entityBuilder As New EntityConnectionStringBuilder

        'Set the provider name.
        entityBuilder.Provider = providerName
        ' Set the provider-specific connection string.
        entityBuilder.ProviderConnectionString = providerString
        ' Set the Metadata location to the current directory.
        entityBuilder.Metadata = "res://*//QMSSystem.csdl|" &
                                    "res://*//QMSSystem.ssdl|" &
                                    "res://*//QMSSystem.msl"

        Return entityBuilder.ToString()
    End Function
End Class
