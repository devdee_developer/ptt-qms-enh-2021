﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("QMSDataLake")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Devdee")> 
<Assembly: AssemblyProduct("QMSDataLake")> 
<Assembly: AssemblyCopyright("Copyright ©  2020")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("2a7cc9fe-3023-42e7-913b-1c81e8aab867")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2022.12.13.1")>
<Assembly: AssemblyFileVersion("2022.12.13.1")>
