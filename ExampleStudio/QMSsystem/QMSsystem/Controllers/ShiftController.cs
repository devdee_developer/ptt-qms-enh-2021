﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_Shift }, DataAccess = DataAccessEnum.Authen)]
    public class ShiftController : BaseController
    {
        //
        // GET: /OperationShift/

        public ActionResult ListMasterShift()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ControlValueSearchModel searchCorrectValue = new ControlValueSearchModel();
            searchCorrectValue.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.PRODUCT;
              
            ViewBag.searchCorrectValue = searchCorrectValue;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.listYear = masterDataServices.getShiftYear(_db);

            return View();
        }

        public JsonResult GetListMasterShift(int year)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    ShiftTableData shiftTable = new ShiftTableData(); 
                    shiftTable.tableData = new List<List<ColumnShiftData>>();


                    shiftTable = masterDataServices.getOperationShiftByYearId(_db, year);

                    result = new { result = ReturnStatus.SUCCESS, message = shiftTable.tableData, CREATE_USER = shiftTable.CREATE_USER, UPDATE_USER = shiftTable.UPDATE_USER, SHOW_CREATE_DATE = shiftTable.SHOW_CREATE_DATE, SHOW_UPDATE_DATE = shiftTable.SHOW_UPDATE_DATE };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteListMasterShift(int year)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<List<ColumnShiftData>> listData = new List<List<ColumnShiftData>>();
                    bool bResult = masterDataServices.DeleteQMS_MA_OPERATION_SHIFTByYear(_db, year);

                    if (true == bResult)
                    {
                        List<ShiftYear> listResult = masterDataServices.getShiftYear(_db);
                        result = new { result = ReturnStatus.SUCCESS, message = listResult };
                    }
                    else {
                        result = new { result = ReturnStatus.ERROR, message = ReturnStatus.ERROR };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateMasterShift(long id)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_CONTROL createQMS_MA_CONTROL = new CreateQMS_MA_CONTROL();
            string actionName = Resources.Strings.AddNew;
            
            createQMS_MA_CONTROL.GROUP_TYPE = (byte)CONTROL_GROUP_TYPE.PRODUCT;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_CONTROL = masterDataServices.getQMS_MA_CONTROLById(_db, id);
            }

            ViewBag.createQMS_MA_CONTROL = createQMS_MA_CONTROL;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult SaveEditShiftData(ColumnShiftData model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveOperationShiftDetail(_db, model);

                    if (nResult > 0)
                    {
                        ShiftTableData shiftTable = new ShiftTableData();
                        shiftTable.tableData = new List<List<ColumnShiftData>>();


                        shiftTable = masterDataServices.getOperationShiftByYearId(_db, model.YEAR);

                        result = new { result = ReturnStatus.SUCCESS, message = shiftTable.tableData, CREATE_USER = shiftTable.CREATE_USER, UPDATE_USER = shiftTable.UPDATE_USER, SHOW_CREATE_DATE = shiftTable.SHOW_CREATE_DATE, SHOW_UPDATE_DATE = shiftTable.SHOW_UPDATE_DATE };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.SUCCESS, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
