﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMVC4.CoreDB.services
{
    public class BaseService
    {
        public string _errMsg;
        public long _result;

        public BaseService()
        {
            _errMsg = "";
            _result = -1;
        }

        public string getError()
        {
            return _errMsg;
        }

        public string getEexceptionError(Exception ex)
        {
            string result = "";

            if (ex.InnerException != null)
            {
                result = ex.Message + " : " + ex.InnerException.Message;
            }
            else
            {
                result = ex.Message;
            }

            return result;
        }

        protected string ShowYearMonthDay(DateTime? dt)
        {
            string result = "";

            if (null != dt)
            {

                if (dt.Value.Day < 10)
                {

                    result += "0" + dt.Value.Day.ToString() + "/";
                }
                else
                {
                    result += dt.Value.Day.ToString() + "/";
                }

                if (dt.Value.Month < 10)
                {

                    result += "0" + dt.Value.Month.ToString() + "/";
                }
                else
                {
                    result += dt.Value.Month.ToString() + "/";
                }

                if (dt.Value.Year < 2500)
                {

                    result += (dt.Value.Year + 543).ToString();
                }
                else
                {
                    result += dt.Value.Year.ToString();
                }
            }


            return result;
        }
    }
}
