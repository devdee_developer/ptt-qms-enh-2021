﻿using System;
using System.Collections.Generic; 
using System.Text;

namespace QMSSystem.Model
{
    class ConfigModel
    {
    }

    public class LIST_AUTH_DATA
    {
        public string E_NAME { get; set; }
        public bool access { get; set; }
    }

    public class CreateQMS_ST_EXA_CONNECT
    {
        public string LAKE_SERVER { get; set; }
        public string LAKE_DATABASE { get; set; }
        public DateTime? EXA_SCHEDULE { get; set; }
        public string LAKE_USER { get; set; }
        public string LAKE_PASSWORD { get; set; }
    }

    public class CreateQMS_ST_TEMPLATE_CONNECT
    {
        public DateTime? TEMPLATE_SCHEDULE { get; set; }
    }

    public class CreateQMS_ST_LIMS_CONNECT
    {
        public string LIMS_SERVER { get; set; }
        public DateTime? LIMS_SCHEDULE { get; set; }
        public string LIMS_USER { get; set; }
        public string LIMS_PASSWORD { get; set; }
        public int LIMS_PORT { get; set; }
        public string LIMS_SID { get; set; }
    }

    public class CreateQMS_ST_AUTHORIZATION
    {
        public long ID { get; set; }
        public byte GROUP_AUTH { get; set; }
        public string LIST_AUTH { get; set; }

        public PrivilegeMode ATUH_DATA { get; set; }
        public List<AuthenAccess> listAuth { get; set; } = new List<AuthenAccess>();

        //public List<LIST_AUTH_DATA> listAuth { get; set; }
    }

    public class CreateQMS_ST_EXAQUATUM_SCHEDULE
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public byte DOC_STATUS { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public string CREATE_USER { get; set; }
        //public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        //public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_ST_EXAQUATUM_SCHEDULE
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }

        public byte DOC_STATUS { get; set; }
        public string DOC_STATUS_NAME { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }

        public bool ITEM_SELECTED { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class CreateQMS_ST_EXPORT_PREFIX
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string DOWNTIME { get; set; }
        public string DOWNTIME_REMARK { get; set; }
        public string REDUCE_FEED { get; set; }
        public string REDUCE_FEED_REMARK { get; set; }
        public string LPG_GRADE { get; set; }
          
        public string GC_ERROR_1 { get; set; }
        public string GC_ERROR_2 { get; set; }
        public string GC_ERROR_3 { get; set; }
        public string GC_ERROR_4 { get; set; }
        public string GC_ERROR_5 { get; set; }
        public string GC_ERROR_REMARK { get; set; }

        public string GC_CYCCOUNT_1 { get; set; }
        public string GC_CYCCOUNT_2 { get; set; }
        public string GC_CYCCOUNT_3 { get; set; }
        public string GC_CYCCOUNT_4 { get; set; }
        public string GC_CYCCOUNT_5 { get; set; }
        public string GC_CYCCOUNT_6 { get; set; }
        public string GC_CYCCOUNT_7 { get; set; }
        public string GC_CYCCOUNT_8 { get; set; }
        public string GC_CYCCOUNT_9 { get; set; }
        public string GC_CYCCOUNT_10 { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class ViewQMS_ST_EXPORT_PREFIX
    {
        public long ID { get; set; }
        public long PLANT_ID { get; set; }
        public string PLANT_NAME { get; set; }
        public bool ITEM_SELECTED { get; set; }

        public string DOWNTIME { get; set; }
        public string DOWNTIME_REMARK { get; set; }
        public string REDUCE_FEED { get; set; }
        public string REDUCE_FEED_REMARK { get; set; }
        public string LPG_GRADE { get; set; }

        public string GC_ERROR_1 { get; set; }
        public string GC_ERROR_2 { get; set; }
        public string GC_ERROR_3 { get; set; }
        public string GC_ERROR_4 { get; set; }
        public string GC_ERROR_5 { get; set; }
        public string GC_ERROR_REMARK { get; set; }

        public string GC_CYCCOUNT_1 { get; set; }
        public string GC_CYCCOUNT_2 { get; set; }
        public string GC_CYCCOUNT_3 { get; set; }
        public string GC_CYCCOUNT_4 { get; set; }
        public string GC_CYCCOUNT_5 { get; set; }
        public string GC_CYCCOUNT_6 { get; set; }
        public string GC_CYCCOUNT_7 { get; set; }
        public string GC_CYCCOUNT_8 { get; set; }
        public string GC_CYCCOUNT_9 { get; set; }
        public string GC_CYCCOUNT_10 { get; set; }

        public string CREATE_USER { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; } 
    }

    public class CreateQMS_ST_FILE_ATTACH
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public string PATH { get; set; }
        public byte BOARD_TYPE { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class ViewQMS_ST_FILE_ATTACH
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public string PATH { get; set; }
        public byte BOARD_TYPE { get; set; }
        public string BOARD_NAME { get; set; }

        public string CREATE_USER { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string SHOW_CREATE_DATE { get; set; }

        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
    }

    public class CreateQMS_ST_SYSTEM_CONFIG
    {
        public long ID { get; set; }
        public string HOME_BACKGROUND { get; set; }
        public string PRODUCT_CSC_PATH { get; set; }
        public string PRODUCT_CSC_NAME { get; set; }

        public short ABNORMAL_MAX_REPEAT { get; set; }
        public short ABNORMAL_OVERSHOOT { get; set; }
        public string ABNORMAL_ALPHABET { get; set; }
        public Int16 UPPER_BOUNDARY { get; set; }
        public Int16 LOWER_BOUNDARY { get; set; }

        public long REPORT_OFF_CONTROL { get; set; }
        public long REPORT_OFF_SPEC { get; set; }
        public long HIGHLIGHT_1 { get; set; }
        public long HIGHLIGHT_2 { get; set; }
        public long HIGHLIGHT_3 { get; set; }
        public long HIGHLIGHT_4 { get; set; }

        public string LAKE_SERVER { get; set; }
        public string LAKE_DATABASE { get; set; }
        public DateTime EXA_SCHEDULE { get; set; }
        public string LAKE_USER { get; set; }
        public string LAKE_PASSWORD { get; set; }

        public string LIMS_SERVER { get; set; }
        public DateTime LIMS_SCHEDULE { get; set; }
        public string LIMS_USER { get; set; }
        public string LIMS_PASSWORD { get; set; }
        public int LIMS_PORT { get; set; }
        public string LIMS_SID { get; set; }
        public DateTime TEMPLATE_SCHEDULE { get; set; }
    }

    public class UpdateOFFReportQMS_ST_SYSTEM_CONFIG
    {
        public long ID { get; set; }
        public long REPORT_OFF_CONTROL { get; set; }
        public long REPORT_OFF_SPEC { get; set; }
    }

    public class UpdateTrendQMS_ST_SYSTEM_CONFIG
    {
        public long ID { get; set; }
        public long HIGHLIGHT_1 { get; set; }
        public long HIGHLIGHT_2 { get; set; }
        public long HIGHLIGHT_3 { get; set; }
        public long HIGHLIGHT_4 { get; set; }
    }

    public class ViewQMS_ST_SYSTEM_CONFIG
    {
        public long ID { get; set; }
        public string HOME_BACKGROUND { get; set; }
        public byte ABNORMAL_MAX_REPEAT { get; set; }
        public byte ABNORMAL_OVERSHOOT { get; set; }
        public string ABNORMAL_ALPHABET { get; set; }
        public Int16 UPPER_BOUNDARY { get; set; }
        public Int16 LOWER_BOUNDARY { get; set; }

        public long REPORT_OFF_CONTROL { get; set; }
        public long REPORT_OFF_SPEC { get; set; }
        public long HIGHLIGHT_1 { get; set; }
        public long HIGHLIGHT_2 { get; set; }
        public long HIGHLIGHT_3 { get; set; }
        public long HIGHLIGHT_4 { get; set; }

        public DateTime EXA_SCHEDULE { get; set; }
        public string LIMS_SERVER { get; set; }
        public DateTime LIMS_SCHEDULE { get; set; }
        public string LIMS_USER { get; set; }
        public string LIMS_PASSWORD { get; set; }
        public Int16 LIMS_PORT { get; set; }
        public string LIMS_SID { get; set; }
        public DateTime TEMPLATE_SCHEDULE { get; set; }
    }
    public class CreateQMS_ST_SYSTEM_LOG
    {
        public long ID { get; set; }
        public DateTime LOG_DATE { get; set; }
        public byte ERROR_LEVEL { get; set; }
        public string PROCESS_NAME { get; set; }
        public string PROCESS_DESC { get; set; }
    }

    public class ViewQMS_ST_SYSTEM_LOG
    {
        public long ID { get; set; }
        public DateTime LOG_DATE { get; set; }
        public byte ERROR_LEVEL { get; set; }
        public string PROCESS_NAME { get; set; }
        public string PROCESS_DESC { get; set; }
        public bool ITEM_SELECTED { get; set; }
    }

    public class CreateQMS_ST_SYSTEM_USER_STAMP
    {
        public long ID { get; set; }
        public int SYSTEM_TYPE { get; set; }

        public string CREATE_USER { get; set; } 
        public string SHOW_CREATE_DATE { get; set; } 
        public string UPDATE_USER { get; set; }
        public string SHOW_UPDATE_DATE { get; set; }
    }

    public class CreateQMS_ST_OFF_CONTROL
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public string OFF_CONTROL_DESC { get; set; }
    }

    public class ViewQMS_ST_OFF_CONTROL
    {
        public long ID { get; set; }
        public long RP_OFF_CONTROL_ID { get; set; }
        public byte OFF_CONTROL_TYPE { get; set; }
        public string OFF_CONTROL_DESC { get; set; }    
    }

}
