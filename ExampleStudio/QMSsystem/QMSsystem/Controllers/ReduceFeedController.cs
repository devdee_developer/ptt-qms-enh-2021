﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_ReduceFeed }, DataAccess = DataAccessEnum.Authen)]
    public class ReduceFeedController : BaseController
    {
        //
        // GET: /ReduceFeed/


        public ActionResult ListMasterReduceFeed()//control ListMasterReduceFeed.cshtml
        {
            QMSSystem.CoreDB.Services.MasterDataService masterDataServices = new MasterDataService(_username);
            ReduceFeedSearchModel searchReduceFeed = new ReduceFeedSearchModel();
            CreateQMS_MA_REDUCE_FEED createQMS_MA_REDUCE_FEED = new CreateQMS_MA_REDUCE_FEED();
            CreateQMS_MA_REDUCE_FEED_DETAIL createQMS_MA_REDUCE_FEED_DETAIL = new CreateQMS_MA_REDUCE_FEED_DETAIL();
            ReduceFeedDetailSearchModel searchReduceFeedDetail = new ReduceFeedDetailSearchModel();

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            ViewBag.searchReduceFeed = searchReduceFeed;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            ViewBag.createQMS_MA_REDUCE_FEED = createQMS_MA_REDUCE_FEED;
            ViewBag.createQMS_MA_REDUCE_FEED_DETAIL = createQMS_MA_REDUCE_FEED_DETAIL;
            ViewBag.searchReduceFeedDetail = searchReduceFeedDetail;

            return View();
        }

        public ActionResult CreateMasterReduceFeed(long id = 0)//control CresteMasterReduceFeed.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_REDUCE_FEED createQMS_MA_REDUCE_FEED = new CreateQMS_MA_REDUCE_FEED();
            CreateQMS_MA_REDUCE_FEED_DETAIL createQMS_MA_REDUCE_FEED_DETAIL = new CreateQMS_MA_REDUCE_FEED_DETAIL();
            ReduceFeedDetailSearchModel searchReduceFeedDetail = new ReduceFeedDetailSearchModel();

            string actionName = Resources.Strings.AddNew;

            createQMS_MA_REDUCE_FEED.ACTIVE_DATE = getDummyDateTime();
            searchReduceFeedDetail.REDUCE_FEED_ID = id;
            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_REDUCE_FEED = masterDataServices.getQMS_MA_REDUCE_FEEDById(_db, id);
            }


            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listUnit = masterDataServices.getQMS_MA_UNITList(_db);//.getQMS_MA_UNITActiveList(_db);
            ViewBag.listReduceFeedDetail = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();// masterDataServices.searchQMS_MA_REDUCE_FEED_DETAIL(_db, id);

            ViewBag.createQMS_MA_REDUCE_FEED = createQMS_MA_REDUCE_FEED;
            ViewBag.createQMS_MA_REDUCE_FEED_DETAIL = createQMS_MA_REDUCE_FEED_DETAIL;

            ViewBag.searchReduceFeedDetail = searchReduceFeedDetail;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult GetCreateQMS_MA_REDUCE_FEEDById(long  id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_REDUCE_FEED createQMS_MA_REDUCE_FEED = masterDataServices.getQMS_MA_REDUCE_FEEDById(_db, id);
                    result = new { result = ReturnStatus.SUCCESS, message = createQMS_MA_REDUCE_FEED };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_REDUCE_FEED(CreateQMS_MA_REDUCE_FEED model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_REDUCE_FEED(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_MA_REDUCE_FEED_DETAIL modelDetail = new QMSSystem.Model.CreateQMS_MA_REDUCE_FEED_DETAIL();
                        modelDetail.ID = model.REDUCE_FEED_DETAIL_ID;
                        modelDetail.EXA_TAG_NAME = model.EXA_TAG_NAME;
                        modelDetail.CONVERT_VALUE = model.CONVERT_VALUE;
                        modelDetail.REDUCE_FEED_ID = nResult; 
                        long nResultDetail = masterDataServices.SaveQMS_MA_REDUCE_FEED_DETAIL(_db, modelDetail);

                        model = masterDataServices.getQMS_MA_REDUCE_FEEDById(_db, nResult);

                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, detailId = nResultDetail, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_REDUCE_FEEDBySearch(ReduceFeedSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_REDUCE_FEED> listData = new List<ViewQMS_MA_REDUCE_FEED>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_REDUCE_FEED(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_REDUCE_FEED(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_REDUCE_FEEDByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_REDUCE_FEED_DETAIL(CreateQMS_MA_REDUCE_FEED_DETAIL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_REDUCE_FEED_DETAIL(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_REDUCE_FEED_DETAILBySearch(ReduceFeedDetailSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_REDUCE_FEED_DETAIL> listData = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_REDUCE_FEED_DETAIL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_REDUCE_FEED_DETAIL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_REDUCE_FEED_DETAILByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        



        public ActionResult ListManageReduceFeed()//control ListReduceFeedExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TRReduceFeedSearchModel searchTRReduceFeed = new TRReduceFeedSearchModel();
            CreateQMS_TR_REDUCE_FEED createQMS_TR_REDUCE_FEED = new CreateQMS_TR_REDUCE_FEED(); 
            createQMS_TR_REDUCE_FEED.START_DATE = getDummyDateTime();
            createQMS_TR_REDUCE_FEED.END_DATE = getDummyDateTime();
            createQMS_TR_REDUCE_FEED.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.REDUCE_FEED;

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusListWithInitail();

            ViewBag.searchTRReduceFeed = searchTRReduceFeed;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.listDocStatusEx = this.getDocStatusList();

            ViewBag.createQMS_TR_REDUCE_FEED = createQMS_TR_REDUCE_FEED;
            return View();
        }

        public ActionResult CreateManageReduceFeed(long id = 0)//control CresteReduceFeedExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_REDUCE_FEED createQMS_TR_REDUCE_FEED = new CreateQMS_TR_REDUCE_FEED();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_REDUCE_FEED.START_DATE = getDummyDateTime();
            createQMS_TR_REDUCE_FEED.END_DATE = getDummyDateTime();
            createQMS_TR_REDUCE_FEED.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.REDUCE_FEED;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_REDUCE_FEED = transactionServices.getQMS_TR_REDUCE_FEEDById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();

            ViewBag.createQMS_TR_REDUCE_FEED = createQMS_TR_REDUCE_FEED;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_REDUCE_FEED(CreateQMS_TR_REDUCE_FEED model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);

                    VALIDATE_EXCEPTION_OVERLLAP checkOverlap = transactionServices.IsOverlapMasterCorrectDataEx(_db, model.ID, model.PLANT_ID, model.PRODUCT_ID, 0, model.START_DATE, model.END_DATE, (byte)CORRECT_DATA_TYPE.REDUCE_FEED);
                     if (checkOverlap.IS_OVERLAP)
                     {
                         result = new { result = ReturnStatus.ERROR, message = checkOverlap.MSG_ERROR };
                     }
                     else
                     {
                         long nResult = transactionServices.SaveQMS_TR_REDUCE_FEED(_db, model);

                         if (nResult > 0)
                         {
                             transactionServices.SaveQMS_TR_OFF_CONTROL_CAL_With_REDUCE_FEED(_db, model); //Adjust
                             model = transactionServices.getQMS_TR_REDUCE_FEEDById(_db, nResult);
                             result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                         }
                         else
                         {
                             result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                         }
                     }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_REDUCE_FEEDBySearch(TRReduceFeedSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_REDUCE_FEED> listData = new List<ViewQMS_TR_REDUCE_FEED>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_REDUCE_FEED(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_REDUCE_FEED(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_REDUCE_FEEDByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void ReduceFeedDataExcel(ProductExaDataSearchModel searchModel)
        {
            TransactionService transDataServices = new TransactionService(_username);
            //ProductExaDataSearchModel searchModel = new ProductExaDataSearchModel();

            //searchModel.PRODUCT_ID = PLANT_ID;
            //searchModel.PLANT_ID = PRODUCT_ID;
            //searchModel.START_DATE = START_DATE;
            //searchModel.END_DATE = END_DATE;

            ExcelPackage excel = transDataServices.ReduceFeedExaDataExcelByPlantId(_db, searchModel);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.ReduceFeed + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }

        public JsonResult UpdateDocStatusQMS_TR_REDUCE_FEED(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_REDUCE_FEEDByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
