﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using QMSSystem.Model;
using System.Globalization;

namespace QMSSystem.ExaDB
{
    public class ExaCommandSQL
    {
        public string msgError = "";
        public string _msgUser = "Exaquatum serivce";
        public string _msgGCERROR = "GC Error";
        public string _msgFLowRate = "Flow Rate Error";
        public string _msgUnderFlow = "Under Flow";
        public string _msgFrezz = "Freeze Value";
        public string _msgTargetError = "Target Error";
        public string _msgDesc = "AUTO CALCULATE";
        public string _msgExqReadErr = "Can not get data from exaquatum";
        //test server
        //private string _mConnectionString = "Data Source=ptt-db-t03.ptt.corp; Initial Catalog=PTT-GSPQualityManagementSystem_test; User ID=pttgspqtstusr; Password=cpttgspqtstusr";

        //product server
        private string _mConnectionString = "Data Source=ptt-db-p03.ptt.corp; Initial Catalog=PTT-GSPQualityManagementSystem; User ID=pttgspqprdusr; Password=cpttgspqprdusr";
        //local
        //private string _mConnectionString = "Data Source=.\\SQLEXPRESS; Initial Catalog=QMSSystem; Integrated Security=SSPI";

        public ExaCommandSQL()
        {

        }

        public ExaCommandSQL(string connectString)
        {
            _mConnectionString = connectString;
        }

        public decimal getConvertTextToDecimal(string textValue)
        {
            decimal decimalValue = 0;

            try
            {
                decimalValue = Decimal.Parse(textValue, NumberStyles.Any, CultureInfo.InvariantCulture); //Convert.ToDecimal(textValue);
            }
            catch (Exception ex)
            {

            }
            return decimalValue;
        } 

        public SqlConnection getConnection()
        {
            //local
            string connectionString = _mConnectionString; // "Data Source=.\\SQLEXPRESS; Initial Catalog=QMSSystem; Integrated Security=SSPI";
           
            SqlConnection connection = new SqlConnection(connectionString);

            return connection;
        } 

        public void insertDatabase(string sqlstring)
        {
            SqlConnection dbConnect = getConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = sqlstring;// "INSERT Region (RegionID, RegionDescription) VALUES (5, 'NorthWestern')";
            cmd.Connection = dbConnect;

            dbConnect.Open();
            int count = cmd.ExecuteNonQuery();
            dbConnect.Close();

            //DataSet userDataset = new DataSet();
            //SqlDataAdapter myDataAdapter = new SqlDataAdapter(
            //       "SELECT au_lname, au_fname FROM Authors WHERE au_id = @au_id",
            //       connection);
            //myCommand.SelectCommand.Parameters.Add("@au_id", SqlDbType.VarChar, 11);
            //myCommand.SelectCommand.Parameters["@au_id"].Value = SSN.Text;
            //myDataAdapter.Fill(userDataset);
        }

        public void getAllProductEXQData()
        {
            try
            {
                string queryString = "SELECT * FROM QMS_MA_PLANT";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Console.WriteLine(String.Format("{0}, {1}", reader[0], reader[1]));
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public List<ViewQMS_MA_PLANT> getAllPlantData()
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_PLANT> listResult = new List<ViewQMS_MA_PLANT>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_PLANT WHERE DELETE_FLAG = 0 ";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand( queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
 
                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_MA_PLANT temp = new ViewQMS_MA_PLANT();
                        temp.ID = Convert.ToInt64( reader["ID"] );
                        temp.NAME = reader["NAME"].ToString();
                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<ViewQMS_MA_PRODUCT_MAPPING> getQMS_MA_PRODUCT_MAPPINGList()
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_PRODUCT_MAPPING> listResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_PRODUCT_MAPPING WHERE DELETE_FLAG = 0 ";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_MA_PRODUCT_MAPPING temp = new ViewQMS_MA_PRODUCT_MAPPING();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.GRADE_ID = Convert.ToInt64(reader["GRADE_ID"]);
                        temp.PRODUCT_DESC = reader["PRODUCT_DESC"].ToString();

                        //temp.PLANT_NAME = reader["PLANT_NAME"].ToString();
                        //temp.GRADE_NAME = reader["GRADE_NAME"].ToString();
                        //temp.PRODUCT_NAME = reader["PRODUCT_NAME"].ToString();
                        temp.HIGHLOW_CHECK = (reader["HIGHLOW_CHECK"].ToString() == "1") ? true : false;

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<ViewQMS_MA_PRODUCT_MAPPING> getQMS_MA_PRODUCT_MAPPINGListByPlantId(long plantId)
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_PRODUCT_MAPPING> listResult = new List<ViewQMS_MA_PRODUCT_MAPPING>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_PRODUCT_MAPPING WHERE DELETE_FLAG = 0 AND  PLANT_ID = " + plantId ;
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    { 
                        ViewQMS_MA_PRODUCT_MAPPING temp = new ViewQMS_MA_PRODUCT_MAPPING();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.GRADE_ID = Convert.ToInt64(reader["GRADE_ID"]);
                        temp.PRODUCT_DESC = reader["PRODUCT_DESC"].ToString();

                        //temp.PLANT_NAME = reader["PLANT_NAME"].ToString();
                        //temp.GRADE_NAME = reader["GRADE_NAME"].ToString();
                        //temp.PRODUCT_NAME = reader["PRODUCT_NAME"].ToString(); 
                        temp.HIGHLOW_CHECK = ( reader["HIGHLOW_CHECK"].ToString() == "1" ) ? true:false;

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString(); 
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        //public void insertDatabase(string sqlstring, SqlCommand command)
        //{
        //    SqlConnection dbConnect = getConnection();

        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandType = System.Data.CommandType.Text;
        //    cmd.CommandText = sqlstring;// "INSERT Region (RegionID, RegionDescription) VALUES (5, 'NorthWestern')";
        //    cmd.Connection = dbConnect; 

        //    dbConnect.Open();
        //    cmd.ExecuteNonQuery();
        //    dbConnect.Close();

        //    //DataSet userDataset = new DataSet();
        //    //SqlDataAdapter myDataAdapter = new SqlDataAdapter(
        //    //       "SELECT au_lname, au_fname FROM Authors WHERE au_id = @au_id",
        //    //       connection);
        //    //myCommand.SelectCommand.Parameters.Add("@au_id", SqlDbType.VarChar, 11);
        //    //myCommand.SelectCommand.Parameters["@au_id"].Value = SSN.Text;
        //    //myDataAdapter.Fill(userDataset);
        //}

        //public void insertToDb()
        //{
        //    //SqlCommand prikaz = new SqlCommand("INSERT INTO klient(name,surname) values(@kname,@ksurname)", spojeni);

        //    //prikaz.Parameters.AddWithValue("@kname", 'sss');
        //    //prikaz.Parameters.AddWithValue("@ksurname", 'sf');
        //    //spojeni.Open();
        //    //prikaz.ExecuteNonQuery();
        //}

        //private static void CreateCommand(string queryString, string connectionString)
        //{
        //    using (SqlConnection connection = new SqlConnection(
        //               connectionString))
        //    {
        //        SqlCommand command = new SqlCommand(queryString, connection);
        //        command.Connection.Open();
        //        command.ExecuteNonQuery();
        //    }
        //}

        //private static void ReadOrderData(string connectionString)
        //{
        //    string queryString =
        //        "SELECT OrderID, CustomerID FROM dbo.Orders;";
        //    using (SqlConnection connection = new SqlConnection(
        //               connectionString))
        //    {
        //        SqlCommand command = new SqlCommand(
        //            queryString, connection);
        //        connection.Open();
        //        SqlDataReader reader = command.ExecuteReader();
        //        try
        //        {
        //            while (reader.Read())
        //            {
        //                Console.WriteLine(String.Format("{0}, {1}",
        //                    reader[0], reader[1]));
        //            }
        //        }
        //        finally
        //        {
        //            // Always call Close when done reading.
        //            reader.Close();
        //        }
        //    }
        //}

        public List<ViewQMS_MA_EXQ_TAG> convertReaderToListViewQMS_MA_EXQ_TAG(SqlDataReader reader)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();
            ViewQMS_MA_EXQ_TAG tempResult;

            try
            {
                while (reader.Read())
                {
                    tempResult = new ViewQMS_MA_EXQ_TAG();
                    tempResult.ID = Convert.ToInt64(reader["ID"]);
                    tempResult.PRODUCT_MAPPING_ID = Convert.ToInt64(reader["PRODUCT_MAPPING_ID"]);
                    tempResult.UNIT_ID = Convert.ToInt64(reader["UNIT_ID"]);
                    //tempResult.UNIT_NAME = getUnitName(listUnit, dt.UNIT_ID);
                    tempResult.CONTROL_ID = Convert.ToInt64(reader["CONTROL_ID"]);
                    tempResult.SPEC_ID = Convert.ToInt64(reader["SPEC_ID"]);
                    tempResult.EXA_TAG_NAME = reader["EXA_TAG_NAME"].ToString();
                    tempResult.EXCEL_NAME = reader["EXCEL_NAME"].ToString();
                    tempResult.TAG_TYPE = Convert.ToByte(reader["TAG_TYPE"]);
                    //tempResult.SHOW_TAG_TYPE = getShowTargetValue(dt.TAG_TYPE),
                    tempResult.TAG_FLOW_CONVERT_VALUE = getConvertTextToDecimal(reader["TAG_FLOW_CONVERT_VALUE"].ToString());//Convert.ToDecimal(reader["TAG_FLOW_CONVERT_VALUE"]);
                    tempResult.TAG_FLOW_CHECK = Convert.ToByte(reader["TAG_FLOW_CHECK"]);
                    tempResult.TAG_FLOW_CHECK_VALUE = getConvertTextToDecimal(reader["TAG_FLOW_CHECK_VALUE"].ToString());//Convert.ToDecimal(reader["TAG_FLOW_CHECK_VALUE"]);
                    tempResult.TAG_TARGET_CHECK = Convert.ToByte(reader["TAG_TARGET_CHECK"]);
                    tempResult.TAG_TARGET_MIN = getConvertTextToDecimal(reader["TAG_TARGET_MIN"].ToString());//Convert.ToDecimal(reader["TAG_TARGET_MIN"]);
                    tempResult.TAG_TARGET_MAX = getConvertTextToDecimal(reader["TAG_TARGET_MAX"].ToString());//Convert.ToDecimal(reader["TAG_TARGET_MAX"]);
                    //SHOW_TAG_TARGET_VALUE = getShowTargetValue(dt.TAG_TARGET_CHECK, dt.TAG_TARGET_MIN),
                    tempResult.TAG_CORRECT = Convert.ToByte(reader["TAG_CORRECT"]);
                    tempResult.TAG_CORRECT_MIN = getConvertTextToDecimal(reader["TAG_CORRECT_MIN"].ToString());//Convert.ToDecimal(reader["TAG_CORRECT_MIN"]);
                    tempResult.TAG_CORRECT_MAX = getConvertTextToDecimal(reader["TAG_CORRECT_MAX"].ToString());// Convert.ToDecimal(reader["TAG_CORRECT_MAX"]);

                    //CONTROL_GROUP_SMAPLE_ID = getControlGroupRowIdByControlDataId(listControlData, dt.CONTROL_ID),
                    //CONTROL_GROUP_ITEM_ID = getControlGroupColumnIdByControlDataId(listControlData, dt.CONTROL_ID),
                    //CONTROL_GROUP_ID = getControlGroupIdByControlDataId(listControlData, dt.CONTROL_ID),
                    //CONTROL_VALUE = getControlValueTextByControlDataId(listControlData, dt.CONTROL_ID),

                    //SPEC_GROUP_SMAPLE_ID = getControlGroupRowIdByControlDataId(listSpecData, dt.SPEC_ID),
                    //SPEC_GROUP_ITEM_ID = getControlGroupColumnIdByControlDataId(listSpecData, dt.SPEC_ID),
                    //SPEC_GROUP_ID = getControlGroupIdByControlDataId(listSpecData, dt.SPEC_ID),
                    //SPEC_VALUE = getControlValueTextByControlDataId(listSpecData, dt.SPEC_ID),

                    //SHOW_CONVERT_VAlUE = getShowConvertValue(dt.TAG_TYPE, dt.TAG_FLOW_CONVERT_VALUE),
                    //SHOW_PROOF_VALUE = getShowProofValue(dt.TAG_TYPE, dt.TAG_FLOW_CHECK_VALUE),

                    tempResult.POSITION = Convert.ToInt16(reader["POSITION"]);

                    tempResult.CREATE_USER = reader["POSITION"].ToString();
                    tempResult.UPDATE_USER = reader["UPDATE_USER"].ToString();
                    //tempResult.SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy hh:mm:ss");
                    //tempResult.SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy hh:mm:ss");

                    tempResult.ARROW_DOWN = true;
                    tempResult.ARROW_UP = true;

                    listResult.Add(tempResult);
                }
            }
            catch
            {

            }
            return listResult;
        }

        public string getSQLControlColumnName(List<ViewQMS_MA_CONTROL_COLUMN> listData, long id)
        {
            string szResult = "";
            try
            {
                foreach (ViewQMS_MA_CONTROL_COLUMN dt in listData)
                {
                    if (dt.ID == id)
                    {
                        szResult = dt.ITEM; break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return szResult;
        } 

        public List<ViewQMS_MA_CONTROL_DATA> convertReaderToListViewQMS_MA_CONTROL_DATA(SqlDataReader reader)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();
            //List<ViewQMS_MA_CONTROL_DATA> listUnitName


            ViewQMS_MA_CONTROL_DATA tempResult;

            try
            {
                while (reader.Read())
                {
                    tempResult = new ViewQMS_MA_CONTROL_DATA();
                    tempResult.ID =  Convert.ToInt64(reader["ID"]);
                    //SHOW_ON_TREND_FLAG = (dt.SHOW_ON_TREND_FLAG == 1) ? true : false,
                    tempResult.CONTROL_ROW_ID = Convert.ToInt64(reader["CONTROL_ROW_ID"]);
                    tempResult.CONTROL_COLUMN_ID = Convert.ToInt64(reader["CONTROL_COLUMN_ID"]);
                    tempResult.CONTROL_COLUMN_NAME = reader["ITEM"].ToString();
                    tempResult.CONTROL_ID =  Convert.ToInt64(reader["CONTROL_ID"]);
                    tempResult.MAX_FLAG = (Convert.ToByte(reader["MAX_FLAG"])== 1) ? true : false;
                    tempResult.MAX_VALUE = getConvertTextToDecimal(reader["MAX_VALUE"].ToString()); //Convert.ToDecimal(reader["MAX_VALUE"]);
                    tempResult.MAX_NO_CAL = (Convert.ToByte(reader["MAX_NO_CAL"]) == 1) ? true : false;

                    tempResult.MIN_FLAG = (Convert.ToByte(reader["MIN_FLAG"])== 1) ? true : false;
                    tempResult.MIN_VALUE = getConvertTextToDecimal(reader["MIN_VALUE"].ToString()); // Convert.ToDecimal(reader["MIN_VALUE"]);
                    tempResult.MIN_NO_CAL = (Convert.ToByte(reader["MIN_NO_CAL"]) == 1) ? true : false;
                  
                    tempResult.CONC_FLAG = (Convert.ToByte(reader["CONC_FLAG"])== 1) ? true : false;
                    tempResult.CONC_VALUE = getConvertTextToDecimal(reader["CONC_VALUE"].ToString()); //Convert.ToDecimal(reader["CONC_VALUE"]);
                    tempResult.CONC_NO_CAL = (Convert.ToByte(reader["CONC_NO_CAL"]) == 1) ? true : false;

                    tempResult.LOADING_FLAG = (Convert.ToByte(reader["LOADING_FLAG"])== 1) ? true : false;
                    tempResult.LOADING_VALUE = getConvertTextToDecimal(reader["LOADING_VALUE"].ToString()); //Convert.ToDecimal(reader["LOADING_VALUE"]);
                    tempResult.LOADING_NO_CAL = (Convert.ToByte(reader["LOADING_NO_CAL"]) == 1) ? true : false;

                    tempResult.UNIT_NAME = reader["NAME"].ToString();  //getUnitName(listColumn, listUnit, dt.CONTROL_COLUMN_ID)

                    listResult.Add(tempResult);
                }
            }
            catch
            {

            }
            return listResult;
        }

        #region get ExaTag Data

        public List<ViewQMS_MA_EXQ_TAG> getSQLListQMS_MA_EXQ_TAG(long id, byte type)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                ViewQMS_MA_EXQ_TAG tempResult = new ViewQMS_MA_EXQ_TAG();
                string queryString = "SELECT * FROM QMS_MA_EXQ_TAG WHERE PRODUCT_MAPPING_ID = @ID AND ";

                if (type == 0) // เอาทุก tag
                {
                    queryString += " ORDER BY TAG_TYPE , POSITION ";
                }
                else
                {
                    if (type == (byte)EXA_TAG_TYPE.QUALITY_TAG) //เป็น tag คุณภาพ
                    {
                        queryString += " TAG_TYPE = " + (byte)EXA_TAG_TYPE.QUALITY_TAG;
                    }
                    else if (type == (byte)EXA_TAG_TYPE.ACCUM) //Accum
                    {
                        queryString += " TAG_TYPE = " + (byte)EXA_TAG_TYPE.ACCUM;
                    } 
                    else //0 เอาทั้งหมด 
                    {
                        queryString += " ( TAG_TYPE = " + (byte)EXA_TAG_TYPE.QUANTITY_PV + " OR TAG_TYPE = " + (byte)EXA_TAG_TYPE.QUANTITY_SUM + " ) ";
                    }

                    queryString += " ORDER BY POSITION ";

                }
                 
                 
                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();
                try
                {
                    listResult = convertReaderToListViewQMS_MA_EXQ_TAG(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                } 
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getSQLQualityQMS_MA_EXQ_TAGListByProductMapId(long id)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                listResult = getSQLListQMS_MA_EXQ_TAG( id, (byte)EXA_TAG_TYPE.QUALITY_TAG);
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getSQLQuantityQMS_MA_EXQ_TAGListByProductMapId(long id)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                listResult = getSQLListQMS_MA_EXQ_TAG(id, (byte)EXA_TAG_TYPE.QUANTITY_PV);
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getSQLAccumTagQMS_MA_EXQ_TAGListByProductMapId(long id)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            { 
                listResult = getSQLListQMS_MA_EXQ_TAG(id, (byte)EXA_TAG_TYPE.ACCUM);
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getSQLgetListEXQTagByListId(long id)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            { 
                listResult = getSQLListQMS_MA_EXQ_TAG(id, (byte)EXA_TAG_TYPE.ALL);
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }
        

        #endregion

        #region SaveLog


        public long SaveSQLListQMS_ST_SYSTEM_LOG( List<CreateQMS_ST_SYSTEM_LOG> listData)
        {
            long result = 0;
            try
            {

                if (null != listData)
                {
                    //Step 1. Delete duplicate data...
                    SqlConnection conn = getConnection();
                    SqlCommand SQLcommand;
                     
                    conn.Open(); 
                    //Step 2. Add Data 

                    SQLcommand = new SqlCommand("INSERT INTO QMS_ST_SYSTEM_LOG   "
                                            + " ( LOG_DATE, ERROR_LEVEL, PROCESS_NAME, PROCESS_DESC  ) "
                                            + " VALUES(@LOG_DATE, @ERROR_LEVEL, @PROCESS_NAME, @PROCESS_DESC )  ", conn);

                    CreateQMS_ST_SYSTEM_LOG temp = new CreateQMS_ST_SYSTEM_LOG();

                    SQLcommand.Parameters.AddWithValue("@LOG_DATE", temp.LOG_DATE);
                    SQLcommand.Parameters.AddWithValue("@ERROR_LEVEL", temp.ERROR_LEVEL);
                    SQLcommand.Parameters.AddWithValue("@PROCESS_NAME", temp.PROCESS_NAME);
                    SQLcommand.Parameters.AddWithValue("@PROCESS_DESC", temp.PROCESS_DESC); 

                    foreach (CreateQMS_ST_SYSTEM_LOG dtEXQProduct in listData)
                    {
                        SQLcommand.Parameters["@LOG_DATE"].Value = dtEXQProduct.LOG_DATE;
                        SQLcommand.Parameters["@ERROR_LEVEL"].Value = dtEXQProduct.ERROR_LEVEL;
                        SQLcommand.Parameters["@PROCESS_NAME"].Value = dtEXQProduct.PROCESS_NAME;
                        SQLcommand.Parameters["@PROCESS_DESC"].Value = dtEXQProduct.PROCESS_DESC;
                        SQLcommand.ExecuteNonQuery();
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return result;
        }

        #endregion 

        #region SaveExaData

        public CreateAbnormalSetting getAllConfigAbnormalSetting()
        {
            //DataTable result = new DataTable();
            CreateAbnormalSetting dtResult = new CreateAbnormalSetting();
            try
            {
                string queryString = "SELECT * FROM QMS_ST_SYSTEM_CONFIG  ";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    { 
                        dtResult.ABNORMAL_MAX_REPEAT = Convert.ToInt16(reader["ABNORMAL_MAX_REPEAT"]);
                        dtResult.ABNORMAL_OVERSHOOT = Convert.ToInt16(reader["ABNORMAL_OVERSHOOT"]);
                        dtResult.ABNORMAL_ALPHABET1 = reader["ABNORMAL_ALPHABET1"].ToString();
                        dtResult.ABNORMAL_ALPHABET2 = reader["ABNORMAL_ALPHABET2"].ToString();
                        break;
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return dtResult;
        }

        public List<long> getControlIdAndSpecIdFromExaList(List<ViewQMS_MA_EXQ_TAG> listExaValue)
        {
            List<long> listResult = new List<long>();
            bool bFound;
            try
            {
                foreach (ViewQMS_MA_EXQ_TAG dt in listExaValue)
                {
                    //Control ID add
                    if (dt.CONTROL_ID > 0)
                    {
                        bFound = false;
                        for (int i = 0; i < listResult.ToArray().Length; i++)
                        {
                            if (dt.CONTROL_ID == listResult[i])
                            {
                                bFound = true;
                            }
                        }

                        if (false == bFound)
                        {
                            listResult.Add(dt.CONTROL_ID);
                        } 
                    }
                  

                    //Spec ID add
                    if (dt.SPEC_ID > 0)
                    {
                        bFound = false;
                        for (int i = 0; i < listResult.ToArray().Length; i++)
                        {
                            if (dt.SPEC_ID == listResult[i])
                            {
                                bFound = true;
                            }
                        }

                        if (false == bFound )
                        {
                            listResult.Add(dt.SPEC_ID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return listResult;
        }

        public string CheckOffControlOffSpec(string tagConvert, string tagGCError, ViewQMS_MA_CONTROL_DATA controlData, ViewQMS_MA_CONTROL_DATA specData)
        {
            Decimal valueTag = 0;
            try
            {
                string[] TAG_CONVERT = tagConvert.Split(',');
                string[] TAG_GCERROR = tagGCError.Split(',');

                //step1 . ดึงค่าที่สนใจ
                if (TAG_CONVERT.Length > 0)
                {
                    for (int i = 0; i < TAG_CONVERT.Length; i++)
                    {
                        #region check off control ..
                        try
                        {
                            if (TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.GC_ERROR).ToString() || TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.GC_ERROR_FLOWRATE).ToString())
                            {
                                valueTag = getConvertTextToDecimal(TAG_CONVERT[i]); //Convert.ToDecimal(TAG_CONVERT[i]);
                                if (null != controlData)
                                {
                                    if (controlData.MAX_FLAG == true && valueTag > controlData.MAX_VALUE)
                                    {
                                        TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString();
                                    }

                                    if (controlData.MIN_FLAG == true && valueTag < controlData.MIN_VALUE) //ไม่มีทางเป็นไปได้ ที่จะ off ทั้ง low และ high
                                    {
                                        TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.OFF_CONTROL_LOW).ToString();
                                    }
                                }

                                if (null != specData)
                                {
                                    if (specData.MAX_FLAG == true && valueTag > specData.MAX_VALUE)
                                    {
                                        if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString())
                                        {
                                            TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.OFF_CONTROL_AND_SPEC_HIGH).ToString();
                                        }
                                        else
                                        {
                                            TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.OFF_SPEC_HIGH).ToString();
                                        }
                                    }

                                    if (specData.MIN_FLAG == true && valueTag < specData.MIN_VALUE)
                                    {
                                        if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_SPEC_LOW).ToString())
                                        {
                                            TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.OFF_CONTORL_AND_SPEC_LOW).ToString();
                                        }
                                        else
                                        {
                                            TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.OFF_SPEC_LOW).ToString();
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {

                        }
                        #endregion

                    }
                    tagGCError = convertStringArrayToString(TAG_GCERROR);// String.Join(",", TAG_GCERROR.Select(x => x.ToString()).ToArray());
                }

            }
            catch (Exception ex)
            {

            }
            return tagGCError;
        }

        public void recheckOffControl(ExaProductSearchModel searchModel)
        {
             //DataTable result = new DataTable();
            List<ViewQMS_TR_EXQ_PRODUCT> listResult = new List<ViewQMS_TR_EXQ_PRODUCT>();
            try
            {

                List<ViewQMS_TR_EXQ_PRODUCT> listProductData = this.GetSQLAllQMS_TR_EXQ_PRODUCTBySearch(searchModel);

                if (listProductData.ToArray().Length > 0)
                {
                    foreach (ViewQMS_TR_EXQ_PRODUCT dtEXQProduct in listProductData)
                    {

                        //ViewQMS_MA_EXQ_TAG cuurentTag = getViewQMS_TR_EXQ_PRODUCTFromListById(listExaValue, dtEXQProduct.TAG_EXA_ID);

                        ////2.1 ตรวจสอบ ค่าซ้ำ
                        //if (cuurentTag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG)
                        //{ //ตรวจเฉพาะ tag quality เพราะค่าการไหล อาจเป็นการไหลคงที่
                        //    dtEXQProduct.TAG_GCERROR = CheckGCErrorRepeatValueEx(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, configData.ABNORMAL_MAX_REPEAT);
                        //}

                        ////2.2 ตรวจสอบ ค่าควบคุม ไม่คิดที่ accum
                        //if (null != cuurentTag && (cuurentTag.CONTROL_ID > 0 || cuurentTag.SPEC_ID > 0) && cuurentTag.TAG_TYPE != (byte)EXA_TAG_TYPE.ACCUM) //check off control 
                        //{
                        //    controlData = getControlDataById(listControlSpec, cuurentTag.CONTROL_ID);
                        //    specData = getControlDataById(listControlSpec, cuurentTag.SPEC_ID);
                        //    dtEXQProduct.TAG_GCERROR = CheckOffControlOffSpec(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, controlData, specData);
                        //}
                    }
                }
                 
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
             
        }

        public long SaveSQLListCreateQMS_TR_EXQ_PRODUCT(List<CreateQMS_TR_EXQ_PRODUCT> listData, List<ViewQMS_MA_EXQ_TAG> listExaValue, ExaProductSearchModel search)
        {
            long result = 0;
            int qty = 0;

            try
            {
                if (null != listData)
                {
                    //Step 0. Prepare data for cal
                    CreateAbnormalSetting configData = getAllConfigAbnormalSetting();
                    long[] idsControl = getControlIdAndSpecIdFromExaList(listExaValue).ToArray();
                    List<ViewQMS_MA_CONTROL_DATA> listControlSpec = getQMS_MA_CONTROL_DATAListByListIds(idsControl);
                    ViewQMS_MA_CONTROL_DATA controlData = new ViewQMS_MA_CONTROL_DATA();
                    ViewQMS_MA_CONTROL_DATA specData = new ViewQMS_MA_CONTROL_DATA();

                    //Step 1. Delete duplicate data...
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_EXQ_PRODUCT   " 
                                            + " WHERE PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                                            + " AND TAG_DATE >= @START_DATE AND TAG_DATE <= @END_DATE ", conn);

                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", search.PRODUCT_ID);
                    SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                    SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                    conn.Open();
                    SQLcommand.ExecuteNonQuery();

                    //Step 2. Add Data 

                    SQLcommand = new SqlCommand("INSERT INTO QMS_TR_EXQ_PRODUCT   "
                                            + " ( PLANT_ID, PRODUCT_ID, TAG_DATE, TAG_EXA_ID ,  "
                                            + " TAG_VALUE, TAG_CONVERT, TAG_GCERROR, STATUS_1, STATUS_2 ) "
                                            + " VALUES(@PLANT_ID, @PRODUCT_ID, @TAG_DATE, @TAG_EXA_ID, "
                                            + " @TAG_VALUE,  @TAG_CONVERT, @TAG_GCERROR, @STATUS_1, @STATUS_2 )  " , conn);

                    CreateQMS_TR_EXQ_PRODUCT temp = new CreateQMS_TR_EXQ_PRODUCT();

                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", temp.PLANT_ID);
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", temp.PRODUCT_ID);
                    SQLcommand.Parameters.AddWithValue("@TAG_DATE", temp.TAG_DATE);
                    SQLcommand.Parameters.AddWithValue("@TAG_EXA_ID", temp.TAG_EXA_ID);

                    SQLcommand.Parameters.AddWithValue("@TAG_VALUE", temp.TAG_VALUE);
                    SQLcommand.Parameters.AddWithValue("@TAG_CONVERT", temp.TAG_CONVERT);
                    SQLcommand.Parameters.AddWithValue("@TAG_GCERROR", temp.TAG_GCERROR);
                    SQLcommand.Parameters.AddWithValue("@STATUS_1", temp.STATUS_1);
                    SQLcommand.Parameters.AddWithValue("@STATUS_2", temp.STATUS_2);

                    foreach (CreateQMS_TR_EXQ_PRODUCT dtEXQProduct in listData)
                    {

                        ViewQMS_MA_EXQ_TAG cuurentTag = getViewQMS_TR_EXQ_PRODUCTFromListById(listExaValue, dtEXQProduct.TAG_EXA_ID);

                        //2.0  ตรวจสอบว่าเป็น tag accum หรือเปล่า
                        if (cuurentTag.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM)
                        {
                            dtEXQProduct.TAG_CONVERT = GetAccumTagValueEx(listData, cuurentTag);
                            dtEXQProduct.TAG_VALUE = dtEXQProduct.TAG_CONVERT;
                        } 

                        //2.1 ตรวจสอบ ค่าซ้ำ
                        if (cuurentTag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUALITY_TAG || cuurentTag.TAG_TYPE == (byte)EXA_TAG_TYPE.ACCUM) //
                        { //ตรวจเฉพาะ tag quality เพราะค่าการไหล อาจเป็นการไหลคงที่
                            dtEXQProduct.TAG_GCERROR = CheckGCErrorRepeatValueEx(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, configData.ABNORMAL_MAX_REPEAT);
                        }

                        //2.2 ตรวจสอบ ค่าควบคุม ไม่คิดที่ accum (เปลี่ยนเป็น คิดหมด ถ้ามี ค่า control id หรือ spec id )
                        if (null != cuurentTag && (cuurentTag.CONTROL_ID > 0 || cuurentTag.SPEC_ID > 0)) // && cuurentTag.TAG_TYPE != (byte)EXA_TAG_TYPE.ACCUM) //check off control 
                        {
                            controlData = getControlDataById(listControlSpec, cuurentTag.CONTROL_ID);
                            specData = getControlDataById(listControlSpec, cuurentTag.SPEC_ID);
                            dtEXQProduct.TAG_GCERROR = CheckOffControlOffSpec(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, controlData, specData);
                        }

                        SQLcommand.Parameters["@PLANT_ID"].Value = dtEXQProduct.PLANT_ID;
                        SQLcommand.Parameters["@PRODUCT_ID"].Value = dtEXQProduct.PRODUCT_ID;
                        SQLcommand.Parameters["@TAG_DATE"].Value = dtEXQProduct.TAG_DATE;
                        SQLcommand.Parameters["@TAG_EXA_ID"].Value = dtEXQProduct.TAG_EXA_ID;

                        SQLcommand.Parameters["@TAG_VALUE"].Value = dtEXQProduct.TAG_VALUE;
                        SQLcommand.Parameters["@TAG_CONVERT"].Value = dtEXQProduct.TAG_CONVERT;
                        SQLcommand.Parameters["@TAG_GCERROR"].Value = dtEXQProduct.TAG_GCERROR;
                        SQLcommand.Parameters["@STATUS_1"].Value = dtEXQProduct.STATUS_1;
                        SQLcommand.Parameters["@STATUS_2"].Value = dtEXQProduct.STATUS_2;

                        SQLcommand.ExecuteNonQuery();
                    }
                     
                    conn.Close();
                }

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            } 
            return result;
        }

        #endregion

        #region Save Reduce Feed 

        
        public List<ViewQMS_MA_REDUCE_FEED> getAllQMS_MA_REDUCE_FEEDByPlantId(long id)
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_REDUCE_FEED> listResult = new List<ViewQMS_MA_REDUCE_FEED>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_REDUCE_FEED WHERE DELETE_FLAG = 0 AND PLANT_ID =  " + id + " ORDER BY ACTIVE_DATE ";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_MA_REDUCE_FEED temp = new ViewQMS_MA_REDUCE_FEED();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.EXCEL_NAME = reader["EXCEL_NAME"].ToString();
                        temp.LIMIT_VALUE = getConvertTextToDecimal(reader["LIMIT_VALUE"].ToString()); //Convert.ToDecimal(reader["LIMIT_VALUE"]);
                        temp.UNIT_ID = Convert.ToInt64(reader["UNIT_ID"]);
                        temp.ACTIVE_DATE = Convert.ToDateTime(reader["ACTIVE_DATE"]);
                        temp.REDUCE_FEED_DESC = reader["REDUCE_FEED_DESC"].ToString();
                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }


        public List<ViewQMS_MA_REDUCE_FEED_DETAIL> getActiveAllByREDUCE_FEED_ID(long id, decimal limitValue)
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_REDUCE_FEED_DETAIL> listResult = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_REDUCE_FEED_DETAIL WHERE DELETE_FLAG = 0 AND REDUCE_FEED_ID =  " + id;
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_MA_REDUCE_FEED_DETAIL temp = new ViewQMS_MA_REDUCE_FEED_DETAIL();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.REDUCE_FEED_ID = Convert.ToInt64(reader["REDUCE_FEED_ID"]);
                        temp.EXA_TAG_NAME = reader["EXA_TAG_NAME"].ToString();
                        temp.CONVERT_VALUE = getConvertTextToDecimal(reader["CONVERT_VALUE"].ToString()); //Convert.ToDecimal(reader["CONVERT_VALUE"]);
                        temp.LIMIT_VALUE = limitValue;
                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<ViewQMS_MA_REDUCE_FEED_DETAIL> getQMS_MA_REDUCE_FEED_DETAILActiveListByPlantAndActiveDate(long plantId, DateTime activeDate)
        {
            List<ViewQMS_MA_REDUCE_FEED_DETAIL> listResult = new List<ViewQMS_MA_REDUCE_FEED_DETAIL>();
            try
            {
                //Get All Reduce feed by plantId
                List<ViewQMS_MA_REDUCE_FEED> listReduce = getAllQMS_MA_REDUCE_FEEDByPlantId(plantId);
                long reduceFeedId = 0;
                decimal limitValue = 0;

                if (listReduce.ToArray().Length > 0) //ถ้ามี list Downtime แสดงว่า plant นี้มีการตรวจสอบ
                {
                    if (listReduce.ToArray().Length == 1)//มีข้อมูล เพียงชุดเดียว
                    {
                        reduceFeedId = listReduce[0].ID;
                        limitValue = listReduce[0].LIMIT_VALUE;
                    }
                    else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                    {
                        foreach (ViewQMS_MA_REDUCE_FEED dt in listReduce)
                        {
                            reduceFeedId = dt.ID;
                            limitValue = dt.LIMIT_VALUE;
                            if (activeDate >= dt.ACTIVE_DATE)
                            {
                                break;//set อย่างน้อย 1 ตัว
                            }
                        }
                    }

                    listResult = getActiveAllByREDUCE_FEED_ID(reduceFeedId, limitValue);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public long SaveSQLListQMS_TR_EXQ_REDUCE_FEED(List<CreateQMS_TR_EXQ_REDUCE_FEED> listData, List<ViewQMS_MA_REDUCE_FEED_DETAIL> exaValue, ExaReduceFeedSearchModel search)
        {
            long result = 0;

            try
            {
                if (null != listData)
                {
                    CreateAbnormalSetting configData = getAllConfigAbnormalSetting();

                    //Step 1. Delete duplicate data...
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_EXQ_REDUCE_FEED   "
                                            + " WHERE PLANT_ID = @PLANT_ID  "
                                            + " AND TAG_DATE >= @START_DATE AND TAG_DATE <= @END_DATE ", conn);

                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID); 
                    SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                    SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                    conn.Open();
                    SQLcommand.ExecuteNonQuery();

                    //Step 2. Add Data 
                    SQLcommand = new SqlCommand("INSERT INTO QMS_TR_EXQ_REDUCE_FEED   "
                                            + " ( PLANT_ID,  TAG_DATE, TAG_EXA_ID ,  "
                                            + " TAG_VALUE, TAG_CONVERT, TAG_GCERROR, STATUS_1, STATUS_2 ) "
                                            + " VALUES(@PLANT_ID,  @TAG_DATE, @TAG_EXA_ID, "
                                            + " @TAG_VALUE,  @TAG_CONVERT, @TAG_GCERROR, @STATUS_1, @STATUS_2 )  ", conn);

                    CreateQMS_TR_EXQ_REDUCE_FEED temp = new CreateQMS_TR_EXQ_REDUCE_FEED();

                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", temp.PLANT_ID); 
                    SQLcommand.Parameters.AddWithValue("@TAG_DATE", temp.TAG_DATE);
                    SQLcommand.Parameters.AddWithValue("@TAG_EXA_ID", temp.TAG_EXA_ID);

                    SQLcommand.Parameters.AddWithValue("@TAG_VALUE", temp.TAG_VALUE);
                    SQLcommand.Parameters.AddWithValue("@TAG_CONVERT", temp.TAG_CONVERT);
                    SQLcommand.Parameters.AddWithValue("@TAG_GCERROR", temp.TAG_GCERROR);
                    SQLcommand.Parameters.AddWithValue("@STATUS_1", temp.STATUS_1);
                    SQLcommand.Parameters.AddWithValue("@STATUS_2", temp.STATUS_2);

                    foreach (CreateQMS_TR_EXQ_REDUCE_FEED dtEXQProduct in listData)
                    {
                        //2.1 ตรวจสอบ ค่าซ้ำ
                        
                        if(exaValue.Count > 0){ // มีการส่งค่า ที่ใช้ตรวจสอบมา
                            dtEXQProduct.TAG_GCERROR = CheckSQLReduceFeed(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, exaValue[0].LIMIT_VALUE);
                        }else{//ตรวจเฉพาะ tag quality เพราะค่าการไหล อาจเป็นการไหลคงที่ 
                            dtEXQProduct.TAG_GCERROR = CheckGCErrorRepeatValueEx(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, configData.ABNORMAL_MAX_REPEAT);
                        }

                        SQLcommand.Parameters["@PLANT_ID"].Value = dtEXQProduct.PLANT_ID; 
                        SQLcommand.Parameters["@TAG_DATE"].Value = dtEXQProduct.TAG_DATE;
                        SQLcommand.Parameters["@TAG_EXA_ID"].Value = dtEXQProduct.TAG_EXA_ID;

                        SQLcommand.Parameters["@TAG_VALUE"].Value = dtEXQProduct.TAG_VALUE;
                        SQLcommand.Parameters["@TAG_CONVERT"].Value = dtEXQProduct.TAG_CONVERT;
                        SQLcommand.Parameters["@TAG_GCERROR"].Value = dtEXQProduct.TAG_GCERROR;
                        SQLcommand.Parameters["@STATUS_1"].Value = dtEXQProduct.STATUS_1;
                        SQLcommand.Parameters["@STATUS_2"].Value = dtEXQProduct.STATUS_2;

                        SQLcommand.ExecuteNonQuery();
                    }

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result; 
        }

        #endregion

        #region Save Downtime

        public List<ViewQMS_MA_DOWNTIME> getAllQMS_MA_DOWNTIMEByPlantId(long id)
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_DOWNTIME> listResult = new List<ViewQMS_MA_DOWNTIME>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_DOWNTIME WHERE DELETE_FLAG = 0 AND PLANT_ID =  " + id + " ORDER BY ACTIVE_DATE ";
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_MA_DOWNTIME temp = new ViewQMS_MA_DOWNTIME();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.EXCEL_NAME = reader["EXCEL_NAME"].ToString();
                        temp.LIMIT_VALUE = getConvertTextToDecimal(reader["LIMIT_VALUE"].ToString()); //Convert.ToDecimal(reader["LIMIT_VALUE"]);
                        temp.UNIT_ID = Convert.ToInt64(reader["UNIT_ID"]);
                        temp.ACTIVE_DATE = Convert.ToDateTime(reader["ACTIVE_DATE"]);
                        temp.DOWNTIME_DESC = reader["DOWNTIME_DESC"].ToString();
                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<ViewQMS_MA_DOWNTIME_DETAIL> getActiveAllByDOWNTIME_ID(long id, decimal LimitValue)
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_DOWNTIME_DETAIL> listResult = new List<ViewQMS_MA_DOWNTIME_DETAIL>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_DOWNTIME_DETAIL WHERE DELETE_FLAG = 0 AND DOWNTIME_ID =  " + id  ;
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_MA_DOWNTIME_DETAIL temp = new ViewQMS_MA_DOWNTIME_DETAIL();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.DOWNTIME_ID = Convert.ToInt64(reader["DOWNTIME_ID"]);
                        temp.EXA_TAG_NAME = reader["EXA_TAG_NAME"].ToString();
                        temp.CONVERT_VALUE = getConvertTextToDecimal(reader["CONVERT_VALUE"].ToString()); //Convert.ToDecimal(reader["CONVERT_VALUE"]);
                        temp.MIN_LIMIT = LimitValue;
                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public List<ViewQMS_MA_DOWNTIME_DETAIL> getQMS_MA_DOWNTIME_DETAILActiveListByPlantAndActiveDate( long plantId, DateTime activeDate)
        {
            List<ViewQMS_MA_DOWNTIME_DETAIL> listResult = new List<ViewQMS_MA_DOWNTIME_DETAIL>(); 
            try
            {
                //Get All Reduce feed by plantId
                List<ViewQMS_MA_DOWNTIME> listDowntime = getAllQMS_MA_DOWNTIMEByPlantId(plantId);
                long downTimeId = 0;
                decimal limitValue = 0;

                if (listDowntime.ToArray().Length > 0) //ถ้ามี list Downtime แสดงว่า plant นี้มีการตรวจสอบ
                {
                    if (listDowntime.ToArray().Length == 1)//มีข้อมูล เพียงชุดเดียว
                    {
                        downTimeId = listDowntime[0].ID;
                        limitValue = listDowntime[0].LIMIT_VALUE;
                    }
                    else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                    {
                        foreach (ViewQMS_MA_DOWNTIME dt in listDowntime)
                        {
                            downTimeId = dt.ID;
                            limitValue = dt.LIMIT_VALUE;
                            if (activeDate >= dt.ACTIVE_DATE)
                            {
                                break;//set อย่างน้อย 1 ตัว
                            }
                        }
                    }

                    listResult = getActiveAllByDOWNTIME_ID(downTimeId, limitValue); 

                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public long SaveSQLListQMS_TR_EXQ_DOWNTIME( List<CreateQMS_TR_EXQ_DOWNTIME> listData, List<ViewQMS_MA_DOWNTIME_DETAIL> exaValue, ExaDowntimeSearchModel search)
        {
            long result = 0;

            try
            {
                if (null != listData)
                {

                    //Step 0. Prepare data for cal
                    CreateAbnormalSetting configData = getAllConfigAbnormalSetting();

                    //Step 1. Delete duplicate data...
                    SqlConnection conn = getConnection();

                    SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_EXQ_DOWNTIME   "
                                            + " WHERE PLANT_ID = @PLANT_ID  "
                                            + " AND TAG_DATE >= @START_DATE AND TAG_DATE <= @END_DATE ", conn);

                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                    SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                    SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                    conn.Open();
                    SQLcommand.ExecuteNonQuery();

                    //Step 2. Add Data 

                    SQLcommand = new SqlCommand("INSERT INTO QMS_TR_EXQ_DOWNTIME   "
                                            + " ( PLANT_ID, TAG_DATE, TAG_EXA_ID ,  "
                                            + " TAG_VALUE, TAG_CONVERT, TAG_GCERROR, STATUS_1, STATUS_2 ) "
                                            + " VALUES(@PLANT_ID,  @TAG_DATE, @TAG_EXA_ID, "
                                            + " @TAG_VALUE,  @TAG_CONVERT, @TAG_GCERROR, @STATUS_1, @STATUS_2 )  ", conn);

                    CreateQMS_TR_EXQ_DOWNTIME temp = new CreateQMS_TR_EXQ_DOWNTIME();

                    SQLcommand.Parameters.AddWithValue("@PLANT_ID", temp.PLANT_ID);
                    SQLcommand.Parameters.AddWithValue("@TAG_DATE", temp.TAG_DATE);
                    SQLcommand.Parameters.AddWithValue("@TAG_EXA_ID", temp.TAG_EXA_ID);

                    SQLcommand.Parameters.AddWithValue("@TAG_VALUE", temp.TAG_VALUE);
                    SQLcommand.Parameters.AddWithValue("@TAG_CONVERT", temp.TAG_CONVERT);
                    SQLcommand.Parameters.AddWithValue("@TAG_GCERROR", temp.TAG_GCERROR);
                    SQLcommand.Parameters.AddWithValue("@STATUS_1", temp.STATUS_1);
                    SQLcommand.Parameters.AddWithValue("@STATUS_2", temp.STATUS_2);

                    foreach (CreateQMS_TR_EXQ_DOWNTIME dtEXQProduct in listData)
                    {
                        //2.1 ตรวจสอบ ค่าซ้ำ
                        
                        if(exaValue.Count > 0){ // มีการส่งค่า ที่ใช้ตรวจสอบมา
                            dtEXQProduct.TAG_GCERROR = CheckSQLDowntime(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, exaValue[0].MIN_LIMIT);
                        }else{//ตรวจเฉพาะ tag quality เพราะค่าการไหล อาจเป็นการไหลคงที่
                            dtEXQProduct.TAG_GCERROR = CheckGCErrorRepeatValueEx(dtEXQProduct.TAG_CONVERT, dtEXQProduct.TAG_GCERROR, configData.ABNORMAL_MAX_REPEAT);
                        }

                        SQLcommand.Parameters["@PLANT_ID"].Value = dtEXQProduct.PLANT_ID;
                        SQLcommand.Parameters["@TAG_DATE"].Value = dtEXQProduct.TAG_DATE;
                        SQLcommand.Parameters["@TAG_EXA_ID"].Value = dtEXQProduct.TAG_EXA_ID;

                        SQLcommand.Parameters["@TAG_VALUE"].Value = dtEXQProduct.TAG_VALUE;
                        SQLcommand.Parameters["@TAG_CONVERT"].Value = dtEXQProduct.TAG_CONVERT;
                        SQLcommand.Parameters["@TAG_GCERROR"].Value = dtEXQProduct.TAG_GCERROR;
                        SQLcommand.Parameters["@STATUS_1"].Value = dtEXQProduct.STATUS_1;
                        SQLcommand.Parameters["@STATUS_2"].Value = dtEXQProduct.STATUS_2;

                        SQLcommand.ExecuteNonQuery();
                    }

                    conn.Close();
                }

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }
        #endregion

        #region GetExaData 
        public List<ViewQMS_TR_EXQ_PRODUCT> GetSQLAllQMS_TR_EXQ_PRODUCTBySearch(ExaProductSearchModel search)
        {
            List<ViewQMS_TR_EXQ_PRODUCT> listResult = new List<ViewQMS_TR_EXQ_PRODUCT>();
            int qty = 0;

            try
            {

                string queryString = "SELECT * FROM QMS_TR_EXQ_PRODUCT  "
                                + " WHERE PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                                + " AND TAG_DATE >= @START_DATE "
                                + " AND TAG_DATE <= @END_DATE " ; 

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", search.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();


                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_TR_EXQ_PRODUCT temp = new ViewQMS_TR_EXQ_PRODUCT();

                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.TAG_DATE = Convert.ToDateTime(reader["TAG_DATE"]);
                        temp.TAG_EXA_ID = Convert.ToInt64(reader["TAG_EXA_ID"]);

                        temp.TAG_VALUE = reader["TAG_VALUE"].ToString();
                        temp.TAG_CONVERT = reader["TAG_CONVERT"].ToString();
                        temp.TAG_GCERROR = reader["TAG_GCERROR"].ToString();
                        temp.STATUS_1 = Convert.ToByte( reader["STATUS_1"] );
                        temp.STATUS_2 = Convert.ToByte( reader["STATUS_2"] );
                         
                        listResult.Add(temp);
                    } 
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return listResult;
        }

        public List<ViewQMS_TR_EXQ_DOWNTIME> GetSQLAllQMS_TR_EXQ_DOWNTIMEBySearch(ExaProductSearchModel search)
        {
            List<ViewQMS_TR_EXQ_DOWNTIME> listResult = new List<ViewQMS_TR_EXQ_DOWNTIME>();
            int qty = 0;

            try
            {

                string queryString = "SELECT * FROM QMS_TR_EXQ_DOWNTIME  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND TAG_DATE >= @START_DATE "
                                + " AND TAG_DATE <= @END_DATE ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID); 
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();


                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_TR_EXQ_DOWNTIME temp = new ViewQMS_TR_EXQ_DOWNTIME();

                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]); 
                        temp.TAG_DATE = Convert.ToDateTime(reader["TAG_DATE"]);
                        temp.TAG_EXA_ID = Convert.ToInt64(reader["TAG_EXA_ID"]);

                        temp.TAG_VALUE = reader["TAG_VALUE"].ToString();
                        temp.TAG_CONVERT = reader["TAG_CONVERT"].ToString();
                        temp.TAG_GCERROR = reader["TAG_GCERROR"].ToString();
                        temp.STATUS_1 = Convert.ToByte(reader["STATUS_1"]);
                        temp.STATUS_2 = Convert.ToByte(reader["STATUS_2"]);

                        listResult.Add(temp);
                    }
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return listResult;
        }

        public List<ViewQMS_TR_EXQ_REDUCE_FEED> GetSQLAllQMS_TR_EXQ_REDUCE_FEEDBySearch(ExaProductSearchModel search)
        {
            List<ViewQMS_TR_EXQ_REDUCE_FEED> listResult = new List<ViewQMS_TR_EXQ_REDUCE_FEED>();
            int qty = 0;

            try
            {

                string queryString = "SELECT * FROM QMS_TR_EXQ_REDUCE_FEED  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND TAG_DATE >= @START_DATE "
                                + " AND TAG_DATE <= @END_DATE ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();


                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_TR_EXQ_REDUCE_FEED temp = new ViewQMS_TR_EXQ_REDUCE_FEED();

                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.TAG_DATE = Convert.ToDateTime(reader["TAG_DATE"]);
                        temp.TAG_EXA_ID = Convert.ToInt64(reader["TAG_EXA_ID"]);

                        temp.TAG_VALUE = reader["TAG_VALUE"].ToString();
                        temp.TAG_CONVERT = reader["TAG_CONVERT"].ToString();
                        temp.TAG_GCERROR = reader["TAG_GCERROR"].ToString();
                        temp.STATUS_1 = Convert.ToByte(reader["STATUS_1"]);
                        temp.STATUS_2 = Convert.ToByte(reader["STATUS_2"]);

                        listResult.Add(temp);
                    }
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return listResult;
        }



        public List<long> getListQMS_TR_EXQ_PRODUCTId(List<ViewQMS_TR_EXQ_PRODUCT> listData)
        {
            List<long> listResult = new List<long>();
            bool bFound = false;

            try { 
                foreach (ViewQMS_TR_EXQ_PRODUCT dt in listData)
                {
                    bFound = false;
                    for (int i = 0; i < listResult.ToArray().Length; i++)
                    {
                        if (dt.TAG_EXA_ID == listResult[i])
                        {
                            bFound = true;
                        }
                    }

                    if (false == bFound)
                    {
                        listResult.Add(dt.TAG_EXA_ID);
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return listResult;
        }

        public List<long> getControlIdFromExaList (List<ViewQMS_MA_EXQ_TAG> listData)
        {
            List<long> listResult = new List<long>();
            bool bFound;
            try
            {
                foreach (ViewQMS_MA_EXQ_TAG dt in listData)
                {
                    if (dt.CONTROL_ID > 0)
                    {
                        bFound = false;
                        for (int i = 0; i < listResult.ToArray().Length; i++)
                        {
                            if (dt.CONTROL_ID == listResult[i])
                            {
                                bFound = true;
                            }
                        }

                        if (false == bFound && dt.CONTROL_ID > 0)
                        {
                            listResult.Add(dt.CONTROL_ID);
                        }
                    } 
                }

                foreach (ViewQMS_MA_EXQ_TAG dt in listData)
                {
                    if (dt.CHANGE_CONTROL_ID > 0)
                    {
                        bFound = false;
                        for (int i = 0; i < listResult.ToArray().Length; i++)
                        {
                            if (dt.CHANGE_CONTROL_ID == listResult[i])
                            {
                                bFound = true;
                            }
                        }

                        if (false == bFound && dt.CHANGE_CONTROL_ID > 0)
                        {
                            listResult.Add(dt.CHANGE_CONTROL_ID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return listResult;
        }

        public List<long> getSpecIdFromExaList(List<ViewQMS_MA_EXQ_TAG> listData)
        {
            List<long> listResult = new List<long>();
            bool bFound;
            try
            {
                foreach (ViewQMS_MA_EXQ_TAG dt in listData)
                {
                    if (dt.SPEC_ID > 0)
                    {
                        bFound = false;
                        for (int i = 0; i < listResult.ToArray().Length; i++)
                        {
                            if (dt.SPEC_ID == listResult[i])
                            {
                                bFound = true;
                            }
                        }

                        if (false == bFound && dt.SPEC_ID > 0)
                        {
                            listResult.Add(dt.SPEC_ID);
                        }
                    }
                }

                foreach (ViewQMS_MA_EXQ_TAG dt in listData)
                {
                    if (dt.CHANGE_SPEC_ID > 0)
                    {
                        bFound = false;
                        for (int i = 0; i < listResult.ToArray().Length; i++)
                        {
                            if (dt.CHANGE_SPEC_ID == listResult[i])
                            {
                                bFound = true;
                            }
                        }

                        if (false == bFound && dt.CHANGE_SPEC_ID > 0)
                        {
                            listResult.Add(dt.CHANGE_SPEC_ID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return listResult;
        }

        public List<ViewQMS_MA_EXQ_TAG> getListEXQTagByListId(long[] ids)
        {
            List<ViewQMS_MA_EXQ_TAG> listResult = new List<ViewQMS_MA_EXQ_TAG>();

            try
            {
                string queryInData = "";
                if (ids.Length > 0)
                {
                    for (int i = 0; i < ids.Length; i++)
                    {
                        if (i == 0) //first
                        {
                            queryInData += " ( " + ids[i].ToString();
                        }
                        else
                        {
                            queryInData += " , " + ids[i].ToString();
                        }
                    }

                    queryInData += " )";

                    string queryString = "SELECT * FROM QMS_MA_EXQ_TAG  "
                                    + " WHERE  ID IN " + queryInData;


                    SqlConnection connection = getConnection();
                    SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                    connection.Open();

                    SqlDataReader reader = SQLcommand.ExecuteReader();
                    try
                    {
                        listResult = convertReaderToListViewQMS_MA_EXQ_TAG(reader);
                    }
                    catch (Exception ex)
                    {
                        msgError = ex.Message;
                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                    connection.Close();
                }
                 
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_MA_UNIT> getSQLListUnit()
        {
            List<ViewQMS_MA_UNIT> listResult = new List<ViewQMS_MA_UNIT>(); 

            try
            {
                ViewQMS_MA_UNIT tempResult;
                string queryString = "SELECT * FROM QMS_MA_UNIT  ";  
                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        tempResult = new ViewQMS_MA_UNIT();
                        tempResult.ID = Convert.ToInt64(reader["ID"]);
                        tempResult.NAME = reader["NAME"].ToString(); 

                        listResult.Add(tempResult);
                    }
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
             
            return listResult;
        }

        public List<ViewQMS_MA_CONTROL_COLUMN> getSQLListQMS_MA_CONTROL_COLUMN()
        {
            List<ViewQMS_MA_CONTROL_COLUMN> listResult = new List<ViewQMS_MA_CONTROL_COLUMN>();

            try
            {
                ViewQMS_MA_CONTROL_COLUMN tempResult;
                string queryString = "SELECT * FROM QMS_MA_CONTROL_COLUMN  ";
                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        tempResult = new ViewQMS_MA_CONTROL_COLUMN();
                        tempResult.ID = Convert.ToInt64(reader["ID"]);
                        tempResult.ITEM = reader["ITEM"].ToString();
                        tempResult.CONTROL_ID = Convert.ToInt64(reader["CONTROL_ID"]);
                        tempResult.UNIT_ID = Convert.ToInt64(reader["UNIT_ID"]);

                        listResult.Add(tempResult);
                    }
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_MA_CONTROL_DATA> getQMS_MA_CONTROL_DATAListByListIds(long[] ids)
        {
            List<ViewQMS_MA_CONTROL_DATA> listResult = new List<ViewQMS_MA_CONTROL_DATA>();
            //List<ViewQMS_MA_CONTROL_COLUMN> listColumn;// = QMS_MA_CONTROL_COLUMN.GetAll(db);
            //List<ViewQMS_MA_UNIT> listUnit;//= QMS_MA_UNIT.GetAll(db);

            try
            {
                string queryInData = "";
                if (ids.Length > 0)
                {

                    for (int i = 0; i < ids.Length; i++)
                    {
                        if (i == 0) //first
                        {
                            queryInData += " ( " + ids[i].ToString();
                        }
                        else
                        {
                            queryInData += " , " + ids[i].ToString();
                        }
                    }

                    queryInData += " )";

                    string queryString = "SELECT  cd.ID, cd.CONTROL_ROW_ID, cd.CONTROL_COLUMN_ID, cd.CONTROL_ID,  "
                                    + " cd.MAX_FLAG, cd.MAX_VALUE, cd.MIN_FLAG, cd.MIN_VALUE, "
                                    + " cd.CONC_FLAG, cd.CONC_VALUE, cd.LOADING_FLAG, cd.LOADING_VALUE, "
                                    + " cc.ITEM, u.NAME, cd.MAX_NO_CAL, cd.MIN_NO_CAL, cd.CONC_NO_CAL, cd.LOADING_NO_CAL "
                                    + " FROM QMS_MA_CONTROL_DATA cd   "
                                    + " LEFT JOIN QMS_MA_CONTROL_COLUMN cc  ON  cd.CONTROL_COLUMN_ID = cc.ID "
                                    + " LEFT JOIN QMS_MA_UNIT u ON cc.UNIT_ID = u.ID " 
                                    + " WHERE  cd.ID IN " + queryInData;


                    SqlConnection connection = getConnection();
                    SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                    connection.Open();

                    SqlDataReader reader = SQLcommand.ExecuteReader();
                    try
                    {
                        listResult = convertReaderToListViewQMS_MA_CONTROL_DATA(reader);
                    }
                    catch (Exception ex)
                    {
                        msgError = ex.Message;
                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }  
        #endregion

        #region AUTO CALCULATE Off control

        public string CheckGCErrorRepeatValueEx(string tagConvert, string tagGCError, short ABNORMAL_MAX_REPEAT)
        {
            string tempExaTagValue = "";
            string currentExaTagValue = "";
            long countGCError = 0;
            bool bInitial = true;
            string[] TAG_CONVERT = tagConvert.Split(',');
            string[] TAG_GCERROR = tagGCError.Split(',');

            List<long> tempGCError = new List<long>();
            try
            {
                if (ABNORMAL_MAX_REPEAT > 0) //check repeat
                {
                    for (int i = 0; i < TAG_CONVERT.Length; i++)
                    {
                        currentExaTagValue = TAG_CONVERT[i];

                        if (currentExaTagValue == tempExaTagValue)
                        {
                            //tempGCError.Add(listData[i].ID); //ใช้ id ไม่ได้ เพราะ ตอนนี้ id เป็น 0 
                            tempGCError.Add(i);

                            if (i != 0 && currentExaTagValue == TAG_CONVERT[i - 1] && bInitial) //check initial
                            {
                                tempGCError.Add(i - 1);
                                bInitial = false;
                            }
                        }
                        else //ค่าไม่เหมือนเดิมแล้ว
                        {
                            if (null != tempGCError && tempGCError.ToArray().Length > ABNORMAL_MAX_REPEAT) //ค่าที่เหมือนเดิมมีค่า มากกว่า  ค่าที่กำหนด
                            {
                                foreach (int dataID in tempGCError)
                                { // update ทุกค่าให้เป็น gc Error
                                    TAG_GCERROR[dataID] = ((int)CAL_OFF_STATUS.GC_ERROR_FREEZ).ToString();
                                }
                            }
                            
                            //reset
                            bInitial = true;
                            tempGCError = new List<long>();
                            tempExaTagValue = currentExaTagValue; //เปลี่ยนใช้ ค่าใหม่
                        }
                        countGCError++;
                    }

                    if (null != tempGCError && tempGCError.ToArray().Length > ABNORMAL_MAX_REPEAT) //ค่าที่เหมือนเดิมมีค่า มากกว่า  ค่าที่กำหนด
                    {
                        foreach (int dataID in tempGCError)
                        { // update ทุกค่าให้เป็น gc Error
                            TAG_GCERROR[dataID] = ((int)CAL_OFF_STATUS.GC_ERROR_FREEZ).ToString();
                        }
                    }

                    tagGCError = convertStringArrayToString(TAG_GCERROR); 
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return tagGCError;
        }

        public string CheckSQLReduceFeed(string tagConvert, string tagGCError, decimal checkValue)
        {
            Decimal valueTag = 0;
            try
            {
                string[] TAG_CONVERT = tagConvert.Split(',');
                string[] TAG_GCERROR = tagGCError.Split(',');

                //step1 . ดึงค่าที่สนใจ
                if (TAG_CONVERT.Length > 0)
                {
                    for (int i = 0; i < TAG_CONVERT.Length; i++)
                    {
                        #region CheckReduceFeed
                        try
                        {
                            if (TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.GC_ERROR).ToString() || TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.GC_ERROR_FLOWRATE).ToString())
                            {
                                valueTag = getConvertTextToDecimal(TAG_CONVERT[i]);//Convert.ToDecimal(TAG_CONVERT[i]);

                                if (valueTag <= checkValue)
                                {
                                    TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.FEED_REDUCE_ERR).ToString();
                                }
                            }
                        }
                        catch
                        {

                        }
                        #endregion

                    }
                    tagGCError = convertStringArrayToString(TAG_GCERROR); 
                }

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return tagGCError;
        }

        public string CheckSQLDowntime(string tagConvert, string tagGCError, decimal checkValue)
        {
            Decimal valueTag = 0;
            try
            {
                string[] TAG_CONVERT = tagConvert.Split(',');
                string[] TAG_GCERROR = tagGCError.Split(',');

                //step1 . ดึงค่าที่สนใจ
                if (TAG_CONVERT.Length > 0)
                {
                    for (int i = 0; i < TAG_CONVERT.Length; i++)
                    {
                        #region Check Downtime
                        try
                        {
                            if (TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.GC_ERROR).ToString() || TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.GC_ERROR_FLOWRATE).ToString())
                            {
                                valueTag = getConvertTextToDecimal(TAG_CONVERT[i]);//Convert.ToDecimal(TAG_CONVERT[i]);

                                if (valueTag <= checkValue)
                                {
                                    TAG_GCERROR[i] = ((byte)CAL_OFF_STATUS.DOWNTIME_ERR).ToString();
                                }
                            }
                        }
                        catch
                        {

                        }
                        #endregion

                    }
                    tagGCError = convertStringArrayToString(TAG_GCERROR); 
                }

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return tagGCError;
        }

        public List<ViewQMS_MA_EXQ_ACCUM_TAG> getQMS_MA_EXQ_ACCUM_TAGListByEXA_TAG_ID(long EXA_TAG_ID)
        {
            //DataTable result = new DataTable();
            List<ViewQMS_MA_EXQ_ACCUM_TAG> listResult = new List<ViewQMS_MA_EXQ_ACCUM_TAG>();
            try
            {
                string queryString = "SELECT * FROM QMS_MA_EXQ_ACCUM_TAG WHERE DELETE_FLAG = 0 AND  EXA_TAG_ID = " + EXA_TAG_ID;
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        ViewQMS_MA_EXQ_ACCUM_TAG temp = new ViewQMS_MA_EXQ_ACCUM_TAG();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.EXA_TAG_ID = Convert.ToInt64(reader["EXA_TAG_ID"]);
                        temp.EXA_TAG_VALUE = Convert.ToInt64(reader["EXA_TAG_VALUE"]);
                        temp.CONVERT_VALUE = getConvertTextToDecimal(reader["CONVERT_VALUE"].ToString()); //Convert.ToDecimal(reader["CONVERT_VALUE"]); 
                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        private string getAccumConvert(string offsetValue ,string tagValue, decimal convertValue)
        {
            string bResult = "0";

            try
            {
                decimal OffsetValue = getConvertTextToDecimal(offsetValue); //Convert.ToDecimal(offsetValue);
                decimal TagValue = getConvertTextToDecimal(tagValue); //Convert.ToDecimal(tagValue);
                decimal ConvertValue = convertValue; //Convert.ToDecimal(convertValue);

                bResult = (OffsetValue + (TagValue * ConvertValue)).ToString();

            }
            catch (Exception ex)
            {

            }

            return bResult;
        }

        private string getSumDowntime(string offsetValue, string tagValue )
        {
            string bResult = offsetValue;

            try
            {
                decimal OffsetValue = getConvertTextToDecimal(offsetValue); //Convert.ToDecimal(offsetValue);
                decimal TagValue = getConvertTextToDecimal(tagValue); // Convert.ToDecimal(tagValue); 

                bResult = (OffsetValue + (TagValue)).ToString();

            }
            catch (Exception ex)
            {

            }

            return bResult;
        }

        public string GetAccumTagValueEx(List<CreateQMS_TR_EXQ_PRODUCT> listProductData, ViewQMS_MA_EXQ_TAG currentTag)
        {
            string tagConvertData = ""; 
            long detailExaId = 0; 

            List<ViewQMS_MA_EXQ_ACCUM_TAG> listExqAccum  = new List<ViewQMS_MA_EXQ_ACCUM_TAG>();
            List<string> TAG_CONVERT = new List<string>();  
            List<long> tempGCError = new List<long>();
            try
            { 
                //step 1. get tag detail 
                listExqAccum = getQMS_MA_EXQ_ACCUM_TAGListByEXA_TAG_ID(currentTag.ID);
                if (listExqAccum.ToArray().Length > 0)
                {
                    //step get sumvalue;
                    foreach (ViewQMS_MA_EXQ_ACCUM_TAG dtAccum in listExqAccum)
                    {
                        //ต้องการ ตัวคูณ กับ id ของ detail  
                        detailExaId = dtAccum.EXA_TAG_VALUE; //ตัวที่เป็น รายละเอียด

                        try
                        {
                            CreateQMS_TR_EXQ_PRODUCT accumData = getCreateQMS_TR_EXQ_PRODUCTById(listProductData, detailExaId);
                            string[] TAG_ACCUM_CONVERT = accumData.TAG_CONVERT.Split(','); 
                             
                            for (int i = 0; i < TAG_ACCUM_CONVERT.Length; i++)
                            {
                                if (TAG_CONVERT.ToArray().Length == i) // แสกงว่ายังไม่มีค่า
                                {
                                    string tempValue = getAccumConvert("0", TAG_ACCUM_CONVERT[i], dtAccum.CONVERT_VALUE);
                                    TAG_CONVERT.Add(tempValue);
                                }
                                else
                                {
                                    TAG_CONVERT[i] = getAccumConvert(TAG_CONVERT[i], TAG_ACCUM_CONVERT[i], dtAccum.CONVERT_VALUE);
                                } 
                            }
                        }
                        catch (Exception ex)
                        {
                            msgError = ex.Message;
                        }

                    }

                    tagConvertData = convertStringArrayToString(TAG_CONVERT.ToArray());
                }

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return tagConvertData;
        }

        public ViewQMS_MA_EXQ_TAG getViewQMS_TR_EXQ_PRODUCTFromListById(List<ViewQMS_MA_EXQ_TAG> listEXQ_TAG, long id)
        {
            ViewQMS_MA_EXQ_TAG result = new ViewQMS_MA_EXQ_TAG();
            try
            {
                foreach (ViewQMS_MA_EXQ_TAG dtEXQTag in listEXQ_TAG)
                {
                    if (dtEXQTag.ID == id)
                    {
                        result = dtEXQTag; break;
                    }
                }
            }
            catch
            {

            }

            return result;
        }

        public CreateQMS_TR_EXQ_PRODUCT getCreateQMS_TR_EXQ_PRODUCTById(List<CreateQMS_TR_EXQ_PRODUCT> listProduct, long id)
        {
            CreateQMS_TR_EXQ_PRODUCT result = new CreateQMS_TR_EXQ_PRODUCT();
            try
            {
                foreach (CreateQMS_TR_EXQ_PRODUCT dtProduct in listProduct)
                {
                    if (dtProduct.TAG_EXA_ID == id)
                    {
                        result = dtProduct; break;
                    }
                }
            }
            catch
            {

            }

            return result;
        }

        public string getControlUnitNameFromListByControlId(List<ViewQMS_MA_CONTROL_DATA> listControl, long CONTROL_ID)
        {
            string result = "";
            try
            {
                foreach (ViewQMS_MA_CONTROL_DATA dtControl in listControl)
                {
                    if (dtControl.ID == CONTROL_ID)
                    {
                        result = dtControl.CONTROL_COLUMN_NAME + "(" + dtControl.UNIT_NAME + ")"; break;
                    }
                }
            }
            catch
            {

            }

            return result;
        }

        public string getControlName(List<ViewQMS_MA_CONTROL_DATA> listControl, long controlId)
        {
            string bResult = "";

            try
            {
                foreach (ViewQMS_MA_CONTROL_DATA dt in listControl)
                {
                    if (dt.ID == controlId)
                    {
                        bResult = dt.CONTROL_COLUMN_NAME + " " + dt.UNIT_NAME;
                        break;
                    }
                } 
            }
            catch
            {

            }

            return bResult;
        }

        public ViewQMS_MA_CONTROL_DATA getControlDataById(List<ViewQMS_MA_CONTROL_DATA> listControl, long controlId)
        {
            ViewQMS_MA_CONTROL_DATA dtResult = new ViewQMS_MA_CONTROL_DATA();

            try
            {
                foreach (ViewQMS_MA_CONTROL_DATA dt in listControl)
                {
                    if (dt.ID == controlId)
                    {
                        dtResult = dt;
                        break;
                    }
                }
            }
            catch
            {

            }

            return dtResult;
        }

         
        public string setSQLInitialOffControlSpecName(string tagGCError, long controlId, List<ViewQMS_MA_CONTROL_DATA> listControl)
        {
            string szResult = "";

            try
            {
                string[] TAG_GCERROR = tagGCError.Split(',');
                string tempControlName = "";
                string controlName = "";
                bool bInitial = true;

                if (controlId > 0)
                {
                    controlName = getControlName(listControl, controlId); 
                }
                for (int i = 0; i < TAG_GCERROR.Length; i++)
                {
                    tempControlName = "";
                    if (controlName != ""
                        && TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString()
                        && TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_AND_SPEC_HIGH).ToString()
                    )
                    {
                        tempControlName = controlName;// ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString() + ";" + controlName;
                    }

                    if (controlName != ""
                        && TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_LOW).ToString()
                        && TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTORL_AND_SPEC_LOW).ToString()
                    )
                    {
                        tempControlName = controlName;// ((byte)CAL_OFF_STATUS.OFF_CONTROL_LOW).ToString() + ";" + controlName;
                    }

                    if (bInitial == true)
                    {
                        szResult = tempControlName;// TAG_GCERROR[i];
                        bInitial = false;
                    }
                    else
                    {
                        szResult = szResult + "," + tempControlName;// TAG_GCERROR[i];
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return szResult;
        }

        public CAL_OFF_CONTROL getSQLInitialCalOffControl(ViewQMS_TR_EXQ_PRODUCT dtProductData, ViewQMS_MA_EXQ_TAG tempEXQTAG, List<ViewQMS_MA_CONTROL_DATA> listControl)
        {
            CAL_OFF_CONTROL tempProduct = new CAL_OFF_CONTROL();

            try
            {
                string[] TAG_GCERROR = dtProductData.TAG_GCERROR.Split(',');

                for (int i = 0; i < TAG_GCERROR.Length; i++)
                {
                    TAG_GCERROR[i] = "0"; //reset to initaial
                }

                tempProduct.PLANT_ID = dtProductData.PLANT_ID;
                tempProduct.PRODUCT_ID = dtProductData.PRODUCT_ID;
                tempProduct.TAG_DATE = dtProductData.TAG_DATE;
                tempProduct.TAG_GCERROR = convertStringArrayToString(TAG_GCERROR);

                //ใส่ปริมาณ

                if (tempEXQTAG.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_SUM)
                {
                    tempProduct.TAG_VOLUME = dtProductData.TAG_CONVERT;
                }

                //ใส่ค่า off control
                tempProduct.OFF_CONTROL_NAME = setSQLInitialOffControlSpecName(tempProduct.TAG_GCERROR, tempEXQTAG.CONTROL_ID, listControl);
                //ใส่ค่า off Spec
                tempProduct.OFF_SPEC_NAME = setSQLInitialOffControlSpecName(tempProduct.TAG_GCERROR, tempEXQTAG.SPEC_ID, listControl);
            }
            catch
            {

            }

            return tempProduct;
        }

        public string convertStringArrayToString(string[] dataArray)
        {
            string result = "";

            try
            {
                for (int i = 0; i < dataArray.Length; i++)
                {
                    if (i == 0)
                    {
                        result = dataArray[i];
                    }
                    else
                    {
                        result += "," + dataArray[i];
                    }
                }

            }
            catch
            {

            }

            return result;
        }

        public CAL_OFF_CONTROL getCAL_OFF_CONTROLFromListById(List<CAL_OFF_CONTROL> listCalData, DateTime TAG_DATE)
        {
            CAL_OFF_CONTROL result = new CAL_OFF_CONTROL();
            try
            {
                foreach (CAL_OFF_CONTROL dtCal in listCalData)
                {
                    if (dtCal.TAG_DATE == TAG_DATE)
                    {
                        result = dtCal; break;
                    }
                }
            }
            catch
            {

            }

            return result;
        }

        public List<CAL_OFF_CONTROL> getSQLCalListData(List<ViewQMS_TR_EXQ_PRODUCT> listProductData, List<ViewQMS_MA_EXQ_TAG> listEXQ_TAG, List<ViewQMS_MA_CONTROL_DATA> listControl, List<ViewQMS_MA_CONTROL_DATA> listSpec)
        {
            List<CAL_OFF_CONTROL> listCalData = new List<CAL_OFF_CONTROL>();
            ViewQMS_MA_EXQ_TAG tempEXQTAG = new ViewQMS_MA_EXQ_TAG();
            bool bInitial = true;

            try
            {
                foreach (ViewQMS_TR_EXQ_PRODUCT dtProductData in listProductData)
                {
                    tempEXQTAG = getViewQMS_TR_EXQ_PRODUCTFromListById(listEXQ_TAG, dtProductData.TAG_EXA_ID);

                    //ตรวจสอบ เฉพาะค่าที่มีการควบคุม และ ค่าการไหล
                    if (tempEXQTAG.SPEC_ID > 0 || tempEXQTAG.CONTROL_ID > 0
                        || tempEXQTAG.TAG_TARGET_CHECK == 1 //เพิ่มส่วนที่เป็น target
                        || (tempEXQTAG.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV || tempEXQTAG.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_SUM))
                    {
                        CAL_OFF_CONTROL productData = new CAL_OFF_CONTROL();

                        if (bInitial == true) //(listCalData.Count() == 0) //1.1 initial
                        {
                            CAL_OFF_CONTROL tempProduct = getSQLInitialCalOffControl(dtProductData, tempEXQTAG, listControl);
                            productData = tempProduct;
                            listCalData.Add(tempProduct);
                            bInitial = false;
                        }
                        else
                        {
                            //1.2 ตรวจสอบว่ามีในระบบ หรือยัง
                            productData = getCAL_OFF_CONTROLFromListById(listCalData, dtProductData.TAG_DATE);

                            if (null == productData.TAG_GCERROR || "" == productData.TAG_GCERROR) //2.0 ไม่รู้จัก model null 
                            {
                                CAL_OFF_CONTROL tempProduct = getSQLInitialCalOffControl(dtProductData, tempEXQTAG, listControl);
                                productData = tempProduct;
                                listCalData.Add(tempProduct);
                            }
                        }

                        string[] TAG_GCERROR = dtProductData.TAG_GCERROR.Split(',');
                        string[] TAG_GCERROR2 = productData.TAG_GCERROR.Split(',');
                        string[] OFF_CONTROL_NAME = productData.OFF_CONTROL_NAME.Split(',');
                        string[] OFF_SPEC_NAME = productData.OFF_SPEC_NAME.Split(',');

                        for (int i = 0; i < TAG_GCERROR.Length; i++)
                        {
                            if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR).ToString()
                                || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR_FLOWRATE).ToString()
                                || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR_FREEZ).ToString()
                                || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.TAG_TARGET_ERR).ToString()
                                ) //reset off control to GC_ERROR
                            {
                                TAG_GCERROR2[i] = TAG_GCERROR[i];
                            }
                            else
                            {
                                #region offcontrol
                                string tempControlName = "";
                                string controlName = "";
                                if (tempEXQTAG.CONTROL_ID > 0)
                                {
                                    controlName = getControlUnitNameFromListByControlId(listControl, tempEXQTAG.CONTROL_ID);
                                     
                                    if (controlName != "")
                                    {
                                        ViewQMS_MA_CONTROL_DATA temControlData = getControlDataById(listControl, tempEXQTAG.CONTROL_ID);

                                        //check high value
                                        if (controlName != ""
                                                && (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString()
                                                || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_AND_SPEC_HIGH).ToString())
                                                && (temControlData.MAX_NO_CAL == false)
                                            )
                                        {
                                            tempControlName = controlName;// ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString() + ";" + controlName;
                                            TAG_GCERROR2[i] = TAG_GCERROR[i];
                                        }

                                        //check low value
                                        if (controlName != ""
                                            && (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_LOW).ToString()
                                            || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTORL_AND_SPEC_LOW).ToString())
                                            && (temControlData.MIN_NO_CAL == false)
                                        )
                                        {
                                            tempControlName = controlName;//((byte)CAL_OFF_STATUS.OFF_CONTROL_LOW).ToString() + ";" + controlName;
                                            TAG_GCERROR2[i] = TAG_GCERROR[i];
                                        }

                                        if (tempControlName != "")
                                        {
                                            if (OFF_CONTROL_NAME[i] == "")
                                            {
                                                OFF_CONTROL_NAME[i] = tempControlName;// TAG_GCERROR[i];
                                                TAG_GCERROR2[i] = TAG_GCERROR[i];
                                            }
                                            else
                                            {
                                                OFF_CONTROL_NAME[i] = OFF_CONTROL_NAME[i] + " : " + tempControlName;// TAG_GCERROR[i];
                                                TAG_GCERROR2[i] = TAG_GCERROR[i];
                                            }
                                        }

                                    }
                                }
                                 
                                #endregion
                                #region offSpec
                                tempControlName = "";
                                controlName = "";

                                if (tempEXQTAG.SPEC_ID > 0)
                                {
                                    controlName = getControlUnitNameFromListByControlId(listSpec, tempEXQTAG.SPEC_ID);


                                    if (controlName != "")
                                    {
                                        ViewQMS_MA_CONTROL_DATA temSpecData = getControlDataById(listSpec, tempEXQTAG.SPEC_ID);

                                        if (controlName != ""
                                                  && (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_SPEC_HIGH).ToString()
                                                  || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_AND_SPEC_HIGH).ToString())
                                                   && (temSpecData.MAX_NO_CAL == false)
                                           )
                                        {
                                            tempControlName = controlName;// ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString() + ";" + controlName;
                                            TAG_GCERROR2[i] = TAG_GCERROR[i];
                                        }

                                        if (controlName != ""
                                            && (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_SPEC_LOW).ToString()
                                            || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTORL_AND_SPEC_LOW).ToString())
                                            && (temSpecData.MIN_NO_CAL == false)
                                        )
                                        {
                                            tempControlName = controlName;//((byte)CAL_OFF_STATUS.OFF_CONTROL_LOW).ToString() + ";" + controlName;
                                            TAG_GCERROR2[i] = TAG_GCERROR[i];
                                        }
                                        if (tempControlName != "")
                                        {
                                            if (OFF_SPEC_NAME[i] == "")
                                            {
                                                OFF_SPEC_NAME[i] = tempControlName;// TAG_GCERROR[i];
                                                TAG_GCERROR2[i] = TAG_GCERROR[i];
                                            }
                                            else
                                            {
                                                OFF_SPEC_NAME[i] = OFF_SPEC_NAME[i] + " : " + tempControlName;// TAG_GCERROR[i];
                                                TAG_GCERROR2[i] = TAG_GCERROR[i];
                                            }
                                        }
                                    }

                                } 
                                #endregion
                            }
                        }

                        #region assign value
                        productData.TAG_GCERROR = convertStringArrayToString(TAG_GCERROR2);
                        productData.OFF_CONTROL_NAME = convertStringArrayToString(OFF_CONTROL_NAME); 
                        productData.OFF_SPEC_NAME = convertStringArrayToString(OFF_SPEC_NAME); 

                        //Assign data to cal 
                        for (int pos = 0; pos < listCalData.ToArray().Length; pos++)
                        {
                            if (listCalData[pos].TAG_DATE == productData.TAG_DATE)
                            {
                                listCalData[pos].TAG_GCERROR = productData.TAG_GCERROR;
                                listCalData[pos].OFF_CONTROL_NAME = productData.OFF_CONTROL_NAME;
                                listCalData[pos].OFF_SPEC_NAME = productData.OFF_SPEC_NAME;

                                //ใส่ปริมาณ 
                                if (tempEXQTAG.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV)
                                {
                                    listCalData[pos].TAG_VOLUME = dtProductData.TAG_CONVERT;
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listCalData;
        }

        public List<ViewQMS_TR_OFF_CONTROL_CAL> getSQLQMS_TR_OFF_CONTROL_CALExpand(long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_OFF_CONTROL_CAL> listResult = new List<ViewQMS_TR_OFF_CONTROL_CAL>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "SELECT * FROM QMS_TR_OFF_CONTROL_CAL  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND PRODUCT_ID = @PRODUCT_ID "
                                + " AND DOC_STATUS = @DOC_STATUS "
                                + " AND ( START_DATE = @START_DATE "
                                + " OR END_DATE = @END_DATE )";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                SQLcommand.Parameters.AddWithValue("@START_DATE", endDate); //start == end
                SQLcommand.Parameters.AddWithValue("@END_DATE", startDate); //end == start

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_OFF_CONTROL_CAL temp = new ViewQMS_TR_OFF_CONTROL_CAL();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public long SaveQMS_TR_OFF_CONTROL_CAL(CreateQMS_TR_OFF_CONTROL_CAL model)
        {
            long result = 0;

            #region check overlap

            

            #endregion


            #region SaveQMS_TR_OFF_CONTROL
            string idsData = "";
            //จุดเชื่อม ที่จุดเริ่มต้น -2 นาที และ จุดสิ้นสุด +2 นาที
            List<ViewQMS_TR_OFF_CONTROL_CAL> listExpand = getSQLQMS_TR_OFF_CONTROL_CALExpand(model.ID, model.PLANT_ID, model.PRODUCT_ID, model.DOC_STATUS, model.START_DATE.AddMinutes(-2), model.END_DATE.AddMinutes(2));
            //มีได้ ทังหมด 4 กรณี
            //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย 
            //case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
            //case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
            //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด

            if (null != listExpand && listExpand.Count > 0)
            {
                //check case 2 && 3
                if (listExpand.Count == 1)
                { 
                    if (listExpand[0].START_DATE == model.END_DATE.AddMinutes(2))//case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
                    {
                        model.END_DATE = listExpand[0].END_DATE;
                    }
                    else//case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
                    {
                        model.START_DATE = listExpand[0].START_DATE;
                    }

                    if (model.ID > 0) //ลบก่อน ที่จะอัพไปเป็นอีกตัว
                    {
                        idsData = "( '" + model.ID + "' )";
                        DeleteQMS_TR_OFF_CONTROL_CALbyList(idsData, model.DOC_STATUS);
                    }

                    model.ID = listExpand[0].ID;
                    result = UpdateQMS_TR_OFF_CONTROL_CAL(model);
                }
                else
                {
                    //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด
                    DateTime startExpand = new DateTime(); //listExpand.Min(m => m.START_DATE);
                    DateTime endExpand = new DateTime();//listExpand.Max(m => m.END_DATE);
                    
                    for (int i = 0; i < listExpand.Count; i++)
                    {
                        startExpand = listExpand[i].START_DATE;
                        endExpand = listExpand[i].END_DATE;

                        model.START_DATE = (model.START_DATE >= startExpand) ? startExpand : model.START_DATE;
                        model.END_DATE = (model.END_DATE >= endExpand) ? model.END_DATE : endExpand;

                        if (i == 0)
                        {
                            idsData = "( '" + listExpand[i].ID + "' ";
                        }
                        else
                        {
                            idsData += " , '" + listExpand[i].ID + "' ";
                        }
                    }

                    idsData += " )";

                    //ลบ by id 
                    if (0 == DeleteQMS_TR_OFF_CONTROL_CALbyList(idsData, model.DOC_STATUS))
                    {   //เพิ่มตัวใหม่เข้าระบบ
                        model.ID = 0;
                        result = AddQMS_TR_OFF_CONTROL_CAL(model);
                    }

                }
            }
            else
            {
                //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย   
                if (model.ID > 0)
                {
                    result = UpdateQMS_TR_OFF_CONTROL_CAL(model);
                }
                else
                {
                    result = AddQMS_TR_OFF_CONTROL_CAL(model);
                }
            }

            #endregion
            return result;
        }


        public long AddQMS_TR_OFF_CONTROL_CAL(  CreateQMS_TR_OFF_CONTROL_CAL model)
        {
            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;
                           
                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;
                     
                conn.Open(); 
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("INSERT INTO QMS_TR_OFF_CONTROL_CAL   "
                                        + " ( PLANT_ID, PRODUCT_ID, OFF_CONTROL_TYPE, DOC_STATUS,  "
                                        + " START_DATE, END_DATE, VOLUME, CONTROL_VALUE, ROOT_CAUSE_ID, OFF_CONTROL_DESC, "
                                        + " CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE )"
                                        + " VALUES(@PLANT_ID, @PRODUCT_ID, @OFF_CONTROL_TYPE, @DOC_STATUS, "
                                        + " @START_DATE, @END_DATE, @VOLUME, @CONTROL_VALUE, @ROOT_CAUSE_ID, @OFF_CONTROL_DESC,"
                                        + " @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE ) ", conn);
                 

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@OFF_CONTROL_TYPE", model.OFF_CONTROL_TYPE);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS); 

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);
                SQLcommand.Parameters.AddWithValue("@CONTROL_VALUE", model.CONTROL_VALUE); 
                SQLcommand.Parameters.AddWithValue("@ROOT_CAUSE_ID", model.ROOT_CAUSE_ID);
                SQLcommand.Parameters.AddWithValue("@OFF_CONTROL_DESC", model.OFF_CONTROL_DESC);

                SQLcommand.Parameters.AddWithValue("@CREATE_USER",  _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp); 

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long UpdateQMS_TR_OFF_CONTROL_CAL(CreateQMS_TR_OFF_CONTROL_CAL model)
        {
            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("UPDATE QMS_TR_OFF_CONTROL_CAL   "
                                        + " SET PLANT_ID = @PLANT_ID , PRODUCT_ID =  @PRODUCT_ID, OFF_CONTROL_TYPE = @OFF_CONTROL_TYPE, DOC_STATUS = @DOC_STATUS,  "
                                        + " START_DATE = @START_DATE, END_DATE = @END_DATE, VOLUME = @VOLUME, CONTROL_VALUE = @CONTROL_VALUE, ROOT_CAUSE_ID = @ROOT_CAUSE_ID,  "
                                        + " OFF_CONTROL_DESC = @OFF_CONTROL_DESC, UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@OFF_CONTROL_TYPE", model.OFF_CONTROL_TYPE);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);
                SQLcommand.Parameters.AddWithValue("@CONTROL_VALUE", model.CONTROL_VALUE);
                SQLcommand.Parameters.AddWithValue("@ROOT_CAUSE_ID", model.ROOT_CAUSE_ID);
                SQLcommand.Parameters.AddWithValue("@OFF_CONTROL_DESC", model.OFF_CONTROL_DESC);

                //SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                //SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public List<ViewQMS_TR_TURN_AROUND> getAllTurnAroundByPlant(long modelID, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_TURN_AROUND> listResult = new List<ViewQMS_TR_TURN_AROUND>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

                queryString = "SELECT * FROM QMS_TR_TURN_AROUND  "
                                   + " WHERE PLANT_ID = @PLANT_ID   "
                                   + " AND DOC_STATUS = @DOC_STATUS "
                                   + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                   + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                   + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                   + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);

                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_TURN_AROUND temp = new ViewQMS_TR_TURN_AROUND();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        temp.CORRECT_DATA_TYPE = Convert.ToInt64(reader["CORRECT_DATA_TYPE"]);
                        temp.CAUSE_DESC = reader["CAUSE_DESC"].ToString();

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public VALIDATE_EXCEPTION_OVERLLAP IsOverlapMasterCorrectDataEx(long modelId, long plant, long product, byte docStatus, DateTime startDate, DateTime endDate, int statCheck)
        {
            VALIDATE_EXCEPTION_OVERLLAP modelResult = new VALIDATE_EXCEPTION_OVERLLAP();


            modelResult.IS_OVERLAP = false;
            modelResult.MSG_ERROR = "";

            try
            {
                ////Step 1 Turn Around
                List<ViewQMS_TR_TURN_AROUND> listTurnAround = getAllTurnAroundByPlant(modelId, plant, docStatus, startDate, endDate);
                if (null != listTurnAround && listTurnAround.Count > 0 && (listTurnAround.Count > 1 || listTurnAround[0].ID != modelId))
                {
                    modelResult.IS_OVERLAP = true;
                    //for (int i = 0; i < listTurnAround.Count; i++)
                    //{
                    //    modelResult.statDate = (null != modelResult.statDate && modelResult.statDate <= listTurnAround[i].START_DATE) ? modelResult.statDate : listTurnAround[i].START_DATE;
                    //    modelResult.endDate = (null != modelResult.endDate && modelResult.endDate >= listTurnAround[i].END_DATE) ? modelResult.endDate : listTurnAround[i].END_DATE;
                    //}
                    //modelResult.MSG_ERROR = ;
                }
                else if (statCheck >= (byte)CORRECT_DATA_TYPE.DOWN_TIME)
                {
                    //Step 2 Downtime
                    List<ViewQMS_TR_DOWNTIME> listDowntime = getAllDowntimeByPlant(modelId, plant, docStatus, startDate, endDate);
                    if (null != listDowntime && listDowntime.Count > 0 && (listDowntime.Count > 1 || listDowntime[0].ID != modelId))
                    {
                        modelResult.IS_OVERLAP = true;
                        modelResult.MSG_ERROR = "";
                        //for (int i = 0; i < listDowntime.Count; i++)
                        //{
                        //    modelResult.statDate = (null != modelResult.statDate && modelResult.statDate <= listDowntime[i].START_DATE) ? modelResult.statDate : listDowntime[i].START_DATE;
                        //    modelResult.endDate = (null != modelResult.endDate && modelResult.endDate >= listDowntime[i].END_DATE) ? modelResult.endDate : listDowntime[i].END_DATE;
                        //}
                    }
                    else if (statCheck == (byte)CORRECT_DATA_TYPE.CHANGE_GRADE) //check self 
                    {
                        //Step 3 Change grade
                        List<ViewQMS_TR_CHANGE_GRADE> listChangeGrade = getAllChangeGradeByPlant(modelId, plant, product, docStatus, startDate, endDate);
                        if (null != listChangeGrade && listChangeGrade.Count  > 0 && (listChangeGrade.Count  > 1 || listChangeGrade[0].ID != modelId))
                        {
                            modelResult.IS_OVERLAP = true;
                            modelResult.MSG_ERROR = "";
                            //for (int i = 0; i < listChangeGrade.Count; i++)
                            //{
                            //    modelResult.statDate = (null != modelResult.statDate && modelResult.statDate <= listChangeGrade[i].START_DATE) ? modelResult.statDate : listChangeGrade[i].START_DATE;
                            //    modelResult.endDate = (null != modelResult.endDate && modelResult.endDate >= listChangeGrade[i].END_DATE) ? modelResult.endDate : listChangeGrade[i].END_DATE;
                            //}
                        }
                    }
                    else if (statCheck == (byte)CORRECT_DATA_TYPE.CROSS_VOLUME) //check self
                    {
                        //Step 4 Cross volumn
                        List<ViewQMS_TR_CROSS_VOLUME> listCrossVolume = getAllCrossVolumeByPlant(modelId, plant, product, docStatus, startDate, endDate);
                        if (null != listCrossVolume && listCrossVolume.Count > 0 && (listCrossVolume.Count > 1 || listCrossVolume[0].ID != modelId))
                        {
                            modelResult.IS_OVERLAP = true;
                            modelResult.MSG_ERROR = "";
                            //for (int i = 0; i < listCrossVolume.Count; i++)
                            //{
                            //    modelResult.statDate = (null != modelResult.statDate && modelResult.statDate <= listCrossVolume[i].START_DATE) ? modelResult.statDate : listCrossVolume[i].START_DATE;
                            //    modelResult.endDate = (null != modelResult.endDate && modelResult.endDate >= listCrossVolume[i].END_DATE) ? modelResult.endDate : listCrossVolume[i].END_DATE;
                            //}
                        }
                    }
                    else if (statCheck >= (byte)CORRECT_DATA_TYPE.REDUCE_FEED)
                    {
                        //Step 6 Reduce Feed
                        List<ViewQMS_TR_REDUCE_FEED> listReduceFeed = getAllReduceFeedByPlantProduct(modelId, plant, docStatus, startDate, endDate);
                        if (null != listReduceFeed && listReduceFeed.Count > 0 && (listReduceFeed.Count > 1 || listReduceFeed[0].ID != modelId))
                        {
                            modelResult.IS_OVERLAP = true;
                            modelResult.MSG_ERROR = "";
                            //for (int i = 0; i < listReduceFeed.Count; i++)
                            //{
                            //    modelResult.statDate = (null != modelResult.statDate && modelResult.statDate <= listReduceFeed[i].START_DATE) ? modelResult.statDate : listReduceFeed[i].START_DATE;
                            //    modelResult.endDate = (null != modelResult.endDate && modelResult.endDate >= listReduceFeed[i].END_DATE) ? modelResult.endDate : listReduceFeed[i].END_DATE;
                            //}
                        }
                    }
                    else if (statCheck >= (byte)CORRECT_DATA_TYPE.ABNORMAL) //check self
                    {
                        //Step 5 GC Error
                        List<ViewQMS_TR_ABNORMAL> listAbnormal = getAllAbnormalByPlantProduct(modelId, plant, product, docStatus, startDate, endDate);
                        if (null != listAbnormal && listAbnormal.Count > 0)
                        {
                            if ((statCheck == (byte)CORRECT_DATA_TYPE.ABNORMAL && (listAbnormal.Count > 1 || listAbnormal[0].ID != modelId))
                                || (statCheck > (byte)CORRECT_DATA_TYPE.ABNORMAL && listAbnormal.Count > 0))
                            {
                                modelResult.IS_OVERLAP = true;
                                modelResult.MSG_ERROR = "";
                                //for (int i = 0; i < listAbnormal.Count; i++)
                                //{
                                //    modelResult.statDate = (null != modelResult.statDate && modelResult.statDate <= listAbnormal[i].START_DATE) ? modelResult.statDate : listAbnormal[i].START_DATE;
                                //    modelResult.endDate = (null != modelResult.endDate && modelResult.endDate >= listAbnormal[i].END_DATE) ? modelResult.endDate : listAbnormal[i].END_DATE;
                                //}
                            }

                        }
                    }
                 
                }
            }
            catch (Exception ex)
            {
                modelResult.IS_OVERLAP = true;
                modelResult.MSG_ERROR = ex.Message;
            }

            return modelResult;
        }

        public void SQLCalOffControlToDB(List<CAL_OFF_CONTROL> listCalData, byte OFFType)
        {
            try
            {
                CreateQMS_TR_OFF_CONTROL_CAL createOffControl = new CreateQMS_TR_OFF_CONTROL_CAL();
                //1. list data by serach
                VALIDATE_EXCEPTION_OVERLLAP checkOverlap = new VALIDATE_EXCEPTION_OVERLLAP();
                int start = -1;
                int end = -1;
                bool bfound = false;
                decimal volume = 0;
                string CONTROL_VALUE = "";
                int indexPos = 0;

                foreach (CAL_OFF_CONTROL dtCAL_OFF_CONTROL in listCalData)
                {
                    string[] TAG_GCERROR = dtCAL_OFF_CONTROL.TAG_GCERROR.Split(',');
                    string[] TAG_VOLUME = dtCAL_OFF_CONTROL.TAG_VOLUME.Split(',');
                    string[] OFF_CONTROL_NAME = dtCAL_OFF_CONTROL.OFF_CONTROL_NAME.Split(',');

                    //initial value;
                    bfound = false; start = -1; end = -1; volume = 0; CONTROL_VALUE = ""; indexPos = 0;

                    for (int i = 0; i < TAG_GCERROR.Length; i++)
                    {
                        if (TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.NORMAL).ToString())//แสดงว่ามี error
                        {
                            //ตรวจสอบว่าเป็น off spec || off control


                            if (    (OFFType == (byte)OFF_TYPE.CONTROL 
                                    &&  (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_HIGH).ToString()
                                    || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_LOW).ToString() ))
                                    || (OFFType == (byte)OFF_TYPE.SPEC 
                                    && (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_SPEC_HIGH).ToString()
                                    || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_SPEC_LOW).ToString())) 
                                    || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTROL_AND_SPEC_HIGH).ToString()
                                    || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.OFF_CONTORL_AND_SPEC_LOW).ToString()
                                )
                            {  
                                bfound = true;
                               
                                
                                if (start == -1)
                                {
                                    start = i;
                                    end = i;
                                    volume += getConvertTextToDecimal(TAG_VOLUME[i]); //Convert.ToDecimal(TAG_VOLUME[i]); 
                                }
                                else if ((end + 1) == i)
                                {
                                    end = i;
                                    volume += getConvertTextToDecimal(TAG_VOLUME[i]); //Convert.ToDecimal(TAG_VOLUME[i]);
                                }
                                else
                                {  
                                    createOffControl = new CreateQMS_TR_OFF_CONTROL_CAL();
                                    createOffControl.ID = 0;
                                    createOffControl.PLANT_ID = dtCAL_OFF_CONTROL.PLANT_ID;
                                    createOffControl.PRODUCT_ID = dtCAL_OFF_CONTROL.PRODUCT_ID;
                                    createOffControl.START_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                                    createOffControl.END_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                                    createOffControl.OFF_CONTROL_TYPE = OFFType;// (byte)OFF_TYPE.CONTROL;
                                    createOffControl.CONTROL_VALUE = CONTROL_VALUE;
                                    createOffControl.VOLUME = volume;
                                    createOffControl.OFF_CONTROL_DESC = _msgDesc;
                                    createOffControl.DOC_STATUS = (byte)DOC_STATUS.PENDING;
                                    createOffControl.ROOT_CAUSE_ID = (byte)OFF_CAL_ROOTCAUSE.DEFAULT; //set default;

                                    checkOverlap = IsOverlapMasterCorrectDataEx(createOffControl.ID, createOffControl.PLANT_ID, createOffControl.PRODUCT_ID, createOffControl.DOC_STATUS, createOffControl.START_DATE, createOffControl.END_DATE, (byte)CORRECT_DATA_TYPE.EXCEPTION);
                                    if (checkOverlap.IS_OVERLAP == false)
                                    {
                                        SaveQMS_TR_OFF_CONTROL_CAL(createOffControl);
                                    }

                                    //Save Initial save เสมอ
                                    createOffControl.ID = 0;
                                    createOffControl.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                                    SaveQMS_TR_OFF_CONTROL_CAL(createOffControl);

                                    start = i;
                                    end = i;
                                    volume = getConvertTextToDecimal(TAG_VOLUME[i]); //Convert.ToDecimal(TAG_VOLUME[i]); 
                                    bfound = false;
                                    CONTROL_VALUE = "";
                                }

                                if (CONTROL_VALUE == "")
                                {
                                    CONTROL_VALUE = OFF_CONTROL_NAME[i];

                                }
                                else
                                {
                                    //indexPos = CONTROL_VALUE.IndexOf(OFF_CONTROL_NAME[i]);// OFF_CONTROL_NAME[i].IndexOf(CONTROL_VALUE);
                                    //if (indexPos == -1)
                                    //{
                                    //    CONTROL_VALUE = CONTROL_VALUE + " " + OFF_CONTROL_NAME[i];
                                    //}

                                    //indexPos = OFF_CONTROL_NAME[i].IndexOf(CONTROL_VALUE);

                                    string[] controlName = OFF_CONTROL_NAME[i].Split(':');

                                    for (int k = 0; k < controlName.Length; k++)
                                    {
                                        indexPos = CONTROL_VALUE.IndexOf(controlName[k]);
                                        if (indexPos == -1)
                                        {
                                            //CONTROL_VALUE = CONTROL_VALUE + " " + OFF_CONTROL_NAME[i];
                                            CONTROL_VALUE = CONTROL_VALUE + " " + controlName[k];
                                        }
                                    }
                                }

                            }
                        }
                    }

                    if (bfound == true)
                    {
                        createOffControl = new CreateQMS_TR_OFF_CONTROL_CAL();
                        createOffControl.ID = 0;
                        createOffControl.PLANT_ID = dtCAL_OFF_CONTROL.PLANT_ID;
                        createOffControl.PRODUCT_ID = dtCAL_OFF_CONTROL.PRODUCT_ID;
                        createOffControl.START_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                        createOffControl.END_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                        createOffControl.OFF_CONTROL_TYPE = OFFType;// (byte)OFF_TYPE.CONTROL;
                        createOffControl.CONTROL_VALUE = CONTROL_VALUE;
                        createOffControl.VOLUME = volume;
                        createOffControl.OFF_CONTROL_DESC = _msgDesc;
                        createOffControl.DOC_STATUS = (byte)DOC_STATUS.PENDING;
                        createOffControl.ROOT_CAUSE_ID = (byte)OFF_CAL_ROOTCAUSE.DEFAULT; //set default;
                         
                        checkOverlap = IsOverlapMasterCorrectDataEx(createOffControl.ID, createOffControl.PLANT_ID, createOffControl.PRODUCT_ID, createOffControl.DOC_STATUS, createOffControl.START_DATE, createOffControl.END_DATE, (byte)CORRECT_DATA_TYPE.EXCEPTION);
                        if (checkOverlap.IS_OVERLAP == false)
                        {
                            SaveQMS_TR_OFF_CONTROL_CAL(createOffControl);
                        }

                        //Save Initial save เสมอ
                        createOffControl.ID = 0;
                        createOffControl.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                        SaveQMS_TR_OFF_CONTROL_CAL(createOffControl);

                        volume = 0;
                        bfound = false;
                    }

                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
        }

        public List<ViewQMS_TR_ABNORMAL> getSQLQMS_TR_ABNORMALExpand(long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_ABNORMAL> listResult = new List<ViewQMS_TR_ABNORMAL>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "SELECT * FROM QMS_TR_ABNORMAL  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND PRODUCT_ID = @PRODUCT_ID "
                                + " AND DOC_STATUS = @DOC_STATUS "
                                + " AND ( START_DATE = @START_DATE "
                                + " OR END_DATE = @END_DATE )";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                SQLcommand.Parameters.AddWithValue("@START_DATE", endDate); //start == end
                SQLcommand.Parameters.AddWithValue("@END_DATE", startDate); //end == start

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_ABNORMAL temp = new ViewQMS_TR_ABNORMAL();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }
        public long SaveQMS_TR_ABNORMAL(CreateQMS_TR_ABNORMAL model)
        {
            long result = 0;
            string idsData = "";
            //จุดเชื่อม ที่จุดเริ่มต้น -2 นาที และ จุดสิ้นสุด +2 นาที
            List<ViewQMS_TR_ABNORMAL> listExpand = getSQLQMS_TR_ABNORMALExpand(model.ID, model.PLANT_ID, model.PRODUCT_ID, model.DOC_STATUS, model.START_DATE.AddMinutes(-2), model.END_DATE.AddMinutes(2));
            //มีได้ ทังหมด 4 กรณี
            //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย 
            //case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
            //case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
            //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด

            if (null != listExpand && listExpand.Count > 0)
            {
                //check case 2 && 3
                if (listExpand.Count == 1)
                {
                    
                    if (listExpand[0].START_DATE == model.END_DATE.AddMinutes(2))//case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
                    {
                        model.END_DATE = listExpand[0].END_DATE;
                    }
                    else//case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
                    {
                        model.START_DATE = listExpand[0].START_DATE;
                    }

                    if (model.ID > 0) //ลบก่อน ที่จะอัพไปเป็นอีกตัว
                    {
                        idsData = "( '" + model.ID + "' )";
                        DeleteQMS_TR_ABNORMALbyList(idsData, model.DOC_STATUS);
                    }

                    model.ID = listExpand[0].ID;
                    result = UpdateQMS_TR_ABNORMAL(model);
                }
                else
                {
                    //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด
                    DateTime startExpand = new DateTime(); //listExpand.Min(m => m.START_DATE);
                    DateTime endExpand = new DateTime();//listExpand.Max(m => m.END_DATE);
                     

                    for (int i = 0; i < listExpand.Count; i++)
                    {
                        startExpand = listExpand[i].START_DATE;
                        endExpand = listExpand[i].END_DATE;

                        model.START_DATE = (model.START_DATE >= startExpand) ? startExpand : model.START_DATE;
                        model.END_DATE = (model.END_DATE >= endExpand) ? model.END_DATE : endExpand;

                        if (i == 0)
                        {
                            idsData = "( '" + listExpand[i].ID + "' ";
                        }
                        else
                        {
                            idsData += " , '" + listExpand[i].ID + "' ";
                        }
                    }

                    idsData += " )";

                    //ลบ by id 
                    if (0 == DeleteQMS_TR_ABNORMALbyList(idsData, model.DOC_STATUS))
                    {   //เพิ่มตัวใหม่เข้าระบบ
                        model.ID = 0;
                        result = AddQMS_TR_ABNORMAL(model);
                    }

                }
            }
            else
            {
                //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย   
                if (model.ID > 0)
                {
                    result = UpdateQMS_TR_ABNORMAL(model);
                }
                else
                {
                    result = AddQMS_TR_ABNORMAL(model);
                }
            }
            return result;
        }

        public long UpdateQMS_TR_ABNORMAL(CreateQMS_TR_ABNORMAL model)
        {
            long result = 0;

            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("UPDATE QMS_TR_ABNORMAL   "
                                        + " SET PLANT_ID = @PLANT_ID , PRODUCT_ID =  @PRODUCT_ID, DOC_STATUS = @DOC_STATUS, "
                                        + " START_DATE = @START_DATE, END_DATE = @END_DATE, VOLUME = @VOLUME, CORRECT_DATA_TYPE = @CORRECT_DATA_TYPE , "
                                        + " CAUSE_DESC = @CAUSE_DESC, UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC); 

                //SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                //SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long AddQMS_TR_ABNORMAL(CreateQMS_TR_ABNORMAL model)
        {  

            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("INSERT INTO QMS_TR_ABNORMAL   "
                                        + " ( PLANT_ID, PRODUCT_ID, DOC_STATUS,  "
                                        + " START_DATE, END_DATE, VOLUME, CORRECT_DATA_TYPE, CAUSE_DESC,  "
                                        + " CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE )"
                                        + " VALUES(@PLANT_ID, @PRODUCT_ID, @DOC_STATUS, "
                                        + " @START_DATE, @END_DATE, @VOLUME,  @CORRECT_DATA_TYPE, @CAUSE_DESC,"
                                        + " @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE ) ", conn);


                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID); 
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC); 

                SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public CAL_DOWNTIME_DATA getCAL_DOWNTIME_DATAFromListByDate(List<CAL_DOWNTIME_DATA> listCalData, DateTime TAG_DATE)
        {
            CAL_DOWNTIME_DATA result = null;// new CAL_DOWNTIME_DATA();
            try
            {
                if (listCalData.ToArray().Length > 0)
                {
                    foreach (CAL_DOWNTIME_DATA dtCal in listCalData)
                    {
                        if (dtCal.TAG_DATE == TAG_DATE)
                        {
                            result = dtCal; break;
                        }
                    }
                }
            }
            catch
            {

            }

            return result;
        }

        public List<CAL_DOWNTIME_DATA> getCAL_DOWNTIME_DATA(List<ViewQMS_TR_EXQ_DOWNTIME> listData)
        {
            List<CAL_DOWNTIME_DATA> listDowntimeData = new List<CAL_DOWNTIME_DATA>();
            CAL_DOWNTIME_DATA productData = new CAL_DOWNTIME_DATA();
            try
            {
                if (listData.ToArray().Length > 0)
                {

                    foreach (ViewQMS_TR_EXQ_DOWNTIME dtData in listData)
                    {
                        productData = getCAL_DOWNTIME_DATAFromListByDate(listDowntimeData, dtData.TAG_DATE);

                        if (null == productData || "" == productData.TAG_GCERROR) //2.0 ไม่รู้จัก model null 
                        {
                            productData = new CAL_DOWNTIME_DATA();
                            productData.PLANT_ID = dtData.PLANT_ID; 
                            productData.TAG_DATE = dtData.TAG_DATE;
                            productData.TAG_GCERROR = dtData.TAG_GCERROR;
                            productData.TAG_VALUE = dtData.TAG_VALUE;
                        }

                        string[] TAG_VALUE = dtData.TAG_VALUE.Split(',');
                        string[] TAG_VALUE2 = productData.TAG_VALUE.Split(',');

                        if (TAG_VALUE2.Length >= TAG_VALUE.Length)
                        {
                            for (int i = 0; i < TAG_VALUE2.Length; i++)
                            {
                                TAG_VALUE2[i] = getSumDowntime(TAG_VALUE2[i], TAG_VALUE[i]);
                            }
                            productData.TAG_VALUE = convertStringArrayToString(TAG_VALUE2);
                        }
                        else
                        {
                            for (int i = 0; i < TAG_VALUE.Length; i++)
                            {
                                TAG_VALUE[i] = getSumDowntime(TAG_VALUE[i], TAG_VALUE2[i]);
                            }
                            productData.TAG_VALUE = convertStringArrayToString(TAG_VALUE);
                        }

                        listDowntimeData.Add(productData);
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listDowntimeData; 
        }

        public decimal getDowntimeOverLimit(List<ViewQMS_MA_DOWNTIME> listDowntime, DateTime activeDate)
        { 
            decimal limited = 0;

            try
            {
                if (listDowntime.ToArray().Length == 1)//มีข้อมูล เพียงชุดเดียว
                {
                    limited = listDowntime[0].LIMIT_VALUE;
                }
                else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                {
                    foreach (ViewQMS_MA_DOWNTIME dt in listDowntime)
                    {
                        limited = dt.LIMIT_VALUE;
                        if (activeDate >= dt.ACTIVE_DATE)
                        {
                            break;//set อย่างน้อย 1 ตัว
                        }
                    }
                }
            }
            catch
            {

            }

            return limited;
        }

        private bool isOverLimit(string TAG_VALUE, decimal limitedCheck)
        {
            bool bResult = false;
            try
            {
                bResult = (limitedCheck > getConvertTextToDecimal(TAG_VALUE)); //Convert.ToDecimal(TAG_VALUE));
            }
            catch
            {
                bResult = true;
            }

            return bResult;
        }

        public List<ViewQMS_TR_DOWNTIME> getSQLQMS_TR_DOWNTIMEExpand(long modelID, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_DOWNTIME> listResult = new List<ViewQMS_TR_DOWNTIME>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "SELECT * FROM QMS_TR_DOWNTIME  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND DOC_STATUS = @DOC_STATUS "
                                + " AND (START_DATE = @START_DATE "
                                + " OR END_DATE = @END_DATE) ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                SQLcommand.Parameters.AddWithValue("@START_DATE", endDate); //start == end
                SQLcommand.Parameters.AddWithValue("@END_DATE", startDate); //end == start

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader(); 

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_DOWNTIME temp = new ViewQMS_TR_DOWNTIME();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public long SaveQMS_TR_CHANGE_GRADE(CreateQMS_TR_CHANGE_GRADE model)
        {
            long result = 0;

            model.START_DATE = model.START_DATE.AddSeconds(-model.START_DATE.Second);
            model.END_DATE = model.END_DATE.AddSeconds(-model.END_DATE.Second);

            if (model.ID > 0)
            {
                result = UpdateQMS_TR_CHANGE_GRADE(model);
            }
            else
            {
                result = AddQMS_TR_CHANGE_GRADE( model);
            }

            return result;
        }

        public long UpdateQMS_TR_CHANGE_GRADE(CreateQMS_TR_CHANGE_GRADE model)
        {
            long result = 0;

            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("UPDATE QMS_TR_CHANGE_GRADE   "
                                        + " SET PLANT_ID = @PLANT_ID , PRODUCT_ID = @PRODUCT_ID,  DOC_STATUS = @DOC_STATUS, "
                                        + " GRADE_FROM = @GRADE_FROM, GRADE_TO = @GRADE_TO, VOLUME = @VOLUME , "
                                        + " START_DATE = @START_DATE, END_DATE = @END_DATE, CORRECT_DATA_TYPE = @CORRECT_DATA_TYPE , "
                                        + " CAUSE_DESC = @CAUSE_DESC, UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@GRADE_FROM", model.GRADE_FROM);
                SQLcommand.Parameters.AddWithValue("@GRADE_TO", model.GRADE_TO);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);
                 

                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long AddQMS_TR_CHANGE_GRADE(CreateQMS_TR_CHANGE_GRADE model)
        {

            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("INSERT INTO QMS_TR_CHANGE_GRADE   "
                                        + " ( PLANT_ID, PLANT_ID, DOC_STATUS,  "
                                        + " GRADE_FROM, GRADE_TO,  VOLUME, "
                                        + " START_DATE, END_DATE,  CORRECT_DATA_TYPE, CAUSE_DESC,  "
                                        + " CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE )"
                                        + " VALUES(@PLANT_ID,   @DOC_STATUS, "
                                        + " @START_DATE, @END_DATE,   @CORRECT_DATA_TYPE, @CAUSE_DESC,"
                                        + " @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE ) ", conn);


                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@GRADE_FROM", model.GRADE_FROM);
                SQLcommand.Parameters.AddWithValue("@GRADE_TO", model.GRADE_TO);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);

                SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long SaveQMS_TR_CROSS_VOLUME(CreateQMS_TR_CROSS_VOLUME model)
        {
            long result = 0;

            model.START_DATE = model.START_DATE.AddSeconds(-model.START_DATE.Second);
            model.END_DATE = model.END_DATE.AddSeconds(-model.END_DATE.Second);

            if (model.ID > 0)
            {
                result = UpdateQMS_TR_CROSS_VOLUME(model);
            }
            else
            {
                result = AddQMS_TR_CROSS_VOLUME(model);
            }

            return result;
        }

        public long UpdateQMS_TR_CROSS_VOLUME(CreateQMS_TR_CROSS_VOLUME model)
        {
            long result = 0;

            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("UPDATE QMS_TR_CROSS_VOLUME   "
                                        + " SET PLANT_ID = @PLANT_ID , PRODUCT_ID = @PRODUCT_ID,  DOC_STATUS = @DOC_STATUS, "
                                        + " PRODUCT_TO = @PRODUCT_TO , VOLUME = @VOLUME , "
                                        + " START_DATE = @START_DATE, END_DATE = @END_DATE, CORRECT_DATA_TYPE = @CORRECT_DATA_TYPE , "
                                        + " CAUSE_DESC = @CAUSE_DESC, UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);
                 
                SQLcommand.Parameters.AddWithValue("@PRODUCT_TO", model.PRODUCT_TO);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);


                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long AddQMS_TR_CROSS_VOLUME(CreateQMS_TR_CROSS_VOLUME model)
        {

            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("INSERT INTO QMS_TR_CROSS_VOLUME   "
                                        + " ( PLANT_ID, PLANT_ID, DOC_STATUS,  "
                                        + " PRODUCT_TO,  VOLUME, "
                                        + " START_DATE, END_DATE,  CORRECT_DATA_TYPE, CAUSE_DESC,  "
                                        + " CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE )"
                                        + " VALUES(@PLANT_ID,   @DOC_STATUS, "
                                        + " @START_DATE, @END_DATE,   @CORRECT_DATA_TYPE, @CAUSE_DESC,"
                                        + " @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE ) ", conn);


                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);
                 
                SQLcommand.Parameters.AddWithValue("@PRODUCT_TO", model.PRODUCT_TO);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);

                SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long SaveQMS_TR_EXCEPTION(CreateQMS_TR_EXCEPTION model)
        {
            long result = 0;

            model.START_DATE = model.START_DATE.AddSeconds(-model.START_DATE.Second);
            model.END_DATE = model.END_DATE.AddSeconds(-model.END_DATE.Second);

            if (model.ID > 0)
            {
                result = UpdateQMS_TR_EXCEPTION(model);
            }
            else
            {
                result = AddQMS_TR_EXCEPTION(model);
            }

            return result;
        }

        public long UpdateQMS_TR_EXCEPTION(CreateQMS_TR_EXCEPTION model)
        {
            long result = 0;

            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("UPDATE QMS_TR_EXCEPTION   "
                                        + " SET PLANT_ID = @PLANT_ID , DOC_STATUS = @DOC_STATUS, "
                                        + " PRODUCT_TO = @PRODUCT_TO , VOLUME = @VOLUME , "
                                        + " START_DATE = @START_DATE, END_DATE = @END_DATE, CORRECT_DATA_TYPE = @CORRECT_DATA_TYPE , "
                                        + " CAUSE_DESC = @CAUSE_DESC, UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS); 
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);


                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long AddQMS_TR_EXCEPTION(CreateQMS_TR_EXCEPTION model)
        {

            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("INSERT INTO QMS_TR_EXCEPTION   "
                                        + " ( PLANT_ID, PLANT_ID, DOC_STATUS,  "
                                        + " VOLUME, "
                                        + " START_DATE, END_DATE,  CORRECT_DATA_TYPE, CAUSE_DESC,  "
                                        + " CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE )"
                                        + " VALUES(@PLANT_ID,   @DOC_STATUS, "
                                        + " @START_DATE, @END_DATE,   @CORRECT_DATA_TYPE, @CAUSE_DESC,"
                                        + " @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE ) ", conn);


                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);
                SQLcommand.Parameters.AddWithValue("@VOLUME", model.VOLUME);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);

                SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long SaveQMS_TR_DOWNTIME(CreateQMS_TR_DOWNTIME model)
        {
            long result = 0;
            string idsData = "";
            //จุดเชื่อม ที่จุดเริ่มต้น -2 นาที และ จุดสิ้นสุด +2 นาที
            List<ViewQMS_TR_DOWNTIME> listExpand = getSQLQMS_TR_DOWNTIMEExpand(model.ID, model.PLANT_ID, model.DOC_STATUS, model.START_DATE.AddMinutes(-2), model.END_DATE.AddMinutes(2));
            //มีได้ ทังหมด 4 กรณี
            //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย 
            //case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
            //case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
            //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด

            if (null != listExpand && listExpand.Count > 0)
            {
                //check case 2 && 3
                if (listExpand.Count == 1)
                { 
                    if (listExpand[0].START_DATE == model.END_DATE.AddMinutes(2))//case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
                    {
                        model.END_DATE = listExpand[0].END_DATE;
                    }
                    else//case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
                    {
                        model.START_DATE = listExpand[0].START_DATE;
                    }
                    

                    if (model.ID > 0) //ลบก่อน ที่จะอัพไปเป็นอีกตัว
                    {
                        idsData = "( '" + model.ID + "' )";
                        DeleteQMS_TR_DOWNTIMEbyList(idsData, model.DOC_STATUS);
                    }

                    model.ID = listExpand[0].ID;
                    result = UpdateQMS_TR_DOWNTIME(model);
                }
                else
                {
                    //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด
                    DateTime startExpand = new DateTime(); //listExpand.Min(m => m.START_DATE);
                    DateTime endExpand = new DateTime();//listExpand.Max(m => m.END_DATE);
                    

                    for (int i = 0; i < listExpand.Count; i++)
                    {
                        startExpand = listExpand[i].START_DATE;
                        endExpand = listExpand[i].END_DATE;

                        model.START_DATE = (model.START_DATE >= startExpand) ? startExpand : model.START_DATE;
                        model.END_DATE = (model.END_DATE >= endExpand) ? model.END_DATE : endExpand;

                        if( i == 0){
                            idsData = "( '" + listExpand[i].ID + "' ";
                        }else{
                            idsData += " , '" + listExpand[i].ID + "' ";
                        }
                    }

                    idsData += " )";

                    //ลบ by id 
                    if (0 == DeleteQMS_TR_DOWNTIMEbyList(idsData, model.DOC_STATUS))
                    {   //เพิ่มตัวใหม่เข้าระบบ
                        model.ID = 0;
                        result = AddQMS_TR_DOWNTIME(model);
                    }

                }
            }
            else
            {
                //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย   
                if (model.ID > 0)
                {
                    result = UpdateQMS_TR_DOWNTIME(model);
                }
                else
                {
                    result = AddQMS_TR_DOWNTIME(model);
                }
            }
            return result;
        }

        public long UpdateQMS_TR_DOWNTIME(CreateQMS_TR_DOWNTIME model)
        {
            long result = 0;

            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("UPDATE QMS_TR_DOWNTIME   "
                                        + " SET PLANT_ID = @PLANT_ID , DOC_STATUS = @DOC_STATUS, "
                                        + " START_DATE = @START_DATE, END_DATE = @END_DATE, CORRECT_DATA_TYPE = @CORRECT_DATA_TYPE , "
                                        + " CAUSE_DESC = @CAUSE_DESC, UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID); 
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE); 
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);

                //SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                //SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long AddQMS_TR_DOWNTIME(CreateQMS_TR_DOWNTIME model)
        {

            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("INSERT INTO QMS_TR_DOWNTIME   "
                                        + " ( PLANT_ID,  DOC_STATUS,  "
                                        + " START_DATE, END_DATE,  CORRECT_DATA_TYPE, CAUSE_DESC,  "
                                        + " CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE )"
                                        + " VALUES(@PLANT_ID,   @DOC_STATUS, "
                                        + " @START_DATE, @END_DATE,   @CORRECT_DATA_TYPE, @CAUSE_DESC,"
                                        + " @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE ) ", conn);


                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID); 
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE); 
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);

                SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public void SQLCalDowntimeToDB(ExaProductSearchModel searchModel)
        {
            try
            {
                //del old inital data
                DeleteIntialQMS_TR_DOWNTIME(searchModel);

                List<CAL_DOWNTIME_DATA> listDowntimeData = new List<CAL_DOWNTIME_DATA>();
                CreateQMS_TR_DOWNTIME createDowntime = new CreateQMS_TR_DOWNTIME();
                //0. get downtime detail 
                List<ViewQMS_MA_DOWNTIME> listDowntime = getAllQMS_MA_DOWNTIMEByPlantId(searchModel.PLANT_ID);

                //1. list data by serach
                List<ViewQMS_TR_EXQ_DOWNTIME> listData = this.GetSQLAllQMS_TR_EXQ_DOWNTIMEBySearch(searchModel);
                listDowntimeData = getCAL_DOWNTIME_DATA(listData);
                int start;
                int end;
                bool bfound = false;
                bool bGCError = false;
                bool bGCFlowError = false;
                string MESSAGE_NAME = "";
                VALIDATE_EXCEPTION_OVERLLAP checkOverlap = new VALIDATE_EXCEPTION_OVERLLAP();

                if (listDowntimeData.ToArray().Length > 0)
                {
                    foreach (CAL_DOWNTIME_DATA dtCalDowntime in listDowntimeData)
                    {
                        bfound = false; start = -1; end = 0; MESSAGE_NAME = ""; bGCError = false; bGCFlowError = false;
                        
                        try
                        {
                            //decimal limitedCheck = getDowntimeOverLimit(listDowntime, dtCalDowntime.TAG_DATE);
                            string[] TAG_VALUE = dtCalDowntime.TAG_VALUE.Split(',');
                            string[] TAG_GCERROR = dtCalDowntime.TAG_GCERROR.Split(',');

                            if ((TAG_GCERROR.Length == 1 && dtCalDowntime.TAG_VALUE == "NULL") || TAG_GCERROR.Length < 719) //cannot read data
                            {
                                bfound = true;
                                start = 0;
                                end = 719;
                                MESSAGE_NAME = _msgExqReadErr;
                            }
                            else
                            {

                                for (int i = 0; i < TAG_VALUE.Length; i++)
                                {
                                    MESSAGE_NAME = "";// _msgDesc;

                                    //if (isOverLimit(TAG_VALUE[i], limitedCheck) || (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR).ToString()))
                                    if (TAG_GCERROR[i] != "0")//แสดงว่ามี error
                                    {
                                        bfound = true;
                                        if (start == -1)
                                        {
                                            start = i;
                                            end = i;
                                        }
                                        else if ((end + 1) == i)
                                        {
                                            end = i;
                                        }
                                        else
                                        {
                                            createDowntime = new CreateQMS_TR_DOWNTIME();
                                            
                                            if (bGCError == true)
                                            {
                                                MESSAGE_NAME = _msgGCERROR;
                                            }

                                            if (bGCFlowError == true)
                                            {
                                                if (MESSAGE_NAME != "")
                                                {
                                                    MESSAGE_NAME += " , " + _msgUnderFlow;
                                                }
                                                else
                                                {
                                                    MESSAGE_NAME = _msgUnderFlow;
                                                }
                                            }

                                            createDowntime.ID = 0;
                                            createDowntime.PLANT_ID = dtCalDowntime.PLANT_ID;
                                            createDowntime.START_DATE = new DateTime(dtCalDowntime.TAG_DATE.Year, dtCalDowntime.TAG_DATE.Month, dtCalDowntime.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                                            createDowntime.END_DATE = new DateTime(dtCalDowntime.TAG_DATE.Year, dtCalDowntime.TAG_DATE.Month, dtCalDowntime.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                                            createDowntime.CAUSE_DESC = MESSAGE_NAME;// _msgDesc; 
                                            createDowntime.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.DOWN_TIME;
                                            createDowntime.DOC_STATUS = (byte)DOC_STATUS.PENDING;


                                            checkOverlap = IsOverlapMasterCorrectDataEx(createDowntime.ID, createDowntime.PLANT_ID, 0, createDowntime.DOC_STATUS, createDowntime.START_DATE, createDowntime.END_DATE, (byte)CORRECT_DATA_TYPE.DOWN_TIME);
                                            if (checkOverlap.IS_OVERLAP == false)
                                            {
                                                SaveQMS_TR_DOWNTIME(createDowntime);
                                                SaveQMS_TR_OFF_CONTROL_CAL_With_DOWNTIME(createDowntime);
                                            }

                                            //Save Initial 
                                            createDowntime.ID = 0;
                                            createDowntime.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                                            SaveQMS_TR_DOWNTIME(createDowntime);
                                            //SaveQMS_TR_OFF_CONTROL_CAL_With_DOWNTIME(createDowntime);

                                            start = i;
                                            end = i;
                                            bfound = false;
                                            MESSAGE_NAME = ""; bGCError = false; bGCFlowError = false;
                                        }


                                        if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR).ToString())
                                        {
                                            bGCError = true;
                                        }
                                        else
                                        {
                                            bGCFlowError = true;
                                        }
                                    }

                                }
                            }

                            if (bfound == true)
                            {
                                createDowntime = new CreateQMS_TR_DOWNTIME();

                                if (bGCError == true)
                                {
                                    MESSAGE_NAME = _msgGCERROR;
                                }

                                if (bGCFlowError == true)
                                {
                                    if (MESSAGE_NAME != "")
                                    {
                                        MESSAGE_NAME += " , " + _msgUnderFlow;
                                    }
                                    else
                                    {
                                        MESSAGE_NAME = _msgUnderFlow;
                                    }
                                }

                                createDowntime.ID = 0;
                                createDowntime.PLANT_ID = dtCalDowntime.PLANT_ID;
                                createDowntime.START_DATE = new DateTime(dtCalDowntime.TAG_DATE.Year, dtCalDowntime.TAG_DATE.Month, dtCalDowntime.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                                createDowntime.END_DATE = new DateTime(dtCalDowntime.TAG_DATE.Year, dtCalDowntime.TAG_DATE.Month, dtCalDowntime.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                                createDowntime.CAUSE_DESC = MESSAGE_NAME;//_msgDesc;
                                createDowntime.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.DOWN_TIME;
                                createDowntime.DOC_STATUS = (byte)DOC_STATUS.PENDING;

                                checkOverlap = IsOverlapMasterCorrectDataEx(createDowntime.ID, createDowntime.PLANT_ID, 0, createDowntime.DOC_STATUS, createDowntime.START_DATE, createDowntime.END_DATE, (byte)CORRECT_DATA_TYPE.DOWN_TIME);
                                if (checkOverlap.IS_OVERLAP == false)
                                {
                                    SaveQMS_TR_DOWNTIME(createDowntime);
                                    SaveQMS_TR_OFF_CONTROL_CAL_With_DOWNTIME(createDowntime);
                                }

                                //Save Initial 
                                createDowntime.ID = 0;
                                createDowntime.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                                SaveQMS_TR_DOWNTIME(createDowntime);
                                //SaveQMS_TR_OFF_CONTROL_CAL_With_DOWNTIME(createDowntime);

                                start = 0;
                                end = 0;
                                bfound = false;
                            }
                        }
                        catch
                        {

                        }


                    } 
                }
                 
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
        }

        public CAL_REDUCE_FEED_DATA getCAL_REDUCE_FEED_DATAFromListByDate(List<CAL_REDUCE_FEED_DATA> listCalData, DateTime TAG_DATE)
        {
            CAL_REDUCE_FEED_DATA result = null;// new CAL_DOWNTIME_DATA();
            try
            {
                if (listCalData.ToArray().Length > 0)
                {
                    foreach (CAL_REDUCE_FEED_DATA dtCal in listCalData)
                    {
                        if (dtCal.TAG_DATE == TAG_DATE)
                        {
                            result = dtCal; break;
                        }
                    }
                }
            }
            catch
            {

            }

            return result;
        }

        public List<CAL_REDUCE_FEED_DATA> getCAL_REDUCE_FEED_DATA(List<ViewQMS_TR_EXQ_REDUCE_FEED> listData)
        {
            List<CAL_REDUCE_FEED_DATA> listReduceFeedData = new List<CAL_REDUCE_FEED_DATA>();
            CAL_REDUCE_FEED_DATA productData = new CAL_REDUCE_FEED_DATA();
            try
            {
                if (listData.ToArray().Length > 0)
                {

                    foreach (ViewQMS_TR_EXQ_REDUCE_FEED dtData in listData)
                    {
                        productData = getCAL_REDUCE_FEED_DATAFromListByDate(listReduceFeedData, dtData.TAG_DATE);

                        if (null == productData || "" == productData.TAG_GCERROR) //2.0 ไม่รู้จัก model null 
                        {
                            productData = new CAL_REDUCE_FEED_DATA();
                            productData.PLANT_ID = dtData.PLANT_ID;
                            productData.TAG_DATE = dtData.TAG_DATE;
                            productData.TAG_GCERROR = dtData.TAG_GCERROR;
                            productData.TAG_VALUE = dtData.TAG_VALUE;

                        }

                        string[] TAG_VALUE = dtData.TAG_VALUE.Split(',');
                        string[] TAG_VALUE2 = productData.TAG_VALUE.Split(',');

                        if (TAG_VALUE2.Length >= TAG_VALUE.Length)
                        {
                            for (int i = 0; i < TAG_VALUE2.Length; i++)
                            {
                                TAG_VALUE2[i] = getSumDowntime(TAG_VALUE2[i], TAG_VALUE[i]);
                            }
                            productData.TAG_VALUE = convertStringArrayToString(TAG_VALUE2);
                        }
                        else
                        {
                            for (int i = 0; i < TAG_VALUE.Length; i++)
                            {
                                TAG_VALUE[i] = getSumDowntime(TAG_VALUE[i], TAG_VALUE2[i]);
                            }
                            productData.TAG_VALUE = convertStringArrayToString(TAG_VALUE);
                        }

                        listReduceFeedData.Add(productData);
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listReduceFeedData;
        }

        public decimal getReduceFeedOverLimit(List<ViewQMS_MA_REDUCE_FEED> listReduceFeed, DateTime activeDate)
        {
            decimal limited = 0;

            try
            {
                if (listReduceFeed.ToArray().Length == 1)//มีข้อมูล เพียงชุดเดียว
                {
                    limited = listReduceFeed[0].LIMIT_VALUE;
                }
                else // มีข้อมูลหลายชุด ต้องเลือกแค่ 1 ชุดเผื่อไปหา tag โดนตรวจสอบ active date
                {
                    foreach (ViewQMS_MA_REDUCE_FEED dt in listReduceFeed)
                    {
                        limited = dt.LIMIT_VALUE;
                        if (activeDate >= dt.ACTIVE_DATE)
                        {
                            break;//set อย่างน้อย 1 ตัว
                        }
                    }
                }
            }
            catch
            {

            }

            return limited;
        }

        public List<ViewQMS_TR_REDUCE_FEED> getSQLQMS_TR_REDUCE_FEEDExpand(long modelID, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_REDUCE_FEED> listResult = new List<ViewQMS_TR_REDUCE_FEED>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "SELECT * FROM QMS_TR_REDUCE_FEED  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND DOC_STATUS = @DOC_STATUS "
                                + " AND (START_DATE = @START_DATE "
                                + " OR END_DATE = @END_DATE) ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                SQLcommand.Parameters.AddWithValue("@START_DATE", endDate); //start == end
                SQLcommand.Parameters.AddWithValue("@END_DATE", startDate); //end == start

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_REDUCE_FEED temp = new ViewQMS_TR_REDUCE_FEED();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public long SaveQMS_TR_REDUCE_FEED(CreateQMS_TR_REDUCE_FEED model)
        {
            long result = 0;
            string idsData = "";
            //จุดเชื่อม ที่จุดเริ่มต้น -2 นาที และ จุดสิ้นสุด +2 นาที
            List<ViewQMS_TR_REDUCE_FEED> listExpand = getSQLQMS_TR_REDUCE_FEEDExpand(model.ID, model.PLANT_ID, model.DOC_STATUS, model.START_DATE.AddMinutes(-2), model.END_DATE.AddMinutes(2));
            //มีได้ ทังหมด 4 กรณี
            //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย 
            //case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
            //case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
            //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด

            if (null != listExpand && listExpand.Count > 0)
            {
                //check case 2 && 3
                if (listExpand.Count == 1)
                {
                   
                    if (listExpand[0].START_DATE == model.END_DATE.AddMinutes(2))//case 2. คาบเกี่ยวจุดเริ่มต้น 1 จุด
                    {
                        model.END_DATE = listExpand[0].END_DATE;
                    }
                    else//case 3. คาบเกี่ยวจุดสิ้นสุด 1 จุด
                    {
                        model.START_DATE = listExpand[0].START_DATE;
                    }

                    if (model.ID > 0) //ลบก่อน ที่จะอัพไปเป็นอีกตัว
                    {
                        idsData = "( '" + model.ID + "' )";
                        DeleteQMS_TR_REDUCE_FEEDbyList(idsData, model.DOC_STATUS);
                    }

                    model.ID = listExpand[0].ID;
                    result = UpdateQMS_TR_REDUCE_FEED(model);
                }
                else
                {
                    //case 4. คาบเกี่ยวจุดเริ่ต้น และ จุดสิ้นสุด
                    DateTime startExpand = new DateTime(); //listExpand.Min(m => m.START_DATE);
                    DateTime endExpand = new DateTime();//listExpand.Max(m => m.END_DATE);
                    

                    for (int i = 0; i < listExpand.Count; i++)
                    {
                        startExpand = listExpand[i].START_DATE;
                        endExpand = listExpand[i].END_DATE;

                        model.START_DATE = (model.START_DATE >= startExpand) ? startExpand : model.START_DATE;
                        model.END_DATE = (model.END_DATE >= endExpand) ? model.END_DATE : endExpand;

                        if (i == 0)
                        {
                            idsData = "( '" + listExpand[i].ID + "' ";
                        }
                        else
                        {
                            idsData += " , '" + listExpand[i].ID + "' ";
                        }
                    }

                    idsData += " )";

                    //ลบ by id 
                    if (0 == DeleteQMS_TR_REDUCE_FEEDbyList(idsData, model.DOC_STATUS))
                    {   //เพิ่มตัวใหม่เข้าระบบ
                        model.ID = 0;
                        result = AddQMS_TR_REDUCE_FEED(model);
                    }

                }
            }
            else
            {
                //case 1. ไม่มี ช่วงเวลา คาบเกี่ยวเลย   
                if (model.ID > 0)
                {
                    result = UpdateQMS_TR_REDUCE_FEED(model);
                }
                else
                {
                    result = AddQMS_TR_REDUCE_FEED(model);
                }
            }

            return result;
        }

        public long UpdateQMS_TR_REDUCE_FEED(CreateQMS_TR_REDUCE_FEED model)
        {
            long result = 0;

            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("UPDATE QMS_TR_REDUCE_FEED   "
                                        + " SET PLANT_ID = @PLANT_ID , PRODUCT_ID = @PRODUCT_ID,  DOC_STATUS = @DOC_STATUS, "
                                        + " START_DATE = @START_DATE, END_DATE = @END_DATE, CORRECT_DATA_TYPE = @CORRECT_DATA_TYPE , "
                                        + " CAUSE_DESC = @CAUSE_DESC, UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);

                //SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                //SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public long AddQMS_TR_REDUCE_FEED(CreateQMS_TR_REDUCE_FEED model)
        {

            long result = 0;
            try
            {
                DateTime timeStamp = DateTime.Now;

                //Step 1. Delete duplicate data...
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                //Step 2. Add Data  
                SQLcommand = new SqlCommand("INSERT INTO QMS_TR_REDUCE_FEED   "
                                        + " ( PLANT_ID, PRODUCT_ID, DOC_STATUS,  "
                                        + " START_DATE, END_DATE,  CORRECT_DATA_TYPE, CAUSE_DESC,  "
                                        + " CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE )"
                                        + " VALUES(@PLANT_ID, @PRODUCT_ID,   @DOC_STATUS, "
                                        + " @START_DATE, @END_DATE,   @CORRECT_DATA_TYPE, @CAUSE_DESC,"
                                        + " @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE ) ", conn);


                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", model.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);

                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@CORRECT_DATA_TYPE", model.CORRECT_DATA_TYPE);
                SQLcommand.Parameters.AddWithValue("@CAUSE_DESC", model.CAUSE_DESC);

                SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        public void SQLCalReduceFeedToDB(ExaProductSearchModel searchModel)
        {
            try
            {
                //del old inital data
                DeleteIntialQMS_TR_REDUCE_FEED(searchModel);

                List<CAL_REDUCE_FEED_DATA> listReduceFeedData = new List<CAL_REDUCE_FEED_DATA>();
                CreateQMS_TR_REDUCE_FEED createReduceeFeed = new CreateQMS_TR_REDUCE_FEED();
                //0. get downtime detail 
                List<ViewQMS_MA_REDUCE_FEED> listReduceFeed = getAllQMS_MA_REDUCE_FEEDByPlantId(searchModel.PLANT_ID);

                //1. list data by serach
                List<ViewQMS_TR_EXQ_REDUCE_FEED> listData = this.GetSQLAllQMS_TR_EXQ_REDUCE_FEEDBySearch(searchModel);
                listReduceFeedData = getCAL_REDUCE_FEED_DATA(listData);
                int start;
                int end;
                bool bfound = false;
                bool bGCError = false;
                bool bGCFlowError = false;
                string MESSAGE_NAME = "";
                VALIDATE_EXCEPTION_OVERLLAP checkOverlap = new VALIDATE_EXCEPTION_OVERLLAP();

                if (listReduceFeedData.ToArray().Length > 0)
                {
                    foreach (CAL_REDUCE_FEED_DATA dtCalReduceFeed in listReduceFeedData)
                    {
                        bfound = false; start = -1; end = 0; MESSAGE_NAME = ""; bGCError = false; bGCFlowError = false;

                        try
                        {
                           // decimal limitedCheck = getReduceFeedOverLimit(listReduceFeed, dtCalReduceFeed.TAG_DATE);
                            string[] TAG_VALUE = dtCalReduceFeed.TAG_VALUE.Split(',');
                            string[] TAG_GCERROR = dtCalReduceFeed.TAG_GCERROR.Split(',');


                            if ((TAG_GCERROR.Length == 1 && dtCalReduceFeed.TAG_VALUE == "NULL") || TAG_GCERROR.Length < 719) //cannot read data
                            { 
                                bfound = true;
                                start = 0;
                                end = 719;
                                MESSAGE_NAME = _msgExqReadErr;
                            }
                            else
                            {
                                for (int i = 0; i < TAG_VALUE.Length; i++)
                                {
                                    MESSAGE_NAME = ""; 
                                    //if (isOverLimit(TAG_VALUE[i], limitedCheck) || (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR).ToString()))
                                    if (TAG_GCERROR[i] != "0")//แสดงว่ามี error 
                                    {
                                        bfound = true;



                                        if (start == -1)
                                        {
                                            start = i;
                                            end = i;
                                        }
                                        else if ((end + 1) == i)
                                        {
                                            end = i;
                                        }
                                        else
                                        {
                                            createReduceeFeed = new CreateQMS_TR_REDUCE_FEED();

                                            if (bGCError == true)
                                            {
                                                MESSAGE_NAME = _msgGCERROR;
                                            }

                                            if (bGCFlowError == true)
                                            {
                                                if (MESSAGE_NAME != "")
                                                {
                                                    MESSAGE_NAME += " , " + _msgUnderFlow;
                                                }
                                                else
                                                {
                                                    MESSAGE_NAME = _msgUnderFlow;
                                                }
                                            }

                                            createReduceeFeed.ID = 0;
                                            createReduceeFeed.PLANT_ID = dtCalReduceFeed.PLANT_ID;
                                            createReduceeFeed.START_DATE = new DateTime(dtCalReduceFeed.TAG_DATE.Year, dtCalReduceFeed.TAG_DATE.Month, dtCalReduceFeed.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                                            createReduceeFeed.END_DATE = new DateTime(dtCalReduceFeed.TAG_DATE.Year, dtCalReduceFeed.TAG_DATE.Month, dtCalReduceFeed.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                                            createReduceeFeed.CAUSE_DESC = MESSAGE_NAME;// _msgDesc;
                                            createReduceeFeed.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.REDUCE_FEED;
                                            createReduceeFeed.DOC_STATUS = (byte)DOC_STATUS.PENDING;

                                            checkOverlap = IsOverlapMasterCorrectDataEx(createReduceeFeed.ID, createReduceeFeed.PLANT_ID, createReduceeFeed.PRODUCT_ID, createReduceeFeed.DOC_STATUS, createReduceeFeed.START_DATE, createReduceeFeed.END_DATE, (byte)CORRECT_DATA_TYPE.REDUCE_FEED);
                                            if (checkOverlap.IS_OVERLAP == false)
                                            {
                                                SaveQMS_TR_REDUCE_FEED(createReduceeFeed);
                                                SaveQMS_TR_OFF_CONTROL_CAL_With_REDUCE_FEED(createReduceeFeed);
                                            }
                                             

                                            //Save Initial 
                                            createReduceeFeed.ID = 0;
                                            createReduceeFeed.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                                            SaveQMS_TR_REDUCE_FEED(createReduceeFeed);
                                            //SaveQMS_TR_OFF_CONTROL_CAL_With_REDUCE_FEED(createReduceeFeed);

                                            start = i;
                                            end = i;
                                            bfound = false;
                                            MESSAGE_NAME = ""; bGCError = false; bGCFlowError = false;
                                        }

                                        if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR).ToString())
                                        {
                                            bGCError = true;
                                        }
                                        else
                                        {
                                            bGCFlowError = true;
                                        }
                                    }
                                }
                            }

                            if (bfound == true)
                            {
                                createReduceeFeed = new CreateQMS_TR_REDUCE_FEED();

                                if (bGCError == true)
                                {
                                    MESSAGE_NAME = _msgGCERROR;
                                }

                                if (bGCFlowError == true)
                                {
                                    if (MESSAGE_NAME != "")
                                    {
                                        MESSAGE_NAME += " , " + _msgUnderFlow;
                                    }
                                    else
                                    {
                                        MESSAGE_NAME = _msgUnderFlow;
                                    }
                                }

                                createReduceeFeed.ID = 0;
                                createReduceeFeed.PLANT_ID = dtCalReduceFeed.PLANT_ID;
                                createReduceeFeed.START_DATE = new DateTime(dtCalReduceFeed.TAG_DATE.Year, dtCalReduceFeed.TAG_DATE.Month, dtCalReduceFeed.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                                createReduceeFeed.END_DATE = new DateTime(dtCalReduceFeed.TAG_DATE.Year, dtCalReduceFeed.TAG_DATE.Month, dtCalReduceFeed.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                                createReduceeFeed.CAUSE_DESC = MESSAGE_NAME;// _msgDesc;
                                createReduceeFeed.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.REDUCE_FEED;
                                createReduceeFeed.DOC_STATUS = (byte)DOC_STATUS.PENDING;

                                checkOverlap = IsOverlapMasterCorrectDataEx(createReduceeFeed.ID, createReduceeFeed.PLANT_ID, createReduceeFeed.PRODUCT_ID, createReduceeFeed.DOC_STATUS, createReduceeFeed.START_DATE, createReduceeFeed.END_DATE, (byte)CORRECT_DATA_TYPE.REDUCE_FEED);
                                if (checkOverlap.IS_OVERLAP == false)
                                {
                                    SaveQMS_TR_REDUCE_FEED(createReduceeFeed);
                                    SaveQMS_TR_OFF_CONTROL_CAL_With_REDUCE_FEED(createReduceeFeed);
                                }

                                //Save Initial 
                                createReduceeFeed.ID = 0;
                                createReduceeFeed.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                                SaveQMS_TR_REDUCE_FEED(createReduceeFeed);
                                //SaveQMS_TR_OFF_CONTROL_CAL_With_REDUCE_FEED(createReduceeFeed);

                                start = 0;
                                end = 0;
                                bfound = false;
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
        }

        protected string getAbnormalMessage(bool bGCError, bool bGCFlowError, bool bFlowFreez, bool bTarget)
        {
            string MESSAGE_NAME = "";
            try
            {
                

                if (bGCError == true)
                {
                    MESSAGE_NAME = _msgGCERROR;
                }

                if (bGCFlowError == true)
                {
                    if (MESSAGE_NAME != "")
                    {
                        MESSAGE_NAME += " , " + _msgUnderFlow;
                    }
                    else
                    {
                        MESSAGE_NAME = _msgUnderFlow;
                    }
                }

                if (bFlowFreez == true)
                {
                    if (MESSAGE_NAME != "")
                    {
                        MESSAGE_NAME += " , " + _msgFrezz;
                    }
                    else
                    {
                        MESSAGE_NAME = _msgFrezz;
                    }
                }


                if (bTarget == true)
                {
                    if (MESSAGE_NAME != "")
                    {
                        MESSAGE_NAME += " , " + _msgTargetError;
                    }
                    else
                    {
                        MESSAGE_NAME = _msgTargetError;
                    }
                }
            }
            catch
            {

            }
            return MESSAGE_NAME;
        }

        public void SQLCalAbnormalToDB(List<CAL_OFF_CONTROL> listCalData) //cal ทั้งวัน
        {
            try
            {
                CreateQMS_TR_ABNORMAL createAbnormal = new CreateQMS_TR_ABNORMAL();
                //1. list data by serach

                int start = -1;
                int end = -1;
                bool bfound = false;
                bool bGCError = false;
                bool bGCFlowError = false;
                bool bFlowFreez = false;
                bool bTarget = false;
                decimal volume = 0; 
                int indexPos = 0;
                VALIDATE_EXCEPTION_OVERLLAP checkOverlap = new VALIDATE_EXCEPTION_OVERLLAP();

                foreach (CAL_OFF_CONTROL dtCAL_OFF_CONTROL in listCalData)
                {
                    string[] TAG_GCERROR = dtCAL_OFF_CONTROL.TAG_GCERROR.Split(',');
                    string[] TAG_VOLUME = dtCAL_OFF_CONTROL.TAG_VOLUME.Split(',');
                    string[] OFF_SPEC_NAME = dtCAL_OFF_CONTROL.OFF_SPEC_NAME.Split(',');

                    //initial value;
                    bfound = false; start = -1; end = -1; volume = 0;  indexPos = 0; bGCError = false; bGCFlowError = false;  bFlowFreez = false; bTarget = false;

                    for (int i = 0; i < TAG_GCERROR.Length; i++)
                    {
                        if (TAG_GCERROR[i] != ((byte)CAL_OFF_STATUS.NORMAL).ToString())//แสดงว่ามี error
                        {
                            if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR).ToString()
                                    || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR_FLOWRATE).ToString()
                                    || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR_FREEZ).ToString()
                                    || TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.TAG_TARGET_ERR).ToString()
                                )
                            {
                                
                                bfound = true;
                                
                               

                                if (start == -1)
                                {
                                    start = i;
                                    end = i;
                                    volume += getConvertTextToDecimal(TAG_VOLUME[i]); //Convert.ToDecimal(TAG_VOLUME[i]); 
                                }
                                else if ((end + 1) == i)
                                {
                                    end = i;
                                    volume += getConvertTextToDecimal(TAG_VOLUME[i]); //Convert.ToDecimal(TAG_VOLUME[i]);
                                }
                                else
                                {
                                    createAbnormal = new CreateQMS_TR_ABNORMAL();

                                    

                                    createAbnormal.ID = 0;
                                    createAbnormal.PLANT_ID = dtCAL_OFF_CONTROL.PLANT_ID;
                                    createAbnormal.PRODUCT_ID = dtCAL_OFF_CONTROL.PRODUCT_ID;
                                    createAbnormal.START_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                                    createAbnormal.END_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                                    createAbnormal.CAUSE_DESC = getAbnormalMessage(bGCError, bGCFlowError, bFlowFreez, bTarget);
                                    createAbnormal.VOLUME = volume;
                                    createAbnormal.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.ABNORMAL;
                                    createAbnormal.DOC_STATUS = (byte)DOC_STATUS.PENDING;

                                    checkOverlap = IsOverlapMasterCorrectDataEx(createAbnormal.ID, createAbnormal.PLANT_ID, createAbnormal.PRODUCT_ID, createAbnormal.DOC_STATUS, createAbnormal.START_DATE, createAbnormal.END_DATE, (byte)CORRECT_DATA_TYPE.ABNORMAL);
                                    if (checkOverlap.IS_OVERLAP == false)
                                    {
                                        SaveQMS_TR_ABNORMAL(createAbnormal);
                                        SaveQMS_TR_OFF_CONTROL_CAL_With_ABNORMAL(createAbnormal);
                                    }

                                   

                                    //Save Initial 
                                    createAbnormal.ID = 0;
                                    createAbnormal.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                                    SaveQMS_TR_ABNORMAL(createAbnormal);
                                    //SaveQMS_TR_OFF_CONTROL_CAL_With_ABNORMAL(createAbnormal);

                                    start = i;
                                    end = i;
                                    volume = getConvertTextToDecimal(TAG_VOLUME[i]); //Convert.ToDecimal(TAG_VOLUME[i]);
                                    bfound = false;
                                    bGCError = false; bGCFlowError = false; bFlowFreez = false; bTarget = false;
                                }

                                if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR).ToString())
                                {
                                    bGCError = true;
                                }

                                if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR_FLOWRATE).ToString())
                                {
                                    bGCFlowError = true;
                                }

                                if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.GC_ERROR_FREEZ).ToString())
                                {
                                    bFlowFreez = true;
                                }

                                if (TAG_GCERROR[i] == ((byte)CAL_OFF_STATUS.TAG_TARGET_ERR).ToString())
                                {
                                    bTarget = true;
                                }
                            }
                        }
                    }

                    if (bfound == true)
                    {
                        createAbnormal = new CreateQMS_TR_ABNORMAL();

                         

                        createAbnormal.ID = 0;
                        createAbnormal.PLANT_ID = dtCAL_OFF_CONTROL.PLANT_ID;
                        createAbnormal.PRODUCT_ID = dtCAL_OFF_CONTROL.PRODUCT_ID;
                        createAbnormal.START_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(start * 2);
                        createAbnormal.END_DATE = new DateTime(dtCAL_OFF_CONTROL.TAG_DATE.Year, dtCAL_OFF_CONTROL.TAG_DATE.Month, dtCAL_OFF_CONTROL.TAG_DATE.Day, 0, 0, 0).AddMinutes(end * 2);
                        createAbnormal.CAUSE_DESC = getAbnormalMessage(bGCError, bGCFlowError, bFlowFreez, bTarget);
                        createAbnormal.CORRECT_DATA_TYPE = (byte)CORRECT_DATA_TYPE.ABNORMAL;
                        createAbnormal.VOLUME = volume;
                        createAbnormal.DOC_STATUS = (byte)DOC_STATUS.PENDING;

                        checkOverlap = IsOverlapMasterCorrectDataEx(createAbnormal.ID, createAbnormal.PLANT_ID, createAbnormal.PRODUCT_ID, createAbnormal.DOC_STATUS, createAbnormal.START_DATE, createAbnormal.END_DATE, (byte)CORRECT_DATA_TYPE.ABNORMAL);
                        if (checkOverlap.IS_OVERLAP == false)
                        {
                            SaveQMS_TR_ABNORMAL(createAbnormal);
                            SaveQMS_TR_OFF_CONTROL_CAL_With_ABNORMAL(createAbnormal);
                        }

                        //Save Initial 
                        createAbnormal.ID = 0;
                        createAbnormal.DOC_STATUS = (byte)DOC_STATUS.INITIAL;
                        SaveQMS_TR_ABNORMAL(createAbnormal);
                        //SaveQMS_TR_OFF_CONTROL_CAL_With_ABNORMAL(createAbnormal);

                        volume = 0;
                        bfound = false; 
                    }

                }
            }
            catch (Exception ex)
            {


            }
        }

        public void SQLCalOffControlOffSpec(ExaProductSearchModel searchModel)
        {
            try
            {
                //del old inital data
                DeleteAllQMS_TR_OFF_CONTROL_CAL(searchModel); //เปลี่ยนเป็น ลบทั้งหมดทุกสถานะ ที่เป็น off control, off spec DeleteIntialQMS_TR_OFF_CONTROL_CAL(searchModel);
                DeleteAllQMS_TR_ABNORMAL(searchModel);//เปลี่ยนเป็น ลบทั้งหมดทุกสถานะ DeleteIntialQMS_TR_ABNORMAL(searchModel);


                //ค้นหาที ละ plant และ product 
                CreateQMS_TR_EXQ_PRODUCT createProductData = new CreateQMS_TR_EXQ_PRODUCT();
                //1. list data by serach
                List<ViewQMS_TR_EXQ_PRODUCT> listProductData = this.GetSQLAllQMS_TR_EXQ_PRODUCTBySearch(searchModel);
                List<CAL_OFF_CONTROL> listCalData = new List<CAL_OFF_CONTROL>();
                //int start = 0;
                //int end = 0;
                //bool bfound = true;


                if (null != listProductData && listProductData.ToArray().Length > 0)
                {
                    //Step 0. เตรียมดาต้า ดึงค่า EXQ_TAG ออกมาทั้งหมด 
                    long[] idsTAG_EXA_ID = getListQMS_TR_EXQ_PRODUCTId(listProductData).ToArray();

                    List<ViewQMS_MA_EXQ_TAG> listEXQ_TAG = this.getListEXQTagByListId(idsTAG_EXA_ID);
                    long[] idsControl = getControlIdFromExaList(listEXQ_TAG).ToArray();
                    long[] idsSpec = getSpecIdFromExaList(listEXQ_TAG).ToArray();
                    List<ViewQMS_MA_CONTROL_DATA> listControl = getQMS_MA_CONTROL_DATAListByListIds(idsControl);
                    List<ViewQMS_MA_CONTROL_DATA> listSpec = getQMS_MA_CONTROL_DATAListByListIds(idsSpec);

                    //Step 1. จัดเรียงดาต้าให้ เหลือ วันละ 1 แถวก่อน
                    listCalData = getSQLCalListData(listProductData, listEXQ_TAG, listControl, listSpec);

                    //Step 2. เก็บข้อมุลไปที่ off control, off spec, abnormal...
                    SQLCalOffControlToDB(listCalData, (byte)OFF_TYPE.CONTROL);
                    SQLCalOffControlToDB(listCalData, (byte)OFF_TYPE.SPEC);
                    SQLCalAbnormalToDB(listCalData);

                    //SQLCalDowntimeToDB(searchModel);
                    //SQLCalReduceFeedToDB(searchModel);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
        }

        #endregion

        #region schedule 
        public List<ViewQMS_ST_EXAQUATUM_SCHEDULE> getSQLQMS_ST_EXAQUATUM_SCHEDULEListByStatus(int status)
        {
            List<ViewQMS_ST_EXAQUATUM_SCHEDULE> listResult = new List<ViewQMS_ST_EXAQUATUM_SCHEDULE>(); 
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "SELECT * FROM QMS_ST_EXAQUATUM_SCHEDULE WHERE DELETE_FLAG = 0 AND  DOC_STATUS = " + status;
                SqlConnection connection = getConnection();
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    { 

                        ViewQMS_ST_EXAQUATUM_SCHEDULE temp = new ViewQMS_ST_EXAQUATUM_SCHEDULE();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());
                          
                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {

            }

            return listResult;
        }

        public long SaveQMS_ST_EXAQUATUM_SCHEDULE(CreateQMS_ST_EXAQUATUM_SCHEDULE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_ST_EXAQUATUM_SCHEDULE(  model);
            }
            else
            {
                result = AddQMS_ST_EXAQUATUM_SCHEDULE( model);
            }

            return result;
        }


        public long AddQMS_ST_EXAQUATUM_SCHEDULE(CreateQMS_ST_EXAQUATUM_SCHEDULE model)
        {
            long result = 0;

            try
            { 
                SqlConnection conn = getConnection();
                DateTime timeStamp = DateTime.Now; 
                SqlCommand SQLcommand = new SqlCommand("INSERT INTO QMS_ST_EXAQUATUM_SCHEDULE   "
                                        + " ( PLANT_ID, DOC_STATUS, START_DATE, END_DATE ,  "
                                        + " DELETE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE ) "
                                        + " VALUES(@PLANT_ID, @DOC_STATUS, @START_DATE, @END_DATE, "
                                        + " @DELETE_FLAG,  @CREATE_USER, @CREATE_DATE, @UPDATE_USER, @UPDATE_DATE )  ", conn);
                conn.Open();


                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);
                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                SQLcommand.Parameters.AddWithValue("@DELETE_FLAG", 0);

                SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }


        public long UpdateQMS_ST_EXAQUATUM_SCHEDULE(CreateQMS_ST_EXAQUATUM_SCHEDULE model)
        {
            long result = 0;

            try
            {
                DateTime timeStamp = DateTime.Now; 
                SqlConnection conn = getConnection();
                SqlCommand SQLcommand;

                conn.Open();
                SQLcommand = new SqlCommand("UPDATE QMS_ST_EXAQUATUM_SCHEDULE   "
                                        + " SET PLANT_ID = @PLANT_ID , DOC_STATUS =  @DOC_STATUS, START_DATE = @START_DATE, "
                                        + " END_DATE = @END_DATE,  UPDATE_USER = @UPDATE_USER, UPDATE_DATE = @UPDATE_DATE "
                                        + " WHERE ID = @ID", conn);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", model.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", model.DOC_STATUS);
                SQLcommand.Parameters.AddWithValue("@START_DATE", model.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", model.END_DATE);
                //SQLcommand.Parameters.AddWithValue("@DELETE_FLAG", model.DELETE_FLAG); 

                //SQLcommand.Parameters.AddWithValue("@CREATE_USER", _msgUser);
                //SQLcommand.Parameters.AddWithValue("@CREATE_DATE", timeStamp);
                SQLcommand.Parameters.AddWithValue("@UPDATE_USER", _msgUser);
                SQLcommand.Parameters.AddWithValue("@UPDATE_DATE", timeStamp);

                SQLcommand.Parameters.AddWithValue("@ID", model.ID);

                SQLcommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }
            return result;
        }

        #endregion


        #region delete correct data intial
        public long DeleteIntialQMS_TR_REDUCE_FEED(ExaProductSearchModel search)
        {
            long result = 0;

            try
            {
                //check only inital and pending  DOC_STATUS.INITIAL, DOC_STATUS.PENDING,  

                List<ViewQMS_TR_REDUCE_FEED> listReduceFeed = getAllReduceFeedByPlantProduct(search.PLANT_ID, 0, (int)DOC_STATUS.INITIAL, search.START_DATE, search.END_DATE);
                if (null != listReduceFeed && listReduceFeed.Count > 0)
                {
                    foreach (ViewQMS_TR_REDUCE_FEED dtDownTime in listReduceFeed)
                    {
                        SaveReduceFeedBySearch(search.START_DATE, search.END_DATE, dtDownTime);
                    }
                }

                listReduceFeed = getAllReduceFeedByPlantProduct(search.PLANT_ID, 0, (int)DOC_STATUS.PENDING, search.START_DATE, search.END_DATE);
                if (null != listReduceFeed && listReduceFeed.Count > 0)
                {
                    foreach (ViewQMS_TR_REDUCE_FEED dtDownTime in listReduceFeed)
                    {
                        SaveReduceFeedBySearch(search.START_DATE, search.END_DATE, dtDownTime);
                    }
                }
                 
                //SqlConnection conn = getConnection();

                ////SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_REDUCE_FEED   "
                ////                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                ////                        + " AND PLANT_ID = @PLANT_ID "
                ////                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);

                

                //SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_REDUCE_FEED   "
                //                        + " WHERE PLANT_ID = @PLANT_ID "
                //                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);

                //SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                //SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                //SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                //conn.Open();
                //SQLcommand.ExecuteNonQuery();
                //conn.Close(); 

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteQMS_TR_REDUCE_FEEDbyList(string ids, byte docStatus)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_REDUCE_FEED   "
                                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                                        + " AND ID in " + ids, conn);

                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                //SQLcommand.Parameters.AddWithValue("@IDS", ids);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteIntialQMS_TR_DOWNTIME(ExaProductSearchModel search) //All
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                //SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_DOWNTIME   "
                //                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                //                        + " AND PLANT_ID = @PLANT_ID "
                //                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_DOWNTIME   "
                                        + " WHERE PLANT_ID = @PLANT_ID "
                                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);
                 
                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteQMS_TR_DOWNTIMEbyList(string ids, byte docStatus)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_DOWNTIME   "
                                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                                        + " AND ID in " + ids, conn);

                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                //SQLcommand.Parameters.AddWithValue("@IDS", ids); 

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteIntialQMS_TR_ABNORMAL(ExaProductSearchModel search)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                //SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_ABNORMAL   "
                //                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                //                        + " AND PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                //                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_ABNORMAL   "
                                        + " WHERE  PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);

                //SQLcommand.Parameters.AddWithValue("@DOC_STATUS", (byte)DOC_STATUS.INITIAL);
                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", search.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteQMS_TR_ABNORMALbyList(string ids, byte docStatus)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_ABNORMAL   "
                                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                                        + " AND ID in " + ids, conn);

                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                //SQLcommand.Parameters.AddWithValue("@IDS", ids);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteAllQMS_TR_ABNORMAL(ExaProductSearchModel search)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_ABNORMAL   "
                                        + " WHERE PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);
                 
                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", search.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteIntialQMS_TR_OFF_CONTROL_CAL(ExaProductSearchModel search)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                //SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_OFF_CONTROL_CAL   "
                //                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                //                        + " AND PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                //                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_OFF_CONTROL_CAL   "
                                       + " WHERE  PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                                       + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);

                //SQLcommand.Parameters.AddWithValue("@DOC_STATUS", (byte)DOC_STATUS.INITIAL);
                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", search.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteQMS_TR_OFF_CONTROL_CALbyList(string ids, byte docStatus)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_OFF_CONTROL_CAL   "
                                        + " WHERE DOC_STATUS = @DOC_STATUS  "
                                        + " AND ID in " + ids, conn);

                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                //SQLcommand.Parameters.AddWithValue("@IDS", ids);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public long DeleteAllQMS_TR_OFF_CONTROL_CAL(ExaProductSearchModel search)
        {
            long result = 0;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_OFF_CONTROL_CAL   "
                                        + " WHERE PLANT_ID = @PLANT_ID AND PRODUCT_ID = @PRODUCT_ID "
                                        + " AND START_DATE >= @START_DATE AND END_DATE <= @END_DATE ", conn);
 
                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", search.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                result = -1;
            }

            return result;
        }

        public bool DeleteQMS_TR_OFF_CONTROL_CALById(long id)
        {
            bool result = false;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_OFF_CONTROL_CAL WHERE ID = @ID ", conn);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

                result = true;
            }
            catch (Exception ex)
            {
                msgError = ex.Message; 
            }

            return result;
        }

        public bool DeleteQMS_TR_DOWNTIMEById(long id)
        {
            bool result = false;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_DOWNTIME WHERE ID = @ID ", conn);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

                result = true;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return result;
        }


        public bool DeleteQMS_TR_CHANGE_GRADEById(long id)
        {
            bool result = false;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_CHANGE_GRADE WHERE ID = @ID ", conn);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

                result = true;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return result;
        }

        public bool DeleteQMS_TR_CROSS_VOLUMEById(long id)
        {
            bool result = false;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_CROSS_VOLUME WHERE ID = @ID ", conn);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

                result = true;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return result;
        }

        public bool DeleteQMS_TR_ABNORMALById(long id)
        {
            bool result = false;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_ABNORMAL WHERE ID = @ID ", conn);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

                result = true;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return result;
        }

        public bool DeleteQMS_TR_REDUCE_FEEDById(long id)
        {
            bool result = false;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_REDUCE_FEED WHERE ID = @ID ", conn);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

                result = true;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return result;
        }

        public bool DeleteQMS_TR_EXCEPTIONById(long id)
        {
            bool result = false;

            try
            {

                SqlConnection conn = getConnection();

                SqlCommand SQLcommand = new SqlCommand("DELETE FROM QMS_TR_EXCEPTION WHERE ID = @ID ", conn);

                SQLcommand.Parameters.AddWithValue("@ID", id);

                conn.Open();
                SQLcommand.ExecuteNonQuery();
                conn.Close();

                result = true;
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return result;
        }

        #endregion


        #region Resolve probelm
        
        public List<ViewQMS_TR_OFF_CONTROL_CAL> getAllOffControlByPlantProduct(long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_OFF_CONTROL_CAL> listResult = new List<ViewQMS_TR_OFF_CONTROL_CAL>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

                if (productId == 0) //productId
                {
                    queryString = "SELECT * FROM QMS_TR_OFF_CONTROL_CAL  "
                                    + " WHERE PLANT_ID = @PLANT_ID   " 
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }
                else
                {
                    queryString = "SELECT * FROM QMS_TR_OFF_CONTROL_CAL  "
                                    + " WHERE PLANT_ID = @PLANT_ID   "
                                    + " AND PRODUCT_ID = @PRODUCT_ID "
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                if (productId != 0) //productId
                {
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                }
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);

                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_OFF_CONTROL_CAL temp = new ViewQMS_TR_OFF_CONTROL_CAL();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.OFF_CONTROL_TYPE = Convert.ToByte(reader["OFF_CONTROL_TYPE"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        temp.VOLUME = Convert.ToDecimal(reader["VOLUME"].ToString());
                        temp.CONTROL_VALUE = reader["CONTROL_VALUE"].ToString();
                        temp.ROOT_CAUSE_ID = Convert.ToInt64(reader["ROOT_CAUSE_ID"]);
                        temp.OFF_CONTROL_DESC = reader["OFF_CONTROL_DESC"].ToString();
                        temp.CROSS_VOLUME_FLAG = Convert.ToInt64(reader["CROSS_VOLUME_FLAG"]);

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }


        public List<ViewQMS_TR_DOWNTIME> getAllDowntimeByPlant(long modelID, long plantID, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_DOWNTIME> listResult = new List<ViewQMS_TR_DOWNTIME>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

                queryString = "SELECT * FROM QMS_TR_DOWNTIME  "
                                   + " WHERE PLANT_ID = @PLANT_ID   "
                                   + " AND DOC_STATUS = @DOC_STATUS "
                                   + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                   + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                   + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                   + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);

                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_DOWNTIME temp = new ViewQMS_TR_DOWNTIME();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);  
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        temp.CORRECT_DATA_TYPE = Convert.ToInt64(reader["CORRECT_DATA_TYPE"]);
                        temp.CAUSE_DESC = reader["CAUSE_DESC"].ToString();

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_TR_CHANGE_GRADE> getAllChangeGradeByPlant(long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_CHANGE_GRADE> listResult = new List<ViewQMS_TR_CHANGE_GRADE>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

               

                if (productId == 0) //productId
                {
                    queryString = "SELECT * FROM QMS_TR_CHANGE_GRADE  "
                                  + " WHERE PLANT_ID = @PLANT_ID   "
                                  + " AND DOC_STATUS = @DOC_STATUS "
                                  + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                  + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                  + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                  + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }
                else
                {
                    queryString = "SELECT * FROM QMS_TR_CHANGE_GRADE  "
                                  + " WHERE PLANT_ID = @PLANT_ID   "
                                  + " AND PRODUCT_ID = @PRODUCT_ID "
                                  + " AND DOC_STATUS = @DOC_STATUS "
                                  + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                  + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                  + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                  + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) "; 
                }

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);
                if (productId != 0) //productId
                {
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                }
                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_CHANGE_GRADE temp = new ViewQMS_TR_CHANGE_GRADE();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        temp.GRADE_FROM = Convert.ToInt64(reader["GRADE_FROM"]);
                        temp.GRADE_TO = Convert.ToInt64(reader["GRADE_TO"]);
                        temp.VOLUME = Convert.ToDecimal(reader["VOLUME"]);

                        temp.CORRECT_DATA_TYPE = Convert.ToInt64(reader["CORRECT_DATA_TYPE"]);
                        temp.CAUSE_DESC = reader["CAUSE_DESC"].ToString();

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }


        public List<ViewQMS_TR_CROSS_VOLUME> getAllCrossVolumeByPlant(long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_CROSS_VOLUME> listResult = new List<ViewQMS_TR_CROSS_VOLUME>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

                if (productId == 0) //productId
                {

                    queryString = "SELECT * FROM QMS_TR_CROSS_VOLUME  "
                                       + " WHERE PLANT_ID = @PLANT_ID   "
                                       + " AND DOC_STATUS = @DOC_STATUS "
                                       + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                       + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                       + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                       + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }
                else
                {

                    queryString = "SELECT * FROM QMS_TR_CROSS_VOLUME  "
                                       + " WHERE PLANT_ID = @PLANT_ID   "
                                       + " AND PRODUCT_ID = @PRODUCT_ID "
                                       + " AND DOC_STATUS = @DOC_STATUS "
                                       + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                       + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                       + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                       + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                     
                }

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                if (productId != 0) //productId
                {
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                }
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);

                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_CROSS_VOLUME temp = new ViewQMS_TR_CROSS_VOLUME();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        temp.PRODUCT_TO = Convert.ToInt64(reader["PRODUCT_TO"]);
                        temp.VOLUME = Convert.ToDecimal(reader["VOLUME"]);

                        temp.CORRECT_DATA_TYPE = Convert.ToInt64(reader["CORRECT_DATA_TYPE"]);
                        temp.CAUSE_DESC = reader["CAUSE_DESC"].ToString();

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_TR_ABNORMAL> getAllAbnormalByPlantProduct(long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_ABNORMAL> listResult = new List<ViewQMS_TR_ABNORMAL>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

                if (productId == 0) //productId
                {
                    queryString = "SELECT * FROM QMS_TR_ABNORMAL  "
                                    + " WHERE PLANT_ID = @PLANT_ID   "
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }
                else
                {
                    queryString = "SELECT * FROM QMS_TR_ABNORMAL  "
                                    + " WHERE PLANT_ID = @PLANT_ID   "
                                    + " AND PRODUCT_ID = @PRODUCT_ID "
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                if (productId != 0) //productId
                {
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                }
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);

                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_ABNORMAL temp = new ViewQMS_TR_ABNORMAL();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]); 
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());

                        temp.VOLUME = Convert.ToDecimal(reader["VOLUME"].ToString()); 
                        temp.CORRECT_DATA_TYPE = Convert.ToInt64(reader["CORRECT_DATA_TYPE"]);
                        temp.CAUSE_DESC = reader["CAUSE_DESC"].ToString();

                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_TR_REDUCE_FEED> getAllReduceFeedByPlantProduct(long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_REDUCE_FEED> listResult = new List<ViewQMS_TR_REDUCE_FEED>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

                if (productId == 0) //productId
                {
                    queryString = "SELECT * FROM QMS_TR_REDUCE_FEED  "
                                    + " WHERE PLANT_ID = @PLANT_ID   "
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }
                else
                {
                    queryString = "SELECT * FROM QMS_TR_REDUCE_FEED  "
                                    + " WHERE PLANT_ID = @PLANT_ID   "
                                    + " AND PRODUCT_ID = @PRODUCT_ID "
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                if (productId != 0) //productId
                {
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                }
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);

                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_REDUCE_FEED temp = new ViewQMS_TR_REDUCE_FEED();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());
                         
                        temp.CORRECT_DATA_TYPE = Convert.ToInt64(reader["CORRECT_DATA_TYPE"]);
                        temp.CAUSE_DESC = reader["CAUSE_DESC"].ToString();
                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<ViewQMS_TR_EXCEPTION> getAllExceptionByPlantProduct(long modelID, long plantID, long productId, byte docStatus, DateTime startDate, DateTime endDate)
        {
            List<ViewQMS_TR_EXCEPTION> listResult = new List<ViewQMS_TR_EXCEPTION>();
            //DataTable result = new DataTable(); 
            try
            {
                string queryString = "";

                if (productId == 0) //productId
                {
                    queryString = "SELECT * FROM QMS_TR_EXCEPTION  "
                                    + " WHERE PLANT_ID = @PLANT_ID   "
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }
                else
                {
                    queryString = "SELECT * FROM QMS_TR_EXCEPTION  "
                                    + " WHERE PLANT_ID = @PLANT_ID   "
                                    + " AND PRODUCT_ID = @PRODUCT_ID "
                                    + " AND DOC_STATUS = @DOC_STATUS "
                                    + " AND ( ( @START_DATE1 > START_DATE AND @END_DATE1 < END_DATE) "
                                    + " OR (START_DATE >= @START_DATE2 AND END_DATE <= @END_DATE2) "
                                    + " OR (@END_DATE3 > START_DATE AND @END_DATE4 < END_DATE) "
                                    + " OR (@START_DATE3> START_DATE AND @START_DATE4 < END_DATE) ) ";
                }

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                if (productId != 0) //productId
                {
                    SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);
                }
                SQLcommand.Parameters.AddWithValue("@DOC_STATUS", docStatus);

                SQLcommand.Parameters.AddWithValue("@START_DATE1", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE2", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE3", startDate);
                SQLcommand.Parameters.AddWithValue("@START_DATE4", startDate);

                SQLcommand.Parameters.AddWithValue("@END_DATE1", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE2", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE3", endDate);
                SQLcommand.Parameters.AddWithValue("@END_DATE4", endDate);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        ViewQMS_TR_EXCEPTION temp = new ViewQMS_TR_EXCEPTION();
                        temp.ID = Convert.ToInt64(reader["ID"]);
                        temp.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        temp.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        temp.DOC_STATUS = Convert.ToByte(reader["DOC_STATUS"]);
                        temp.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                        temp.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());
                        temp.VOLUME = Convert.ToDecimal(reader["VOLUME"].ToString()); 

                        temp.CORRECT_DATA_TYPE = Convert.ToInt64(reader["CORRECT_DATA_TYPE"]);
                        temp.CAUSE_DESC = reader["CAUSE_DESC"].ToString();
                        temp.CREATE_DATE = Convert.ToDateTime(reader["CREATE_DATE"].ToString());
                        temp.CREATE_USER = reader["CREATE_USER"].ToString();
                        temp.UPDATE_DATE = Convert.ToDateTime(reader["UPDATE_DATE"].ToString());
                        temp.UPDATE_USER = reader["UPDATE_USER"].ToString();

                        listResult.Add(temp);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public long SaveQMS_TR_OFF_CONTROL_CAL_With_ABNORMAL(CreateQMS_TR_ABNORMAL model)
        {
            long nResult = 0;

            CreateQMS_TR_OFF_CONTROL_CAL tempCreate = new CreateQMS_TR_OFF_CONTROL_CAL();
            CreateQMS_TR_OFF_CONTROL_CAL tempCreate2 = new CreateQMS_TR_OFF_CONTROL_CAL();

            try
            {
                //Step1. Clean or off control in this time  
                List<ViewQMS_TR_OFF_CONTROL_CAL> listOffCal = getAllOffControlByPlantProduct(model.ID, model.PLANT_ID, model.PRODUCT_ID, model.DOC_STATUS, model.START_DATE, model.END_DATE);

                if (listOffCal.Count > 0)
                {
                    foreach (ViewQMS_TR_OFF_CONTROL_CAL dtOffCal in listOffCal)
                    {
                        SaveOffControlBySearch(model.START_DATE, model.END_DATE, dtOffCal);

                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public long SaveQMS_TR_OFF_CONTROL_CAL_With_REDUCE_FEED(CreateQMS_TR_REDUCE_FEED model)
        {
            long nResult = 0;

            CreateQMS_TR_OFF_CONTROL_CAL tempCreate = new CreateQMS_TR_OFF_CONTROL_CAL();
            CreateQMS_TR_OFF_CONTROL_CAL tempCreate2 = new CreateQMS_TR_OFF_CONTROL_CAL();

            try
            {
                //Step1. Clean or off control in this time  
                List<ViewQMS_TR_OFF_CONTROL_CAL> listOffCal = getAllOffControlByPlantProduct(model.ID, model.PLANT_ID, model.PRODUCT_ID, model.DOC_STATUS, model.START_DATE, model.END_DATE);

                if (listOffCal.Count > 0)
                {
                    foreach (ViewQMS_TR_OFF_CONTROL_CAL dtOffCal in listOffCal)
                    {
                        SaveOffControlBySearch( model.START_DATE, model.END_DATE, dtOffCal);
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public void CorrectDataAction(CreateQMS_TR_TURN_AROUND model, int state)
        {
            try
            {
                //Step 2 Down Time
                if (state < (int)CORRECT_DATA_TYPE.DOWN_TIME)
                {
                    List<ViewQMS_TR_DOWNTIME> listDownTime = getAllDowntimeByPlant(model.ID, model.PLANT_ID, model.DOC_STATUS, model.START_DATE, model.END_DATE);
                    if (listDownTime.Count > 0)
                    {
                        foreach (ViewQMS_TR_DOWNTIME dtDownTime in listDownTime)
                        {
                            SaveDOWNTIMEBySearch(model.START_DATE, model.END_DATE, dtDownTime);
                        }
                    }
                }

                //Step 3 Change grade
                if (state < (int)CORRECT_DATA_TYPE.CHANGE_GRADE)
                {
                    List<ViewQMS_TR_CHANGE_GRADE> listChangeGrade = getAllChangeGradeByPlant(model.ID, model.PLANT_ID, 0, model.DOC_STATUS, model.START_DATE, model.END_DATE);
                    if (null != listChangeGrade && listChangeGrade.Count > 0)
                    {
                        foreach (ViewQMS_TR_CHANGE_GRADE dtChangeGrade in listChangeGrade)
                        {
                            SaveChangeGradeBySearch(model.START_DATE, model.END_DATE, dtChangeGrade);
                        }
                    }
                }

                //Step 4 Cross volumn
                if (state < (int)CORRECT_DATA_TYPE.CROSS_VOLUME)
                {
                    List<ViewQMS_TR_CROSS_VOLUME> listCrossVolume = getAllCrossVolumeByPlant(model.ID, model.PLANT_ID, 0, model.DOC_STATUS, model.START_DATE, model.END_DATE);
                    if (null != listCrossVolume && listCrossVolume.Count  > 0)
                    {
                        foreach (ViewQMS_TR_CROSS_VOLUME dtCrossVolume in listCrossVolume)
                        {
                            SaveCrossVolumeBySearch(model.START_DATE, model.END_DATE, dtCrossVolume);
                        }
                    }
                }

                //Step 5 GC Error
                if (state < (int)CORRECT_DATA_TYPE.ABNORMAL)
                {
                    List<ViewQMS_TR_ABNORMAL> listAbnormal = getAllAbnormalByPlantProduct(model.ID, model.PLANT_ID, 0, model.DOC_STATUS, model.START_DATE, model.END_DATE);
                    if (null != listAbnormal && listAbnormal.Count  > 0)
                    {
                        foreach (ViewQMS_TR_ABNORMAL dtAbnormal in listAbnormal)
                        {
                            SaveABNORMALBySearch(model.START_DATE, model.END_DATE, dtAbnormal);
                        }
                    }
                }

                //Step 6 Reduce Feed
                if (state < (int)CORRECT_DATA_TYPE.REDUCE_FEED)
                {
                    List<ViewQMS_TR_REDUCE_FEED> listReduceFeed = getAllReduceFeedByPlantProduct(model.PLANT_ID, 0, model.DOC_STATUS, model.START_DATE, model.END_DATE);
                    if (null != listReduceFeed && listReduceFeed.Count > 0)
                    {
                        foreach (ViewQMS_TR_REDUCE_FEED dtDownTime in listReduceFeed)
                        {
                            SaveReduceFeedBySearch(model.START_DATE, model.END_DATE, dtDownTime);
                        }
                    }
                }

                //Step 7 Exception Case  
                if (state < (int)CORRECT_DATA_TYPE.EXCEPTION)
                {
                    List<ViewQMS_TR_EXCEPTION> listException = getAllExceptionByPlantProduct(model.ID, model.PLANT_ID, 0, model.DOC_STATUS, model.START_DATE, model.END_DATE);
                    if (null != listException && listException.Count > 0)
                    {
                        foreach (ViewQMS_TR_EXCEPTION dtDownTime in listException)
                        {
                            SaveExceptionBySearch(model.START_DATE, model.END_DATE, dtDownTime);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public long SaveQMS_TR_OFF_CONTROL_CAL_With_DOWNTIME(CreateQMS_TR_DOWNTIME model)
        {
            long nResult = 0;

            CreateQMS_TR_OFF_CONTROL_CAL tempCreate = new CreateQMS_TR_OFF_CONTROL_CAL();
            CreateQMS_TR_OFF_CONTROL_CAL tempCreate2 = new CreateQMS_TR_OFF_CONTROL_CAL();

            try
            {
                //Step1. Clean or off control in this time  
                List<ViewQMS_TR_OFF_CONTROL_CAL> listOffCal = getAllOffControlByPlantProduct(model.ID, model.PLANT_ID, 0, model.DOC_STATUS, model.START_DATE, model.END_DATE);

                if (listOffCal.Count  > 0)
                {
                    foreach (ViewQMS_TR_OFF_CONTROL_CAL dtOffCal in listOffCal)
                    {
                        SaveOffControlBySearch(model.START_DATE, model.END_DATE, dtOffCal);
                    }
                }

                CreateQMS_TR_TURN_AROUND modelTURN_AROUND = new CreateQMS_TR_TURN_AROUND();
                modelTURN_AROUND.START_DATE = model.START_DATE;
                modelTURN_AROUND.END_DATE = model.END_DATE;
                modelTURN_AROUND.PLANT_ID = model.PLANT_ID;
                modelTURN_AROUND.DOC_STATUS = model.DOC_STATUS;

                CorrectDataAction(modelTURN_AROUND, (int)CORRECT_DATA_TYPE.DOWN_TIME);
            }
            catch
            {


            }

            return nResult;
        }

        protected DateTime resetSecond(DateTime data)
        {
            DateTime mDummy = new DateTime(data.Year, data.Month, data.Day, data.Hour, data.Minute, 0);
            return mDummy;
        }

        private CreateQMS_TR_OFF_CONTROL_CAL convertDBToModel(ViewQMS_TR_OFF_CONTROL_CAL model)
        {
            CreateQMS_TR_OFF_CONTROL_CAL result = new CreateQMS_TR_OFF_CONTROL_CAL();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.OFF_CONTROL_TYPE = model.OFF_CONTROL_TYPE;
            result.DOC_STATUS = model.DOC_STATUS;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.VOLUME = model.VOLUME;
            result.CONTROL_VALUE = (null == model.CONTROL_VALUE) ? "" : model.CONTROL_VALUE;
            result.ROOT_CAUSE_ID = model.ROOT_CAUSE_ID;
            result.OFF_CONTROL_DESC = (null == model.OFF_CONTROL_DESC) ? "" : model.OFF_CONTROL_DESC;
            //result.DELETE_FLAG = model.DELETE_FLAG;
            result.CROSS_VOLUME_FLAG = model.CROSS_VOLUME_FLAG;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            //result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            //result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));

            return result;
        }

        private CreateQMS_TR_ABNORMAL convertDBToModel(ViewQMS_TR_ABNORMAL model)
        {
            CreateQMS_TR_ABNORMAL result = new CreateQMS_TR_ABNORMAL();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.DOC_STATUS = model.DOC_STATUS;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.VOLUME = model.VOLUME;
            result.CORRECT_DATA_TYPE = model.CORRECT_DATA_TYPE;
            result.CAUSE_DESC = (null == model.CAUSE_DESC) ? "" : model.CAUSE_DESC;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 

            return result;
        }

        private CreateQMS_TR_REDUCE_FEED convertDBToModel(ViewQMS_TR_REDUCE_FEED model)
        {
            CreateQMS_TR_REDUCE_FEED result = new CreateQMS_TR_REDUCE_FEED();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.DOC_STATUS = model.DOC_STATUS;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.CORRECT_DATA_TYPE = model.CORRECT_DATA_TYPE;
            result.CAUSE_DESC = (null == model.CAUSE_DESC) ? "" : model.CAUSE_DESC;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 

            return result;
        }

        private CreateQMS_TR_EXCEPTION convertDBToModel(ViewQMS_TR_EXCEPTION model)
        {
            CreateQMS_TR_EXCEPTION result = new CreateQMS_TR_EXCEPTION();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.DOC_STATUS = model.DOC_STATUS;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.VOLUME = model.VOLUME;
            result.CORRECT_DATA_TYPE = model.CORRECT_DATA_TYPE;
            result.CAUSE_DESC = (null == model.CAUSE_DESC) ? "" : model.CAUSE_DESC; 

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 

            return result;
        }

        private CreateQMS_TR_DOWNTIME convertDBToModel(ViewQMS_TR_DOWNTIME model)
        { 
            CreateQMS_TR_DOWNTIME result = new CreateQMS_TR_DOWNTIME();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.DOC_STATUS = model.DOC_STATUS;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.CORRECT_DATA_TYPE = model.CORRECT_DATA_TYPE;
            result.CAUSE_DESC = (null == model.CAUSE_DESC) ? "" : model.CAUSE_DESC;
             
            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 

            return result;
        }

        private CreateQMS_TR_CHANGE_GRADE convertDBToModel(ViewQMS_TR_CHANGE_GRADE model)
        {
            CreateQMS_TR_CHANGE_GRADE result = new CreateQMS_TR_CHANGE_GRADE();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.DOC_STATUS = model.DOC_STATUS;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.GRADE_FROM = model.GRADE_FROM;
            result.GRADE_TO = model.GRADE_TO;
            result.VOLUME = model.VOLUME;
            result.CORRECT_DATA_TYPE = model.CORRECT_DATA_TYPE;
            result.CAUSE_DESC = (null == model.CAUSE_DESC) ? "" : model.CAUSE_DESC;
        

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 

            return result;
        }

        private CreateQMS_TR_CROSS_VOLUME convertDBToModel(ViewQMS_TR_CROSS_VOLUME model)
        {
            CreateQMS_TR_CROSS_VOLUME result = new CreateQMS_TR_CROSS_VOLUME();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.PRODUCT_ID = model.PRODUCT_ID;
            result.DOC_STATUS = model.DOC_STATUS;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            //result.PRODUCT_FROM = model.PRODUCT_FROM;
            result.PRODUCT_TO = model.PRODUCT_TO;
            result.VOLUME = model.VOLUME;
            result.CORRECT_DATA_TYPE = model.CORRECT_DATA_TYPE;
            result.CAUSE_DESC = (null == model.CAUSE_DESC) ? "" : model.CAUSE_DESC; 

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER; 

            return result;
        }

        public CreateQMS_MA_PRODUCT_MAPPING getByPlantIdProductId(long plantID, long productId)
        {
            CreateQMS_MA_PRODUCT_MAPPING dtResult = new CreateQMS_MA_PRODUCT_MAPPING();

            try{

                string queryString = "SELECT * FROM QMS_MA_PRODUCT_MAPPING  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND PRODUCT_ID = @PRODUCT_ID " ;

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", plantID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", productId);

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        dtResult.ID = Convert.ToInt64(reader["ID"]);
                        dtResult.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        dtResult.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]);
                        dtResult.GRADE_ID =  Convert.ToInt64(reader["GRADE_ID"]);
                        dtResult.PRODUCT_DESC =  reader["PRODUCT_DESC"].ToString();
                        
                        dtResult.HIGHLOW_CHECK = Convert.ToBoolean(reader["HIGHLOW_CHECK"]);
                        break;
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return dtResult;
        }


        public List<CreateQMS_MA_EXQ_TAG> getQuantityTagAllByProductMapId(long mapId)
        {
            List<CreateQMS_MA_EXQ_TAG> listResult = new List<CreateQMS_MA_EXQ_TAG>();

            try
            { 

                string queryString = "SELECT * FROM QMS_MA_EXQ_TAG  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND PRODUCT_ID = @PRODUCT_ID AND TAG_TYPE IN ('" + (byte)EXA_TAG_TYPE.QUANTITY_PV + "','" + EXA_TAG_TYPE.QUANTITY_SUM + "')" 
                                + " ORDER BY POSITION ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", mapId); 

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        CreateQMS_MA_EXQ_TAG dtResult = new CreateQMS_MA_EXQ_TAG();
                        dtResult.ID = Convert.ToInt64(reader["ID"]);
                        dtResult.PRODUCT_MAPPING_ID = Convert.ToInt64(reader["PRODUCT_MAPPING_ID"]);
                        dtResult.UNIT_ID = Convert.ToInt64(reader["UNIT_ID"]);
                        dtResult.CONTROL_ID = Convert.ToInt64(reader["CONTROL_ID"]);
                        dtResult.SPEC_ID = Convert.ToInt64(reader["SPEC_ID"]);

                        dtResult.CHANGE_CONTROL_CHECK = Convert.ToByte(reader["CHANGE_CONTROL_CHECK"]);
                        dtResult.CHANGE_CONTROL_ID = Convert.ToInt64(reader["CHANGE_CONTROL_ID"]);
                        dtResult.CHANGE_SPEC_CHECK = Convert.ToByte(reader["CHANGE_SPEC_CHECK"]);
                        dtResult.CHANGE_SPEC_ID = Convert.ToInt64(reader["CHANGE_SPEC_ID"]); 
                        dtResult.EXCEL_NAME = reader["SPEC_ID"].ToString();

                        dtResult.EXA_TAG_NAME = reader["EXA_TAG_NAME"].ToString();
                        dtResult.TAG_TYPE = Convert.ToByte(reader["TAG_TYPE"]);
                        dtResult.TAG_FLOW_CONVERT_VALUE = Convert.ToDecimal(reader["TAG_FLOW_CONVERT_VALUE"]);
                        dtResult.TAG_FLOW_CHECK = Convert.ToByte(reader["TAG_FLOW_CHECK"]);
                        dtResult.TAG_FLOW_CHECK_VALUE = Convert.ToInt64(reader["TAG_FLOW_CHECK_VALUE"]); 

                        dtResult.TAG_TARGET_CHECK = Convert.ToByte(reader["TAG_TARGET_CHECK"]);
                        dtResult.TAG_TARGET_MIN = Convert.ToDecimal(reader["TAG_TARGET_MIN"]); 
                        dtResult.TAG_TARGET_MAX = Convert.ToDecimal(reader["TAG_TARGET_MAX"]); 
                        dtResult.TAG_CORRECT = Convert.ToByte(reader["TAG_CORRECT"]); 
                        dtResult.TAG_CORRECT_MIN = Convert.ToDecimal(reader["TAG_CORRECT_MIN"]); 
                        dtResult.TAG_CORRECT_MAX = Convert.ToDecimal(reader["TAG_CORRECT_MAX"]);
 
                        dtResult.POSITION = Convert.ToInt16(reader["POSITION"]);

                        listResult.Add(dtResult);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult;
        }

        public List<CreateQMS_TR_EXQ_PRODUCT> getEXQProductAllBySearchEx(ExaProductExSearchModel search)
        {

            List<CreateQMS_TR_EXQ_PRODUCT> listResult = new List<CreateQMS_TR_EXQ_PRODUCT>();

            try
            { 
                string listEXA_ID = "";

                if (search.TAG_EXA_ID.Count > 0)
                {
                    for (int i = 0; i < search.TAG_EXA_ID.Count; i++)
                    {
                        if (listEXA_ID == "")
                        {
                            listEXA_ID = "'" + search.TAG_EXA_ID[i].ToString() + "'";
                        }
                        else
                        {
                            listEXA_ID = " , '" + search.TAG_EXA_ID[i].ToString() + "'";
                        }
                    }
                }

                

                string queryString = "SELECT * FROM QMS_MA_EXQ_TAG  "
                                + " WHERE PLANT_ID = @PLANT_ID   "
                                + " AND PRODUCT_ID = @PRODUCT_ID "
                                + " AND TAG_DATE >= @START_DATE  "
                                + " AND TAG_DATE <= @END_DATE "; 
                if (listEXA_ID != "")
                {
                    queryString += " AND TAG_TYPE IN (" + listEXA_ID + ") ";
                }

                queryString += " ORDER BY TAG_DATE ";

                SqlConnection connection = getConnection();
                SqlCommand SQLcommand = new SqlCommand(queryString, connection);

                SQLcommand.Parameters.AddWithValue("@PLANT_ID", search.PLANT_ID);
                SQLcommand.Parameters.AddWithValue("@PRODUCT_ID", search.PRODUCT_ID);
                SQLcommand.Parameters.AddWithValue("@START_DATE", search.START_DATE);
                SQLcommand.Parameters.AddWithValue("@END_DATE", search.END_DATE); 

                connection.Open();

                SqlDataReader reader = SQLcommand.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        CreateQMS_TR_EXQ_PRODUCT dtResult = new CreateQMS_TR_EXQ_PRODUCT();
                        dtResult.ID = Convert.ToInt64(reader["ID"]);
                        dtResult.PLANT_ID = Convert.ToInt64(reader["PLANT_ID"]);
                        dtResult.PRODUCT_ID = Convert.ToInt64(reader["PRODUCT_ID"]); 
                        dtResult.TAG_DATE = Convert.ToDateTime(reader["TAG_DATE"].ToString());

                        dtResult.TAG_EXA_ID = Convert.ToInt64(reader["TAG_EXA_ID"]);
                        dtResult.TAG_VALUE = reader["TAG_VALUE"].ToString();
                        dtResult.TAG_CONVERT = reader["TAG_CONVERT"].ToString();
                        dtResult.TAG_GCERROR = reader["TAG_GCERROR"].ToString();

                        dtResult.STATUS_1 = Convert.ToByte(reader["STATUS_1"]); 
                        dtResult.STATUS_2 = Convert.ToByte(reader["STATUS_2"]);

                        listResult.Add(dtResult);
                    }

                    //result.Load(reader);
                }
                catch (Exception ex)
                {
                    msgError = ex.Message;
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return listResult; 
        }

        public decimal getVolumeByVoulmeEXQSearch(VoulmeEXQSearchModel search)
        {
            decimal volumeResult = 0;

            try
            { 
                DateTime currentStartTime;
                DateTime compareStartTime;
                DateTime searchStartTime;
                DateTime searchEndTime;
                CreateQMS_MA_EXQ_TAG tagQuantity = null;

                //Step 1.1 get Mapping id
                CreateQMS_MA_PRODUCT_MAPPING plantMapping = getByPlantIdProductId(search.PLANT_ID, search.PRODUCT_ID);

                if (null != plantMapping)
                {
                    List<CreateQMS_MA_EXQ_TAG> listQuantityTag = getQuantityTagAllByProductMapId(plantMapping.ID);


                    if (null != listQuantityTag && listQuantityTag.Count > 0)
                    {
                         
                        foreach (CreateQMS_MA_EXQ_TAG qtyTag in listQuantityTag)
                        {
                            if (qtyTag.TAG_TYPE == (byte)EXA_TAG_TYPE.QUANTITY_PV)
                            {
                                tagQuantity = qtyTag;
                            }
                        }

                        if (null != tagQuantity) //Found tag qty
                        {
                            ExaProductExSearchModel detailSearch = new ExaProductExSearchModel();
                            detailSearch.START_DATE = new DateTime(search.START_DATE.Year, search.START_DATE.Month, search.START_DATE.Day, 0, 0, 0);
                            detailSearch.END_DATE = new DateTime(search.END_DATE.Year, search.END_DATE.Month, search.END_DATE.Day, 0, 0, 0).AddDays(1);
                            detailSearch.PLANT_ID = search.PLANT_ID;
                            detailSearch.PRODUCT_ID = search.PRODUCT_ID;
                            detailSearch.TAG_EXA_ID = new List<long>();
                            detailSearch.TAG_EXA_ID.Add(tagQuantity.ID);
                            List<CreateQMS_TR_EXQ_PRODUCT> listVolumeProductEXQ = getEXQProductAllBySearchEx( detailSearch);

                            searchStartTime = new DateTime(search.START_DATE.Year, search.START_DATE.Month, search.START_DATE.Day, search.START_DATE.Hour, search.START_DATE.Minute, 0);
                            searchEndTime = new DateTime(search.END_DATE.Year, search.END_DATE.Month, search.END_DATE.Day, search.END_DATE.Hour, search.END_DATE.Minute, 0);

                            if (null != listVolumeProductEXQ && listVolumeProductEXQ.Count > 0)
                            {
                                foreach (CreateQMS_TR_EXQ_PRODUCT dtProduct in listVolumeProductEXQ)
                                {
                                    string[] TAG_CONVERT = dtProduct.TAG_CONVERT.Split(',');
                                    currentStartTime = new DateTime(dtProduct.TAG_DATE.Year, dtProduct.TAG_DATE.Month, dtProduct.TAG_DATE.Day, 0, 0, 0);

                                    for (int i = 0; i < TAG_CONVERT.Length; i++)
                                    {
                                        compareStartTime = currentStartTime.AddMinutes(2 * i); //ถูกแล้ว

                                        if (compareStartTime >= searchStartTime && compareStartTime <= searchEndTime)
                                        {
                                            volumeResult += getConvertTextToDecimal(TAG_CONVERT[i]); //Convert.ToDecimal(TAG_CONVERT[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return volumeResult;
        }

        public long SaveDOWNTIMEBySearch(DateTime START_DATE, DateTime END_DATE, ViewQMS_TR_DOWNTIME dtOffCal)
        {
            long nResult = 0;
            bool bResult = false;
            try
            {
                CreateQMS_TR_DOWNTIME tempCreate = new CreateQMS_TR_DOWNTIME();
                CreateQMS_TR_DOWNTIME tempCreate2 = new CreateQMS_TR_DOWNTIME();

                dtOffCal.START_DATE = resetSecond(dtOffCal.START_DATE);
                dtOffCal.END_DATE = resetSecond(dtOffCal.END_DATE);

                TimeSpan timeDiff = dtOffCal.END_DATE - dtOffCal.START_DATE;
                decimal minDiff = (decimal)timeDiff.TotalMinutes;
                //decimal volPerMin = (minDiff > 0) ? dtOffCal.VOLUME / minDiff : 0;
                VoulmeEXQSearchModel search = new VoulmeEXQSearchModel();


                search.PLANT_ID = dtOffCal.PLANT_ID;


                if (START_DATE > dtOffCal.START_DATE && END_DATE < dtOffCal.END_DATE)
                {
                    //Case 1. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ทั้งหมด แยก ออกเป็น 2 เส้น
                    tempCreate = new CreateQMS_TR_DOWNTIME();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate2 = new CreateQMS_TR_DOWNTIME();
                    tempCreate2 = convertDBToModel(dtOffCal);


                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น END_DATE;  
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE; 
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;


                    nResult = SaveQMS_TR_DOWNTIME(tempCreate);


                    tempCreate2.ID = 0; //สร้างใหม่
                    tempCreate2.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น START_DATE; 
                    search.START_DATE = tempCreate2.START_DATE;
                    search.END_DATE = tempCreate2.END_DATE;

                    nResult = SaveQMS_TR_DOWNTIME(tempCreate2);
                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                {
                    //Case 2. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ลบทิ้งทั้งอัน
                    bResult = DeleteQMS_TR_DOWNTIMEById(dtOffCal.ID);

                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE > END_DATE)
                {
                    //case 3. กรณีที่ start อยู่ใน ช่วง แต่ end แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_DOWNTIME();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น END_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    nResult = SaveQMS_TR_DOWNTIME(tempCreate);

                }
                //else if (START_DATE >= dtOffCal.START_DATE && END_DATE <= dtOffCal.END_DATE)
                else if (dtOffCal.START_DATE < START_DATE && dtOffCal.END_DATE <= END_DATE)// (dtOffCal.END_DATE > START_DATE && dtOffCal.END_DATE < END_DATE)
                {
                    //Case 4. กรณีที่ end อยู่ใน ช่วง แต่ start แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_DOWNTIME();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น START_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    nResult = SaveQMS_TR_DOWNTIME(tempCreate);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public long SaveCrossVolumeBySearch(DateTime START_DATE, DateTime END_DATE, ViewQMS_TR_CROSS_VOLUME dtOffCal)
        {
            long nResult = 0;
            bool bResult = false;
            try
            {
                CreateQMS_TR_CROSS_VOLUME tempCreate = new CreateQMS_TR_CROSS_VOLUME();
                CreateQMS_TR_CROSS_VOLUME tempCreate2 = new CreateQMS_TR_CROSS_VOLUME();

                dtOffCal.START_DATE = resetSecond(dtOffCal.START_DATE);
                dtOffCal.END_DATE = resetSecond(dtOffCal.END_DATE);

                TimeSpan timeDiff = dtOffCal.END_DATE - dtOffCal.START_DATE;
                decimal minDiff = (decimal)timeDiff.TotalMinutes;
                //decimal volPerMin = (minDiff > 0) ? dtOffCal.VOLUME / minDiff : 0;
                VoulmeEXQSearchModel search = new VoulmeEXQSearchModel();


                search.PLANT_ID = dtOffCal.PLANT_ID;


                if (START_DATE > dtOffCal.START_DATE && END_DATE < dtOffCal.END_DATE)
                {
                    //Case 1. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ทั้งหมด แยก ออกเป็น 2 เส้น
                    tempCreate = new CreateQMS_TR_CROSS_VOLUME();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate2 = new CreateQMS_TR_CROSS_VOLUME();
                    tempCreate2 = convertDBToModel(dtOffCal);


                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น END_DATE;  
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;

                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch( search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CROSS_VOLUME(tempCreate);


                    tempCreate2.ID = 0; //สร้างใหม่
                    tempCreate2.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น START_DATE;

                    search.START_DATE = tempCreate2.START_DATE;
                    search.END_DATE = tempCreate2.END_DATE;
                    tempCreate2.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CROSS_VOLUME(tempCreate2);
                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                {
                    //Case 2. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ลบทิ้งทั้งอัน
                    bResult = DeleteQMS_TR_CROSS_VOLUMEById(dtOffCal.ID);

                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE > END_DATE) //
                {
                    //case 3. กรณีที่ start อยู่ใน ช่วง แต่ end แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_CROSS_VOLUME();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น END_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CROSS_VOLUME(tempCreate);

                }
                //else if (START_DATE >= dtOffCal.START_DATE && END_DATE <= dtOffCal.END_DATE)
                else if (dtOffCal.START_DATE < START_DATE && dtOffCal.END_DATE <= END_DATE)//
                {
                    //Case 4. กรณีที่ end อยู่ใน ช่วง แต่ start แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_CROSS_VOLUME();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น START_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CROSS_VOLUME(tempCreate);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public long SaveABNORMALBySearch(DateTime START_DATE, DateTime END_DATE, ViewQMS_TR_ABNORMAL dtOffCal)
        {
            long nResult = 0;
            bool bResult = false;
            try
            {
                CreateQMS_TR_ABNORMAL tempCreate = new CreateQMS_TR_ABNORMAL();
                CreateQMS_TR_ABNORMAL tempCreate2 = new CreateQMS_TR_ABNORMAL();

                dtOffCal.START_DATE = resetSecond(dtOffCal.START_DATE);
                dtOffCal.END_DATE = resetSecond(dtOffCal.END_DATE);

                TimeSpan timeDiff = dtOffCal.END_DATE - dtOffCal.START_DATE;
                decimal minDiff = (decimal)timeDiff.TotalMinutes;
                //decimal volPerMin = (minDiff > 0) ? dtOffCal.VOLUME / minDiff : 0;
                VoulmeEXQSearchModel search = new VoulmeEXQSearchModel();


                search.PLANT_ID = dtOffCal.PLANT_ID;


                if (START_DATE > dtOffCal.START_DATE && END_DATE < dtOffCal.END_DATE)
                {
                    //Case 1. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ทั้งหมด แยก ออกเป็น 2 เส้น
                    tempCreate = new CreateQMS_TR_ABNORMAL();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate2 = new CreateQMS_TR_ABNORMAL();
                    tempCreate2 = convertDBToModel(dtOffCal);


                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น END_DATE;  
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;

                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_ABNORMAL(tempCreate);


                    tempCreate2.ID = 0; //สร้างใหม่
                    tempCreate2.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น START_DATE;

                    search.START_DATE = tempCreate2.START_DATE;
                    search.END_DATE = tempCreate2.END_DATE;
                    tempCreate2.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_ABNORMAL(tempCreate2);
                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                {
                    //Case 2. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ลบทิ้งทั้งอัน
                    bResult = DeleteQMS_TR_ABNORMALById(dtOffCal.ID);

                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE > END_DATE) //else if (dtOffCal.START_DATE > START_DATE && START_DATE < dtOffCal.END_DATE)
                {
                    //case 3. กรณีที่ start อยู่ใน ช่วง แต่ end แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_ABNORMAL();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น END_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_ABNORMAL(tempCreate);

                }
                //else if (START_DATE >= dtOffCal.START_DATE && END_DATE <= dtOffCal.END_DATE)
                else if (dtOffCal.START_DATE < START_DATE && dtOffCal.END_DATE <= END_DATE)//
                {
                    //Case 4. กรณีที่ end อยู่ใน ช่วง แต่ start แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_ABNORMAL();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น START_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_ABNORMAL(tempCreate);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public long SaveReduceFeedBySearch(DateTime START_DATE, DateTime END_DATE, ViewQMS_TR_REDUCE_FEED dtOffCal)
        {
            long nResult = 0;
            bool bResult = false;
            try
            {
                CreateQMS_TR_REDUCE_FEED tempCreate = new CreateQMS_TR_REDUCE_FEED();
                CreateQMS_TR_REDUCE_FEED tempCreate2 = new CreateQMS_TR_REDUCE_FEED();

                dtOffCal.START_DATE = resetSecond(dtOffCal.START_DATE);
                dtOffCal.END_DATE = resetSecond(dtOffCal.END_DATE);

                TimeSpan timeDiff = dtOffCal.END_DATE - dtOffCal.START_DATE;
                decimal minDiff = (decimal)timeDiff.TotalMinutes;
                //decimal volPerMin = (minDiff > 0) ? dtOffCal.VOLUME / minDiff : 0;
                VoulmeEXQSearchModel search = new VoulmeEXQSearchModel();


                search.PLANT_ID = dtOffCal.PLANT_ID;


                if (START_DATE > dtOffCal.START_DATE && END_DATE < dtOffCal.END_DATE)
                {
                    //Case 1. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ทั้งหมด แยก ออกเป็น 2 เส้น
                    tempCreate = new CreateQMS_TR_REDUCE_FEED();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate2 = new CreateQMS_TR_REDUCE_FEED();
                    tempCreate2 = convertDBToModel(dtOffCal);


                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น END_DATE;  
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;

                    //tempCreate.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_REDUCE_FEED(tempCreate);


                    tempCreate2.ID = 0; //สร้างใหม่
                    tempCreate2.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น START_DATE;

                    search.START_DATE = tempCreate2.START_DATE;
                    search.END_DATE = tempCreate2.END_DATE;
                    //tempCreate2.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_REDUCE_FEED(tempCreate2);
                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                {
                    //Case 2. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ลบทิ้งทั้งอัน
                    bResult = DeleteQMS_TR_REDUCE_FEEDById(dtOffCal.ID);

                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE > END_DATE) //else if (dtOffCal.START_DATE > START_DATE && START_DATE < dtOffCal.END_DATE)
                {
                    //case 3. กรณีที่ start อยู่ใน ช่วง แต่ end แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_REDUCE_FEED();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น END_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    //tempCreate.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_REDUCE_FEED(tempCreate);

                }
                //else if (START_DATE >= dtOffCal.START_DATE && END_DATE <= dtOffCal.END_DATE)
                else if (dtOffCal.START_DATE < START_DATE && dtOffCal.END_DATE <= END_DATE)//else if (dtOffCal.END_DATE > START_DATE && dtOffCal.END_DATE < END_DATE)
                {
                    //Case 4. กรณีที่ end อยู่ใน ช่วง แต่ start แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_REDUCE_FEED();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น START_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    //tempCreate.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_REDUCE_FEED(tempCreate);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public long SaveExceptionBySearch(DateTime START_DATE, DateTime END_DATE, ViewQMS_TR_EXCEPTION dtOffCal)
        {
            long nResult = 0;
            bool bResult = false;
            try
            {
                CreateQMS_TR_EXCEPTION tempCreate = new CreateQMS_TR_EXCEPTION();
                CreateQMS_TR_EXCEPTION tempCreate2 = new CreateQMS_TR_EXCEPTION();

                dtOffCal.START_DATE = resetSecond(dtOffCal.START_DATE);
                dtOffCal.END_DATE = resetSecond(dtOffCal.END_DATE);

                TimeSpan timeDiff = dtOffCal.END_DATE - dtOffCal.START_DATE;
                decimal minDiff = (decimal)timeDiff.TotalMinutes;
                //decimal volPerMin = (minDiff > 0) ? dtOffCal.VOLUME / minDiff : 0;
                VoulmeEXQSearchModel search = new VoulmeEXQSearchModel();


                search.PLANT_ID = dtOffCal.PLANT_ID;


                if (START_DATE > dtOffCal.START_DATE && END_DATE < dtOffCal.END_DATE)
                {
                    //Case 1. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ทั้งหมด แยก ออกเป็น 2 เส้น
                    tempCreate = new CreateQMS_TR_EXCEPTION();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate2 = new CreateQMS_TR_EXCEPTION();
                    tempCreate2 = convertDBToModel(dtOffCal);


                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น END_DATE;  
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;

                    //tempCreate.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_EXCEPTION(tempCreate);


                    tempCreate2.ID = 0; //สร้างใหม่
                    tempCreate2.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น START_DATE;

                    search.START_DATE = tempCreate2.START_DATE;
                    search.END_DATE = tempCreate2.END_DATE;
                    //tempCreate2.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_EXCEPTION(tempCreate2);
                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                {
                    //Case 2. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ลบทิ้งทั้งอัน
                    bResult = DeleteQMS_TR_EXCEPTIONById(dtOffCal.ID);

                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE > END_DATE) //else if (dtOffCal.START_DATE > START_DATE && START_DATE < dtOffCal.END_DATE)
                {
                    //case 3. กรณีที่ start อยู่ใน ช่วง แต่ end แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_EXCEPTION();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น END_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    //tempCreate.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_EXCEPTION(tempCreate);

                }
                //else if (START_DATE >= dtOffCal.START_DATE && END_DATE <= dtOffCal.END_DATE)
                else if (dtOffCal.START_DATE < START_DATE && dtOffCal.END_DATE <= END_DATE)//else if (dtOffCal.END_DATE > START_DATE && dtOffCal.END_DATE < END_DATE)
                {
                    //Case 4. กรณีที่ end อยู่ใน ช่วง แต่ start แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_EXCEPTION();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น START_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    //tempCreate.VOLUME = GetVolumeByVoulmeEXQSearch(db, search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_EXCEPTION(tempCreate);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public long SaveChangeGradeBySearch(DateTime START_DATE, DateTime END_DATE, ViewQMS_TR_CHANGE_GRADE dtOffCal)
        {
            long nResult = 0;
            bool bResult = false;
            try
            {
                CreateQMS_TR_CHANGE_GRADE tempCreate = new CreateQMS_TR_CHANGE_GRADE();
                CreateQMS_TR_CHANGE_GRADE tempCreate2 = new CreateQMS_TR_CHANGE_GRADE();

                dtOffCal.START_DATE = resetSecond(dtOffCal.START_DATE);
                dtOffCal.END_DATE = resetSecond(dtOffCal.END_DATE);

                TimeSpan timeDiff = dtOffCal.END_DATE - dtOffCal.START_DATE;
                decimal minDiff = (decimal)timeDiff.TotalMinutes;
                //decimal volPerMin = (minDiff > 0) ? dtOffCal.VOLUME / minDiff : 0;
                VoulmeEXQSearchModel search = new VoulmeEXQSearchModel();


                search.PLANT_ID = dtOffCal.PLANT_ID;


                if (START_DATE > dtOffCal.START_DATE && END_DATE < dtOffCal.END_DATE)
                {
                    //Case 1. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ทั้งหมด แยก ออกเป็น 2 เส้น
                    tempCreate = new CreateQMS_TR_CHANGE_GRADE();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate2 = new CreateQMS_TR_CHANGE_GRADE();
                    tempCreate2 = convertDBToModel(dtOffCal);


                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น END_DATE;  
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;

                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CHANGE_GRADE(tempCreate);


                    tempCreate2.ID = 0; //สร้างใหม่
                    tempCreate2.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น START_DATE;

                    search.START_DATE = tempCreate2.START_DATE;
                    search.END_DATE = tempCreate2.END_DATE;
                    tempCreate2.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CHANGE_GRADE(tempCreate2);
                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                {
                    //Case 2. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ลบทิ้งทั้งอัน
                    bResult = DeleteQMS_TR_CHANGE_GRADEById(dtOffCal.ID);

                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE > END_DATE) //(dtOffCal.START_DATE > START_DATE && START_DATE < dtOffCal.END_DATE)
                {
                    //case 3. กรณีที่ start อยู่ใน ช่วง แต่ end แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_CHANGE_GRADE();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น END_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CHANGE_GRADE(tempCreate);

                }
                //else if (START_DATE >= dtOffCal.START_DATE && END_DATE <= dtOffCal.END_DATE)
                else if (dtOffCal.START_DATE < START_DATE && dtOffCal.END_DATE <= END_DATE)//
                {
                    //Case 4. กรณีที่ end อยู่ใน ช่วง แต่ start แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_CHANGE_GRADE();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น START_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch( search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_CHANGE_GRADE(tempCreate);
                }

            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }

        public long SaveOffControlBySearch(DateTime START_DATE, DateTime END_DATE, ViewQMS_TR_OFF_CONTROL_CAL dtOffCal)
        {
            long nResult = 0;
            bool bResult = false;
            try
            {

                CreateQMS_TR_OFF_CONTROL_CAL tempCreate = new CreateQMS_TR_OFF_CONTROL_CAL();
                CreateQMS_TR_OFF_CONTROL_CAL tempCreate2 = new CreateQMS_TR_OFF_CONTROL_CAL();

                dtOffCal.START_DATE = resetSecond(dtOffCal.START_DATE);
                dtOffCal.END_DATE = resetSecond(dtOffCal.END_DATE);

                TimeSpan timeDiff = dtOffCal.END_DATE - dtOffCal.START_DATE;
                decimal minDiff = (decimal)timeDiff.TotalMinutes;
                decimal volPerMin = (minDiff > 0) ? dtOffCal.VOLUME / minDiff : 0;
                VoulmeEXQSearchModel search = new VoulmeEXQSearchModel();


                search.PLANT_ID = dtOffCal.PLANT_ID;
                search.PRODUCT_ID = dtOffCal.PRODUCT_ID;


                if (START_DATE > dtOffCal.START_DATE && END_DATE < dtOffCal.END_DATE)
                {
                    //Case 1. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ทั้งหมด แยก ออกเป็น 2 เส้น
                    tempCreate = new CreateQMS_TR_OFF_CONTROL_CAL();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate2 = new CreateQMS_TR_OFF_CONTROL_CAL();
                    tempCreate2 = convertDBToModel(dtOffCal);


                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น END_DATE;  
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;

                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_OFF_CONTROL_CAL(tempCreate);


                    tempCreate2.ID = 0; //สร้างใหม่
                    tempCreate2.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น START_DATE;

                    search.START_DATE = tempCreate2.START_DATE;
                    search.END_DATE = tempCreate2.END_DATE;

                    //timeDiff = tempCreate2.END_DATE - tempCreate2.START_DATE;
                    tempCreate2.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_OFF_CONTROL_CAL(tempCreate2);


                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                {
                    //Case 2. กรณีที่ อยู่ในช่วงเวลาที่ค้นหา ลบทิ้งทั้งอัน
                    bResult = DeleteQMS_TR_OFF_CONTROL_CALById(dtOffCal.ID);

                }
                //else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE <= END_DATE)
                else if (dtOffCal.START_DATE >= START_DATE && dtOffCal.END_DATE > END_DATE) //else if (dtOffCal.START_DATE > START_DATE && START_DATE < dtOffCal.END_DATE)
                {
                    //case 3. กรณีที่ start อยู่ใน ช่วง แต่ end แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_OFF_CONTROL_CAL();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.START_DATE = END_DATE.AddMinutes(2); // เปลี่ยนเป็น END_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;
                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_OFF_CONTROL_CAL(tempCreate);

                }
                //else if (START_DATE >= dtOffCal.START_DATE && END_DATE <= dtOffCal.END_DATE)
                else if (dtOffCal.START_DATE < START_DATE && dtOffCal.END_DATE <= END_DATE)// else if (dtOffCal.END_DATE > START_DATE && dtOffCal.END_DATE < END_DATE)
                {
                    //Case 4. กรณีที่ end อยู่ใน ช่วง แต่ start แยู่นอกช่วง Edit

                    tempCreate = new CreateQMS_TR_OFF_CONTROL_CAL();
                    tempCreate = convertDBToModel(dtOffCal);
                    tempCreate.END_DATE = START_DATE.AddMinutes(-2); // เปลี่ยนเป็น START_DATE;
                    //timeDiff = tempCreate.END_DATE - tempCreate.START_DATE;

                    search.START_DATE = tempCreate.START_DATE;
                    search.END_DATE = tempCreate.END_DATE;
                    tempCreate.VOLUME = getVolumeByVoulmeEXQSearch(search); //volPerMin * (decimal)timeDiff.TotalMinutes;
                    nResult = SaveQMS_TR_OFF_CONTROL_CAL(tempCreate);
                }
                 
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return nResult;
        }
        #endregion
    }
}
