﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using System.IO;
using QMSSystem.CoreDB.Helper;
using QMSSystem.Model;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.N_OffControlCal, PrivilegeModeEnum.N_OffSpecCal, PrivilegeModeEnum.D_OffControl, PrivilegeModeEnum.D_OffSpec }, DataAccess = DataAccessEnum.Authen)]
    public class OffSpecController : BaseController
    {
        //
        // GET: /OffSpec/
        public ActionResult DashboardSpec(long id = 0)
        {
            ConfigServices ConfigServices = new ConfigServices(_username);
            ReportServices ReportServices = new ReportServices(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_ST_SYSTEM_CONFIG createQMS_ST_SYSTEM_CONFIG = new CreateQMS_ST_SYSTEM_CONFIG();

            ReportOffControlSearchModel searchOffControl = new ReportOffControlSearchModel();
            ReportOffControlDetailSearchModel searchOffControlDetail = new ReportOffControlDetailSearchModel();
            ReportOffControlAttachSearchModel searchOffControlAttach = new ReportOffControlAttachSearchModel();
            ReportOffControlGraphSearchModel searchOffControlGraph = new ReportOffControlGraphSearchModel();
            ReportOffControlSummarySearchModel searchOffControlSummary = new ReportOffControlSummarySearchModel();

            searchOffControlDetail.RP_OFF_CONTROL_ID = id;
            searchOffControlGraph.RP_OFF_CONTROL_ID = id;
            searchOffControlAttach.RP_OFF_CONTROL_ID = id;
            searchOffControlSummary.RP_OFF_CONTROL_ID = id;
            

            List<ViewQMS_ST_SYSTEM_CONFIG> listData = new List<ViewQMS_ST_SYSTEM_CONFIG>();

            listData = ConfigServices.getAllQMS_ST_SYSTEM_CONFIG(_db);

            ViewQMS_ST_SYSTEM_CONFIG dataItem = listData.FirstOrDefault();

            if (null != dataItem)
            {
                createQMS_ST_SYSTEM_CONFIG = ConfigServices.getQMS_ST_SYSTEM_CONFIGById(_db, dataItem.ID);

                id = dataItem.REPORT_OFF_SPEC;
                searchOffControl.ID = id;
            }
            searchOffControl.OFF_CONTROL_TYPE = (byte)OFF_TYPE.SPEC;

            ViewBag.createQMS_ST_SYSTEM_CONFIG = createQMS_ST_SYSTEM_CONFIG;

            ViewBag.RP_OFF_CONROL_ID = id;

            ViewBag.listOffSpec = ReportServices.getQMS_RP_OFF_CONTROLList(_db, searchOffControl.OFF_CONTROL_TYPE);

            ViewBag.searchOffControl = searchOffControl;
            ViewBag.searchOffControlDetail = searchOffControlDetail;
            ViewBag.searchOffControlAttach = searchOffControlAttach;
            ViewBag.searchOffControlGraph = searchOffControlGraph;
            ViewBag.searchOffControlSummary = searchOffControlSummary;

            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.DateNow = getDummyDateTime();

            return View();
        }

        public JsonResult GetDashboardOffSpecSummary_T0(long RPOffSpecId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getDashboardOffControlSummary_T0(_db, RPOffSpecId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDashboardOffSpecSummary_T1(long RPOffSpecId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getDashboardOffControlSummary_T1(_db, RPOffSpecId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDashboardOffSpecSummary_T2(long RPOffSpecId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);

                try
                {
                    List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>> listData = new List<List<ViewQMS_RP_OFF_CONTROL_SUMMARY>>();
                    listData = reportServices.getDashboardOffControlSummary_T2(_db, RPOffSpecId);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void OffSpecDataExcel(long rp_off_control_id)
        {
            ReportServices ReportServices = new ReportServices(_username);

            ExcelPackage excel = ReportServices.OffSpecDataExcel(_db, rp_off_control_id);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.ReportOffSpec + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }

        public void OffSpecGraphDataExcel(long rp_off_control_id)
        {
            ReportServices ReportServices = new ReportServices(_username);

            ExcelPackage excel = ReportServices.OffControlGraphDataExcel(_db, rp_off_control_id);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.OffSpecSummaryReportGraph + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }


        public ActionResult ListManageOffSpec()//control ListRawDataExport.cshtml
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            OffControlCalSearchModel searchOffControlCal = new OffControlCalSearchModel();
            CreateQMS_TR_OFF_CONTROL_CAL createQMS_TR_OFF_CONTROL_CAL = new CreateQMS_TR_OFF_CONTROL_CAL();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();

            createQMS_TR_OFF_CONTROL_CAL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.SPEC;
            createQMS_TR_OFF_CONTROL_CAL.START_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.END_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.ROOT_CAUSE_ID = 1;//default;

            searchOffControlCal.OFF_CONTROL_TYPE = (byte)OFF_TYPE.SPEC;

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusListWithInitail();
            
            ViewBag.listDocStatusEx = this.getDocStatusList(); 
            ViewBag.searchOffControlCal = searchOffControlCal;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.createQMS_TR_OFF_CONTROL_CAL = createQMS_TR_OFF_CONTROL_CAL;
            ViewBag.volumeSearch = volumeSearch;

            return View();
        }

        public ActionResult CreateManageOffSpec(long id = 0)//control CresteRawDataExport.cshtml
        {
            TransactionService transactionServices = new TransactionService(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_TR_OFF_CONTROL_CAL createQMS_TR_OFF_CONTROL_CAL = new CreateQMS_TR_OFF_CONTROL_CAL();
            VoulmeEXQSearchModel volumeSearch = new VoulmeEXQSearchModel();
            string actionName = Resources.Strings.AddNew;

            createQMS_TR_OFF_CONTROL_CAL.OFF_CONTROL_TYPE = (byte)OFF_TYPE.SPEC;
            createQMS_TR_OFF_CONTROL_CAL.START_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.END_DATE = getDummyDateTime();
            createQMS_TR_OFF_CONTROL_CAL.ROOT_CAUSE_ID = 1;//default;
            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_TR_OFF_CONTROL_CAL = transactionServices.getQMS_TR_OFF_CONTROL_CALById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listRootCause = masterDataServices.getQMS_MA_ROOT_CAUSEActiveList(_db);
            ViewBag.listDocStatus = this.getDocStatusList();
           

            ViewBag.createQMS_TR_OFF_CONTROL_CAL = createQMS_TR_OFF_CONTROL_CAL;
            ViewBag.volumeSearch = volumeSearch;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_TR_OFF_CONTROL_CAL(CreateQMS_TR_OFF_CONTROL_CAL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    model.START_DATE = resetSecond(model.START_DATE);
                    model.END_DATE = resetSecond(model.END_DATE);
                    long nResult = transactionServices.SaveQMS_TR_OFF_CONTROL_CAL(_db, model);

                    if (nResult > 0)
                    {
                        model = transactionServices.getQMS_TR_OFF_CONTROL_CALById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_TR_OFF_CONTROL_CALBySearch(OffControlCalSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_OFF_CONTROL_CAL> listData = new List<ViewQMS_TR_OFF_CONTROL_CAL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transactionServices.searchQMS_TR_OFF_CONTROL_CAL(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_TR_OFF_CONTROL_CAL(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.DeleteQMS_TR_OFF_CONTROL_CALByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDocStatusQMS_TR_OFF_CONTROL_CAL(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_OFF_CONTROL_CALByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }



    }
}
