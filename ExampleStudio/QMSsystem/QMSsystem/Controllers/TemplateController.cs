﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using System.IO;
using QMSsystem.Models.Helper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QMSsystem.Models;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_TemplateGas, PrivilegeModeEnum.M_TemplateUtility
     , PrivilegeModeEnum.N_ImpExpTemplate, PrivilegeModeEnum.N_TemplateCOA, PrivilegeModeEnum.N_TemplateGas, 
        PrivilegeModeEnum.N_TemplateUtility, PrivilegeModeEnum.M_TemplateEmission, PrivilegeModeEnum.M_TemplateClarifiedSystem,
        PrivilegeModeEnum.M_TemplateHotOilFlashPoint,PrivilegeModeEnum.M_TemplateHotOilPhysical,PrivilegeModeEnum.M_TemplateSulfurInGasMonthly,
        PrivilegeModeEnum.M_TemplateHgInPLMonthly, PrivilegeModeEnum.M_TemplateHgInPL, PrivilegeModeEnum.M_TemplateHgOutletMRU,PrivilegeModeEnum.M_TemplateSulfurInGasWeekly,
        PrivilegeModeEnum.M_TemplateAcidOffGas,PrivilegeModeEnum.M_TemplateHgStab, PrivilegeModeEnum.M_TemplateEmission,
        PrivilegeModeEnum.M_TemplateObservePonds, PrivilegeModeEnum.M_TemplateOilyWaters}, DataAccess = DataAccessEnum.Authen)]
    public class TemplateController : BaseController
    {
        //
        // GET: /Template/

        public ActionResult ImportExportEXQTemplate()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            return View();
        }
 
        public ActionResult ListMasterTemplateGAS()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate  = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();

            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);// .getQMS_MA_UNITActiveList(_db);
            //List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listExcel = masterDataServices.getTemplateExcelFile(_db, id);
            //List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, id);
            //List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, id);

            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.GAS);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listPlant = listPlant;
            ViewBag.listUnit = listUnit;
            //ViewBag.listExcel = listExcel;
            //ViewBag.listSample = listSample;
            //ViewBag.listItem = listItem;

            return View();
        }

        public ActionResult CreateMasterTemplateGAS(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE(); 
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
             
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);// .getQMS_MA_UNITActiveList(_db);
            List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listExcel = masterDataServices.getTemplateExcelFile(_db, id);
            List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, id);
            List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, id);
             

            string actionName = Resources.Strings.AddNew;

             
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_TEMPLATE = masterDataServices.getQMS_MA_TEMPLATEById(_db, id); 
            }

            createQMS_MA_TEMPLATE_DATA.ROW_DATA = new CreateQMS_MA_TEMPLATE_ROW();
            createQMS_MA_TEMPLATE_DATA.COLUMN_DATA = new CreateQMS_MA_TEMPLATE_COLUMN();
            createQMS_MA_TEMPLATE_DATA.CONTROL_DATA = new CreateQMS_MA_TEMPLATE_CONTROL();

            createQMS_MA_TEMPLATE_DATA.ROW_DATA.TEMPLATE_ID = id;
            createQMS_MA_TEMPLATE_DATA.COLUMN_DATA.TEMPLATE_ID = id;
            createQMS_MA_TEMPLATE_DATA.CONTROL_DATA.TEMPLATE_ID = id;

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.GAS);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listPlant = listPlant;
            ViewBag.listUnit = listUnit;
            ViewBag.listExcel = listExcel;
            ViewBag.listSample = listSample;
            ViewBag.listItem = listItem;

            ViewBag.ActionName = actionName;
            return View();
        }


        public ActionResult ListMasterTemplateUtility()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();

            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

             
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);//getQMS_MA_UNITActiveList(_db);
            //List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listExcel = masterDataServices.getTemplateExcelFile(_db, id);
            //List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, id);
            //List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, id);


            string actionName = Resources.Strings.AddNew;


            

            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.Utility);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            //ViewBag.listPlant = listPlant;
            ViewBag.listUnit = listUnit;
            //ViewBag.listExcel = listExcel;
            //ViewBag.listSample = listSample;
            //ViewBag.listItem = listItem;

            return View();
        }

        public ActionResult CreateMasterTemplateUtility(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();

            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            //List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);//getQMS_MA_UNITActiveList(_db);
            List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listExcel = masterDataServices.getTemplateExcelFile(_db, id);
            List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, id);
            List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, id);


            string actionName = Resources.Strings.AddNew;


            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_MA_TEMPLATE = masterDataServices.getQMS_MA_TEMPLATEById(_db, id);
            }

            createQMS_MA_TEMPLATE_DATA.ROW_DATA = new CreateQMS_MA_TEMPLATE_ROW();
            createQMS_MA_TEMPLATE_DATA.COLUMN_DATA = new CreateQMS_MA_TEMPLATE_COLUMN();
            createQMS_MA_TEMPLATE_DATA.CONTROL_DATA = new CreateQMS_MA_TEMPLATE_CONTROL();

            createQMS_MA_TEMPLATE_DATA.ROW_DATA.TEMPLATE_ID = id;
            createQMS_MA_TEMPLATE_DATA.COLUMN_DATA.TEMPLATE_ID = id;
            createQMS_MA_TEMPLATE_DATA.CONTROL_DATA.TEMPLATE_ID = id;

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.Utility);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            //ViewBag.listPlant = listPlant;
            ViewBag.listUnit = listUnit;
            ViewBag.listExcel = listExcel;
            ViewBag.listSample = listSample;
            ViewBag.listItem = listItem;

            ViewBag.ActionName = actionName;
            return View();
        }

        public ActionResult ListMasterTemplateEmission()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();

            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();


            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);//getQMS_MA_UNITActiveList(_db);
            //List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listExcel = masterDataServices.getTemplateExcelFile(_db, id);
            //List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, id);
            //List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, id);


            string actionName = Resources.Strings.AddNew;




            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Emission;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Emission;

            searchTemplate.TEMPLATE_AREA = "";
            createQMS_MA_TEMPLATE.TEMPLATE_AREA = "";

            createQMS_MA_TEMPLATE.EXCEL_DATE = "";

            createQMS_MA_TEMPLATE.EXCEL_TIME = "";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.EMISSION);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            //ViewBag.listPlant = listPlant;
            ViewBag.listUnit = listUnit;
            //ViewBag.listExcel = listExcel;
            //ViewBag.listSample = listSample;
            //ViewBag.listItem = listItem;

            return View();
        }

        public ActionResult ListMasterTemplateClarifiedSystem()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();

            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();


            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);//getQMS_MA_UNITActiveList(_db);
            //List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listExcel = masterDataServices.getTemplateExcelFile(_db, id);
            //List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, id);
            //List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, id);


            string actionName = Resources.Strings.AddNew;




            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.ClarifiedSystem;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.ClarifiedSystem;

            searchTemplate.TEMPLATE_AREA = "Clarified System";
            createQMS_MA_TEMPLATE.TEMPLATE_AREA = "Clarified System";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.ClarifiedSystem);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            //ViewBag.listPlant = listPlant;
            ViewBag.listUnit = listUnit;
            //ViewBag.listExcel = listExcel;
            //ViewBag.listSample = listSample;
            //ViewBag.listItem = listItem;

            return View();
        }

        public ActionResult ListMasterTemplateWatseWater()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();

            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();


            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);//getQMS_MA_UNITActiveList(_db);
            //List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listExcel = masterDataServices.getTemplateExcelFile(_db, id);
            //List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, id);
            //List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, id);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();

            string actionName = Resources.Strings.AddNew;




            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.WatseWater;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.WatseWater;

            //searchTemplate.TEMPLATE_AREA = "WatseWater";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "WatseWater";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.WatseWater);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            //ViewBag.listPlant = listPlant;
            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;
            //ViewBag.listExcel = listExcel;
            //ViewBag.listSample = listSample;
            //ViewBag.listItem = listItem;

            return View();
        }


        public ActionResult ListMasterTemplateObservePonds() 
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.ObservePonds;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.ObservePonds;
            //searchTemplate.TEMPLATE_AREA = "SulfurInGasMonthly";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "SulfurInGasMonthly";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.ObservePonds);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateOilyWaters()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.OilyWaters;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.OilyWaters;

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.OilyWaters);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateHotOilFlashPoint()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilFlashPoint;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilFlashPoint;
            //searchTemplate.TEMPLATE_AREA = "HotOilFlashPoint";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "HotOilFlashPoint";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.HotOilFlashPoint);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateHotOilPhysical()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilPhysical;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilPhysical;
            //searchTemplate.TEMPLATE_AREA = "HotOilPhysical";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "HotOilPhysical";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.HotOilPhysical);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateHgInPLMonthly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPLMonthly;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPLMonthly;
            //searchTemplate.TEMPLATE_AREA = "HgInPLMonthly";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "HgInPLMonthly";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.HgInPLMonthly);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateHgInPL()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPL;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPL;
            //searchTemplate.TEMPLATE_AREA = "HgInPL";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "HgInPL";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.HgInPL);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateHgOutletMRU()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgOutletMRU;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgOutletMRU;
            //searchTemplate.TEMPLATE_AREA = "HgOutletMRU";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "HgOutletMRU";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.HgOutletMRU);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateSulfurInGasMonthly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasMonthly;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasMonthly;
            //searchTemplate.TEMPLATE_AREA = "SulfurInGasMonthly";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "SulfurInGasMonthly";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.SulfurInGasMonthly);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateSulfurInGasWeekly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            //searchTemplate.TEMPLATE_AREA = "SulfurInGasWeekly";
            //createQMS_MA_TEMPLATE.TEMPLATE_AREA = "SulfurInGasWeekly";

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.SulfurInGasWeekly);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }

        public ActionResult ListMasterTemplateTwoHundredThousandPond()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.TwoHundredThousandPond;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.TwoHundredThousandPond;

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.TwoHundredThousandPond);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateHgStab()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgStab;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgStab;

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.HgStab);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }
        public ActionResult ListMasterTemplateAcidOffGas()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearchModel searchTemplate = new TemplateSearchModel();
            CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = new CreateQMS_MA_TEMPLATE();
            CreateQMS_MA_TEMPALTE_EXCEL createQMS_MA_TEMPLATE_DATA = new CreateQMS_MA_TEMPALTE_EXCEL();
            TemplateRowSearchModel searchSample = new TemplateRowSearchModel();
            TemplateColumnSearchModel searchItem = new TemplateColumnSearchModel();
            TemplateControlDataSearchModel searchControlValue = new TemplateControlDataSearchModel();

            List<ViewQMS_MA_UNIT> listUnit = masterDataServices.getQMS_MA_UNITList(_db);
            List<ViewAlignmentOption> ListAlignment = masterDataServices.getAlignmentOption();
            List<ViewQMS_MA_PLANT> listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);

            string actionName = Resources.Strings.AddNew;
            searchTemplate.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.AcidOffGas;
            createQMS_MA_TEMPLATE.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.AcidOffGas;

            ViewBag.listPlant = listPlant;
            ViewBag.searchTemplate = searchTemplate;
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.createQMS_MA_TEMPLATE = createQMS_MA_TEMPLATE;
            ViewBag.createQMS_MA_TEMPLATE_DATA = createQMS_MA_TEMPLATE_DATA;

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.AcidOffGas);

            ViewBag.searchSample = searchSample;
            ViewBag.searchItem = searchItem;
            ViewBag.searchControlValue = searchControlValue;

            ViewBag.listUnit = listUnit;
            ViewBag.ListAlignment = ListAlignment;

            return View();
        }

        public ActionResult ListTemplateGAS()
        {
            return View();
        }

        public ActionResult CreateTemplateGAS(long id = 0)
        {
            string actionName = Resources.Strings.AddNew;


            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
            }

            ViewBag.ActionName = actionName;
            return View();
        }

        public ActionResult ListTemplateUtility()
        {
            return View();
        }

        public ActionResult CreateTemplateUtility(long id = 0)
        {
            string actionName = Resources.Strings.AddNew;


            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
            }

            ViewBag.ActionName = actionName;
            return View();
        }

        public ActionResult ListTemplateCOA()
        {
            return View();
        }

        public ActionResult CreateTemplateCOA(long id = 0)
        {
            string actionName = Resources.Strings.AddNew;


            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
            }

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_MA_TEMPLATE(CreateQMS_MA_TEMPLATE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool bCheckDuplicate = masterDataServices.checkDuplicateTemplateName(_db, model);

                    if (bCheckDuplicate)
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.DuplicateTemplateName };
                    }
                    else
                    { 
                        long nResult = masterDataServices.SaveQMS_MA_TEMPLATE(_db, model);

                        if (nResult > 0)
                        {
                            model = masterDataServices.getQMS_MA_TEMPLATEById(_db, nResult);
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = model };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateQMS_MA_TEMPLATE_DATA(CreateQMS_MA_TEMPALTE_EXCEL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_TEMPLATE_DATA(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTemplateExcelData(long templateId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listData = new List<List<ViewQMS_MA_TEMPALTE_EXCEL>>();
                    listData = masterDataServices.getTemplateExcelFile(_db, templateId);

                    List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, templateId);
                    List<ViewQMS_MA_TEMPLATE_ROW> tempListSample = new List<ViewQMS_MA_TEMPLATE_ROW>();
                    CreateQMS_MA_TEMPLATE template = masterDataServices.getQMS_MA_TEMPLATEById(_db, templateId);
                    if (template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasWeekly || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasMonthly
                        || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.ObservePonds || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.OilyWaters 
                        || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.Emission)
                    {
                        foreach (ViewQMS_MA_TEMPLATE_ROW dt in listSample)
                        {
                            var findNameInTemp = tempListSample.Where(a => a.NAME == dt.NAME && a.AREA_NAME == dt.AREA_NAME).ToList();
                            if (findNameInTemp.Count > 0)
                            {
                                continue;
                            }
                            else
                            {
                                tempListSample.Add(dt);
                            }
                        }
                    }
                    else if(template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgStab || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.AcidOffGas)
                    {
                        tempListSample = listSample;
                    }
                    else
                    {
                        foreach (ViewQMS_MA_TEMPLATE_ROW dt in listSample)
                        {
                            var findNameInTemp = tempListSample.Where(a => a.NAME == dt.NAME).ToList();
                            if (findNameInTemp.Count > 0)
                            {
                                continue;
                            }
                            else
                            {
                                tempListSample.Add(dt);
                            }
                        }
                    }
                    List<ViewQMS_MA_TEMPLATE_ROW> tempListSampleArea = new List<ViewQMS_MA_TEMPLATE_ROW>();
                    foreach (ViewQMS_MA_TEMPLATE_ROW dt in listSample)
                    {
                        var findAreaNameInTemp = tempListSampleArea.Where(a => a.AREA_NAME == dt.AREA_NAME).ToList();
                        if (findAreaNameInTemp.Count > 0)
                        {
                            continue;
                        }
                        else
                        {
                            tempListSampleArea.Add(dt);
                        }
                    }
                    List <ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, templateId);
                    List<ViewQMS_MA_TEMPLATE_COLUMN> listSampleItem = new List<ViewQMS_MA_TEMPLATE_COLUMN>();
                    foreach (ViewQMS_MA_TEMPLATE_ROW dt in listSample)
                    {
                        if (dt.ITEM_NAME != null && dt.ITEM_NAME != "")
                        {
                            var findNameInTempItem = listSampleItem.Where(a => a.NAME == dt.ITEM_NAME).ToList();
                            if (findNameInTempItem.Count > 0)
                            {
                                continue;
                            }
                            else
                            {
                                ViewQMS_MA_TEMPLATE_COLUMN md = new ViewQMS_MA_TEMPLATE_COLUMN();
                                md.ID = dt.ID;
                                md.TEMPLATE_ID = dt.TEMPLATE_ID;
                                md.EXCEL_FIELD = dt.EXCEL_FIELD_ITEM;
                                md.UNIT_ID = dt.ITEM_UNIT.Value;
                                md.UNIT_NAME = dt.ITEM_UNIT_NAME;
                                md.NAME = dt.ITEM_NAME;
                                listSampleItem.Add(md);
                            }
                        }
                    }
                    
                    result = new { result = ReturnStatus.SUCCESS, message = listData, listSample = tempListSample, listItem = listItem, listSampleItem= listSampleItem, listSampleArea = tempListSampleArea };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return new JsonResult
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_TEMPLATE_DATA(CreateQMS_MA_TEMPALTE_EXCEL model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_TEMPLATE_DATA(_db, model);

                    if (nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_TEMPLATE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_TEMPLATE_DATAByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetQMS_MA_TEMPLATEBySearch(TemplateSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username); 

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_MA_TEMPLATE> listData = new List<ViewQMS_MA_TEMPLATE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = masterDataServices.searchQMS_MA_TEMPLATE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet] 
        public void GetTemplateExcelDataPDF(long templateId, int bDownload = 0)
        {
            MemoryStream ms = new MemoryStream();
            PrintPDFHeader ePdf = new PrintPDFHeader();
            Phrase phrase = new Phrase();
            Document pdfDoc = new Document();//(new Rectangle(288f, 144f), 22, 22, 75, 70); 
            pdfDoc.SetMargins(22, 22, 75, 40); 
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A3.Rotate());


            #region getData 

            #endregion

            
            try
            {
                initialFont();

                #region getData

                MasterDataService masterDataServices = new MasterDataService(_username);
                List<List<ViewQMS_MA_TEMPALTE_EXCEL>> listResult = masterDataServices.getTemplateExcelFile(_db, templateId);
                List<ViewQMS_MA_TEMPLATE_ROW> listSample = masterDataServices.getQMS_MA_TEMPLATE_ROWListById(_db, templateId);
                List<ViewQMS_MA_TEMPLATE_COLUMN> listItem = masterDataServices.getQMS_MA_TEMPLATE_COLUMNListById(_db, templateId);

                CreateQMS_MA_TEMPLATE createQMS_MA_TEMPLATE = masterDataServices.getQMS_MA_TEMPLATEById(_db, templateId); 
 
                #endregion


                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);
               

                #region setHeader
                PdfPTable head = new PdfPTable(9);
                float[] widths = new float[] { 22f, 30f, 75f, 50f, 70f, 70f, 70f, 70f, 116f };
                head.SetWidths(widths);
                head.WidthPercentage = 100;
                 

                Rectangle page = pdfDoc.PageSize;
                head.TotalWidth = page.Width - 22f; //magin right 
                head.HorizontalAlignment = Element.ALIGN_CENTER;
                 
                PdfPCell dataCol1 = new PdfPCell(); 

                String szHead = @Resources.Strings.TemplateGAS + "\n";
                szHead += createQMS_MA_TEMPLATE.NAME + "\n\n";

                Paragraph p = new Paragraph(szHead, fnt14);
                dataCol1 = new PdfPCell(p);
                dataCol1.Colspan = 9;
                dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                dataCol1.Border = PdfPCell.NO_BORDER;
                head.HorizontalAlignment = Element.ALIGN_CENTER;
                head.AddCell(dataCol1);
                head.CompleteRow();
                 
                writer.PageEvent = ePdf;

                float nTop = pdfDoc.GetTop(0) + 40f;
                //ePdf.ImageHeader = image;
                ePdf.ScalePercent = 50f;
                ePdf.xImage = 20f;
                ePdf.yImage = nTop;

                ePdf.fontHeader = fntHeader;
                ePdf.head = head;
                ePdf.bsFont = bf;

                #endregion
                pdfDoc.Open();

                try
                { 

                    if (listResult.Count() > 0)
                    {
                        int maxWidth = 28;
                        int count = 0;
                        PdfPTable body = new PdfPTable(maxWidth); //820/42
                        List<float> listWidth = new List<float>();
                        for (int col = 0; col < maxWidth; col++)
                        {
                            listWidth.Add((float)(1640/maxWidth));
                        }
                        float[] widths2 = listWidth.ToArray();
                        body.SetWidths(widths2);
                        body.WidthPercentage = 100;
                       

                        foreach (List<ViewQMS_MA_TEMPALTE_EXCEL> listData in listResult)
                        {
                            count = 0;
                            foreach( ViewQMS_MA_TEMPALTE_EXCEL dt in listData){

                                if (count == maxWidth) break;//exit foreach 

                                if (dt.ITEM_TYPE == 1  ) 
                                { 
                                    dataCol1 = new PdfPCell(new Phrase(dt.TD_NAME, fnt12White));
                                    dataCol1.BackgroundColor = bHeaderExcel; 
                                }else if( dt.ITEM_TYPE == 2 ){
                                    dataCol1 = new PdfPCell(new Phrase(dt.TD_NAME, fnt12White));
                                    dataCol1.BackgroundColor = bHeaderExcel2; 
                                }
                                else
                                {
                                    if (dt.CONTROL_DATA.SHOW_ON_TREND_FLAG == true )
                                    {
                                        dataCol1 = new PdfPCell(new Phrase(dt.TD_NAME, fnt12));
                                        dataCol1.BackgroundColor = bGreen;
                                    }else if (dt.DATA_TYPE == 2)
                                    {
                                        dataCol1 = new PdfPCell(new Phrase(dt.TD_NAME, fnt12White));
                                        dataCol1.BackgroundColor = bHeaderExcel;
                                    }else if (dt.DATA_TYPE == 1 && (dt.TD_NAME != null && dt.TD_NAME != ""))
                                    {
                                        dataCol1 = new PdfPCell(new Phrase(dt.TD_NAME, fnt12White));
                                        dataCol1.BackgroundColor = bHeaderExcel2;
                                    }else{
                                        dataCol1 = new PdfPCell(new Phrase(dt.TD_NAME, fnt12));
                                    }
                                     
                                }

                                dataCol1.HorizontalAlignment = Element.ALIGN_CENTER;
                                
                                body.AddCell(dataCol1);
                                count++;
                            } 
                            body.CompleteRow();  
                        }
                        body.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(body);

                    }
                    else
                    {
                        phrase.Add(new Chunk("No data :" + listResult.Count()));
                        pdfDoc.Add(phrase);
                    }
                }
                catch (Exception ex)
                {
                    phrase.Add(new Chunk(ex.Message));
                    pdfDoc.Add(phrase);
                } 
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();

            }
            catch (Exception ex)
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);
                pdfDoc.Open();
                phrase.Add(new Chunk(ex.Message));
                pdfDoc.Add(phrase);
                // Close document
                pdfDoc.Close();

                //Close Writer;
                writer.Close();
            }



            // กำหนด ContentType เป็น application/pdf เพื่อ Response เป็น Pdf
            Response.ContentType = "application/pdf";

            // กำหนดชื่อไฟล์ที่ต้องการ Export
            if (bDownload > 0)
                Response.AddHeader("content-disposition", "attachment; filename=" + "UserReport_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf");
            // Export Pdf
            Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            Response.End();
        }

        public void TemplateDataExcel(int downloadType, long plantId, long productId)
        {
            ReportServices ReportServices = new ReportServices(_username);
            string filename = @Resources.Strings.ImportExportEXQTemplate;
             

            ExcelPackage excel = ReportServices.TemplateDataExcel(_db, downloadType, plantId,  productId);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + filename + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray()); 
        }
    }


}
