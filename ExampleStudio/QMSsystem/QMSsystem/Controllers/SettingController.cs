﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using QMSSystem.Model;
using System.Net.Mail;
using QMSSystem.ExaDB;
using System.Configuration;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using QMSsystem.Models.Privilege;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace QMSsystem.Controllers
{ 
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] {  PrivilegeModeEnum.S_Authen,
                  PrivilegeModeEnum.S_Home, PrivilegeModeEnum.S_Abnormal, PrivilegeModeEnum.S_ExportData
                , PrivilegeModeEnum.S_ReportOff, PrivilegeModeEnum.S_ExtraSchedule, PrivilegeModeEnum.S_TrendHightlight
                , PrivilegeModeEnum.S_Connect, PrivilegeModeEnum.S_Log}, DataAccess = DataAccessEnum.Authen)]
    public class SettingController : BaseController
    {
        //
        // GET: /Setting/
        public void DownloadImage()
        {
            UserService usrService = new UserService(_username);
            

            

            //DirectoryEntry de = new DirectoryEntry();
            //de.Path = "LDAP://";

            //DirectorySearcher search = new DirectorySearcher();
            //search.SearchRoot = de;
            //search.Filter = "(&(objectClass=user)(objectCategory=person)(sAMAccountName=" + myUser + "))";
            //search.PropertiesToLoad.Add("samaccountname");
            //search.PropertiesToLoad.Add("thumbnailPhoto");

            //SearchResult user;
            //user = search.FindOne();

            //String userName;

            //if (user == null)
            //{
            //    Response.Redirect("app_graphics/user.jpg");
            //}
            //else
            //    userName = (String)user.Properties["sAMAccountName"][0];

            try
            {
                Response.ContentType = "image/jpeg";
                Response.Clear();
                Response.BufferOutput = true;
                Image testImage = usrService.GetUserPicture("sp590020", "ThaiLand4%0");

                bool bResult = usrService.IsCheckUserExistEx("sp590020", "ThaiLand4%0");

                if (bResult)
                {
                    usrService.writeErrorLog("Login OK ");
                }
                else
                {
                    usrService.writeErrorLog("Login Error ");
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    testImage.Save(ms, ImageFormat.Bmp);
                    //ms.ToArray();
                    Response.BinaryWrite( ms.ToArray());
                    Response.Flush();
                }
                
            }
            catch(Exception ex)
            {
                usrService.writeErrorLog(ex.Message);
                ViewBag.Message = ex.Message;
                //Response.Redirect("app_graphics/user.jpg");
                View("sendMail");
            }
        }


        public ActionResult convertCPK()
        {
            try
            {
                MasterDataService masterDataServices = new MasterDataService(_username);
                ConvertEventRawDataToCPKModel model = new ConvertEventRawDataToCPKModel();
                model.START_DATE = new DateTime(2023, 12, 25);
                model.END_DATE = new DateTime(2023, 12, 27);


                ViewBag.Message  = masterDataServices.convertEventRawDataToCPKTempData(_db, model); 
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }

            return View();
        }

        public ActionResult sendMail()
        {
            try
            {
               //// MailMessage mail = new MailMessage("GSP-LAB-REQUEST@pttplc.com", "nalinee.t@pttplc.com");                
               //// client.Port = 587;
               // //client.DeliveryMethod = SmtpDeliveryMethod.Network;
               // //client.UseDefaultCredentials = true;

               // MailMessage mail = new MailMessage("GSP-LAB-REQUEST@pttplc.com", "zkrittiya.b@pttict.com");
               // SmtpClient client = new SmtpClient();
               // client.Host = "e2k10-hubrelay.ptt.corp";
               // mail.Subject = "ทดสอบ this is a test email.";
               // mail.Body = "<h3>ทดสอบ</h3> this is my test email body";
               // client.Send(mail);

               // ViewBag.Message = "Can send mail";

                TransactionService transServices = new TransactionService(_username);

                bool bResult = transServices.ReadTemplateFileDataToDatabaseByTemplateType(_db, (byte)TEMPLATE_TYPE.GAS);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                
                ViewBag.Message = ex.Message;
            }

            return View();
        }

        public ActionResult TestLogin(string username="sp590020", string password="ThaiLand4%0")
        {
            try
            {
                UserService usrService = new UserService(_username);
                bool  bResult = usrService.IsCheckUserExistEx(username, password);



                if (true == bResult)
                {
                    ViewBag.Message = "Login Success";
                }
                else
                {
                    ViewBag.Message = "Login Failure";
                }

                
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }

            return View("sendMail");
        }

        public ActionResult LoadCOAData()
        {
            try
            {
                //DateTime startDate = new DateTime(2017, 10, 10, 0, 0, 0);
                //DateTime endDate = new DateTime(2017, 10, 11, 0, 0, 0).AddDays(1).AddMilliseconds(-1);

                DateTime startDate = new DateTime(2023, 03, 05, 0, 0, 0);
                DateTime endDate = new DateTime(2023, 03, 06, 0, 0, 0).AddDays(1).AddMilliseconds(-1);

                DateTime startTime = startDate;
                DateTime endTime = startDate.AddDays(1);
                COAServices coaService = new COAServices(_db, _username);
             
                while (startTime <= endDate)
                {
                    endTime = startTime.AddDays(1);
                    coaService.DailyCOAMasterDataCheck(startTime, endTime);
                    startTime = startTime.AddDays(1);
                }
                //SpecControlValue specControl = new SpecControlValue();
                //specControl = masterDataServices.convertStringToDecimal("REPORT");
                //specControl = masterDataServices.convertStringToDecimal("No.1 MAX");
                //specControl = masterDataServices.convertStringToDecimal("+20 Min");
                //specControl = masterDataServices.convertStringToDecimal("0.05 MAX");
                //specControl = masterDataServices.convertStringToDecimal("0.6000 - 0.7000");
                //specControl = masterDataServices.convertStringToDecimal("1 Max***");
                //specControl = masterDataServices.convertStringToDecimal("91.60 Min");
                //specControl = masterDataServices.convertStringToDecimal("FOUND");
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }
            return View("sendMail");
        }


        public ActionResult CalDowntime()
        {
            try
            {
                //ID	NAME	ABBREVIATION
                //3	GSP1	G1
                //4	GSP2	G2
                //5	GSP3	G3
                //6	GSP5	G5
                //7	GSP6	G6
                //8	ESP	ESP
                //9	STAB	test
                //cal Downtime

                TransactionService transService = new TransactionService("System");
                ExaDowntimeSearchModel searchDowntime = new ExaDowntimeSearchModel();
                searchDowntime.PLANT_ID = 4;
                searchDowntime.START_DATE = new DateTime(2017, 9, 1,0,0,0 );
                searchDowntime.END_DATE = new DateTime(2017, 9, 30, 23, 59, 59);

                List<long> plantId = new List<long>();
                plantId.Add(searchDowntime.PLANT_ID);

                //Step 1. Clean current data
                transService.DeleteQMS_TR_DOWNTIMEByDatetime(_db, searchDowntime.START_DATE, searchDowntime.END_DATE, plantId);

                //Step 2 call recal
                transService.recheckRawDowntime(_db, searchDowntime);

                //Step 3 cal Downtime again
                transService.CalDowntime(_db, searchDowntime);
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }
            return View("sendMail");
        }


        public ActionResult CalReduceFeed()
        {
            try
            {
                //ID	NAME	ABBREVIATION
                //3	GSP1	G1
                //4	GSP2	G2
                //5	GSP3	G3
                //6	GSP5	G5
                //7	GSP6	G6
                //8	ESP	ESP
                //9	STAB	test
                //cal Downtime

                TransactionService transService = new TransactionService("System");
                ExaReduceFeedSearchModel searchReduceFeed = new ExaReduceFeedSearchModel();
                searchReduceFeed.PLANT_ID = 3;
                searchReduceFeed.START_DATE = new DateTime(2017, 12, 26, 0, 0, 0);
                searchReduceFeed.END_DATE = new DateTime(2017, 12, 31, 23, 59, 59);

                List<long> plantId = new List<long>();
                plantId.Add(searchReduceFeed.PLANT_ID);

                ////Step 1. Clean current data
                //transService.DeleteQMS_TR_REDUCE_FEEDyDatetime(_db, searchReduceFeed.START_DATE, searchReduceFeed.END_DATE, plantId);

                ////Step 2 call recal
                //transService.recheckRawReduceFeed(_db, searchReduceFeed);

                //Step 3 cal Downtime again
                transService.CalReduceFeed(_db, searchReduceFeed);
                 
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }
            return View("sendMail");
        }

        public ActionResult LoadProductQualityToCSC()
        {
            try
            {
                //DateTime startDate = new DateTime(2017, 09, 21, 0, 0, 0);
                //DateTime endDate = new DateTime(2017, 10, 12, 0, 0, 0).AddDays(1).AddMilliseconds(-1);


                DateTime startDate = new DateTime(2020, 03, 01, 0, 0, 0);
                DateTime endDate = new DateTime(2020, 03, 02, 0, 0, 0).AddDays(1).AddMilliseconds(-1);


                DateTime startTime = startDate;
                DateTime endTime = startDate.AddDays(1);
                COAServices coaService = new COAServices(_db, _username);

                while (startTime <= endDate)
                {
                    endTime = startTime.AddDays(1);
                    coaService.DailyCOAWriteProductQualityToCSC(startTime, endTime, true);
                    startTime = startTime.AddDays(1);
                } 
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }
            return View("sendMail");
        }

        public ActionResult ReCalOffControl()
        {
            try{
                //ExaCommandSQL exaCommand = new ExaCommandSQL();
                //List<ViewQMS_MA_EXQ_TAG> result = new List<ViewQMS_MA_EXQ_TAG>();
                //result = exaCommand.getSQLQualityQMS_MA_EXQ_TAGListByProductMapId(1);
                //result = exaCommand.getSQLQuantityQMS_MA_EXQ_TAGListByProductMapId(1);
                //result = exaCommand.getSQLAccumTagQMS_MA_EXQ_TAGListByProductMapId(1);
                // exaCommand.getAllProductEXQData

                /*List<ViewQMS_MA_PRODUCT_MAPPING> listProductMapping = masterDataServices.getQMS_MA_PRODUCT_MAPPINGListByPlantId(_db, 3);

                foreach (ViewQMS_MA_PRODUCT_MAPPING dt in listProductMapping)
                {
                    TransactionService transServices = new TransactionService(_username);
                    DateTime dtStartDate = new DateTime(2016, 12, 1);
                    DateTime dtEndDate = new DateTime(2016, 12, 31);
                    ExaProductSearchModel search = new ExaProductSearchModel();
                    search.PLANT_ID = dt.PLANT_ID;
                    search.PRODUCT_ID = dt.PRODUCT_ID;
                    search.START_DATE = dtStartDate;
                    search.END_DATE = dtEndDate;
                    transServices.CalOffControlOffSpec(_db, search); //transServices.AssignAccumValueTagEx(_db, search);
                }*/

                ExaCommandSQL exaCommand = new ExaCommandSQL(ConfigurationManager.ConnectionStrings["QMSSystemExaDB"].ToString());
                DateTime dtStartDate = new DateTime(2020, 06, 01, 0, 0, 0);
                DateTime dtEndDate = new DateTime(2020, 06, 30, 23, 58, 0);
                ExaProductSearchModel search = new ExaProductSearchModel();
                search.PLANT_ID = 3;
                search.PRODUCT_ID = 3;
                search.START_DATE = dtStartDate;
                search.END_DATE = dtEndDate;
                //exaCommand.SQLCalOffControlOffSpec(search);
                //exaCommand.SQLCalDowntimeToDB(search);
                exaCommand.SQLCalReduceFeedToDB(search);
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                ViewBag.Message = ex.Message;
            }
            return View("sendMail");
        }

        public ActionResult HomeBackgroud()
        {
            ConfigServices cogServices = new ConfigServices(_username);

            ViewBag.userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_Home);
            ViewBag.BackgroudImage = cogServices.getHOME_BACKGROUND(_db);
            return View();
        }

        public ActionResult ProductCSCPath()
        {
            ConfigServices cogServices = new ConfigServices(_username);
            CreatePRODUCT_CSC_PATH model = new CreatePRODUCT_CSC_PATH();
            model = cogServices.getProductCSCPath(_db);
            ViewBag.userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_ProductCSCPath);
            ViewBag.ProductCSCPath = model;
            return View();
        }

        public JsonResult CreateProductCSCPath(CreatePRODUCT_CSC_PATH model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {
                    long nResult = cogServices.SaveProductCSCPathy(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_ProductCSCPath);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Abnormal()
        {
            ConfigServices cogServices = new ConfigServices(_username);
            CreateAbnormalSetting create = new CreateAbnormalSetting();
            create = cogServices.getCreateAbnormalSetting(_db);

            ViewBag.userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_Abnormal);
            ViewBag.create = create;

            return View();
        }

        public JsonResult CreateAbnormalSetting(CreateAbnormalSetting model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                { 
                    long nResult = cogServices.SaveAbnormalSetting(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_Abnormal);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    } 

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
         
        public ActionResult ExportDataBoundary()
        {
            ConfigServices cogServices = new ConfigServices(_username);
            CreateExportDataBoundary create = new CreateExportDataBoundary();
            create = cogServices.getCreateExportDataBoundary(_db);

            ViewBag.userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_ExportData);
            ViewBag.create = create;

            return View();
        }

        public JsonResult CreateExportDataBoundary(CreateExportDataBoundary model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {
                    long nResult = cogServices.SaveExportDataBoundary(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_ExportData);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    } 
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListExportDataPrefix()
        {
            
            ConfigServices cogServices = new ConfigServices(_username);
            MasterDataService masterServices = new MasterDataService(_username);
            ExportPrefixSearchModel searchExportPrefix = new ExportPrefixSearchModel();
            List<ViewQMS_MA_COA_PRODUCT> listProduct = getMockProduct();
            CreateQMS_ST_EXPORT_PREFIX create = new CreateQMS_ST_EXPORT_PREFIX();
            List<ViewQMS_MA_PLANT> listPlant = masterServices.getQMS_MA_PLANTActiveList(_db);

            ViewBag.searchExportPrefix = searchExportPrefix;
            ViewBag.listProduct = listProduct;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.create = create;
            ViewBag.listPlant = listPlant;
            return View();
        }

        public ActionResult CreateExportDataPrefix(long id = 0)
        {
            ConfigServices cogServices = new ConfigServices(_username);
            MasterDataService masterServices = new MasterDataService(_username);
            CreateQMS_ST_EXPORT_PREFIX create = new CreateQMS_ST_EXPORT_PREFIX();
            List<ViewQMS_MA_PLANT> listPlant = masterServices.getQMS_MA_PLANTActiveList(_db);

            if (id > 0)
            {
                create = cogServices.getCreateQMS_ST_EXPORT_PREFIXById(_db, id);
            }
            
            ViewBag.create = create;
            ViewBag.listPlant = listPlant;

            return View();
        }

        public JsonResult GetQMS_ST_EXPORT_PREFIXBySearch(ExportPrefixSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_ST_EXPORT_PREFIX> listData = new List<ViewQMS_ST_EXPORT_PREFIX>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = cogServices.searchCreateQMS_ST_EXPORT_PREFIX(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CreateQMS_ST_AUTHORIZATION_2(string listCreate)
        {
            Object result = null;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);
                TransactionService transService = new TransactionService(_username);
                UserService userServices = new UserService(_username);
                try
                {
                    long nResult = 0;
                    long userId = 0;
                    long adminId = 0;

                    if (listCreate.Count() > 0)
                    {
                        //foreach (CreateQMS_ST_AUTHORIZATION dt in listCreate)
                        //{
                        //    CreateQMS_ST_AUTHORIZATION model = new CreateQMS_ST_AUTHORIZATION();

                        //    model.ID = dt.ID;
                        //    model.GROUP_AUTH = dt.GROUP_AUTH;
                        //    //model.LIST_AUTH = new JavaScriptSerializer().Serialize(dt.ATUH_DATA);
                        //    model.listAuth = userServices.getListAuthenByPrivilegeMode(dt.listAuth, dt.ATUH_DATA);
                        //    model.LIST_AUTH = new JavaScriptSerializer().Serialize(model.listAuth);
                        //    nResult = cogServices.SaveQMS_ST_AUTHORIZATION(_db, model);
                        //    //nResult = transService.SaveQMS_ST_AUTHORIZATION(_db, model);

                        //    if (dt.GROUP_AUTH == (byte)GROUP_AUTHORIZATION.ADMIN)
                        //    {
                        //        adminId = nResult;
                        //    }
                        //    else
                        //    {
                        //        userId = nResult;
                        //    }
                        //}

                        //if (nResult > 0)
                        //{
                        //    CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_Authen);
                        //    result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, adminId = adminId, userId = userId, history = userStamp };
                        //}
                        //else
                        //{
                        //    result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        //}
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CreateQMS_ST_AUTHORIZATION(List<CreateQMS_ST_AUTHORIZATION> listCreate)
        {
            try
            {
                // Your processing logic here

            }
            catch (Exception ex)
            {
                // Handle exceptions

            }
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);
                TransactionService transService = new TransactionService(_username);
                UserService  userServices =new UserService(_username);
                try
                {
                    long nResult = 0;
                    long userId = 0;
                    long adminId = 0;

                    if (listCreate.Count() > 0)
                    {
                        //foreach (CreateQMS_ST_AUTHORIZATION dt in listCreate)
                        //{
                        //    CreateQMS_ST_AUTHORIZATION model = new CreateQMS_ST_AUTHORIZATION();

                        //    model.ID = dt.ID;
                        //    model.GROUP_AUTH = dt.GROUP_AUTH;
                        //    //model.LIST_AUTH = new JavaScriptSerializer().Serialize(dt.ATUH_DATA);
                        //    model.listAuth = userServices.getListAuthenByPrivilegeMode(dt.listAuth, dt.ATUH_DATA);
                        //    model.LIST_AUTH = new JavaScriptSerializer().Serialize(model.listAuth);
                        //    nResult = cogServices.SaveQMS_ST_AUTHORIZATION(_db, model);
                        //    //nResult = transService.SaveQMS_ST_AUTHORIZATION(_db, model);

                        //    if (dt.GROUP_AUTH == (byte)GROUP_AUTHORIZATION.ADMIN)
                        //    {
                        //        adminId = nResult;
                        //    }
                        //    else
                        //    {
                        //        userId = nResult;
                        //    }
                        //}

                        //if (nResult > 0)
                        //{
                        //    CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_Authen);
                        //    result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, adminId = adminId, userId = userId, history = userStamp };
                        //}
                        //else
                        //{
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        //}
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_ST_EXPORT_PREFIX(CreateQMS_ST_EXPORT_PREFIX model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {

                    long nResult = cogServices.SaveQMS_ST_EXPORT_PREFIX(_db, model);

                    if (nResult > 0)
                    {
                        model =  cogServices.getCreateQMS_ST_EXPORT_PREFIXById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, history = model };
                    }
                    else
                    {
                        if (nResult == -1)
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.PlantExPrefixDuplicate };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        } 
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_ST_EXPORT_PREFIX(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {
                    bool nResult = cogServices.DeleteQMS_ST_EXPORT_PREFIXByListId(_db, ids);

                    if(true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportOffControlSpec(long id = 0)
        {
            ConfigServices ConfigServices = new ConfigServices(_username);
            ReportServices ReportServices = new ReportServices(_username);
            MasterDataService masterDataServices = new MasterDataService(_username);

            CreateQMS_ST_SYSTEM_CONFIG createQMS_ST_SYSTEM_CONFIG = new CreateQMS_ST_SYSTEM_CONFIG();
            UpdateOFFReportQMS_ST_SYSTEM_CONFIG createModel = new QMSSystem.Model.UpdateOFFReportQMS_ST_SYSTEM_CONFIG(); 
            string actionName = Resources.Strings.AddNew;

            createQMS_ST_SYSTEM_CONFIG.EXA_SCHEDULE = getDummyDateTime();
            createQMS_ST_SYSTEM_CONFIG.LIMS_SCHEDULE = getDummyDateTime();
            createQMS_ST_SYSTEM_CONFIG.TEMPLATE_SCHEDULE = getDummyDateTime();

            List<ViewQMS_ST_SYSTEM_CONFIG> listData = new List<ViewQMS_ST_SYSTEM_CONFIG>();

            listData = ConfigServices.getAllQMS_ST_SYSTEM_CONFIG(_db);

            ViewQMS_ST_SYSTEM_CONFIG dataItem = listData.FirstOrDefault();

            if (null != dataItem)
            {
                actionName = Resources.Strings.EditData;
                createQMS_ST_SYSTEM_CONFIG = ConfigServices.getQMS_ST_SYSTEM_CONFIGById(_db, dataItem.ID);
                createQMS_ST_SYSTEM_CONFIG.ID = dataItem.ID;
                createModel.ID = dataItem.ID;
                createModel.REPORT_OFF_CONTROL = createQMS_ST_SYSTEM_CONFIG.REPORT_OFF_CONTROL;
                createModel.REPORT_OFF_SPEC = createQMS_ST_SYSTEM_CONFIG.REPORT_OFF_SPEC;
            }

            ViewBag.listOffControl = ReportServices.getQMS_RP_OFF_CONTROLList(_db, (byte)OFF_TYPE.CONTROL);
            ViewBag.listOffSpec = ReportServices.getQMS_RP_OFF_CONTROLList(_db, (byte)OFF_TYPE.SPEC);


            ViewBag.createModel = createModel;
            ViewBag.userStamp = ConfigServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_ReportOff);
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_ST_SYSTEM_CONFIG(CreateQMS_ST_SYSTEM_CONFIG model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices ConfigServices = new ConfigServices(_username);

                try
                {
                    long nResult = ConfigServices.SaveQMS_ST_SYSTEM_CONFIG(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateOFFReportQMS_ST_SYSTEM_CONFIG(UpdateOFFReportQMS_ST_SYSTEM_CONFIG model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices ConfigServices = new ConfigServices(_username);

                try
                {
                    long nResult = ConfigServices.UpdateOFFReportQMS_ST_SYSTEM_CONFIG(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = ConfigServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_ReportOff);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateTrendReportQMS_ST_SYSTEM_CONFIG(UpdateTrendQMS_ST_SYSTEM_CONFIG model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices ConfigServices = new ConfigServices(_username);

                try
                {
                    long nResult = ConfigServices.UpdateTrendReportQMS_ST_SYSTEM_CONFIG(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = ConfigServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_TrendHightlight);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListFileAttach()
        {
            ConfigServices ConfigServices = new ConfigServices(_username);
            FileAttachSearchModel searchFileAttach = new FileAttachSearchModel();



            ViewBag.listTypeReport = this.getTypeReportList();

            ViewBag.searchFileAttach = searchFileAttach;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public ActionResult CreateFileAttach(long id = 0)
        {
            ConfigServices ConfigServices = new ConfigServices(_username);
            CreateQMS_ST_FILE_ATTACH createQMS_ST_FILE_ATTACH = new CreateQMS_ST_FILE_ATTACH();
            string actionName = Resources.Strings.AddNew;

           
            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                //createQMS_ST_FILE_ATTACH = ConfigServices.getQMS_ST_FILE_ATTACHById(_db, id);
            }

            ViewBag.listTypeReport = this.getTypeReportList();

            ViewBag.createQMS_ST_FILE_ATTACH = createQMS_ST_FILE_ATTACH;

            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_ST_FILE_ATTACH(CreateQMS_ST_FILE_ATTACH model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices ConfigServices = new ConfigServices(_username);

                try
                {
                    long nResult = ConfigServices.SaveQMS_ST_FILE_ATTACH(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_ST_FILE_ATTACHBySearch(FileAttachSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices ConfigServices = new ConfigServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_ST_FILE_ATTACH> listData = new List<ViewQMS_ST_FILE_ATTACH>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    //listData = ConfigServices.searchQMS_ST_FILE_ATTACH(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult DeleteQMS_ST_FILE_ATTACH(long[] ids)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        ConfigServices ConfigServices = new ConfigServices(_username);

        //        try
        //        {
        //            //bool nResult = ConfigServices.DeleteQMS_ST_FILE_ATTACHByListId(_db, ids);
                  
        //            if (true == nResult)
        //            {
        //                result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
        //            }
        //            else
        //            {
        //                result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
  





        

        public ActionResult ListExaquatumSchedule()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ExaScheduleSearchModel searchExaSchedule = new ExaScheduleSearchModel();
             
            ConfigServices configDataServices = new ConfigServices(_username);
            CreateQMS_ST_EXAQUATUM_SCHEDULE createQMS_ST_EXAQUATUM_SCHEDULE = new CreateQMS_ST_EXAQUATUM_SCHEDULE();
            createQMS_ST_EXAQUATUM_SCHEDULE.START_DATE = getDummyDateTime();
            createQMS_ST_EXAQUATUM_SCHEDULE.END_DATE = getDummyDateTime();
            createQMS_ST_EXAQUATUM_SCHEDULE.DOC_STATUS = (byte)EXA_SCHEDULE_STATUS.PENDING;



            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listStatus = getExaScheduleStatusList();
            ViewBag.searchExaSchedule = searchExaSchedule;
            ViewBag.listPageSize = this.getPageSizeList();
              
            ViewBag.createQMS_ST_EXAQUATUM_SCHEDULE = createQMS_ST_EXAQUATUM_SCHEDULE;

            return View();
        }

        public ActionResult CreateExaquatumSchedule(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            ConfigServices configDataServices = new ConfigServices(_username);
            CreateQMS_ST_EXAQUATUM_SCHEDULE createQMS_ST_EXAQUATUM_SCHEDULE = new CreateQMS_ST_EXAQUATUM_SCHEDULE();
            createQMS_ST_EXAQUATUM_SCHEDULE.START_DATE = getDummyDateTime();
            createQMS_ST_EXAQUATUM_SCHEDULE.END_DATE = getDummyDateTime();
            createQMS_ST_EXAQUATUM_SCHEDULE.DOC_STATUS = (byte)EXA_SCHEDULE_STATUS.PENDING;

            string actionName = Resources.Strings.AddNew;

            if (id > 0)
            {
                actionName = Resources.Strings.EditData;
                createQMS_ST_EXAQUATUM_SCHEDULE = configDataServices.getQMS_ST_EXAQUATUM_SCHEDULEById(_db, id);
            }

            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listStatus = getExaScheduleStatusList();
            ViewBag.createQMS_ST_EXAQUATUM_SCHEDULE = createQMS_ST_EXAQUATUM_SCHEDULE;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_ST_EXAQUATUM_SCHEDULE(CreateQMS_ST_EXAQUATUM_SCHEDULE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices configDataServices = new ConfigServices(_username);

                try
                {
                    long nResult = configDataServices.SaveQMS_ST_EXAQUATUM_SCHEDULE(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_ST_EXAQUATUM_SCHEDULEBySearch(ExaScheduleSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            { 
                ConfigServices configService = new ConfigServices(_username);
                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_ST_EXAQUATUM_SCHEDULE> listData = new List<ViewQMS_ST_EXAQUATUM_SCHEDULE>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = configService.searchQMS_ST_EXAQUATUM_SCHEDULE(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteQMS_ST_EXAQUATUM_SCHEDULE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices configService = new ConfigServices(_username);

                try
                {
                    bool nResult = configService.DeleteQMS_ST_EXAQUATUM_SCHEDULEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TrendHighlightSetting()
        {
            ReportServices repServices = new ReportServices(_username);
            ConfigServices ConfigServices = new ConfigServices(_username);
            UpdateTrendQMS_ST_SYSTEM_CONFIG createModel = new UpdateTrendQMS_ST_SYSTEM_CONFIG();
            CreateQMS_ST_SYSTEM_CONFIG createQMS_ST_SYSTEM_CONFIG = new CreateQMS_ST_SYSTEM_CONFIG();

            //ดึงค่าจาก config 
            List<ViewQMS_ST_SYSTEM_CONFIG> listData = new List<ViewQMS_ST_SYSTEM_CONFIG>(); 
            listData = ConfigServices.getAllQMS_ST_SYSTEM_CONFIG(_db);
            ViewQMS_ST_SYSTEM_CONFIG dataItem = listData.FirstOrDefault();

            if (null != dataItem)
            { 
                createQMS_ST_SYSTEM_CONFIG = ConfigServices.getQMS_ST_SYSTEM_CONFIGById(_db, dataItem.ID);
                createModel.ID = dataItem.ID;
                createModel.HIGHLIGHT_1 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_1;
                createModel.HIGHLIGHT_2 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_2;
                createModel.HIGHLIGHT_3 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_3;
                createModel.HIGHLIGHT_4 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_4;
            }

            List<ViewQMS_RP_TREND_DATA_ComboList> listReport = repServices.getQMS_RP_TREND_DATAComboList(_db);

            ViewBag.createModel = createModel;
            ViewBag.listReport = listReport;
            ViewBag.userStamp = ConfigServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_TrendHightlight);
            return View("TrendHighlight");
        }

        public ActionResult SystemConnect()
        {
            ConfigServices cogService = new ConfigServices(_username);
            CreateQMS_ST_LIMS_CONNECT createLimsConnect = new CreateQMS_ST_LIMS_CONNECT();
            CreateQMS_ST_TEMPLATE_CONNECT createTemplateConnect = new CreateQMS_ST_TEMPLATE_CONNECT();
            CreateQMS_ST_EXA_CONNECT createExaConnect = new CreateQMS_ST_EXA_CONNECT();

            createLimsConnect = cogService.getLIMS_CONNECTION(_db);
            createTemplateConnect = cogService.getTEMP_SCHEDULE(_db);
            createExaConnect = cogService.getEXA_SCHEDULE(_db);

            if (createLimsConnect.LIMS_SCHEDULE == null)
            {
                createLimsConnect.LIMS_SCHEDULE = new DateTime(2017, 1, 1, 0, 0, 0);// default
            }

            if (createTemplateConnect.TEMPLATE_SCHEDULE == null)
            {
                createTemplateConnect.TEMPLATE_SCHEDULE = new DateTime(2017, 1, 1, 0, 0, 0);// default
            }

            if (createExaConnect.EXA_SCHEDULE == null)
            {
                createExaConnect.EXA_SCHEDULE = new DateTime(2017, 1, 1, 0, 0, 0);// default
            }

            ViewBag.userStampLIMS_CONNNECT = cogService.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_LIMS_CONNECT);
            ViewBag.userStampTEMP_CONNNECT = cogService.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_TEMP_CONNECT);
            ViewBag.userStampEXA_CONNNECT = cogService.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_EXA_CONNNECT);

            ViewBag.createLIMSConnect = createLimsConnect;
            ViewBag.createTEMPConnect = createTemplateConnect;
            ViewBag.createExaConnect = createExaConnect;

            return View();
        }

        public JsonResult CreateQMS_ST_LIMS_CONNECT_DATA(CreateQMS_ST_LIMS_CONNECT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {
                    long nResult = cogServices.SaveLIMSConnectSetting(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_LIMS_CONNECT);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_ST_EXA_CONNECT_DATA(CreateQMS_ST_EXA_CONNECT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {
                    long nResult = cogServices.SaveExaConnectSetting(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_EXA_CONNNECT);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    } 
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_ST_TEMPLATE_CONNECT_DATA(CreateQMS_ST_TEMPLATE_CONNECT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices cogServices = new ConfigServices(_username);

                try
                {
                    long nResult = cogServices.SaveTemplateConnectSetting(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_ST_SYSTEM_USER_STAMP userStamp = cogServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_TEMP_CONNECT);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, history = userStamp };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Authorization()
        {
            ConfigServices configServices = new ConfigServices(_username);
            UserService usrService = new UserService(_username);
             
            CreateQMS_ST_AUTHORIZATION createUser = configServices.getCreateQMS_ST_AUTHORIZATIONByGROUP_AUTH(_db, (int)GROUP_AUTHORIZATION.USER);
            CreateQMS_ST_AUTHORIZATION createLapAdmin = configServices.getCreateQMS_ST_AUTHORIZATIONByGROUP_AUTH(_db, (int)GROUP_AUTHORIZATION.ADMIN);

            List<AuthenAccess> listResult = usrService.getDefaultListAuthenAccess(false);

            if (createUser.LIST_AUTH != null && createUser.LIST_AUTH != "")
            {
                createUser.listAuth = JsonConvert.DeserializeObject<List<AuthenAccess>>(createUser.LIST_AUTH) as List<AuthenAccess>;
                createUser.listAuth = configServices.convertToCurrentListAuthenAccess(createUser.listAuth);
            }
            else
            {
                createUser.listAuth = listResult;
            }

            if (createLapAdmin.LIST_AUTH != null && createLapAdmin.LIST_AUTH != "")
            {
                createLapAdmin.listAuth = JsonConvert.DeserializeObject<List<AuthenAccess>>(createLapAdmin.LIST_AUTH) as List<AuthenAccess>;
                createLapAdmin.listAuth = configServices.convertToCurrentListAuthenAccess(createLapAdmin.listAuth);
            }
            else
            {
                createLapAdmin.listAuth = usrService.getDefaultListAuthenAccess(true);
            }

             
            createUser.ATUH_DATA = usrService.getPrivilegeModeByListAuthen(createUser.listAuth);
            createLapAdmin.ATUH_DATA = usrService.getPrivilegeModeByListAuthen(createLapAdmin.listAuth);

            //if (listAuthenData != null && listAuthenData.Count() > 0)
            //{
            //    //ถ้ามี list อยู่ใน กีอปปี ค่าปัจจุบัน
            //}
            

            ViewBag.createUser = createUser;
            ViewBag.createLapAdmin = createLapAdmin;
            ViewBag.userStamp = configServices.getQMS_ST_SYSTEM_USER_STAMPBySystemType(_db, (int)ST_CONFIG_STAMP.S_Authen);
            return View();
        }

        public ActionResult SortOption()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            listTemplate = masterDataServices.getAllTemplateForSortOption(_db);
            ViewBag.listTemplate = listTemplate;
            return View();
        }



        public ActionResult SystemLog()
        {
            ConfigServices ConfigServices = new ConfigServices(_username);
            SystemLogSearchModel searchSystemLog = new SystemLogSearchModel();



            ViewBag.searchSystemLog = searchSystemLog;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public JsonResult GetQMS_ST_SYSTEM_LOGBySearch(SystemLogSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices ConfigServices = new ConfigServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_ST_SYSTEM_LOG> listData = new List<ViewQMS_ST_SYSTEM_LOG>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = ConfigServices.searchQMS_ST_SYSTEM_LOG(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_ST_SYSTEM_LOG(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices ConfigServices = new ConfigServices(_username);

                try
                {
                    bool nResult = ConfigServices.DeleteQMS_ST_SYSTEM_LOGByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadLakeTagNameFromExcelTODB()//เพิ่มตอน pop ต่อ DataLeak
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            try
            {
                DateTime StratTime = DateTime.Now;
                string filename = HttpContext.Server.MapPath("~/UploadExceDataLeak/RootTagMapping.xlsx");
                var nResult = masterDataServices.LoadQMS_MA_EXQ_TAG_From_Excel(_db, filename);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }

        public ActionResult LoadDataFromDataLakeTODB()//เพิ่มตอน pop ต่อ DataLeak
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            try
            {
                DateTime? dStratDate = null;
                DateTime? dEndDate = null;
                dStratDate = new DateTime(2020, 05, 25);
                dEndDate = new DateTime(2020, 05, 26);
                DateTime StratTime = DateTime.Now;
                var nResult = masterDataServices.LoadDataLakeTODB(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadRawDataToQmsTrExqProduct()//เปลี่ยน DataLeak จาก ptt-gspcidb-p01.ptt.corp เป็น sna-pttedp-prd-001.privatelink.sql.azuresynapse.net get data realtime 2024/01/17
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            try
            {
                DateTime dStratDate = DateTime.Now.AddDays(-1);
                DateTime dEndDate = DateTime.Now.AddDays(1);
                DateTime StratTime = DateTime.Now;
                var nResult = masterDataServices.LoadRawDataToQmsTrExqProduct(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadAbnormalFromDataLakeTODB()//เพิ่มตอน pop ต่อ DataLeak
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime? dStratDate = null;
                DateTime? dEndDate = null;
                DateTime StratTime = DateTime.Now;
                var nResult = transDataServices.LoadAbnormalFromDataLakeTODB(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadDataLake_EVENT_DATA_TODB()//เพิ่มตอน pop ต่อ DataLeak
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime? dStratDate = null;
                DateTime? dEndDate = null;
                DateTime StratTime = DateTime.Now;
                var nResult = transDataServices.LoadDataLake_EVENT_DATA_TODB(_db);//, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadEventRawDataFromDataLakeTODB()//เพิ่มตอน pop ต่อ DataLeak
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime? dStratDate = null;
                DateTime? dEndDate = null;
                DateTime StratTime = DateTime.Now;
                var nResult = transDataServices.LoadEventRawDataFromDataLakeTODB(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadEventRawDataNewRealTimeFromDataLakeTODB()//เปลี่ยน DataLeak จาก ptt-gspcidb-p01.ptt.corp เป็น sna-pttedp-prd-001.privatelink.sql.azuresynapse.net get data realtime 2024/01/17
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime? dStratDate = DateTime.Now;
                DateTime? dEndDate = DateTime.Now.AddDays(1);
                DateTime StratTime = DateTime.Now;
                var nResult = transDataServices.LoadEventRawDataNewRealTimeFromDataLakeTODB(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadOffControlFromDBTODataLake()//เพิ่มตอน pop ต่อ DataLeak
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime? dStratDate = new DateTime(2020, 05, 22);
                DateTime? dEndDate = new DateTime(2020, 05, 23);
                DateTime StratTime = DateTime.Now;
                var nResult = transDataServices.LoadOffControlFromDBTODataLake(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadRP_OFF_CONTROLFromDBTODataLake()//เพิ่มตอน pop ต่อ DataLeak
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime? dStratDate = null;
                DateTime? dEndDate = null;
                DateTime StratTime = DateTime.Now;
                var nResult = transDataServices.LoadRP_OFF_CONTROLFromDBTODataLake(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }
        public ActionResult LoadRP_OFF_CONTROL_SUMMARYFromDBTODataLake()//เพิ่มตอน pop ต่อ DataLeak
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime? dStratDate = null;
                DateTime? dEndDate = null;
                DateTime StratTime = DateTime.Now;
                var nResult = transDataServices.LoadRP_OFF_CONTROL_SUMMARYFromDBTODataLake(_db, dStratDate, dEndDate);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }


        public ActionResult AssignAccumValueTagEx()//เพิ่มตอน pop ต่อ DataLeak
        {
            TransactionService transDataServices = new TransactionService(_username);
            try
            {
                DateTime StratTime = DateTime.Now;
                DateTime dStratDate = new DateTime(2020, 06, 10);
                DateTime dEndDate = new DateTime(2020, 06, 12);
                ExaProductSearchModel searchExaProduct = new ExaProductSearchModel();
                searchExaProduct.START_DATE = dStratDate.AddMinutes(2);
                searchExaProduct.END_DATE = dEndDate;
                searchExaProduct.PLANT_ID = 3;
                searchExaProduct.PRODUCT_ID = 4;

                var nResult = transDataServices.AssignAccumValueTagEx(_db, searchExaProduct);
                DateTime EndTime = DateTime.Now;
                TimeSpan DifferenceTime = EndTime.TimeOfDay - StratTime.TimeOfDay;
                ViewBag.Message = String.Format("Spend time => {0} hours : {1} minutes : {2} seconds", DifferenceTime.Hours.ToString(), DifferenceTime.Minutes.ToString(), DifferenceTime.Seconds.ToString());
                return View("sendmail");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("sendmail");
            }
        }

        public JsonResult ReadTemplateFileDataToDatabaseByTemplateType(byte templateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transService = new TransactionService(_username);
                try
                {
                    bool nResult = false;
                    nResult = transService.ReadTemplateFileDataToDatabaseByTemplateType(_db, templateType);
                    if (nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess};
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataSortingByTemplateType(long TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();
                    if(TemplateType == (byte)TEMPLATE_TYPE.Emission
                    || TemplateType == (byte)TEMPLATE_TYPE.ObservePonds
                    || TemplateType == (byte)TEMPLATE_TYPE.OilyWaters
                    || TemplateType == (byte)TEMPLATE_TYPE.HgInPLMonthly
                    || TemplateType == (byte)TEMPLATE_TYPE.SulfurInGasMonthly
                    || TemplateType == (byte)TEMPLATE_TYPE.AcidOffGas)
                    {
                        if(TemplateType == (byte)TEMPLATE_TYPE.AcidOffGas)
                        {
                            List<TemplateName_COMBO> listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TemplateType);
                            listTemplate = listTemplate.GroupBy(x => x.TEMPLATE_NAME).Select(g => g.First()).ToList();
                            List<TemplateName_COMBO> tempListAreaSorting = masterDataServices.setMappingSortingAreaAcidOffFas(_db, listTemplate, (byte)TemplateType);
                            listAreaSorting = masterDataServices.convertTrendAreaToViewQMS_MA_SORTING_AREAAcidOffFas(_db, listTemplate, (byte)TemplateType);
                        }
                        else
                        {
                            List<TrendArea> listArea = masterDataServices.getTrendAreaByTemplateType(_db, (byte)TemplateType);
                            List<TrendArea> tempListAreaSorting = masterDataServices.setMappingSortingArea(_db, listArea, (byte)TemplateType);
                            listAreaSorting = masterDataServices.convertTrendAreaToViewQMS_MA_SORTING_AREA(_db, listArea, (byte)TemplateType);
                        }
                    }
                    else if(TemplateType == (byte)TEMPLATE_TYPE.WatseWater
                    || TemplateType == (byte)TEMPLATE_TYPE.HotOilFlashPoint
                    || TemplateType == (byte)TEMPLATE_TYPE.TwoHundredThousandPond
                    || TemplateType == (byte)TEMPLATE_TYPE.HgStab)
                    {
                        List<TrendSample> listSample = new List<TrendSample>();
                        if (TemplateType == (byte)TEMPLATE_TYPE.HotOilFlashPoint)
                        {
                            listSample = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TemplateType);
                            List<TrendSample> listSampleHotOilPhysical = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TEMPLATE_TYPE.HotOilPhysical);
                            listSample.AddRange(listSampleHotOilPhysical);
                            listSample = listSample.GroupBy(m => new { m.SAMPLE_NAME }).Select(g => g.First()).ToList();
                        }
                        else
                        {
                            listSample = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TemplateType);
                        }
                        
                        List<TrendSample> tempListSampleSorting = masterDataServices.setMappingSortingSample(_db, listSample, (byte)TemplateType, "");
                        listSampleSorting = masterDataServices.convertTrendSampleToViewQMS_MA_SORTING_SAMPLE(_db, listSample, (byte)TemplateType, "");
                    }
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult setAreaPostion(List<ViewQMS_MA_SORTING_AREA> listArea, long TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = false;
                    nResult = masterDataServices.setAreaPostion(_db, listArea, TemplateType);
                    if (nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult setSamplePostion(List<ViewQMS_MA_SORTING_SAMPLE> listSample, long TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = false;
                    nResult = masterDataServices.setSamplePostion(_db, listSample, TemplateType);
                    if (nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSampleByTemplateTypeAreaName(byte TemplateType, string AreaName)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();

                    List<TrendSample> listSample = masterDataServices.getTrendSampleListByStringTemplateTypeAreaName(_db, TemplateType, AreaName);
                    listSample = listSample.Where(x => x.SAMPLE_NAME != "").ToList();
                    List<TrendSample> tempListSampleSorting = masterDataServices.setMappingSortingSample(_db, listSample, (byte)TemplateType, AreaName);
                    listSampleSorting = masterDataServices.convertTrendSampleToViewQMS_MA_SORTING_SAMPLE(_db, listSample, (byte)TemplateType, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateId(string AreaName, string SampleName, long TemplateId, byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);
                try
                {
                    
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();

                    List<TrendItem> listData = masterDataServices.getTrendItemListByStringTemplateIdSampleName(_db, TemplateId, SampleName);
                    List<TrendItem> tempListSampleSorting = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    listItemSorting = masterDataServices.convertTrendItemToViewQMS_MA_SORTING_ITEM(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult setItemPostion(List<ViewQMS_MA_SORTING_ITEM> listItem, long TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = false;
                    nResult = masterDataServices.setItemPostion(_db, listItem, TemplateType);
                    if (nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateIdEx(string AreaName, string SampleName, long TemplateId, byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();
                    CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEById(_db, TemplateId);
                    List<TrendItem> listData = masterDataServices.getTrendItemListByStringTemplateId(_db, TemplateId, SampleName);
                    List<TrendItem> tempListSampleSorting = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    listItemSorting = masterDataServices.convertTrendItemToViewQMS_MA_SORTING_ITEM(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemBySampleNameTemplateType(string SampleName, long[] TemplateTypes, string AreaName, byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();
                    List<TrendItem> listData = masterDataServices.getTrendItemListBySampleNameTemplateType(_db, SampleName, TemplateTypes);
                    List<TrendItem> tempListSampleSorting = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    listItemSorting = masterDataServices.convertTrendItemToViewQMS_MA_SORTING_ITEM(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemBySampleNameTemplateTypeEx(string SampleName, long[] TemplateTypes, string AreaName, byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();
                    List<TrendItem> listData = masterDataServices.getTrendItemListBySampleNameTemplateTypeEx(_db, SampleName, TemplateTypes);
                    List<TrendItem> tempListSampleSorting = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    listItemSorting = masterDataServices.convertTrendItemToViewQMS_MA_SORTING_ITEM(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetColItemBySampleNameTemplateTypeEx(string SampleName, long[] TemplateTypes, string AreaName, byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();
                    List<TrendItem> listData = masterDataServices.getTrendColItemListBySampleNameTemplateTypeEx(_db, SampleName, TemplateTypes);
                    List<TrendItem> tempListSampleSorting = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    listItemSorting = masterDataServices.convertTrendItemToViewQMS_MA_SORTING_ITEM(_db, listData, (byte)TemplateType, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateIdAreaNameEx(string SampleName, long TemplateId, string AreaName)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();
                    CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEById(_db, TemplateId);
                    List<TrendItem> listData = masterDataServices.getTrendItemListByStringTemplateIdAreaName(_db, TemplateId, SampleName, AreaName);
                    List<TrendItem> tempListSampleSorting = masterDataServices.setMappingSortingItem(_db, listData, (byte)templateData.TEMPLATE_TYPE, SampleName, AreaName);
                    listItemSorting = masterDataServices.convertTrendItemToViewQMS_MA_SORTING_ITEM(_db, listData, (byte)templateData.TEMPLATE_TYPE, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSampleByTemplateId(string TemplateId, byte TemplateType, string AreaName)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_SORTING_AREA> listAreaSorting = new List<ViewQMS_MA_SORTING_AREA>();
                    List<ViewQMS_MA_SORTING_SAMPLE> listSampleSorting = new List<ViewQMS_MA_SORTING_SAMPLE>();
                    List<ViewQMS_MA_SORTING_ITEM> listItemSorting = new List<ViewQMS_MA_SORTING_ITEM>();
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByStringTemplateId(_db, TemplateId, TemplateType);
                    List<TrendSample> tempListSampleSorting = masterDataServices.setMappingSortingSample(_db, listData, (byte)TemplateType, AreaName);
                    listSampleSorting = masterDataServices.convertTrendSampleToViewQMS_MA_SORTING_SAMPLE(_db, listData, (byte)TemplateType, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, listArea = listAreaSorting, listSample = listSampleSorting, listItem = listItemSorting };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
