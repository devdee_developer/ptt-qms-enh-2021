﻿using System;
using System.Collections.Generic;

using System.Linq;
using QMSSystem.Model;

namespace QMSSystem.CoreDB
{



    //public partial class QMS_MA_COA_CUSTOMER 
    //{
    //    public static List<QMS_MA_COA_CUSTOMER> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_MA_COA_CUSTOMER.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
    //    }

    //    public static QMS_MA_COA_CUSTOMER GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_MA_COA_CUSTOMER.Where(m => m.ID == id).FirstOrDefault();
    //    }
    //}

    //public partial class QMS_MA_COA_ITEM 
    //{
    //    public static List<QMS_MA_COA_ITEM> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_MA_COA_ITEM.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
    //    }

    //    public static QMS_MA_COA_ITEM GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_MA_COA_ITEM.Where(m => m.ID == id).FirstOrDefault();
    //    }
    //}

    //public partial class QMS_MA_COA_PRODUCT 
    //{
    //    public static List<QMS_MA_COA_PRODUCT> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_MA_COA_PRODUCT.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
    //    }

    //    public static QMS_MA_COA_PRODUCT GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_MA_COA_PRODUCT.Where(m => m.ID == id).FirstOrDefault();
    //    }
    //}

    public partial class QMS_MA_CONTROL 
    {
        public static List<QMS_MA_CONTROL> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_CONTROL.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_CONTROL> GetAllByGroupType(QMSDBEntities db, byte GROUP_TYPE)
        {
            return db.QMS_MA_CONTROL.Where(m => m.DELETE_FLAG == 0 &&  m.GROUP_TYPE == GROUP_TYPE ).ToList(); //linq
        }

        public static QMS_MA_CONTROL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CONTROL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_CONTROL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CONTROL.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_CONTROL> GetAllBySearch(QMSDBEntities db, ControlValueSearchModel searchModel)
        {
            List<QMS_MA_CONTROL> lstResult = new List<QMS_MA_CONTROL>();

            lstResult = db.QMS_MA_CONTROL.Where(m => m.DELETE_FLAG == 0 &&  m.GROUP_TYPE == searchModel.GROUP_TYPE ).ToList();

            try
            {
                if (null != lstResult && lstResult.Count() > 0)
                {
                    if (null != searchModel.NAME && "" != searchModel.NAME)
                    {
                        lstResult = lstResult.Where(m => m.NAME.Contains(searchModel.NAME)).ToList();
                    }

                    if (null != searchModel.CONTROL_DESC && "" != searchModel.CONTROL_DESC)
                    {
                        lstResult = lstResult.Where(m => m.CONTROL_DESC.Contains(searchModel.CONTROL_DESC)).ToList();
                    }
                }
            }
            catch (Exception ex)
            {

            }
             
            return lstResult;
        } 
    }

    public partial class QMS_MA_CONTROL_COLUMN 
    {
        public static List<QMS_MA_CONTROL_COLUMN> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_CONTROL_COLUMN.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

		public static List<QMS_MA_CONTROL_COLUMN> GetAllByControlId(QMSDBEntities db, long control_id)
        {
            return db.QMS_MA_CONTROL_COLUMN.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ID == control_id).OrderBy(m => m.POSITION).ToList(); //linq
        }

        public static List<QMS_MA_CONTROL_COLUMN> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CONTROL_COLUMN.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static QMS_MA_CONTROL_COLUMN GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CONTROL_COLUMN.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_CONTROL_COLUMN> GetAllBySearch(QMSDBEntities db, ControlValueColumnSearchModel searchModel)
        {
            List<QMS_MA_CONTROL_COLUMN> lstResult = new List<QMS_MA_CONTROL_COLUMN>();

            lstResult = db.QMS_MA_CONTROL_COLUMN.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (searchModel.CONTROL_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.CONTROL_ID == searchModel.CONTROL_ID).ToList();
                } 
            }
            return lstResult;
        } 
    }
    public partial class QMS_MA_CONTROL_DATACONF
    {
        public static QMS_MA_CONTROL_DATACONF GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CONTROL_DATACONF.Where(m => m.ID == id).FirstOrDefault();
        }
        public static List<QMS_MA_CONTROL_DATACONF> getAllByRowColControlId(QMSDBEntities db, long rowId, long ColumnId, long ControlId)
        {
            return db.QMS_MA_CONTROL_DATACONF.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ROW_ID == rowId && m.CONTROL_COLUMN_ID == ColumnId && m.CONTROL_ID == ControlId).OrderBy(m=>m.START_DATE).ToList(); //linq
        }
        public static List<QMS_MA_CONTROL_DATACONF> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CONTROL_DATACONF.Where(m => ids.Contains(m.ID)).ToList();
        }
        public static List<QMS_MA_CONTROL_DATACONF> GetAllByControlId(QMSDBEntities db, long control_id)
        {
            DateTime Date = DateTime.UtcNow.Date;
            //return db.QMS_MA_CONTROL_DATACONF.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ID == control_id).ToList(); //linq
            return db.QMS_MA_CONTROL_DATACONF.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ID == control_id&& m.START_DATE <= Date).OrderByDescending(m => m.START_DATE).ToList(); //linq
        }
        public static List<QMS_MA_CONTROL_DATACONF> GetAllistByControlConfigSampleId(QMSDBEntities db, long controlId, long sampleId)
        {
            DateTime Date = DateTime.UtcNow.Date;
            return db.QMS_MA_CONTROL_DATACONF.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ID == controlId && m.CONTROL_ROW_ID == sampleId && m.START_DATE <= Date).OrderByDescending(m => m.START_DATE).ToList(); //linq
        }
        public static List<QMS_MA_CONTROL_DATACONF> GetAll(QMSDBEntities db)
        {
            DateTime Date = DateTime.UtcNow.Date;
            return db.QMS_MA_CONTROL_DATACONF.Where(m => m.DELETE_FLAG == 0 && m.START_DATE <= Date).OrderByDescending(m => m.START_DATE).ToList(); //linq
        }
        public static List<QMS_MA_CONTROL_DATACONF> GetAllNotWhereDate(QMSDBEntities db)
        {
            return db.QMS_MA_CONTROL_DATACONF.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }
    }
        public partial class QMS_MA_CONTROL_DATA 
    {
        public static List<QMS_MA_CONTROL_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_CONTROL_DATA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_CONTROL_DATA> GetByListControlId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CONTROL_DATA.Where(m => ids.Contains(m.CONTROL_ID)).ToList();
        }

        public static List<QMS_MA_CONTROL_DATA> GetAllistByControlSampleId(QMSDBEntities db, long controlId, long sampleId)
        {
            return db.QMS_MA_CONTROL_DATA.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ID == controlId && m.CONTROL_ROW_ID == sampleId).ToList(); //linq
        }

        public static List<QMS_MA_CONTROL_DATA> GetAllByControlId(QMSDBEntities db, long control_id)
        {
            return db.QMS_MA_CONTROL_DATA.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ID == control_id).ToList(); //linq
        }

        public static QMS_MA_CONTROL_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CONTROL_DATA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_CONTROL_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CONTROL_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_CONTROL_DATA> GetAllBySearch(QMSDBEntities db, CorrectDataSearchModel searchModel)
        {
            List<QMS_MA_CONTROL_DATA> lstResult = new List<QMS_MA_CONTROL_DATA>();

            lstResult = db.QMS_MA_CONTROL_DATA.Where(m => m.DELETE_FLAG == 0).ToList();
             
            return lstResult; 
        }

        public static List<QMS_MA_CONTROL_DATA> GetAllBySearch(QMSDBEntities db, ControlValueDataSearchModel searchModel)
        {
            List<QMS_MA_CONTROL_DATA> lstResult = new List<QMS_MA_CONTROL_DATA>();

            lstResult = db.QMS_MA_CONTROL_DATA.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (searchModel.CONTROL_ROW_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.CONTROL_ROW_ID == searchModel.CONTROL_ROW_ID).ToList();
                }

                if (searchModel.CONTROL_COLUMN_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.CONTROL_COLUMN_ID == searchModel.CONTROL_COLUMN_ID).ToList();
                }
            }
            return lstResult;
        } 
        
    }

    public partial class QMS_MA_CONTROL_ROW 
    {
        public static List<QMS_MA_CONTROL_ROW> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_CONTROL_ROW.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_CONTROL_ROW> GetAllByControlId(QMSDBEntities db, long control_id)
        {
            return db.QMS_MA_CONTROL_ROW.Where(m => m.DELETE_FLAG == 0 && m.CONTROL_ID == control_id).OrderBy( m => m.POSITION).ToList(); //linq
        }

        public static QMS_MA_CONTROL_ROW GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CONTROL_ROW.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_CONTROL_ROW> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CONTROL_ROW.Where(m => ids.Contains(m.ID)).ToList();
        } 

        public static List<QMS_MA_CONTROL_ROW> GetAllBySearch(QMSDBEntities db, ControlValueRowSearchModel searchModel)
        {
            List<QMS_MA_CONTROL_ROW> lstResult = new List<QMS_MA_CONTROL_ROW>();

            lstResult = db.QMS_MA_CONTROL_ROW.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if ( searchModel.CONTROL_ID > 0 )
                {
                    lstResult = lstResult.Where(m => m.CONTROL_ID == searchModel.CONTROL_ID ).ToList();
                }
            }
            return lstResult;
        } 
    }

    public partial class QMS_MA_CORRECT_DATA 
    {
        public static List<QMS_MA_CORRECT_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_CORRECT_DATA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_CORRECT_DATA> GetAllExceptionWithoutMaster(QMSDBEntities db)
        {
            //master correct data เริ่มจาก 1 ถึง 6
            return db.QMS_MA_CORRECT_DATA.Where(m => m.DELETE_FLAG == 0 && m.ID > 6).ToList(); //linq
        }

        public static QMS_MA_CORRECT_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CORRECT_DATA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_CORRECT_DATA> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_CORRECT_DATA.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).OrderBy(m => m.NAME).ToList(); //linq
        }

        public static List<QMS_MA_CORRECT_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CORRECT_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_CORRECT_DATA> GetAllBySearch(QMSDBEntities db, CorrectDataSearchModel searchModel)
        {
            List<QMS_MA_CORRECT_DATA> lstResult = new List<QMS_MA_CORRECT_DATA>();

            lstResult = db.QMS_MA_CORRECT_DATA.Where(m => m.DELETE_FLAG == 0).ToList();

            if(null != lstResult && lstResult.Count() > 0){

                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    lstResult = lstResult.Where(m => m.NAME.Contains(searchModel.NAME)).ToList();
                }
                 
                if (  searchModel.DELETE_INPUT_QTY  > 0)
                {
                    lstResult = lstResult.Where(m => m.DELETE_INPUT_QTY == searchModel.DELETE_INPUT_QTY).ToList();
                }

                if ( searchModel.DELETE_OUTPUT_QTY > 0)
                {
                    lstResult = lstResult.Where(m => m.DELETE_OUTPUT_QTY == searchModel.DELETE_OUTPUT_QTY).ToList();
                }

                if ( searchModel.CAL_OFF_CONTROL > 0)
                {
                    lstResult = lstResult.Where(m => m.CAL_OFF_CONTROL == searchModel.CAL_OFF_CONTROL).ToList();
                }
            }
            return lstResult; 
        } 
    }

    public partial class QMS_MA_DOWNTIME 
    {
        public static List<QMS_MA_DOWNTIME> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_DOWNTIME.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_DOWNTIME GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_DOWNTIME.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_DOWNTIME> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_DOWNTIME.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_DOWNTIME> GetByListPlantId(QMSDBEntities dataContext, long plantId)
        {
            return dataContext.QMS_MA_DOWNTIME.Where(m => m.PLANT_ID == plantId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_DOWNTIME> GetAllBySearch(QMSDBEntities db, DowntimeSearchModel searchModel)
        {
            List<QMS_MA_DOWNTIME> lstResult = new List<QMS_MA_DOWNTIME>();

            var query = db.QMS_MA_DOWNTIME.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if ( searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID) ;
                }

                if (null != searchModel.EXCEL_NAME && "" != searchModel.EXCEL_NAME)
                {
                    query = query.Where(m => m.DOWNTIME_DESC.Contains(searchModel.EXCEL_NAME)) ;
                }

                if (null != searchModel.LIMIT_VALUE && searchModel.LIMIT_VALUE > 0)
                {
                    query = query.Where(m => m.LIMIT_VALUE == searchModel.LIMIT_VALUE) ;
                }

                //if (null != searchModel.EXA_TAG_NAME && "" != searchModel.EXA_TAG_NAME)
                //{
                //    lstResult = lstResult.Where(m => m.EXA_TAG_NAME.Contains(searchModel.EXA_TAG_NAME)).ToList();
                //}

                if (null != searchModel.ACTIVE_DATE)
                {
                    query = query.Where(m => m.ACTIVE_DATE == searchModel.ACTIVE_DATE) ;
                }

                if (null != searchModel.DOWNTIME_DESC && "" != searchModel.DOWNTIME_DESC)
                {
                    query = query.Where(m => m.DOWNTIME_DESC.Contains(searchModel.DOWNTIME_DESC)) ;
                }

                lstResult = query.ToList();

            }catch{

            }
            return lstResult;
        }

        
       
    }

    public partial class QMS_MA_DOWNTIME_DETAIL
    {
        public static List<QMS_MA_DOWNTIME_DETAIL> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_DOWNTIME_DETAIL.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_DOWNTIME_DETAIL> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_DOWNTIME_DETAIL.Where(m => m.DELETE_FLAG == 0).OrderBy(m => m.EXA_TAG_NAME).ToList(); //linq
        }

        public static QMS_MA_DOWNTIME_DETAIL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_DOWNTIME_DETAIL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_DOWNTIME_DETAIL> GetActiveAllByDOWNTIME_ID(QMSDBEntities db, long downtimeId)
        {
            return db.QMS_MA_DOWNTIME_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.DOWNTIME_ID == downtimeId).ToList(); //linq
        }

        public static List<QMS_MA_DOWNTIME_DETAIL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_DOWNTIME_DETAIL.Where(m => ids.Contains(m.ID)).ToList();
        }


        public static List<QMS_MA_DOWNTIME_DETAIL> GetAllBySearch(QMSDBEntities db, DowntimeDetailSearchModel searchModel)
        {
            List<QMS_MA_DOWNTIME_DETAIL> lstResult = new List<QMS_MA_DOWNTIME_DETAIL>();

            var query = db.QMS_MA_DOWNTIME_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.DOWNTIME_ID == searchModel.DOWNTIME_ID);

            try
            {

                if (null != searchModel.CONVERT_VALUE && searchModel.CONVERT_VALUE > 0)
                {
                    query = query.Where(m => m.CONVERT_VALUE == searchModel.CONVERT_VALUE);
                }

                if (null != searchModel.EXA_TAG_NAME && "" != searchModel.EXA_TAG_NAME)
                {
                    query = query.Where(m => m.EXA_TAG_NAME.Contains(searchModel.EXA_TAG_NAME));
                }

                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }
     
    }

    public partial class QMS_MA_EMAIL 
    {
        public static List<QMS_MA_EMAIL> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_EMAIL.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_EMAIL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_EMAIL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_EMAIL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_EMAIL.Where(m => ids.Contains(m.ID)).ToList();
        }


        public static QMS_MA_EMAIL GetByTypeAndEmpCode(QMSDBEntities dataContext, byte GROUP_TYPE, string EMPLOYEE_ID)
        {
            return dataContext.QMS_MA_EMAIL.Where(m => m.GROUP_TYPE == GROUP_TYPE && m.EMPLOYEE_ID == EMPLOYEE_ID).FirstOrDefault();
        }

        public static List<QMS_MA_EMAIL> GetAllBySearch(QMSDBEntities db, QMS_MA_EMAILSearchModel searchModel)
        {
            List<QMS_MA_EMAIL> lstResult = new List<QMS_MA_EMAIL>();

            lstResult = db.QMS_MA_EMAIL.Where(m => m.DELETE_FLAG == 0 && m.GROUP_TYPE == searchModel.GROUP_TYPE).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (null != searchModel.EMPLOYEE_ID && searchModel.EMPLOYEE_ID != "")
                {
                    lstResult = lstResult.Where(m => m.EMPLOYEE_ID == searchModel.EMPLOYEE_ID).ToList();
                }

                if (null != searchModel.NAME && searchModel.NAME != "")
                {
                    lstResult = lstResult.Where(m => m.NAME == searchModel.NAME).ToList();
                }

                if (null != searchModel.EMAIL && searchModel.EMAIL != "")
                {
                    lstResult = lstResult.Where(m => m.EMAIL == searchModel.EMAIL).ToList();
                }

                if (null != searchModel.POSITION && searchModel.POSITION != "")
                {
                    lstResult = lstResult.Where(m => m.POSITION == searchModel.POSITION).ToList();
                }

                if (null != searchModel.UNIT_NAME && searchModel.UNIT_NAME != "")
                {
                    lstResult = lstResult.Where(m => m.UNIT_NAME == searchModel.UNIT_NAME).ToList();
                }
            }
            return lstResult;
        }

        public static List<QMS_MA_EMAIL> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_EMAIL.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
        }

    }

    public partial class QMS_MA_EXQ_ACCUM_TAG 
    {
        public static List<QMS_MA_EXQ_ACCUM_TAG> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_EXQ_ACCUM_TAG.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_EXQ_ACCUM_TAG GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_EXQ_ACCUM_TAG.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_EXQ_ACCUM_TAG> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_EXQ_ACCUM_TAG.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_EXQ_ACCUM_TAG> GetByExaId(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_EXQ_ACCUM_TAG.Where(m => m.EXA_TAG_ID == id && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_EXQ_ACCUM_TAG> GetByExaListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_EXQ_ACCUM_TAG.Where(m => ids.Contains(m.EXA_TAG_ID)  && m.DELETE_FLAG == 0).ToList();
        }
         
    }

    public partial class QMS_MA_EXQ_TAG 
    {
        public static List<QMS_MA_EXQ_TAG> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_EXQ_TAG.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_EXQ_TAG GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_EXQ_TAG.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_EXQ_TAG> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_EXQ_TAG.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_EXQ_TAG> GetQualityTagAll(QMSDBEntities db)  //1: Quality, 2:Quantity-PV, 3: Quantity -SUM, 4: Accum
        {
            byte tagType = (byte)EXA_TAG_TYPE.QUALITY_TAG;
            return db.QMS_MA_EXQ_TAG.Where(m => m.DELETE_FLAG == 0 && m.TAG_TYPE == tagType).ToList(); //linq
        }

        public static List<QMS_MA_EXQ_TAG> GetQualityTagAllByProductMapId(QMSDBEntities db, long id)  //1: Quality, 2:Quantity-PV, 3: Quantity -SUM, 4: Accum
        {
            byte tagType = (byte)EXA_TAG_TYPE.QUALITY_TAG;
            return db.QMS_MA_EXQ_TAG.Where(m => m.DELETE_FLAG == 0 && m.TAG_TYPE == tagType &&  m.PRODUCT_MAPPING_ID == id ).ToList(); //linq
        }

        public static List<QMS_MA_EXQ_TAG> GetQuantityTagAllByProductMapId(QMSDBEntities db, long id)  //1: Quality, 2:Quantity-PV, 3: Quantity -SUM, 4: Accum
        {
            List<byte> listTag = new List<byte>();
            listTag.Add((byte)EXA_TAG_TYPE.QUANTITY_PV);
            listTag.Add((byte)EXA_TAG_TYPE.QUANTITY_SUM); 
            return db.QMS_MA_EXQ_TAG.Where(m => listTag.Contains(m.TAG_TYPE) && m.DELETE_FLAG == 0   && m.PRODUCT_MAPPING_ID == id).ToList(); //linq
        }

        public static List<QMS_MA_EXQ_TAG> GetAccumTagAllByProductMapId(QMSDBEntities db, long id)  //1: Quality, 2:Quantity-PV, 3: Quantity -SUM, 4: Accum
        {
            byte tagType = (byte)EXA_TAG_TYPE.ACCUM;
            return db.QMS_MA_EXQ_TAG.Where(m => m.DELETE_FLAG == 0 && m.TAG_TYPE == tagType && m.PRODUCT_MAPPING_ID == id).ToList(); //linq
        }
    }

    public partial class QMS_MA_GRADE 
    {
        public static List<QMS_MA_GRADE> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_GRADE.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_GRADE GetByGradeNameAndSkipGradeId(QMSDBEntities dataContext, string gradeName, long gradeId)
        {
            return dataContext.QMS_MA_GRADE.Where(m => m.NAME.ToLower() == gradeName.ToLower() && m.DELETE_FLAG == 0 && m.ID != gradeId).FirstOrDefault();
        }

        public static QMS_MA_GRADE GetByGradeName(QMSDBEntities dataContext, string gradeName)
        {
            return dataContext.QMS_MA_GRADE.Where(m => m.NAME.ToLower() == gradeName.ToLower() && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static List<QMS_MA_GRADE> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_GRADE.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).ToList(); //linq
        }

        public static QMS_MA_GRADE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_GRADE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_GRADE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_GRADE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_GRADE> GetAllByNotinGradeId(QMSDBEntities db, long FromGradeId)
        {
            return db.QMS_MA_GRADE.Where(m => m.DELETE_FLAG == 0 && m.ID != FromGradeId && m.ID > 2).ToList(); //linq
        }
    }

    public partial class QMS_MA_KPI 
    {
        public static List<QMS_MA_KPI> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_KPI.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_KPI GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_KPI.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_KPI> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_KPI.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_KPI> GetAllBySearch(QMSDBEntities db, KPISearchModel searchModel)
        {
            List<QMS_MA_KPI> lstResult = new List<QMS_MA_KPI>();

            lstResult = db.QMS_MA_KPI.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {
           
                if (null != searchModel.PLANT_ID && searchModel.PLANT_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.PLANT_ID == searchModel.PLANT_ID).ToList();
                }

                if (null != searchModel.PRODUCT_ID && searchModel.PRODUCT_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID).ToList();
                }

                if (null != searchModel.ACTIVE_DATE )
                {
                    lstResult = lstResult.Where(m => m.ACTIVE_DATE == searchModel.ACTIVE_DATE).ToList();
                }

                if (null != searchModel.KPI_DESC && "" != searchModel.KPI_DESC)
                {
                    lstResult = lstResult.Where(m => m.KPI_DESC.Contains(searchModel.KPI_DESC)).ToList();
                }
            }
            return lstResult;
        } 
    }

    public partial class QMS_MA_OPERATION_SHIFT 
    {
        public static List<QMS_MA_OPERATION_SHIFT> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_OPERATION_SHIFT.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_OPERATION_SHIFT GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_OPERATION_SHIFT.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_MA_OPERATION_SHIFT GetByMonthIdYearId(QMSDBEntities dataContext, byte MONTH, short YEAR)
        {
            return dataContext.QMS_MA_OPERATION_SHIFT.Where(m => m.MONTH == MONTH && m.YEAR == YEAR && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static List<QMS_MA_OPERATION_SHIFT> GetAllByYear(QMSDBEntities db, int year)
        {
            return db.QMS_MA_OPERATION_SHIFT.Where(m => m.DELETE_FLAG == 0 && m.YEAR == year).OrderBy(m => m.MONTH).ToList(); //linq
        }

        public static List<QMS_MA_OPERATION_SHIFT> GetAllByStartEndYear(QMSDBEntities db, int startYear, int endYear)
        {
            return db.QMS_MA_OPERATION_SHIFT.Where(m => m.DELETE_FLAG == 0 && m.YEAR >= startYear && m.YEAR <= endYear).OrderBy(m => m.MONTH).ToList(); //linq
        }

        public static List<short> GetAllGroupYear(QMSDBEntities db )
        {
            return db.QMS_MA_OPERATION_SHIFT.Where(m => m.DELETE_FLAG == 0 ).GroupBy( m => m.YEAR).OrderBy(m => m.Key).Select( m => m.Key ).ToList(); //linq
        }
    }

    public partial class QMS_MA_PLANT 
    {
        public static List<QMS_MA_PLANT> GetAllByPlantId(QMSDBEntities db, long Plant_id)
        {
            return db.QMS_MA_PLANT.Where(m => m.DELETE_FLAG == 0 && m.ID == Plant_id).ToList(); //linq
        }
        public static List<QMS_MA_PLANT> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_PLANT.OrderBy(m => m.POSITION).ToList(); //linq
            //return db.QMS_MA_PLANT.Where(m => m.DELETE_FLAG == 0).OrderBy(m => m.POSITION).ToList(); //linq
        }

        public static List<QMS_MA_PLANT> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_PLANT.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).OrderBy(m => m.POSITION).ToList(); //linq
        }

        public static QMS_MA_PLANT GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_PLANT.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_MA_PLANT GetByPlantName(QMSDBEntities dataContext, string plantName)
        {
            return dataContext.QMS_MA_PLANT.Where(m => m.NAME.ToLower() == plantName.ToLower() && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static QMS_MA_PLANT GetByPlantNameAndSkipPlantId(QMSDBEntities dataContext, string plantName, long plantId)
        {
            return dataContext.QMS_MA_PLANT.Where(m => m.NAME.ToLower() == plantName.ToLower() && m.DELETE_FLAG == 0 &&  m.ID != plantId ).FirstOrDefault();
        }

        public static List<QMS_MA_PLANT> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_PLANT.Where(m => ids.Contains(m.ID)).ToList();
        }
    }

    public partial class QMS_MA_PRODUCT 
    {
        public static List<QMS_MA_PRODUCT> GetAllByProductId(QMSDBEntities db, long Product_id)
        {
            return db.QMS_MA_PRODUCT.Where(m => m.DELETE_FLAG == 0 && m.ID == Product_id).ToList(); //linq
        }

        public static QMS_MA_PRODUCT GetByProductName(QMSDBEntities dataContext, string productName)
        {
            return dataContext.QMS_MA_PRODUCT.Where(m => m.NAME.ToLower() == productName.ToLower() && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static QMS_MA_PRODUCT GetByProductNameAndSkipProductId(QMSDBEntities dataContext, string productName, long productId)
        {
            return dataContext.QMS_MA_PRODUCT.Where(m => m.NAME.ToLower() == productName.ToLower() && m.DELETE_FLAG == 0 && m.ID != productId).FirstOrDefault();
        }

        public static List<QMS_MA_PRODUCT> GetAll(QMSDBEntities db)
        {
            //return db.QMS_MA_PRODUCT.Where(m => m.DELETE_FLAG == 0).OrderBy(m => m.POSITION).ToList(); //linq
            return db.QMS_MA_PRODUCT.OrderBy(m => m.POSITION).ToList(); //linq
        }

        public static List<QMS_MA_PRODUCT> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_PRODUCT.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).OrderBy(m => m.POSITION).ToList(); //linq
        }

        public static QMS_MA_PRODUCT GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_PRODUCT.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_MA_PRODUCT GetProductLPG(QMSDBEntities dataContext )
        {
            return dataContext.QMS_MA_PRODUCT.Where(m => m.NAME.ToLower() == "lpg").FirstOrDefault();
        }

        public static List<QMS_MA_PRODUCT> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_PRODUCT.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_PRODUCT> GetAllByNotinProductId(QMSDBEntities db, long product_id)
        {
            return db.QMS_MA_PRODUCT.Where(m => m.DELETE_FLAG == 0 && m.ID != product_id && m.ID > 2).OrderBy(m => m.POSITION).ToList(); //linq
        }
    }

    public partial class QMS_MA_PRODUCT_MAPPING 
    {
        public static List<QMS_MA_PRODUCT_MAPPING> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_PRODUCT_MAPPING.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_PRODUCT_MAPPING> GetAllHI(QMSDBEntities db)
        {
            //return db.QMS_MA_PRODUCT_MAPPING.Where(m => m.DELETE_FLAG == 0 && m.HIGHLOW_CHECK == 0 && m.ID > 2).ToList(); //linq
            return db.QMS_MA_PRODUCT_MAPPING.Where(m => m.DELETE_FLAG == 0 && m.HIGHLOW_CHECK == 0  ).ToList(); //linq
        }

        public static List<QMS_MA_PRODUCT_MAPPING> GetAllLOW(QMSDBEntities db)
        {
            return db.QMS_MA_PRODUCT_MAPPING.Where(m => m.DELETE_FLAG == 0 && m.HIGHLOW_CHECK == 1).ToList(); //linq
            //return db.QMS_MA_PRODUCT_MAPPING.Where(m => m.DELETE_FLAG == 0 && m.HIGHLOW_CHECK == 1 && m.ID > 2).ToList(); //linq
        }

        public static QMS_MA_PRODUCT_MAPPING GetByPlantIdProductId (QMSDBEntities dataContext, long plantId, long productId )
        {
            return dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => m.PLANT_ID == plantId && m.PRODUCT_ID == productId  && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static QMS_MA_PRODUCT_MAPPING GetByPlantIdProductIdSkipId(QMSDBEntities dataContext, long plantId, long productId, long id)
        {
            return dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => m.ID != id && m.PLANT_ID == plantId && m.PRODUCT_ID == productId && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static QMS_MA_PRODUCT_MAPPING GetByPlantIdProductIdGradeId(QMSDBEntities dataContext, long plantId, long productId,  long gradeId)
        {
            return dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => m.PLANT_ID == plantId && m.PRODUCT_ID == productId && m.GRADE_ID == gradeId && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static QMS_MA_PRODUCT_MAPPING GetByPlantIdProductIdGradeIdSkipId(QMSDBEntities dataContext, long plantId, long productId, long gradeId, long id)
        {
            return dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => m.ID != id && m.PLANT_ID == plantId && m.PRODUCT_ID == productId && m.GRADE_ID == gradeId && m.DELETE_FLAG == 0).FirstOrDefault();
        }

        public static QMS_MA_PRODUCT_MAPPING GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_PRODUCT_MAPPING> GetByListPlantId(QMSDBEntities dataContext, long plantId)
        {
            return dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => m.PLANT_ID == plantId).ToList();
        } 

        public static List<QMS_MA_PRODUCT_MAPPING> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_PRODUCT_MAPPING> GetAllBySearch(QMSDBEntities dataContext, PlantProductSearchModel searchModel)
        {
            List<QMS_MA_PRODUCT_MAPPING> lstResult = new List<QMS_MA_PRODUCT_MAPPING>();

            lstResult = dataContext.QMS_MA_PRODUCT_MAPPING.Where(m => m.DELETE_FLAG == 0).ToList(); 

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (searchModel.PLANT_ID > 2)
                {
                    lstResult = lstResult.Where(m => m.PLANT_ID == searchModel.PLANT_ID).ToList();
                }

                if (searchModel.PRODUCT_ID > 2)
                {
                    lstResult = lstResult.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID).ToList();
                }

                if (searchModel.GRADE_ID > 2)
                {
                    lstResult = lstResult.Where(m => m.GRADE_ID == searchModel.GRADE_ID).ToList();
                }

            }
            return lstResult; 
        }
    }

    public partial class QMS_MA_REDUCE_FEED 
    {
        public static List<QMS_MA_REDUCE_FEED> GetAll(QMSDBEntities db)
        { 
            return db.QMS_MA_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_REDUCE_FEED GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_REDUCE_FEED.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_REDUCE_FEED> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_REDUCE_FEED.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_REDUCE_FEED> GetByListPlantId(QMSDBEntities dataContext, long plantId)
        {
            return dataContext.QMS_MA_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0 && m.PLANT_ID == plantId).ToList();
        }

        public static List<QMS_MA_REDUCE_FEED> GetAllBySearch(QMSDBEntities db, ReduceFeedSearchModel searchModel)
        {
            List<QMS_MA_REDUCE_FEED> lstResult = new List<QMS_MA_REDUCE_FEED>();

            var query = db.QMS_MA_REDUCE_FEED.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (null != searchModel.PLANT_ID && searchModel.PLANT_ID > 0)
                {
                    query = query.Where(m => m.PLANT_ID == searchModel.PLANT_ID);
                }

                if (null != searchModel.EXCEL_NAME && "" != searchModel.EXCEL_NAME)
                {
                    query = query.Where(m => m.EXCEL_NAME.Contains(searchModel.EXCEL_NAME));
                }

                if (null != searchModel.LIMIT_VALUE && searchModel.LIMIT_VALUE > 0)
                {
                    query = query.Where(m => m.LIMIT_VALUE == searchModel.LIMIT_VALUE) ;
                }

                //if (null != searchModel.EXA_TAG_NAME && "" != searchModel.EXA_TAG_NAME)
                //{
                //    lstResult = lstResult.Where(m => m.EXA_TAG_NAME.Contains(searchModel.EXA_TAG_NAME)).ToList();
                //}

                if (null != searchModel.ACTIVE_DATE)
                {
                    query = query.Where(m => m.ACTIVE_DATE == searchModel.ACTIVE_DATE) ;
                }

                if (null != searchModel.REDUCE_FEED_DESC && "" != searchModel.REDUCE_FEED_DESC)
                {
                    query = query.Where(m => m.REDUCE_FEED_DESC.Contains(searchModel.REDUCE_FEED_DESC)) ;
                }
                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }
       
    }

    public partial class QMS_MA_REDUCE_FEED_DETAIL 
    {
        public static List<QMS_MA_REDUCE_FEED_DETAIL> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_REDUCE_FEED_DETAIL.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_REDUCE_FEED_DETAIL> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_REDUCE_FEED_DETAIL.Where(m => m.DELETE_FLAG == 0).OrderBy(m => m.EXA_TAG_NAME).ToList(); //linq
        }

        public static QMS_MA_REDUCE_FEED_DETAIL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_REDUCE_FEED_DETAIL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_REDUCE_FEED_DETAIL> GetActiveAllByREDUCE_FEED_ID(QMSDBEntities db, long reduceId)  
        {
            return db.QMS_MA_REDUCE_FEED_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.REDUCE_FEED_ID == reduceId).ToList(); //linq
        }

        public static List<QMS_MA_REDUCE_FEED_DETAIL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_REDUCE_FEED_DETAIL.Where(m => ids.Contains(m.ID)).ToList();
        }


        public static List<QMS_MA_REDUCE_FEED_DETAIL> GetAllBySearch(QMSDBEntities db, ReduceFeedDetailSearchModel searchModel)
        {
            List<QMS_MA_REDUCE_FEED_DETAIL> lstResult = new List<QMS_MA_REDUCE_FEED_DETAIL>();

            var query = db.QMS_MA_REDUCE_FEED_DETAIL.Where(m => m.DELETE_FLAG == 0 && m.REDUCE_FEED_ID == searchModel.REDUCE_FEED_ID);

            try
            {

                if (null != searchModel.CONVERT_VALUE && searchModel.CONVERT_VALUE > 0)
                {
                    query = query.Where(m => m.CONVERT_VALUE == searchModel.CONVERT_VALUE) ;
                }

                if (null != searchModel.EXA_TAG_NAME && "" != searchModel.EXA_TAG_NAME)
                {
                    query = query.Where(m => m.EXA_TAG_NAME.Contains(searchModel.EXA_TAG_NAME)) ;
                }
                lstResult = query.ToList();
            }
            catch
            {

            }
            return lstResult;
        }
       
    }

    public partial class QMS_MA_ROOT_CAUSE 
    {
        public static List<QMS_MA_ROOT_CAUSE> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_ROOT_CAUSE.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_ROOT_CAUSE> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_ROOT_CAUSE.Where(m => m.DELETE_FLAG == 0 && m.ID > 0).OrderBy(m => m.NAME).ToList(); //linq
        }

        public static QMS_MA_ROOT_CAUSE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_ROOT_CAUSE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_ROOT_CAUSE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_ROOT_CAUSE.Where(m => ids.Contains(m.ID)).ToList();
        }


        public static List<QMS_MA_ROOT_CAUSE> GetAllBySearch(QMSDBEntities db, RootCauseSearchModel searchModel)
        {
            List<QMS_MA_ROOT_CAUSE> lstResult = new List<QMS_MA_ROOT_CAUSE>();

            var query = db.QMS_MA_ROOT_CAUSE.Where(m => m.DELETE_FLAG == 0) ;

            try
            {

                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    query = query.Where(m => m.NAME.Contains(searchModel.NAME)) ;
                }

                if (null != searchModel.ROOT_CAUSE_TYPE_ID && searchModel.ROOT_CAUSE_TYPE_ID > 0)
                {
                    query = query.Where(m => m.ROOT_CAUSE_TYPE_ID == searchModel.ROOT_CAUSE_TYPE_ID) ;
                }

                if (null != searchModel.ROOT_CAUSE_DESC && "" != searchModel.ROOT_CAUSE_DESC)
                {
                    query = query.Where(m => m.ROOT_CAUSE_DESC.Contains(searchModel.ROOT_CAUSE_DESC)) ;
                }
                lstResult = query.ToList();
            }
            catch
            {
            }
            return lstResult;
        }
       
    }

    public partial class QMS_MA_ROOT_CAUSE_TYPE 
    {
        public static List<QMS_MA_ROOT_CAUSE_TYPE> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_ROOT_CAUSE_TYPE.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).ToList(); //linq
        }

        public static List<QMS_MA_ROOT_CAUSE_TYPE> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_ROOT_CAUSE_TYPE.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).OrderBy(m => m.NAME).ToList(); //linq
        }

        public static QMS_MA_ROOT_CAUSE_TYPE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_ROOT_CAUSE_TYPE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_ROOT_CAUSE_TYPE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_ROOT_CAUSE_TYPE.Where(m => ids.Contains(m.ID)).ToList();
        }
        public static List<QMS_MA_ROOT_CAUSE_TYPE> GetAllBySearch(QMSDBEntities db, RootCauseTypeSearchModel searchModel)
        {
            List<QMS_MA_ROOT_CAUSE_TYPE> lstResult = new List<QMS_MA_ROOT_CAUSE_TYPE>();

            lstResult = db.QMS_MA_ROOT_CAUSE_TYPE.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {
                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    lstResult = lstResult.Where(m => m.NAME.Contains(searchModel.NAME)).ToList();
                }
            }
            return lstResult;
        }
     
    }

    public partial class QMS_MA_TEMPLATE 
    {
        public static List<QMS_MA_TEMPLATE> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_TEMPLATE.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_TEMPLATE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_TEMPLATE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_TEMPLATE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_TEMPLATE.Where(m => ids.Contains(m.ID)).ToList();
        }
        public static List<QMS_MA_TEMPLATE> GetByListType(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_TEMPLATE.Where(m => ids.Contains(m.TEMPLATE_TYPE) && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_TEMPLATE> GetAllByTemplateType(QMSDBEntities dataContext, byte TEMPLATE_TYPE)
        {
            return dataContext.QMS_MA_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_TEMPLATE> GetAllByTemplateTypeAndName(QMSDBEntities dataContext, string TEMPLATE_NAME, byte TEMPLATE_TYPE)
        {
            return dataContext.QMS_MA_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && m.DELETE_FLAG == 0 && m.NAME == TEMPLATE_NAME ).ToList();
        }

        public static List<QMS_MA_TEMPLATE> GetAllByTemplateTypeAndTEMPLATE_AREA(QMSDBEntities dataContext, string TEMPLATE_AREA, byte TEMPLATE_TYPE)
        {
            return dataContext.QMS_MA_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && m.DELETE_FLAG == 0 && m.TEMPLATE_AREA == TEMPLATE_AREA).ToList();
        }

        public static List<QMS_MA_TEMPLATE> GetAllByTemplateTypeAndPlantId(QMSDBEntities dataContext, long PLANT_ID, byte TEMPLATE_TYPE)
        {
            //return dataContext.QMS_MA_TEMPLATE.Where(m => m.PLANT_ID == PLANT_ID && m.TEMPLATE_TYPE == TEMPLATE_TYPE && m.DELETE_FLAG == 0).ToList();
            return dataContext.QMS_MA_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && m.DELETE_FLAG == 0).ToList(); //monchai
        }

        public static List<QMS_MA_TEMPLATE> GetAllByTemplateTypeAndActiveDate(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, byte TEMPLATE_TYPE) //เอาตั้งแต่วันที่เริ่มใช้ จนถึงสิ้นสุด แต่วันที่เริ่มใช้ ไม่ระบุก็ได้
        {
            return dataContext.QMS_MA_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && ((startDate >= m.ACTIVE_DATE || m.ACTIVE_DATE == null) && (endDate <= m.EXPIRE_DATE || m.EXPIRE_DATE == null)) && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_TEMPLATE> GetAllBySearch(QMSDBEntities db, TemplateSearchModel searchModel)
        {
            List<QMS_MA_TEMPLATE> lstResult = new List<QMS_MA_TEMPLATE>();

            lstResult = db.QMS_MA_TEMPLATE.Where(m => m.DELETE_FLAG == 0 && m.TEMPLATE_TYPE == searchModel.TEMPLATE_TYPE).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (null != searchModel.TEMPLATE_AREA && "" != searchModel.TEMPLATE_AREA)
                {
                    lstResult = lstResult.Where(m => m.TEMPLATE_AREA.StartsWith(searchModel.TEMPLATE_AREA) || m.TEMPLATE_AREA.EndsWith(searchModel.TEMPLATE_AREA)).ToList();
                } 

                if (null != searchModel.NAME && "" != searchModel.NAME)
                { 
                    lstResult = lstResult.Where(m => m.NAME.StartsWith(searchModel.NAME) || m.NAME.EndsWith(searchModel.NAME)).ToList();
                }

                if (null != searchModel.FILE_NAME && "" != searchModel.FILE_NAME)
                { 
                    lstResult = lstResult.Where(m => m.FILE_NAME.StartsWith(searchModel.FILE_NAME) || m.FILE_NAME.EndsWith(searchModel.FILE_NAME)).ToList();
                } 
            }
            return lstResult;
        } 
    }

    public partial class QMS_MA_TEMPLATE_COLUMN 
    {
        public static List<QMS_MA_TEMPLATE_COLUMN> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_TEMPLATE_COLUMN.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_TEMPLATE_COLUMN> GetAllByTemplateId(QMSDBEntities db, long template_id)
        {
            return db.QMS_MA_TEMPLATE_COLUMN.Where(m => m.DELETE_FLAG == 0 && m.TEMPLATE_ID == template_id).ToList(); //linq
        }
        public static List<QMS_MA_TEMPLATE_COLUMN> GetByListTemplateId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_TEMPLATE_COLUMN.Where(m => m.DELETE_FLAG == 0 && ids.Contains(m.TEMPLATE_ID)).ToList();
        }
        public static List<QMS_MA_TEMPLATE_COLUMN> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_TEMPLATE_COLUMN.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_TEMPLATE_COLUMN> GetByName(QMSDBEntities dataContext, string Name)
        {
            return dataContext.QMS_MA_TEMPLATE_COLUMN.Where(m => m.NAME == Name).ToList();
        }
        public static List<QMS_MA_TEMPLATE_COLUMN> GetBySampleName(QMSDBEntities dataContext, string SampleName)
        {
            return dataContext.QMS_MA_TEMPLATE_COLUMN.Where(m => m.SAMPLE_NAME == SampleName).ToList();
        }

        public static QMS_MA_TEMPLATE_COLUMN GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_TEMPLATE_COLUMN.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_MA_TEMPLATE_COLUMN GetByNameTemplateId(QMSDBEntities dataContext, string Name, long template_id)
        {
            return dataContext.QMS_MA_TEMPLATE_COLUMN.Where(m => m.NAME == Name && m.TEMPLATE_ID == template_id).FirstOrDefault();
        }
        public static QMS_MA_TEMPLATE_COLUMN GetByNameTemplateIdSampleArea(QMSDBEntities dataContext, string Name, long template_id, string Sample, string Area)
        {
            return dataContext.QMS_MA_TEMPLATE_COLUMN.Where(m => m.NAME == Name && m.TEMPLATE_ID == template_id && m.SAMPLE_NAME == Sample && m.AREA_NAME == Area).FirstOrDefault();
        }
    }

    public partial class QMS_MA_TEMPLATE_CONTROL 
    {
        public static List<QMS_MA_TEMPLATE_CONTROL> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_TEMPLATE_CONTROL.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_TEMPLATE_CONTROL> GetAllByTemplateId(QMSDBEntities db, long id)
        {
            return db.QMS_MA_TEMPLATE_CONTROL.Where(m => m.DELETE_FLAG == 0 && m.TEMPLATE_ID == id).ToList(); //linq
        }

        public static QMS_MA_TEMPLATE_CONTROL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_TEMPLATE_CONTROL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_TEMPLATE_CONTROL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_TEMPLATE_CONTROL.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_TEMPLATE_CONTROL> GetByListSampleId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_TEMPLATE_CONTROL.Where(m => ids.Contains(m.SAMPLE_ID) && m.DELETE_FLAG == 0).ToList();
        }
    }

    public partial class QMS_MA_TEMPLATE_ROW 
    {
        public static List<QMS_MA_TEMPLATE_ROW> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_TEMPLATE_ROW.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_MA_TEMPLATE_ROW> GetAllByTemplateId(QMSDBEntities db, long template_id)
        {
            return db.QMS_MA_TEMPLATE_ROW.Where(m => m.DELETE_FLAG == 0 && m.TEMPLATE_ID == template_id).ToList(); //linq
        }

        public static List<QMS_MA_TEMPLATE_ROW> GetAllByListTemplateId(QMSDBEntities db, long[] template_id)
        {
            return db.QMS_MA_TEMPLATE_ROW.Where(m =>  m.DELETE_FLAG == 0 && template_id.Contains(m.TEMPLATE_ID)).ToList(); //linq
        }

        public static QMS_MA_TEMPLATE_ROW GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_TEMPLATE_ROW.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_TEMPLATE_ROW> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_TEMPLATE_ROW.Where(m => m.DELETE_FLAG == 0 && ids.Contains(m.ID)).ToList();
        }
    }

    public partial class QMS_MA_UNIT 
    {
        public static List<QMS_MA_UNIT> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_UNIT.Where(m => m.DELETE_FLAG == 0 && m.ID > 2).ToList(); //linq
        }

        public static List<QMS_MA_UNIT> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_UNIT.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static  QMS_MA_UNIT  CheckDuplicateUnitName(QMSDBEntities db, long id , string name  )
        {
            return db.QMS_MA_UNIT.Where(m => m.DELETE_FLAG == 0 && m.ID != id && m.NAME.ToLower().Trim() == name.ToLower().Trim()).FirstOrDefault(); //linq
        }

        public static QMS_MA_UNIT GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_UNIT.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_UNIT> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_UNIT.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_UNIT> GetAllBySearch(QMSDBEntities db, UnitSearchModel searchModel)
        {
            List<QMS_MA_UNIT> lstResult = new List<QMS_MA_UNIT>();

            lstResult = db.QMS_MA_UNIT.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (null != searchModel.Name && "" != searchModel.Name)
                {
                    lstResult = lstResult.Where(m => m.NAME.Contains(searchModel.Name)).ToList();
                }

            }
            return lstResult;
        } 
    }

    //public partial class QMS_TR_BLUEPRINT_ABNORMAL 
    //{
    //    public static List<QMS_TR_BLUEPRINT_ABNORMAL> GetAll(QMSDBEntities db)
    //    {
    //        return db.QMS_TR_BLUEPRINT_ABNORMAL.ToList(); 
    //    }

    //    public static QMS_TR_BLUEPRINT_ABNORMAL GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_TR_BLUEPRINT_ABNORMAL.Where(m => m.BlueprintId == id).FirstOrDefault();
    //    }
    //}

    public partial class QMS_MA_IQC_RULE 
    {
        public static List<QMS_MA_IQC_RULE> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_RULE.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_RULE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_RULE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_RULE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_RULE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_RULE> GetByListRuleId(QMSDBEntities dataContext, string RuleId)
        {
            return dataContext.QMS_MA_IQC_RULE.Where(m => m.RULE_ID == RuleId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_RULE> GetAllBySearch(QMSDBEntities db, IQCRuleSearchModel searchModel)
        {
            List<QMS_MA_IQC_RULE> lstResult = new List<QMS_MA_IQC_RULE>();

            var query = db.QMS_MA_IQC_RULE.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (null != searchModel.RULE_ID && "" != searchModel.RULE_ID)
                {
                    query = query.Where(m => m.RULE_ID.Contains(searchModel.RULE_ID));
                }

                if (null != searchModel.RULE_NAME && "" != searchModel.RULE_NAME)
                {
                    query = query.Where(m => m.RULE_NAME.Contains(searchModel.RULE_NAME));
                }

                if (null != searchModel.RULE_DESC && "" != searchModel.RULE_DESC)
                {
                    query = query.Where(m => m.RULE_DESC.Contains(searchModel.RULE_DESC));
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }


    }


    public partial class QMS_MA_IQC_METHOD 
    {
        public static List<QMS_MA_IQC_METHOD> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_METHOD.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_METHOD GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_METHOD.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_METHOD> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_METHOD.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_METHOD> GetByListPossibleCauseId(QMSDBEntities dataContext, byte PossibleCauseId)
        {
            return dataContext.QMS_MA_IQC_METHOD.Where(m => m.METHOD_ID == PossibleCauseId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_METHOD> GetAllBySearch(QMSDBEntities db, IQCMethodSearchModel searchModel)
        {
            List<QMS_MA_IQC_METHOD> lstResult = new List<QMS_MA_IQC_METHOD>();

            var query = db.QMS_MA_IQC_METHOD.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (searchModel.METHOD_ID > 0)
                {
                    query = query.Where(m => m.METHOD_ID == searchModel.METHOD_ID);
                }

                if (null != searchModel.METHOD_NAME && "" != searchModel.METHOD_NAME)
                {
                    query = query.Where(m => m.METHOD_NAME.Contains(searchModel.METHOD_NAME));
                }

                if (null != searchModel.METHOD_DESC && "" != searchModel.METHOD_DESC)
                {
                    query = query.Where(m => m.METHOD_DESC.Contains(searchModel.METHOD_DESC));
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_MA_IQC_METHOD> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_IQC_METHOD.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).OrderBy(m => m.POSITION).ToList(); //linq
        }

    }


    public partial class QMS_MA_IQC_POSSICAUSE 
    {
        public static List<QMS_MA_IQC_POSSICAUSE> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_POSSICAUSE.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_POSSICAUSE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_POSSICAUSE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_POSSICAUSE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_POSSICAUSE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_POSSICAUSE> GetByListPossibleCauseId(QMSDBEntities dataContext, byte PossibleCauseId)
        {
            return dataContext.QMS_MA_IQC_POSSICAUSE.Where(m => m.POSSICAUSE_ID == PossibleCauseId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_POSSICAUSE> GetAllBySearch(QMSDBEntities db, IQCPossibleCauseSearchModel searchModel)
        {
            List<QMS_MA_IQC_POSSICAUSE> lstResult = new List<QMS_MA_IQC_POSSICAUSE>();

            var query = db.QMS_MA_IQC_POSSICAUSE.Where(m => m.DELETE_FLAG == 0);
          
            try
            {

                if (searchModel.POSSICAUSE_ID > 0 )
                {
                    query = query.Where(m => m.POSSICAUSE_ID == searchModel.POSSICAUSE_ID);
                }

                if (null != searchModel.POSSICAUSE_NAME && "" != searchModel.POSSICAUSE_NAME)
                {
                    query = query.Where(m => m.POSSICAUSE_NAME.Contains(searchModel.POSSICAUSE_NAME));
                }

                if (null != searchModel.POSSICAUSE_DESC && "" != searchModel.POSSICAUSE_DESC)
                {
                    query = query.Where(m => m.POSSICAUSE_DESC.Contains(searchModel.POSSICAUSE_DESC));
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_MA_IQC_POSSICAUSE> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_IQC_POSSICAUSE.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).OrderBy(m => m.ID).ToList(); //linq
        }

    }


    public partial class QMS_MA_IQC_FORMULA 
    {
        public static List<QMS_MA_IQC_FORMULA> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_FORMULA.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_FORMULA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_FORMULA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_FORMULA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_FORMULA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_FORMULA> GetByListFormulaId(QMSDBEntities dataContext, long MethodId)
        {
            return dataContext.QMS_MA_IQC_FORMULA.Where(m => m.METHOD_ID == MethodId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_FORMULA> GetAllBySearch(QMSDBEntities db, IQCFormulaSearchModel searchModel)
        {
            List<QMS_MA_IQC_FORMULA> lstResult = new List<QMS_MA_IQC_FORMULA>();

            var query = db.QMS_MA_IQC_FORMULA.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (searchModel.METHOD_ID > 0)
                {
                    query = query.Where(m => m.METHOD_ID == searchModel.METHOD_ID);
                }

                if (null != searchModel.FORMULA_REV && "" != searchModel.FORMULA_REV)
                {
                    query = query.Where(m => m.FORMULA_REV.Contains(searchModel.FORMULA_REV));
                }

                if (searchModel.FORMULA_PROD > 0)
                {
                    query = query.Where(m => m.FORMULA_PROD == searchModel.FORMULA_PROD);
                }

                if (searchModel.FORMULA_SAMPLE > 0)
                {
                    query = query.Where(m => m.FORMULA_SAMPLE == searchModel.FORMULA_SAMPLE);
                }

                if (searchModel.FORMULA_STD > 0)
                {
                    query = query.Where(m => m.FORMULA_SAMPLE == searchModel.FORMULA_STD);
                }

                if (searchModel.FORMULA_UP > 0)
                {
                    query = query.Where(m => m.FORMULA_UWL == searchModel.FORMULA_UP);
                }

                if (searchModel.FORMULA_LCL > 0)
                {
                    query = query.Where(m => m.FORMULA_LCL == searchModel.FORMULA_LCL);

                }
                if (searchModel.FORMULA_UWL > 0)
                {
                    query = query.Where(m => m.FORMULA_UWL == searchModel.FORMULA_UWL);
                }

                if (searchModel.FORMULA_AVG > 0)
                {
                    query = query.Where(m => m.FORMULA_AVG == searchModel.FORMULA_AVG);
                }

                if (searchModel.FORMULA_LWL > 0)
                {
                    query = query.Where(m => m.FORMULA_LWL == searchModel.FORMULA_LWL);
                }

                if (searchModel.FORMULA_LCL > 0)
                {
                    query = query.Where(m => m.FORMULA_LCL == searchModel.FORMULA_LCL);
                }

                if (searchModel.FORMULA_LOW > 0)
                {
                    query = query.Where(m => m.FORMULA_LOW == searchModel.FORMULA_LOW);
                }

                if (null != searchModel.FORMULA_DESC && "" != searchModel.FORMULA_DESC)
                {
                    query = query.Where(m => m.FORMULA_DESC.Contains(searchModel.FORMULA_DESC));
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        

    }


    public partial class QMS_MA_IQC_MAILALERT 
    {
        public static List<QMS_MA_IQC_MAILALERT> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_MAILALERT.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_MAILALERT GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_MAILALERT.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_MAILALERT> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_MAILALERT.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_MAILALERT> GetByListMailAlertId(QMSDBEntities dataContext, byte AlertId)
        {
            return dataContext.QMS_MA_IQC_MAILALERT.Where(m => m.EMAIL_ID == AlertId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_MAILALERT> GetAllBySearch(QMSDBEntities db, IQCMailAlertSearchModel searchModel)
        {
            List<QMS_MA_IQC_MAILALERT> lstResult = new List<QMS_MA_IQC_MAILALERT>();

            var query = db.QMS_MA_IQC_MAILALERT.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if ( searchModel.EMAIL_ID  > 0 )
                {
                    query = query.Where(m => m.EMAIL_ID == searchModel.EMAIL_ID );
                }

                if (searchModel.POSSICAUSE_ID > 0)
                {
                    query = query.Where(m => m.POSSICAUSE_ID == searchModel.POSSICAUSE_ID);
                }

                if (null != searchModel.ALERT_DESC && "" != searchModel.ALERT_DESC)
                {
                    query = query.Where(m => m.ALERT_DESC.Contains(searchModel.ALERT_DESC));
                }
                                

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

  
    }


    public partial class QMS_MA_IQC_ITEM 
    {
        public static List<QMS_MA_IQC_ITEM> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_ITEM.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_ITEM GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_ITEM.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_ITEM> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_ITEM.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_ITEM> GetByListItemId(QMSDBEntities dataContext, long ItemId)
        {
            return dataContext.QMS_MA_IQC_ITEM.Where(m => m.ITEM_ID == ItemId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_ITEM> GetAllBySearch(QMSDBEntities db, IQCItemSearchModel searchModel)
        {
            List<QMS_MA_IQC_ITEM> lstResult = new List<QMS_MA_IQC_ITEM>();

            var query = db.QMS_MA_IQC_ITEM.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if ( searchModel.ITEM_ID > 0 )
                {
                    query = query.Where(m => m.ITEM_ID == searchModel.ITEM_ID);
                }

                if (null != searchModel.ITEM_NAME && "" != searchModel.ITEM_NAME)
                {
                    query = query.Where(m => m.ITEM_NAME.Contains(searchModel.ITEM_NAME));
                }

                if (null != searchModel.ITEM_DESC && "" != searchModel.ITEM_DESC)
                {
                    query = query.Where(m => m.ITEM_DESC.Contains(searchModel.ITEM_DESC));
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_MA_IQC_ITEM> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_IQC_ITEM.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
        }
    }


    public partial class QMS_MA_IQC_EQUIP 
    {
        public static List<QMS_MA_IQC_EQUIP> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_EQUIP.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_EQUIP GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_EQUIP.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_EQUIP> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_EQUIP.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_EQUIP> GetByListEquipId(QMSDBEntities dataContext, long EquipId)
        {
            return dataContext.QMS_MA_IQC_EQUIP.Where(m => m.EQUIP_ID == EquipId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_EQUIP> GetAllBySearch(QMSDBEntities db, IQCEquipSearchModel searchModel)
        {
            List<QMS_MA_IQC_EQUIP> lstResult = new List<QMS_MA_IQC_EQUIP>();

            var query = db.QMS_MA_IQC_EQUIP.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (searchModel.EQUIP_ID > 0)
                {
                    query = query.Where(m => m.EQUIP_ID == searchModel.EQUIP_ID);
                }

                if (null != searchModel.EQUIP_NAME && "" != searchModel.EQUIP_NAME)
                {
                    query = query.Where(m => m.EQUIP_NAME.Contains(searchModel.EQUIP_NAME));
                }

                if (null != searchModel.EQUIP_DESC && "" != searchModel.EQUIP_DESC)
                {
                    query = query.Where(m => m.EQUIP_DESC.Contains(searchModel.EQUIP_DESC));
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_MA_IQC_EQUIP> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_IQC_EQUIP.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).OrderBy(m => m.POSITION).ToList(); //linq
        }

    }


    public partial class QMS_MA_IQC_ITEMEQUIP 
    {
        public static List<QMS_MA_IQC_ITEMEQUIP> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_ITEMEQUIP.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_ITEMEQUIP GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_ITEMEQUIP.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_ITEMEQUIP> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_ITEMEQUIP.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_ITEMEQUIP> GetByListItemEquipId(QMSDBEntities dataContext, long ItemId)
        {
            return dataContext.QMS_MA_IQC_ITEMEQUIP.Where(m => m.ITEM_ID == ItemId && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_ITEMEQUIP> GetAllBySearch(QMSDBEntities db, IQCItemEquipSearchModel searchModel)
        {
            List<QMS_MA_IQC_ITEMEQUIP> lstResult = new List<QMS_MA_IQC_ITEMEQUIP>();

            var query = db.QMS_MA_IQC_ITEMEQUIP.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (searchModel.ITEM_ID > 0)
                {
                    query = query.Where(m => m.ITEM_ID == searchModel.ITEM_ID);
                }

                if (searchModel.EQUIP_ID > 0)
                {
                    query = query.Where(m => m.ITEM_ID == searchModel.ITEM_ID);
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_MA_IQC_ITEMEQUIP> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_IQC_ITEMEQUIP.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
        }

      
    }


    public partial class QMS_MA_IQC_TEMPLATE 
    {
        public static List<QMS_MA_IQC_TEMPLATE> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_IQC_TEMPLATE.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_IQC_TEMPLATE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_TEMPLATE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static QMS_MA_IQC_TEMPLATE GetByTemplateType(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_TEMPLATE.Where(m => m.TEMPLATE_TYPE == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_TEMPLATE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_TEMPLATE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_TEMPLATE> GetAllByTemplateType(QMSDBEntities dataContext, byte TEMPLATE_TYPE)
        {
            return dataContext.QMS_MA_IQC_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_TEMPLATE> GetAllByTemplateTypeAndName(QMSDBEntities dataContext, string TEMPLATE_NAME, byte TEMPLATE_TYPE)
        {
            return dataContext.QMS_MA_IQC_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && m.DELETE_FLAG == 0 && m.NAME == TEMPLATE_NAME).ToList();
        }

        public static List<QMS_MA_IQC_TEMPLATE> GetAllByIQCTemplateTypeAndActiveDate(QMSDBEntities dataContext, DateTime startDate, DateTime endDate, byte TEMPLATE_TYPE) //เอาตั้งแต่วันที่เริ่มใช้ จนถึงสิ้นสุด แต่วันที่เริ่มใช้ ไม่ระบุก็ได้
        {
            return dataContext.QMS_MA_IQC_TEMPLATE.Where(m => m.TEMPLATE_TYPE == TEMPLATE_TYPE && ((startDate >= m.ACTIVE_DATE || m.ACTIVE_DATE == null) && (endDate <= m.EXPIRE_DATE || m.EXPIRE_DATE == null)) && m.DELETE_FLAG == 0).ToList();
        }

        public static List<QMS_MA_IQC_TEMPLATE> GetAllBySearch(QMSDBEntities db, IQCTemplateSearchModel searchModel)
        {
            List<QMS_MA_IQC_TEMPLATE> lstResult = new List<QMS_MA_IQC_TEMPLATE>();

            lstResult = db.QMS_MA_IQC_TEMPLATE.Where(m => m.DELETE_FLAG == 0 && m.TEMPLATE_TYPE == searchModel.TEMPLATE_TYPE).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {
                               
                if (null != searchModel.NAME && "" != searchModel.NAME)
                {
                    lstResult = lstResult.Where(m => m.NAME.StartsWith(searchModel.NAME) || m.NAME.EndsWith(searchModel.NAME)).ToList();
                }

                if (null != searchModel.FILE_NAME && "" != searchModel.FILE_NAME)
                {
                    lstResult = lstResult.Where(m => m.FILE_NAME.StartsWith(searchModel.FILE_NAME) || m.FILE_NAME.EndsWith(searchModel.FILE_NAME)).ToList();
                }

            }
            return lstResult;
        }
        
    }


    public partial class QMS_MA_IQC_CONTROLDATA 
    {
        public static List<QMS_MA_IQC_CONTROLDATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_IQC_CONTROLDATA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_IQC_CONTROLDATA GetById(QMSDBEntities dataContext, long id)
        {
            var data = dataContext.QMS_MA_IQC_CONTROLDATA.Where(m => m.ID == id).FirstOrDefault();

            return data;
        }

        public static List<QMS_MA_IQC_CONTROLDATA> GetAllByTemplateId(QMSDBEntities dataContext, long id)
        {
            var data = dataContext.QMS_MA_IQC_CONTROLDATA.Where(m => m.TEMPLATE_ID == id).ToList();

            return data;
        }

        public static List<QMS_MA_IQC_CONTROLDATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_CONTROLDATA.Where(m => ids.Contains(m.ID)).ToList();
        }
  
        public static List<QMS_MA_IQC_CONTROLDATA> GetAllBySearch(QMSDBEntities db, IQCControlDataSearchModel searchModel)
        {
            List<QMS_MA_IQC_CONTROLDATA> lstResult = new List<QMS_MA_IQC_CONTROLDATA>();

            lstResult = db.QMS_MA_IQC_CONTROLDATA.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (searchModel.TEMPLATE_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.TEMPLATE_ID == searchModel.TEMPLATE_ID).ToList();
                }

                if (null != searchModel.STARTEDGE && null != searchModel.ENDEDGE)
                {
                    lstResult = lstResult.Where(m => (searchModel.STARTEDGE <= m.KEEPTIME && m.KEEPTIME <= searchModel.ENDEDGE)).ToList();
                }

            

            }
            return lstResult;
        }
       

    }


    //public partial class QMS_MA_IQC_CONTROLHISTORY 
    //{
    //    public static List<QMS_MA_IQC_CONTROLHISTORY> GetAll(QMSDBEntities db)
    //    {

    //        return db.QMS_MA_IQC_CONTROLHISTORY.Where(m => m.DELETE_FLAG == 0).ToList();
    //    }

    //    public static QMS_MA_IQC_CONTROLHISTORY GetById(QMSDBEntities dataContext, long id)
    //    {
    //        return dataContext.QMS_MA_IQC_CONTROLHISTORY.Where(m => m.ID == id).FirstOrDefault();
    //    }

    //    public static List<QMS_MA_IQC_CONTROLHISTORY> GetByListId(QMSDBEntities dataContext, long[] ids)
    //    {
    //        return dataContext.QMS_MA_IQC_CONTROLHISTORY.Where(m => ids.Contains(m.ID)).ToList();
    //    }
       
    //    public static List<QMS_MA_IQC_CONTROLHISTORY> GetAllBySearch(QMSDBEntities db, IQCControlHistorySearchModel searchModel)
    //    {
    //        List<QMS_MA_IQC_CONTROLHISTORY> lstResult = new List<QMS_MA_IQC_CONTROLHISTORY>();

    //        var query = db.QMS_MA_IQC_CONTROLHISTORY.Where(m => m.DELETE_FLAG == 0);

    //        try
    //        {

    //            if (searchModel.TEMPLATE_ID > 0)
    //            {
    //                query = query.Where(m => m.TEMPLATE_ID == searchModel.TEMPLATE_ID);
    //            }

    //            if (null != searchModel.TITLE && "" != searchModel.TITLE)
    //            {
    //                query = query.Where(m => m.TITLE.Contains(searchModel.TITLE));
    //            }

    //            lstResult = query.ToList();

    //        }
    //        catch
    //        {

    //        }
    //        return lstResult;
    //    }

    //    public static List<QMS_MA_IQC_CONTROLHISTORY> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
    //    {
    //        return db.QMS_MA_IQC_CONTROLHISTORY.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
    //    }

    //}


    public partial class QMS_MA_IQC_CONTROLRULE 
    {
        public static List<QMS_MA_IQC_CONTROLRULE> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_CONTROLRULE.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_CONTROLRULE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_CONTROLRULE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_CONTROLRULE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_CONTROLRULE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_CONTROLRULE> GetAllBySearch(QMSDBEntities db, IQCControlRuleSearchModel searchModel)
        {
            List<QMS_MA_IQC_CONTROLRULE> lstResult = new List<QMS_MA_IQC_CONTROLRULE>();

            var query = db.QMS_MA_IQC_CONTROLRULE.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (searchModel.RULE_ID > 0)
                {
                    query = query.Where(m => m.RULE_ID == searchModel.RULE_ID);
                }

                //if (searchModel.POSSICAUSE_ID > 0)
                //{
                //    query = query.Where(m => m.POSSICAUSE_ID == searchModel.POSSICAUSE_ID);
                //}

                //if (null != searchModel.REASON && "" != searchModel.REASON)
                //{
                //    query = query.Where(m => m.REASON.Contains(searchModel.REASON));
                //}

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_MA_IQC_CONTROLRULE> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_IQC_CONTROLRULE.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
        }

    }


    public partial class QMS_MA_IQC_CONTROLREASON 
    {
        public static List<QMS_MA_IQC_CONTROLREASON> GetAll(QMSDBEntities db)
        {

            return db.QMS_MA_IQC_CONTROLREASON.Where(m => m.DELETE_FLAG == 0).ToList();
        }

        public static QMS_MA_IQC_CONTROLREASON GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_CONTROLREASON.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_IQC_CONTROLREASON> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_IQC_CONTROLREASON.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_IQC_CONTROLREASON> GetAllBySearch(QMSDBEntities db, IQCControlReasonSearchModel searchModel)
        {
            List<QMS_MA_IQC_CONTROLREASON> lstResult = new List<QMS_MA_IQC_CONTROLREASON>();

            var query = db.QMS_MA_IQC_CONTROLREASON.Where(m => m.DELETE_FLAG == 0);

            try
            {

                if (searchModel.CONTROLRULE_ID > 0)
                {
                    query = query.Where(m => m.CONTROLRULE_ID == searchModel.CONTROLRULE_ID);
                }

                lstResult = query.ToList();

            }
            catch
            {

            }
            return lstResult;
        }

        public static List<QMS_MA_IQC_CONTROLREASON> GetActiveAll(QMSDBEntities db) //ยกเว้น id 1 - ทั้งหมด, 2 - ไม่เลือกทั้งหมด
        {
            return db.QMS_MA_IQC_CONTROLREASON.Where(m => m.DELETE_FLAG == 0 && m.ID >= 1).ToList(); //linq
        }

    }


    public partial class QMS_MA_IQC_DRAFTCONTROLRULE 
    {

        public static QMS_MA_IQC_DRAFTCONTROLRULE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_IQC_DRAFTCONTROLRULE.Where(m => m.ID == id).FirstOrDefault();
        }


    }
    public partial class QMS_MA_SORTING_AREA
    {
        public static List<QMS_MA_SORTING_AREA> GetAllByTemplateType(QMSDBEntities db, long TemplateType)
        {
            return db.QMS_MA_SORTING_AREA.Where(m => m.TEMPLATE_TYPE == TemplateType).OrderByDescending(m => m.POSITION).ToList();
        }
    }
    public partial class QMS_MA_SORTING_SAMPLE
    {
        public static List<QMS_MA_SORTING_SAMPLE> GetAllByTemplateType(QMSDBEntities db, long TemplateType)
        {
            return db.QMS_MA_SORTING_SAMPLE.Where(m => m.TEMPLATE_TYPE == TemplateType).OrderByDescending(m => m.POSITION).ToList();
        }
        public static List<QMS_MA_SORTING_SAMPLE> GetAllByTemplateTypeAreaName(QMSDBEntities db, long TemplateType, string AreaName)
        {
            return db.QMS_MA_SORTING_SAMPLE.Where(m => m.TEMPLATE_TYPE == TemplateType && m.AREA_NAME == AreaName).OrderBy(m => m.POSITION).ToList();
        }
    }
    public partial class QMS_MA_SORTING_ITEM
    {
        public static List<QMS_MA_SORTING_ITEM> GetAllByTemplateType(QMSDBEntities db, long TemplateType)
        {
            return db.QMS_MA_SORTING_ITEM.Where(m => m.TEMPLATE_TYPE == TemplateType).OrderByDescending(m => m.POSITION).ToList();
        }
        public static List<QMS_MA_SORTING_ITEM> GetAllByTemplateTypeAreaNameSampleName(QMSDBEntities db, long TemplateType, string AreaName, string SampleName)
        {
            return db.QMS_MA_SORTING_ITEM.Where(m => m.TEMPLATE_TYPE == TemplateType && m.AREA_NAME == AreaName && m.SAMPLE_NAME == SampleName).OrderBy(m => m.POSITION).ToList();
        }
    }



    public partial class QMS_MA_CPK_INTERVAL_SAMPLING
    {
        public static List<QMS_MA_CPK_INTERVAL_SAMPLING> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_CPK_INTERVAL_SAMPLING.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_CPK_INTERVAL_SAMPLING GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CPK_INTERVAL_SAMPLING.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_CPK_INTERVAL_SAMPLING> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CPK_INTERVAL_SAMPLING.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_CPK_INTERVAL_SAMPLING> GetAllBySearch(QMSDBEntities db, IntervalSamplingSearchModel searchModel)
        {
            List<QMS_MA_CPK_INTERVAL_SAMPLING> lstResult = new List<QMS_MA_CPK_INTERVAL_SAMPLING>();

            lstResult = db.QMS_MA_CPK_INTERVAL_SAMPLING.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (null != searchModel.PLANT_ID && searchModel.PLANT_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.PLANT_ID == searchModel.PLANT_ID).ToList();
                }

                if (null != searchModel.PRODUCT_ID && searchModel.PRODUCT_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID).ToList();
                }

                if (!string.IsNullOrEmpty(searchModel.COMPOSATION))
                {
                    lstResult = lstResult.Where(m => m.COMPOSATION == searchModel.COMPOSATION).ToList();
                }

            }
            return lstResult;
        }


        public static List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING> GetProductMappingBySearchAndControlValue(QMSDBEntities dataContext, long PlanId, long ProductId)
        {
            //List<QMS_MA_EXQ_TAG> lstExqTag = new List<QMS_MA_EXQ_TAG>();
            //List<QMS_MA_PRODUCT_MAPPING> lstProMap = new List<QMS_MA_PRODUCT_MAPPING>();
            List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING> listMappingOrder = new List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING>();

            listMappingOrder = (from ExqTag in dataContext.QMS_MA_EXQ_TAG
                                join ProMap in dataContext.QMS_MA_PRODUCT_MAPPING on ExqTag.PRODUCT_MAPPING_ID equals ProMap.ID into group1
                                from g1 in group1.DefaultIfEmpty()
                                where ExqTag.EXCEL_NAME.Contains("GCO") && ExqTag.CONTROL_ID > 0 
                                select new ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING
                                {
                                    ID = ExqTag.ID,
                                    EXCEL_NAME = ExqTag.EXCEL_NAME,
                                    PLANT_ID = g1.PLANT_ID,
                                    PRODUCT_ID = g1.PRODUCT_ID
                                }).OrderBy(m => m.EXCEL_NAME).ToList();

            if (null != listMappingOrder && listMappingOrder.Count() > 0)
            {

                if (PlanId > 2)
                {
                    listMappingOrder = listMappingOrder.Where(m => m.PLANT_ID == PlanId).ToList();
                }

                if (ProductId > 2)
                {
                    listMappingOrder = listMappingOrder.Where(m => m.PRODUCT_ID == ProductId).ToList();
                }

            }

            listMappingOrder.ForEach(a =>
            {
                string[] tokens = a.EXCEL_NAME.Split('_');
                a.EXCEL_NAME = tokens[2];
            });

            return listMappingOrder;
        }


        public static List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING> GetProductMappingBySearch(QMSDBEntities dataContext, long PlanId, long ProductId)
        {
            //List<QMS_MA_EXQ_TAG> lstExqTag = new List<QMS_MA_EXQ_TAG>();
            //List<QMS_MA_PRODUCT_MAPPING> lstProMap = new List<QMS_MA_PRODUCT_MAPPING>();
            List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING> listMappingOrder = new List<ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING>();

            listMappingOrder = (from ExqTag in dataContext.QMS_MA_EXQ_TAG
                                join ProMap in dataContext.QMS_MA_PRODUCT_MAPPING on ExqTag.PRODUCT_MAPPING_ID equals ProMap.ID into group1
                                from g1 in group1.DefaultIfEmpty()
                                where ExqTag.EXCEL_NAME.Contains("GCO")
                                select new ViewPRODUCT_MAP_WITH_INTERVAL_SAMPLING
                                {
                                    ID = ExqTag.ID,
                                    EXCEL_NAME = ExqTag.EXCEL_NAME,
                                    PLANT_ID = g1.PLANT_ID,
                                    PRODUCT_ID = g1.PRODUCT_ID
                                }).OrderBy(m => m.EXCEL_NAME).ToList();

            if (null != listMappingOrder && listMappingOrder.Count() > 0)
            {

                if (PlanId > 2)
                {
                    listMappingOrder = listMappingOrder.Where(m => m.PLANT_ID == PlanId).ToList();
                }

                if (ProductId > 2)
                {
                    listMappingOrder = listMappingOrder.Where(m => m.PRODUCT_ID == ProductId).ToList();
                }

            }

            listMappingOrder.ForEach(a =>
            {
                string[] tokens = a.EXCEL_NAME.Split('_');
                a.EXCEL_NAME = tokens[2];
            });

            return listMappingOrder;
        }
    }


public partial class QMS_MA_PRODUCT_MERGE
    {
        public static List<QMS_MA_PRODUCT_MERGE> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_PRODUCT_MERGE.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_PRODUCT_MERGE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_PRODUCT_MERGE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_PRODUCT_MERGE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_PRODUCT_MERGE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_PRODUCT_MERGE> GetAllBySearch(QMSDBEntities db, ProductMergeSearchModel searchModel)
        {
            List<QMS_MA_PRODUCT_MERGE> lstResult = new List<QMS_MA_PRODUCT_MERGE>();

            lstResult = db.QMS_MA_PRODUCT_MERGE.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (null != searchModel.ITEM && "" != searchModel.ITEM)
                {
                    lstResult = lstResult.Where(m => m.ITEM.StartsWith(searchModel.ITEM) || m.ITEM.EndsWith(searchModel.ITEM)).ToList();
                }

                if (null != searchModel.DISPLAY_NAME && "" != searchModel.DISPLAY_NAME)
                {
                    lstResult = lstResult.Where(m => m.DISPLAY_NAME.StartsWith(searchModel.DISPLAY_NAME) || m.DISPLAY_NAME.EndsWith(searchModel.DISPLAY_NAME)).ToList();
                }
            }
            return lstResult;
        }
    }

    public partial class QMS_MA_CUSTOMER_MERGE
    {
        public static List<QMS_MA_CUSTOMER_MERGE> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_CUSTOMER_MERGE.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_CUSTOMER_MERGE GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_CUSTOMER_MERGE.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_CUSTOMER_MERGE> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_CUSTOMER_MERGE.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_CUSTOMER_MERGE> GetAllBySearch(QMSDBEntities db, CustomerMergeSearchModel searchModel)
        {
            List<QMS_MA_CUSTOMER_MERGE> lstResult = new List<QMS_MA_CUSTOMER_MERGE>();

            lstResult = db.QMS_MA_CUSTOMER_MERGE.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {


                if (null != searchModel.CUSTOMER_NAME && "" != searchModel.CUSTOMER_NAME)
                {
                    lstResult = lstResult.Where(m => m.CUSTOMER_NAME.StartsWith(searchModel.CUSTOMER_NAME) || m.CUSTOMER_NAME.EndsWith(searchModel.CUSTOMER_NAME)).ToList();
                }

            }
            return lstResult;
        }
    }

    public partial class QMS_MA_SIGMA_LEVEL
    {
        public static List<QMS_MA_SIGMA_LEVEL> GetAll(QMSDBEntities db)
        {
            return db.QMS_MA_SIGMA_LEVEL.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_MA_SIGMA_LEVEL GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_MA_SIGMA_LEVEL.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_MA_SIGMA_LEVEL> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_MA_SIGMA_LEVEL.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_MA_SIGMA_LEVEL> GetAllBySearch(QMSDBEntities db, SigmaLevelSearchModel searchModel)
        {
            List<QMS_MA_SIGMA_LEVEL> lstResult = new List<QMS_MA_SIGMA_LEVEL>();

            lstResult = db.QMS_MA_SIGMA_LEVEL.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                if (null != searchModel.PLANT_ID && searchModel.PLANT_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.PLANT_ID == searchModel.PLANT_ID).ToList();
                }

                if (null != searchModel.PRODUCT_ID && searchModel.PRODUCT_ID > 0)
                {
                    lstResult = lstResult.Where(m => m.PRODUCT_ID == searchModel.PRODUCT_ID).ToList();
                }

                if (null != searchModel.ACTIVE_DATE)
                {
                    lstResult = lstResult.Where(m => m.ACTIVE_DATE == searchModel.ACTIVE_DATE).ToList();
                }

                if (null != searchModel.SIGMA_LEVEL_DESC && "" != searchModel.SIGMA_LEVEL_DESC)
                {
                    lstResult = lstResult.Where(m => m.SIGMA_LEVEL_DESC.Contains(searchModel.SIGMA_LEVEL_DESC)).ToList();
                }
            }
            return lstResult;
        }
    }


    public partial class QMS_TR_CPK_TEMP_DATA
    {
        public static List<QMS_TR_CPK_TEMP_DATA> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_CPK_TEMP_DATA.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static List<QMS_TR_CPK_TEMP_DATA> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_CPK_TEMP_DATA.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static QMS_TR_CPK_TEMP_DATA GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_CPK_TEMP_DATA.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_CPK_TEMP_DATA> GetAllBySearch(QMSDBEntities db, productDistributionChartSearchModel searchModel)
        {
            List<QMS_TR_CPK_TEMP_DATA> lstResult = new List<QMS_TR_CPK_TEMP_DATA>();

            var query = db.QMS_TR_CPK_TEMP_DATA.Where(m => m.DELETE_FLAG == 0);
            if (searchModel.GSP > 0)
            {
                query = query.Where(m => m.PLANT_ID == searchModel.GSP);
            }
            if (searchModel.Product > 0 && searchModel.Product != null)
            {
                query = query.Where(m => m.PRODUCT_ID == searchModel.Product);
            }
            if (!string.IsNullOrEmpty(searchModel.Composition))
            {
                query = query.Where(m => m.ITEM_NAME == searchModel.Composition);
            }
            if (searchModel.START_DATE != null)
            {
                query = query.Where(m => m.SAMPLE_TIME >= searchModel.START_DATE);
            }
            if (searchModel.END_DATE != null)
            {
                query = query.Where(m => m.SAMPLE_TIME <= searchModel.END_DATE);
            }
            var lstData = query.ToList();
            if (null != lstData && lstData.Count() > 0)
            {
                foreach (var item in lstData)
                {
                    QMS_TR_CPK_TEMP_DATA qMS_TR_CPK_TEMP_DATA = new QMS_TR_CPK_TEMP_DATA();
                    qMS_TR_CPK_TEMP_DATA.ID = item.ID;
                    qMS_TR_CPK_TEMP_DATA.PLANT_ID = item.PLANT_ID;
                    qMS_TR_CPK_TEMP_DATA.PRODUCT_ID = item.PRODUCT_ID;
                    qMS_TR_CPK_TEMP_DATA.ITEM_NAME = item.ITEM_NAME;
                    qMS_TR_CPK_TEMP_DATA.SAMPLE_TIME = item.SAMPLE_TIME;
                    qMS_TR_CPK_TEMP_DATA.SAMPLE_VALUE = item.SAMPLE_VALUE;
                    qMS_TR_CPK_TEMP_DATA.TOKEN_USER = item.TOKEN_USER;
                    qMS_TR_CPK_TEMP_DATA.DELETE_FLAG = item.DELETE_FLAG;
                    qMS_TR_CPK_TEMP_DATA.CREATE_USER = item.CREATE_USER;
                    qMS_TR_CPK_TEMP_DATA.CREATE_DATE = item.CREATE_DATE;
                    lstResult.Add(qMS_TR_CPK_TEMP_DATA);
                }
            }
            return lstResult;
        }
    }



    public partial class QMS_TR_PRODUCT_REMARK
    {
        public static List<QMS_TR_PRODUCT_REMARK> GetAll(QMSDBEntities db)
        {
            return db.QMS_TR_PRODUCT_REMARK.Where(m => m.DELETE_FLAG == 0).ToList(); //linq
        }

        public static QMS_TR_PRODUCT_REMARK GetById(QMSDBEntities dataContext, long id)
        {
            return dataContext.QMS_TR_PRODUCT_REMARK.Where(m => m.ID == id).FirstOrDefault();
        }

        public static List<QMS_TR_PRODUCT_REMARK> GetByListId(QMSDBEntities dataContext, long[] ids)
        {
            return dataContext.QMS_TR_PRODUCT_REMARK.Where(m => ids.Contains(m.ID)).ToList();
        }

        public static List<QMS_TR_PRODUCT_REMARK> GetAllBySearch(QMSDBEntities db, ProductMergeSearchModel searchModel)
        {
            List<QMS_TR_PRODUCT_REMARK> lstResult = new List<QMS_TR_PRODUCT_REMARK>();

            lstResult = db.QMS_TR_PRODUCT_REMARK.Where(m => m.DELETE_FLAG == 0).ToList();

            if (null != lstResult && lstResult.Count() > 0)
            {

                //if (null != searchModel.ITEM && "" != searchModel.ITEM)
                //{
                //    lstResult = lstResult.Where(m => m.ITEM.StartsWith(searchModel.ITEM) || m.ITEM.EndsWith(searchModel.ITEM)).ToList();
                //}

                //if (null != searchModel.DISPLAY_NAME && "" != searchModel.DISPLAY_NAME)
                //{
                //    lstResult = lstResult.Where(m => m.DISPLAY_NAME.StartsWith(searchModel.DISPLAY_NAME) || m.DISPLAY_NAME.EndsWith(searchModel.DISPLAY_NAME)).ToList();
                //}
            }
            return lstResult;
        }
    }












}





