﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.Model;
using QMSSystem.CoreDB.Services;
using QMSSystem.ExaDB;
using System.Data;
using System.Configuration;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.M_PlantAndProduct }, DataAccess = DataAccessEnum.Authen)]
    public class PlantProductMappingController : BaseController
    {
        //
        // GET: /PlantProductMapping/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult ListPlantProduct()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            PlantProductSearchModel searchModel= new PlantProductSearchModel();
            CreateQMS_MA_PRODUCT_MAPPING createQMS_MA_PRODUCT_MAPPING = new CreateQMS_MA_PRODUCT_MAPPING();
            CreateQMS_MA_PLANT createQMS_MA_PLANT = new CreateQMS_MA_PLANT();
            CreateQMS_MA_PRODUCT createQMS_MA_PRODUCT = new CreateQMS_MA_PRODUCT();
            CreateQMS_MA_GRADE createQMS_MA_GRADE = new CreateQMS_MA_GRADE();
            CreateQMS_MA_UNIT createQMS_MA_UNIT = new CreateQMS_MA_UNIT();
            CreateQMS_MA_EXQ_TAG createQMS_MA_EXQ_TAG = new CreateQMS_MA_EXQ_TAG();
            CreateQMS_MA_EXQ_ACCUM_TAG createQMS_MA_EXQ_ACCUM_TAG = new CreateQMS_MA_EXQ_ACCUM_TAG();

            createQMS_MA_EXQ_ACCUM_TAG.CONVERT_VALUE = (decimal)1.0;
            createQMS_MA_EXQ_TAG.lstAccumTag = new List<CreateQMS_MA_EXQ_ACCUM_TAG>();
            createQMS_MA_EXQ_TAG.lstAccumTag.Add(createQMS_MA_EXQ_ACCUM_TAG);
            createQMS_MA_EXQ_TAG.TAG_FLOW_CHECK_VALUE = (decimal)1.0;
            createQMS_MA_EXQ_TAG.TAG_FLOW_CONVERT_VALUE = (decimal)1.0;

            createQMS_MA_EXQ_TAG.lstDelAccumTag = new List<long>();
             


            ViewBag.searchPlantProduct = searchModel;
            ViewBag.listPlant = getPlantForSearch();
            ViewBag.listProduct = getProductForSearch();
            ViewBag.listGrade = getGradeForSearch();
            ViewBag.listPageSize = this.getPageSizeList();

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.GAS);
            ViewBag.listSpecGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.SPEC);

            ViewBag.createQMS_MA_PRODUCT_MAPPING = createQMS_MA_PRODUCT_MAPPING;
            ViewBag.createQMS_MA_PLANT = createQMS_MA_PLANT;
            ViewBag.createQMS_MA_PRODUCT = createQMS_MA_PRODUCT;
            ViewBag.createQMS_MA_GRADE = createQMS_MA_GRADE;
            ViewBag.createQMS_MA_UNIT = createQMS_MA_UNIT;
            ViewBag.createQMS_MA_EXQ_TAG = createQMS_MA_EXQ_TAG;
            ViewBag.createQMS_MA_EXQ_ACCUM_TAG = createQMS_MA_EXQ_ACCUM_TAG;

            return View();
        }


        public ActionResult CreatePlantProduct(long id = 0)
        {
            MasterDataService masterDataServices = new MasterDataService(_username);

            CreateQMS_MA_PRODUCT_MAPPING createQMS_MA_PRODUCT_MAPPING = new CreateQMS_MA_PRODUCT_MAPPING();
            CreateQMS_MA_PLANT createQMS_MA_PLANT = new CreateQMS_MA_PLANT();
            CreateQMS_MA_PRODUCT createQMS_MA_PRODUCT = new CreateQMS_MA_PRODUCT();
            CreateQMS_MA_GRADE createQMS_MA_GRADE = new CreateQMS_MA_GRADE();
            CreateQMS_MA_UNIT createQMS_MA_UNIT = new CreateQMS_MA_UNIT();
            CreateQMS_MA_EXQ_TAG createQMS_MA_EXQ_TAG = new CreateQMS_MA_EXQ_TAG();
            CreateQMS_MA_EXQ_ACCUM_TAG createQMS_MA_EXQ_ACCUM_TAG = new CreateQMS_MA_EXQ_ACCUM_TAG();

            createQMS_MA_EXQ_ACCUM_TAG.CONVERT_VALUE = (decimal)1.0;
            createQMS_MA_EXQ_TAG.lstAccumTag = new List<CreateQMS_MA_EXQ_ACCUM_TAG>();
            createQMS_MA_EXQ_TAG.lstAccumTag.Add(createQMS_MA_EXQ_ACCUM_TAG);
            createQMS_MA_EXQ_TAG.TAG_FLOW_CHECK_VALUE = (decimal)1.0;
            createQMS_MA_EXQ_TAG.TAG_FLOW_CONVERT_VALUE = (decimal)1.0;
            
            createQMS_MA_EXQ_TAG.lstDelAccumTag = new List<long>();
            string actionName = Resources.Strings.AddNew;
             

            if (id > 0)
            {
                actionName = Resources.Strings.EditData; 
                createQMS_MA_PRODUCT_MAPPING = masterDataServices.getcreateQMS_MA_PRODUCT_MAPPINGById(_db, id);
            }

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.GAS);
            ViewBag.listSpecGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.SPEC);

            ViewBag.createQMS_MA_PRODUCT_MAPPING = createQMS_MA_PRODUCT_MAPPING;
            ViewBag.createQMS_MA_PLANT = createQMS_MA_PLANT;
            ViewBag.createQMS_MA_PRODUCT = createQMS_MA_PRODUCT;
            ViewBag.createQMS_MA_GRADE = createQMS_MA_GRADE;
            ViewBag.createQMS_MA_UNIT = createQMS_MA_UNIT;
            ViewBag.createQMS_MA_EXQ_TAG = createQMS_MA_EXQ_TAG;
            ViewBag.createQMS_MA_EXQ_ACCUM_TAG = createQMS_MA_EXQ_ACCUM_TAG;
            ViewBag.ActionName = actionName;
            return View();
        }

        public JsonResult CreateQMS_MA_PRODUCT_MAPPING(CreateQMS_MA_PRODUCT_MAPPING model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_PRODUCT_MAPPING(_db, model);

                    if (nResult > 0)
                    {
                        CreateQMS_MA_PRODUCT_MAPPING createQMS_MA_PRODUCT_MAPPING = masterDataServices.getcreateQMS_MA_PRODUCT_MAPPINGById(_db, nResult);
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult, model = createQMS_MA_PRODUCT_MAPPING };
                    }
                    else
                    {
                        if (nResult == -1)
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.PlantProductMappingDuplicate };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_PLANT(CreateQMS_MA_PLANT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_PLANT(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        if (nResult == -1)
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.PlantNameDuplicate };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_PRODUCT(CreateQMS_MA_PRODUCT model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_PRODUCT(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        if (nResult == -1)
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.ProductNameDuplicate };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_GRADE(CreateQMS_MA_GRADE model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_GRADE(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        if (nResult == -1)
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.GradeNameDuplicate };
                        }
                        else
                        {
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQMS_MA_EXQ_TAG(CreateQMS_MA_EXQ_TAG model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_MA_EXQ_TAG(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateSpecAndControlQMS_MA_EXQ_TAG(CreateQMS_MA_EXQ_TAG model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.UpdateSpecAndControlQMS_MA_EXQ_TAG(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQMS_MA_PRODUCT_MAPPINGBySearch(PlantProductSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    List<ViewQMS_MA_PRODUCT_MAPPING> listData = new List<ViewQMS_MA_PRODUCT_MAPPING>();
                    listData = masterDataServices.searchCreateQMS_MA_PRODUCT_MAPPING(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
         
        public JsonResult GetActivePlantList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                { 
                    List<ViewQMS_MA_PLANT> listData = new List<ViewQMS_MA_PLANT>();
                    listData = masterDataServices.getQMS_MA_PLANTActiveList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetPlantPosition(SetPlantPosition model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_PLANT> listData = new List<ViewQMS_MA_PLANT>();
                    listData = masterDataServices.setQMS_MA_PLANTPostion(_db, model);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActiveProductList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_PRODUCT> listData = new List<ViewQMS_MA_PRODUCT>();
                    listData = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetProductPosition(SetProductPosition model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_PRODUCT> listData = new List<ViewQMS_MA_PRODUCT>(); 
                    listData = masterDataServices.setQMS_MA_PRODUCTPostion(_db, model);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        } 

        public JsonResult SetExaTagPosition(SetExaTagPosition model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_EXQ_TAG> listData = new List<ViewQMS_MA_EXQ_TAG>();
                    listData = masterDataServices.setQMS_MA_EXQ_TAGPostion(_db, model);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActiveGradeList()
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_GRADE> listData = new List<ViewQMS_MA_GRADE>();
                    listData = masterDataServices.getQMS_MA_GRADEActiveList(_db);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExaQualityTagList(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_EXQ_TAG> listData = new List<ViewQMS_MA_EXQ_TAG>();
                    listData = masterDataServices.getQualityQMS_MA_EXQ_TAGListByProductMapId(_db, id);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExaQtyTagList(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_EXQ_TAG> listData = new List<ViewQMS_MA_EXQ_TAG>();
                    listData = masterDataServices.getQuantityQMS_MA_EXQ_TAGListByProductMapId(_db, id);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExaAccumTagList(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_EXQ_TAG> listData = new List<ViewQMS_MA_EXQ_TAG>();
                    listData = masterDataServices.getAccumTagQMS_MA_EXQ_TAGListByProductMapId(_db, id);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAccumMemberTagById(long id)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<ViewQMS_MA_EXQ_ACCUM_TAG> listData = new List<ViewQMS_MA_EXQ_ACCUM_TAG>();
                    listData = masterDataServices.getQMS_MA_EXQ_ACCUM_TAGListByExaTagId(_db, id);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_PLANT(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_PLANTByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_PRODUCT(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_PRODUCTByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_GRADE(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_GRADEByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQMS_MA_PRODUCT_MAPPING(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_PRODUCT_MAPPINGByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteExaQualityTag(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_EXQ_TAGByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteExaAccumTag(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_EXQ_TAGByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteExaQtyTag(long[] ids)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    bool nResult = masterDataServices.DeleteQMS_MA_EXQ_TAGByListId(_db, ids);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
    }
}
