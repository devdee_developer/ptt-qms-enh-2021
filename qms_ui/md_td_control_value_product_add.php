<!DOCTYPE html>
<html lang="en">

  <head>

    <?php include("./head_tag.php"); ?>



  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"<span>PTT QMS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include("./user_profile.php"); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php include("./sidemenu_qms.php"); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include("./menu_footer.php"); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php include("./top_nav.php"); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Control Value Of Product</h3>
                <div class="clearfix"></div>


              </div>
            </div>

            <div class="clearfix"></div>
            <hr>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="row"  style="text-align:right;">
                    <!-- <ul class="nav navbar-left panel_toolbox" style="text-align:right;">
                    <il><span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                      </il>
                      <il><span class="btn btn-warning" style="color:white;"><i class="fa fa-save"></i> Save</span>
                      </il>
                    </ul> -->
                    <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                    <span class="btn btn-success" style="color:white;"><i class="fa fa-save"></i> Save</span>
                  </div>
                  <!-- <div class="x_title">
                    <h2>Product</h2>

                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <div class="row">

                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:center;">
                          Group of control value
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Group of control value">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Description
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <textarea id="message" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">

                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Sample Name / Item / Control Value </h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="sample-name-tab" role="tab" data-toggle="tab" aria-expanded="true">Sample Name</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="item-tab" data-toggle="tab" aria-expanded="false">Item</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="control-value-tab" data-toggle="tab" aria-expanded="false">Control Value</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="sample-name-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:left;">
                                <br>
                                   <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                   <br><br>
                              <table class="table table-bordered" style="width:100%;">
                                <thead style="width:100%;">
                                  <tr>
                                    <th><input type="checkbox"></th>

                                    <th >Sample Name</th>
                                    <!-- <th>Plant</th>

                                    <th>Grade</th> -->
                                    <th style="width:20%;">Tools</th>
                                  <tr>
                                </thead>
                                <tbody style="width:100%;" style="text-align:left;">
                                  <tr>
                                    <td><input type="checkbox"></td>

                                    <td><input type="text" class="form-control" placeholder="Sample Name">                                    </td>
                                    <!-- <td>
                                      <div class="col-md-9">
                                        <select class="form-control">
                                        <option>Choose Plant</option>
                                        <option>GSP#1</option>
                                        <option>GSP#2</option>
                                        <option>GSP#3</option>
                                        <option>GSP#5</option>
                                        <option>GSP#6</option>
                                        <option>ESP</option>
                                      </select>
                                      </div>
                                      <div class="col-md-3" style="text-align:left;">
                                        <span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i></span>
                                      </div>


                                  </td>
                                  <td>
                                    <div class="col-md-9">
                                      <select class="form-control">
                                        <option>Choose Grade</option>
                                        <option>Petro</option>
                                        <option>Domestic</option>
                                      </select>
                                    </div>
                                    <div class="col-md-3" style="text-align:left;">
                                      <span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i></span>
                                    </div>


                                </td> -->

                                    <td><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span></td>
                                  </tr>

                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>

                                    <td>Feed GAS</td>
                                    <!-- <td>GSP#1</td>
                                    <td>
                                    </td> -->
                                    <td><a href="#"><i class="fa fa-edit"></i></a>

                                    </td>
                                  </tr>
                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>

                                    <td>Feed GAS</td>
                                    <!-- <td>GSP#2</td>
                                    <td>
                                    </td> -->
                                    <td><a href="#"><i class="fa fa-edit"></i></a>

                                    </td>
                                  </tr>
                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>

                                    <td>LPG Process</td>
                                    <!-- <td>GSP#1</td>
                                    <td>Petro
                                    </td> -->
                                    <td><a href="#"><i class="fa fa-edit"></i></a>

                                    </td>
                                  </tr>
                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>

                                    <td>LPG Process</td>
                                    <!-- <td>GSP#1</td>
                                    <td>Domestic
                                    </td> -->
                                    <td><a href="#"><i class="fa fa-edit"></i></a>

                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="item-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:left;">
                                <br>
                                   <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                   <br><br>
                              <table class="table table-bordered" style="width:100%;">
                                <thead style="width:100%;">
                                  <tr>
                                    <th><input type="checkbox"></th>

                                    <th style="width:30%;">Item</th>
                                    <th>Unit</th>
                                    <th>Tools</th>
                                  <tr>
                                </thead>
                                <tbody style="width:100%;" style="text-align:left;">
                                  <tr>
                                    <td><input type="checkbox"></td>

                                    <td><input type="text" class="form-control" placeholder="Item">                                    </td>
                                    <td>
                                      <div class="col-md-9">
                                        <select class="form-control">
                                        <option>Choose Unit</option>
                                        <option>%mol</option>

                                      </select>
                                      </div>
                                      <div class="col-md-3" style="text-align:left;">
                                        <span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i></span>
                                      </div>


                                  </td>


                                    <td><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span></td>
                                  </tr>

                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>
                                    <td>C1</td>
                                    <td>%mol</td>
                                    <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                  </tr>
                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>
                                    <td>CO2</td>
                                    <td>%mol</td>
                                    <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                  </tr>
                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>
                                    <td>C2</td>
                                    <td>%mol</td>
                                    <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                  </tr>
                                  <tr style="text-align:left;">
                                    <td><input type="checkbox"></td>
                                    <td>C3</td>
                                    <td>%mol</td>
                                    <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="control-value-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
                                <img src="./images/qms/control_value.png" alt="" style="width:80%;" data-toggle="modal" data-target=".control-modal-lg"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- Contol modal -->
        <div class="modal fade control-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Control Value</h4>
              </div>
              <div class="modal-body">
                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      <input type="checkbox">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12" style="text-align:left;">
                      Show on Trend Data
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      <input type="checkbox">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12" style="text-align:left;">
                      CONC / LOADING
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      CONC   <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      LOADING  <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      Min   <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      Max  <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>
                  </div>
                </div>
                <br><br><br><br><br><br><br><br><br><br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-trash"></i> Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
              </div>

            </div>
          </div>
        </div>
        <!-- end Control modal -->
        <!-- footer content -->
          <?php include("./footer.php"); ?>
        <!-- /footer content -->
      </div>
    </div>


    <?php include("./footer_script.php"); ?>



    <!-- Flot -->
    <script>
      $(document).ready(function() {
        //random data
        var d1 = [
          [0, 1],
          [1, 9],
          [2, 6],
          [3, 10],
          [4, 5],
          [5, 17],
          [6, 6],
          [7, 10],
          [8, 7],
          [9, 11],
          [10, 35],
          [11, 9],
          [12, 12],
          [13, 5],
          [14, 3],
          [15, 4],
          [16, 9]
        ];

        //flot options
        var options = {
          series: {
            curvedLines: {
              apply: true,
              active: true,
              monotonicFit: true
            }
          },
          colors: ["#26B99A"],
          grid: {
            borderWidth: {
              top: 0,
              right: 0,
              bottom: 1,
              left: 1
            },
            borderColor: {
              bottom: "#7F8790",
              left: "#7F8790"
            }
          }
        };
        var plot = $.plot($("#placeholder3xx3"), [{
          label: "Registrations",
          data: d1,
          lines: {
            fillColor: "rgba(150, 202, 89, 0.12)"
          }, //#96CA59 rgba(150, 202, 89, 0.42)
          points: {
            fillColor: "#fff"
          }
        }], options);
      });
    </script>
    <!-- /Flot -->

    <!-- jQuery Sparklines -->
    <script>
      $(document).ready(function() {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'bar',
          height: '40',
          barWidth: 9,
          colorMap: {
            '7': '#a1a1a1'
          },
          barSpacing: 2,
          barColor: '#26B99A'
        });

        $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'line',
          width: '200',
          height: '40',
          lineColor: '#26B99A',
          fillColor: 'rgba(223, 223, 223, 0.57)',
          lineWidth: 2,
          spotColor: '#26B99A',
          minSpotColor: '#26B99A'
        });
      });
    </script>
    <!-- /jQuery Sparklines -->

    <!-- Doughnut Chart -->
    <script>
      $(document).ready(function() {
        var options = {
          legend: false,
          responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
              "Symbian",
              "Blackberry",
              "Other",
              "Android",
              "IOS"
            ],
            datasets: [{
              data: [15, 20, 30, 10, 30],
              backgroundColor: [
                "#BDC3C7",
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB"
              ],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });
      });
    </script>
    <!-- /Doughnut Chart -->

    <!-- bootstrap-daterangepicker -->
    <script type="text/javascript">
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- morris.js -->
    <script>
      $(document).ready(function() {
        Morris.Bar({
          element: 'graph_bar',
          data: [
            { "period": "Jan", "Hours worked": 80 },
            { "period": "Feb", "Hours worked": 125 },
            { "period": "Mar", "Hours worked": 176 },
            { "period": "Apr", "Hours worked": 224 },
            { "period": "May", "Hours worked": 265 },
            { "period": "Jun", "Hours worked": 314 },
            { "period": "Jul", "Hours worked": 347 },
            { "period": "Aug", "Hours worked": 287 },
            { "period": "Sep", "Hours worked": 240 },
            { "period": "Oct", "Hours worked": 211 }
          ],
          xkey: 'period',
          hideHover: 'auto',
          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          ykeys: ['Hours worked', 'sorned'],
          labels: ['Hours worked', 'SORN'],
          xLabelAngle: 60,
          resize: true
        });

        $MENU_TOGGLE.on('click', function() {
          $(window).resize();
        });
      });
    </script>
    <!-- /morris.js -->

    <!-- Skycons -->
    <script>
      var icons = new Skycons({
          "color": "#73879C"
        }),
        list = [
          "clear-day", "clear-night", "partly-cloudy-day",
          "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
          "fog"
        ],
        i;

      for (i = list.length; i--;)
        icons.set(list[i], list[i]);

      icons.play();
    </script>
    <!-- /Skycons -->

    <!-- gauge.js -->
    <script>
      var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
          length: 0.75,
          strokeWidth: 0.042,
          color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 100;
      gauge.animationSpeed = 32;
      gauge.set(80);
      gauge.setTextField(document.getElementById("gauge-text"));

      var target = document.getElementById('foo2'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 5000;
      gauge.animationSpeed = 32;
      gauge.set(4200);
      gauge.setTextField(document.getElementById("gauge-text2"));
    </script>
    <!-- /gauge.js -->
  </body>
</html>
