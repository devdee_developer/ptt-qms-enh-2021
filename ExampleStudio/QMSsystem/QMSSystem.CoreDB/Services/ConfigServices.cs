﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QMSSystem.Model;
using System.Net;
using QMSSystem.CoreDB.Helper.Grid;
using QMSSystem.CoreDB.Helper;
using System.IO;
using OfficeOpenXml;
using Excel;
using System.Data;
using System.Globalization;

namespace QMSSystem.CoreDB.Services
{
    public class ConfigServices : BaseService
    {
        public ConfigServices(string userName) : base(userName) { }

        public List<FolderData> getListOfDirectory(string pathName)
        {
            List<FolderData> listData = new List<FolderData>();

            //NetworkCredential ncred = new NetworkCredential ("sp550163", "pwr$p550163", "ptt");//
            //CredentialCache ncache = new CredentialCache();
            //ncache.Add(new Uri(@"\\ryg-app-s01\Lab_Report"), "Basic", ncred);

            if (null != pathName && "" != pathName)
            {
                string[] dirs = Directory.GetDirectories(pathName);
                foreach (string txtDir in dirs)
                {
                    FolderData temp = new FolderData("", txtDir);
                    listData.Add(temp);
                }
            }

            return listData;
        }

        public List<FolderData> getListOfFileInDirectory(string pathName)
        {
            List<FolderData> listData = new List<FolderData>();

            if (null != pathName && "" != pathName)
            {
                string[] files = Directory.GetFiles(pathName);
                foreach (string fileName in files)
                {
                    FolderData temp = new FolderData(fileName, pathName);
                    listData.Add(temp);
                }
            }

            return listData;
        }

        public int Excelcify(string sxCol)
        {
            int result = -1;
            sxCol = sxCol.ToUpper();

            if (string.IsNullOrEmpty(sxCol))
                return result;


            for (int i = sxCol.Length; i > 0; i--)
            {
                char _c = sxCol[i - 1];
                //
                // Function =>  (26 ^ reversed_char_index) * char_value
                //          A = 1 ------ Z = 26 ------ AA = 27 ------ AZ = 54
                // 64 because there 'A' starts at index 65 and we want to give 'A' the value 1.
                result += Convert.ToInt32(Math.Pow(26, sxCol.Length - i)) * (Convert.ToInt32(_c) - 64);
            }

            return result;
        }

        public bool executeFileTemplateToDB(QMSDBEntities db, FolderDataDetail model)
        {
            bool bResult = true;


            //ExcelPackage excel = null;
            try
            {
                MasterDataService masterService = new MasterDataService(this._currentUserName);
                TransactionService transService = new TransactionService(this._currentUserName);
                CreateQMS_MA_TEMPLATE templateData = masterService.getQMS_MA_TEMPLATEById(db, model.TEMPLATE_ID);
                List<ViewQMS_MA_TEMPLATE_ROW> listRows = masterService.getQMS_MA_TEMPLATE_ROWListById(db, model.TEMPLATE_ID);
                List<ViewQMS_MA_TEMPLATE_COLUMN> listColumns = masterService.getQMS_MA_TEMPLATE_COLUMNListById(db, model.TEMPLATE_ID);

                List<CreateQMS_TR_TREND_DATA> listTrendData = new List<CreateQMS_TR_TREND_DATA>();

                decimal? tempVal1 = null;
                decimal? tempVal2 = null;
                //Step 2. TEMPLATE WatseWater
                if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.WatseWater)
                {
                    if (listRows.Count() > 0)
                    {
                        string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                        string workSheet = templateData.SHEET_NAME;
                        string filename = Path.GetFileName(excelFile);
                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                        string ext = Path.GetExtension(fileStream.Name);
                        //ExcelDataReader library .................
                        //Choose one of either 1 or 2
                        IExcelDataReader excelReader;

                        if (ext == ".xls")
                        {
                            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        }
                        else // if( ext == ".xlsx")
                        {
                            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                        }
                        DataSet result = excelReader.AsDataSet();
                        int RowsNumber = 0;
                        string ColName = "";
                        object rawData;
                        var dataCollect = result.Tables[templateData.SHEET_NAME];
                        for (int i = 0; i < result.Tables.Count; i++)
                        {
                            if (templateData.SHEET_NAME == result.Tables[i].TableName.Trim())
                            {
                                dataCollect = result.Tables[result.Tables[i].TableName];
                            }
                        }
                        if (null != dataCollect)
                        {
                            if (templateData.ALIGNMENT == true)
                            {
                                int RowNo = getRowsNumberFromFieldName(templateData.START_POINT);
                                ColName = getColumnNameFromFieldName(templateData.START_POINT);
                                //char ch = char.Parse(ColName);
                                List<string> ListLetter = getListLetter(RowNo.ToString());
                                var ColList = dataCollect.Rows[RowNo - 1].ItemArray;
                                int colNo = Excelcify(ColName);
                                List<ViewQMS_MA_TEMPLATE_COLUMN> listColWatseWater = new List<ViewQMS_MA_TEMPLATE_COLUMN>();
                                for (int i = colNo; i < ColList.Length; i++)
                                {
                                    string Dt = ColList[i].ToString();
                                    if (Dt != "")
                                    {
                                        ViewQMS_MA_TEMPLATE_COLUMN TempColWatse = new ViewQMS_MA_TEMPLATE_COLUMN();
                                        TempColWatse.EXCEL_FIELD = ListLetter[i];
                                        var dDate = Convert.ToDouble(ColList[i].ToString());
                                        DateTime oDate = DateTime.FromOADate(dDate);
                                        string date = oDate.Year.ToString() + "-" + oDate.Month.ToString() + "-" + oDate.Day.ToString();
                                        TempColWatse.NAME = date;
                                        listColWatseWater.Add(TempColWatse);
                                    }
                                }
                                foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                {
                                    RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD);

                                    foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColWatseWater)
                                    {
                                        ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                        CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                        //2.2 ค่า id ต่างๆ 
                                        tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                        tempData.SAMPLE_ID = rowsData.ID;
                                        tempData.ITEM_ID = rowsData.ID;


                                        //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                        //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 
                                        DateTime tempDate = Convert.ToDateTime(colData.NAME);

                                        tempData.RECORD_DATE = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                        int rowdtNo = getRowsNumberFromFieldName(ColName) - 1;
                                        ColName = getColumnNameFromFieldName(ColName);
                                        colNo = Excelcify(ColName);
                                        rawData = dataCollect.Rows[rowdtNo][colNo];
                                        if (rowdtNo == 48 && colNo == 11)
                                        {

                                        }
                                        if (null != rawData && typeof(DBNull) != rawData.GetType())
                                        {
                                            tempVal1 = null;
                                            tempVal2 = null;

                                            try
                                            {
                                                tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                                if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                {
                                                    tempData.CONC_LOADING_FLAG = 1;
                                                    string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                    tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                    tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                }
                                                else
                                                {
                                                    int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                    tempData.CONC_LOADING_FLAG = 0;

                                                    if (startIndex >= 0)
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                        tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                    }
                                                    else
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                this.writeErrorLog(ex.Message);
                                            }

                                            if (null != tempVal1)
                                            {
                                                tempData.DATA_VALUE_1 = tempVal1.Value;

                                                if (null != tempVal2)
                                                    tempData.DATA_VALUE_2 = tempVal2.Value;

                                                listTrendData.Add(tempData);
                                                //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                                DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                            }
                                        }
                                        else
                                        {
                                            tempData.CONC_LOADING_FLAG = 0;
                                            tempData.DATA_VALUE_TEXT = " ";
                                            tempData.DATA_SIGN_1 = " ";
                                            tempData.DATA_VALUE_1 = 0;
                                            tempData.DATA_SIGN_2 = " ";
                                            tempData.DATA_VALUE_2 = 0;
                                            tempData.DESCRIPTION = " ";
                                            listTrendData.Add(tempData);
                                            //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                            DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                            DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                            bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                        }
                                    }
                                }
                                if (listTrendData.Count() > 0)
                                {
                                    listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                    //Step 2 Save value data to database .......
                                    foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                    {
                                        //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                        QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                        if (null != temp)
                                        {
                                            dtData.ID = temp.ID;
                                        }

                                        long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                    }
                                }
                            }
                            else
                            {
                                bResult = false;
                                _errMsg = "This data is not date formating.";
                            }

                        }
                        else
                        {
                            _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                            bResult = false;
                        }
                        excelReader.Close();
                    }
                }
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HotOilFlashPoint)
                {
                    if (listRows.Count() > 0)
                    {
                        string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                        string workSheet = templateData.SHEET_NAME;
                        string filename = Path.GetFileName(excelFile);
                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                        string ext = Path.GetExtension(fileStream.Name);
                        //ExcelDataReader library .................
                        //Choose one of either 1 or 2
                        IExcelDataReader excelReader;

                        if (ext == ".xls")
                        {
                            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        }
                        else // if( ext == ".xlsx")
                        {
                            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                        }
                        DataSet result = excelReader.AsDataSet();
                        int RowsNumber = 0;
                        string ColName = "";
                        object rawData;
                        var dataCollect = result.Tables[templateData.SHEET_NAME];
                        if (null != dataCollect)
                        {
                            if (templateData.ALIGNMENT == true)
                            {
                                int RowNo = getRowsNumberFromFieldName(templateData.START_POINT);
                                ColName = getColumnNameFromFieldName(templateData.START_POINT);
                                //char ch = char.Parse(ColName);
                                List<string> ListLetter = getListLetter(RowNo.ToString());
                                var ColList = dataCollect.Rows[RowNo - 1].ItemArray;
                                int colNo = Excelcify(ColName);
                                List<ViewQMS_MA_TEMPLATE_COLUMN> listColHotOilFlashPoint = new List<ViewQMS_MA_TEMPLATE_COLUMN>();
                                for (int i = colNo; i < ColList.Length; i++)
                                {
                                    string Dt = ColList[i].ToString();
                                    if (Dt != "")
                                    {

                                        ViewQMS_MA_TEMPLATE_COLUMN TempColHotOilFlashPoint = new ViewQMS_MA_TEMPLATE_COLUMN();
                                        TempColHotOilFlashPoint.EXCEL_FIELD = ListLetter[i];
                                        //var tee = ColList[i].ToString();
                                        //DateTime oDate = Convert.ToDateTime(ColList[i].ToString());
                                        var dDate = Convert.ToDouble(ColList[i].ToString());
                                        DateTime oDate = DateTime.FromOADate(dDate);
                                        string date = oDate.Year.ToString() + "-" + oDate.Month.ToString() + "-" + oDate.Day.ToString();
                                        TempColHotOilFlashPoint.NAME = date;
                                        listColHotOilFlashPoint.Add(TempColHotOilFlashPoint);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                {
                                    RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD);

                                    foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColHotOilFlashPoint)
                                    {
                                        ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                        CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                        //2.2 ค่า id ต่างๆ 
                                        tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                        tempData.SAMPLE_ID = rowsData.ID;
                                        tempData.ITEM_ID = rowsData.ID;


                                        //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                        //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 
                                        DateTime tempDate = Convert.ToDateTime(colData.NAME);

                                        tempData.RECORD_DATE = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                        int rowdtNo = getRowsNumberFromFieldName(ColName) - 1;
                                        ColName = getColumnNameFromFieldName(ColName);
                                        colNo = Excelcify(ColName);
                                        rawData = dataCollect.Rows[rowdtNo][colNo];
                                        if (null != rawData && typeof(DBNull) != rawData.GetType())
                                        {
                                            tempVal1 = null;
                                            tempVal2 = null;

                                            try
                                            {
                                                if (rowdtNo == 13 && colNo == 114)
                                                {

                                                }
                                                tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 
                                                var test = rawData.GetType();
                                                if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                {
                                                    tempData.CONC_LOADING_FLAG = 1;
                                                    string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                    tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                    tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                }
                                                else
                                                {
                                                    int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                    tempData.CONC_LOADING_FLAG = 0;

                                                    if (startIndex >= 0)
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                        tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                    }
                                                    else
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                    }
                                                }
                                                if (typeof(string) == rawData.GetType())
                                                {
                                                    tempVal1 = 0;
                                                    tempData.DESCRIPTION = rawData.ToString();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                this.writeErrorLog(ex.Message);
                                            }

                                            if (null != tempVal1)
                                            {
                                                tempData.DATA_VALUE_1 = tempVal1.Value;

                                                if (null != tempVal2)
                                                    tempData.DATA_VALUE_2 = tempVal2.Value;

                                                listTrendData.Add(tempData);
                                                //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                                DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                            }
                                        }
                                        else
                                        {
                                            tempData.CONC_LOADING_FLAG = 0;
                                            tempData.DATA_VALUE_TEXT = " ";
                                            tempData.DATA_SIGN_1 = " ";
                                            tempData.DATA_VALUE_1 = 0;
                                            tempData.DATA_SIGN_2 = " ";
                                            tempData.DATA_VALUE_2 = 0;
                                            tempData.DESCRIPTION = " ";
                                            listTrendData.Add(tempData);
                                            //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                            DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                            DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                            bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                        }
                                    }
                                }
                                if (listTrendData.Count() > 0)
                                {
                                    listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                    //Step 2 Save value data to database .......
                                    foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                    {
                                        //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                        QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                        if (null != temp)
                                        {
                                            dtData.ID = temp.ID;
                                        }

                                        long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                    }
                                }
                            }
                            else
                            {
                                bResult = false;
                                _errMsg = "This data is not date formating.";
                            }

                        }
                        else
                        {
                            _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                            bResult = false;
                        }
                        excelReader.Close();
                    }
                }
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasWeekly
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasMonthly)
                {
                    if (listRows.Count() > 0)
                    {
                        string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                        string workSheet = templateData.SHEET_NAME;
                        string filename = Path.GetFileName(excelFile);
                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                        string ext = Path.GetExtension(fileStream.Name);
                        //ExcelDataReader library .................
                        //Choose one of either 1 or 2
                        IExcelDataReader excelReader;

                        if (ext == ".xls")
                        {
                            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        }
                        else // if( ext == ".xlsx")
                        {
                            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                        }
                        DataSet result = excelReader.AsDataSet();
                        int RowsNumber = 0;
                        string ColName = "";
                        object rawData;
                        var dataCollect = result.Tables[templateData.SHEET_NAME];
                        if (null != dataCollect)
                        {
                            if (templateData.ALIGNMENT == true)
                            {
                                int RowNo = getRowsNumberFromFieldName(templateData.START_POINT);
                                ColName = getColumnNameFromFieldName(templateData.START_POINT);
                                //char ch = char.Parse(ColName);
                                List<string> ListLetter = getListLetter(RowNo.ToString());
                                var ColList = dataCollect.Rows[RowNo - 1].ItemArray;
                                int colNo = Excelcify(ColName);
                                List<ViewQMS_MA_TEMPLATE_COLUMN> listColHotOilFlashPoint = new List<ViewQMS_MA_TEMPLATE_COLUMN>();
                                //for (int i = colNo; i < ColList.Length; i++)
                                //{
                                //    string Dt = ColList[i].ToString();
                                //    if (Dt != "")
                                //    {
                                //        ViewQMS_MA_TEMPLATE_COLUMN TempColHotOilFlashPoint = new ViewQMS_MA_TEMPLATE_COLUMN();
                                //        TempColHotOilFlashPoint.EXCEL_FIELD = ListLetter[i];
                                //        var dDate = Convert.ToDouble(ColList[i].ToString());
                                //        DateTime oDate = DateTime.FromOADate(dDate);
                                //        string date = oDate.Year.ToString() + "-" + oDate.Month.ToString() + "-" + oDate.Day.ToString();
                                //        TempColHotOilFlashPoint.NAME = date;
                                //        listColHotOilFlashPoint.Add(TempColHotOilFlashPoint);
                                //    }
                                //    else
                                //    {
                                //        break;
                                //    }
                                //}
                                for (int i = colNo; i < ColList.Length; i++)
                                {
                                    string Dt = ColList[i].ToString();
                                    if (Dt != "" && Dt != "ค่าที่กำหนด (ug/m3)")
                                    {

                                        ViewQMS_MA_TEMPLATE_COLUMN TempColHotOilFlashPoint = new ViewQMS_MA_TEMPLATE_COLUMN();
                                        TempColHotOilFlashPoint.EXCEL_FIELD = ListLetter[i];
                                        DateTime oDate = Convert.ToDateTime(ColList[i].ToString());
                                        //var dDate = Convert.ToDouble(ColList[i].ToString());
                                        //DateTime oDate = DateTime.FromOADate(dDate);
                                        string date = oDate.Year.ToString() + "-" + oDate.Month.ToString() + "-" + oDate.Day.ToString();
                                        TempColHotOilFlashPoint.NAME = date;
                                        listColHotOilFlashPoint.Add(TempColHotOilFlashPoint);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                {
                                    try
                                    {
                                        RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD_ITEM);

                                        foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColHotOilFlashPoint)
                                        {
                                            ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                            CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                            //2.2 ค่า id ต่างๆ 
                                            tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                            tempData.SAMPLE_ID = rowsData.ID;
                                            tempData.ITEM_ID = rowsData.ID;


                                            //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                            //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 
                                            DateTime tempDate = Convert.ToDateTime(colData.NAME);

                                            tempData.RECORD_DATE = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                            int rowdtNo = getRowsNumberFromFieldName(ColName) - 1;
                                            ColName = getColumnNameFromFieldName(ColName);
                                            colNo = Excelcify(ColName);
                                            rawData = dataCollect.Rows[rowdtNo][colNo];
                                            if (null != rawData && typeof(DBNull) != rawData.GetType())
                                            {
                                                tempVal1 = null;
                                                tempVal2 = null;

                                                try
                                                {
                                                    tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                                    if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                    {
                                                        tempData.CONC_LOADING_FLAG = 1;
                                                        string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                        tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                        tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                    }
                                                    else
                                                    {
                                                        int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                        tempData.CONC_LOADING_FLAG = 0;

                                                        if (startIndex >= 0)
                                                        {
                                                            tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                            tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                        }
                                                        else
                                                        {
                                                            tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    this.writeErrorLog(ex.Message);
                                                }

                                                if (null != tempVal1)
                                                {
                                                    tempData.DATA_VALUE_1 = tempVal1.Value;

                                                    if (null != tempVal2)
                                                        tempData.DATA_VALUE_2 = tempVal2.Value;

                                                    listTrendData.Add(tempData);
                                                    //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                                    DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                    DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                    bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                                }
                                            }
                                            else
                                            {
                                                tempData.CONC_LOADING_FLAG = 0;
                                                tempData.DATA_VALUE_TEXT = " ";
                                                tempData.DATA_SIGN_1 = " ";
                                                tempData.DATA_VALUE_1 = 0;
                                                tempData.DATA_SIGN_2 = " ";
                                                tempData.DATA_VALUE_2 = 0;
                                                tempData.DESCRIPTION = " ";
                                                listTrendData.Add(tempData);
                                                //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                                DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        this.writeErrorLog(ex.Message);
                                    }
                                }
                                if (listTrendData.Count() > 0)
                                {
                                    listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                    //Step 2 Save value data to database .......
                                    foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                    {
                                        //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                        QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                        if (null != temp)
                                        {
                                            dtData.ID = temp.ID;
                                        }

                                        long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                    }
                                }
                            }
                            else
                            {
                                bResult = false;
                                _errMsg = "This data is not date formating.";
                            }

                        }
                        else
                        {
                            _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                            bResult = false;
                        }
                        excelReader.Close();
                    }
                }
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgInPLMonthly
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgInPL
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgOutletMRU)
                {
                    if (listRows.Count() > 0)
                    {
                        string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                        string workSheet = templateData.SHEET_NAME;
                        string filename = Path.GetFileName(excelFile);
                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                        string ext = Path.GetExtension(fileStream.Name);
                        //ExcelDataReader library .................
                        //Choose one of either 1 or 2
                        IExcelDataReader excelReader;

                        if (ext == ".xls")
                        {
                            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        }
                        else // if( ext == ".xlsx")
                        {
                            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                        }
                        DataSet result = excelReader.AsDataSet();
                        int RowsNumber = 0;
                        string ColName = "";
                        object rawData;
                        var dataCollect = result.Tables[templateData.SHEET_NAME];
                        if (null != dataCollect)
                        {
                            if (templateData.ALIGNMENT == true)
                            {
                                int RowNo = getRowsNumberFromFieldName(templateData.START_POINT);
                                ColName = getColumnNameFromFieldName(templateData.START_POINT);
                                //char ch = char.Parse(ColName);
                                List<string> ListLetter = getListLetter(RowNo.ToString());
                                var ColList = dataCollect.Rows[RowNo - 1].ItemArray;
                                int colNo = Excelcify(ColName);
                                List<ViewQMS_MA_TEMPLATE_COLUMN> listColHotOilFlashPoint = new List<ViewQMS_MA_TEMPLATE_COLUMN>();
                                for (int i = colNo; i < ColList.Length; i++)
                                {
                                    string Dt = ColList[i].ToString();
                                    if (Dt != "" && Dt != "ค่าที่กำหนด (ug/m3)")
                                    {

                                        ViewQMS_MA_TEMPLATE_COLUMN TempColHotOilFlashPoint = new ViewQMS_MA_TEMPLATE_COLUMN();
                                        TempColHotOilFlashPoint.EXCEL_FIELD = ListLetter[i];
                                        DateTime oDate = Convert.ToDateTime(ColList[i].ToString());
                                        //var dDate = Convert.ToDouble(ColList[i].ToString());
                                        //DateTime oDate = DateTime.FromOADate(dDate);
                                        string date = oDate.Year.ToString() + "-" + oDate.Month.ToString() + "-" + oDate.Day.ToString();
                                        TempColHotOilFlashPoint.NAME = date;
                                        listColHotOilFlashPoint.Add(TempColHotOilFlashPoint);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                {
                                    RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD);

                                    foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColHotOilFlashPoint)
                                    {
                                        ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                        CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                        //2.2 ค่า id ต่างๆ 
                                        tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                        tempData.SAMPLE_ID = rowsData.ID;
                                        tempData.ITEM_ID = rowsData.ID;


                                        //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                        //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 
                                        DateTime tempDate = Convert.ToDateTime(colData.NAME);

                                        tempData.RECORD_DATE = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                        int rowdtNo = getRowsNumberFromFieldName(ColName) - 1;
                                        ColName = getColumnNameFromFieldName(ColName);
                                        colNo = Excelcify(ColName);
                                        rawData = dataCollect.Rows[rowdtNo][colNo];
                                        if (null != rawData && typeof(DBNull) != rawData.GetType())
                                        {
                                            tempVal1 = null;
                                            tempVal2 = null;

                                            try
                                            {
                                                if (rowdtNo == 13 && colNo == 114)
                                                {

                                                }
                                                tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 
                                                var test = rawData.GetType();
                                                if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                {
                                                    tempData.CONC_LOADING_FLAG = 1;
                                                    string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                    tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                    tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                }
                                                else
                                                {
                                                    int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                    tempData.CONC_LOADING_FLAG = 0;

                                                    if (startIndex >= 0)
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                        tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                    }
                                                    else
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                    }
                                                }
                                                if (typeof(string) == rawData.GetType())
                                                {
                                                    tempVal1 = 0;
                                                    tempData.DESCRIPTION = rawData.ToString();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                this.writeErrorLog(ex.Message);
                                            }

                                            if (null != tempVal1)
                                            {
                                                tempData.DATA_VALUE_1 = tempVal1.Value;

                                                if (null != tempVal2)
                                                    tempData.DATA_VALUE_2 = tempVal2.Value;

                                                listTrendData.Add(tempData);
                                                //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                                DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                            }
                                        }
                                        else
                                        {
                                            tempData.CONC_LOADING_FLAG = 0;
                                            tempData.DATA_VALUE_TEXT = " ";
                                            tempData.DATA_SIGN_1 = " ";
                                            tempData.DATA_VALUE_1 = 0;
                                            tempData.DATA_SIGN_2 = " ";
                                            tempData.DATA_VALUE_2 = 0;
                                            tempData.DESCRIPTION = " ";
                                            listTrendData.Add(tempData);
                                            //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                            DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                            DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                            bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                        }
                                    }
                                }
                                if (listTrendData.Count() > 0)
                                {
                                    listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                    //Step 2 Save value data to database .......
                                    foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                    {
                                        //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                        QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                        if (null != temp)
                                        {
                                            dtData.ID = temp.ID;
                                        }

                                        long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                    }
                                }
                            }
                            else
                            {
                                bResult = false;
                                _errMsg = "This data is not date formating.";
                            }

                        }
                        else
                        {
                            _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                            bResult = false;
                        }
                        excelReader.Close();
                    }
                }
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HotOilPhysical)
                {
                    try
                    {
                        if (listRows.Count() > 0)
                        {
                            string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                            string workSheet = templateData.SHEET_NAME;
                            string filename = Path.GetFileName(excelFile);
                            FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                            string ext = Path.GetExtension(fileStream.Name);
                            //ExcelDataReader library .................
                            //Choose one of either 1 or 2
                            IExcelDataReader excelReader;

                            if (ext == ".xls")
                            {
                                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                                excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                            }
                            else // if( ext == ".xlsx")
                            {
                                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                                excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                            }
                            DataSet result = excelReader.AsDataSet();
                            int RowsNumber = 0;
                            string ColName = "";
                            object rawData;
                            var dataCollect = result.Tables[templateData.SHEET_NAME];
                            if (null != dataCollect)
                            {
                                if (templateData.ALIGNMENT == true)
                                {
                                    int RowNo = getRowsNumberFromFieldName(templateData.START_POINT);
                                    ColName = getColumnNameFromFieldName(templateData.START_POINT);
                                    //char ch = char.Parse(ColName);
                                    List<string> ListLetter = getListLetter(RowNo.ToString());
                                    var ColList = dataCollect.Rows[RowNo - 1].ItemArray;
                                    int colNo = Excelcify(ColName);
                                    List<ViewQMS_MA_TEMPLATE_COLUMN> listColHotOilFlashPoint = new List<ViewQMS_MA_TEMPLATE_COLUMN>();
                                    for (int i = colNo; i < ColList.Length; i++)
                                    {
                                        string Dt = ColList[i].ToString();
                                        if (Dt != "")
                                        {
                                            ViewQMS_MA_TEMPLATE_COLUMN TempColHotOilFlashPoint = new ViewQMS_MA_TEMPLATE_COLUMN();
                                            TempColHotOilFlashPoint.EXCEL_FIELD = ListLetter[i];
                                            var dDate = Convert.ToDouble(ColList[i].ToString());
                                            DateTime oDate = DateTime.FromOADate(dDate);
                                            string date = oDate.Year.ToString() + "-" + oDate.Month.ToString() + "-" + oDate.Day.ToString();
                                            TempColHotOilFlashPoint.NAME = date;
                                            listColHotOilFlashPoint.Add(TempColHotOilFlashPoint);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                    {
                                        try
                                        {
                                            RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD_ITEM);

                                            foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColHotOilFlashPoint)
                                            {
                                                try
                                                {
                                                    ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                                    CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                                    //2.2 ค่า id ต่างๆ 
                                                    tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                                    tempData.SAMPLE_ID = rowsData.ID;
                                                    tempData.ITEM_ID = rowsData.ID;


                                                    //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                                    //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 
                                                    DateTime tempDate = Convert.ToDateTime(colData.NAME);

                                                    tempData.RECORD_DATE = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                    int rowdtNo = getRowsNumberFromFieldName(ColName) - 1;
                                                    ColName = getColumnNameFromFieldName(ColName);
                                                    colNo = Excelcify(ColName);
                                                    rawData = dataCollect.Rows[rowdtNo][colNo];
                                                    if (null != rawData && typeof(DBNull) != rawData.GetType())
                                                    {
                                                        tempVal1 = null;
                                                        tempVal2 = null;

                                                        try
                                                        {
                                                            tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                                            if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                            {
                                                                tempData.CONC_LOADING_FLAG = 1;
                                                                string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                                tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                                tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                            }
                                                            else
                                                            {
                                                                int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                                tempData.CONC_LOADING_FLAG = 0;

                                                                if (startIndex >= 0)
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                                    tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                                }
                                                                else
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            this.writeErrorLog(ex.Message);
                                                        }

                                                        if (null != tempVal1)
                                                        {
                                                            tempData.DATA_VALUE_1 = tempVal1.Value;

                                                            if (null != tempVal2)
                                                                tempData.DATA_VALUE_2 = tempVal2.Value;

                                                            listTrendData.Add(tempData);
                                                            //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                                            DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                            DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                            bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tempData.CONC_LOADING_FLAG = 0;
                                                        tempData.DATA_VALUE_TEXT = " ";
                                                        tempData.DATA_SIGN_1 = " ";
                                                        tempData.DATA_VALUE_1 = 0;
                                                        tempData.DATA_SIGN_2 = " ";
                                                        tempData.DATA_VALUE_2 = 0;
                                                        tempData.DESCRIPTION = " ";
                                                        listTrendData.Add(tempData);
                                                        //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                                        DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                        DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                        bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    this.writeErrorLog(ex.Message);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            this.writeErrorLog(ex.Message);
                                        }
                                    }
                                    if (listTrendData.Count() > 0)
                                    {
                                        listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                        //Step 2 Save value data to database .......
                                        foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                        {
                                            //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                            QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                            if (null != temp)
                                            {
                                                dtData.ID = temp.ID;
                                            }

                                            long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                        }
                                    }
                                }
                                else
                                {
                                    bResult = false;
                                    _errMsg = "This data is not date formating.";
                                }

                            }
                            else
                            {
                                _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                                bResult = false;
                            }
                            excelReader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        this.writeErrorLog(ex.Message);
                    }
                }
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.TwoHundredThousandPond)
                {
                    try
                    {
                        if (listRows.Count() > 0)
                        {
                            string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                            string workSheet = templateData.SHEET_NAME;
                            string filename = Path.GetFileName(excelFile);
                            FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                            string ext = Path.GetExtension(fileStream.Name);
                            //ExcelDataReader library .................
                            //Choose one of either 1 or 2
                            IExcelDataReader excelReader;
                            if (ext == ".xls")
                            {
                                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                                excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                            }
                            else // if( ext == ".xlsx")
                            {
                                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                                excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                            }
                            DataSet result = excelReader.AsDataSet();
                            int RowsNumber = 0;
                            string ColName = "";
                            object rawData;
                            var dataCollect = result.Tables[templateData.SHEET_NAME];
                            if (null != dataCollect)
                            {
                                if (templateData.ALIGNMENT == false)
                                {
                                    int RowNo = getRowsNumberFromFieldName(templateData.START_POINT);
                                    string ColDate = getColumnNameFromFieldName(templateData.START_POINT);
                                    int colDateNo = Excelcify(ColDate);

                                    List<ViewQMS_MA_TEMPLATE_ROW> listRowsTwoHundredThousandPond = new List<ViewQMS_MA_TEMPLATE_ROW>();
                                    for (int i = RowNo; i <= dataCollect.Rows.Count; i++)
                                    {
                                        try
                                        {
                                            ViewQMS_MA_TEMPLATE_ROW TempRowTwoHundredThousandPond = new ViewQMS_MA_TEMPLATE_ROW();
                                            TempRowTwoHundredThousandPond = listRows.FirstOrDefault();
                                            foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColumns)
                                            {
                                                try
                                                {
                                                    ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                                    CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                                    //2.2 ค่า id ต่างๆ 
                                                    tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                                    tempData.SAMPLE_ID = TempRowTwoHundredThousandPond.ID;
                                                    tempData.ITEM_ID = colData.ID;

                                                    //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                        
                                                    ColName = getColumnNameFromFieldName(ColName);
                                                    int colNo = Excelcify(ColName);

                                                    rawData = dataCollect.Rows[i - 1][colNo];

                                                    DateTime tempDate = Convert.ToDateTime(dataCollect.Rows[i - 1][colDateNo]);
                                                    tempData.RECORD_DATE = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                    if (null != rawData && typeof(DBNull) != rawData.GetType())
                                                    {
                                                        tempVal1 = null;
                                                        tempVal2 = null;

                                                        try
                                                        {
                                                            tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                                            if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                            {
                                                                tempData.CONC_LOADING_FLAG = 1;
                                                                string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                                tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                                tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                            }
                                                            else
                                                            {
                                                                int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                                tempData.CONC_LOADING_FLAG = 0;

                                                                if (startIndex >= 0)
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                                    tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                                }
                                                                else
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            this.writeErrorLog(ex.Message);
                                                        }

                                                        if (null != tempVal1)
                                                        {
                                                            tempData.DATA_VALUE_1 = tempVal1.Value;

                                                            if (null != tempVal2)
                                                                tempData.DATA_VALUE_2 = tempVal2.Value;

                                                            listTrendData.Add(tempData);
                                                        }
                                                        //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน
                                                        DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                        DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);
                                                        bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                                    }
                                                    else
                                                    {
                                                        tempData.CONC_LOADING_FLAG = 0;
                                                        tempData.DATA_VALUE_TEXT = " ";
                                                        tempData.DATA_SIGN_1 = " ";
                                                        tempData.DATA_VALUE_1 = 0;
                                                        tempData.DATA_SIGN_2 = " ";
                                                        tempData.DATA_VALUE_2 = 0;
                                                        tempData.DESCRIPTION = " ";
                                                        listTrendData.Add(tempData);
                                                        //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน
                                                        DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                        DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);
                                                        bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    this.writeErrorLog(ex.Message);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            this.writeErrorLog(ex.Message);
                                        }
                                    }
                                    if (listTrendData.Count() > 0)
                                    {
                                        listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                        //Step 2 Save value data to database .......
                                        foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                        {
                                            //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                            QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);
                                            if (null != temp)
                                            {
                                                dtData.ID = temp.ID;
                                            }
                                            long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                        }
                                    }
                                }
                                else
                                {
                                    bResult = false;
                                    _errMsg = "This data is not date formating.";
                                }
                            }
                            else
                            {
                                _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                                bResult = false;
                            }
                            excelReader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        this.writeErrorLog(ex.Message);
                    }
                }
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.AcidOffGas || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgStab)
                {
                    try
                    {
                        if (listRows.Count() > 0)
                        {
                            string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                            string workSheet = templateData.SHEET_NAME;
                            string filename = Path.GetFileName(excelFile);
                            FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                            string ext = Path.GetExtension(fileStream.Name);
                            //ExcelDataReader library .................
                            //Choose one of either 1 or 2
                            IExcelDataReader excelReader;
                            if (ext == ".xls")
                            {
                                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                                excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                            }
                            else // if( ext == ".xlsx")
                            {
                                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                                excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                            }
                            DataSet result = excelReader.AsDataSet();
                            int RowsNumber = 0;
                            string ColName = "";
                            object rawData;
                            var dataCollect = result.Tables[templateData.SHEET_NAME];
                            if (null != dataCollect)
                            {
                                if (templateData.ALIGNMENT == false)
                                {
                                    int RowNo = getRowsNumberFromFieldName(templateData.START_POINT);
                                    string ColDate = getColumnNameFromFieldName(templateData.START_POINT);
                                    int colDateNo = Excelcify(ColDate);
                                    List<string> ListLetter = getListLetter(RowNo.ToString());
                                    string ColTime = getColumnNameFromFieldName(ListLetter[colDateNo + 1]);
                                    int colTimeNo = Excelcify(ColTime);
                                   
                                    if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.AcidOffGas)
                                    {
                                        colTimeNo = Excelcify(templateData.EXCEL_TIME);
                                    }
                                    List<ViewQMS_MA_TEMPLATE_ROW> listRowsHgStab = new List<ViewQMS_MA_TEMPLATE_ROW>();
                                    for (int i = RowNo; i <= dataCollect.Rows.Count; i++)
                                    {
                                        try
                                        {
                                            rawData = dataCollect.Rows[i - 1][colTimeNo];
                                            if (null == rawData || typeof(DBNull) == rawData.GetType())
                                            {
                                                break;
                                            }
                                            foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColumns)
                                            {
                                                try
                                                {
                                                    DateTime tempDate = new DateTime();
                                                    DateTime tempTime = new DateTime();
                                                    ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                                    CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();
                                                    //2.2 ค่า id ต่างๆ 
                                                    tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                                    tempData.SAMPLE_ID = colData.SAMPLE_ID.Value;
                                                    tempData.ITEM_ID = colData.ID;
                                                    //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                        
                                                    ColName = getColumnNameFromFieldName(ColName);
                                                    int colNo = Excelcify(ColName);
                                                    rawData = dataCollect.Rows[i - 1][colNo];
                                                    
                                                    if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.AcidOffGas)
                                                    {
                                                        //var test = dataCollect.Rows[i - 1][colDateNo];
                                                        try
                                                        {
                                                            var dDate = Convert.ToDouble(dataCollect.Rows[i - 1][colDateNo]);
                                                            var dTime = Convert.ToDouble(dataCollect.Rows[i - 1][colTimeNo]);
                                                            tempDate = DateTime.FromOADate(dDate);
                                                            tempTime = DateTime.FromOADate(dTime);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            tempDate = Convert.ToDateTime(dataCollect.Rows[i - 1][colDateNo]);
                                                            tempTime = Convert.ToDateTime(dataCollect.Rows[i - 1][colTimeNo]);
                                                        }
                                                    }
                                                    if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgStab)
                                                    {
                                                        tempDate = Convert.ToDateTime(dataCollect.Rows[i - 1][colDateNo]);
                                                        tempTime = Convert.ToDateTime(dataCollect.Rows[i - 1][colTimeNo]);
                                                    }

                                                    tempData.RECORD_DATE = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, tempTime.Hour, tempTime.Minute, 0);
                                                    if (null != rawData && typeof(DBNull) != rawData.GetType())
                                                    {
                                                        tempVal1 = null;
                                                        tempVal2 = null;
                                                        try
                                                        {
                                                            tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 
                                                            if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                            {
                                                                tempData.CONC_LOADING_FLAG = 1;
                                                                string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');
                                                                tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                                tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                            }
                                                            else
                                                            {
                                                                int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                                tempData.CONC_LOADING_FLAG = 0;
                                                                if (startIndex >= 0)
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                                    tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                                }
                                                                else
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            this.writeErrorLog(ex.Message);
                                                        }
                                                        if (null != tempVal1)
                                                        {
                                                            tempData.DATA_VALUE_1 = tempVal1.Value;
                                                            if (null != tempVal2)
                                                                tempData.DATA_VALUE_2 = tempVal2.Value;
                                                            listTrendData.Add(tempData);
                                                        }
                                                        //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน
                                                        DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                        DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);
                                                        bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                                    }
                                                    else
                                                    {
                                                        tempData.CONC_LOADING_FLAG = 0;
                                                        tempData.DATA_VALUE_TEXT = " ";
                                                        tempData.DATA_SIGN_1 = " ";
                                                        tempData.DATA_VALUE_1 = 0;
                                                        tempData.DATA_SIGN_2 = " ";
                                                        tempData.DATA_VALUE_2 = 0;
                                                        tempData.DESCRIPTION = " ";
                                                        listTrendData.Add(tempData);
                                                        //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน
                                                        DateTime startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 9, 0, 0);
                                                        DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);
                                                        bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    this.writeErrorLog(ex.Message);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            this.writeErrorLog(ex.Message);
                                        }
                                    }
                                    if (listTrendData.Count() > 0)
                                    {
                                        listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                        //Step 2 Save value data to database .......
                                        foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                        {
                                            //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                            QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);
                                            if (null != temp)
                                            {
                                                dtData.ID = temp.ID;
                                            }
                                            long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                        }
                                    }
                                }
                                else
                                {
                                    bResult = false;
                                    _errMsg = "This data is not date formating.";
                                }
                            }
                            else
                            {
                                _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                                bResult = false;
                            }
                            excelReader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        this.writeErrorLog(ex.Message);
                    }
                }
                ////// Obeser Ponds & Oil Water  
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.ObservePonds || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.OilyWaters)
                {
                    if (listRows.Count() > 0 && listColumns.Count() > 0)
                    {
                        string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                        string workSheet = templateData.SHEET_NAME;
                        string filename = Path.GetFileName(excelFile);

                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                        string ext = Path.GetExtension(fileStream.Name);

                        //ExcelDataReader library .................
                        //Choose one of either 1 or 2
                        IExcelDataReader excelReader;

                        if (ext == ".xls")
                        {
                            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        }
                        else // if( ext == ".xlsx")
                        {
                            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                        }

                        ////Choose one of either 3, 4, or 5
                        ////3. DataSet - The result of each spreadsheet will be created in the result.Tables
                        DataSet result = excelReader.AsDataSet();


                        ///4. วนลูป Tempalte
                        ///
                        int colDateNo = Excelcify(templateData.EXCEL_DATE);
                        int colNo = 0;
                        int rowNo = 0;
                        int RowsNumber = 0;
                        string ColName = "";
                        object rawData;
                        DateTime? recordDate = null;// getDateTimeFromFileName(templateData.FILE_NAME, model.m_dtFile);
                        for (int i = 0; i < result.Tables.Count; i++)
                        {
                            try
                            {
                                string sheetName = result.Tables[i].ToString().Substring(0, 3).ToLower().Trim();

                                if (this.monthEnString.Contains(sheetName) || this.monthEnShortString.Contains(sheetName)
                                    || this.monthThString.Contains(sheetName) || this.monthThShortString.Contains(sheetName))
                                {
                                    //อยู่ในกรุ๊ป เดือนที่กำหนด
                                    var dataCollect = result.Tables[i];


                                    if (null != dataCollect)
                                    {

                                        foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                        {
                                            try
                                            {
                                                RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD);

                                                foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColumns)
                                                {
                                                    try
                                                    {

                                                        ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                                        CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                                        //2.2 ค่า id ต่างๆ 
                                                        tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                                        tempData.SAMPLE_ID = rowsData.ID;
                                                        tempData.ITEM_ID = colData.ID;


                                                        //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                                        //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 

                                                        rowNo = getRowsNumberFromFieldName(ColName) - 1;
                                                        ColName = getColumnNameFromFieldName(ColName);
                                                        colNo = Excelcify(ColName);

                                                        rawData = dataCollect.Rows[rowNo][colNo];



                                                        var dateRecord = dataCollect.Rows[rowNo][colDateNo];// Convert.ToInt64(result.Tables[0].Rows[rowNo][colNo]);
                                                        recordDate = null;
                                                        if (typeof(DBNull) != dateRecord.GetType())
                                                        {
                                                            if (typeof(DateTime) == dateRecord.GetType())
                                                            {
                                                                recordDate = (DateTime)dateRecord;
                                                            }
                                                            else
                                                            {
                                                                double oaDate = Convert.ToDouble(dataCollect.Rows[rowNo][colDateNo]);
                                                                recordDate = DateTime.FromOADate(oaDate);
                                                            }
                                                        }

                                                        if (null == recordDate) // แสดงว่า หาวันที่ไม่เจอ
                                                        {
                                                            continue;// next loop
                                                        }


                                                        tempData.RECORD_DATE = recordDate.Value;
                                                        if (null != rawData && typeof(DBNull) != rawData.GetType())
                                                        {
                                                            tempVal1 = null;
                                                            tempVal2 = null;

                                                            tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                                            if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                            {
                                                                tempData.CONC_LOADING_FLAG = 1;
                                                                string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                                tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                                tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                            }
                                                            else
                                                            {
                                                                int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                                tempData.CONC_LOADING_FLAG = 0;

                                                                if (startIndex >= 0)
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                                    tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                                }
                                                                else
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                                }
                                                            }

                                                            if (null != tempVal1)
                                                            {
                                                                tempData.DATA_VALUE_1 = tempVal1.Value;

                                                                if (null != tempVal2)
                                                                    tempData.DATA_VALUE_2 = tempVal2.Value;

                                                                listTrendData.Add(tempData);
                                                            }
                                                        }

                                                    }
                                                    catch (Exception exRow)
                                                    {
                                                        LogWriter.WriteErrorLog(exRow.Message);
                                                    }
                                                }
                                            }
                                            catch (Exception exCol)
                                            {
                                                LogWriter.WriteErrorLog(exCol.Message);
                                            }

                                        }

                                        ///End Column 
                                        ///
                                        DateTime startDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 0, 0, 0);
                                        DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                        bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                    }

                                    if (listTrendData.Count() > 0)
                                    {
                                        listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                        //Step 2 Save value data to database .......
                                        foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                        {
                                            //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                            QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                            if (null != temp)
                                            {
                                                dtData.ID = temp.ID;
                                            }

                                            long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                        }
                                    }
                                    //End rows  
                                }
                            }
                            catch(Exception exSheet)
                            {
                                LogWriter.WriteErrorLog(exSheet.Message);

                            }
                        } 
                            //System.Threading.Thread.Sleep(500);
                            //}
                            //else
                            //{
                            //    _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                            //    bResult = false;
                            //}  
                        excelReader.Close(); 
                    }
                }
                ////// Obeser Ponds & Oil Water 
                ///
                /////////////// Emission ///////////////
                /// 
                else if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.Emission)
                {
                    if (listRows.Count() > 0 && listColumns.Count() > 0)
                    {
                        string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                        string workSheet = templateData.SHEET_NAME;
                        string filename = Path.GetFileName(excelFile);

                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                        string ext = Path.GetExtension(fileStream.Name);

                        //ExcelDataReader library .................
                        //Choose one of either 1 or 2
                        IExcelDataReader excelReader;

                        if (ext == ".xls")
                        {
                            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        }
                        else // if( ext == ".xlsx")
                        {
                            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                        }

                        ////Choose one of either 3, 4, or 5
                        ////3. DataSet - The result of each spreadsheet will be created in the result.Tables
                        DataSet result = excelReader.AsDataSet();


                        ///4. วนลูป Tempalte
                        ///
                        int colDateNo = Excelcify(templateData.EXCEL_DATE);
                        int colTimeNo = Excelcify(templateData.EXCEL_TIME);
                        int colNo = 0;
                        int rowNo = 0;
                        int RowsNumber = 0;
                        string ColName = "";
                        object rawData;
                        DateTime? recordDate = null;// getDateTimeFromFileName(templateData.FILE_NAME, model.m_dtFile);
                        listRows = listRows.Where(x => x.NAME != "").ToList();
                        for (int i = 0; i < result.Tables.Count; i++)
                        {
                            try
                            {
                                string sheetName = result.Tables[i].ToString().ToLower().Trim();

                                if (sheetName.Contains(templateData.SHEET_NAME.ToLower()))
                                {
                                    //อยู่ในกรุ๊ป เดือนที่กำหนด
                                    var dataCollect = result.Tables[i];


                                    if (null != dataCollect)
                                    {
                                        
                                        foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                        {
                                            try
                                            {
                                                RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD);
                                                if (RowsNumber == 19)
                                                {

                                                }
                                                foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColumns)
                                                {
                                                    try
                                                    {

                                                        ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                                        CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                                        //2.2 ค่า id ต่างๆ 
                                                        tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                                        tempData.SAMPLE_ID = rowsData.ID;
                                                        tempData.ITEM_ID = colData.ID;


                                                        //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                                        //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 

                                                        rowNo = getRowsNumberFromFieldName(ColName) - 1;
                                                        ColName = getColumnNameFromFieldName(ColName);
                                                        colNo = Excelcify(ColName);

                                                        rawData = dataCollect.Rows[rowNo][colNo];



                                                        var dateRecord = dataCollect.Rows[rowNo][colDateNo];// Convert.ToInt64(result.Tables[0].Rows[rowNo][colNo]);
                                                        recordDate = null;
                                                        if (typeof(DBNull) != dateRecord.GetType())
                                                        {
                                                            if (typeof(DateTime) == dateRecord.GetType())
                                                            {
                                                                recordDate = (DateTime)dateRecord;
                                                            }
                                                            else
                                                            {
                                                                double oaDate = Convert.ToDouble(dataCollect.Rows[rowNo][colDateNo]);
                                                                recordDate = DateTime.FromOADate(oaDate);
                                                            }
                                                        }

                                                        var timeRecord = dataCollect.Rows[rowNo][colTimeNo];// Convert.ToInt64(result.Tables[0].Rows[rowNo][colNo]);
                                                        DateTime? recordTime = null;
                                                        if (typeof(DBNull) != timeRecord.GetType())
                                                        {
                                                            if (typeof(DateTime) == timeRecord.GetType())
                                                            {
                                                                recordTime = (DateTime)timeRecord;
                                                            }
                                                            else
                                                            {
                                                                double oaDate = Convert.ToDouble(dataCollect.Rows[rowNo][colTimeNo]);
                                                                recordTime = DateTime.FromOADate(oaDate);
                                                            }
                                                        }


                                                        if (null == recordDate || null == recordTime) // แสดงว่า หาวันที่ไม่เจอ
                                                        {
                                                            continue;// next loop
                                                        }
                                                        else
                                                        {
                                                            recordDate = new   DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, recordTime.Value.Hour, recordTime.Value.Minute, 0);
                                                        }


                                                        tempData.RECORD_DATE = recordDate.Value;
                                                        if (null != rawData && typeof(DBNull) != rawData.GetType())
                                                        {
                                                            tempVal1 = null;
                                                            tempVal2 = null;

                                                            tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                                            if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                            {
                                                                tempData.CONC_LOADING_FLAG = 1;
                                                                string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                                tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                                tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                            }
                                                            else
                                                            {
                                                                int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                                tempData.CONC_LOADING_FLAG = 0;

                                                                if (startIndex >= 0)
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                                    tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                                }
                                                                else
                                                                {
                                                                    tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                                }
                                                            }

                                                            if (null != tempVal1)
                                                            {
                                                                tempData.DATA_VALUE_1 = tempVal1.Value;

                                                                if (null != tempVal2)
                                                                    tempData.DATA_VALUE_2 = tempVal2.Value;

                                                                listTrendData.Add(tempData);
                                                            }
                                                        }

                                                    }
                                                    catch (Exception exRow)
                                                    {
                                                        LogWriter.WriteErrorLog(exRow.Message);
                                                    }
                                                }
                                            }
                                            catch (Exception exCol)
                                            {
                                                LogWriter.WriteErrorLog(exCol.Message);
                                            }

                                            if (null == recordDate) // แสดงว่า หาวันที่ไม่เจอ
                                            {
                                                continue;// next loop
                                            }
                                            else
                                            {
                                                DateTime startDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 0, 0, 0);
                                                DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                                bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                            }
                                            
                                        }

                                        ///End Column 
                                        ///
                                       
                                    }

                                    if (listTrendData.Count() > 0)
                                    {
                                        listTrendData = listTrendData.Where(x => x.RECORD_DATE >= templateData.ACTIVE_DATE && x.RECORD_DATE <= templateData.EXPIRE_DATE).ToList();
                                        //Step 2 Save value data to database .......
                                        foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                        {
                                            //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                            QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                            if (null != temp)
                                            {
                                                dtData.ID = temp.ID;
                                            }

                                            long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                        }
                                    }
                                    //End rows  
                                }
                            }
                            catch (Exception exSheet)
                            {
                                LogWriter.WriteErrorLog(exSheet.Message);

                            }
                        }
                        //System.Threading.Thread.Sleep(500);
                        //}
                        //else
                        //{
                        //    _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                        //    bResult = false;
                        //}  
                        excelReader.Close();
                    }
                }
                /// 
                /////////////// Emission ///////////////
                else
                {
                    if (listRows.Count() > 0 && listColumns.Count() > 0)
                    {
                        string excelFile = model.m_dtDirectory + "\\" + model.m_dtFile;
                        string workSheet = templateData.SHEET_NAME;
                        string filename = Path.GetFileName(excelFile);
                        //var m_FileModify = File.GetLastWriteTime(excelFile);
                        //Step 1. เปิดไฟล์ excel... string ext = Path.GetExtension(myFilePath);

                        //byte[] file = File.ReadAllBytes(excelFile);
                        //MemoryStream ms = new MemoryStream(file);

                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //File.Open(excelFile, FileMode.Open, FileAccess.Read);  
                        string ext = Path.GetExtension(fileStream.Name);

                        //ExcelDataReader library .................
                        //Choose one of either 1 or 2
                        IExcelDataReader excelReader;

                        if (ext == ".xls")
                        {
                            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                        }
                        else // if( ext == ".xlsx")
                        {
                            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                        }

                        ////Choose one of either 3, 4, or 5
                        ////3. DataSet - The result of each spreadsheet will be created in the result.Tables
                        DataSet result = excelReader.AsDataSet();

                        ////4. DataSet - Create column names from first row
                        //excelReader.IsFirstRowAsColumnNames = true;
                        //DataSet result = excelReader.AsDataSet();

                        //5. Data Reader methods
                        //while (excelReader.Read())
                        //{
                        //    //excelReader.GetInt32(0);
                        //}


                        //for (int i = 1; i < result.Tables[0].Rows.Count; i++)
                        //{
                        //    DataRow data = result.Tables[0].Rows[i];

                        //    int x = data.Table.Columns.Count;
                        //    System.Diagnostics.Debug.Write(data.Table.Rows[i]["Amortization"]);
                        //}


                        //6. Free resources (IExcelDataReader is IDisposable)



                        //var file = new FileInfo(excelFile);
                        //excel = new ExcelPackage(fileStream); 

                        //var worksheet = excel.Workbook.Worksheets[workSheet];

                        int RowsNumber = 0;
                        string ColName = "";
                        object rawData;
                        //Step 2. วนลูป Row, Column ของ template เผื่อ อ่าน ค่า ดาต้า

                        //2.1 get Datetime 
                        int rowNo = getRowsNumberFromFieldName(templateData.EXCEL_DATE) - 1;
                        ColName = getColumnNameFromFieldName(templateData.EXCEL_DATE);
                        int colNo = Excelcify(ColName);
                        DateTime? recordDate = getDateTimeFromFileName(templateData.FILE_NAME, model.m_dtFile);


                        var dataCollect = result.Tables[templateData.SHEET_NAME];

                        if (null == dataCollect && null != result.Tables[0]) //ให้ โอกาส อีกครั้ง
                        {
                            for (int i = 0; i < result.Tables.Count; i++)
                            {
                                if (templateData.SHEET_NAME.ToLower().Trim() == result.Tables[i].ToString().ToLower().Trim())
                                {
                                    dataCollect = result.Tables[i];
                                }
                            }
                        }

                        if (null != dataCollect)
                        {
                            var dateRecord = dataCollect.Rows[rowNo][colNo];// Convert.ToInt64(result.Tables[0].Rows[rowNo][colNo]);

                            if (typeof(DBNull) != dateRecord.GetType())
                            {
                                if (typeof(DateTime) == dateRecord.GetType())
                                {
                                    recordDate = (DateTime)dateRecord;
                                }
                                else
                                {
                                    long oaDate = Convert.ToInt64(dataCollect.Rows[rowNo][colNo]);
                                    recordDate = DateTime.FromOADate(oaDate);
                                }
                            }

                            if (null == recordDate)
                            {
                                bResult = false;
                                _errMsg = "This data is not date formating.";
                            }
                            else
                            {
                                foreach (ViewQMS_MA_TEMPLATE_ROW rowsData in listRows)
                                {
                                    RowsNumber = getRowsNumberFromFieldName(rowsData.EXCEL_FIELD);

                                    foreach (ViewQMS_MA_TEMPLATE_COLUMN colData in listColumns)
                                    {
                                        ColName = getColumnNameFromFieldName(colData.EXCEL_FIELD) + RowsNumber.ToString();
                                        CreateQMS_TR_TREND_DATA tempData = new CreateQMS_TR_TREND_DATA();

                                        //2.2 ค่า id ต่างๆ 
                                        tempData.TEMPLATE_ID = model.TEMPLATE_ID;
                                        tempData.SAMPLE_ID = rowsData.ID;
                                        tempData.ITEM_ID = colData.ID;


                                        //2.3 เตรียมค่าดาต้าเป็น ค่าต่างๆ ที่ต้องการ                                 
                                        //rawData = worksheet.Cells[ColName].Value; //ค่าดั้งเดิม 

                                        rowNo = getRowsNumberFromFieldName(ColName) - 1;
                                        ColName = getColumnNameFromFieldName(ColName);
                                        colNo = Excelcify(ColName);

                                        rawData = dataCollect.Rows[rowNo][colNo];


                                        try
                                        {
                                            recordDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 9, 0, 0); //default 9 โมง
                                                                                                                                                     //getTime
                                            try
                                            {
                                                int TimeRowNo = rowNo;
                                                int TimeColNo = Excelcify(templateData.EXCEL_TIME);

                                                //DateTime tempRecordTime = DateTime.FromOADate(Double.Parse(dataCollect.Rows[TimeRowNo][TimeColNo].ToString()));
                                                DateTime tempRecordTime = Convert.ToDateTime(dataCollect.Rows[TimeRowNo][TimeColNo].ToString());
                                                recordDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, tempRecordTime.Hour, tempRecordTime.Minute, tempRecordTime.Second);
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        catch (Exception ex)
                                        {

                                        }

                                        tempData.RECORD_DATE = recordDate.Value;
                                        if (null != rawData && typeof(DBNull) != rawData.GetType())
                                        {
                                            tempVal1 = null;
                                            tempVal2 = null;

                                            try
                                            {
                                                tempData.DATA_VALUE_TEXT = rawData.ToString(); //ค่าตัวอักษรใน excel 

                                                if (tempData.DATA_VALUE_TEXT.IndexOf("/") >= 0)
                                                {
                                                    tempData.CONC_LOADING_FLAG = 1;
                                                    string[] namesArray = tempData.DATA_VALUE_TEXT.Split('/');

                                                    tempVal1 = getConvertTextToDecimalEx(namesArray[0].ToString());
                                                    tempVal2 = getConvertTextToDecimalEx(namesArray[1].ToString());
                                                }
                                                else
                                                {
                                                    int startIndex = tempData.DATA_VALUE_TEXT.IndexOfAny("><".ToCharArray());
                                                    tempData.CONC_LOADING_FLAG = 0;

                                                    if (startIndex >= 0)
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT.Substring(startIndex + 1));
                                                        tempData.DATA_SIGN_1 = tempData.DATA_VALUE_TEXT.Substring(0, startIndex + 1);
                                                    }
                                                    else
                                                    {
                                                        tempVal1 = getConvertTextToDecimalEx(tempData.DATA_VALUE_TEXT);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                this.writeErrorLog(ex.Message);
                                            }

                                            if (null != tempVal1)
                                            {
                                                tempData.DATA_VALUE_1 = tempVal1.Value;

                                                if (null != tempVal2)
                                                    tempData.DATA_VALUE_2 = tempVal2.Value;

                                                listTrendData.Add(tempData);
                                            }
                                            //else
                                            //{
                                            //    DateTime startDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 0, 0, 0);
                                            //    DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);
                                            //    bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);
                                            //}
                                        }
                                    }
                                }

                                //Step 1 Clear all data .... //สมมุติฐานว่า 1 tempalte = 1 วัน

                                DateTime startDate = new DateTime(recordDate.Value.Year, recordDate.Value.Month, recordDate.Value.Day, 0, 0, 0);
                                DateTime endDate = startDate.AddDays(1).AddMilliseconds(-1);

                                bResult = transService.DeleteQMS_TR_TREND_DATAByTempIdAndDate(db, model.TEMPLATE_ID, startDate, endDate);


                                if (listTrendData.Count() > 0)
                                {
                                    //Step 2 Save value data to database .......
                                    foreach (CreateQMS_TR_TREND_DATA dtData in listTrendData)
                                    {
                                        //key  TEMPLATE_ID ,  SAMPLE_ID, ITEM_ID
                                        QMS_TR_TREND_DATA temp = transService.getQMS_TR_TREND_DATABySampleItemTemplateId(db, dtData.TEMPLATE_ID, dtData.SAMPLE_ID, dtData.ITEM_ID, dtData.RECORD_DATE);

                                        if (null != temp)
                                        {
                                            dtData.ID = temp.ID;
                                        }

                                        long nResult = transService.SaveQMS_TR_TREND_DATA(db, dtData);
                                    }
                                }
                            }
                            //System.Threading.Thread.Sleep(500);
                        }
                        else
                        {
                            _errMsg = "Sheet name " + templateData.SHEET_NAME + " isn't exist.";
                            bResult = false;
                        }


                        excelReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _errMsg = ex.Message;
                bResult = false;
                //this.writeErrorLog(ex.Message);
            }



            return bResult;
        }

        public List<string> getListLetter(string Row)
        {
            List<string> ColList = new List<string>();
            for (char letter = 'A'; letter <= 'Z'; letter++)
            {
                ColList.Add(letter.ToString() + Row);
            }
            for (char letter = 'A'; letter <= 'Z'; letter++)
            {
                for (char letter2 = 'A'; letter2 <= 'Z'; letter2++)
                {
                    string txt = letter.ToString() + letter2.ToString() + Row;
                    ColList.Add(txt);
                }
            }
            return ColList;
        }

        public List<FolderDataDetail> getFileDataByTemplateManageSearch(QMSDBEntities db, TrendManageSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {

            List<FolderDataDetail> objResult = new List<FolderDataDetail>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "m_dtFile";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<FolderDataDetail> listData = new List<FolderDataDetail>();
                MasterDataService masterService = new MasterDataService(this._currentUserName);
                CreateQMS_MA_TEMPLATE templateData = masterService.getQMS_MA_TEMPLATEById(db, searchModel.mSearch.TEMPLATE_ID);

                if (null != templateData)
                {
                    listData = getFolderDetailListOfDirectory(templateData, searchModel);//pathName, templateData.FILE_NAME, searchModel.mSearch.START_DATE, searchModel.mSearch.END_DATE);
                    if (listData != null)
                    {
                        //listData.ForEach(m => m.TEMPLATE_ID = searchModel.mSearch.TEMPLATE_ID);
                        var query = grid.LoadGridData<FolderDataDetail>(listData.AsQueryable(), out count, out totalPage);
                        objResult = query.ToList();
                        listPageIndex = getPageIndexList(totalPage);
                    }
                }

            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public long IsFileHasModify(FolderDataDetail dtFile, List<QMS_TR_FILE_TREND_DATA> listModifyFile)
        {
            long bResult = 0; //ไม่มีไฟลืในระบบ 

            try
            {
                QMS_TR_FILE_TREND_DATA tempFile = listModifyFile.Where(m => m.NAME.ToLower() == dtFile.m_dtFile.ToLower()).FirstOrDefault();

                if (null != tempFile)
                {

                    if (tempFile.UPDATE_DATE.AddMilliseconds(-tempFile.UPDATE_DATE.Millisecond) < dtFile.m_FileModify.AddMilliseconds(-dtFile.m_FileModify.Millisecond))
                    {
                        bResult = tempFile.ID;
                    }
                    else
                    {
                        bResult = -1;//file ไม่มีการแก้ไข
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return bResult;
        }

        public List<FolderDataDetail> getFolderDetailListOfDirectory(CreateQMS_MA_TEMPLATE templateData, TrendManageSearch searchModel)//string pathName, string fileName, DateTime startDate, DateTime endDate)
        {
            List<FolderDataDetail> listData = new List<FolderDataDetail>();
            List<FolderData> listYearFolder = getListOfDirectory(templateData.PATH_NAME);
            try
            {
                DateTime startDate = searchModel.mSearch.START_DATE;
                DateTime endDate = searchModel.mSearch.END_DATE;
                string fileName = templateData.FILE_NAME;

                #region Step1 Get Year list
                foreach (FolderData dt in listYearFolder)
                {

                    if (true == dt.IsFolderInYearRange(startDate, endDate)) //folder Year อยู่ในช่วงนั้น      
                    {
                        List<FolderData> listMothFolder = getListOfDirectory(dt.m_dtDirectory);
                        foreach (FolderData dtMonth in listMothFolder)
                        {
                            if (true == dtMonth.IsFolderInMonthRange(startDate, endDate)) //folder Month-year อยู่ในช่วงนั้น      
                            {
                                List<FolderData> listFile = getListOfFileInDirectory(dtMonth.m_dtDirectory);

                                foreach (FolderData dtFile in listFile)
                                {
                                    if (true == dtFile.IsFileInRange(fileName, startDate, endDate))
                                    {
                                        FolderDataDetail tdFile = new FolderDataDetail();
                                        string ext = Path.GetExtension(dtFile.m_dtFile);
                                        if (ext == ".xls" || ext == ".xlsx")
                                        {
                                            tdFile.m_dtFile = dtFile.m_dtFile;
                                            tdFile.m_dtFolder = dtFile.m_dtFolder;
                                            tdFile.m_dtDirectory = dtFile.m_dtDirectory;
                                            tdFile.m_FileModify = dtFile.m_FileModify;
                                            tdFile.m_FolderModify = dtFile.m_FolderModify;
                                            tdFile.TEMPLATE_ID = templateData.ID;
                                            listData.Add(tdFile);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                #endregion
                #region TEMPLATE_TYPE Emission
                if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.Emission
                    || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.ObservePonds
                    || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.OilyWaters
                    || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.WatseWater)
                {
                    listYearFolder = getListOfDirectory(templateData.PATH_NAME);
                    DateTime firstDay = new DateTime(startDate.Year, startDate.Month, startDate.Day, 00, 00, 00);
                    DateTime lastDay = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                    foreach (FolderData dt in listYearFolder)
                    {
                        if (true == dt.IsFolderInYearRange(startDate, endDate)) //folder Year อยู่ในช่วงนั้น      
                        {
                            List<FolderData> listFile = getListOfFileInDirectory(dt.m_dtDirectory);
                            foreach (FolderData dtFile in listFile)
                            {
                                FolderDataDetail tdFile = new FolderDataDetail();
                                string ext = Path.GetExtension(dtFile.m_dtFile);
                                if (ext == ".xls"|| ext == ".xlsx")
                                {
                                    tdFile.m_dtFile = dtFile.m_dtFile;
                                    tdFile.m_dtFolder = dtFile.m_dtFolder;
                                    tdFile.m_dtDirectory = dtFile.m_dtDirectory;
                                    tdFile.m_FileModify = dtFile.m_FileModify;
                                    tdFile.m_FolderModify = dtFile.m_FolderModify;
                                    tdFile.TEMPLATE_ID = templateData.ID;
                                    listData.Add(tdFile);
                                }
                                
                            }
                        }
                    }
                }
                #endregion
                #region TEMPLATE_TYPE WatseWater
                //if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.WatseWater)
                //{
                //    List<FolderData> listFile = getListOfFileInDirectory(templateData.PATH_NAME);
                //    DateTime firstDayOfMonth = new DateTime(startDate.Year, startDate.Month, startDate.Day, 00, 00, 00);
                //    DateTime lastDayOfMonth = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                //    foreach (FolderData dtFile in listFile)
                //    {
                //        int targetLength = fileName.Length;
                //        string fileDate = dtFile.m_dtFile.Substring(targetLength + 1, 4); //ดึง เฉพาะปี 4 ตัว
                //        DateTime dateOfFile = new DateTime(Int16.Parse(fileDate), 1, 1); //สร้างวันเดือนเป็นวันแรกและเดือนแรกของปีนั้น

                //        if (dateOfFile >= firstDayOfMonth && dateOfFile <= lastDayOfMonth)
                //        {
                //            FolderDataDetail tdFile = new FolderDataDetail();
                //            tdFile.m_dtFile = dtFile.m_dtFile;
                //            tdFile.m_dtFolder = dtFile.m_dtFolder;
                //            tdFile.m_dtDirectory = dtFile.m_dtDirectory;
                //            tdFile.m_FileModify = dtFile.m_FileModify;
                //            tdFile.m_FolderModify = dtFile.m_FolderModify;
                //            tdFile.TEMPLATE_ID = templateData.ID;
                //            listData.Add(tdFile);
                //        }

                //    }
                //}

                //if(templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.ObservePonds 
                //|| templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.OilyWaters
                //|| templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.WatseWater)
                //{
                //    List<FolderData> listFile = getListOfFileInDirectory(templateData.PATH_NAME);

                //    DateTime firstDayOfMonth = new DateTime(startDate.Year, startDate.Month, startDate.Day, 00, 00, 00); 
                //    DateTime lastDayOfMonth = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                //    foreach (FolderData dtFile in listFile)
                //    {
                //        int targetLength = fileName.Length;
                //        //string fileDate = dtFile.m_dtFile.Substring(targetLength + 1, 4); //ดึง เฉพาะปี 4 ตัว
                //        string fileDate = dtFile.m_dtFile.Substring(targetLength , 4); //ดึง เฉพาะปี 4 ตัว

                //        if ( (Int16.Parse(fileDate)) < 2000 && templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.Emission)
                //        {
                //            fileDate = dtFile.m_dtFile.Substring(targetLength, 5); //ดึง เฉพาะปี 4 ตัว นับ space 1 ตัว
                //        }

                //        DateTime dateOfFile = new DateTime(Int16.Parse(fileDate), 1, 1); //สร้างวันเดือนเป็นวันแรกและเดือนแรกของปีนั้น

                //        if (dateOfFile >= firstDayOfMonth && dateOfFile <= lastDayOfMonth)
                //        {
                //            FolderDataDetail tdFile = new FolderDataDetail();
                //            tdFile.m_dtFile = dtFile.m_dtFile;
                //            tdFile.m_dtFolder = dtFile.m_dtFolder;
                //            tdFile.m_dtDirectory = dtFile.m_dtDirectory;
                //            tdFile.m_FileModify = dtFile.m_FileModify;
                //            tdFile.m_FolderModify = dtFile.m_FolderModify;
                //            tdFile.TEMPLATE_ID = templateData.ID;
                //            listData.Add(tdFile);
                //        }

                //    }

                //}
                #endregion
                #region TEMPLATE_TYPE HotOilFlashPoint
                if (templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HotOilPhysical
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HotOilFlashPoint
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.TwoHundredThousandPond
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgInPL
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgInPLMonthly
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgOutletMRU
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasMonthly
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasWeekly
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.HgStab
                || templateData.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.AcidOffGas)
                {
                    List<FolderData> listFile = getListOfFileInDirectory(templateData.PATH_NAME);
                    DateTime firstDayOfMonth = new DateTime(startDate.Year, startDate.Month, startDate.Day, 00, 00, 00);
                    DateTime lastDayOfMonth = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                    foreach (FolderData dtFile in listFile)
                    {
                        if (dtFile.m_FileModify >= firstDayOfMonth && dtFile.m_FileModify <= lastDayOfMonth)
                        {
                            FolderDataDetail tdFile = new FolderDataDetail();
                            string ext = Path.GetExtension(dtFile.m_dtFile);
                            if (ext == ".xls" || ext == ".xlsx")
                            {
                                tdFile.m_dtFile = dtFile.m_dtFile;
                                tdFile.m_dtFolder = dtFile.m_dtFolder;
                                tdFile.m_dtDirectory = dtFile.m_dtDirectory;
                                tdFile.m_FileModify = dtFile.m_FileModify;
                                tdFile.m_FolderModify = dtFile.m_FolderModify;
                                tdFile.TEMPLATE_ID = templateData.ID;
                                listData.Add(tdFile);
                            }
                        }

                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listData;
        }

        #region QMS_ST_AUTHORIZATION
        public CreateQMS_ST_AUTHORIZATION getCreateQMS_ST_AUTHORIZATIONById(QMSDBEntities db, long id)
        {
            CreateQMS_ST_AUTHORIZATION createAuth = new CreateQMS_ST_AUTHORIZATION();
            try
            {
                createAuth.ID = id;
                QMS_ST_AUTHORIZATION data = QMS_ST_AUTHORIZATION.GetById(db, id);
                if (data != null)
                {
                    createAuth.ID = id;
                    createAuth.GROUP_AUTH = data.GROUP_AUTH;
                    createAuth.LIST_AUTH = data.LIST_AUTH;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return createAuth;
        }

        public List<AuthenAccess> convertToCurrentListAuthenAccess(List<AuthenAccess> listAuthen)
        {
            List<AuthenAccess> listResult = listAuthen;
            UserService usrService = new UserService(_currentUserName);
            try
            {
                listResult = usrService.getDefaultListAuthenAccess(false);

                foreach (AuthenAccess dt in listResult)
                {
                    AuthenAccess userData = listAuthen.Where(m => m.code == dt.code).FirstOrDefault();
                    if (null != userData)
                    {
                        dt.access = userData.access;
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return listResult;
        }


        public CreateQMS_ST_AUTHORIZATION getCreateQMS_ST_AUTHORIZATIONByGROUP_AUTH(QMSDBEntities db, byte id)
        {
            CreateQMS_ST_AUTHORIZATION createAuth = new CreateQMS_ST_AUTHORIZATION();
            try
            {
                createAuth.GROUP_AUTH = id;
                QMS_ST_AUTHORIZATION data = QMS_ST_AUTHORIZATION.GetByGROUP_AUTHId(db, id);
                if (data != null)
                {
                    createAuth.ID = data.ID;
                    createAuth.GROUP_AUTH = data.GROUP_AUTH;
                    createAuth.LIST_AUTH = data.LIST_AUTH;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return createAuth;
        }


        public List<CreateQMS_ST_AUTHORIZATION> getQMS_ST_AUTHORIZATIONList(QMSDBEntities db)
        {
            List<CreateQMS_ST_AUTHORIZATION> listResult = new List<CreateQMS_ST_AUTHORIZATION>();

            try
            {
                List<QMS_ST_AUTHORIZATION> list = QMS_ST_AUTHORIZATION.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new CreateQMS_ST_AUTHORIZATION
                                  {
                                      ID = dt.ID,
                                      GROUP_AUTH = dt.GROUP_AUTH,
                                      LIST_AUTH = dt.LIST_AUTH
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
                //this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_ST_AUTHORIZATION convertModelToDB(CreateQMS_ST_AUTHORIZATION model)
        {
            QMS_ST_AUTHORIZATION result = new QMS_ST_AUTHORIZATION();

            result.ID = model.ID;
            result.GROUP_AUTH = model.GROUP_AUTH;
            result.LIST_AUTH = (null == model.LIST_AUTH) ? "" : model.LIST_AUTH;


            return result;
        }

        public bool DeleteQMS_ST_AUTHORIZATIONById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_ST_AUTHORIZATION result = new QMS_ST_AUTHORIZATION();
            result = QMS_ST_AUTHORIZATION.GetById(db, id);
            try
            {
                if (null != result)
                {
                    db.QMS_ST_AUTHORIZATION.Remove(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }


        public long SaveQMS_ST_AUTHORIZATION(QMSDBEntities db, CreateQMS_ST_AUTHORIZATION model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_ST_AUTHORIZATION(db, model);
            }
            else
            {
                result = AddQMS_ST_AUTHORIZATION(db, convertModelToDB(model));
            }

            this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_Authen);
            return result;
        }

        public long UpdateQMS_ST_AUTHORIZATION(QMSDBEntities db, CreateQMS_ST_AUTHORIZATION model)
        {
            long result = 0;

            try
            {
                QMS_ST_AUTHORIZATION dt = new QMS_ST_AUTHORIZATION();
                dt = QMS_ST_AUTHORIZATION.GetById(db, model.ID);

                dt.GROUP_AUTH = model.GROUP_AUTH;
                dt.LIST_AUTH = (null == model.LIST_AUTH) ? "" : model.LIST_AUTH;
                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_ST_AUTHORIZATION(QMSDBEntities _db, QMS_ST_AUTHORIZATION model)
        {
            long result = 0;
            try
            {
                QMS_ST_AUTHORIZATION dt = new QMS_ST_AUTHORIZATION();
                dt = QMS_ST_AUTHORIZATION.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_ST_AUTHORIZATION.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_ST_EXPORT_PREFIX
        public List<CreateQMS_ST_EXPORT_PREFIX> getQMS_ST_EXPORT_PREFIXList(QMSDBEntities db)
        {
            List<CreateQMS_ST_EXPORT_PREFIX> listResult = new List<CreateQMS_ST_EXPORT_PREFIX>();

            try
            {
                List<QMS_ST_EXPORT_PREFIX> list = QMS_ST_EXPORT_PREFIX.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new CreateQMS_ST_EXPORT_PREFIX
                                  {
                                      ID = dt.ID,
                                      DOWNTIME = dt.DOWNTIME,
                                      DOWNTIME_REMARK = dt.DOWNTIME_REMARK,
                                      LPG_GRADE = dt.LPG_GRADE,
                                      PLANT_ID = dt.PLANT_ID,
                                      REDUCE_FEED = dt.REDUCE_FEED,
                                      REDUCE_FEED_REMARK = dt.REDUCE_FEED_REMARK,

                                      GC_ERROR_1 = dt.GC_ERROR_1,
                                      GC_ERROR_2 = dt.GC_ERROR_2,
                                      GC_ERROR_3 = dt.GC_ERROR_3,
                                      GC_ERROR_4 = dt.GC_ERROR_4,
                                      GC_ERROR_5 = dt.GC_ERROR_5,
                                      GC_ERROR_REMARK = dt.GC_ERROR_REMARK,

                                      GC_CYCCOUNT_1 = dt.GC_CYCCOUNT_1,
                                      GC_CYCCOUNT_2 = dt.GC_CYCCOUNT_2,
                                      GC_CYCCOUNT_3 = dt.GC_CYCCOUNT_3,
                                      GC_CYCCOUNT_4 = dt.GC_CYCCOUNT_4,
                                      GC_CYCCOUNT_5 = dt.GC_CYCCOUNT_5,
                                      GC_CYCCOUNT_6 = dt.GC_CYCCOUNT_6,
                                      GC_CYCCOUNT_7 = dt.GC_CYCCOUNT_7,
                                      GC_CYCCOUNT_8 = dt.GC_CYCCOUNT_8,
                                      GC_CYCCOUNT_9 = dt.GC_CYCCOUNT_9,
                                      GC_CYCCOUNT_10 = dt.GC_CYCCOUNT_10

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_ST_EXPORT_PREFIX convertModelToDB(CreateQMS_ST_EXPORT_PREFIX model)
        {
            QMS_ST_EXPORT_PREFIX result = new QMS_ST_EXPORT_PREFIX();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;

            result.DOWNTIME = (null == model.DOWNTIME) ? "" : model.DOWNTIME;
            result.DOWNTIME_REMARK = (null == model.DOWNTIME_REMARK) ? "" : model.DOWNTIME_REMARK;
            result.LPG_GRADE = (null == model.LPG_GRADE) ? "" : model.LPG_GRADE;
            result.REDUCE_FEED = (null == model.REDUCE_FEED) ? "" : model.REDUCE_FEED;
            result.REDUCE_FEED_REMARK = (null == model.REDUCE_FEED_REMARK) ? "" : model.REDUCE_FEED_REMARK;

            result.GC_ERROR_1 = (null == model.GC_ERROR_1) ? "" : model.GC_ERROR_1;
            result.GC_ERROR_2 = (null == model.GC_ERROR_2) ? "" : model.GC_ERROR_2;
            result.GC_ERROR_3 = (null == model.GC_ERROR_3) ? "" : model.GC_ERROR_3;
            result.GC_ERROR_4 = (null == model.GC_ERROR_4) ? "" : model.GC_ERROR_4;
            result.GC_ERROR_5 = (null == model.GC_ERROR_5) ? "" : model.GC_ERROR_5;
            result.GC_ERROR_REMARK = (null == model.GC_ERROR_REMARK) ? "" : model.GC_ERROR_REMARK;

            result.GC_CYCCOUNT_1 = (null == model.GC_CYCCOUNT_1) ? "" : model.GC_CYCCOUNT_1;
            result.GC_CYCCOUNT_2 = (null == model.GC_CYCCOUNT_2) ? "" : model.GC_CYCCOUNT_2;
            result.GC_CYCCOUNT_3 = (null == model.GC_CYCCOUNT_3) ? "" : model.GC_CYCCOUNT_3;
            result.GC_CYCCOUNT_4 = (null == model.GC_CYCCOUNT_4) ? "" : model.GC_CYCCOUNT_4;
            result.GC_CYCCOUNT_5 = (null == model.GC_CYCCOUNT_5) ? "" : model.GC_CYCCOUNT_5;
            result.GC_CYCCOUNT_6 = (null == model.GC_CYCCOUNT_6) ? "" : model.GC_CYCCOUNT_6;
            result.GC_CYCCOUNT_7 = (null == model.GC_CYCCOUNT_7) ? "" : model.GC_CYCCOUNT_7;
            result.GC_CYCCOUNT_8 = (null == model.GC_CYCCOUNT_8) ? "" : model.GC_CYCCOUNT_8;
            result.GC_CYCCOUNT_9 = (null == model.GC_CYCCOUNT_9) ? "" : model.GC_CYCCOUNT_9;
            result.GC_CYCCOUNT_10 = (null == model.GC_CYCCOUNT_10) ? "" : model.GC_CYCCOUNT_10;


            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_ST_EXPORT_PREFIX convertDBToModel(QMS_ST_EXPORT_PREFIX model)
        {
            CreateQMS_ST_EXPORT_PREFIX result = new CreateQMS_ST_EXPORT_PREFIX();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;

            result.DOWNTIME = (null == model.DOWNTIME) ? "" : model.DOWNTIME;
            result.DOWNTIME_REMARK = (null == model.DOWNTIME_REMARK) ? "" : model.DOWNTIME_REMARK;
            result.LPG_GRADE = (null == model.LPG_GRADE) ? "" : model.LPG_GRADE;
            result.REDUCE_FEED = (null == model.REDUCE_FEED) ? "" : model.REDUCE_FEED;
            result.REDUCE_FEED_REMARK = (null == model.REDUCE_FEED_REMARK) ? "" : model.REDUCE_FEED_REMARK;

            result.GC_ERROR_1 = (null == model.GC_ERROR_1) ? "" : model.GC_ERROR_1;
            result.GC_ERROR_2 = (null == model.GC_ERROR_2) ? "" : model.GC_ERROR_2;
            result.GC_ERROR_3 = (null == model.GC_ERROR_3) ? "" : model.GC_ERROR_3;
            result.GC_ERROR_4 = (null == model.GC_ERROR_4) ? "" : model.GC_ERROR_4;
            result.GC_ERROR_5 = (null == model.GC_ERROR_5) ? "" : model.GC_ERROR_5;
            result.GC_ERROR_REMARK = (null == model.GC_ERROR_REMARK) ? "" : model.GC_ERROR_REMARK;

            result.GC_CYCCOUNT_1 = (null == model.GC_CYCCOUNT_1) ? "" : model.GC_CYCCOUNT_1;
            result.GC_CYCCOUNT_2 = (null == model.GC_CYCCOUNT_2) ? "" : model.GC_CYCCOUNT_2;
            result.GC_CYCCOUNT_3 = (null == model.GC_CYCCOUNT_3) ? "" : model.GC_CYCCOUNT_3;
            result.GC_CYCCOUNT_4 = (null == model.GC_CYCCOUNT_4) ? "" : model.GC_CYCCOUNT_4;
            result.GC_CYCCOUNT_5 = (null == model.GC_CYCCOUNT_5) ? "" : model.GC_CYCCOUNT_5;
            result.GC_CYCCOUNT_6 = (null == model.GC_CYCCOUNT_6) ? "" : model.GC_CYCCOUNT_6;
            result.GC_CYCCOUNT_7 = (null == model.GC_CYCCOUNT_7) ? "" : model.GC_CYCCOUNT_7;
            result.GC_CYCCOUNT_8 = (null == model.GC_CYCCOUNT_8) ? "" : model.GC_CYCCOUNT_8;
            result.GC_CYCCOUNT_9 = (null == model.GC_CYCCOUNT_9) ? "" : model.GC_CYCCOUNT_9;
            result.GC_CYCCOUNT_10 = (null == model.GC_CYCCOUNT_10) ? "" : model.GC_CYCCOUNT_10;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            return result;
        }

        public bool DeleteQMS_ST_EXPORT_PREFIXById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_ST_EXPORT_PREFIX result = new QMS_ST_EXPORT_PREFIX();
            result = QMS_ST_EXPORT_PREFIX.GetById(db, id);
            try
            {
                if (null != result)
                {
                    db.QMS_ST_EXPORT_PREFIX.Remove(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_ST_EXPORT_PREFIXByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_ST_EXPORT_PREFIX> result = new List<QMS_ST_EXPORT_PREFIX>();
            result = QMS_ST_EXPORT_PREFIX.GetByListId(db, ids);
            try
            {
                if (null != result)
                {
                    //result.ForEach(m => m.DELETE_FLAG = 1);
                    //result.ForEach(db.DeleteObject); //db.DeleteObject(result);
                    db.QMS_ST_EXPORT_PREFIX.RemoveRange(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_ST_EXPORT_PREFIX(QMSDBEntities db, CreateQMS_ST_EXPORT_PREFIX model)
        {
            long result = 0;

            result = CheckDuplicatePlant(db, model);

            if (result == 0)
            {
                if (model.ID > 0)
                {
                    result = UpdateQMS_ST_EXPORT_PREFIX(db, model);
                }
                else
                {
                    result = AddQMS_ST_EXPORT_PREFIX(db, convertModelToDB(model));
                }
            }

            return result;
        }

        public long UpdateQMS_ST_EXPORT_PREFIX(QMSDBEntities db, CreateQMS_ST_EXPORT_PREFIX model)
        {
            long result = 0;

            try
            {
                QMS_ST_EXPORT_PREFIX dt = new QMS_ST_EXPORT_PREFIX();
                dt = QMS_ST_EXPORT_PREFIX.GetById(db, model.ID);

                dt.ID = model.ID;
                dt.PLANT_ID = model.PLANT_ID;

                dt.DOWNTIME = (null == model.DOWNTIME) ? "" : model.DOWNTIME;
                dt.DOWNTIME_REMARK = (null == model.DOWNTIME_REMARK) ? "" : model.DOWNTIME_REMARK;
                dt.LPG_GRADE = (null == model.LPG_GRADE) ? "" : model.LPG_GRADE;
                dt.REDUCE_FEED = (null == model.REDUCE_FEED) ? "" : model.REDUCE_FEED;
                dt.REDUCE_FEED_REMARK = (null == model.REDUCE_FEED_REMARK) ? "" : model.REDUCE_FEED_REMARK;

                dt.GC_ERROR_1 = (null == model.GC_ERROR_1) ? "" : model.GC_ERROR_1;
                dt.GC_ERROR_2 = (null == model.GC_ERROR_2) ? "" : model.GC_ERROR_2;
                dt.GC_ERROR_3 = (null == model.GC_ERROR_3) ? "" : model.GC_ERROR_3;
                dt.GC_ERROR_4 = (null == model.GC_ERROR_4) ? "" : model.GC_ERROR_4;
                dt.GC_ERROR_5 = (null == model.GC_ERROR_5) ? "" : model.GC_ERROR_5;
                dt.GC_ERROR_REMARK = (null == model.GC_ERROR_REMARK) ? "" : model.GC_ERROR_REMARK;

                dt.GC_CYCCOUNT_1 = (null == model.GC_CYCCOUNT_1) ? "" : model.GC_CYCCOUNT_1;
                dt.GC_CYCCOUNT_2 = (null == model.GC_CYCCOUNT_2) ? "" : model.GC_CYCCOUNT_2;
                dt.GC_CYCCOUNT_3 = (null == model.GC_CYCCOUNT_3) ? "" : model.GC_CYCCOUNT_3;
                dt.GC_CYCCOUNT_4 = (null == model.GC_CYCCOUNT_4) ? "" : model.GC_CYCCOUNT_4;
                dt.GC_CYCCOUNT_5 = (null == model.GC_CYCCOUNT_5) ? "" : model.GC_CYCCOUNT_5;
                dt.GC_CYCCOUNT_6 = (null == model.GC_CYCCOUNT_6) ? "" : model.GC_CYCCOUNT_6;
                dt.GC_CYCCOUNT_7 = (null == model.GC_CYCCOUNT_7) ? "" : model.GC_CYCCOUNT_7;
                dt.GC_CYCCOUNT_8 = (null == model.GC_CYCCOUNT_8) ? "" : model.GC_CYCCOUNT_8;
                dt.GC_CYCCOUNT_9 = (null == model.GC_CYCCOUNT_9) ? "" : model.GC_CYCCOUNT_9;
                dt.GC_CYCCOUNT_10 = (null == model.GC_CYCCOUNT_10) ? "" : model.GC_CYCCOUNT_10;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_ST_EXPORT_PREFIX(QMSDBEntities _db, QMS_ST_EXPORT_PREFIX model)
        {
            long result = 0;
            try
            {
                QMS_ST_EXPORT_PREFIX dt = new QMS_ST_EXPORT_PREFIX();
                dt = QMS_ST_EXPORT_PREFIX.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_ST_EXPORT_PREFIX.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }


        public long CheckDuplicatePlant(QMSDBEntities db, CreateQMS_ST_EXPORT_PREFIX model)
        {
            long result = 0;

            try
            {
                QMS_ST_EXPORT_PREFIX duplicate = new QMS_ST_EXPORT_PREFIX();

                if (model.ID > 0)
                {
                    duplicate = QMS_ST_EXPORT_PREFIX.GetByPlantIdSkipId(db, model.ID, model.PLANT_ID);
                }
                else
                {
                    duplicate = QMS_ST_EXPORT_PREFIX.GetByPlantId(db, model.PLANT_ID);
                }


                if (null != duplicate && duplicate.ID > 0)
                {
                    result = -1; // Error
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }
            return result;
        }

        public CreateQMS_ST_EXPORT_PREFIX getCreateQMS_ST_EXPORT_PREFIXById(QMSDBEntities db, long id)
        {
            CreateQMS_ST_EXPORT_PREFIX objResult = new CreateQMS_ST_EXPORT_PREFIX();

            try
            {
                QMS_ST_EXPORT_PREFIX dt = new QMS_ST_EXPORT_PREFIX();
                dt = QMS_ST_EXPORT_PREFIX.GetById(db, id);

                if(dt != null)
                    objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_ST_EXPORT_PREFIX> searchCreateQMS_ST_EXPORT_PREFIX(QMSDBEntities db, ExportPrefixSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_ST_EXPORT_PREFIX> objResult = new List<ViewQMS_ST_EXPORT_PREFIX>();
            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();
            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }
                MasterDataService masterData = new MasterDataService(_currentUserName);
                List<QMS_ST_EXPORT_PREFIX> listData = new List<QMS_ST_EXPORT_PREFIX>();
                listData = QMS_ST_EXPORT_PREFIX.GetAllBySearch(db, searchModel.mSearch);
                List<ViewQMS_MA_PLANT> listPlant = masterData.getQMS_MA_PLANTActiveList(db);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_ST_EXPORT_PREFIX>(listData.AsQueryable(), out count, out totalPage);
                    objResult = (from dt in query
                                 select new ViewQMS_ST_EXPORT_PREFIX
                                 {
                                     ID = dt.ID,
                                     PLANT_ID = dt.PLANT_ID,
                                     PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                     ITEM_SELECTED = false,

                                     DOWNTIME = dt.DOWNTIME,
                                     DOWNTIME_REMARK = dt.DOWNTIME_REMARK,
                                     LPG_GRADE = dt.LPG_GRADE,
                                     REDUCE_FEED = dt.REDUCE_FEED,
                                     REDUCE_FEED_REMARK = dt.REDUCE_FEED_REMARK,

                                     GC_ERROR_1 = dt.GC_ERROR_1,
                                     GC_ERROR_2 = dt.GC_ERROR_2,
                                     GC_ERROR_3 = dt.GC_ERROR_3,
                                     GC_ERROR_4 = dt.GC_ERROR_4,
                                     GC_ERROR_5 = dt.GC_ERROR_5,
                                     GC_ERROR_REMARK = dt.GC_ERROR_REMARK,

                                     GC_CYCCOUNT_1 = dt.GC_CYCCOUNT_1,
                                     GC_CYCCOUNT_2 = dt.GC_CYCCOUNT_2,
                                     GC_CYCCOUNT_3 = dt.GC_CYCCOUNT_3,
                                     GC_CYCCOUNT_4 = dt.GC_CYCCOUNT_4,
                                     GC_CYCCOUNT_5 = dt.GC_CYCCOUNT_5,
                                     GC_CYCCOUNT_6 = dt.GC_CYCCOUNT_6,
                                     GC_CYCCOUNT_7 = dt.GC_CYCCOUNT_7,
                                     GC_CYCCOUNT_8 = dt.GC_CYCCOUNT_8,
                                     GC_CYCCOUNT_9 = dt.GC_CYCCOUNT_9,
                                     GC_CYCCOUNT_10 = dt.GC_CYCCOUNT_10,

                                     SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss"),
                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER,
                                     SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss")

                                 }).ToList();
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        #endregion

        #region QMS_ST_EXAQUATUM_SCHEDULE

        public List<ViewQMS_ST_EXAQUATUM_SCHEDULE> convertListData(QMSDBEntities db, List<QMS_ST_EXAQUATUM_SCHEDULE> list)
        {
            MasterDataService masterService = new MasterDataService(_currentUserName);

            List<ViewQMS_ST_EXAQUATUM_SCHEDULE> listResult = new List<ViewQMS_ST_EXAQUATUM_SCHEDULE>();
            List<ViewQMS_MA_PLANT> listPlant = masterService.getQMS_MA_PLANTActiveList(db);

            try
            {
                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_ST_EXAQUATUM_SCHEDULE
                                  {
                                      ID = dt.ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      PLANT_NAME = getPlantName(listPlant, dt.PLANT_ID),
                                      DOC_STATUS = dt.DOC_STATUS,
                                      DOC_STATUS_NAME = getExaScheduleName(dt.DOC_STATUS),
                                      END_DATE = dt.END_DATE,
                                      START_DATE = dt.START_DATE,

                                      CREATE_DATE = dt.CREATE_DATE,
                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      UPDATE_DATE = dt.UPDATE_DATE,

                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss"),
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss")
                                      //DELETE_FLAG = dt.DELETE_FLAG

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_ST_EXAQUATUM_SCHEDULE> searchQMS_ST_EXAQUATUM_SCHEDULE(QMSDBEntities db, ExaScheduleSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_ST_EXAQUATUM_SCHEDULE> objResult = new List<ViewQMS_ST_EXAQUATUM_SCHEDULE>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "PLANT_ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_ST_EXAQUATUM_SCHEDULE> listData = new List<QMS_ST_EXAQUATUM_SCHEDULE>();
                listData = QMS_ST_EXAQUATUM_SCHEDULE.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    var query = grid.LoadGridData<QMS_ST_EXAQUATUM_SCHEDULE>(listData.AsQueryable(), out count, out totalPage).ToList();
                    objResult = convertListData(db, query);
                    listPageIndex = getPageIndexList(totalPage);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<CreateQMS_ST_EXAQUATUM_SCHEDULE> getQMS_ST_EXAQUATUM_SCHEDULEList(QMSDBEntities db)
        {
            List<CreateQMS_ST_EXAQUATUM_SCHEDULE> listResult = new List<CreateQMS_ST_EXAQUATUM_SCHEDULE>();

            try
            {
                List<QMS_ST_EXAQUATUM_SCHEDULE> list = QMS_ST_EXAQUATUM_SCHEDULE.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new CreateQMS_ST_EXAQUATUM_SCHEDULE
                                  {
                                      ID = dt.ID,
                                      PLANT_ID = dt.PLANT_ID,
                                      START_DATE = dt.START_DATE,
                                      END_DATE = dt.END_DATE,
                                      DOC_STATUS = dt.DOC_STATUS,

                                      SHOW_CREATE_DATE = dt.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss"),
                                      CREATE_USER = dt.CREATE_USER,
                                      UPDATE_USER = dt.UPDATE_USER,
                                      SHOW_UPDATE_DATE = dt.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss")
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        public List<ViewQMS_ST_EXAQUATUM_SCHEDULE> getQMS_ST_EXAQUATUM_SCHEDULEListByStatus(QMSDBEntities db, int status)
        {
            List<ViewQMS_ST_EXAQUATUM_SCHEDULE> listResult = new List<ViewQMS_ST_EXAQUATUM_SCHEDULE>();

            try
            {
                List<QMS_ST_EXAQUATUM_SCHEDULE> list = QMS_ST_EXAQUATUM_SCHEDULE.GetAllByStatus(db, status);

                if (null != list && list.Count() > 0)
                {
                    listResult = convertListData(db, list);
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private CreateQMS_ST_EXAQUATUM_SCHEDULE convertDBToModel(QMS_ST_EXAQUATUM_SCHEDULE model)
        {
            CreateQMS_ST_EXAQUATUM_SCHEDULE result = new CreateQMS_ST_EXAQUATUM_SCHEDULE();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.DOC_STATUS = model.DOC_STATUS;

            //result.CREATE_DATE = model.CREATE_DATE;
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.CREATE_USER = model.CREATE_USER;
            //result.UPDATE_DATE = model.UPDATE_DATE;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.UPDATE_USER = model.UPDATE_USER;
            return result;
        }

        public CreateQMS_ST_EXAQUATUM_SCHEDULE getQMS_ST_EXAQUATUM_SCHEDULEById(QMSDBEntities db, long id)
        {
            CreateQMS_ST_EXAQUATUM_SCHEDULE objResult = new CreateQMS_ST_EXAQUATUM_SCHEDULE();

            try
            {
                QMS_ST_EXAQUATUM_SCHEDULE dt = new QMS_ST_EXAQUATUM_SCHEDULE();
                dt = QMS_ST_EXAQUATUM_SCHEDULE.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private QMS_ST_EXAQUATUM_SCHEDULE convertModelToDB(CreateQMS_ST_EXAQUATUM_SCHEDULE model)
        {
            QMS_ST_EXAQUATUM_SCHEDULE result = new QMS_ST_EXAQUATUM_SCHEDULE();

            result.ID = model.ID;
            result.PLANT_ID = model.PLANT_ID;
            result.START_DATE = model.START_DATE;
            result.END_DATE = model.END_DATE;
            result.DOC_STATUS = model.DOC_STATUS;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;
            return result;
        }

        public bool DeleteQMS_ST_EXAQUATUM_SCHEDULEById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_ST_EXAQUATUM_SCHEDULE result = new QMS_ST_EXAQUATUM_SCHEDULE();
            result = QMS_ST_EXAQUATUM_SCHEDULE.GetById(db, id);
            try
            {
                if (null != result)
                {
                    db.QMS_ST_EXAQUATUM_SCHEDULE.Remove(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_ST_EXAQUATUM_SCHEDULEByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_ST_EXAQUATUM_SCHEDULE> result = new List<QMS_ST_EXAQUATUM_SCHEDULE>();
            result = QMS_ST_EXAQUATUM_SCHEDULE.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_ST_EXAQUATUM_SCHEDULE(QMSDBEntities db, CreateQMS_ST_EXAQUATUM_SCHEDULE model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_ST_EXAQUATUM_SCHEDULE(db, model);
            }
            else
            {
                result = AddQMS_ST_EXAQUATUM_SCHEDULE(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_ST_EXAQUATUM_SCHEDULE(QMSDBEntities db, CreateQMS_ST_EXAQUATUM_SCHEDULE model)
        {
            long result = 0;

            try
            {
                QMS_ST_EXAQUATUM_SCHEDULE dt = new QMS_ST_EXAQUATUM_SCHEDULE();
                dt = QMS_ST_EXAQUATUM_SCHEDULE.GetById(db, model.ID);

                dt.ID = model.ID;
                dt.PLANT_ID = model.PLANT_ID;
                dt.START_DATE = model.START_DATE;
                dt.END_DATE = model.END_DATE;
                dt.DOC_STATUS = model.DOC_STATUS;

                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_ST_EXAQUATUM_SCHEDULE(QMSDBEntities _db, QMS_ST_EXAQUATUM_SCHEDULE model)
        {
            long result = 0;
            try
            {
                QMS_ST_EXAQUATUM_SCHEDULE dt = new QMS_ST_EXAQUATUM_SCHEDULE();
                dt = QMS_ST_EXAQUATUM_SCHEDULE.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_ST_EXAQUATUM_SCHEDULE.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        #endregion

        #region QMS_ST_FILE_ATTACH
        public List<ViewQMS_ST_FILE_ATTACH> searchQMS_ST_FILE_ATTACH(QMSDBEntities db, FileAttachSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_ST_FILE_ATTACH> objResult = new List<ViewQMS_ST_FILE_ATTACH>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "NAME";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_ST_FILE_ATTACH> listData = new List<QMS_ST_FILE_ATTACH>();
                listData = QMS_ST_FILE_ATTACH.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_ST_FILE_ATTACH
                                 {
                                     ID = dt.ID,
                                     BOARD_TYPE = dt.BOARD_TYPE,
                                     BOARD_NAME = revoleTypeReportName(dt.BOARD_TYPE),
                                     NAME = dt.NAME,
                                     PATH = dt.PATH,

                                     CREATE_DATE = dt.CREATE_DATE,
                                     CREATE_USER = dt.CREATE_USER,
                                     UPDATE_USER = dt.UPDATE_USER,
                                     UPDATE_DATE = dt.UPDATE_DATE
                                     //DELETE_FLAG = dt.DELETE_FLAG

                                 }).ToList();
                    objResult = grid.LoadGridData<ViewQMS_ST_FILE_ATTACH>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }


            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public CreateQMS_ST_FILE_ATTACH getQMS_ST_FILE_ATTACHById(QMSDBEntities db, long id)
        {
            CreateQMS_ST_FILE_ATTACH objResult = new CreateQMS_ST_FILE_ATTACH();

            try
            {
                QMS_ST_FILE_ATTACH dt = new QMS_ST_FILE_ATTACH();
                dt = QMS_ST_FILE_ATTACH.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        public List<ViewQMS_ST_FILE_ATTACH> getQMS_ST_FILE_ATTACHList(QMSDBEntities db)
        {
            List<ViewQMS_ST_FILE_ATTACH> listResult = new List<ViewQMS_ST_FILE_ATTACH>();


            try
            {
                List<QMS_ST_FILE_ATTACH> list = QMS_ST_FILE_ATTACH.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new ViewQMS_ST_FILE_ATTACH
                                  {
                                      ID = dt.ID,
                                      BOARD_TYPE = dt.BOARD_TYPE,
                                      BOARD_NAME = revoleTypeReportName(dt.BOARD_TYPE),
                                      NAME = dt.NAME,
                                      PATH = dt.PATH

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_ST_FILE_ATTACH convertModelToDB(CreateQMS_ST_FILE_ATTACH model)
        {
            QMS_ST_FILE_ATTACH result = new QMS_ST_FILE_ATTACH();

            result.ID = model.ID;
            result.BOARD_TYPE = model.BOARD_TYPE;

            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.PATH = (null == model.PATH) ? "" : model.PATH;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        private CreateQMS_ST_FILE_ATTACH convertDBToModel(QMS_ST_FILE_ATTACH model)
        {
            CreateQMS_ST_FILE_ATTACH result = new CreateQMS_ST_FILE_ATTACH();

            result.ID = model.ID;
            result.BOARD_TYPE = model.BOARD_TYPE;

            result.NAME = (null == model.NAME) ? "" : model.NAME;
            result.PATH = (null == model.PATH) ? "" : model.PATH;

            result.CREATE_DATE = model.CREATE_DATE;
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_DATE = model.UPDATE_DATE;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.UPDATE_USER = model.UPDATE_USER;

            return result;
        }

        public bool DeleteQMS_ST_FILE_ATTACHById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_ST_FILE_ATTACH result = new QMS_ST_FILE_ATTACH();
            result = QMS_ST_FILE_ATTACH.GetById(db, id);
            try
            {
                if (null != result)
                {
                    result.DELETE_FLAG = 1;
                    result.UPDATE_DATE = DateTime.Now;
                    result.UPDATE_USER = _currentUserName;
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_ST_FILE_ATTACHByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_ST_FILE_ATTACH> result = new List<QMS_ST_FILE_ATTACH>();
            result = QMS_ST_FILE_ATTACH.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    result.ForEach(m =>
                    {
                        m.DELETE_FLAG = 1;
                        m.UPDATE_DATE = dtCurrent;
                        m.UPDATE_USER = _currentUserName;
                    });

                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_ST_FILE_ATTACH(QMSDBEntities db, CreateQMS_ST_FILE_ATTACH model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_ST_FILE_ATTACH(db, model);
            }
            else
            {
                result = AddQMS_ST_FILE_ATTACH(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_ST_FILE_ATTACH(QMSDBEntities db, CreateQMS_ST_FILE_ATTACH model)
        {
            long result = 0;

            try
            {
                QMS_ST_FILE_ATTACH dt = new QMS_ST_FILE_ATTACH();
                dt = QMS_ST_FILE_ATTACH.GetById(db, model.ID);


                dt.ID = model.ID;
                dt.BOARD_TYPE = model.BOARD_TYPE;

                dt.NAME = (null == model.NAME) ? "" : model.NAME;
                dt.PATH = (null == model.PATH) ? "" : model.PATH;


                //dt.CREATE_DATE = DateTime.Now;
                //dt.CREATE_USER = _currentUserName;
                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_ST_FILE_ATTACH(QMSDBEntities _db, QMS_ST_FILE_ATTACH model)
        {
            long result = 0;
            try
            {
                QMS_ST_FILE_ATTACH dt = new QMS_ST_FILE_ATTACH();
                dt = QMS_ST_FILE_ATTACH.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_ST_FILE_ATTACH.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion 

        #region QMS_ST_SYSTEM_CONFIG
        public CreateQMS_ST_SYSTEM_CONFIG getQMS_ST_SYSTEM_CONFIGById(QMSDBEntities db, long id)
        {
            CreateQMS_ST_SYSTEM_CONFIG objResult = new CreateQMS_ST_SYSTEM_CONFIG();

            try
            {
                QMS_ST_SYSTEM_CONFIG dt = new QMS_ST_SYSTEM_CONFIG();
                dt = QMS_ST_SYSTEM_CONFIG.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private CreateQMS_ST_SYSTEM_CONFIG convertDBToModel(QMS_ST_SYSTEM_CONFIG model)
        {
            CreateQMS_ST_SYSTEM_CONFIG result = new CreateQMS_ST_SYSTEM_CONFIG();

            result.ID = model.ID;

            result.ABNORMAL_ALPHABET = (null == model.ABNORMAL_ALPHABET) ? "" : model.ABNORMAL_ALPHABET;
            result.ABNORMAL_MAX_REPEAT = model.ABNORMAL_MAX_REPEAT;
            result.ABNORMAL_OVERSHOOT = model.ABNORMAL_OVERSHOOT;

            result.EXA_SCHEDULE = model.EXA_SCHEDULE;

            result.HIGHLIGHT_1 = model.HIGHLIGHT_1;
            result.HIGHLIGHT_2 = model.HIGHLIGHT_2;
            result.HIGHLIGHT_3 = model.HIGHLIGHT_3;
            result.HIGHLIGHT_4 = model.HIGHLIGHT_4;

            result.HOME_BACKGROUND = model.HOME_BACKGROUND;
            result.PRODUCT_CSC_PATH = (null == model.PRODUCT_CSC_PATH) ? "" : model.PRODUCT_CSC_PATH;
            result.PRODUCT_CSC_NAME = (null == model.PRODUCT_CSC_NAME) ? "" : model.PRODUCT_CSC_NAME;

            result.LIMS_PASSWORD = (null == model.LIMS_PASSWORD) ? "" : model.LIMS_PASSWORD;
            result.LIMS_PORT = model.LIMS_PORT;
            result.LIMS_SCHEDULE = model.LIMS_SCHEDULE;
            result.LIMS_SERVER = (null == model.LIMS_SERVER) ? "" : model.LIMS_SERVER;
            result.LIMS_SID = model.LIMS_SID;
            result.LIMS_USER = (null == model.LIMS_USER) ? "" : model.LIMS_USER;

            result.LOWER_BOUNDARY = model.LOWER_BOUNDARY;
            result.UPPER_BOUNDARY = model.UPPER_BOUNDARY;

            result.TEMPLATE_SCHEDULE = model.TEMPLATE_SCHEDULE;

            result.REPORT_OFF_CONTROL = model.REPORT_OFF_CONTROL;
            result.REPORT_OFF_SPEC = model.REPORT_OFF_SPEC;



            return result;
        }

        public List<CreateQMS_ST_SYSTEM_CONFIG> getQMS_ST_SYSTEM_CONFIGList(QMSDBEntities db)
        {
            List<CreateQMS_ST_SYSTEM_CONFIG> listResult = new List<CreateQMS_ST_SYSTEM_CONFIG>();

            try
            {
                List<QMS_ST_SYSTEM_CONFIG> list = QMS_ST_SYSTEM_CONFIG.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new CreateQMS_ST_SYSTEM_CONFIG
                                  {
                                      ID = dt.ID,

                                      ABNORMAL_ALPHABET = dt.ABNORMAL_ALPHABET,
                                      ABNORMAL_MAX_REPEAT = dt.ABNORMAL_MAX_REPEAT,
                                      ABNORMAL_OVERSHOOT = dt.ABNORMAL_OVERSHOOT,

                                      EXA_SCHEDULE = dt.EXA_SCHEDULE,

                                      LAKE_SERVER = dt.LAKE_SERVER,
                                      LAKE_DATABASE = dt.LAKE_DATABASE,
                                      LAKE_USER = dt.LAKE_USER,
                                      LAKE_PASSWORD = dt.LAKE_PASSWORD,

                                      HIGHLIGHT_1 = dt.HIGHLIGHT_1,
                                      HIGHLIGHT_2 = dt.HIGHLIGHT_2,
                                      HIGHLIGHT_3 = dt.HIGHLIGHT_3,
                                      HIGHLIGHT_4 = dt.HIGHLIGHT_4,

                                      HOME_BACKGROUND = dt.HOME_BACKGROUND,
                                      PRODUCT_CSC_PATH = (null == dt.PRODUCT_CSC_PATH) ? "" : dt.PRODUCT_CSC_PATH,
                                      PRODUCT_CSC_NAME = (null == dt.PRODUCT_CSC_NAME) ? "" : dt.PRODUCT_CSC_NAME,
                                      LIMS_PASSWORD = dt.LIMS_PASSWORD,
                                      LIMS_PORT = dt.LIMS_PORT,
                                      LIMS_SCHEDULE = dt.LIMS_SCHEDULE,
                                      LIMS_SERVER = dt.LIMS_SERVER,
                                      LIMS_SID = dt.LIMS_SID,
                                      LIMS_USER = dt.LIMS_USER,

                                      LOWER_BOUNDARY = dt.LOWER_BOUNDARY,
                                      UPPER_BOUNDARY = dt.UPPER_BOUNDARY,

                                      TEMPLATE_SCHEDULE = dt.TEMPLATE_SCHEDULE,

                                      REPORT_OFF_CONTROL = dt.REPORT_OFF_CONTROL,
                                      REPORT_OFF_SPEC = dt.REPORT_OFF_SPEC

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_ST_SYSTEM_CONFIG convertModelToDB(CreateQMS_ST_SYSTEM_CONFIG model)
        {
            QMS_ST_SYSTEM_CONFIG result = new QMS_ST_SYSTEM_CONFIG();

            result.ID = model.ID;
            result.ABNORMAL_ALPHABET = model.ABNORMAL_ALPHABET;
            result.ABNORMAL_MAX_REPEAT = model.ABNORMAL_MAX_REPEAT;
            result.ABNORMAL_OVERSHOOT = model.ABNORMAL_OVERSHOOT;

            result.EXA_SCHEDULE = model.EXA_SCHEDULE;

            result.HIGHLIGHT_1 = model.HIGHLIGHT_1;
            result.HIGHLIGHT_2 = model.HIGHLIGHT_2;
            result.HIGHLIGHT_3 = model.HIGHLIGHT_3;
            result.HIGHLIGHT_4 = model.HIGHLIGHT_4;

            result.HOME_BACKGROUND = (null == model.HOME_BACKGROUND) ? "" : model.HOME_BACKGROUND;
            result.PRODUCT_CSC_PATH = (null == model.PRODUCT_CSC_PATH) ? "" : model.PRODUCT_CSC_PATH;
            result.PRODUCT_CSC_NAME = (null == model.PRODUCT_CSC_NAME) ? "" : model.PRODUCT_CSC_NAME;

            result.LIMS_PASSWORD = (null == model.LIMS_PASSWORD) ? "" : model.LIMS_PASSWORD;
            result.LIMS_PORT = model.LIMS_PORT;
            result.LIMS_SCHEDULE = model.LIMS_SCHEDULE;
            result.LIMS_SERVER = model.LIMS_SERVER;
            result.LIMS_SID = model.LIMS_SID;
            result.LIMS_USER = (null == model.LIMS_USER) ? "" : model.LIMS_USER;

            result.LOWER_BOUNDARY = model.LOWER_BOUNDARY;
            result.UPPER_BOUNDARY = model.UPPER_BOUNDARY;

            result.TEMPLATE_SCHEDULE = model.TEMPLATE_SCHEDULE;

            result.REPORT_OFF_CONTROL = model.REPORT_OFF_CONTROL;
            result.REPORT_OFF_SPEC = model.REPORT_OFF_SPEC;

            return result;
        }

        public bool DeleteQMS_ST_SYSTEM_CONFIGById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_ST_SYSTEM_CONFIG result = new QMS_ST_SYSTEM_CONFIG();
            result = QMS_ST_SYSTEM_CONFIG.GetById(db, id);
            try
            {
                if (null != result)
                {
                    db.QMS_ST_SYSTEM_CONFIG.Remove(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_ST_SYSTEM_CONFIG(QMSDBEntities db, CreateQMS_ST_SYSTEM_CONFIG model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_ST_SYSTEM_CONFIG(db, model);
            }
            else
            {
                result = AddQMS_ST_SYSTEM_CONFIG(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateOFFReportQMS_ST_SYSTEM_CONFIG(QMSDBEntities db, UpdateOFFReportQMS_ST_SYSTEM_CONFIG model)
        {
            long result = 0;

            try
            {
                QMS_ST_SYSTEM_CONFIG dt = new QMS_ST_SYSTEM_CONFIG();
                dt = QMS_ST_SYSTEM_CONFIG.GetById(db, model.ID);

                dt.ID = model.ID;
                dt.REPORT_OFF_CONTROL = model.REPORT_OFF_CONTROL;
                dt.REPORT_OFF_SPEC = model.REPORT_OFF_SPEC;

                db.SaveChanges();
                result = model.ID;
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_ReportOff);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long UpdateTrendReportQMS_ST_SYSTEM_CONFIG(QMSDBEntities db, UpdateTrendQMS_ST_SYSTEM_CONFIG model)
        {
            long result = 0;

            try
            {
                QMS_ST_SYSTEM_CONFIG dt = new QMS_ST_SYSTEM_CONFIG();
                dt = QMS_ST_SYSTEM_CONFIG.GetById(db, model.ID);

                dt.ID = model.ID;
                dt.HIGHLIGHT_1 = model.HIGHLIGHT_1;
                dt.HIGHLIGHT_2 = model.HIGHLIGHT_2;
                dt.HIGHLIGHT_3 = model.HIGHLIGHT_3;
                dt.HIGHLIGHT_4 = model.HIGHLIGHT_4;

                db.SaveChanges();
                result = model.ID;
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_TrendHightlight);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long UpdateQMS_ST_SYSTEM_CONFIG(QMSDBEntities db, CreateQMS_ST_SYSTEM_CONFIG model)
        {
            long result = 0;

            try
            {
                QMS_ST_SYSTEM_CONFIG dt = new QMS_ST_SYSTEM_CONFIG();
                dt = QMS_ST_SYSTEM_CONFIG.GetById(db, model.ID);

                dt.ID = model.ID;
                dt.ABNORMAL_ALPHABET = model.ABNORMAL_ALPHABET;
                dt.ABNORMAL_MAX_REPEAT = model.ABNORMAL_MAX_REPEAT;
                dt.ABNORMAL_OVERSHOOT = model.ABNORMAL_OVERSHOOT;

                dt.LAKE_SERVER = model.LAKE_SERVER;
                dt.LAKE_DATABASE = model.LAKE_DATABASE;
                dt.EXA_SCHEDULE = model.EXA_SCHEDULE;
                dt.LAKE_USER = model.LAKE_USER;
                dt.LAKE_PASSWORD = model.LAKE_PASSWORD;

                dt.HIGHLIGHT_1 = model.HIGHLIGHT_1;
                dt.HIGHLIGHT_2 = model.HIGHLIGHT_2;
                dt.HIGHLIGHT_3 = model.HIGHLIGHT_3;
                dt.HIGHLIGHT_4 = model.HIGHLIGHT_4;

                dt.HOME_BACKGROUND = (null == model.HOME_BACKGROUND) ? "" : model.HOME_BACKGROUND;
                dt.PRODUCT_CSC_PATH = (null == model.PRODUCT_CSC_PATH) ? "" : model.PRODUCT_CSC_PATH;
                dt.PRODUCT_CSC_NAME = (null == model.PRODUCT_CSC_NAME) ? "" : model.PRODUCT_CSC_NAME;

                dt.LIMS_PASSWORD = (null == model.LIMS_PASSWORD) ? "" : model.LIMS_PASSWORD;
                dt.LIMS_PORT = model.LIMS_PORT;
                dt.LIMS_SCHEDULE = model.LIMS_SCHEDULE;
                dt.LIMS_SERVER = (null == model.LIMS_SERVER) ? "" : model.LIMS_SERVER;
                dt.LIMS_SID = (null == model.LIMS_SID) ? "" : model.LIMS_SID;
                dt.LIMS_USER = (null == model.LIMS_USER) ? "" : model.LIMS_USER;

                dt.LOWER_BOUNDARY = model.LOWER_BOUNDARY;
                dt.UPPER_BOUNDARY = model.UPPER_BOUNDARY;

                dt.TEMPLATE_SCHEDULE = model.TEMPLATE_SCHEDULE;

                dt.REPORT_OFF_CONTROL = model.REPORT_OFF_CONTROL;
                dt.REPORT_OFF_SPEC = model.REPORT_OFF_SPEC;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_ST_SYSTEM_CONFIG(QMSDBEntities _db, QMS_ST_SYSTEM_CONFIG model)
        {
            long result = 0;
            try
            {
                QMS_ST_SYSTEM_CONFIG dt = new QMS_ST_SYSTEM_CONFIG();
                dt = QMS_ST_SYSTEM_CONFIG.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_ST_SYSTEM_CONFIG.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long SaveExaConnectSetting(QMSDBEntities db, CreateQMS_ST_EXA_CONNECT model)
        {
            long result = 0;

            try
            {
                CreateQMS_ST_SYSTEM_CONFIG dt = this.getFirstOrDefaultConfig(db);
                dt.LAKE_SERVER = (model.LAKE_SERVER != null) ? model.LAKE_SERVER : "";
                dt.LAKE_DATABASE = (model.LAKE_DATABASE != null) ? model.LAKE_DATABASE : "";
                dt.LAKE_USER = (model.LAKE_USER != null) ? model.LAKE_USER : "";
                dt.LAKE_PASSWORD = (model.LAKE_PASSWORD != null) ? model.LAKE_PASSWORD : "";
                if (null != model.EXA_SCHEDULE)
                {
                    dt.EXA_SCHEDULE = new DateTime(2017, 1, 1, model.EXA_SCHEDULE.Value.Hour, model.EXA_SCHEDULE.Value.Minute, 0);
                }
                else
                {
                    dt.EXA_SCHEDULE = new DateTime(2017, 1, 1, 0, 0, 0);
                }
                result = UpdateQMS_ST_SYSTEM_CONFIG(db, dt);
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_EXA_CONNNECT);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveLIMSConnectSetting(QMSDBEntities db, CreateQMS_ST_LIMS_CONNECT model)
        {
            long result = 0;

            try
            {
                CreateQMS_ST_SYSTEM_CONFIG dt = this.getFirstOrDefaultConfig(db);

                dt.LIMS_SERVER = (model.LIMS_SERVER != null) ? model.LIMS_SERVER : "";
                dt.LIMS_SID = (model.LIMS_SID != null) ? model.LIMS_SID : "";
                dt.LIMS_USER = (model.LIMS_USER != null) ? model.LIMS_USER : "";
                dt.LIMS_PASSWORD = (model.LIMS_PASSWORD != null) ? model.LIMS_PASSWORD : "";
                dt.LIMS_PORT = model.LIMS_PORT;

                if (null != model.LIMS_SCHEDULE)
                {
                    dt.LIMS_SCHEDULE = new DateTime(2017, 1, 1, model.LIMS_SCHEDULE.Value.Hour, model.LIMS_SCHEDULE.Value.Minute, 0);

                }
                else
                {
                    dt.LIMS_SCHEDULE = new DateTime(2017, 1, 1, 0, 0, 0);
                }

                dt.TEMPLATE_SCHEDULE = dt.LIMS_SCHEDULE; // set same value; 
                result = UpdateQMS_ST_SYSTEM_CONFIG(db, dt);
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_LIMS_CONNECT);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveTemplateConnectSetting(QMSDBEntities db, CreateQMS_ST_TEMPLATE_CONNECT model)
        {
            long result = 0;

            try
            {
                CreateQMS_ST_SYSTEM_CONFIG dt = this.getFirstOrDefaultConfig(db);

                if (null != model.TEMPLATE_SCHEDULE)
                {
                    dt.TEMPLATE_SCHEDULE = new DateTime(2017, 1, 1, model.TEMPLATE_SCHEDULE.Value.Hour, model.TEMPLATE_SCHEDULE.Value.Minute, 0);

                }
                else
                {
                    dt.TEMPLATE_SCHEDULE = new DateTime(2017, 1, 1, 0, 0, 0);
                }

                dt.LIMS_SCHEDULE = dt.TEMPLATE_SCHEDULE; // set same value;
                result = UpdateQMS_ST_SYSTEM_CONFIG(db, dt);
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_TEMP_CONNECT);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveAbnormalSetting(QMSDBEntities db, CreateAbnormalSetting model)
        {
            long result = 0;

            try
            {
                CreateQMS_ST_SYSTEM_CONFIG dt = this.getFirstOrDefaultConfig(db);

                dt.ABNORMAL_MAX_REPEAT = model.ABNORMAL_MAX_REPEAT;
                dt.ABNORMAL_OVERSHOOT = model.ABNORMAL_OVERSHOOT;
                dt.ABNORMAL_ALPHABET = model.ABNORMAL_ALPHABET1 + "," + model.ABNORMAL_ALPHABET2;

                result = UpdateQMS_ST_SYSTEM_CONFIG(db, dt);
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_Abnormal);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveProductQualityCSCSetting(QMSDBEntities db, CreateAbnormalSetting model)
        {
            long result = 0;

            try
            {
                CreateQMS_ST_SYSTEM_CONFIG dt = this.getFirstOrDefaultConfig(db);

                dt.ABNORMAL_MAX_REPEAT = model.ABNORMAL_MAX_REPEAT;
                dt.ABNORMAL_OVERSHOOT = model.ABNORMAL_OVERSHOOT;
                dt.ABNORMAL_ALPHABET = model.ABNORMAL_ALPHABET1 + "," + model.ABNORMAL_ALPHABET2;

                result = UpdateQMS_ST_SYSTEM_CONFIG(db, dt);

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveProductCSCPathy(QMSDBEntities db, CreatePRODUCT_CSC_PATH model)
        {
            long result = 0;

            try
            {
                CreateQMS_ST_SYSTEM_CONFIG dt = this.getFirstOrDefaultConfig(db);

                dt.PRODUCT_CSC_PATH = model.PRODUCT_CSC_PATH;
                dt.PRODUCT_CSC_NAME = model.PRODUCT_CSC_NAME;

                result = UpdateQMS_ST_SYSTEM_CONFIG(db, dt);
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_ProductCSCPath);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public long SaveExportDataBoundary(QMSDBEntities db, CreateExportDataBoundary model)
        {
            long result = 0;

            try
            {
                CreateQMS_ST_SYSTEM_CONFIG dt = this.getFirstOrDefaultConfig(db);

                dt.UPPER_BOUNDARY = model.UPPER_BOUNDARY;
                dt.LOWER_BOUNDARY = model.LOWER_BOUNDARY;

                result = UpdateQMS_ST_SYSTEM_CONFIG(db, dt);
                this.SaveStampUser(db, (int)ST_CONFIG_STAMP.S_ExportData);
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public CreateQMS_ST_SYSTEM_CONFIG getFirstOrDefaultConfig(QMSDBEntities _db)
        {
            CreateQMS_ST_SYSTEM_CONFIG result = new CreateQMS_ST_SYSTEM_CONFIG();
            try
            {
                List<CreateQMS_ST_SYSTEM_CONFIG> listData = this.getQMS_ST_SYSTEM_CONFIGList(_db);

                if (listData.Count() > 0)
                    result = listData.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public string getHOME_BACKGROUND(QMSDBEntities _db)
        {
            string result = "";
            try
            {
                CreateQMS_ST_SYSTEM_CONFIG listData = this.getFirstOrDefaultConfig(_db);
                result = (null != listData.HOME_BACKGROUND) ? listData.HOME_BACKGROUND : "";

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public CreateQMS_ST_EXA_CONNECT getEXA_SCHEDULE(QMSDBEntities _db)
        {
            CreateQMS_ST_EXA_CONNECT result = new CreateQMS_ST_EXA_CONNECT();
            try
            {
                CreateQMS_ST_SYSTEM_CONFIG listData = this.getFirstOrDefaultConfig(_db);

                result.LAKE_SERVER = listData.LAKE_SERVER;
                result.LAKE_DATABASE = listData.LAKE_DATABASE;
                result.EXA_SCHEDULE = listData.EXA_SCHEDULE;
                result.LAKE_USER = listData.LAKE_USER;
                result.LAKE_PASSWORD = listData.LAKE_PASSWORD;

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public CreateQMS_ST_TEMPLATE_CONNECT getTEMP_SCHEDULE(QMSDBEntities _db)
        {
            CreateQMS_ST_TEMPLATE_CONNECT result = new CreateQMS_ST_TEMPLATE_CONNECT();
            try
            {
                CreateQMS_ST_SYSTEM_CONFIG listData = this.getFirstOrDefaultConfig(_db);

                result.TEMPLATE_SCHEDULE = listData.TEMPLATE_SCHEDULE;

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public CreateQMS_ST_LIMS_CONNECT getLIMS_CONNECTION(QMSDBEntities _db)
        {
            CreateQMS_ST_LIMS_CONNECT result = new CreateQMS_ST_LIMS_CONNECT();
            try
            {
                CreateQMS_ST_SYSTEM_CONFIG listData = this.getFirstOrDefaultConfig(_db);

                result.LIMS_SERVER = listData.LIMS_SERVER;
                result.LIMS_SID = listData.LIMS_SID;
                result.LIMS_PORT = listData.LIMS_PORT;
                result.LIMS_USER = listData.LIMS_USER;
                result.LIMS_PASSWORD = listData.LIMS_PASSWORD;
                result.LIMS_SCHEDULE = listData.LIMS_SCHEDULE;

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public CreatePRODUCT_CSC_PATH getProductCSCPath(QMSDBEntities _db)
        {
            CreatePRODUCT_CSC_PATH result = new CreatePRODUCT_CSC_PATH();
            try
            {
                CreateQMS_ST_SYSTEM_CONFIG listData = this.getFirstOrDefaultConfig(_db);
                result.PRODUCT_CSC_PATH = (null != listData.PRODUCT_CSC_PATH) ? listData.PRODUCT_CSC_PATH : "";
                result.PRODUCT_CSC_NAME = (null != listData.PRODUCT_CSC_NAME) ? listData.PRODUCT_CSC_NAME : "";

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public CreateAbnormalSetting getCreateAbnormalSetting(QMSDBEntities _db)
        {
            CreateAbnormalSetting result = new CreateAbnormalSetting();
            try
            {
                CreateQMS_ST_SYSTEM_CONFIG listData = this.getFirstOrDefaultConfig(_db);

                if (null != listData)
                {
                    string[] separators = { "," };
                    result.ABNORMAL_MAX_REPEAT = listData.ABNORMAL_MAX_REPEAT;
                    result.ABNORMAL_OVERSHOOT = listData.ABNORMAL_OVERSHOOT;

                    if (listData.ABNORMAL_ALPHABET != "")
                    {
                        if (listData.ABNORMAL_ALPHABET.IndexOf(",") != -1)
                        {
                            string[] words = listData.ABNORMAL_ALPHABET.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                            result.ABNORMAL_ALPHABET1 = WebUtility.HtmlDecode(words[0]);
                            result.ABNORMAL_ALPHABET2 = WebUtility.HtmlDecode(words[1]);
                        }
                        else
                        {
                            result.ABNORMAL_ALPHABET1 = WebUtility.HtmlDecode(listData.ABNORMAL_ALPHABET);
                            result.ABNORMAL_ALPHABET2 = "";
                        }
                    }
                    else
                    {
                        result.ABNORMAL_ALPHABET1 = "";
                        result.ABNORMAL_ALPHABET2 = "";
                    }

                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }


        public CreateExportDataBoundary getCreateExportDataBoundary(QMSDBEntities _db)
        {
            CreateExportDataBoundary result = new CreateExportDataBoundary();
            try
            {
                CreateQMS_ST_SYSTEM_CONFIG listData = this.getFirstOrDefaultConfig(_db);

                if (null != listData)
                {
                    result.UPPER_BOUNDARY = listData.UPPER_BOUNDARY;
                    result.LOWER_BOUNDARY = listData.LOWER_BOUNDARY;
                }
                else
                {
                    result.UPPER_BOUNDARY = 0;
                    result.LOWER_BOUNDARY = 0;
                }

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return result;
        }

        public List<ViewQMS_ST_SYSTEM_CONFIG> getAllQMS_ST_SYSTEM_CONFIG(QMSDBEntities db)
        {
            List<ViewQMS_ST_SYSTEM_CONFIG> objResult = new List<ViewQMS_ST_SYSTEM_CONFIG>();
            try
            {
                List<QMS_ST_SYSTEM_CONFIG> listData = new List<QMS_ST_SYSTEM_CONFIG>();
                listData = QMS_ST_SYSTEM_CONFIG.GetAll(db);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_ST_SYSTEM_CONFIG
                                 {
                                     ID = dt.ID,
                                     REPORT_OFF_CONTROL = dt.REPORT_OFF_CONTROL,
                                     REPORT_OFF_SPEC = dt.REPORT_OFF_SPEC

                                 }).ToList();

                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        #endregion

        #region QMS_ST_SYSTEM_LOG       
        public List<ViewQMS_ST_SYSTEM_LOG> searchQMS_ST_SYSTEM_LOG(QMSDBEntities db, SystemLogSearch searchModel, out long count, out long totalPage, out List<PageIndexList> listPageIndex)
        {
            List<ViewQMS_ST_SYSTEM_LOG> objResult = new List<ViewQMS_ST_SYSTEM_LOG>();

            count = 0;
            totalPage = 0;
            listPageIndex = new List<PageIndexList>();

            try
            {
                GridSettings grid = new GridSettings(searchModel.SortColumn, true);
                grid.PageIndex = searchModel.PageIndex;
                grid.PageSize = searchModel.PageSize;
                grid.SortOrder = searchModel.SortOrder;
                if ((searchModel.SortColumn == "") || (searchModel.SortColumn == null))
                {
                    grid.SortColumn = "ID";
                }
                else
                {
                    grid.SortColumn = searchModel.SortColumn;
                }

                List<QMS_ST_SYSTEM_LOG> listData = new List<QMS_ST_SYSTEM_LOG>();
                listData = QMS_ST_SYSTEM_LOG.GetAllBySearch(db, searchModel.mSearch);

                if (listData != null)
                {
                    objResult = (from dt in listData
                                 select new ViewQMS_ST_SYSTEM_LOG
                                 {
                                     ID = dt.ID,
                                     PROCESS_NAME = dt.PROCESS_NAME,
                                     PROCESS_DESC = dt.PROCESS_DESC,
                                     LOG_DATE = dt.LOG_DATE,
                                     ERROR_LEVEL = dt.ERROR_LEVEL

                                 }).ToList();
                    objResult = grid.LoadGridData<ViewQMS_ST_SYSTEM_LOG>(objResult.AsQueryable(), out count, out totalPage).ToList();

                    listPageIndex = getPageIndexList(totalPage);
                }


            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }
        public CreateQMS_ST_SYSTEM_LOG getQMS_ST_SYSTEM_LOGById(QMSDBEntities db, long id)
        {
            CreateQMS_ST_SYSTEM_LOG objResult = new CreateQMS_ST_SYSTEM_LOG();

            try
            {
                QMS_ST_SYSTEM_LOG dt = new QMS_ST_SYSTEM_LOG();
                dt = QMS_ST_SYSTEM_LOG.GetById(db, id);

                objResult = convertDBToModel(dt);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return objResult;
        }

        private CreateQMS_ST_SYSTEM_LOG convertDBToModel(QMS_ST_SYSTEM_LOG model)
        {
            CreateQMS_ST_SYSTEM_LOG result = new CreateQMS_ST_SYSTEM_LOG();

            result.ID = model.ID;

            result.LOG_DATE = model.LOG_DATE;
            result.ERROR_LEVEL = model.ERROR_LEVEL;

            result.PROCESS_NAME = (null == model.PROCESS_NAME) ? "" : model.PROCESS_NAME;
            result.PROCESS_DESC = (null == model.PROCESS_DESC) ? "" : model.PROCESS_DESC;



            return result;
        }

        public List<CreateQMS_ST_SYSTEM_LOG> getQMS_ST_SYSTEM_LOGList(QMSDBEntities db)
        {
            List<CreateQMS_ST_SYSTEM_LOG> listResult = new List<CreateQMS_ST_SYSTEM_LOG>();

            try
            {
                List<QMS_ST_SYSTEM_LOG> list = QMS_ST_SYSTEM_LOG.GetAll(db);

                if (null != list && list.Count() > 0)
                {
                    listResult = (from dt in list
                                  select new CreateQMS_ST_SYSTEM_LOG
                                  {
                                      ID = dt.ID,

                                      PROCESS_NAME = dt.PROCESS_NAME,
                                      PROCESS_DESC = dt.PROCESS_DESC,
                                      LOG_DATE = dt.LOG_DATE,
                                      ERROR_LEVEL = dt.ERROR_LEVEL

                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return listResult;
        }

        private QMS_ST_SYSTEM_LOG convertModelToDB(CreateQMS_ST_SYSTEM_LOG model)
        {
            QMS_ST_SYSTEM_LOG result = new QMS_ST_SYSTEM_LOG();

            result.ID = model.ID;
            result.LOG_DATE = model.LOG_DATE;
            result.ERROR_LEVEL = model.ERROR_LEVEL;

            result.PROCESS_NAME = (null == model.PROCESS_NAME) ? "" : model.PROCESS_NAME;
            result.PROCESS_DESC = (null == model.PROCESS_DESC) ? "" : model.PROCESS_DESC;

            return result;
        }

        public bool DeleteQMS_ST_SYSTEM_LOGById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_ST_SYSTEM_LOG result = new QMS_ST_SYSTEM_LOG();
            result = QMS_ST_SYSTEM_LOG.GetById(db, id);
            try
            {
                if (null != result)
                {
                    db.QMS_ST_SYSTEM_LOG.Remove(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_ST_SYSTEM_LOGByListId(QMSDBEntities db, long[] ids)
        {
            bool bResult = false;
            List<QMS_ST_SYSTEM_LOG> result = new List<QMS_ST_SYSTEM_LOG>();
            result = QMS_ST_SYSTEM_LOG.GetByListId(db, ids);
            try
            {
                DateTime dtCurrent = DateTime.Now;
                if (null != result)
                {
                    //result.ForEach(db.DeleteObject); 
                    db.QMS_ST_SYSTEM_LOG.RemoveRange(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_ST_SYSTEM_LOG(QMSDBEntities db, CreateQMS_ST_SYSTEM_LOG model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_ST_SYSTEM_LOG(db, model);
            }
            else
            {
                result = AddQMS_ST_SYSTEM_LOG(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_ST_SYSTEM_LOG(QMSDBEntities db, CreateQMS_ST_SYSTEM_LOG model)
        {
            long result = 0;

            try
            {
                QMS_ST_SYSTEM_LOG dt = new QMS_ST_SYSTEM_LOG();
                dt = QMS_ST_SYSTEM_LOG.GetById(db, model.ID);

                dt.ID = model.ID;
                dt.LOG_DATE = model.LOG_DATE;
                dt.ERROR_LEVEL = model.ERROR_LEVEL;

                dt.PROCESS_NAME = (null == model.PROCESS_NAME) ? "" : model.PROCESS_NAME;
                dt.PROCESS_DESC = (null == model.PROCESS_DESC) ? "" : model.PROCESS_DESC;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_ST_SYSTEM_LOG(QMSDBEntities _db, QMS_ST_SYSTEM_LOG model)
        {
            long result = 0;
            try
            {
                QMS_ST_SYSTEM_LOG dt = new QMS_ST_SYSTEM_LOG();
                dt = QMS_ST_SYSTEM_LOG.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_ST_SYSTEM_LOG.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion




        #region QMS_ST_SYSTEM_USER_STAMP 

        private CreateQMS_ST_SYSTEM_USER_STAMP convertDBToModel(QMSDBEntities db, QMS_ST_SYSTEM_USER_STAMP model)
        {
            CreateQMS_ST_SYSTEM_USER_STAMP result = new CreateQMS_ST_SYSTEM_USER_STAMP();

            result.ID = model.ID;
            result.SYSTEM_TYPE = model.SYSTEM_TYPE;

            result.CREATE_USER = model.CREATE_USER;
            result.UPDATE_USER = model.UPDATE_USER;
            result.SHOW_UPDATE_DATE = model.UPDATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));
            result.SHOW_CREATE_DATE = model.CREATE_DATE.ToString("dd-MM-yyyy HH:mm:ss", new CultureInfo("en-US"));

            return result;
        }


        private QMS_ST_SYSTEM_USER_STAMP convertModelToDB(CreateQMS_ST_SYSTEM_USER_STAMP model)
        {
            QMS_ST_SYSTEM_USER_STAMP result = new QMS_ST_SYSTEM_USER_STAMP();

            result.ID = model.ID;
            result.SYSTEM_TYPE = model.SYSTEM_TYPE;

            result.CREATE_DATE = DateTime.Now;
            result.CREATE_USER = _currentUserName;
            result.UPDATE_DATE = DateTime.Now;
            result.UPDATE_USER = _currentUserName;

            return result;
        }

        public CreateQMS_ST_SYSTEM_USER_STAMP getQMS_ST_SYSTEM_USER_STAMPBySystemType(QMSDBEntities db, int SYSTEM_TYPE)
        {
            CreateQMS_ST_SYSTEM_USER_STAMP result = new CreateQMS_ST_SYSTEM_USER_STAMP();
            try
            {
                QMS_ST_SYSTEM_USER_STAMP tempResult = QMS_ST_SYSTEM_USER_STAMP.GetBySystemType(db, SYSTEM_TYPE);
                result = convertDBToModel(db, tempResult);
            }
            catch (Exception ex)
            {
                this.writeErrorLog(ex.Message);
            }

            return result;
        }

        public long SaveStampUser(QMSDBEntities db, int SYSTEM_TYPE)
        {
            long bResult = 0;
            //int SYSTEM_TYPE = (int)ST_CONFIG_STAMP.S_Home;
            try
            {
                CreateQMS_ST_SYSTEM_USER_STAMP userStamp = this.getQMS_ST_SYSTEM_USER_STAMPBySystemType(db, SYSTEM_TYPE);

                if (null != userStamp && userStamp.ID > 0)
                {
                    //have old value;
                }
                else
                {
                    userStamp = new CreateQMS_ST_SYSTEM_USER_STAMP();
                    userStamp.ID = 0;
                    userStamp.SYSTEM_TYPE = SYSTEM_TYPE;
                }

                bResult = SaveQMS_ST_SYSTEM_USER_STAMP(db, userStamp);

            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public bool DeleteQMS_ST_SYSTEM_USER_STAMPById(QMSDBEntities db, long id)
        {
            bool bResult = false;
            QMS_ST_SYSTEM_USER_STAMP result = new QMS_ST_SYSTEM_USER_STAMP();
            result = QMS_ST_SYSTEM_USER_STAMP.GetById(db, id);
            try
            {
                if (null != result)
                {
                    db.QMS_ST_SYSTEM_USER_STAMP.Remove(result);
                    db.SaveChanges();
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }

            return bResult;
        }

        public long SaveQMS_ST_SYSTEM_USER_STAMP(QMSDBEntities db, CreateQMS_ST_SYSTEM_USER_STAMP model)
        {
            long result = 0;

            if (model.ID > 0)
            {
                result = UpdateQMS_ST_SYSTEM_USER_STAMP(db, model);
            }
            else
            {
                result = AddQMS_ST_SYSTEM_USER_STAMP(db, convertModelToDB(model));
            }

            return result;
        }

        public long UpdateQMS_ST_SYSTEM_USER_STAMP(QMSDBEntities db, CreateQMS_ST_SYSTEM_USER_STAMP model)
        {
            long result = 0;

            try
            {
                QMS_ST_SYSTEM_USER_STAMP dt = new QMS_ST_SYSTEM_USER_STAMP();
                dt = QMS_ST_SYSTEM_USER_STAMP.GetById(db, model.ID);

                dt.ID = model.ID;

                dt.UPDATE_DATE = DateTime.Now;
                dt.UPDATE_USER = _currentUserName;

                db.SaveChanges();
                result = model.ID;
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }

        public long AddQMS_ST_SYSTEM_USER_STAMP(QMSDBEntities _db, QMS_ST_SYSTEM_USER_STAMP model)
        {
            long result = 0;
            try
            {
                QMS_ST_SYSTEM_USER_STAMP dt = new QMS_ST_SYSTEM_USER_STAMP();
                dt = QMS_ST_SYSTEM_USER_STAMP.GetById(_db, model.ID);

                if (null == dt)
                {
                    //model.bank_code = this.getBankCode(_db, model);
                    _db.QMS_ST_SYSTEM_USER_STAMP.Add(model);
                    _db.SaveChanges();
                    result = model.ID;
                }
                else
                {
                    _errMsg = QMSSystem.CoreDB.Resource.ResourceString.AlreadyHaveData;
                }
            }
            catch (Exception ex)
            {
                _errMsg = this.getEexceptionError(ex);
            }
            return result;
        }
        #endregion

    }
}
