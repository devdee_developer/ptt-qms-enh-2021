﻿Imports System.ServiceProcess

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DevDeeService
    Inherits System.ServiceProcess.ServiceBase

    'UserService overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    <System.Diagnostics.DebuggerNonUserCode()> _
    Shared Sub Main(args As String())
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase
        'System.Diagnostics.Debugger.Launch()
        Try
            If args.Length > 0 Then
                If System.Environment.UserInteractive Then
                    If args.Count() = 1 Then
                        If args(0) = "-install" Then
                            ' ManagedInstallerClass.InstallHelper(New String() {Assembly.GetExecutingAssembly().Location})
                            Service.InstallService()
                        End If

                        If args(0) = "-uninstall" Then
                            'ManagedInstallerClass.InstallHelper(New String() {"/u", Assembly.GetExecutingAssembly().Location})
                            Service.UninstallService()
                        End If
                    End If
                    Exit Sub
                End If
            Else
                ' More than one NT Service may run within the same process. To add
                ' another service to this process, change the following line to
                ' create a second service object. For example,
                '
                '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
                '
                ServicesToRun = New System.ServiceProcess.ServiceBase() {New DevDeeService}

                System.ServiceProcess.ServiceBase.Run(ServicesToRun)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "peServiceTest Service Install")
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        '
        'DevDeeService
        '
        Me.ServiceName = "DevDeeService"

    End Sub

End Class
