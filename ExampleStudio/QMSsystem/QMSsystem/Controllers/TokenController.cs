﻿using QMSSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QMSsystem.Controllers
{
    public class TokenController : BaseController
    {
        //
        // GET: /Token/
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetToken(string username, string password)
        {
            string json = "{\"name\":\"Joe\"}";
            TokenMod responseResult = new TokenMod();
            if (username == "qmsauth" && password == "hyrC8sjZ@9sdpp$")
            {
                responseResult.code = 200;
                responseResult.status = true;
                responseResult.message = "success";
                responseResult.access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJzdWIiOiJoeXJDOHNqWkA5c2RwcCQiLCJuYW1lIjoicW1zYXV0aCIsImlhdCI6MjAyMzA5MjR9eDThsbIwMqQQRjQ1UsZEbX1GbPA2YN5iEr4CL3EWkU";
                responseResult.token_type = "Bearer";
                responseResult.expires_in = "3600";
            }
            else
            {
                responseResult.code = 401;
                responseResult.status = false;
                responseResult.message = "Unauthorized";
            }
            return Json(responseResult, JsonRequestBehavior.AllowGet);
        }

    }
}
