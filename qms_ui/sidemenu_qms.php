<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>LAB Dept.</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="./index.php">Dashboard</a></li>
          <!-- <li><a href="index2.html">Dashboard2</a></li>
          <li><a href="index3.html">Dashboard3</a></li> -->
        </ul>
      </li>
      <li><a><i class="fa fa-bar-chart"></i> Off Control & Spec <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="offcontrol_report.php">Off Control Report</a></li>
          <li><a href="offspec_report.php">Off Spec Report</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-line-chart"></i> Trend Data <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="trend_highlight.php">Trend Highlight</a></li>
          <li><a href="trend_gas.php">Trend GAS</a></li>
          <li><a href="trend_utility.php">Trend Utility</a></li>
          <li><a href="trend_product.php">Trend Product</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-sitemap"></i> Master Data <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
        <li>

            <li>
              <a>Off Control<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="md_oc_plant_and_product.php">Plant & Propduct Mapping</a>
                </li>
                <li class="sub_menu"><a href="md_oc_reduce_feed.php">Reducr Feed</a>
                </li>
                <li class="sub_menu"><a href="md_oc_downtime.php">Downtime</a>
                </li>
                <li class="sub_menu"><a href="md_oc_correct_data.php">Correct Data</a>
                </li>
                <li class="sub_menu"><a href="md_oc_root_cause.php">Root Cause</a>
                </li>
                <li class="sub_menu"><a href="md_oc_kpi.php">KPI</a>
                </li>
                <li class="sub_menu"><a href="md_oc_operation_shift.php">Operation Shift</a>
                </li>
                <li class="sub_menu"><a href="md_oc_control_value_gas.php">Control Value of GAS</a>
                </li>
                <li class="sub_menu"><a href="md_oc_spec_value.php">Spec Value</a>
                </li><!-- <li><a href="#level2_1">Level Two</a>
                </li>
                <li><a href="#level2_2">Level Two</a>
                </li> -->
              </ul>
            </li>
            <li>
              <a>Trend Data<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="md_td_control_value_utility.php">Control Value of Utility</a>
                </li>
                <li class="sub_menu"><a href="md_td_control_value_product.php">Control Value of Product</a>
                </li>
                <li class="sub_menu"><a href="md_td_template_gas.php">Template GAS</a>
                </li>
                <li class="sub_menu"><a href="md_td_template_utility.php">Template Utility</a>
                </li>
              </ul>
            </li>
            <li><a href="md_email_list.php">Email List</a>
            </li>
            <li><a href="md_unit.php">Unit</a>
            </li>
        </ul>
      </li>
      <li><a><i class="fa fa-pencil"></i> Manage Data <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
        <li>

            <li>
              <a>Off Control<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="mn_oc_raw_data_export.php">Raw Data Export</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_cal.php">Off Control calculate</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_spec_cal.php">Off Spec calculate</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_turn_around.php">Turn Around</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_downtime.php">Downtime</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_change_grade.php">Change Grade</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_cross_volume.php">Cross Volume</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_abnormal.php">Abnormal</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_reduce_feed.php">Reduce Feed</a>
                </li>
                <li class="sub_menu"><a href="mn_oc_exception_case.php">Exception Case</a>
                </li>
              </ul>
            </li>
            <li>
              <a>Trend Data<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="mn_td_template_gas.php">Template GAS Data</a>
                </li>
                <li class="sub_menu"><a href="mn_td_template_utility.php">Template Utility Data</a>
                </li>
                <li class="sub_menu"><a href="mn_td_template_coa.php">Template COA</a>
                </li>
              </ul>
            </li>

        </ul>
      </li>
      <li><a><i class="fa fa-desktop"></i> Report <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
        <li>

            <li>
              <a>Off Control<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="report_oc_offcontrol_summary.php">Off Control Summary</a>
                </li>
                <li class="sub_menu"><a href="report_oc_offspec_summary.php">Off Spec Summary</a>
                </li>
                <li class="sub_menu"><a href="report_oc_offcontrol_correct_data.php">Off Control Correct Data</a>
                </li>
                <li class="sub_menu"><a href="report_oc_offcontrol_operation_shift.php">Off Control Shift Analysis</a>
                </li>

              </ul>
            </li>
            <li>
              <a>Trend Data<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="report_trend_gas.php">Trend GAS</a>
                </li>
                <li class="sub_menu"><a href="report_trend_utility.php">Trend Utility</a>
                </li>
                <li class="sub_menu"><a href="report_trend_product.php">Trend Product</a>
                </li>
              </ul>
            </li>

        </ul>
      </li>
      <li><a><i class="fa fa-gear"></i> Setting <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
        <li>
          <li class="sub_menu"><a href="setting_home_background.php">Home Background</a>
          </li>
            <li>
              <a>Off Control<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="setting_oc_abnormal.php">Abnormal</a>
                </li>
                <!-- <li class="sub_menu"><a href="setting_oc_control_spec_value.php">Control/Spec Value</a>
                </li> -->
                <li class="sub_menu"><a href="setting_oc_export_data_boundary.php">Export Data Boundary</a>
                </li>
                <li class="sub_menu"><a href="setting_oc_export_data_prefix.php">Export Data Prefix</a>
                </li>
                <li class="sub_menu"><a href="setting_oc_report.php">Report Off Control/Spec</a>
                </li>
                <li class="sub_menu"><a href="setting_oc_file_attach.php">File Attach</a>
                </li>
                <li class="sub_menu"><a href="setting_oc_exaquantum.php">Exaquantum Schedule</a>
                </li>
              </ul>
            </li>
            <li>
              <a>Trend Data<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="setting_td_highlight.php">Trend Highlight</a>
                </li>

              </ul>
            </li>
            <li class="sub_menu"><a href="setting_system_connect.php">System Connect</a>
            </li>
            <li class="sub_menu"><a href="setting_authen.php">Authorization</a>
            </li>
            <li class="sub_menu"><a href="setting_system_log.php">System Log</a>
            </li>
        </ul>
      </li>
      <!-- <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="form.html">General Form</a></li>
          <li><a href="form_advanced.html">Advanced Components</a></li>
          <li><a href="form_validation.html">Form Validation</a></li>
          <li><a href="form_wizards.html">Form Wizard</a></li>
          <li><a href="form_upload.html">Form Upload</a></li>
          <li><a href="form_buttons.html">Form Buttons</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="general_elements.html">General Elements</a></li>
          <li><a href="media_gallery.html">Media Gallery</a></li>
          <li><a href="typography.html">Typography</a></li>
          <li><a href="icons.html">Icons</a></li>
          <li><a href="glyphicons.html">Glyphicons</a></li>
          <li><a href="widgets.html">Widgets</a></li>
          <li><a href="invoice.html">Invoice</a></li>
          <li><a href="inbox.html">Inbox</a></li>
          <li><a href="calendar.html">Calendar</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="tables.html">Tables</a></li>
          <li><a href="tables_dynamic.html">Table Dynamic</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="chartjs.html">Chart JS</a></li>
          <li><a href="chartjs2.html">Chart JS2</a></li>
          <li><a href="morisjs.html">Moris JS</a></li>
          <li><a href="echarts.html">ECharts</a></li>
          <li><a href="other_charts.html">Other Charts</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
          <li><a href="fixed_footer.html">Fixed Footer</a></li>
        </ul>
      </li> -->
    </ul>
  </div>
  <!-- <div class="menu_section">
    <h3>Live On</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="e_commerce.html">E-commerce</a></li>
          <li><a href="projects.html">Projects</a></li>
          <li><a href="project_detail.html">Project Detail</a></li>
          <li><a href="contacts.html">Contacts</a></li>
          <li><a href="profile.html">Profile</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="page_403.html">403 Error</a></li>
          <li><a href="page_404.html">404 Error</a></li>
          <li><a href="page_500.html">500 Error</a></li>
          <li><a href="plain_page.html">Plain Page</a></li>
          <li><a href="login.html">Login Page</a></li>
          <li><a href="pricing_tables.html">Pricing Tables</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="#level1_1">Level One</a>
            <li><a>Level One<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="level2.html">Level Two</a>
                </li>
                <li><a href="#level2_1">Level Two</a>
                </li>
                <li><a href="#level2_2">Level Two</a>
                </li>
              </ul>
            </li>
            <li><a href="#level1_2">Level One</a>
            </li>
        </ul>
      </li>
      <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
    </ul>
  </div> -->

</div>
