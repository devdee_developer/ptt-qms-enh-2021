﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using System.IO;
using QMSSystem.CoreDB.Helper;
using QMSSystem.Model;
using OfficeOpenXml;
using System.Web.Script.Serialization;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{
    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_TrendGAS, PrivilegeModeEnum.D_TrendHighlight ,
        PrivilegeModeEnum.D_TrendProduct, PrivilegeModeEnum.D_TrendUtility, PrivilegeModeEnum.D_ProductCSC, PrivilegeModeEnum.D_TrendWasteWater,
        PrivilegeModeEnum.D_TrendHotOilFlashPoint, PrivilegeModeEnum.D_TrendHotOilPhysical, PrivilegeModeEnum.D_TrendHgInPLMonthly, PrivilegeModeEnum.D_TrendHgInPL, PrivilegeModeEnum.D_TrendHgOutletMRU,
        PrivilegeModeEnum.D_TrendTwoHundredThousandPond, PrivilegeModeEnum.D_TrendEmission, PrivilegeModeEnum.D_TrenddObservePonds, PrivilegeModeEnum.D_TrenddOilyWaters, PrivilegeModeEnum.D_TrendLabControlChart
    }, DataAccess = DataAccessEnum.Authen)]
    public class TrendDataController : BaseController
    {
        //
        // GET: /TrendData/

        public ActionResult TrendGas()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;
            List<TemplateName_COMBO> listTemplate = new List<TemplateName_COMBO>();
            listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.GAS);

            ViewBag.listTemplate = listTemplate;
            //ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }

        public ActionResult ManageLIMS()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday =  DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;
            
            return View();
        }

        public ActionResult ManageCSCProduct()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }

        public ActionResult ManageCOAToDataLake()
        {
            ManageLIMSSearchModel searchModel = new ManageLIMSSearchModel();
            DateTime dtToday = DateTime.Now;
            searchModel.START_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 0, 0, 0).AddDays(-1);
            searchModel.END_DATE = new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, 23, 59, 59);

            ViewBag.searchModel = searchModel;

            return View();
        }
        public JsonResult LoadManageLIMS(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            { 
                try
                {
                    COAServices coaService = new COAServices(_db, _username);
                    DateTime startTime  = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                    //searchModel.bLoadCOA = false;
                    //searchModel.bLoadCSC = true;

                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        if (searchModel.bLoadCOA)
                        {
                            coaService.DailyCOAMasterDataCheck(startTime, endTime);
                        }

                        if (searchModel.bLoadCSC)
                        {
                            coaService.DailyCOAWriteProductQualityToCSC(startTime, endTime, false);
                        }

                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadManageCOAToDataLake(ManageLIMSSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                try
                {
                    COAServices coaService = new COAServices(_db, _username);
                    TransactionService transServices = new TransactionService(_username);
                    DateTime startTime = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    DateTime endTime = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                    //searchModel.bLoadCOA = false;
                    //searchModel.bLoadCSC = true;

                    double diff = (endTime - startTime).TotalDays;

                    if (diff <= 31)
                    {
                        //if (searchModel.bLoadCOA)
                        //{
                        //    coaService.DailyCOAMasterDataCheck(startTime, endTime);
                        //}

                        //if (searchModel.bLoadCSC)
                        //{
                        //    coaService.DailyCOAWriteProductQualityToCSC(startTime, endTime, true);
                        //}
                        transServices.LoadQMS_TR_COA_DATAFromDBTODataLake(_db, startTime, endTime);
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = @Resources.Strings.TotalDayOverLimit };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TrendHighlight()
        {
            ReportServices repServices = new ReportServices(_username);
            ConfigServices ConfigServices = new ConfigServices(_username);
            TransactionService transServices = new TransactionService(_username); 

            UpdateTrendQMS_ST_SYSTEM_CONFIG createModel = new UpdateTrendQMS_ST_SYSTEM_CONFIG();
            CreateQMS_ST_SYSTEM_CONFIG createQMS_ST_SYSTEM_CONFIG = new CreateQMS_ST_SYSTEM_CONFIG();

            //ดึงค่าจาก config 
            List<ViewQMS_ST_SYSTEM_CONFIG> listData = new List<ViewQMS_ST_SYSTEM_CONFIG>();
            listData = ConfigServices.getAllQMS_ST_SYSTEM_CONFIG(_db);
            ViewQMS_ST_SYSTEM_CONFIG dataItem = listData.FirstOrDefault();

            if (null != dataItem)
            {
                createQMS_ST_SYSTEM_CONFIG = ConfigServices.getQMS_ST_SYSTEM_CONFIGById(_db, dataItem.ID);
                createModel.ID = dataItem.ID;
                createModel.HIGHLIGHT_1 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_1;
                createModel.HIGHLIGHT_2 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_2;
                createModel.HIGHLIGHT_3 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_3;
                createModel.HIGHLIGHT_4 = createQMS_ST_SYSTEM_CONFIG.HIGHLIGHT_4;
            }

            //ดึงค่า ตรวจสอบว่ามี ค่าหรือเปล่า

            TrendCOAGraphSearchModel search = new TrendCOAGraphSearchModel();

            //Step 1 check  ว่าเป็น ชนิดไหน 
            CreateQMS_RP_TREND_DATA REPORT_MASTER1 = new CreateQMS_RP_TREND_DATA();
            GRAPH_TREDN_GAS listTrendData1 = new GRAPH_TREDN_GAS();
            CreateQMS_RP_TREND_DATA REPORT_MASTER2 = new CreateQMS_RP_TREND_DATA();
            GRAPH_TREDN_GAS listTrendData2 = new GRAPH_TREDN_GAS();
            CreateQMS_RP_TREND_DATA REPORT_MASTER3 = new CreateQMS_RP_TREND_DATA();
            GRAPH_TREDN_GAS listTrendData3 = new GRAPH_TREDN_GAS();
            CreateQMS_RP_TREND_DATA REPORT_MASTER4 = new CreateQMS_RP_TREND_DATA();
            GRAPH_TREDN_GAS listTrendData4 = new GRAPH_TREDN_GAS();

            if (createModel.HIGHLIGHT_1 > 0)
            {
                REPORT_MASTER1 = repServices.getQMS_RP_TREND_DATAById(_db, createModel.HIGHLIGHT_1);
                listTrendData1 = transServices.getTrendDataById(_db, createModel.HIGHLIGHT_1, (byte)REPORT_MASTER1.TREND_TYPE);
            }


            if (createModel.HIGHLIGHT_2 > 0)
            {
                REPORT_MASTER2 = repServices.getQMS_RP_TREND_DATAById(_db, createModel.HIGHLIGHT_2);
                listTrendData2 = transServices.getTrendDataById(_db, createModel.HIGHLIGHT_2, (byte)REPORT_MASTER2.TREND_TYPE);
            }

            if (createModel.HIGHLIGHT_3 > 0)
            {
                REPORT_MASTER3 = repServices.getQMS_RP_TREND_DATAById(_db, createModel.HIGHLIGHT_3);
                listTrendData3 = transServices.getTrendDataById(_db, createModel.HIGHLIGHT_3, (byte)REPORT_MASTER3.TREND_TYPE);
            } 


            if (createModel.HIGHLIGHT_4 > 0)
            {
                REPORT_MASTER4 = repServices.getQMS_RP_TREND_DATAById(_db, createModel.HIGHLIGHT_4);
                listTrendData4 = transServices.getTrendDataById(_db, createModel.HIGHLIGHT_4, (byte)REPORT_MASTER4.TREND_TYPE);
            } 
            
            ViewBag.REPORT_MASTER1 = REPORT_MASTER1;
            ViewBag.listTrendData1 = listTrendData1;

            ViewBag.REPORT_MASTER2 = REPORT_MASTER2;
            ViewBag.listTrendData2 = listTrendData2;

            ViewBag.REPORT_MASTER3 = REPORT_MASTER3;
            ViewBag.listTrendData3 = listTrendData3;

            ViewBag.REPORT_MASTER4 = REPORT_MASTER4;
            ViewBag.listTrendData4 = listTrendData4;

            return View();
        }

        public ActionResult TrendProduct()
        {
            TransactionService transServices = new TransactionService(_username);
            TrendCOAGraphSearchModel searhTrendData = new TrendCOAGraphSearchModel();

            ViewBag.listProduct = transServices.getProductCOAList(_db); 
            ViewBag.listCustomer = transServices.getCustomerCOAList(_db);
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }

        public ActionResult ProductQualityToCSC()
        {
            TransactionService transServices = new TransactionService(_username);
            ProductQualityToCSCSearchModel searhTrendData = new ProductQualityToCSCSearchModel();
              
            ViewBag.listCustomer = transServices.getCustomerCOAList(_db);
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }

        public ActionResult LabControlChart()
        {
            TransactionService transServices = new TransactionService(_username);
            ProductQualityToCSCSearchModel searhTrendData = new ProductQualityToCSCSearchModel();

            ViewBag.listCustomer = transServices.getCustomerCOAList(_db);
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }


        public ActionResult ProcessControlChart()
        {
            TransactionService transServices = new TransactionService(_username);
            ProductQualityToCSCSearchModel searhTrendData = new ProductQualityToCSCSearchModel();

            ViewBag.listCustomer = transServices.getCustomerCOAList(_db);
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }

        public JsonResult GetProductByCustomer(string customer)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);
                MasterDataService masServices = new MasterDataService(_username);

                try
                {
                    List<PRODUCT_COA> listData = transServices.getProductCOAListByCustomer(_db, customer); 

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerByProduct(string product)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);

                try
                {
                    List<CUSTOMER_COA> listData = transServices.getCustomerCOAListByProduct(_db, product);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemByCustomerAndProduct(string customer, string product)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);

                try
                {
                    List<ITEM_COA> listData = transServices.getItemListByCustomerAndProduct(_db, product, customer);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
         
        public JsonResult GetTankByCustomerAndProduct(string customer, string product)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);

                try
                {
                    List<TANK_COA> listData = transServices.getTankListByCustomerAndProduct(_db, product, customer);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemAndTankByCustomerAndProduct(string customer, string product)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);

                try
                {
                    List<ITEM_COA> listItemData = transServices.getItemListByCustomerAndProduct(_db, product, customer);
                    List<TANK_COA> listTankData = transServices.getTankListByCustomerAndProduct(_db, product, customer);
                    result = new { result = ReturnStatus.SUCCESS, listItem = listItemData, listTank = listTankData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TrendUtility()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            //searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;
 
            List<TemplateName_COMBO> listTemplate = new List<TemplateName_COMBO>();
            List<TemplateName_COMBO> listTemplateClarifiedSystem = new List<TemplateName_COMBO>();
            listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.Utility);
            listTemplateClarifiedSystem = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.ClarifiedSystem);
            listTemplate.AddRange(listTemplateClarifiedSystem);
            List<PageIndexList> listPageIndex = new List<PageIndexList>();

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            return View(); 
        }

        public ActionResult TrendWasteWater()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.WatseWater;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = new List<TrendSample>();
            listSample = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TEMPLATE_TYPE.WatseWater);
            listSample = masterDataServices.setMappingSortingSample(_db, listSample, (byte)TEMPLATE_TYPE.WatseWater, "");
            ViewBag.listSample = listSample;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendHotOilFlashPoint()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilFlashPoint;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TEMPLATE_TYPE.HotOilFlashPoint);
            List<TrendSample> listSampleHotOilPhysical = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TEMPLATE_TYPE.HotOilPhysical);
            listSample.AddRange(listSampleHotOilPhysical);
            listSample = listSample.GroupBy(m => new { m.SAMPLE_NAME }).Select(g => g.First()).ToList();
            listSample = masterDataServices.setMappingSortingSample(_db, listSample, (byte)TEMPLATE_TYPE.HotOilFlashPoint, "");
            ViewBag.listSample = listSample;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendHotOilPhysical()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilPhysical;
            searhTrendData.TEMPLATE_TYPE1 = (byte)TEMPLATE_TYPE.HotOilPhysical;
            searhTrendData.TEMPLATE_TYPE2 = (byte)TEMPLATE_TYPE.HotOilPhysical;
            searhTrendData.TEMPLATE_TYPE3 = (byte)TEMPLATE_TYPE.HotOilPhysical;
            searhTrendData.TEMPLATE_TYPE4 = (byte)TEMPLATE_TYPE.HotOilPhysical;
            searhTrendData.TEMPLATE_TYPE5 = (byte)TEMPLATE_TYPE.HotOilPhysical;
            searhTrendData.TEMPLATE_TYPE6 = (byte)TEMPLATE_TYPE.HotOilPhysical;
            CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEByTemplateType(_db, (byte)TEMPLATE_TYPE.HotOilPhysical);
            searhTrendData.TEMPLATE_AREA1 = templateData.TEMPLATE_AREA;
            searhTrendData.TEMPLATE_AREA2 = templateData.TEMPLATE_AREA;
            searhTrendData.TEMPLATE_AREA3 = templateData.TEMPLATE_AREA;
            searhTrendData.TEMPLATE_AREA4 = templateData.TEMPLATE_AREA;
            searhTrendData.TEMPLATE_AREA5 = templateData.TEMPLATE_AREA;
            searhTrendData.TEMPLATE_AREA6 = templateData.TEMPLATE_AREA;

            List<TrendSample> listSample = masterDataServices.getTrendSampleListByStringTemplateId(_db, templateData.TEMPLATE_AREA, (byte)TEMPLATE_TYPE.HotOilPhysical);
            List<PageIndexList> listPageIndex = new List<PageIndexList>();

            ViewBag.listSample = listSample;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendHgInPLMonthly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPLMonthly;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendArea> listArea = masterDataServices.getTrendAreaListByTemplateIds(_db, (byte)TEMPLATE_TYPE.HgInPLMonthly);
            listArea = masterDataServices.setMappingSortingArea(_db, listArea, (byte)TEMPLATE_TYPE.HgInPLMonthly);
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendHgInPL()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPL;
            searhTrendData.TEMPLATE_TYPE1 = (byte)TEMPLATE_TYPE.HgInPL;
            searhTrendData.TEMPLATE_TYPE2 = (byte)TEMPLATE_TYPE.HgInPL;
            searhTrendData.TEMPLATE_TYPE3 = (byte)TEMPLATE_TYPE.HgInPL;
            searhTrendData.TEMPLATE_TYPE4 = (byte)TEMPLATE_TYPE.HgInPL;
            searhTrendData.TEMPLATE_TYPE5 = (byte)TEMPLATE_TYPE.HgInPL;
            searhTrendData.TEMPLATE_TYPE6 = (byte)TEMPLATE_TYPE.HgInPL;
            CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEByTemplateType(_db, (byte)TEMPLATE_TYPE.HgInPL);
            //searhTrendData.TEMPLATE_AREA1 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA2 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA3 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA4 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA5 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA6 = templateData.TEMPLATE_AREA;

            List<TrendSample> listSample = masterDataServices.getTrendSampleListByStringTemplateId(_db, templateData.TEMPLATE_AREA, (byte)TEMPLATE_TYPE.HgInPL);
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            List<TrendArea> listArea = masterDataServices.getTrendAreaListByStringTemplateId(_db, templateData.ID);
            ViewBag.listSample = listSample;
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendHgOutletMRU()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgOutletMRU;
            searhTrendData.TEMPLATE_TYPE1 = (byte)TEMPLATE_TYPE.HgOutletMRU;
            searhTrendData.TEMPLATE_TYPE2 = (byte)TEMPLATE_TYPE.HgOutletMRU;
            searhTrendData.TEMPLATE_TYPE3 = (byte)TEMPLATE_TYPE.HgOutletMRU;
            searhTrendData.TEMPLATE_TYPE4 = (byte)TEMPLATE_TYPE.HgOutletMRU;
            searhTrendData.TEMPLATE_TYPE5 = (byte)TEMPLATE_TYPE.HgOutletMRU;
            searhTrendData.TEMPLATE_TYPE6 = (byte)TEMPLATE_TYPE.HgOutletMRU;
            CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEByTemplateType(_db, (byte)TEMPLATE_TYPE.HgOutletMRU);
            //searhTrendData.TEMPLATE_AREA1 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA2 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA3 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA4 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA5 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA6 = templateData.TEMPLATE_AREA;

            List<TrendSample> listSample = masterDataServices.getTrendSampleListByStringTemplateId(_db, templateData.TEMPLATE_AREA, (byte)TEMPLATE_TYPE.HgOutletMRU);
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            List<TrendArea> listArea = masterDataServices.getTrendAreaListByStringTemplateId(_db, templateData.ID);
            ViewBag.listSample = listSample;
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendSulfurInGasMonthly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasMonthly;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = new List<TrendSample>();
            List<TrendArea> listArea = masterDataServices.getTrendAreaByTemplateType(_db, (byte)TEMPLATE_TYPE.SulfurInGasMonthly);
            listArea = masterDataServices.setMappingSortingArea(_db, listArea, (byte)TEMPLATE_TYPE.SulfurInGasMonthly);
            ViewBag.listSample = listSample;
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendSulfurInGasWeekly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            searhTrendData.TEMPLATE_TYPE1 = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            searhTrendData.TEMPLATE_TYPE2 = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            searhTrendData.TEMPLATE_TYPE3 = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            searhTrendData.TEMPLATE_TYPE4 = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            searhTrendData.TEMPLATE_TYPE5 = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            searhTrendData.TEMPLATE_TYPE6 = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEByTemplateType(_db, (byte)TEMPLATE_TYPE.SulfurInGasWeekly);
            //searhTrendData.TEMPLATE_AREA1 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA2 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA3 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA4 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA5 = templateData.TEMPLATE_AREA;
            //searhTrendData.TEMPLATE_AREA6 = templateData.TEMPLATE_AREA;

            List<TrendSample> listSample = masterDataServices.getTrendSampleListByStringTemplateId(_db, templateData.TEMPLATE_AREA, (byte)TEMPLATE_TYPE.SulfurInGasWeekly);
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            List<TrendArea> listArea = masterDataServices.getTrendAreaListByStringTemplateId(_db, templateData.ID);
            ViewBag.listSample = listSample;
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendTwoHundredThousandPond()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.TwoHundredThousandPond;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TEMPLATE_TYPE.TwoHundredThousandPond);
            listSample = masterDataServices.setMappingSortingSample(_db, listSample, (byte)TEMPLATE_TYPE.TwoHundredThousandPond, "");
            ViewBag.listSample = listSample;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        //public ActionResult TrendAcidOffGas()
        //{
        //    MasterDataService masterDataServices = new MasterDataService(_username);
        //    TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

        //    searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.AcidOffGas;
        //    searhTrendData.TEMPLATE_ALL_FLAG1 = true;
        //    searhTrendData.TEMPLATE_ALL_FLAG2 = true;
        //    searhTrendData.TEMPLATE_ALL_FLAG3 = true;
        //    searhTrendData.TEMPLATE_ALL_FLAG4 = true;
        //    searhTrendData.TEMPLATE_ALL_FLAG5 = true;
        //    searhTrendData.TEMPLATE_ALL_FLAG6 = true;
        //    List<TrendSample> listSample = new List<TrendSample>();
        //    List<TrendArea> listArea = masterDataServices.getTrendAreaByTemplateTypeAcidOffGas(_db, (byte)TEMPLATE_TYPE.AcidOffGas);

        //    ViewBag.listSample = listSample;
        //    ViewBag.listArea = listArea;
        //    ViewBag.searhTrendData = searhTrendData;
        //    return View();
        //}
        public ActionResult TrendAcidOffGas()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();
            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.AcidOffGas;
            List<TemplateName_COMBO> listTemplate = new List<TemplateName_COMBO>();
            listTemplate = masterDataServices.getMasterTemplateCombo(_db, (byte)TEMPLATE_TYPE.AcidOffGas);
            listTemplate = listTemplate.GroupBy(x => x.TEMPLATE_NAME).Select(g => g.First()).ToList();
            listTemplate = masterDataServices.setMappingSortingAreaAcidOffFas(_db, listTemplate, (byte)TEMPLATE_TYPE.AcidOffGas);
            List<PageIndexList> listPageIndex = new List<PageIndexList>();

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendHgStab()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgStab;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = new List<TrendSample>();
            listSample = masterDataServices.getTrendSampleListByStringTemplateType(_db, (byte)TEMPLATE_TYPE.HgStab);
            listSample = masterDataServices.setMappingSortingSample(_db, listSample, (byte)TEMPLATE_TYPE.HgStab, "");
            ViewBag.listSample = listSample;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendObservePonds()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.ObservePonds;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = new List<TrendSample>();
            List<TrendArea> listArea = masterDataServices.getTrendAreaByTemplateType(_db, (byte)TEMPLATE_TYPE.ObservePonds);
            listArea = masterDataServices.setMappingSortingArea(_db, listArea, (byte)TEMPLATE_TYPE.ObservePonds);

            ViewBag.listSample = listSample;
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendOilyWaters()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.OilyWaters;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = new List<TrendSample>();
            List<TrendArea> listArea = masterDataServices.getTrendAreaByTemplateType(_db, (byte)TEMPLATE_TYPE.OilyWaters);
            listArea = masterDataServices.setMappingSortingArea(_db, listArea, (byte)TEMPLATE_TYPE.OilyWaters);

            ViewBag.listSample = listSample;
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult TrendEmission()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TrendGasGraphSearchModel searhTrendData = new TrendGasGraphSearchModel();

            searhTrendData.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Emission;
            searhTrendData.TEMPLATE_ALL_FLAG1 = true;
            searhTrendData.TEMPLATE_ALL_FLAG2 = true;
            searhTrendData.TEMPLATE_ALL_FLAG3 = true;
            searhTrendData.TEMPLATE_ALL_FLAG4 = true;
            searhTrendData.TEMPLATE_ALL_FLAG5 = true;
            searhTrendData.TEMPLATE_ALL_FLAG6 = true;
            List<TrendSample> listSample = new List<TrendSample>();
            List<TrendArea> listArea = masterDataServices.getTrendAreaByTemplateType(_db, (byte)TEMPLATE_TYPE.Emission);
            listArea = masterDataServices.setMappingSortingArea(_db, listArea, (byte)TEMPLATE_TYPE.Emission);

            ViewBag.listSample = listSample;
            ViewBag.listArea = listArea;
            ViewBag.searhTrendData = searhTrendData;
            return View();
        }
        public ActionResult ListManageTrendGas()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);  
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            TransactionService transService = new TransactionService(_username);
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.GAS;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        } 

        public ActionResult ListManageTrendUtility()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Utility;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public ActionResult ListManageTrendClarifiedSystem()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.ClarifiedSystem;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public ActionResult ListManageTrendWasteWater()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.WatseWater;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendHotOilFlashPoint()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilFlashPoint;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendHotOilPhysical()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HotOilPhysical;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public ActionResult ListManageTrendHgInPLMonthly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPLMonthly;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendHgInPL()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgInPL;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendHgOutletMRU()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgOutletMRU;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendSulfurInGasMonthly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasMonthly;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendSulfurInGasWeekly()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.SulfurInGasWeekly;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendTwoHundredThousandPond()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.TwoHundredThousandPond;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendHgStab()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.HgStab;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendAcidOffGas()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.AcidOffGas;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendCOA()
        {
            TransactionService transService = new TransactionService(_username);
            TemplateCOASearchModel searchModel = new TemplateCOASearchModel();
            CreateQMS_TR_TEMPLATE_COA coaData = new CreateQMS_TR_TEMPLATE_COA();
            
            ViewBag.listDocStatus = getTemplateTrendCOAStatusList();

            ViewBag.listCustomer = transService.getAllCustomerCOAList(_db);
            ViewBag.listProduct = transService.getAllProductCOAList(_db);

            ViewBag.searchModel = searchModel;
            ViewBag.listPageSize = this.getPageSizeList();
            ViewBag.CreateModel = coaData;
            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.PRODUCT);
            return View();
        }

        public ActionResult ListManageTrendObservePonds()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.ObservePonds;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendOilyWaters()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.OilyWaters;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }
        public ActionResult ListManageTrendEmission()
        {
            MasterDataService masterDataServices = new MasterDataService(_username);
            TemplateSearch searchTemplate = new TemplateSearch();
            TrendManageSearchModel searhTrendData = new TrendManageSearchModel();
            searchTemplate.mSearch = new TemplateSearchModel();
            searchTemplate.mSearch.TEMPLATE_TYPE = (byte)TEMPLATE_TYPE.Emission;
            searchTemplate.PageSize = -1;

            long count = 0;
            long totalPage = 0;
            List<ViewQMS_MA_TEMPLATE> listTemplate = new List<ViewQMS_MA_TEMPLATE>();
            List<PageIndexList> listPageIndex = new List<PageIndexList>();
            listTemplate = masterDataServices.searchQMS_MA_TEMPLATE(_db, searchTemplate, out count, out totalPage, out listPageIndex);

            searhTrendData.START_DATE = getDummyDateTime();
            searhTrendData.END_DATE = searhTrendData.START_DATE;

            ViewBag.listTemplate = listTemplate;
            ViewBag.searhTrendData = searhTrendData;
            ViewBag.listPageSize = this.getPageSizeList();

            return View();
        }

        public JsonResult UpdateControlQMS_TR_TEMPLATE_COA_DETAIL(long id, long controlId, string CUSTOMER_NAME, string PRODUCT_NAME)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    long nResult = transactionServices.UpdateControlIdQMS_TR_TEMPLATE_COA_DETAIL(_db, id, controlId);

                    if (nResult > 0)
                    { 

                        List<ViewQMS_TR_TEMPLATE_COA_DETAIL> listTemplateCOADetail = transactionServices.getQMS_TR_TEMPLATE_COA_DETAILListByCOAID(_db, CUSTOMER_NAME, PRODUCT_NAME); 
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, listTemplateCOADetail = listTemplateCOADetail };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        } 

        public JsonResult UpdateDocStatusQMS_TR_TEMPLATE_COA(long[] ids, byte doc_status_value)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateDocStatusQMS_TR_TEMPLATE_COAByListId(_db, ids, doc_status_value);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateTrendStatusQMS_TR_TEMPLATE_COA(TemplateCOAModel model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                {
                    bool nResult = transactionServices.UpdateTrendStatusQMS_TR_TEMPLATE_COAByListId(_db, model);

                    if (true == nResult)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateTrendStatusQMS_TR_TEMPLATE_COA_DETAIL(TemplateCOADetailModel model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transactionServices = new TransactionService(_username);

                try
                { 
                    bool nResult = transactionServices.UpdateTrendStatusQMS_TR_TEMPLATE_COA_DETAILByListId(_db, model);

                    if (true == nResult)
                    {
                        CreateQMS_TR_TEMPLATE_COA coaData = transactionServices.getQMS_TR_TEMPLATE_COAById(_db, model.userId);
                        List<ViewQMS_TR_TEMPLATE_COA_DETAIL> listTemplateCOADetail = transactionServices.getQMS_TR_TEMPLATE_COA_DETAILListByCOAID(_db, coaData.CUSTOMER_NAME, coaData.PRODUCT_NAME);

                        if (listTemplateCOADetail.Count() > 0)
                        {
                            listTemplateCOADetail = listTemplateCOADetail.OrderBy(m => m.USER_SEQUENCE).ToList();
                        }

                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, listData = listTemplateCOADetail };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMasterCOABySearch(TemplateCOASearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transServices = new TransactionService(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<ViewQMS_TR_TEMPLATE_COA> listData = new List<ViewQMS_TR_TEMPLATE_COA>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listData = transServices.searchQMS_TR_TEMPLATE_COA(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditTrendCOA(long id)
        {
            TransactionService transServie = new TransactionService(_username);

            CreateQMS_TR_TEMPLATE_COA coaData = transServie.getQMS_TR_TEMPLATE_COAById(_db, id); 
            List<ViewQMS_TR_TEMPLATE_COA_DETAIL> listTemplateCOADetail = transServie.getQMS_TR_TEMPLATE_COA_DETAILListByCOAID(_db, coaData.CUSTOMER_NAME, coaData.PRODUCT_NAME);

            if (listTemplateCOADetail.Count() > 0)
            {
                listTemplateCOADetail = listTemplateCOADetail.OrderBy(m => m.USER_SEQUENCE).ToList();
            }

            coaData.DOC_STATUS_VIEW = getDocStatusName((int)coaData.DOC_STATUS);

            ViewBag.listControlGroup = getControlGroupList((byte)CONTROL_GROUP_TYPE.PRODUCT);
            ViewBag.ActionName = Resources.Strings.EditData;
            ViewBag.listTemplateCOADetail = listTemplateCOADetail;
            ViewBag.coaData = coaData;
            ViewBag.ID = id;
            return View();
        }

        public JsonResult getCOADetail(long id)
        {
            TransactionService transServie = new TransactionService(_username);
            Object result;
            if (ModelState.IsValid)
            {
                CreateQMS_TR_TEMPLATE_COA coaData = transServie.getQMS_TR_TEMPLATE_COAById(_db, id);
                List<ViewQMS_TR_TEMPLATE_COA_DETAIL> listTemplateCOADetail = transServie.getQMS_TR_TEMPLATE_COA_DETAILListByCOAID(_db, coaData.CUSTOMER_NAME, coaData.PRODUCT_NAME);

                try
                {
                    if (listTemplateCOADetail.Count() > 0)
                    {
                        listTemplateCOADetail = listTemplateCOADetail.OrderBy(m => m.USER_SEQUENCE).ToList();
                    }

                    coaData.DOC_STATUS_VIEW = getDocStatusName((int)coaData.DOC_STATUS);

                    result = new { result = ReturnStatus.SUCCESS, message = coaData, listDetail = listTemplateCOADetail  };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFileAndFolderName(TrendManageSearch model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices configDataServices = new ConfigServices(_username);

                try
                {
                    long count = 0;
                    long totalPage = 0;
                    List<FolderDataDetail> listFolderDetail = new List<FolderDataDetail>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listFolderDetail = configDataServices.getFileDataByTemplateManageSearch(_db, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listFolderDetail, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExecuteFileTemplateToDB(List<FolderDataDetail> modelFolderDataDetail)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices configDataServices = new ConfigServices(_username);
                bool bResult = false;
                int nSuccess = 0;
                int nFailure = 0;
                string szFailName = "";
                 
                try
                {
                    foreach (FolderDataDetail model in modelFolderDataDetail)
                    {
                        bResult = configDataServices.executeFileTemplateToDB(_db, model);
                        if (true == bResult)
                        {
                            nSuccess++;
                        }
                        else
                        {
                            szFailName = model.m_dtFile;
                            nFailure++;
                            break;
                        }
                    }

                    if (0 == nFailure)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                    }
                    else
                    {
                        //result = new { result = ReturnStatus.ERROR, message = configDataServices.getError() };
                        //result = new { result = ReturnStatus.ERROR, message = Resources.Strings.TotalSuccess + " " + nFailure + " " + Resources.Strings.ErrorFileName + " : " + szFailName + " " + configDataServices.getError() };
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.ErrorFileName + " : " + szFailName + " " + configDataServices.getError() };
                    }
                   
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExecuteAllFileTemplateToDB(TrendManageSearch Model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ConfigServices configDataServices = new ConfigServices(_username);

                long count = 0;
                long totalPage = 0;
                List<FolderDataDetail> listFolderDetail = new List<FolderDataDetail>();
                List<PageIndexList> listPageIndex = new List<PageIndexList>();
                listFolderDetail = configDataServices.getFileDataByTemplateManageSearch(_db, Model, out count, out totalPage, out listPageIndex);
                if(listFolderDetail.Count > 0) { 
                    bool bResult = false;
                    int nSuccess = 0;
                    int nFailure = 0;
                    string szFailName = "";

                    try
                    {
                        foreach (FolderDataDetail model in listFolderDetail)
                        {
                            bResult = configDataServices.executeFileTemplateToDB(_db, model);
                            if (true == bResult)
                            {
                                nSuccess++;
                            }
                            else
                            {
                                szFailName = model.m_dtFile;
                                nFailure++;
                                break;
                            }
                        }

                        if (0 == nFailure)
                        {
                            result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess };
                        }
                        else
                        {
                            //result = new { result = ReturnStatus.ERROR, message = configDataServices.getError() };
                            result = new { result = ReturnStatus.ERROR, message = Resources.Strings.TotalSuccess + " " + nFailure + " " + Resources.Strings.ErrorFileName + " : " + szFailName + " " + configDataServices.getError() };
                        }

                    }
                    catch (Exception ex)
                    {
                        result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                    }
                }
                else
                {
                    result = new { result = ReturnStatus.ERROR, message = "No data in this period!" };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSampleByPlantId(long PLANT_ID, byte TEMPLATE_TYPE)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByPlantAndTypeId(_db, PLANT_ID, TEMPLATE_TYPE);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
         
        public JsonResult GetSampleByTemplateId(string TemplateId, byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByStringTemplateId(_db, TemplateId, TemplateType);
                    listData = masterDataServices.setMappingSortingSample(_db, listData, (byte)TemplateType, TemplateId);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSampleByTemplateType(byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByStringTemplateType(_db, TemplateType);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSampleByTemplateTypeAcidOffGas(byte TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByStringTemplateTypeAcidOffGas(_db, TemplateType);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSampleByTemplateTypeAreaName(byte TemplateType, string AreaName)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByStringTemplateTypeAreaName(_db, TemplateType, AreaName);
                    listData = listData.Where(x => x.SAMPLE_NAME != "").ToList();
                    listData = masterDataServices.setMappingSortingSample(_db, listData, (byte)TemplateType, "");
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSampleByTemplateTypeAreaNameAcidOffGas(byte TemplateType, string AreaName)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByStringTemplateTypeAreaNameAcidOffGas(_db, TemplateType, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSampleByAreaName(string AreaName, long TemplateId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_TEMPLATE template = masterDataServices.getQMS_MA_TEMPLATEById(_db, TemplateId);
                    
                    List<TrendSample> listData = masterDataServices.getTrendSampleListByStringAreaName(_db, AreaName, TemplateId);
                    List<TrendSample> tempListSample = new List<TrendSample>();
                    if (template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasWeekly || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.SulfurInGasMonthly
                        || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.ObservePonds || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.OilyWaters || template.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.Emission)
                    {
                        foreach (TrendSample dt in listData)
                        {
                            var findSAMPLE_NAMEInTemp = tempListSample.Where(a => a.SAMPLE_NAME == dt.SAMPLE_NAME).ToList();
                            if (findSAMPLE_NAMEInTemp.Count > 0)
                            {
                                continue;
                            }
                            else
                            {
                                tempListSample.Add(dt);
                            }
                        }
                        listData = new List<TrendSample>();
                        listData = tempListSample;
                    }
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemBySampleName(String SampleName, long PLANT_ID, byte TEMPLATE_TYPE)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendItem> listData = masterDataServices.getTrendItemListBySample(_db, SampleName, PLANT_ID, TEMPLATE_TYPE);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetItemBySampleNameEx(String SampleName, long TemplateId)
        public JsonResult GetItemBySampleNameEx(string SampleName, string TemplateId, byte TEMPLATE_TYPE)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendItem> listData = masterDataServices.getTrendItemListBySampleEx(_db, SampleName, TemplateId, TEMPLATE_TYPE);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemBySampleNameTemplateType(string SampleName, long[] TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendItem> listData = masterDataServices.getTrendItemListBySampleNameTemplateType(_db, SampleName, TemplateType);
                    listData = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType[1], SampleName, "");
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemBySampleNameTemplateTypeEx(string SampleName, long[] TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendItem> listData = masterDataServices.getTrendItemListBySampleNameTemplateTypeEx(_db, SampleName, TemplateType);
                    listData = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType[0], SampleName, "");
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetColItemBySampleNameTemplateTypeEx(string SampleName, long[] TemplateType)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendItem> listData = masterDataServices.getTrendColItemListBySampleNameTemplateTypeEx(_db, SampleName, TemplateType);
                    listData = masterDataServices.setMappingSortingItem(_db, listData, (byte)TemplateType[0], SampleName, "");
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateIdEx(string SampleName, long TemplateId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEById(_db, TemplateId);
                    List<TrendItem> listData = masterDataServices.getTrendItemListByStringTemplateId(_db, TemplateId, SampleName);
                    listData = masterDataServices.setMappingSortingItem(_db, listData, (byte)templateData.TEMPLATE_TYPE, SampleName, "");
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateId(string SampleName, long TemplateId)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEById(_db, TemplateId);
                    List<TrendItem> listData = masterDataServices.getTrendItemListByStringTemplateIdSampleName(_db, TemplateId, SampleName);
                    listData = masterDataServices.setMappingSortingItem(_db, listData, (byte)templateData.TEMPLATE_TYPE, SampleName, "");
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateTypeEx(byte TEMPLATE_TYPE)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendItem> listData = masterDataServices.getTrendItemListByTemplateType(_db, TEMPLATE_TYPE);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateIdAreaNameEx(string SampleName, long TemplateId, string AreaName)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEById(_db, TemplateId);
                    List<TrendItem> listData = masterDataServices.getTrendItemListByStringTemplateIdAreaName(_db, TemplateId, SampleName, AreaName);
                    listData = masterDataServices.setMappingSortingItem(_db, listData, (byte)templateData.TEMPLATE_TYPE, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemByTemplateIdAreaNameExAcidOffGas(string SampleName, long TemplateId, string AreaName)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    CreateQMS_MA_TEMPLATE templateData = masterDataServices.getQMS_MA_TEMPLATEById(_db, TemplateId);
                    List<TrendItem> listData = masterDataServices.getTrendItemListByStringTemplateIdAreaNameAcidOffGas(_db, TemplateId, SampleName, AreaName);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAreaByTemplateTypeAndTemplateName(byte TEMPLATE_TYPE, string TEMPLATE_NAME)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendArea> listData = masterDataServices.getTrendAreaByTemplateTypeAndTemplateName(_db, TEMPLATE_TYPE, TEMPLATE_NAME);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAreaByTemplateType(byte TEMPLATE_TYPE)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    List<TrendArea> listData = masterDataServices.getTrendAreaByTemplateType(_db, TEMPLATE_TYPE);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGraphData(TrendGasGraphSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transDataServices = new TransactionService(_username); 

                try
                {
                    //List<TrendGraphSum> listSummary = new List<TrendGraphSum>();
                    //List<ViewQMS_TR_TREND_DATA> listData = transDataServices.getGetGraphData(_db, searchModel, out listSummary);
                    searchModel.START_DATE = resetStartTime(searchModel.START_DATE);
                    searchModel.END_DATE = resetEndTime(searchModel.END_DATE);

                    GRAPH_TREDN_GAS listData = transDataServices.getGetGraphDataEx(_db, searchModel);

                    result = new { result = ReturnStatus.SUCCESS, message = listData };
                    //result = new { result = ReturnStatus.SUCCESS, message = listData, listSummary = listSummary };

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return new JsonResult
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            //return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCOAGraphData(TrendCOAGraphSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transDataServices = new TransactionService(_username);

                try
                {
                    searchModel.START_DATE = resetStartTime(searchModel.START_DATE);
                    searchModel.END_DATE = resetEndTime(searchModel.END_DATE);

                    GRAPH_TREDN_GAS listData = transDataServices.getGetCOAGraphData(_db, searchModel); 
                    result = new { result = ReturnStatus.SUCCESS, message = listData };

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return new JsonResult
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            //return Json(result, JsonRequestBehavior.AllowGet);
        }


        public void GetCSCDataExcel(string PRODUCT)
        {
            TransactionService transDataServices = new TransactionService(_username);
            ConfigServices cogServices = new ConfigServices(_username);
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                ProductQualityToCSCSearchModel searchModel = jss.Deserialize<ProductQualityToCSCSearchModel>(PRODUCT);
                

                if (null != searchModel)
                {
                    searchModel.START_DATE = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    searchModel.END_DATE = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 0, 0, 0).AddDays(1).AddMilliseconds(-1);
                    ExcelPackage excel = transDataServices.CSCDataExcel(_db, searchModel);

                  
                    CreatePRODUCT_CSC_PATH model = new CreatePRODUCT_CSC_PATH();
                    model = cogServices.getProductCSCPath(_db);
                    string fileName = (model.PRODUCT_CSC_NAME != null) ? model.PRODUCT_CSC_NAME : @Resources.Strings.ProductQualityToCSC;

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + "_" + searchModel.CUSTOMER_NAME + ".xlsx");
                    Response.BinaryWrite(excel.GetAsByteArray());
                }
            }catch(Exception ex){
                transDataServices.writeErrorLog(ex.Message);
            }
        }

        public JsonResult GetCSCData(ProductQualityToCSCSearchModel searchModel)
        {
            Object result;
            if (ModelState.IsValid)
            {
                TransactionService transDataServices = new TransactionService(_username);

                try
                {
                    searchModel.START_DATE = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                    searchModel.END_DATE = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 23, 59, 59);
                    GRAPH_TREDN_GAS listData = transDataServices.getCSCData(_db, searchModel);
                    result = new { result = ReturnStatus.SUCCESS, message = listData };

                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void TreandDataExcel( string searchData)
        {
            try{ 
                JavaScriptSerializer jss = new JavaScriptSerializer();
                TrendGasGraphSearchModel searchModel = jss.Deserialize<TrendGasGraphSearchModel>(searchData); 
                TransactionService transDataServices = new TransactionService(_username);

                string szFileName = @Resources.Strings.TrendGAS;
                if (searchModel.TEMPLATE_TYPE == (byte)TEMPLATE_TYPE.Utility)
                {
                    szFileName = @Resources.Strings.TrendUtility;
                }
            
                ExcelPackage excel = transDataServices.TreandDataExcel(_db, searchModel);
                searchModel.START_DATE = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                searchModel.END_DATE = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 0, 0, 0).AddDays(1).AddMilliseconds(-1);
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + szFileName + ".xlsx");
                Response.BinaryWrite(excel.GetAsByteArray());
            }catch(Exception ex){

            }
        }


        public void TreandDataExcelByReportId(long id)
        {
            TransactionService transDataServices = new TransactionService(_username);
            ReportServices repServices = new ReportServices(_username);

            CreateQMS_RP_TREND_DATA REPORT_MASTER = repServices.getQMS_RP_TREND_DATAById(_db, id);
            ExcelPackage excel = transDataServices.TreandDataExcelByReportId(_db, id);
            
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + REPORT_MASTER.NAME + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }

        public void TreandProductExcel(string searchJson)
        { 
            TransactionService transDataServices = new TransactionService(_username);

            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                TrendCOAGraphSearchModel searchModel = jss.Deserialize<TrendCOAGraphSearchModel>(searchJson);

                searchModel.START_DATE = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
                searchModel.END_DATE = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 0, 0, 0).AddDays(1).AddMilliseconds(-1);
                ExcelPackage excel = transDataServices.TreandCOAExcel(_db, searchModel);
                  
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.TrendProduct + ".xlsx");
                Response.BinaryWrite(excel.GetAsByteArray());
            }
            catch (Exception ex)
            { 
                transDataServices.writeErrorLog(ex.Message); 
            }
            
          

        }

        public void ProductDataExcel() //(ProductExaDataSearchModel searchModel)
        {
            TransactionService transDataServices = new TransactionService(_username);
            ProductExaDataSearchModel searchModel = new ProductExaDataSearchModel();

            searchModel.PRODUCT_ID = 3;
            searchModel.PLANT_ID = 4;
            searchModel.START_DATE = new DateTime(2017, 1, 1, 18, 0, 0);
            searchModel.END_DATE = new DateTime(2017, 1, 2, 8, 50, 0);

            //searchModel.START_DATE = new DateTime(searchModel.START_DATE.Year, searchModel.START_DATE.Month, searchModel.START_DATE.Day, 0, 0, 0);
            //searchModel.END_DATE = new DateTime(searchModel.END_DATE.Year, searchModel.END_DATE.Month, searchModel.END_DATE.Day, 0, 0, 0).AddDays(1).AddMilliseconds(-1);
            ExcelPackage excel = transDataServices.ProductExaDataExcel(_db, searchModel);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + @Resources.Strings.TrendData + ".xlsx");
            Response.BinaryWrite(excel.GetAsByteArray());

        }
    }
}
