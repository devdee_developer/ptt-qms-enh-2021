<!DOCTYPE html>
<html lang="en">

  <head>

    <?php include("./head_tag.php"); ?>



  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"<span>PTT QMS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php include("./user_profile.php"); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php include("./sidemenu_qms.php"); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include("./menu_footer.php"); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php include("./top_nav.php"); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Off Control Summary Report (Add New)</h3>
                <div class="clearfix"></div>


              </div>
            </div>

            <div class="clearfix"></div>
            <hr>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="row"  style="text-align:right;">
                    <!-- <ul class="nav navbar-left panel_toolbox" style="text-align:right;">
                    <il><span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                      </il>
                      <il><span class="btn btn-warning" style="color:white;"><i class="fa fa-save"></i> Save</span>
                      </il>
                    </ul> -->

                    <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                    <span class="btn btn-success" style="color:white;"><i class="fa fa-save"></i> Save</span>
                  </div>
                  <div class="x_title">
                    <h2></h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">

                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Name
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Group of control value">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Status
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control">
                          <option>Choose Status</option>
                          <option></option>
                        </select>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          Start
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:left;">
                          <input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="Start date" aria-describedby="inputSuccess2Status">
                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                          End
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:right;">
                          <input type="text" class="form-control has-feedback-left" id="single_cal2" placeholder="Start date" aria-describedby="inputSuccess2Status">
                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
                      <br>
                    </div>
                  </div>
                </div>
              </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Off Control Summary / Detail / Graph / Description </h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="oc-summary-tab" role="tab" data-toggle="tab" aria-expanded="true">Off Control Summary</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="oc-detaiol-tab" data-toggle="tab" aria-expanded="false">Off Control Detail</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="oc-graph-tab" data-toggle="tab" aria-expanded="false">Graph</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="oc-description-tab" data-toggle="tab" aria-expanded="false">Description</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="oc-summary-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:left;">
                                <br>
                                   ใส่ค่าปริมาณ Off Control
                                   <br><br>
                              <table class="table table-bordered" style="width:100%;">
                                <thead style="width:100%;">
                                  <tr>
                                    <th>Product</th>

                                    <th>GSP#1</th>
                                    <th>GSP#2</th>
                                    <th>GSP#3</th>
                                    <th>GSP#5</th>
                                    <th>GSP#6</th>
                                    <th>ESP</th>
                                    <th>New Proudct</th>
                                  </tr>
                                </thead>
                                <tbody style="width:100%;" style="text-align:left;">
                                  <tr>
                                    <td>Ethane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                  <tr>
                                    <td>Propane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                  <tr>
                                    <td>Butane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>LPG</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>NGL</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>Total</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:left;">
                                <br>
                                   ใส่ค่าปริมาณ Input
                                   <br><br>
                              <table class="table table-bordered" style="width:100%;">
                                <thead style="width:100%;">
                                  <tr>
                                    <th>Product</th>

                                    <th>GSP#1</th>
                                    <th>GSP#2</th>
                                    <th>GSP#3</th>
                                    <th>GSP#5</th>
                                    <th>GSP#6</th>
                                    <th>ESP</th>
                                    <th>New Proudct</th>
                                  </tr>
                                </thead>
                                <tbody style="width:100%;" style="text-align:left;">
                                  <tr>
                                    <td>Ethane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                  <tr>
                                    <td>Propane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                  <tr>
                                    <td>Butane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>LPG</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>NGL</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>Total</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:left;">
                                <br>
                                   % Off Control คำนวนมาจากตรารางด้านบน
                                   <br><br>
                              <table class="table table-bordered" style="width:100%;">
                                <thead style="width:100%;">
                                  <tr>
                                    <th>Product</th>

                                    <th>GSP#1</th>
                                    <th>GSP#2</th>
                                    <th>GSP#3</th>
                                    <th>GSP#5</th>
                                    <th>GSP#6</th>
                                    <th>ESP</th>
                                    <th>New Proudct</th>
                                  </tr>
                                </thead>
                                <tbody style="width:100%;" style="text-align:left;">
                                  <tr>
                                    <td>Ethane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                  <tr>
                                    <td>Propane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                  <tr>
                                    <td>Butane</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>LPG</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>NGL</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>
                                  <tr>
                                    <td>Total</td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                  </tr>

                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="oc-detail-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <!-- <h2>Total Production</h2> -->
                                  <ul class="nav navbar-left panel_toolbox">
                                    <!-- <il>ตรวจสอบรายละเอียด
                                    </il> -->
                                    <il><span class="btn btn-primary" style="color:white;"><i class="fa fa-search"></i> Search</span>
                                    </il>
                                    <!-- <il><a href="mn_oc_cal_add.php"><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span></a>
                                    </il> -->
                                    <il><span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                    </il>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table class="table table-bordered" style="width:100%;">
                                      <thead style="width:100%;">
                                        <tr>
                                          <th>
                                            <input type="checkbox">
                                          </th>
                                          <th>Plant</th>
                                          <th>Product</th>
                                          <th>Start Date</th>
                                          <th>End Date</th>
                                          <th>Volume(TON)</th>
                                          <!-- <th>Status</th> -->
                                          <th>Root Cause</th>
                                          <!-- <th>Description</th> -->
                                          <th>Tools</th>
                                        <tr>
                                      </thead>
                                      <tbody style="width:100%;">
                                        <tr>
                                          <td></td>
                                          <td>  <select class="form-control">
                                            <option>Choose Plant</option>
                                            <option>GSP#1</option>
                                            <option>GSP#2</option>
                                            <option>GSP#3</option>
                                            <option>GSP#5</option>
                                            <option>GSP#6</option>
                                            <option>ESP</option>
                                          </select>
                                          </td>
                                          <td>
                                            <select class="form-control">
                                              <option>Choose Product</option>
                                              <option>Product#1</option>
                                              <option>Product#2</option>
                                              <option>Product#3</option>
                                              <option>Product#4</option>
                                              <option>Product#5</option>
                                            </select>
                                          </td>
                                          <td>
                                            <input type="text" class="form-control has-feedback-left" id="single_cal3" placeholder="Start date" aria-describedby="inputSuccess2Status">
                                          </td>
                                          <td>
                                            <input type="text" class="form-control has-feedback-left" id="single_cal4" placeholder="Start date" aria-describedby="inputSuccess2Status">
                                          </td>
                                          <td>
                                            <input type="text" class="form-control" placeholder="">
                                          </td>
                                          <!-- <th>xxx</th> -->
                                          <td>  <select class="form-control">
                                              <option>Choose Root Cause</option>
                                              <option></option>

                                            </select></td>

                                          <td><a href=""><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span></a></td>
                                        </tr>
                                        <tr>
                                          <td><input type="checkbox"></td>
                                          <td>GSP#1</td>
                                          <td>Ethane</td>
                                          <td>1/8/2016 00:00:00</td>
                                          <td>31/8/2016 02:00:00</td>
                                          <th>xxx</th>
                                          <td>Man</td>
                                          <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                        </tr>

                                        <tr>
                                          <td><input type="checkbox"></td>
                                          <td>GSP#1</td>
                                          <td>Propane</td>
                                          <td>1/8/2016 04:00:00</td>
                                          <td>31/8/2016 05:00:00</td>
                                          <th>xxx</th>
                                          <td>Machine</td>
                                          <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                        </tr>
                                        <tr>
                                          <td><input type="checkbox"></td>
                                          <td>GSP#1</td>
                                          <td>Pantane</td>
                                          <td>1/8/2016 10:00:00</td>
                                          <td>31/8/2016 11:00:00</td>
                                          <th>xxx</th>
                                          <td>Machine</td>
                                          <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                        </tr>
                                        <tr>
                                          <td><input type="checkbox"></td>
                                          <td>GSP#1</td>
                                          <td>LPG</td>
                                          <td>1/8/2016 12:00:00</td>
                                          <td>31/8/2016 14:00:00</td>
                                          <th>xxx</th>
                                          <td>Machine</td>
                                          <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                        </tr>

                                      </tbody>
                                    </table>


                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                  Total 4 records
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                  Results per page
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                  <select class="form-control">
                                    <option>20 items</option>
                                    <option>30 items</option>
                                    <option>50 items</option>
                                    <option>100 items</option>
                                  </select>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                                  <i class="fa fa-chevron-left"></i> Previous
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12" >
                                  <select class="form-control">
                                    <option>1</option>
                                  </select>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                  / 1 Next <i class="fa fa-chevron-right"></i>
                                </div>
                              </div>

                            </div>


                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="oc-graph-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
                                <!-- <img src="./images/qms/control_value.png" alt="" style="width:80%;" data-toggle="modal" data-target=".control-modal-lg"/> -->
                                <div class="row" style="text-align:right;">
                                    <a href="report_offcontrol_summary_graph.php"><span class="btn btn-primary" style="color:white;"><i class="fa fa-bar-chart"></i> View Graph</span></a>
                                </div>
                                <div class="row">

                                    <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      Title
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Group of control value">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">

                                    </div>
                                  </div>
                                  <br>

                                  <div class="row">

                                      <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                        X-Aix Title
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-12">
                                          <input type="text" class="form-control" placeholder="Group of control value">
                                      </div>
                                      <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                        Y-Aix Title
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Group of control value">
                                      </div>
                                    </div>
                                  <hr>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="row" style="text-align:left;">
                                      <span class="btn btn-danger" style="color:white;"><i class="fa fa-trash"></i> Delete</span>
                                  </div>

                                  <br>
                                  <table class="table table-bordered" style="width:100%;">
                                    <thead style="width:100%;">
                                      <tr>
                                        <th><input type="checkbox"></th>
                                        <th>Previouse</th>
                                        <th>% Of Control</th>
                                        <th>Description</th>
                                        <th>Tools</th>
                                      <tr>
                                    </thead>
                                    <tbody style="width:100%;">
                                      <tr>
                                        <td></td>
                                        <td><input type="text" class="form-control" placeholder=""></td>
                                        <td><input type="text" class="form-control" placeholder=""></td>
                                        <td><input type="text" class="form-control" placeholder=""></td>
                                        <td><span class="btn btn-warning" style="color:white;"><i class="fa fa-plus"></i> Add</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>2014</td>
                                        <td>1.61</td>
                                        <td></td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>2015</td>
                                        <td>0.87</td>
                                        <td></td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Jan-2016</td>
                                        <td>0.87</td>
                                        <td>8-9 Jun GSP#1 increase feed gas</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-arrow-down"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Feb-2016</td>
                                        <td>1.03</td>
                                        <td>24Feb GSP#1 WHB & Refrig Trip</td>
                                        <td><a href="#"><i class="fa fa-edit"></i></a>
                                          <a href="#"><i class="fa fa-arrow-up"></i></a>
                                        </td>
                                      </tr>

                                    </tbody>
                                  </table>
                                </div>
                            </div>
                          </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="oc-description-tab">
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
                                <br>
                                <div class="row">

                                    <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      Text Title
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Text Title">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">

                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">

                                    </div>
                                  </div>
                                  <br>
                                  <div class="row">

                                      <div class="col-md-2 col-sm-2 col-xs-12" style="text-align:right;">
                                      Description
                                      </div>

                                      <div class="col-md-8 col-sm-8 col-xs-12" style="text-align:right;">
                                        <textarea id="message" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">
                                        </textarea>
                                      </div>

                                    </div>
                                  <hr>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- Contol modal -->
        <div class="modal fade control-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Control Value</h4>
              </div>
              <div class="modal-body">
                <!-- <h4>เลือกรายงาน Off Control Report ที่ต้องการ</h4> -->
                <!-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      <input type="checkbox">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12" style="text-align:left;">
                      Show on Trend Data
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      <input type="checkbox">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12" style="text-align:left;">
                      CONC / LOADING
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      CONC   <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      LOADING  <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      Min   <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:right;">
                      Max  <input type="checkbox">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12" style="text-align:left;">
                        <input type="text" class="form-control" placeholder="">
                    </div>
                  </div>
                </div>
                <br><br><br><br><br><br><br><br><br><br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-trash"></i> Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
              </div>

            </div>
          </div>
        </div>
        <!-- end Control modal -->
        <!-- footer content -->
          <?php include("./footer.php"); ?>
        <!-- /footer content -->
      </div>
    </div>


    <?php include("./footer_script.php"); ?>


    <!-- Flot -->

    <!-- /Flot -->

    <!-- jQuery Sparklines -->
    <script>
      $(document).ready(function() {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'bar',
          height: '40',
          barWidth: 9,
          colorMap: {
            '7': '#a1a1a1'
          },
          barSpacing: 2,
          barColor: '#26B99A'
        });

        $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
          type: 'line',
          width: '200',
          height: '40',
          lineColor: '#26B99A',
          fillColor: 'rgba(223, 223, 223, 0.57)',
          lineWidth: 2,
          spotColor: '#26B99A',
          minSpotColor: '#26B99A'
        });
      });
    </script>
    <!-- /jQuery Sparklines -->

    <!-- Doughnut Chart -->

    <!-- /Doughnut Chart -->

    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'right',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };

        $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange_right').daterangepicker(optionSet1, cb);

        $('#reportrange_right').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange_right').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });

        $('#options1').click(function() {
          $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
        });

        $('#options2').click(function() {
          $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
        });

        $('#destroy').click(function() {
          $('#reportrange_right').data('daterangepicker').remove();
        });

      });
    </script>

    <script>
      $(document).ready(function() {
        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>

    <script>
      $(document).ready(function() {
        $('#single_cal1').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_1"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal2').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_2"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal3').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal4').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>

    <script>
      $(document).ready(function() {
        $('#reservation').daterangepicker(null, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- morris.js -->
    <script>
      $(document).ready(function() {
        Morris.Bar({
          element: 'graph_bar',
          data: [
            { "period": "Jan", "Hours worked": 80 },
            { "period": "Feb", "Hours worked": 125 },
            { "period": "Mar", "Hours worked": 176 },
            { "period": "Apr", "Hours worked": 224 },
            { "period": "May", "Hours worked": 265 },
            { "period": "Jun", "Hours worked": 314 },
            { "period": "Jul", "Hours worked": 347 },
            { "period": "Aug", "Hours worked": 287 },
            { "period": "Sep", "Hours worked": 240 },
            { "period": "Oct", "Hours worked": 211 }
          ],
          xkey: 'period',
          hideHover: 'auto',
          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          ykeys: ['Hours worked', 'sorned'],
          labels: ['Hours worked', 'SORN'],
          xLabelAngle: 60,
          resize: true
        });

        $MENU_TOGGLE.on('click', function() {
          $(window).resize();
        });
      });
    </script>
    <!-- /morris.js -->

    <!-- Skycons -->
    <script>
      var icons = new Skycons({
          "color": "#73879C"
        }),
        list = [
          "clear-day", "clear-night", "partly-cloudy-day",
          "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
          "fog"
        ],
        i;

      for (i = list.length; i--;)
        icons.set(list[i], list[i]);

      icons.play();
    </script>
    <!-- /Skycons -->

    <!-- gauge.js -->
    <script>
      var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
          length: 0.75,
          strokeWidth: 0.042,
          color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 100;
      gauge.animationSpeed = 32;
      gauge.set(80);
      gauge.setTextField(document.getElementById("gauge-text"));

      var target = document.getElementById('foo2'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 5000;
      gauge.animationSpeed = 32;
      gauge.set(4200);
      gauge.setTextField(document.getElementById("gauge-text2"));
    </script>
    <!-- /gauge.js -->
  </body>
</html>
