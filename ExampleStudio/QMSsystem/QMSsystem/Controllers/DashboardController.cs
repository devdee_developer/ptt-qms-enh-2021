﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMSSystem.CoreDB.Services;
using System.IO;
using QMSSystem.CoreDB.Helper;
using QMSSystem.Model;
using OfficeOpenXml;
using QMSsystem.Models.Privilege;

namespace QMSsystem.Controllers
{

    [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Dashboard }, DataAccess = DataAccessEnum.Authen)]
    //[LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { 
    //    PrivilegeModeEnum.D_Home, PrivilegeModeEnum.N_LabControlChart }, DataAccess = DataAccessEnum.Anonymous)]
    public class DashboardController : BaseController
    {
        public ActionResult Dashboard(long id = 0)
        {

            MasterDataService masterDataServices = new MasterDataService(_username);
            DashboardSearchModel searchDashboard = new DashboardSearchModel();
            KPISearchModel searchKPI = new KPISearchModel();
            //CreateQMS_MA_KPI createQMS_MA_KPI = new CreateQMS_MA_KPI();
            //createQMS_MA_KPI.ACTIVE_DATE = getDummyDateTime();


            ViewBag.listPlant = masterDataServices.getQMS_MA_PLANTActiveList(_db);
            ViewBag.listProduct = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.listComposition = masterDataServices.getQMS_MA_PRODUCTActiveList(_db);
            ViewBag.searchKPI = searchKPI;

            if (masterDataServices.getQMS_MA_PLANTActiveList(_db).Count() > 0 && masterDataServices.getQMS_MA_PRODUCTActiveList(_db).Count() > 0)
            {
                long PlantId = masterDataServices.getQMS_MA_PLANTActiveList(_db)[0].ID;
                long Product = masterDataServices.getQMS_MA_PRODUCTActiveList(_db)[0].ID;
                ViewBag.listComposition = masterDataServices.GetComposationbyplanIdproIdControlValue(_db, PlantId, Product);
            }

            ViewBag.searchDashboard = searchDashboard;
            ViewBag.listPageSize = this.getPageSizeList();

            //ViewBag.createQMS_MA_KPI = createQMS_MA_KPI;
            return View();

        }
        [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home }, DataAccess = DataAccessEnum.Authen)]
        public JsonResult getOffControlByOffControlCalAutoSearch(DashOffControlCalAutoSearchModel search)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);
                TransactionService transService = new TransactionService(_username);
                try
                {
                    ViewALL_QMS_RP_OFF_CONTROL_SUMMARY listData = new ViewALL_QMS_RP_OFF_CONTROL_SUMMARY();
                    //reset date
                    search.START_DATE = new DateTime(search.START_DATE.Year, search.START_DATE.Month, search.START_DATE.Day, 0, 0, 0);
                    search.END_DATE = new DateTime(search.END_DATE.Year, search.END_DATE.Month, search.END_DATE.Day, 23, 59, 59);


                    if (search.CHK_RECALRAW)
                    {
                        reportServices.reCalRawDataByDashRPOffControlId(_db, search);
                    }

                    listData = reportServices.getTemplateDashOffControlSummaryWithAutoCalEx(_db, search);

                    long count = 0;
                    long totalPage = 0;
                    ReportOffControlDetailSearch modelSearch = new ReportOffControlDetailSearch();
                    modelSearch.PageIndex = 1;
                    modelSearch.PageSize = 20; //เอาทั้งหมด
                    modelSearch.SortOrder = "";
                    modelSearch.SortOrder = "PLANT_ID";
                    modelSearch.mSearch = new ReportOffControlDetailSearchModel();
                    modelSearch.mSearch.START_DATE = search.START_DATE;
                    modelSearch.mSearch.END_DATE = search.END_DATE;

                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listDataDetail = reportServices.searchDashQMS_RP_OFF_CONTROL_DETAILEx(_db, modelSearch, search, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, listDataDetail = listDataDetail, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getProductCompositionByOffControlCalAutoSearch(DashOffControlCalAutoSearchModel search)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);
                TransactionService transService = new TransactionService(_username);
                try
                {
                    ViewALL_QMS_RP_OFF_CONTROL_DETAIL listData = new ViewALL_QMS_RP_OFF_CONTROL_DETAIL();
                    //reset date
                    search.START_DATE = new DateTime(search.START_DATE.Year, search.START_DATE.Month, search.START_DATE.Day, 0, 0, 0);
                    search.END_DATE = new DateTime(search.END_DATE.Year, search.END_DATE.Month, search.END_DATE.Day, 23, 59, 59);


                    if (search.CHK_RECALRAW)
                    {
                        reportServices.reCalRawDataByDashRPOffControlId(_db, search);
                    }

                    listData = reportServices.getTemplateProductCompoOffControlWithAutoCalEx(_db, search);

                    long count = 0;
                    long totalPage = 0;
                    ReportOffControlDetailSearch modelSearch = new ReportOffControlDetailSearch();
                    modelSearch.PageIndex = 1;
                    modelSearch.PageSize = 20; //เอาทั้งหมด
                    modelSearch.SortOrder = "";
                    modelSearch.SortOrder = "PLANT_ID";
                    modelSearch.mSearch = new ReportOffControlDetailSearchModel();
                    modelSearch.mSearch.START_DATE = search.START_DATE;
                    modelSearch.mSearch.END_DATE = search.END_DATE;

                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listDataDetail = reportServices.searchDashQMS_RP_OFF_CONTROL_DETAILEx(_db, modelSearch, search, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, listDataDetail = listDataDetail, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //public JsonResult GetProductDistributionChartByGSPProductComposition(productDistributionChartSearchModel model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        MasterDataService masterDataServices = new MasterDataService(_username);

        //        try
        //        {
        //            //model.START_DATE = new DateTime(2023, 12, 01);
        //            //model.END_DATE = new DateTime(2023, 12, 10);
        //            //model.GSP = 4;
        //            //model.Product = 4;
        //            //model.Composition = "C1";
        //            ProductDistributionChartMod objData = masterDataServices.GetProductDistributionChartByGSPProductComposition(_db, model);
        //            result = new { result = ReturnStatus.SUCCESS, message = objData };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home }, DataAccess = DataAccessEnum.Authen)]
        //public JsonResult GetSigmaLevelChartByProductComposition(productDistributionChartSearchModel model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        MasterDataService masterDataServices = new MasterDataService(_username);

        //        try
        //        {
        //            //model.START_DATE = new DateTime(2023, 12, 01);
        //            //model.END_DATE = new DateTime(2024, 01, 14);
        //            //model.GSP = 3;
        //            //model.Product = 4;
        //            //model.Composition = "C1";
        //            SigmaLevelChartMod objData = masterDataServices.GetSigmaLevelChartByProductComposition(_db, model);
        //            result = new { result = ReturnStatus.SUCCESS, message = objData };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //[LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home }, DataAccess = DataAccessEnum.Authen)]
        //public JsonResult GetSigmaLevelChartByProductCompositionYear(productDistributionChartSearchModel model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        MasterDataService masterDataServices = new MasterDataService(_username);

        //        try
        //        {
        //            //model.END_DATE = new DateTime(2024, 01, 02);
        //            //model.Product = 4;
        //            //model.Composition = "C1";
        //            SigmaLevelChartBarMod objData = masterDataServices.GetSigmaLevelChartByProductCompositionYear(_db, model);
        //            result = new { result = ReturnStatus.SUCCESS, message = objData };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}


        public JsonResult GetProductCompositionChartByProductComposition(productDistributionChartSearchModel model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    //model.END_DATE = new DateTime(2023, 12, 10);
                    //model.Product = 4;
                    //model.Composition = "C1";
                    SigmaLevelChartMod objData = masterDataServices.GetSigmaLevelChartByProductComposition(_db, model);
                    result = new { result = ReturnStatus.SUCCESS, message = objData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home }, DataAccess = DataAccessEnum.Authen)]
        public JsonResult GetProductCompositionChartLineByProduct(productDistributionChartSearchModel model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    //model.START_DATE = new DateTime(2023, 12, 01);
                    //model.END_DATE = new DateTime(2024, 01, 10);
                    //model.GSP = 3;
                    //model.Product = 4;
                    //model.Composition = "C1";
                    SigmaLevelChartBarMod objData = masterDataServices.GetProductCompositionChartLineByProduct(_db, model);
                    result = new { result = ReturnStatus.SUCCESS, message = objData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home}, DataAccess = DataAccessEnum.Authen)]
        public JsonResult GetProductCompositionChartLineByProductComposition(productDistributionChartSearchModel model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {

                    //model.START_DATE = new DateTime(2023, 11, 30);
                    //model.END_DATE = new DateTime(2023, 12, 10);
                    //model.GSP = 3;
                    //model.Product = 4;
                    //model.Composition = "C1";
                    SigmaLevelChartBarMod objData = masterDataServices.GetProductCompositionChartLineByProductComposition(_db, model);
                    result = new { result = ReturnStatus.SUCCESS, message = objData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult convertEventRawDataToCPKTempData(ConvertEventRawDataToCPKModel model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                     
                    model.START_DATE = new DateTime(2023, 12, 27);
                    model.END_DATE = new DateTime(2023, 12, 27 );


                    string objData = masterDataServices.convertEventRawDataToCPKTempData(_db, model);
                    result = new { result = ReturnStatus.SUCCESS, message = objData };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //[LegacyAuthorize(RoleTask = new PrivilegeModeEnum[] { PrivilegeModeEnum.D_Dashboard, PrivilegeModeEnum.D_Home }, DataAccess = DataAccessEnum.Authen)]
        //public JsonResult getOffControlByYeargetOffControlByYear(DashOffControlCalAutoSearchModel model)
        //{
        //    Object result;
        //    if (ModelState.IsValid)
        //    {
        //        ReportServices reportServices = new ReportServices(_username);
        //        try
        //        {
        //            OffControlStaticByYearChartBarMod listData = new OffControlStaticByYearChartBarMod();
        //            //reset date

        //            //if (model.CHK_RECALRAW)
        //            //{
        //            //    reportServices.reCalRawDataByDashRPOffControlId(_db, model);
        //            //}

        //            listData = reportServices.getTemplateByYearOffControlWithAutoCalEx(_db, model);

        //            long count = 0;
        //            long totalPage = 0;
        //            ReportOffControlDetailSearch modelSearch = new ReportOffControlDetailSearch();
        //            modelSearch.PageIndex = 1;
        //            modelSearch.PageSize = 20; //เอาทั้งหมด
        //            modelSearch.SortOrder = "";
        //            modelSearch.SortOrder = "PLANT_ID";
        //            modelSearch.mSearch = new ReportOffControlDetailSearchModel();

        //            List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
        //            List<PageIndexList> listPageIndex = new List<PageIndexList>();
        //            listDataDetail = reportServices.searchDashQMS_RP_OFF_CONTROL_DETAILEx(_db, modelSearch, model, out count, out totalPage, out listPageIndex);

        //            result = new { result = ReturnStatus.SUCCESS, message = listData, listDataDetail = listDataDetail, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
        //        }
        //        catch (Exception ex)
        //        {
        //            result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
        //        }
        //    }
        //    else
        //    {
        //        result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult getProductOffControlbyGSP(DashOffControlCalAutoSearchModel model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                ReportServices reportServices = new ReportServices(_username);
                try
                {
                    OffControlStaticByYearChartBarMod listData = new OffControlStaticByYearChartBarMod();
                    //reset date

                    //if (model.CHK_RECALRAW)
                    //{
                    //    reportServices.reCalRawDataByDashRPOffControlId(_db, model);
                    //}

                    listData = reportServices.getTemplateProductOffControlbyGSP(_db, model);

                    long count = 0;
                    long totalPage = 0;
                    ReportOffControlDetailSearch modelSearch = new ReportOffControlDetailSearch();
                    modelSearch.PageIndex = 1;
                    modelSearch.PageSize = 20; //เอาทั้งหมด
                    modelSearch.SortOrder = "";
                    modelSearch.SortOrder = "PLANT_ID";
                    modelSearch.mSearch = new ReportOffControlDetailSearchModel();

                    List<ViewQMS_RP_OFF_CONTROL_DETAIL> listDataDetail = new List<ViewQMS_RP_OFF_CONTROL_DETAIL>();
                    List<PageIndexList> listPageIndex = new List<PageIndexList>();
                    listDataDetail = reportServices.searchDashQMS_RP_OFF_CONTROL_DETAILEx(_db, modelSearch, model, out count, out totalPage, out listPageIndex);

                    result = new { result = ReturnStatus.SUCCESS, message = listData, listDataDetail = listDataDetail, totalRecords = count, toTalPage = totalPage, listPageIndex = listPageIndex };
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateQMS_TR_PRODUCT_REMARK(CreateQMS_TR_PRODUCT_REMARK model)
        {
            Object result;
            if (ModelState.IsValid)
            {
                MasterDataService masterDataServices = new MasterDataService(_username);

                try
                {
                    long nResult = masterDataServices.SaveQMS_TR_PRODUCT_REMARK(_db, model);

                    if (nResult > 0)
                    {
                        result = new { result = ReturnStatus.SUCCESS, message = Resources.Strings.SaveSuccess, dataId = nResult };
                    }
                    else
                    {
                        result = new { result = ReturnStatus.ERROR, message = Resources.Strings.SaveFailure };
                    }
                }
                catch (Exception ex)
                {
                    result = new { result = ReturnStatus.ERROR, message = this.getEexceptionError(ex) };
                }
            }
            else
            {
                result = new { result = ReturnStatus.ERROR, message = this.getModelStateError(ModelState) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }






    }
}
