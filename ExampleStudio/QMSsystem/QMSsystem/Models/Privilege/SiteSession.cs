﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMSSystem.Model;

namespace QMSsystem.Models.Privilege
{
    public class SiteSession
    {
        
        public int UserID { get; set; } 
        public string Username { get; set; }
        public string UserShowName { get; set; }
        public string DeptName { get; set; }
        public int UserRole { get; set; }
        public string ImagePath { get; set; }
        public List<AuthenAccess> m_Authen { get; set; }
        
         
        public SiteSession()
        { 
        }

        public SiteSession(User_pwd user)
        {
            this.UserID = user.user_id;
            this.Username = user.user_name;
            this.UserRole = user.user_level;
            //
            // TO DO: Cache other user settings!
            //
        }

        public List<AuthenAccess> getAuthenAccess()
        {
            return m_Authen;
        }
    }

    public class User_pwd
    {
        public int user_id { get; set; }
        public string user_name { get; set; }
        public int user_level { get; set; }
    }
}