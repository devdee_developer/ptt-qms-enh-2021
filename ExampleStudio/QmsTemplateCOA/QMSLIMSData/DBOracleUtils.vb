﻿Imports Oracle.DataAccess.Client

Module DBOracleUtils
    Public Function GetDBConnection(host As String, port As Integer, sid As String, user As String, password As String) As OracleConnection
        Dim conn As New OracleConnection()
        Dim connectString As String

        connectString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = " _
                 + host + ")(PORT = " + port + "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = " _
                 + sid + ")));Password=" + password + ";User ID=" + user

        conn.ConnectionString = connectString
        Return conn
    End Function
End Module
