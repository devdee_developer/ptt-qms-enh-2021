﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using OfficeOpenXml;

namespace QMSSystem.CoreDB.Helper
{

    public class OutLetRawTank
    {
        public DateTime m_measureDate { get; set; }
        public double m_pH { get; set; }
        public double m_TURB { get; set; }
        public double m_SS { get; set; }
    }


    public class FolderData
    {
        public string m_dtFile { get; set; }
        public string m_dtFolder { get; set; }
        public string m_dtDirectory { get; set; }
        public bool m_isFolderInRange { get; set; }
        public DateTime m_FolderModify { get; set; }
        public DateTime m_FileModify { get; set; }


        public FolderData(string szFile, string szFolder)
        {
            m_dtFile = "";
            m_dtDirectory  = szFolder;

           

            try
            {
                if (null != szFolder && "" != szFolder)
                {
                    m_FolderModify = File.GetLastWriteTime(szFolder);
                    m_dtFolder = szFolder.Replace(Path.GetDirectoryName(szFolder) + Path.DirectorySeparatorChar, ""); //Path.GetDirectoryName(dtFolder);                    
                }
                else
                {
                    m_dtFolder = "";
                }

                if (null != szFile && "" != szFile)
                {
                    m_FileModify = File.GetLastWriteTime(szFile);
                    m_dtFile = szFile.Replace(Path.GetDirectoryName(szFile) + Path.DirectorySeparatorChar, ""); //Path.GetDirectoryName(dtFolder);                    
                }
                else
                {
                    m_dtFile = szFile;
                }
            }
            catch (Exception ex)
            {
                m_dtDirectory = "";
            }
        }

        public bool IsFolderInYearRange(DateTime start, DateTime end)
        {
            m_isFolderInRange = false;
            try
            {
                if (null != m_dtFolder && "" != m_dtFolder)
                {
                    //กำหนด folder year ให้เป็น คศ วันที่ 1 มกราคม ของปีนั้น
                    DateTime folderDate = DateTime.ParseExact("01-01-" + m_dtFolder, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    DateTime firstDayOfMonth = new DateTime(start.Year, 1, 1); //ปรับให้เป็น วันที่ 1 มค
                    DateTime lastDayOfMonth = new DateTime(end.Year, 2, 1);//ปรับให้เป็นวันที่ 2 มค

                    if (folderDate >= firstDayOfMonth && folderDate <= lastDayOfMonth)
                    {
                        m_isFolderInRange = true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return m_isFolderInRange;
        }

        public bool IsFolderInMonthRange(DateTime start, DateTime end)
        {
            m_isFolderInRange = false;
            try
            {
                if (null != m_dtFolder && "" != m_dtFolder)
                {
                    //กำหนด folder year ให้เป็น คศ วันที่ 1 ของเดือนนั้นๆ
                    DateTime folderDate = DateTime.ParseExact("01-" + m_dtFolder, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    DateTime firstDayOfMonth = new DateTime(start.Year, start.Month, 1); //ปรับให้เป็น วันที่ 1 มค
                    DateTime lastDayOfMonth = new DateTime(end.Year, end.Month, 2);//ปรับให้เป็นวันที่ 2 มค

                    if (folderDate >= firstDayOfMonth && folderDate <= lastDayOfMonth)
                    {
                        m_isFolderInRange = true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return m_isFolderInRange;
        }

        public bool IsFileInRange(string fileName, DateTime start, DateTime end)
        {
            m_isFolderInRange = false;
            try
            {
                int targetLength = fileName.Length;
                int fileLenght = m_dtFile.Length;

                if (fileLenght > targetLength) //ตรวจสอบว่าความยาว มีมากกว่า ค่าที่จะ compare หรือไม่
                {
                    string compare = m_dtFile.Substring(0, targetLength);
                    //Step 1 ตรวจสอบ ชื่อไฟล์
                    if (fileName.ToLower() == compare.ToLower()) //compare data filename เดียวกันหรือไม่
                    {
                        string fileDate = m_dtFile.Substring(targetLength, 10); //ดึง เฉพาะวัน ที่ 00-00-0000 10 ตัวพอดี

                        //กำหนด folder year ให้เป็น คศ วันที่ 1 ของเดือนนั้นๆ
                        DateTime folderDate = DateTime.ParseExact(fileDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        DateTime firstDayOfMonth = new DateTime(start.Year, start.Month, start.Day); //ปรับให้เป็น เวลา ให้เป็น 00:00:00
                        DateTime lastDayOfMonth = new DateTime(end.Year, end.Month, end.Day);//ปรับให้เป็นวันที่ เวลา ให้เป็น 00:00:00

                        if (folderDate >= firstDayOfMonth && folderDate <= lastDayOfMonth)
                        {
                            m_isFolderInRange = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return m_isFolderInRange;
        }

        public bool IsFolderInRangeChecking(DateTime start, DateTime end)
        {
            m_isFolderInRange = false;

            try
            {
                if (null != m_dtFolder && "" != m_dtFolder)
                {
                    //string[] dtMonthYear = dtDirectory.Split('-');//แยก โดย - 
                    DateTime folderDate = DateTime.ParseExact("01-" + m_dtFolder, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    DateTime firstDayOfMonth = new DateTime(start.Year, start.Month, 1);
                    DateTime TempDayOfMonth = new DateTime(end.Year, end.Month, 1);
                    DateTime lastDayOfMonth = TempDayOfMonth.AddMonths(1).AddDays(-1);

                    if (folderDate >= firstDayOfMonth && folderDate <= lastDayOfMonth)
                    {
                        m_isFolderInRange = true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return m_isFolderInRange;
        }
    }


    public class FileData
    {
        public List<OutLetRawTank> m_listData = new List<OutLetRawTank>();

        public void getAllDataInDirectory(string filePath, DateTime start, DateTime end)
        {
            try
            {
                string supportedExtensions = "*.xls,*.xlsx";
                foreach (string excelFile in Directory.GetFiles(filePath, "*.*", SearchOption.AllDirectories).Where(s => supportedExtensions.Contains(Path.GetExtension(s).ToLower())))
                {
                    //do work here
                    ExcelPackage excel = null;
                    try
                    {
                        string filename = Path.GetFileName(excelFile);

                        //FileInfo fileInfo = new FileInfo(excelFile);
                        FileStream fileStream = new FileStream(excelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        excel = new ExcelPackage(fileStream);



                        var worksheet = excel.Workbook.Worksheets["utility"];

                        //Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                        //Microsoft.Office.Interop.Excel.Range range;
                        //wb = excel.Workbooks.Open(excelFile);
                        //Worksheet excelSheet = wb.ActiveSheet;
                        //range = excelSheet.UsedRange;
                        OutLetRawTank tempData = new OutLetRawTank();
                        //Read the first cell 
                        tempData.m_pH = Convert.ToDouble(worksheet.Cells["D8"].Value);
                        tempData.m_TURB = Convert.ToDouble(worksheet.Cells["F8"].Value);
                        tempData.m_SS = Convert.ToDouble(worksheet.Cells["U8"].Value);
                        long dateNum = long.Parse(worksheet.Cells["X4"].Value.ToString());
                        tempData.m_measureDate = DateTime.FromOADate(dateNum);

                        m_listData.Add(tempData);

                    }
                    catch (Exception ex)
                    {

                    }

                    if (null != excel)
                        excel.Dispose();
                }


            }
            catch (Exception ex)
            {

            }
        }


        //public bool IsFolderInRanageChecking(DateTime start, DateTime end)
        //{
        //    m_isFolderInRange = false;

        //    try
        //    {
        //        if (null != m_dtDirectory && "" != m_dtDirectory)
        //        {
        //            //string[] dtMonthYear = dtDirectory.Split('-');//แยก โดย - 
        //            DateTime folderDate = DateTime.ParseExact("01-" + m_dtDirectory, "dd-MM-yyyy", CultureInfo.InvariantCulture);

        //            DateTime firstDayOfMonth = new DateTime(start.Year, start.Month, 1);
        //            DateTime TempDayOfMonth = new DateTime(end.Year, end.Month, 1);
        //            DateTime lastDayOfMonth = TempDayOfMonth.AddMonths(1).AddDays(-1);

        //            if (folderDate >= firstDayOfMonth && folderDate <= lastDayOfMonth)
        //            {
        //                m_isFolderInRange = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return m_isFolderInRange;
        //} 
    }
}
